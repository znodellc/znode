﻿<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Secure.Orders.ReturnsManagement.RMAConfiguration.Default" ValidateRequest="false"
    CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <script type="text/javascript">
            function textboxMultilineMaxNumber(txt, maxLen) {
                try {
                    if (txt.value.length > (maxLen - 1))
                        return false;
                } catch (e) {
                }
            }
        </script>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Localize ID="TitleRMAConfiguration" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextRMAConfiguration %>'></asp:Localize></h1>
            <p style="width: 650px;">
                <asp:Localize ID="TextRMA" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextRMAConfiguration %>'></asp:Localize>
            </p>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer10" SpacerHeight="50" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div style="clear: both;">
            <ajaxToolKit:TabContainer ID="tabStoreSettings" runat="server">
                <ajaxToolKit:TabPanel ID="pnlGeneral" runat="server">
                    <HeaderTemplate>
                        <asp:Localize ID="TabTitleGeneral" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleGeneral %>'></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="FormView">
                            <div>
                                <ZNode:Spacer ID="Spacer18" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>

                            <div class="FieldStyle1">
                                <asp:Localize ID="ColumnTitleMaximumDaysRMA" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleMaximumDaysRMA %>'></asp:Localize><br />
                                <asp:Localize ID="ColumnTitleMaximumDaysRMARequest" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleMaximumDaysRMARequest %>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtMaxNoOfDays" MaxLength="5" runat="server" Width="100px"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="reqMaxNoOfDays" runat="server" ControlToValidate="txtMaxNoOfDays"
                                    ErrorMessage='<%$Resources:ZnodeAdminResource, RequiredEnterMaximumnumberofdays %>' CssClass="Error" Display="Dynamic" ValidationGroup="Genral" ValidateRequestMode="Enabled"></asp:RequiredFieldValidator>

                                <asp:RegularExpressionValidator ID="regMaxNoofDays" runat="server" ControlToValidate="txtMaxNoOfDays"
                                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularEntervalidnumberofdays %>' Display="Dynamic" ValidationExpression='<%$ Resources:ZnodeAdminResource, RqgularValidExpressionNoofDays %>' ValidationGroup="General"
                                    CssClass="Error"></asp:RegularExpressionValidator>
                            </div>
                            <div class="FieldStyle1">
                                <asp:Localize ID="EmailNotifications" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEmailNotifications %>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:DropDownList ID="ddlEnable" runat="server">
                                    <asp:ListItem Selected="True" Text='<%$ Resources:ZnodeAdminResource, DropDownTextYes %>' Value="1"></asp:ListItem>
                                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextNo %>' Value="0"></asp:ListItem>
                                </asp:DropDownList>

                            </div>

                            <div class="FieldStyle1">
                                <asp:Localize ID="ReturnsDepartmentTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColunbTitleReturnsDepartmentTitle %>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtDisplayName" MaxLength="100" runat="server" Width="255px"></asp:TextBox>
                                <div class="tooltip RMAConfiguration">
                                    <a href="javascript:void(0);" class="learn-more"><span>
                                        <asp:Localize ID="Localize1" runat="server"></asp:Localize></span></a>
                                    <div class="content">
                                        <h6>
                                            <asp:Localize ID="HintHelp" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintHelp %>'></asp:Localize></h6>
                                        <p>
                                            <asp:Localize ID="RMATitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintSubTextRMATitle %>'></asp:Localize>
                                        </p>

                                    </div>
                                </div>
                            </div>
                            <div class="FieldStyle1">
                                <asp:Localize ID="CustomerServiceEmailID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCustomerServiceEmailID %>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtdepartmentemail" MaxLength="100" runat="server" Width="250px"></asp:TextBox>

                                <asp:RegularExpressionValidator ID="regdepartmentemail" runat="server" ControlToValidate="txtdepartmentemail"
                                    ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularValidEmail %>' ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidEmailIdAddress %>'
                                    CssClass="Error" Display="Dynamic" ValidationGroup="General"></asp:RegularExpressionValidator>

                            </div>

                            <div class="FieldStyle1">
                                <asp:Localize ID="MailingAddressforReturns" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleMailingAddressforReturns %>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtAddress" runat="server" Width="250px" MaxLength="250"></asp:TextBox>
                                <div class="tooltip RMAConfiguration">
                                    <a href="javascript:void(0);" class="learn-more"><span>
                                        <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                                    <div class="content">
                                        <h6>
                                            <asp:Localize ID="HintHelpText" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintHelp %>'></asp:Localize></h6>
                                        <p>

                                            <asp:Localize ID="RMAmailingAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintSubTextRMAmailingAddress %>'></asp:Localize>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="FieldStyle1">
                                <asp:Localize ID="ShippingInstructions" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleShippingInstructions %>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingDirection" runat="server" Width="250px" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                <div class="tooltip RMAConfiguration">
                                    <a href="javascript:void(0);" class="learn-more"><span>
                                        <asp:Localize ID="Localize3" runat="server"></asp:Localize></span></a>
                                    <div class="content">
                                        <h6>
                                            <asp:Localize ID="Help" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintHelp %>'></asp:Localize></h6>
                                        <p>

                                            <asp:Localize ID="TextShippingInstructions" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintSubTextShippingInstructions %>'></asp:Localize>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="ClearBoth">
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer1110" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>

                            </div>
                            <div>
                                <zn:Button runat="server" ID="btnCancel" ButtonType="CancelButton" OnClick="btnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
                                <zn:Button runat="server" ID="btnSave" ButtonType="SubmitButton" OnClick="btnSave_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSave%>' CausesValidation="true" ValidationGroup="Genral" />

                            </div>

                        </div>
                    </ContentTemplate>

                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="TabPanel2" runat="server">
                    <HeaderTemplate>
                        <asp:Localize ID="ReasonsforReturn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleReasonsforReturn %>'></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>

                        <div class="ViewForm200">
                            <asp:Panel ID="pnlDiplayReasonGrid" runat="server" Visible="true">
                                <div>
                                    <ZNode:Spacer ID="Spacer31" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                                </div>
                                <div class="ImageButtons">
                                    <div class="ButtonStyle">
                                        <zn:LinkButton ID="lbCreateReason" runat="server" ButtonType="Button" OnClick="lbCreateReason_Click"
                                            Text='<%$ Resources:ZnodeAdminResource, LinkButtonCreateReturnReason %>' CausesValidation="false" ButtonPriority="Primary" />
                                    </div>
                                    <div>
                                        <ZNode:Spacer ID="Spacer8" runat="server" SpacerHeight="15" SpacerWidth="3" />
                                    </div>
                                </div>
                                <div colspan="2" valign="middle">

                                    <asp:GridView ID="gvReason" ShowHeader="true" CaptionAlign="Left" runat="server"
                                        ForeColor="Black" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid"
                                        Width="100%" GridLines="None" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridReasonsReturnsEmptyData %>'
                                        AllowPaging="True" PageSize="20" OnRowCommand="gvReason_RowCommand" OnRowCreated="gvReason_RowCreated"
                                        OnPageIndexChanging="gvReason_PageIndexChanging">
                                        <Columns>
                                            <asp:BoundField DataField="ReasonForReturnID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" Visible="false">
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleReturnReason %>' HeaderStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsEnabled %>' HeaderStyle-HorizontalAlign="Left" Visible="false">
                                                <ItemTemplate>
                                                    <img alt="" id="Img11" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsEnabled").ToString()))%>'
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAction %>' HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton CausesValidation="false" ID="EditProductView" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>'
                                                        CommandArgument='<%# Eval("ReasonForReturnID") %>'
                                                        CommandName="EditReason" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CommandArgument='<%# Eval("ReasonForReturnID") %>'
                                                            CommandName="RemoveItem" Visible='<%# HideDeleteButton(Eval("ReasonForReturnID").ToString()) %>' CausesValidation="false"
                                                            runat="server" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                        <FooterStyle CssClass="FooterStyle" />
                                    </asp:GridView>


                                </div>
                            </asp:Panel>

                            <zn:Button ID="btnShowPopup" runat="server" Style="display: none" />

                            <ajaxToolKit:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="btnShowPopup"
                                PopupControlID="pnlCreateReason" BackgroundCssClass="modalBackground" />


                            <asp:Panel ID="pnlCreateReason" runat="server" Style="display: none;" CssClass="PopupStyle" Width="400" Visible="true">


                                <h4 class="SubTitle">
                                    <asp:Label ID="lblTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCreateReturnReason %>'></asp:Label></h4>


                                <div class="FieldStyle" style="width: 40%">
                                    <asp:Localize ID="Localize18" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>'></asp:Localize>
                                </div>
                                <div class="ValueStyle" style="width: 55%">
                                    <asp:TextBox ID="txtName" runat="server" MaxLength="250" Width="200px"></asp:TextBox>

                                    <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtName" ValidationGroup="grpReasonForReturn"
                                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredName %>' CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>

                                </div>
                                <div>
                                    <ZNode:Spacer ID="Spacer2" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>

                                </div>
                                <div class="FieldStyle" style="width: 40%">
                                    <asp:Localize ID="ColumnEnabled" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnable %>'></asp:Localize>
                                </div>
                                <div class="ValueStyle" style="width: 55%">
                                    <asp:DropDownList ID="ddlReasonenabled" runat="server">
                                        <asp:ListItem Selected="True" Text='<%$ Resources:ZnodeAdminResource, DropDownTextYes %>' Value="1"></asp:ListItem>
                                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextNo %>' Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnReasonID" runat="server" Value="0" />


                                </div>

                                <div class="ClearBoth">
                                </div>
                                <div>
                                    <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>

                                </div>
                                <div>
                                    <div>
                                        <zn:Button Width="100px" ID="btnReasonOk" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonOk %>' Visible="false" runat="server" OnClick="btnReasonOk_Click" />
                                    </div>
                                    <zn:Button runat="server" ID="btnSaveReason" ButtonType="SubmitButton" OnClick="btnSaveReason_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSave%>' CausesValidation="true" />
                                    <zn:Button runat="server" ID="btnCancelReason" ButtonType="CancelButton" OnClick="btnCancelReason_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />

                                </div>
                            </asp:Panel>
                        </div>

                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlrequestStatuses" runat="server">
                    <HeaderTemplate>
                        <asp:Localize ID="RequestStatuses" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleRequestStatuses %>'></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="ViewForm200" id="5">

                            <asp:Panel ID="pnlRequeststatus" runat="server">
                                <div>
                                    <ZNode:Spacer ID="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                                </div>

                                <div colspan="2" valign="middle">

                                    <asp:GridView ID="gvRequeststatus" ShowHeader="true" CaptionAlign="Left" runat="server"
                                        ForeColor="Black" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid"
                                        Width="100%" GridLines="None" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridRMARequestEmptyData %>'
                                        AllowPaging="True" PageSize="20" OnRowCommand="gvRequeststatus_RowCommand" OnRowCreated="gvRequeststatus_RowCreated"
                                        OnPageIndexChanging="gvRequeststatus_PageIndexChanging">
                                        <Columns>
                                            <asp:BoundField DataField="RequestStatusID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" Visible="false">
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleRequestStatus %>' HeaderStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:BoundField>

                                            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAction %>' HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="EditProductView" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CausesValidation="false"
                                                        CommandArgument='<%# Eval("RequestStatusID") %>' CommandName="EditStatus" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                       <%-- <asp:LinkButton ID="Delete" Text="Delete &raquo" CommandArgument='<%# Eval("RequestStatusID") %>' CausesValidation="false"
                                                            CommandName="DeleteStatus" runat="server" />--%>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                        <FooterStyle CssClass="FooterStyle" />
                                    </asp:GridView>


                                </div>
                            </asp:Panel>

                            <zn:Button ID="btnShowPopupReqStatus" runat="server" Style="display: none" />

                            <ajaxToolKit:ModalPopupExtender ID="mdlPopupReqStatus" runat="server" TargetControlID="btnShowPopupReqStatus"
                                PopupControlID="pnlEditRequestStatus" BackgroundCssClass="modalBackground" />

                            <asp:Panel ID="pnlEditRequestStatus" runat="server" Visible="true" Style="display: none;" CssClass="PopupStyle" Width="620">

                                <div class="FormView">
                                    <div>
                                        <asp:HiddenField ID="hdnRequestID" runat="server" Value="0" />
                                    </div>
                                    <h4 class="SubTitle">
                                        <asp:Label ID="lblRequest" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleEditStatus %>'></asp:Label></h4>
                                    <div class="FieldStyle" style="width: 40%">
                                        <asp:Localize ID="Name" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle" style="width: 55%">
                                        <asp:TextBox ID="txtStatusName" runat="server" Width="180px" Enabled="false"></asp:TextBox>
                                    </div>
                                    <div class="FieldStyle" style="width: 40%">
                                        <asp:Localize ID="NotificationMessageTo" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNotificationMessageTo %>'></asp:Localize>
                                        <br />
                                        <asp:Localize ID="Customer" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCustomer %>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle" style="width: 55%">
                                        <asp:TextBox ID="txtCustomerNotification" runat="server" Width="350px" TextMode="MultiLine" Height="100px"></asp:TextBox>
                                    </div>
                                    <div class="FieldStyle" style="width: 40%">
                                        <asp:Localize ID="Localize24" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNotificationMessageTo %>'></asp:Localize>
                                        <br />
                                        <asp:Localize ID="Administrator" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAdministrator %>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle" style="width: 55%">
                                        <asp:TextBox ID="txtAdminNotification" runat="server" Width="350px" TextMode="MultiLine" Height="100px"></asp:TextBox>
                                    </div>
                                    <div class="ClearBoth">
                                    </div>
                                    <div>
                                        <div>
                                            <zn:Button Width="100px" ID="btnRequestOK" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonOk %>' Visible="False" runat="server" OnClick="btnRequestOK_Click"/>
                                        </div>
                                        <zn:Button runat="server" ID="btnCancelStatus" ButtonType="CancelButton" OnClick="btnCancelStatus_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
                                        <zn:Button runat="server" ID="btnSaveStatus" ButtonType="SubmitButton" OnClick="btnSaveStatus_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSave%>' CausesValidation="true" />

                                    </div>

                                </div>
                            </asp:Panel>
                        </div>

                    </ContentTemplate>
                </ajaxToolKit:TabPanel>

                <ajaxToolKit:TabPanel ID="pnlIssueGC" runat="server">
                    <HeaderTemplate>
                        <asp:Localize ID="TabIssueGiftCard" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleIssueGiftCard %>'></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="FormView">
                            <div>
                                <ZNode:Spacer ID="Spacer28" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div class="FieldStyle1">
                                <asp:Localize ID="Daysuntilexpiration" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDaysuntilexpiration %>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtGCExpiration" MaxLength="5" runat="server" Width="100px"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="ReqGCExpiration" runat="server" ControlToValidate="txtGCExpiration"
                                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterNumberofdays %>' CssClass="Error" Display="Dynamic" ValidationGroup="GC"></asp:RequiredFieldValidator>

                                <asp:RegularExpressionValidator ID="RegGCExpiration" runat="server" ControlToValidate="txtGCExpiration"
                                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularEntervalidnumberofdays %>' Display="Dynamic" ValidationExpression="[0-9]+" ValidationGroup="GC"
                                    CssClass="Error"></asp:RegularExpressionValidator>
                            </div>
                            <div class="FieldStyle1">
                                <asp:Localize ID="ColumnNotificationsenttoCustomer" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNotificationsenttoCustomer %>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtGCNotification" runat="server" Width="250px" Rows="5" TextMode="MultiLine"></asp:TextBox>
                            </div>

                            <div class="ClearBoth">
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer210" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>

                            </div>
                            <div>
                                <zn:Button runat="server" ID="btnGCCancel" ButtonType="CancelButton" OnClick="btnGCCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
                                <zn:Button runat="server" ID="btnGCSubmit" ButtonType="SubmitButton" OnClick="btnGCSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSave%>' CausesValidation="true" />
                            </div>
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
            </ajaxToolKit:TabContainer>
        </div>
    </div>

</asp:Content>
