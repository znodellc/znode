<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.Default" Title="Manage Suppliers" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <div class="LeftMargin">
        <h1><asp:Localize runat="server" ID="OrderManagement" Text='<%$ Resources:ZnodeAdminResource, TitleOrderManagement %>'></asp:Localize></h1>
        <hr />

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/approve orders.png" />
            </div>
            <div class="Shortcut"><a id="A2" href="~/Secure/Orders/OrderManagement/ViewOrders/Default.aspx" runat="server"><asp:Localize runat="server" ID="ViewOrders" Text='<%$ Resources:ZnodeAdminResource, LinkTextViewOrders %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="ViewOrdersText" Text='<%$ Resources:ZnodeAdminResource, TextViewOrders %>'></asp:Localize></p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/create-an-order.png" />
            </div>
            <div class="Shortcut"><a id="A3" href="~/Secure/Orders/OrderManagement/OrderDesk/Default.aspx" runat="server"><asp:Localize runat="server" ID="CreateOrder" Text='<%$ Resources:ZnodeAdminResource, LinkTextCreateOrder %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="CreateOrderText" Text='<%$ Resources:ZnodeAdminResource, TextCreateOrder %>'></asp:Localize></p>
            </div>
        </div>

        <h1><asp:Localize runat="server" ID="CustomerManagement" Text='<%$ Resources:ZnodeAdminResource, TitleCustomerManagement %>'></asp:Localize></h1>
        <hr />

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/profiles.png" />
            </div>
            <div class="Shortcut"><a id="A11" href="~/Secure/Orders/CustomerManagement/Profiles/default.aspx" runat="server"><asp:Localize runat="server" ID="Profiles" Text='<%$ Resources:ZnodeAdminResource, LinkTextProfiles %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="ProfilesText" Text='<%$ Resources:ZnodeAdminResource, TextProfiles %>'></asp:Localize></p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/CustomerAcctManager.png" />
            </div>
            <div class="Shortcut"><a id="A6" href="~/Secure/Orders/CustomerManagement/Customers/Default.aspx" runat="server"><asp:Localize runat="server" ID="Customers" Text='<%$ Resources:ZnodeAdminResource, LinKTextCusotmers %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="CustomersText" Text='<%$ Resources:ZnodeAdminResource, TextCustomers %>'></asp:Localize></p>
            </div>
        </div>


        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/ServiceRequests.png" />
            </div>
            <div class="Shortcut"><a id="A1" href="~/Secure/Orders/CustomerManagement/ServiceRequests/Default.aspx" runat="server"><asp:Localize runat="server" ID="ServiceRequests" Text='<%$ Resources:ZnodeAdminResource, LinkTextServiceRequests %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="ServiceRequestsText" Text='<%$ Resources:ZnodeAdminResource, TextServiceRequests %>'></asp:Localize></p>
            </div>
        </div>

        <h1><asp:Localize runat="server" ID="ReturnsManagement" Text='<%$ Resources:ZnodeAdminResource, TitleReturnsManagement %>'></asp:Localize></h1>
        <hr />
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/RMAconfiguration.png" />
            </div>
            <div class="Shortcut"><a id="A4" href="~/Secure/Orders/ReturnsManagement/RMAConfiguration/Default.aspx" runat="server"><asp:Localize runat="server" ID="RMAConfiguration" Text='<%$ Resources:ZnodeAdminResource, LinkTextRMAConfiguration %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="RMAConfigurationText" Text='<%$ Resources:ZnodeAdminResource, TextRMAConfiguration %>'></asp:Localize></p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/RMAmanager.png" />
            </div>

            <div class="Shortcut"><a id="A7" href="~/Secure/Orders/ReturnsManagement/RMAManager/Default.aspx" runat="server"><asp:Localize runat="server" ID="RMAManager" Text='<%$ Resources:ZnodeAdminResource, LinkTextRMAManager %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="RMAManagerText" Text='<%$ Resources:ZnodeAdminResource, TextRMAManager %>'></asp:Localize></p>
            </div>

        </div>

        <h1><asp:Localize runat="server" ID="SupplierManagement" Text='<%$ Resources:ZnodeAdminResource, TitleSupplierManagement %>'></asp:Localize></h1>
        <hr />

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/suppliers.png" />
            </div>
            <div class="Shortcut"><a id="A8" href="~/Secure/Orders/SupplierManagement/Suppliers/Default.aspx" runat="server"><asp:Localize runat="server" ID="Suppliers" Text='<%$ Resources:ZnodeAdminResource, LinkTextSuppliers %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, TextSuppliers %>'></asp:Localize></p>
            </div>
        </div>
    </div>
</asp:Content>

