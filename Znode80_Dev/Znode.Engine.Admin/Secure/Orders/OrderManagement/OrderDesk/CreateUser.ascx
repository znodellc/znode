<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.OrderManagement.OrderDesk.CreateUser"
    CodeBehind="CreateUser.ascx.cs" %>
<%@ Register Src="~/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<div class="FormView">
    <h4 class="SubTitle">
        <asp:Localize ID="SubTitleUpdateCustomerAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleUpdateCustomerAddress%>'></asp:Localize></h4>
    <div>
        <uc1:spacer ID="Spacer1" SpacerHeight="4" SpacerWidth="10" runat="server"></uc1:spacer>
    </div>
    <asp:UpdatePanel ID="pnlCustomerDetail" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <asp:Panel ID="pnlBillingAddress" runat="server">
                    <div class="LeftFloat">
                        <div>
                            <div class="HeaderStyle" colspan="2">
                                <asp:Localize ID="ColumnBillingAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBillingAddress%>'></asp:Localize>
                            </div>
                        </div>
                        <div runat="server" id="divBillingAddressName">
                            <div class="FieldStyle">
                                <asp:Label ID="lbl" runat="server" Text="Address Name"></asp:Label>
                            </div>
                            <div class="ValueStyle">
                                <asp:DropDownList ID="ddlBillingAddressName" Width="180px" runat="server" AutoPostBack="True"
                                    OnSelectedIndexChanged="DdlAddressName_SelectedIndexChanged">
                                </asp:DropDownList>
                                <br />
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAddressName%>'></asp:Localize><span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtBillingAddressName" ValidationGroup="groupCreateUser" runat="server"
                                    Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAddressName%>' ControlToValidate="txtBillingAddressName"
                                    runat="server" Display="Dynamic" CssClass="Error" ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnFirstName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFirstName%>'></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingFirstName" ValidationGroup="groupCreateUser" runat="server"
                                Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="req1" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredFirstName%>' ControlToValidate="txtBillingFirstName"
                                        runat="server" Display="Dynamic" CssClass="Error" ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnLastName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLastName%>'></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingLastName" ValidationGroup="groupCreateUser" runat="server"
                                Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredLastName%>'
                                        ControlToValidate="txtBillingLastName" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnCompanyName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCompanyName%>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingCompanyName" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnStreet1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStreet1%>'></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingStreet1" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterStreet%>'
                                    ControlToValidate="txtBillingStreet1" runat="server" Display="Dynamic" CssClass="Error"
                                    ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnStreet2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStreet2%>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingStreet2" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnCity" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCity%>'></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingCity" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterCity%>'
                                    ControlToValidate="txtBillingCity" runat="server" Display="Dynamic" CssClass="Error"
                                    ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnState" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnState%>'></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingState" runat="server" Width="30" Columns="10" MaxLength="2"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterState%>'
                                    ControlToValidate="txtBillingState" runat="server" Display="Dynamic" CssClass="Error"
                                    ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnPostalCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPostalCode%>'></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingPostalCode" runat="server" Width="130" Columns="30" MaxLength="10"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterPostalCode%>'
                                    ControlToValidate="txtBillingPostalCode" runat="server" Display="Dynamic" CssClass="Error"
                                    ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnCountry" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCountry%>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="lstBillingCountryCode" Width="200" runat="server">
                            </asp:DropDownList>
                            <div>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterCountry%>'
                                    ControlToValidate="lstBillingCountryCode" runat="server" Display="Dynamic" CssClass="Error"
                                    ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize11" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPhoneNumber%>'></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingPhoneNumber" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterPhoneNumber%>'
                                    ControlToValidate="txtBillingPhoneNumber" runat="server" Display="Dynamic" CssClass="Error"
                                    ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnEmailAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEmailAddress%>'></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtEmailAddress" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" Width="450px"
                                    ValidationGroup="groupCreateUser" ControlToValidate="txtEmailAddress" CssClass="Error"
                                    Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterEmailAddress%>'>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" Width="10px" ControlToValidate="txtEmailAddress"
                                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularEmailAddress %>' Display="Dynamic"
                                    ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularValidEmailExpression%>'
                                    ValidationGroup="groupCreateUser" CssClass="Error">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <div class="LeftFloat" style="padding-left: 50px">
                    <asp:Panel ID="pnlShipping" runat="server" Visible="true">
                        <div class="Form">
                            <div class="HeaderStyle" colspan="2">
                                <asp:Localize ID="ColumnShippingAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingAddress%>'></asp:Localize>
                            </div>
                            <div runat="server" id="divShippingAddressName">
                                <div class="FieldStyle">
                                    <asp:Label ID="Label1" runat="server" Text="Address Name"></asp:Label>
                                </div>
                                <div class="ValueStyle">
                                    <asp:DropDownList ID="ddlShippingAddressName" runat="server" AutoPostBack="True"
                                        Width="180px" OnSelectedIndexChanged="DdlAddressName_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <br />
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnAddressName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAddressName%>'></asp:Localize><span class="Asterix">*</span>
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="txtShippingAddressName" ValidationGroup="groupCreateUser" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAddressName%>' ControlToValidate="txtShippingAddressName"
                                        runat="server" Display="Dynamic" CssClass="Error" ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFirstName%>'></asp:Localize><span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingFirstName" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredFirstName%>'
                                        ControlToValidate="txtShippingFirstName" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLastName%>'></asp:Localize><span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingLastName" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredLastName%>'
                                        ControlToValidate="txtShippingLastName" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCompanyName%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingCompanyName" runat="server" Width="130" Columns="30"
                                    MaxLength="100"></asp:TextBox>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="Localize5" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStreet1%>'></asp:Localize><span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingStreet1" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterStreet%>'
                                        ControlToValidate="txtShippingStreet1" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnShippingStreet" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStreet2%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingStreet2" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnShippingCity" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCity%>'></asp:Localize><span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingCity" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterCity%>'
                                        ControlToValidate="txtShippingCity" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnShippingState" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnState%>'></asp:Localize><span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingState" runat="server" Width="30" Columns="10" MaxLength="2"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterState%>'
                                        ControlToValidate="txtShippingState" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="Localize6" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPostalCode%>'></asp:Localize><span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingPostalCode" runat="server" Width="130" Columns="30" MaxLength="10"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator14" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterPostalCode%>'
                                        ControlToValidate="txtShippingPostalCode" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnShippingCountry" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCountry%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:DropDownList ID="lstShippingCountryCode" Width="200" runat="server">
                                </asp:DropDownList>
                                <div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator15" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterCountry%>'
                                        ControlToValidate="lstShippingCountryCode" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnPhoneNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPhoneNumber%>'></asp:Localize><span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingPhoneNumber" runat="server" Width="130" Columns="30"
                                    MaxLength="100"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterPhoneNumber%>'
                                            ControlToValidate="txtShippingPhoneNumber" runat="server" Display="Dynamic" CssClass="Error"
                                            ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                    </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="ClearBoth">
                </div>
                <div class="FieldStyle">
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="chkSameAsBilling" runat="server" Text='<%$ Resources:ZnodeAdminResource, CheckboxTextShippingAddressSame%>'
                        Checked="false" OnCheckedChanged="ChkSameAsBilling_CheckedChanged" AutoPostBack="true" />
                </div>
                <div class="Error">
                    <asp:Literal ID="lblError" runat="server" EnableViewState="false"></asp:Literal>
                </div>
                <div>
                    <uc1:spacer ID="Spacer2" SpacerHeight="4" SpacerWidth="10" runat="server"></uc1:spacer>
                </div>
                <br />
                <div>
                    <div class="ClearBoth">
                        <div align="center">
                            <zn:Button runat="server" ButtonType="EditButton" Width="150px" OnClick="BtnUpdate_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCreateAccount%>' ID="btnUpdate" ValidationGroup="groupCreateUser" />
                            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelBottom" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
