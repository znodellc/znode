﻿<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.OrderManagement.OrderDesk.OrderDeskReceipt" CodeBehind="OrderDeskReceipt.aspx.cs" %>

<%@ Register Src="~/Secure/Orders/OrderManagement/OrderDesk/Confirm.ascx" TagName="OrderReceipt" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div align="right">
        <zn:LinkButton ID="AddContact" runat="server" CausesValidation="false" Text='<%$ Resources:ZnodeAdminResource, LinkButtonCreateNewOrder%>'
            CommandArgument="CUSTOMER" OnCommand="LinkNewOrder_Click"
            ButtonType="Button" ButtonPriority="Primary" />
    </div>
    <!-- Order Receipt -->
    <div>
        <ZNode:OrderReceipt ID="uxConfirm" runat="server" />
    </div>
    <div>
        <ZNode:Spacer SpacerHeight="20" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <zn:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" Text='<%$ Resources:ZnodeAdminResource, LinkButtonCreateNewOrder%>'
            CommandArgument="CUSTOMER" OnCommand="LinkNewOrder_Click"
            ButtonType="Button" ButtonPriority="Primary" />
    </div>
    <div>
        <ZNode:Spacer SpacerHeight="40" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>



   
</asp:Content>
