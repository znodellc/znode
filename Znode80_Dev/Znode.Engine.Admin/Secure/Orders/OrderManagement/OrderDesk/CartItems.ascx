﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CartItems.ascx.cs" Inherits="Znode.Engine.Admin.Secure.Orders.OrderManagement.OrderDesk.CartList" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>
<%@ Register Src="MultipleAddressCart.ascx" TagName="ShoppingCart" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Accounts/Address.ascx" TagName="NewAddress" TagPrefix="ZNode"  %>

<div class="ShoppingCart">
    <asp:UpdatePanel ID="updatePnlCart" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pnlCartItems" runat="server">
                <zn:Button runat="server" ID="btnNewAddress" Style="display: none"  />
                <div class="NewAddressText">
                    <asp:Localize ID="TextCartItem" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCartItem%>'></asp:Localize>
                </div>
                <zn:Button runat="server" ID="btnShowMultipleAddress" Style="display: none"/>
                <div class="Clear">
                    <div class="Mylink">
                        <asp:UpdatePanel ID="UpdatePnlNewAddress" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:LinkButton CausesValidation="false" ID="addNewAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource, ButtonAddNewAddress%>'
                                    OnClick="addNewAddress_Click" />
                                <div style="float: right">
                                    <asp:LinkButton ID="btnClose" runat="server" CausesValidation="false" OnClick="btnClose_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClose%>'></asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <ajaxToolKit:ModalPopupExtender ID="mdlNewAddressPopup" runat="server" TargetControlID="btnNewAddress"
                            PopupControlID="pnlNewAddress" BackgroundCssClass="modalBackground" />

                        <asp:Panel ID="pnlNewAddress" runat="server" Style="display: none;" CssClass="PopupStyle CustomerAddressPopStyle">
                            <div>
                                <ZNode:NewAddress ID="uxNewAddress" runat="server" />
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <div class="Error">
                    <asp:Label ID="uxMsg" EnableViewState="false" runat="server"></asp:Label>
                </div>
                <br />
                <asp:Panel ID="pnlShoppingCart" runat="server" >
                    <div class="Error">
                        <asp:Label ID="uxErrorMsg" runat="server"></asp:Label>
                    </div>
                    <div>
                        <ZNode:ShoppingCart ID="uxCart" runat="server" />
                    </div>
                    <div class="Continue">
                        <zn:Button runat="server" ButtonType="EditButton" OnClick="btnContinue_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonContinue%>' ID="btnContinue" CausesValidation="false" Width="100px"/>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
