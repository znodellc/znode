﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultipleAddressCart.ascx.cs" Inherits="Znode.Engine.Admin.Secure.Orders.OrderManagement.OrderDesk.MultipleAddressCart" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<div>
    <asp:UpdatePanel ID="updatePnlAddress" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Repeater ID="rptCartItems" runat="server" OnItemDataBound="rptCartItems_ItemDataBound">
                <HeaderTemplate>
                    <div class="MultiShipHeader">
                        <div class="QTY">
                           <asp:Localize ID="ColumnTitleQuantity" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantity%>'></asp:Localize> 
                        </div>
                        <div class="PdtImage">
                           <asp:Localize ID="ColumnTitleProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProduct%>'></asp:Localize>  
                        </div>
                        <div class="Product">
                            &nbsp;
                        </div>
                        <div class="ShipTo"><asp:Localize ID="ColumnShipTo" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShipTo%>'></asp:Localize></div>
                    </div>

                </HeaderTemplate>
                <ItemTemplate>
                    <asp:GridView ID="uxCart" class="TableContainer" runat="server" AutoGenerateColumns="False"
                        EmptyDataText="Shopping Cart Empty" GridLines="None" CellPadding="4" OnRowCommand="Cart_RowCommand"
                        OnRowDataBound="Cart_RowDataBound" CssClass="Grid" ShowHeader="false">
                        <Columns>
                            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantity%>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="120px">
                                <ItemTemplate>
                                    <asp:DropDownList ID="uxQty" runat="server">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="GUID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ItemGUID").ToString() %>' />
                                    <asp:HiddenField ID="SLNO" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "SlNo").ToString() %>' />

                                    <div class="Mylink">
                                        <asp:LinkButton ID="lnkRemoveLineItem" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SlNo").ToString() %>'
                                            CommandName="remove" Text='<%$ Resources:ZnodeAdminResource, ButtonRemove%>' CssClass="mylink" ToolTip='<%$ Resources:ZnodeAdminResource, TextRemovethisItem%>' CausesValidation="false" />
                                    </div>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />

                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                <ItemTemplate>
                                        <img id="imgProduct" enableviewstate="false" alt='<%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ImageAltTag")%>'
                                            border='0' src='<%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ThumbnailImageFilePath") %>'
                                            runat="server" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle CssClass="QTY" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProduct%>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="320px">
                                <ItemTemplate>
                                    <div class="ProductName">
                                        <asp:Label ID="lblProductName" runat="server" Text='<%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"Name")%>'></asp:Label>
                                    </div>
                                    <div class="Description" enableviewstate="false">
                                        Item#
                        <%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ProductNum")%><br>
                                        <%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ShoppingCartDescription")%>
                                    </div>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleShipTo%>' HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlShippingAddress" runat="server"></asp:DropDownList>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:TemplateField>

                        </Columns>
                        <FooterStyle CssClass="Footer" />
                        <RowStyle CssClass="Row" />
                        <HeaderStyle CssClass="Header" />
                        <AlternatingRowStyle CssClass="AlternatingRow" />
                    </asp:GridView>
                </ItemTemplate>
            </asp:Repeater>
            <uc1:Spacer ID="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
            <div class="Error" style="margin-bottom: 20px;">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </div>
            <div class="UpdatedQuantity">
                <asp:Localize ID="ColumnTextChangedQuantities" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextChangedQuantities%>'></asp:Localize>&nbsp;&nbsp;
                <zn:Button runat="server" ButtonType="EditButton" OnClick="btnUpdate_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonUpdate%>' ID="btnUpdate" CausesValidation="false" Width="100px" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
