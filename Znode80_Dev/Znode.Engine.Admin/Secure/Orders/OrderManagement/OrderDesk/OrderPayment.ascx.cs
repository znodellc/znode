using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using EnumPaymentType = ZNode.Libraries.ECommerce.Entities.PaymentType;

/// <summary>
/// Checkout Payment
/// </summary>
namespace Znode.Engine.Admin.Secure.Orders.OrderManagement.OrderDesk
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Orders.Sales.OrderDesk - Payment user control class
    /// </summary>
    public partial class OrderPayment : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodePayment _Payment;
        private bool _IsShippingOptionValid = false;
        private int CurrentPortalID = 0;
        private ZNodeShoppingCart shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        #endregion

        #region Private Events
        private System.EventHandler _ShippingSelectedIndexChanged;

        /// <summary>
        /// Gets or sets the shipping item selected event.
        /// </summary>
        public System.EventHandler ShippingSelectedIndexChanged
        {
            get { return this._ShippingSelectedIndexChanged; }
            set { this._ShippingSelectedIndexChanged = value; }
        }

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Account Object
        /// </summary>
        public ZNodeUserAccount UserAccount
        {
            get
            {
                if (ViewState["AccountObject"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the session and
                    // It is converted to a Entity Account object
                    return (ZNodeUserAccount)ViewState["AccountObject"];
                }

                return new ZNodeUserAccount();
            }

            set
            {
                // Customer account object is placed in the session and assigned a key, AccountObject.            
                ViewState["AccountObject"] = value;
            }
        }

        /// <summary>
        /// Sets a value indicating whether hides or shows the Payment section on the final checkout page
        /// </summary>
        public bool ShowPaymentSection
        {
            set
            {
                pnlPayment.Visible = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether returns false, if there is no payment options exists for this profile    
        /// </summary>
        public bool IsPaymentOptionExists
        {
            get
            {
                if (lstPaymentType.Items.Count == 0)
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the shipping option valid property
        /// </summary>
        public bool IsShippingOptionValid
        {
            get
            {
                return this._IsShippingOptionValid;
            }

            set
            {
                this._IsShippingOptionValid = value;
            }
        }

        /// <summary>
        /// Gets or sets the ZnodePayment  object
        /// </summary>
        public ZNodePayment Payment
        {
            get
            {
                // Check payment type            
                if (this.PaymentType == EnumPaymentType.CREDIT_CARD)
                {
                    ZNodePayment _creditCardPayment = new ZNodePayment();
                    _creditCardPayment.CreditCard.CardNumber = txtCreditCardNumber.Text.Trim();
                    _creditCardPayment.CreditCard.CreditCardExp = lstMonth.SelectedValue + "/" + lstYear.SelectedValue;
                    _creditCardPayment.CreditCard.CardSecurityCode = txtCVV.Text.Trim();
                    this._Payment = _creditCardPayment;
                }
                else if (this.PaymentType == EnumPaymentType.PURCHASE_ORDER)
                {
                    ZNodePayment _purchaseOrderPayment = new ZNodePayment();
                    this._Payment = _purchaseOrderPayment;
                }
                else if (this.PaymentType == EnumPaymentType.COD)
                {
                    ZNodePayment _chargeOnDeliveryPayment = new ZNodePayment();
                    this._Payment = _chargeOnDeliveryPayment;
                }

                //Znode Version 7.2.2
                //Added Condition for check PaymentType is PayPal - Start
                else if (this.PaymentType == EnumPaymentType.PAYPAL)
                {
                    ZNodePayment _chargeOnDeliveryPayment = new ZNodePayment();
                    this._Payment = _chargeOnDeliveryPayment;
                }
                //Added Condition for check PaymentType is PayPal - End

                if (lstPaymentType.Items.Count > 0)
                {
                    // Set payment display name
                    this._Payment.PaymentName = lstPaymentType.SelectedItem.Text;
                }

                return this._Payment;
            }

            set
            {
                this._Payment = value;
            }
        }

        /// <summary>
        /// Gets the Payment type selected
        /// </summary>
        public EnumPaymentType PaymentType
        {
            get
            {
                int paymentTypeID = 0;

                if (int.TryParse(lstPaymentType.SelectedValue, out paymentTypeID))
                {
                    PaymentSettingService _pmtServ = new PaymentSettingService();
                    PaymentSetting pmtSetting = _pmtServ.DeepLoadByPaymentSettingID(int.Parse(lstPaymentType.SelectedValue), true, DeepLoadType.IncludeChildren, typeof(ZNode.Libraries.DataAccess.Entities.PaymentType));
                    int _paymentTypeID = pmtSetting.PaymentTypeID;

                    if (_paymentTypeID == (int)EnumPaymentType.CREDIT_CARD)
                    {
                        return EnumPaymentType.CREDIT_CARD;
                    }
                    else if (_paymentTypeID == (int)EnumPaymentType.PURCHASE_ORDER)
                    {
                        return EnumPaymentType.PURCHASE_ORDER;
                    }
                    else if (_paymentTypeID == (int)EnumPaymentType.COD)
                    {
                        return EnumPaymentType.COD;
                    }

                    //Znode Version 7.2.2 
                    //Added Condition for check PaymentType is PayPal- Start
                    else if (_paymentTypeID == (int)EnumPaymentType.PAYPAL)
                    {
                        return EnumPaymentType.PAYPAL;
                    }
                    //Added Condition for check PaymentType is PayPal - End
                }

                return EnumPaymentType.CREDIT_CARD;
            }
        }

        /// <summary>
        /// Gets or sets the payment settingId associated with the payment type selected
        /// </summary>
        public int PaymentSettingID
        {
            get
            {
                return int.Parse(lstPaymentType.SelectedValue);
            }

            set
            {
                lstPaymentType.SelectedValue = value.ToString();
            }
        }

        /// <summary>
        /// Gets the PO number if purchase Order payment Method selected
        /// </summary>
        public string PurchaseOrderNumber
        {
            get
            {
                return txtPONumber.Text.Trim();
            }
        }

        /// <summary>
        /// Gets the additional instruction
        /// </summary>
        public string AdditionalInstructions
        {
            get
            {
                return Server.HtmlEncode(txtAdditionalInstructions.Text.Trim());
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Binds the payment types
        /// </summary>
        public void BindPaymentTypeData()
        {
            if (lstPaymentType.Items.Count == 0)
            {
                PaymentSettingService _pmtServ = new PaymentSettingService();
                ZNode.Libraries.DataAccess.Entities.TList<PaymentSetting> _pmtSetting = _pmtServ.GetAll();
                int profileID = 0;

                profileID = ZNodeProfile.CurrentUserProfileId;

                //Znode Version 7.2.2
                //To show PayPal Express Checkout option in Payment Type dropdown  - Start
                //remove PaymentType !=3 condition 
                _pmtSetting.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt) { return (pmt.ProfileID == null || pmt.ProfileID == profileID) && (pmt.PaymentTypeID != 4 && pmt.PaymentTypeID != 6) && pmt.ActiveInd == true; });
                //To show PayPal Express Checkout option in Payment Type dropdown  - End

                _pmtSetting = _pmtSetting.FindAllDistinct(PaymentSettingColumn.PaymentSettingID);

                _pmtServ.DeepLoad(_pmtSetting, true, DeepLoadType.IncludeChildren, typeof(ZNode.Libraries.DataAccess.Entities.PaymentType));
                _pmtSetting.Sort("DisplayOrder");

                foreach (PaymentSetting _pmt in _pmtSetting)
                {
                    ListItem li = new ListItem();
                    li.Text = _pmt.PaymentTypeIDSource.Name;
                    li.Value = _pmt.PaymentSettingID.ToString();

                    if (_pmt.PaymentTypeID != (int)EnumPaymentType.GOOGLE_CHECKOUT || _pmt.PaymentTypeID != (int)EnumPaymentType.PAYPAL)
                    {
                        lstPaymentType.Items.Add(li);
                    }

                    if (_pmt.PaymentTypeID == (int)EnumPaymentType.CREDIT_CARD)
                    {
                        imgAmex.Visible = (bool)_pmt.EnableAmex;
                        imgMastercard.Visible = (bool)_pmt.EnableMasterCard;
                        imgVisa.Visible = (bool)_pmt.EnableVisa;
                        imgDiscover.Visible = (bool)_pmt.EnableDiscover;
                    }
                }

                // Select first item
                if (lstPaymentType.Items.Count > 0)
                {
                    lstPaymentType.Items[0].Selected = true;
                }

                // Show appropriate payment control
                this.SetPaymentControl();
            }
        }

        /// <summary>
        /// Shows the appropriate payment control based on the option selected
        /// </summary>
        public void SetPaymentControl()
        {
            int paymentTypeID = 0;

            // Show credit card panel
            pnlCreditCard.Visible = false;

            if (int.TryParse(lstPaymentType.SelectedValue, out paymentTypeID))
            {
                PaymentSettingService _pmtServ = new PaymentSettingService();
                ZNode.Libraries.DataAccess.Entities.PaymentSetting pmtSetting = _pmtServ.DeepLoadByPaymentSettingID(int.Parse(lstPaymentType.SelectedValue), true, DeepLoadType.IncludeChildren, typeof(ZNode.Libraries.DataAccess.Entities.PaymentType));

                int paymentTypeId = pmtSetting.PaymentTypeID;

                if (paymentTypeId == (int)EnumPaymentType.CREDIT_CARD)
                {
                    // Hide purchase order panel
                    pnlPurchaseOrder.Visible = false;

                    // show credit card panel
                    pnlCreditCard.Visible = true;
                }
                else if (paymentTypeId == (int)EnumPaymentType.PURCHASE_ORDER)
                {
                    // hide credit card panel
                    pnlCreditCard.Visible = false;

                    // show purchase order panel
                    pnlPurchaseOrder.Visible = true;
                }
                else if (paymentTypeId == (int)EnumPaymentType.COD)
                {
                    // Hide credit card panel
                    pnlCreditCard.Visible = false;

                    // hide purchase order panel
                    pnlPurchaseOrder.Visible = false;
                }
            }
        }

        /// <summary>
        /// Clears the collection of items in the list control
        /// </summary>
        public void ClearUI()
        {
            lstPaymentType.Items.Clear();
        }
        #endregion

        #region Page Load Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set session value for current store portal        
            this.CurrentPortalID = ZNodeConfigManager.SiteConfig.PortalID;

            if (lstPaymentType.Items.Count == 0)
            {
                this.BindPaymentTypeData();
            }

            if (lstYear.Items.Count < 1)
            {
                this.BindYearList();
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Payment type selected index changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetPaymentControl();
        }

        /// <summary>
        /// Apply Gift Cart Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnApplyGiftCard_Click(object sender, EventArgs e)
        {
            var znodePortalCart = this.shoppingCart.PortalCarts.FirstOrDefault();
            if (znodePortalCart == null) return;
            znodePortalCart.AddGiftCard(txtGiftCardNumber.Text);

            txtGiftCardNumber.Text = znodePortalCart.GiftCardNumber;
            lblGiftCardMessage.Text = znodePortalCart.GiftCardMessage;

        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Binds the expiration year list based on current year
        /// </summary>
        private void BindYearList()
        {
            ListItem defaultItem = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextYear").ToString(), string.Empty);
            lstYear.Items.Add(defaultItem);
            defaultItem.Selected = true;

            int currentYear = System.DateTime.Now.Year;
            int counter = 25;

            do
            {
                string itemtext = currentYear.ToString();

                lstYear.Items.Add(new ListItem(itemtext));

                currentYear = currentYear + 1;

                counter = counter - 1;
            }
            while (counter > 0);
        }
        #endregion

    }
}