<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" EnableEventValidation="false" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.OrderManagement.OrderDesk.Default"
    ValidateRequest="false" CodeBehind="Default.aspx.cs" %>
<%@ Register Src="OrderPayment.ascx" TagName="Payment" TagPrefix="ZNode" %>
<%@ Register Src="QuickOrder.ascx" TagName="QuickOrder" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/ShoppingCart.ascx" TagName="ShoppingCart"
    TagPrefix="ZNode" %>
<%@ Register Src="CreateUser.ascx" TagName="CreateUser" TagPrefix="ZNode" %>
<%@ Register Src="CustomerSearch.ascx" TagName="CustomerSearch" TagPrefix="ZNode" %>
<%@ Register Src="CartItems.ascx" TagName="CartItems" TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <script type="text/JavaScript" language="JavaScript">
        function pageLoad() {
            var manager = Sys.WebForms.PageRequestManager.getInstance();
            manager.add_endRequest(endRequest);
            manager.add_beginRequest(OnBeginRequest);
        }
        function OnBeginRequest(sender, args) {

            document.getElementById("OrderDesk").disabled = true;
        }
        function endRequest(sender, args) {

            document.getElementById("OrderDesk").disabled = false;
        }
    </script>

    <div class="OrderDesk" id="OrderDesk">
        <asp:UpdatePanel ID="UpdatePanelOrderDesk" runat="server" UpdateMode="Conditional"
            ChildrenAsTriggers="true" RenderMode="Inline">
            <ContentTemplate>
                <div class="OrderDeskContent">
                    <div class="LeftFloat" style="width: 60%">
                        <h1><asp:Localize ID="LinkTextCreateOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextCreateOrder%>'></asp:Localize></h1>
                    </div>
                    <div class="ClearBoth">
                    </div>
                    <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
                    <!-- Ajax Modal Popup Box  -->
                    <asp:Panel ID="pnlOrderDesk" runat="server">
                        <zn:Button ID="btnShowPopup" runat="server" Style="display: none" />
                        <zn:Button ID="btnShowPopup1" runat="server" Style="display: none" />
                        <zn:Button ID="btnShowQuickOrder" runat="server" Style="display: none" />
                        <zn:Button ID="btnShowMultipleAddress" runat="server" Style="display: none" />
                        <ajaxToolKit:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="btnShowPopup"
                            PopupControlID="pnlSelectUsers" BackgroundCssClass="modalBackground" />
                        <ajaxToolKit:ModalPopupExtender ID="mdlCreateUserPopup" runat="server" TargetControlID="btnShowPopup1"
                            PopupControlID="pnlCreateUser" BackgroundCssClass="modalBackground" />
                        <ajaxToolKit:ModalPopupExtender ID="mdlQuickOrderPopup" runat="server" TargetControlID="btnShowQuickOrder"
                            PopupControlID="pnlQuickOrder" BackgroundCssClass="modalBackground" />
                        <div>
                            <div style="float: left;">
                                <!-- Store Section -->
                                <asp:Panel ID="pnlSelectStore" runat="server">
                                    <div class="ValueStyle">
                                        <asp:DropDownList ID="ddlPortal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlPortal_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </asp:Panel>
                            </div>

                            <div style="float: left; margin-left: 50px;">
                                <!-- Catalog Section -->
                                <asp:Panel ID="pnlSelectCatalog" runat="server">
                                    <div class="ValueStyle">
                                        <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlCatalog_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </asp:Panel>
                                <!-- Customer Section -->
                            </div>
                        </div>
                        <div class="ClearBoth"></div>

                        <asp:Panel ID="pnltitle" runat="server" Visible="false">
                            <h4 class="SubTitle" style="padding-bottom: 0px;"><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCustomer%>'></asp:Localize></h4>
                        </asp:Panel>

                        <asp:Panel ID="ui_pnlAcctInfo" runat="server" Visible="false">

                            <div class="LeftFloat" style="padding-bottom: 20px;">
                                <div>
                                    <strong><asp:Localize ID="ColumnProfileName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProfileName%>'></asp:Localize></strong>&nbsp;&nbsp;&nbsp;<asp:Label ID="ui_lblProfile" runat="server"
                                        Text=''></asp:Label>
                                </div>
                                <div>
                                    <strong><asp:Localize ID="ColumnUserID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnUserID%>'></asp:Localize></strong>&nbsp;<asp:Label ID="ui_lblUserID" runat="server" Text=''></asp:Label>
                                </div>
                            </div>
                            <div class="ClearBoth"></div>

                            <div class="LeftFloat" style="width: 400px; word-wrap: break-word;">
                                <strong><asp:Localize ID="ColumnShipping" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShipping%>'></asp:Localize></strong>
                                <asp:UpdatePanel ID="UpdatePnlMultipleAddress" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="ChangeAddress">
                                            <asp:LinkButton CausesValidation="false" ID="linkChangeShipAddress" runat="server" Text="Change"
                                                OnClick="linkChangeShipAddress_Click" />
                                        </div>
                                        <div class="MultipleAddress">
                                            <asp:LinkButton CausesValidation="false" ID="linkShipMultipleAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextShipToMultipleAddress%>'
                                                OnClick="linkShipMultipleAddress_Click" Visible="false" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <ajaxToolKit:ModalPopupExtender ID="mdlCartItemsPopup" runat="server" TargetControlID="btnShowMultipleAddress"
                                    PopupControlID="pnlMultipleAddress" BackgroundCssClass="modalBackground" />

                                <asp:Panel ID="pnlMultipleAddress" runat="server" Style="display: none;" CssClass="PopupStyle ProductPopupStyle" >
                                    <div >
                                        <ZNode:CartItems ID="uxCartItems" runat="server" />
                                    </div>
                                </asp:Panel>
                                <br />
                                <br />
                                <asp:Label ID="ui_ShippingAddress" runat="server" Text=''></asp:Label>
                                <div class="ViewMultipleAddress">
                                    <asp:LinkButton CausesValidation="false" ID="lnkView" runat="server" Text="View"
                                        OnClick="linkShipMultipleAddress_Click" Visible="false" />
                                </div>
                            </div>
                            <div class="LeftFloat" style="width: 300px; word-wrap: break-word;">
                                <strong><asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBilling%>'></asp:Localize></strong>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="BillingAddress">
                                            <asp:LinkButton CausesValidation="false" ID="linkChangeBillingAddress" runat="server" Text="Change"
                                                OnClick="Ui_Edit_Click" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <br />
                                <asp:Label ID="ui_BillingAddress" runat="server" Text=''></asp:Label>
                            </div>
                        </asp:Panel>
                        <div class="ClearBoth"></div>
                        <div>
                            <uc1:Spacer ID="Spacer1" SpacerWidth="1" SpacerHeight="10" runat="server" />
                        </div>
                        <div class="SearchCustomer">
                            <asp:Panel ID="pnlSelectCustomer" runat="server">

                                <asp:UpdatePanel ID="UpdatePnlSelecCustomer" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="LinkButton">
                                            <asp:LinkButton CausesValidation="false" ID="linkSearchCustomer" runat="server" Text="Search Existing Customers"
                                                OnClick="LinkSearchCustomer_Click" />
                                        </div>
                                        <div class="LinkButton">
                                            <asp:LinkButton CausesValidation="false" ID="linkNewCustomer" runat="server" Text="Add New Customer"
                                                OnClick="LinkNewCustomer_Click" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </div>
                        <asp:Panel ID="pnlSelectUsers" runat="server" Style="display: none;" CssClass="PopupStyle">
                            <div>
                                <ZNode:CustomerSearch ID="uxCustomerSearch" runat="server" />
                            </div>
                        </asp:Panel>

                        <!-- Customer Section -->
                        <!-- Create Customer Popup -->
                        <asp:Panel ID="pnlCreateUser" Style="display: none;" runat="server" CssClass="PopupStyle CustomerAddressPopStyle">
                            <div  >
                                <ZNode:CreateUser ID="uxCreateUser" runat="server" />
                            </div>
                        </asp:Panel>
                        <!-- Quick Order Section-->
                        <asp:Panel ID="pnlQuickOrder" runat="server" CssClass="PopupStyle" Style="display: none;">
                            <asp:UpdatePanel ID="UpdatePanelQuickOrder" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="QuickOrder">
                                        <ZNode:QuickOrder ID="uxQuickOrder" runat="server" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        <!-- Shopping Cart section -->
                        <asp:Panel ID="pnlCart" runat="server" CssClass="ShoppingCart">
                            <h4 class="SubTitle"><asp:Localize ID="SubTitleShoppingCart" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleShoppingCart%>'></asp:Localize></h4>
                            <div>
                                <asp:LinkButton ID="linkbtnQuickOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchProducts%>' OnClick="LinkbtnQuickOrder_Click"></asp:LinkButton>
                            </div>
                            <div>
                                <uc1:Spacer ID="Spacer" SpacerWidth="1" SpacerHeight="10" runat="server" />
                            </div>
                            <div>
                                <ZNode:ShoppingCart ID="uxShoppingCart" ShowTaxShipping="true" runat="server" />
                            </div>
                        </asp:Panel>
                        <!-- Payment and Shipping Section -->
                        <asp:Panel ID="pnlPaymentShipping" runat="server" CssClass="Payment" Visible="false">
                            <div>
                                <ZNode:Payment ID="uxPayment" runat="server" />
                            </div>
                        </asp:Panel>
                        <div>
                            <asp:Label ID="lblError" runat="server" EnableViewState="false" CssClass="Error"></asp:Label>
                        </div>
                        <div>
                            <uc1:Spacer ID="Spacer8" SpacerWidth="1" SpacerHeight="15" runat="server" />
                        </div>
                        <div align="right">
                            <zn:Button runat="server" ButtonType="EditButton" Width="100px" OnClick="Submit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmitOrder%>' ID="btnSubmit" ValidationGroup="groupPayment" />
                            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancelOrder_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelOrder" />
                            <div>
                                <uc1:Spacer ID="Spacer9" SpacerWidth="1" SpacerHeight="10" runat="server" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdateProgress ID="uxSearchUpdateProgress" runat="server" AssociatedUpdatePanelID="UpdatePanelOrderDesk"
        DisplayAfter="1">
        <ProgressTemplate>
            <div id="ajaxProgressBg">
            </div>
            <div id="ajaxProgress">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
