<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.OrderManagement.OrderDesk.OrderPayment"
	CodeBehind="OrderPayment.ascx.cs" %>
<%@ Register Src="~/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<%@ Register TagPrefix="ZNode" Assembly="ZNode.Libraries.ECommerce.Catalog" Namespace="ZNode.Libraries.ECommerce.Catalog" %>
<div>
	<asp:Panel ID="pnlPayment" runat="server" Visible="true">
		<div>
			<h4 class="SubTitle"><asp:Localize ID="SubTitlePayment" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitlePayment%>'></asp:Localize></h4>
		</div>
		<div class="FormView Size100">
			<div class="FieldStyle">
				<asp:Localize ID="ColumnPaymentOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPaymentOption%>'></asp:Localize>
			</div>
			<div class="ValueStyle">
				<asp:DropDownList ID="lstPaymentType" runat="server" OnSelectedIndexChanged="LstPaymentType_SelectedIndexChanged"
					AutoPostBack="true">
				</asp:DropDownList>
			</div>
		</div>
		<!-- COD payment section -->
		<asp:Panel ID="pnlCOD" runat="server" Visible="false">
			<asp:Localize ID="ValidCODPayment" runat="server" Text='<%$ Resources:ZnodeAdminResource, ValidCODPayment%>'></asp:Localize>
		</asp:Panel>
		<!-- purchase order payment section -->
		<asp:Panel ID="pnlPurchaseOrder" runat="server" Visible="false">
			<div class="FormView Size100">
				<div class="FieldStyle">
					<asp:Localize ID="ColumnPurchaseOrderNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPurchaseOrderNumber%>'></asp:Localize>
				</div>
				<div class="ValueStyle">
					<asp:TextBox ID="txtPONumber" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
					<div>
						<asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPurchaseOrderNumber%>'
							ControlToValidate="txtPONumber" runat="server" Display="Dynamic" CssClass="Error"
							ValidationGroup="groupPayment"></asp:RequiredFieldValidator>
					</div>
				</div>
			</div>
		</asp:Panel>
		<div class="ClearBoth">
		</div>
		<!-- Credit card payment section-->
		<asp:Panel ID="pnlCreditCard" runat="server">
			<div class="FormView Size100">
				<div class="FieldStyle">
					<asp:Localize ID="ColumnCardNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCardNumber%>'></asp:Localize>
				</div>
				<div class="ValueStyle">
					<asp:TextBox ID="txtCreditCardNumber" runat="server" Width="130" Columns="30" MaxLength="20"
						autoComplete="off"></asp:TextBox>&nbsp;&nbsp;<img id="imgVisa" src="~/Themes/images/card_visa.gif"
							runat="server" align="absmiddle" /><img id="imgMastercard" src="~/Themes/images/card_mastercard.gif"
								runat="server" align="absmiddle" /><img id="imgAmex" src="~/Themes/images/card_amex.gif"
									align="absmiddle" runat="server" /><img id="imgDiscover" src="~/Themes/images/card_discover.gif"
										align="absmiddle" runat="server" /><br />
					<div>
						<asp:RequiredFieldValidator ID="req1" ErrorMessage='<%$ Resources:ZnodeAdminResource, ColumnTitleEnterCreditCard%>' ControlToValidate="txtCreditCardNumber"
							runat="server" Display="Dynamic" CssClass="Error" ValidationGroup="groupPayment"></asp:RequiredFieldValidator>
						<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCreditCardNumber"
							Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularCreditCardNumber%>' ValidationExpression="^[3|4|5|6]([0-9]{15}$|[0-9]{12}$|[0-9]{13}$|[0-9]{14}$)"
							ValidationGroup="groupPayment"></asp:RegularExpressionValidator>
					</div>
					<br />
					<ZNode:ZNodeCreditCardValidator ID="MyValidator" ControlToValidate="txtCreditCardNumber"
						ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidCreditCardNumber%>' Display="Dynamic" runat="server"
						ValidateCardType="True" ValidationGroup="groupPayment" />
				</div>
			</div>
			<div class="ClearForm">
			</div>
			<div class="FormView Size100">
				<div class="FieldStyle">
					<asp:Localize ID="ColumnTitleExpirationDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleExpirationDate%>'></asp:Localize>
				</div>
				<div class="ValueStyle">
					<asp:DropDownList ID="lstMonth" runat="server">
						<asp:ListItem Value=""   Text='<%$ Resources:ZnodeAdminResource, DropdownTextMonth%>'></asp:ListItem>
						<asp:ListItem Value="01" Text='<%$ Resources:ZnodeAdminResource, DropDownTextJan%>'></asp:ListItem>
						<asp:ListItem Value="02" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFeb%>'></asp:ListItem>
						<asp:ListItem Value="03" Text='<%$ Resources:ZnodeAdminResource, DropDownTextMar%>'></asp:ListItem>
						<asp:ListItem Value="04" Text='<%$ Resources:ZnodeAdminResource, DropDownTextApr%>'></asp:ListItem>
						<asp:ListItem Value="05" Text='<%$ Resources:ZnodeAdminResource, DropDownTextMay%>'></asp:ListItem>
						<asp:ListItem Value="06" Text='<%$ Resources:ZnodeAdminResource, DropDownTextJun%>'></asp:ListItem>
						<asp:ListItem Value="07" Text='<%$ Resources:ZnodeAdminResource, DropDownTextJul%>'></asp:ListItem>
						<asp:ListItem Value="08" Text='<%$ Resources:ZnodeAdminResource, DropDownTextAug%>'></asp:ListItem>
						<asp:ListItem Value="09" Text='<%$ Resources:ZnodeAdminResource, DropDownTextSep%>'></asp:ListItem>
						<asp:ListItem Value="10" Text='<%$ Resources:ZnodeAdminResource, DropDownTextOct%>'></asp:ListItem>
						<asp:ListItem Value="11" Text='<%$ Resources:ZnodeAdminResource, DropDownTextNov%>'></asp:ListItem>
						<asp:ListItem Value="12" Text='<%$ Resources:ZnodeAdminResource, DropDownTextDec%>'></asp:ListItem>
					</asp:DropDownList>
					&nbsp;&nbsp;
                    <asp:DropDownList ID="lstYear" runat="server">
					</asp:DropDownList>
				</div>
			</div>
			<div class="FormView Size100">
				<div class="FieldStyle">
					<asp:Localize ID="ColumnPostalCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSecurityCode%>'></asp:Localize>
				</div>
				<div class="ValueStyle">
					<asp:TextBox ID="txtCVV" runat="server" Width="30" autoComplete="off" Columns="30"
						MaxLength="4"></asp:TextBox>&nbsp;&nbsp;<a href="javascript:popupWin=window.open('../../../../../controls/default/cvv/cvv.htm','EIN','scrollbars,resizable,width=515,height=300,left=50,top=50');popupWin.focus();"
							runat="server"><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintHelp%>'></asp:Localize></a><div>
								<asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSecurityCode%>' ControlToValidate="txtCVV" runat="server" Display="Dynamic" CssClass="Error"
									ValidationGroup="groupPayment"></asp:RequiredFieldValidator>
							</div>
				</div>
			</div>
		</asp:Panel>
		<div class="ClearForm">
		</div> 
	</asp:Panel>
	
	<asp:Panel ID="Panel1" runat="server">
			<div class="FormView Size100">
				<!-- Gift Card Section -->
				<asp:Panel ID="pnlGiftCardNumber" runat="server">
					<div>
						<div class="FieldStyle LeftContent">
							<asp:Localize ID="ColumnGiftCardNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnGiftCardNumber%>'></asp:Localize>
						</div>
						<div class="ValueStyle">
							<asp:TextBox ID="txtGiftCardNumber" runat="server" EnableViewState="true" Width="130"
								Columns="30" MaxLength="100" ValidationGroup="GiftCard"></asp:TextBox>&nbsp;
                        <asp:ImageButton ID="btnApplyGiftCard" ImageAlign="Top" ImageUrl="~/Themes/images/Apply.gif"
							runat="server" AlternateText="Apply" CssClass="gobutton" ValidationGroup="GiftCard"
							OnClick="BtnApplyGiftCard_Click"></asp:ImageButton>
						</div>
					</div>
					<div class="ClearBoth">
					</div>
					<div>
						<asp:Label ID="lblGiftCardMessage" runat="server" CssClass="Error" Text=""></asp:Label>
					</div>
				</asp:Panel>
			</div>
		</asp:Panel>

		<div class="ClearBoth">
			<uc1:spacer ID="Spacer9" SpacerHeight="15" SpacerWidth="5" runat="server" />
			<div class="HintStyle">
				<asp:Localize ID="SubTextOrderComments" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextOrderComments%>'></asp:Localize>
			</div>
			<div>
				<uc1:spacer ID="Spacer15" SpacerHeight="15" SpacerWidth="5" runat="server" />
				<asp:TextBox Columns="45" Rows="3" ID="txtAdditionalInstructions" runat="server"
					TextMode="MultiLine"></asp:TextBox>
			</div>
		</div>
</div>
