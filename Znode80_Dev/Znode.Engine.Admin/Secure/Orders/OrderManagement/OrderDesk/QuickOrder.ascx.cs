using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Secure.Orders.OrderManagement.OrderDesk
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Orders.Sales.OrderDesk - QuickOrder class
    /// </summary>
    public partial class QuickOrder : System.Web.UI.UserControl
    {
        #region Private Variables
        private bool _ShowInventoryLevels = false;
        private ZNodeProduct _Product;
        private ZNodeShoppingCart shoppingCart = null;
        private int CurrentPortalID = 0;
        #endregion

        #region Private Events
        private System.EventHandler _SubmitButtonClicked;
        private System.EventHandler _CancelButtonClicked;
        private System.EventHandler _AddProductButtonClicked;

        /// <summary>
        /// Gets or sets the Account Object
        /// </summary>
        public string ExternalAccountNo
        {
            get
            {
                if (HttpContext.Current.Session["AliasUserAccount"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the session and
                    // It is converted to a Entity Account object
                    var userAcct = (ZNodeUserAccount)HttpContext.Current.Session["AliasUserAccount"];
                    if (userAcct != null)
                        return userAcct.ExternalAccountNo;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the submit button click event
        /// </summary>
        public System.EventHandler SubmitButtonClicked
        {
            get { return this._SubmitButtonClicked; }
            set { this._SubmitButtonClicked = value; }
        }

        /// <summary>
        /// Gets or sets the Add product button click event
        /// </summary>        
        public System.EventHandler AddProductButtonClicked
        {
            get { return this._AddProductButtonClicked; }
            set { this._AddProductButtonClicked = value; }
        }

        /// <summary>
        /// Gets or sets the cancel button click event
        /// </summary>
        public System.EventHandler CancelButtonClicked
        {
            get { return this._CancelButtonClicked; }
            set { this._CancelButtonClicked = value; }
        }

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the catalog item object. The control will bind to the data from this object
        /// </summary>
        public ZNodeProduct Product
        {
            get
            {
                if (Session["CatalogItem"] != null)
                {
                    return Session["CatalogItem"] as ZNodeProduct;
                }

                return new ZNodeProduct();
            }

            set
            {
                Session["CatalogItem"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the catalog item object. The control will bind to the data from this object
        /// </summary>
        public int Quantity
        {
            get
            {
                if (ViewState["ShoppingCartQuantity"] != null)
                {
                    return (int)ViewState["ShoppingCartQuantity"];
                }

                return 1;
            }

            set
            {
                ViewState["ShoppingCartQuantity"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the catalog Id. 
        /// </summary>
        public int CatalogId
        {
            get
            {
                if (ViewState["CatalogId"] != null)
                {
                    return (int)ViewState["CatalogId"];
                }

                return 0;
            }

            set
            {
                ViewState["CatalogId"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether ShowInventoryLevels is true or false
        /// </summary>
        public bool ShowInventoryLevels
        {
            get
            {
                return this._ShowInventoryLevels;
            }

            set
            {
                this._ShowInventoryLevels = value;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Reset the field values
        /// </summary>
        public void ResetUI()
        {
            tablerow.Visible = false;

            pnlProductDetails.Visible = false;
            pnlProductList.Visible = false;
            txtProductName.Text = string.Empty;
            txtProductNum.Text = string.Empty;
            txtProductSku.Text = string.Empty;
            txtBrand.Text = string.Empty;
            txtCategory.Text = string.Empty;

            CatalogItemImage.ImageUrl = string.Empty;
            ProductDescription.Text = string.Empty;
            lblUnitPrice.Text = 0.ToString("c");
            lblTotalPrice.Text = lblUnitPrice.Text;
            Qty.SelectedIndex = Qty.Items.IndexOf(Qty.Items.FindByValue("1"));
            ControlPlaceHolder.Controls.Clear();

            CatalogItemImage.Visible = this.ShowCatalogImage(this._Product.MediumImageFilePath);
        }

        public string GetPrice(object retailPrice, object salePrice, object negotiatedPrice)
        {
            if (!string.IsNullOrEmpty(salePrice.ToString()))
                return string.Format("{0:c}", Convert.ToDecimal(salePrice));

            if (!string.IsNullOrEmpty(negotiatedPrice.ToString()))
                return string.Format("{0:c}", Convert.ToDecimal(negotiatedPrice));


            return retailPrice.ToString();
        }

        /// <summary>
        /// Bind edit data
        /// </summary>
        public void Bind()
        {
            this._Product = this.Product;

            if (this._Product.ProductID > 0)
            {
                this.BindQuantityList();

                this.DisplayPrice();

                tablerow.Visible = true;
            }
            else
            {
                this.ResetUI();
            }
        }
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set session value for current store portal       
            this.CurrentPortalID = ZNodeConfigManager.SiteConfig.PortalID;

            if (Page.IsPostBack)
            {
                this.BindAddOnsAttributes();
            }
        }

        #endregion

        #region Protected Methods and Events

        /// <summary>
        /// Show product price
        /// </summary>
        protected void DisplayPrice()
        {
            decimal unitPrice = 0;
            decimal extendedPrice = 0;
            int shoppingCartQuantity = int.Parse(Qty.Text);

            if (this._Product.ProductID > 0)
            {
                unitPrice = this._Product.FinalPrice + this._Product.AddOnPrice;
            }

            if (this._Product.ZNodeBundleProductCollection.Count > 0)
            {
                for (int idx = 0; idx < this._Product.ZNodeBundleProductCollection.Count; idx++)
                {
                    unitPrice += this._Product.ZNodeBundleProductCollection[idx].AddOnPrice;
                }
            }

            extendedPrice = unitPrice * shoppingCartQuantity;

            lblUnitPrice.Text = unitPrice.ToString("c");
            lblTotalPrice.Text = extendedPrice.ToString("c");
        }

        /// <summary>
        /// Validate selected addons and returns the Inventory related messages
        /// </summary>
        /// <param name="addOn">The ZNodeAddon object</param>
        /// <param name="addOnValue">The ZNodeAddonValue object</param>
        /// <returns>Returs the Status Message</returns>
        protected string BindStatusMsg(ZNodeAddOn addOn, ZNodeAddOnValue addOnValue)
        {
            // If quantity available is less and track inventory is enabled
            AddOnValueService avs = new AddOnValueService();
            addOnValue.QuantityOnHand = ProductAdmin.GetQuantity(avs.GetByAddOnValueID(addOnValue.AddOnValueID));
            if (addOnValue.QuantityOnHand <= 0 && addOn.AllowBackOrder == false && addOn.TrackInventoryInd)
            {
                return addOn.OutOfStockMsg;
            }
            else if (addOnValue.QuantityOnHand <= 0 && addOn.AllowBackOrder == true && addOn.TrackInventoryInd)
            {
                return addOn.BackOrderMsg;
            }
            else if (addOn.TrackInventoryInd && addOnValue.QuantityOnHand > 0)
            {
                return addOn.InStockMsg;
            }
            else if (addOn.AllowBackOrder == false && addOn.TrackInventoryInd == false)
            {
                // Don't track Inventory
                // Items can always be added to the cart,TrackInventory is disabled
                return addOn.InStockMsg;
            }

            return string.Empty;
        }

        /// <summary>
        /// SubmitAnother Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmitAnother_Click(object sender, EventArgs e)
        {
            if (this.AddToCart() && this.AddProductButtonClicked != null)
            {
                this.AddProductButtonClicked(sender, e);
            }
        }

        /// <summary>
        /// Search Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Search_Click(object sender, EventArgs e)
        {
            pnlProductDetails.Visible = false;
            pnlProductList.Visible = false;

            NextPageLink.Enabled = false;
            PreviousPageLink.Enabled = false;
            FirstPageLink.Enabled = false;
            LastPageLink.Enabled = false;

            uxGrid.DataSource = null;
            uxGrid.DataBind();

            TotalPages.Value = "0";
            PageIndex.Value = "1";

            this.BindProducts();
        }

        /// <summary>
        /// Close Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClose_Click(object sender, EventArgs e)
        {
            if (this.CancelButtonClicked != null)
            {
                this.CancelButtonClicked(sender, e);
            }
        }

        /// <summary>
        /// Grid Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Retrieve Product Id
            Label lblId = uxGrid.Rows[uxGrid.SelectedIndex].FindControl("ui_CustomerIdSelect") as Label;

            if (lblId.Text != "0")
            {
                int productId = 0;

                int.TryParse(lblId.Text, out productId);

                if (productId > 0)
                {
                    pnlProductDetails.Visible = true;
                    pnlProductList.Visible = false;
                    uxGrid.DataSource = null;
                    uxGrid.DataBind();

                    // Clear resources
                    uxGrid.Dispose();

                    int quantity = 1;
                    int.TryParse(Qty.SelectedValue, out quantity);

                    if (this.Product.ProductID != productId)
                    {
                        this._Product = ZNodeProduct.Create(productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, true, ExternalAccountNo);
                        quantity = 1;
                        this.Product = this._Product;
                    }
                    else
                    {
                        this._Product = this.Product;
                    }

                    this.Quantity = quantity;

                    CatalogItemImage.ImageUrl = this._Product.MediumImageFilePath;
                    CatalogItemImage.Visible = this.ShowCatalogImage(this._Product.MediumImageFilePath);
                    ProductDescription.Text = Server.HtmlDecode(this._Product.Description);
                    this.BindAddOnsAttributes();

                    this.ValidateAttributes(sender, e);

                    // Enable stock validator
                    this.EnableStockValidator(this._Product);

                    this.ValidateAddOns(sender, e);
                }
            }
        }

        /// <summary>
        /// Quantity SelectedIndexChanged  Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Qty_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Quantity = int.Parse(Qty.SelectedValue);

            this.DisplayPrice();
        }
        #endregion

        #region Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (this.AddToCart() && this.SubmitButtonClicked != null)
            {
                this.SubmitButtonClicked(sender, e);
            }
        }

        /// <summary>
        /// Event raised when the "Cancel" button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.CancelButtonClicked != null)
            {
                this.CancelButtonClicked(sender, e);
            }
        }

        /// <summary>
        /// Selector List Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void SelectorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList list = sender as DropDownList;

            if (list.SelectedValue != "0")
            {
                int productId = int.Parse(list.SelectedValue);
                int quantity = 0;

                if (!int.TryParse(Qty.Text.Trim(), out quantity))
                {
                    quantity = 1;
                }

                if (this.Product.ProductID != productId)
                {
                    // Get a product object using create static method
                    this._Product = ZNodeProduct.Create(productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                    this.Product = this._Product;
                }
                else
                {
                    this._Product = this.Product;
                }

                CatalogItemImage.ImageUrl = this._Product.MediumImageFilePath;
                CatalogItemImage.Visible = this.ShowCatalogImage(this._Product.MediumImageFilePath);
                ProductDescription.Text = Server.HtmlDecode(this._Product.Description);

                this.BindAddOnsAttributes();

                if (this._Product.ZNodeAddOnCollection.Count > 0)
                {
                    this.ValidateAddOns(sender, e);
                }
                else
                {
                    this.Bind();
                }

                this.ValidateAttributes(sender, e);

                // Enable stock validator
                this.EnableStockValidator(this._Product);
            }
            else
            {
                this.Product = new ZNodeProduct();
                this.Bind();
            }
        }
        #endregion

        #region Paging Events
        /// <summary>
        ///  Next Record Method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void NextRecord(object sender, EventArgs e)
        {
            if (!PageIndex.Value.Equals(TotalPages.Value))
            {
                PageIndex.Value = (int.Parse(PageIndex.Value) + 1).ToString();

                this.BindProducts();

                FirstPageLink.Enabled = true;
                PreviousPageLink.Enabled = true;
            }

            if (PageIndex.Value.Equals(TotalPages.Value))
            {
                LastPageLink.Enabled = false;
                NextPageLink.Enabled = false;
            }

            lblSearhError.Text = string.Empty;
        }

        /// <summary>
        /// Previous Record Method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PrevRecord(object sender, EventArgs e)
        {
            if (!PageIndex.Value.Equals("1"))
            {
                PageIndex.Value = (int.Parse(PageIndex.Value) - 1).ToString();

                this.BindProducts();

                LastPageLink.Enabled = true;
                NextPageLink.Enabled = true;
            }

            if (PageIndex.Value.Equals("1"))
            {
                FirstPageLink.Enabled = false;
                PreviousPageLink.Enabled = false;
            }

            lblSearhError.Text = string.Empty;
        }

        /// <summary>
        /// Move To Last Page Method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void MoveToLastPage(object sender, EventArgs e)
        {
            if (!PageIndex.Value.Equals(TotalPages.Value))
            {
                int pageLength = int.Parse(TotalPages.Value);

                PageIndex.Value = pageLength.ToString();
                this.BindProducts();

                FirstPageLink.Enabled = true;
                PreviousPageLink.Enabled = true;
            }

            if (PageIndex.Value.Equals(TotalPages.Value))
            {
                LastPageLink.Enabled = false;
                NextPageLink.Enabled = false;
            }

            lblSearhError.Text = string.Empty;
        }

        /// <summary>
        /// Move to First Page Method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void MoveToFirstPage(object sender, EventArgs e)
        {
            if (!PageIndex.Value.Equals("1"))
            {
                PageIndex.Value = "1";
                this.BindProducts();

                NextPageLink.Enabled = true;
                LastPageLink.Enabled = true;
            }

            if (PageIndex.Value.Equals("1"))
            {
                FirstPageLink.Enabled = false;
                PreviousPageLink.Enabled = false;
            }

            lblSearhError.Text = string.Empty;
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Creates attribute drop downlist controls dynamically for a product
        /// </summary>
        protected void BindAddOnsAttributes()
        {
            this._Product = this.Product;

            if (this._Product.ProductID > 0)
            {
                int counter = 0;

                // Remove existing dynamic controls from the control place holder object
                ControlPlaceHolder.Controls.Clear();

                // Find the placeholder control and Add the literal control to the Controls collection
                // of the PlaceHolder control.
                Literal literal = new Literal();
                literal.Text = "<div class='Attributes'>";
                ControlPlaceHolder.Controls.Add(literal);

                // Add the product name
                Literal literalProductName = new Literal();
                literalProductName.Text = "<div style='font-weight:bold;font-size:10px;margin-bottom:5px;'>" + this._Product.Name + "</div>";
                ControlPlaceHolder.Controls.Add(literalProductName);

                // Create list control for each product Attribute
                if (this._Product.ZNodeAttributeTypeCollection.Count > 0)
                {
                    foreach (ZNodeAttributeType AttributeType in this._Product.ZNodeAttributeTypeCollection)
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                        lstControl.ID = "lstAttribute" + AttributeType.AttributeTypeId.ToString();
                        lstControl.Attributes.Add("class", "ValueStyle");
                        lstControl.AutoPostBack = true;
                        lstControl.SelectedIndexChanged += new System.EventHandler(this.ValidateAttributes);

                        foreach (ZNodeAttribute Attribute in AttributeType.ZNodeAttributeCollection)
                        {
                            ListItem li1 = new ListItem(Attribute.Name, Attribute.AttributeId.ToString());
                            lstControl.Items.Add(li1);
                        }

                        string attributeValues = this._Product.SelectedSKU.AttributesValue;
                        string[] attributes = new string[this._Product.ZNodeAttributeTypeCollection.Count];

                        if (attributeValues.Length > 0)
                        {
                            attributes = attributeValues.Split(new char[] { ',' }, StringSplitOptions.None);
                        }

                        if (lstControl.Items.Count > 0)
                        {
                            if (attributes != null)
                            {
                                lstControl.SelectedValue = attributes[counter++];
                            }
                        }

                        // Add the Attribute dropdownlist control to the Controls collection
                        // of the PlaceHolder control.                
                        ControlPlaceHolder.Controls.Add(lstControl);

                        Literal ltrlSpacer = new Literal();
                        ltrlSpacer.Text = "<br />";
                        ControlPlaceHolder.Controls.Add(ltrlSpacer);
                    }
                }
                else
                {
                    ZNodeSKU SKU = ZNodeSKU.CreateByProductDefault(this._Product.ProductID, ExternalAccountNo);
                    this._Product.SelectedSKU = SKU;
                }

                // Create list control for each product AddOns
                string addonValues = this._Product.SelectedAddOnItems.SelectedAddOnValueIds;

                string[] addonValueId = new string[this._Product.ZNodeAddOnCollection.Count];
                if (addonValues.Length > 0)
                {
                    addonValueId = addonValues.Split(new string[] { "," }, StringSplitOptions.None);
                }

                if (this._Product.ZNodeAddOnCollection.Count > 0)
                {
                    foreach (ZNodeAddOn AddOn in this._Product.ZNodeAddOnCollection)
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                        lstControl.ID = "lstAddOn" + AddOn.AddOnID.ToString();
                        lstControl.AutoPostBack = true;
                        lstControl.SelectedIndexChanged += new System.EventHandler(this.ValidateAddOns);
                        lstControl.Attributes.Add("class", "ValueStyle");

                        // Don't display list box if there is no add-on values for AddOns
                        if (AddOn.ZNodeAddOnValueCollection.Count > 0)
                        {
                            foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                            {
                                string AddOnValueName = AddOnValue.Name;
                                decimal decRetailPrice = AddOnValue.FinalPrice;

                                if (decRetailPrice < 0)
                                {
                                    // Price format
                                    string priceformat = "-" + ZNodeCurrencyManager.GetCurrencyPrefix() + "{0:0.00}" + ZNodeCurrencyManager.GetCurrencySuffix();
                                    AddOnValueName += " : " + String.Format(priceformat, System.Math.Abs(decRetailPrice)) + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                                }
                                else if (decRetailPrice > 0)
                                {
                                    AddOnValueName += " : " + decRetailPrice.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                                }

                                // Added Inventory Message with the Addon Value Name in the dropdownlist
                                AddOnValueName += " " + this.BindStatusMsg(AddOn, AddOnValue);

                                ListItem li1 = new ListItem(AddOnValueName, AddOnValue.AddOnValueID.ToString());
                                lstControl.Items.Add(li1);

                                if (AddOnValue.IsDefault)
                                {
                                    lstControl.SelectedValue = AddOnValue.AddOnValueID.ToString();
                                }
                            }

                            // Check for Optional 
                            if (AddOn.OptionalInd)
                            {
                                ListItem OptionalItem = new ListItem("Optional", "-1");
                                lstControl.Items.Insert(0, OptionalItem);
                                lstControl.SelectedValue = "-1";
                            }

                            // Pre-select previous selected addon values in the list
                            for (int i = 0; i < addonValueId.Length; i++)
                            {
                                if (addonValueId[i] != null)
                                {
                                    lstControl.SelectedValue = addonValueId[i];
                                    if (lstControl.SelectedValue == addonValueId[i])
                                    {
                                        break;
                                    }
                                }
                            }

                            // Dropdown list control
                            ControlPlaceHolder.Controls.Add(lstControl);

                            Literal literalSpacer = new Literal();
                            literalSpacer.Text = "<br />";

                            // Add controls to the place holder
                            ControlPlaceHolder.Controls.Add(literalSpacer);
                        }
                    }
                }

                // Create Attributes & Addons for Bundle Products
                if (this._Product.ZNodeBundleProductCollection.Count > 0)
                {
                    foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in this._Product.ZNodeBundleProductCollection)
                    {
                        ZNodeProduct _znodeBundleProduct = ZNodeProduct.Create(_bundleProduct.ProductID);

                        counter = 0;

                        // Add the product name
                        Literal literalbundleProductName = new Literal();
                        literalbundleProductName.Text = "<div style='font-weight:bold;font-size:10px;margin-bottom:5px;'>" + _bundleProduct.Name + "</div>";
                        ControlPlaceHolder.Controls.Add(literalbundleProductName);

                        if (_znodeBundleProduct.ZNodeAttributeTypeCollection.Count > 0)
                        {
                            foreach (ZNodeAttributeType BundleAttributeType in _znodeBundleProduct.ZNodeAttributeTypeCollection)
                            {
                                System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                                lstControl.ID = string.Format("lstBundleAttribute_{0}_{1}", _znodeBundleProduct.ProductID, BundleAttributeType.AttributeTypeId.ToString());
                                lstControl.Attributes.Add("class", "ValueStyle");
                                lstControl.AutoPostBack = true;
                                lstControl.SelectedIndexChanged += new System.EventHandler(this.ValidateAttributes);

                                foreach (ZNodeAttribute Attribute in BundleAttributeType.ZNodeAttributeCollection)
                                {
                                    ListItem li1 = new ListItem(Attribute.Name, Attribute.AttributeId.ToString());
                                    lstControl.Items.Add(li1);
                                }

                                string attributeValues = _znodeBundleProduct.SelectedSKU.AttributesValue;
                                string[] attributes = new string[_znodeBundleProduct.ZNodeAttributeTypeCollection.Count];

                                if (attributeValues.Length > 0)
                                {
                                    attributes = attributeValues.Split(new char[] { ',' }, StringSplitOptions.None);
                                }

                                if (lstControl.Items.Count > 0)
                                {
                                    if (attributes != null)
                                    {
                                        lstControl.SelectedValue = attributes[counter++];
                                    }
                                }

                                // Add the Attribute dropdownlist control to the Controls collection
                                // of the PlaceHolder control.                
                                ControlPlaceHolder.Controls.Add(lstControl);

                                Literal ltrlSpacer = new Literal();
                                ltrlSpacer.Text = "<br />";
                                ControlPlaceHolder.Controls.Add(ltrlSpacer);
                            }
                        }
                        else
                        {
                            ZNodeSKU SKU = ZNodeSKU.CreateByProductDefault(_znodeBundleProduct.ProductID, ExternalAccountNo);
                            _znodeBundleProduct.SelectedSKU = SKU;
                        }

                        if (_znodeBundleProduct.ZNodeAddOnCollection.Count > 0)
                        {
                            foreach (ZNodeAddOn BundleAddon in _znodeBundleProduct.ZNodeAddOnCollection)
                            {
                                System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                                lstControl.ID = string.Format("lstBundleAddOn_{0}_{1}", _znodeBundleProduct.ProductID, BundleAddon.AddOnID.ToString());
                                lstControl.AutoPostBack = true;
                                lstControl.SelectedIndexChanged += new System.EventHandler(this.ValidateAddOns);
                                lstControl.Attributes.Add("class", "ValueStyle");

                                // Don't display list box if there is no add-on values for AddOns
                                if (BundleAddon.ZNodeAddOnValueCollection.Count > 0)
                                {
                                    foreach (ZNodeAddOnValue AddOnValue in BundleAddon.ZNodeAddOnValueCollection)
                                    {
                                        string AddOnValueName = AddOnValue.Name;
                                        decimal decRetailPrice = AddOnValue.FinalPrice;

                                        if (decRetailPrice < 0)
                                        {
                                            // Price format
                                            string priceformat = "-" + ZNodeCurrencyManager.GetCurrencyPrefix() + "{0:0.00}" + ZNodeCurrencyManager.GetCurrencySuffix();
                                            AddOnValueName += " : " + String.Format(priceformat, System.Math.Abs(decRetailPrice)) + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                                        }
                                        else if (decRetailPrice > 0)
                                        {
                                            AddOnValueName += " : " + decRetailPrice.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                                        }

                                        // Added Inventory Message with the BundleAddon Value Name in the dropdownlist
                                        AddOnValueName += " " + this.BindStatusMsg(BundleAddon, AddOnValue);

                                        ListItem li1 = new ListItem(AddOnValueName, AddOnValue.AddOnValueID.ToString());
                                        lstControl.Items.Add(li1);

                                        if (AddOnValue.IsDefault)
                                        {
                                            lstControl.SelectedValue = AddOnValue.AddOnValueID.ToString();
                                        }
                                    }

                                    // Check for Optional 
                                    if (BundleAddon.OptionalInd)
                                    {
                                        ListItem OptionalItem = new ListItem("Optional", "-1");
                                        lstControl.Items.Insert(0, OptionalItem);
                                        lstControl.SelectedValue = "-1";
                                    }

                                    // Pre-select previous selected BundleAddon values in the list
                                    for (int i = 0; i < addonValueId.Length; i++)
                                    {
                                        if (addonValueId[i] != null)
                                        {
                                            lstControl.SelectedValue = addonValueId[i];
                                            if (lstControl.SelectedValue == addonValueId[i])
                                            {
                                                break;
                                            }
                                        }
                                    }

                                    // Dropdown list control
                                    ControlPlaceHolder.Controls.Add(lstControl);

                                    Literal literalSpacer = new Literal();
                                    literalSpacer.Text = "<br />";

                                    // Add controls to the place holder
                                    ControlPlaceHolder.Controls.Add(literalSpacer);
                                }
                            }
                        }
                    }
                }

                // Add the literal control to the Controls collection
                // of the PlaceHolder control.
                literal = new Literal();
                literal.Text = "</div>";
                ControlPlaceHolder.Controls.Add(literal);
            }
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Validate selected addons
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ValidateAddOns(object sender, EventArgs e)
        {
            // Local Variables
            System.Text.StringBuilder _addonValues = new System.Text.StringBuilder();
            this._Product = this.Product;

            foreach (ZNodeAddOn AddOn in this._Product.ZNodeAddOnCollection)
            {
                System.Web.UI.WebControls.DropDownList lstCtrl = new DropDownList();

                lstCtrl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                if (_addonValues.Length > 0)
                {
                    _addonValues.Append(",");
                }

                if (lstCtrl.SelectedValue == "0" || lstCtrl.SelectedValue == "-1")
                {
                    _addonValues.Append(lstCtrl.SelectedValue);
                }
                else
                {
                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        // Optional Addons are not selected,then leave those addons 
                        // If optinal Addons are selected, it should add with the Selected item 
                        // Check for Selected Addon value 
                        if (AddOnValue.AddOnValueID.ToString() == lstCtrl.SelectedValue)
                        {
                            // Add to Selected Addon list for this product
                            _addonValues.Append(AddOnValue.AddOnValueID.ToString());
                        }
                    }
                }
            }

            ZNodeAddOnList SelectedAddOn = new ZNodeAddOnList();

            if (!_addonValues.ToString().Contains("0"))
            {
                // Get a sku based on Add-ons selected
                SelectedAddOn = ZNodeAddOnList.CreateByProductAndAddOns(this._Product.ProductID, _addonValues.ToString());

                SelectedAddOn.SelectedAddOnValueIds = _addonValues.ToString();
            }
            else
            {
                SelectedAddOn.SelectedAddOnValueIds = _addonValues.ToString();
            }

            // Set Selected Add-on 
            this._Product.SelectedAddOnItems = SelectedAddOn;

            // Validate Bundle Addons
            foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in this._Product.ZNodeBundleProductCollection)
            {
                _addonValues = new System.Text.StringBuilder();
                ZNodeProduct _znodeBundleProduct = ZNodeProduct.Create(_bundleProduct.ProductID);

                if (_znodeBundleProduct.ZNodeAddOnCollection.Count > 0)
                {
                    foreach (ZNodeAddOn BundleAddon in _znodeBundleProduct.ZNodeAddOnCollection)
                    {
                        System.Web.UI.WebControls.DropDownList lstCtrl = new DropDownList();

                        lstCtrl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl(string.Format("lstBundleAddOn_{0}_{1}", _znodeBundleProduct.ProductID, BundleAddon.AddOnID.ToString()));

                        if (_addonValues.Length > 0)
                        {
                            _addonValues.Append(",");
                        }

                        if (lstCtrl.SelectedValue == "0" || lstCtrl.SelectedValue == "-1")
                        {
                            _addonValues.Append(lstCtrl.SelectedValue);
                        }
                        else
                        {
                            // Loop through the Add-on values for each Add-on
                            foreach (ZNodeAddOnValue AddOnValue in BundleAddon.ZNodeAddOnValueCollection)
                            {
                                // Optional Addons are not selected,then leave those addons 
                                // If optinal Addons are selected, it should add with the Selected item 
                                // Check for Selected Addon value 
                                if (AddOnValue.AddOnValueID.ToString() == lstCtrl.SelectedValue)
                                {
                                    // Add to Selected Addon list for this product
                                    _addonValues.Append(AddOnValue.AddOnValueID.ToString());
                                }
                            }
                        }
                    }

                    ZNodeAddOnList bundleSelectedAddOn = new ZNodeAddOnList();

                    if (!_addonValues.ToString().Contains("0"))
                    {
                        // Get a sku based on Add-ons selected
                        bundleSelectedAddOn = ZNodeAddOnList.CreateByProductAndAddOns(_znodeBundleProduct.ProductID, _addonValues.ToString());

                        bundleSelectedAddOn.SelectedAddOnValueIds = _addonValues.ToString();
                    }
                    else
                    {
                        bundleSelectedAddOn.SelectedAddOnValueIds = _addonValues.ToString();
                    }

                    // Set Selected Add-on 
                    int idx = this._Product.ZNodeBundleProductCollection.IndexOf((ZNode.Libraries.ECommerce.Entities.ZNodeBundleProductEntity)_bundleProduct);
                    this._Product.ZNodeBundleProductCollection[idx].SelectedAddOns = bundleSelectedAddOn;
                }
            }

            // Update Product
            this.Product = this._Product;
            this.Bind();
        }

        /// <summary>
        /// Validate selected attributes
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ValidateAttributes(object sender, EventArgs e)
        {
            this.ValidateAttributes();
        }

        /// <summary>
        /// Check whether catalog image is exists or not
        /// If exists, then return true otherwise false
        /// </summary>
        /// <param name="filePath">The value of filepath</param>
        /// <returns>Returns bool value to show catalog name </returns>
        protected bool ShowCatalogImage(string filePath)
        {
            // FileInfo object
            System.IO.FileInfo Info = new System.IO.FileInfo(Server.MapPath(filePath));

            // If file exists
            if (Info.Exists)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns boolean value to enable Validator to check against the Quantity textbox
        /// It checks for AllowBackOrder and TrackInventory settings for this product,and returns the value
        /// </summary>
        /// <param name="product">The ZNodeProduct object</param>
        protected void EnableStockValidator(ZNodeProduct product)
        {
            this._Product = product;

            if (this._Product.ProductID > 0)
            {
                // Allow Back Order
                if (this._Product.AllowBackOrder && this._Product.TrackInventoryInd)
                {
                    QuantityRangeValidator.Enabled = false;
                    return;
                }
                else if ((!this._Product.AllowBackOrder) && (!this._Product.TrackInventoryInd))
                {
                    // Don't track inventory
                    QuantityRangeValidator.Enabled = false;
                    return;
                }

                // Enable validator
                this.CheckInventory();
                QuantityRangeValidator.Enabled = true;
            }
            else
            {
                QuantityRangeValidator.Enabled = false;
            }
        }

        /// <summary>
        /// Returns Quantity on Hand (inventory stock value)
        /// </summary>
        protected void CheckInventory()
        {
            this._Product = this.Product;

            if (this._Product.ProductID > 0)
            {
                QuantityRangeValidator.MaximumValue = this._Product.QuantityOnHand.ToString();
                int CurrentQuantity = this._Product.QuantityOnHand;

                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in this._Product.ZNodeBundleProductCollection)
                {
                    if (CurrentQuantity > _bundleProduct.QuantityOnHand && _bundleProduct.AllowBackOrder == false &&
                        _bundleProduct.TrackInventoryInd)
                    {
                        CurrentQuantity = _bundleProduct.QuantityOnHand;
                    }
                }

                // Retreive shopping cart object from current session
                this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                if (this.shoppingCart != null)
                {
                    ZNodeShoppingCartItem Item = new ZNodeShoppingCartItem();
                    Item.Product = this._Product;

                    // Get Current Qunatity by Subtracting QunatityOrdered from Quantity On Hand(Inventory)
                    CurrentQuantity -= this.shoppingCart.GetQuantityOrdered(Item);
                }

                if (CurrentQuantity <= 0)
                {
                    CurrentQuantity = 0;
                }

                QuantityRangeValidator.MaximumValue = CurrentQuantity.ToString();
            }
            else
            {
                QuantityRangeValidator.MaximumValue = "1";
            }

            QuantityRangeValidator.Validate();
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Validate Attributes Method
        /// </summary>
        private void ValidateAttributes()
        {
            // Local Variables
            System.Text.StringBuilder _attributes = new System.Text.StringBuilder();
            System.Text.StringBuilder _description = new System.Text.StringBuilder();
            System.Text.StringBuilder _addonValues = new System.Text.StringBuilder();

            this._Product = this.Product;

            // Validate attributes
            // Loop through types to locate the controls
            foreach (ZNodeAttributeType AttributeType in this._Product.ZNodeAttributeTypeCollection)
            {
                if (_attributes.Length > 0)
                {
                    _attributes.Append(",");
                }

                if (!AttributeType.IsPrivate)
                {
                    System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString());

                    int selValue = 0;
                    if (lstControl.SelectedIndex != -1)
                    {
                        selValue = int.Parse(lstControl.SelectedValue);
                    }

                    if (selValue > 0)
                    {
                        AttributeType.SelectedAttributeId = selValue;

                        _attributes.Append(selValue.ToString());

                        _description.Append(AttributeType.Name);
                        _description.Append(": ");
                        _description.Append(lstControl.SelectedItem.Text);
                        _description.Append("<br />");
                    }
                    else
                    {
                        _attributes.Append(selValue.ToString());
                    }
                }
            }

            ZNodeSKU SKU = new ZNodeSKU();

            if (_attributes.Length > 0)
            {
                SKU = ZNodeSKU.CreateByProductAndAttributes(this._Product.ProductID, _attributes.ToString());

                SKU.AttributesDescription = _description.ToString();
                SKU.AttributesValue = _attributes.ToString();
            }
            else
            {
                SKU = ZNodeSKU.CreateByProductDefault(this._Product.ProductID, ExternalAccountNo);
            }

            this._Product.SelectedSKU = SKU;

            // Validate Bundle Attributes
            foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in this._Product.ZNodeBundleProductCollection)
            {
                ZNodeProduct _znodeBundleProduct = ZNodeProduct.Create(_bundleProduct.ProductID);

                _attributes = new System.Text.StringBuilder();
                _description = new System.Text.StringBuilder();

                if (_znodeBundleProduct.ZNodeAttributeTypeCollection.Count > 0)
                {
                    // Loop through types to locate the controls
                    foreach (ZNodeAttributeType AttributeType in _znodeBundleProduct.ZNodeAttributeTypeCollection)
                    {
                        if (_attributes.Length > 0)
                        {
                            _attributes.Append(",");
                        }

                        if (!AttributeType.IsPrivate)
                        {
                            System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl(string.Format("lstBundleAttribute_{0}_{1}", _znodeBundleProduct.ProductID, AttributeType.AttributeTypeId.ToString()));

                            int selValue = 0;
                            if (lstControl.SelectedIndex != -1)
                            {
                                selValue = int.Parse(lstControl.SelectedValue);
                            }

                            if (selValue > 0)
                            {
                                AttributeType.SelectedAttributeId = selValue;

                                _attributes.Append(selValue.ToString());

                                _description.Append(AttributeType.Name);
                                _description.Append(": ");
                                _description.Append(lstControl.SelectedItem.Text);
                                _description.Append("<br />");
                            }
                            else
                            {
                                _attributes.Append(selValue.ToString());
                            }
                        }
                    }

                    ZNodeSKU bundleSKU = new ZNodeSKU();

                    if (_attributes.Length > 0)
                    {
                        bundleSKU = ZNodeSKU.CreateByProductAndAttributes(_znodeBundleProduct.ProductID, _attributes.ToString());

                        bundleSKU.AttributesDescription = _description.ToString();
                        bundleSKU.AttributesValue = _attributes.ToString();
                    }

                    int idx = this._Product.ZNodeBundleProductCollection.IndexOf((ZNode.Libraries.ECommerce.Entities.ZNodeBundleProductEntity)_bundleProduct);
                    this._Product.ZNodeBundleProductCollection[idx].SelectedSKUvalue = bundleSKU;
                }
            }

            // Product & Bind Fields
            this.Product = this._Product;
            this.Bind();

            // Enable Stock validator
            this.EnableStockValidator(this._Product);
        }

        /// <summary>
        /// Validate the selected product
        /// </summary>
        /// <param name="product">The ZNodeProduct object</param>
        /// <returns>Returns bool value</returns>
        private bool ValidateProduct(ZNodeProduct product)
        {
            if (product != null)
            {
                if (product.ZNodeAttributeTypeCollection.Count == 0)
                {
                    if (product.QuantityOnHand == 0 && product.ProductID > 0 && (!product.AllowBackOrder) && product.TrackInventoryInd)
                    {
                        uxStatus.Text = product.OutOfStockMsg;
                        return false;
                    }
                }
                else
                {
                    string attributeIds = string.Empty;

                    for (int i = 0; i < product.ZNodeAttributeTypeCollection.Count; i++)
                    {
                        if (attributeIds.Length > 0)
                        {
                            attributeIds += ",";
                        }

                        attributeIds += "0";
                    }

                    if (product.SelectedSKU.SKUID == 0 || SKUAdmin.GetQuantity(product.SKU).GetValueOrDefault(0) == 0)
                    {
                        if (product.SelectedSKU.AttributesValue == attributeIds)
                        {
                            uxStatus.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidColorSizeWidth").ToString();
                        }
                        else if (product.SelectedSKU.AttributesValue.Contains("0"))
                        {
                            uxStatus.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidColorSizeWidth").ToString();
                        }
                        else
                        {
                            uxStatus.Text = product.OutOfStockMsg;
                        }

                        return false;
                    }
                }

                if (product.ZNodeAddOnCollection.Count > 0)
                {
                    // Check Addons
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    System.Text.StringBuilder seletecAddOns = new System.Text.StringBuilder();
                    System.Text.StringBuilder optionalAddOns = new System.Text.StringBuilder();

                    foreach (ZNodeAddOn AddOn in product.ZNodeAddOnCollection)
                    {
                        if (sb.Length > 0)
                        {
                            if (!AddOn.OptionalInd)
                            {
                                sb.Append(",");
                            }
                        }

                        if (optionalAddOns.Length > 0)
                        {
                            optionalAddOns.Append(",");
                        }

                        if (AddOn.OptionalInd)
                        {
                            optionalAddOns.Append(AddOn.AddOnID);
                        }
                        else
                        {
                            sb.Append(AddOn.AddOnID);
                            optionalAddOns.Append(AddOn.AddOnID);
                        }
                    }

                    foreach (ZNodeAddOn AddOn in product.SelectedAddOnItems.ZNodeAddOnCollection)
                    {
                        if (seletecAddOns.Length > 0)
                        {
                            seletecAddOns.Append(",");
                        }

                        seletecAddOns.Append(AddOn.AddOnID);
                    }

                    if (sb.ToString() == seletecAddOns.ToString() || seletecAddOns.ToString() == optionalAddOns.ToString())
                    {
                    }
                    else
                    {
                        uxStatus.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RequiredSelectAddon").ToString();
                        return false;
                    }

                    foreach (ZNodeAddOn AddOn in product.SelectedAddOnItems.ZNodeAddOnCollection)
                    {
                        // Loop through the Add-on values for each Add-on
                        foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                        {
                            int currentAddonQuantity = int.Parse(Qty.Text.Trim());
                            this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                            if (this.shoppingCart != null)
                            {
                                ZNodeShoppingCartItem Item = new ZNodeShoppingCartItem();
                                Item.Product = this._Product;

                                // Get Current Qunatity by Subtracting QunatityOrdered from Quantity On Hand(Inventory)
                                currentAddonQuantity += this.shoppingCart.GetQuantityAddOnOrdered(Item);
                            }
                            // Check for quantity on hand and back-order,track inventory settings
                            if (ProductAdmin.GetQuantity(AddOnValue.SKU) < currentAddonQuantity && (!AddOn.AllowBackOrder) && AddOn.TrackInventoryInd)
                            {
                                uxStatus.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ValidAddOnOutOfStock").ToString(), AddOn.Name).ToString();
                                return false;
                            }
                        }
                    }
                }

                if (product.ProductID > 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Add To Cart Method
        /// </summary>
        private bool AddToCart()
        {
            // Get product object from Cache object
            this._Product = this.Product;
            int Quantity = int.Parse(Qty.Text.Trim());

            if (!this.ValidateProduct(this._Product) && Quantity > 0 && this._Product.ProductID > 0)
            {
                return false;
            }

            // Retreive shopping cart object from current session
            this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (this.shoppingCart == null)
            {
                this.shoppingCart = new ZNodeShoppingCart();
                this.shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
            }

            // Add these item if Quantity is greater than zero
            if (Quantity > 0 && this._Product.ProductID > 0)
            {
                ZNodeShoppingCartItem Item = new ZNodeShoppingCartItem();
                Item.Quantity = Quantity;
                Item.Product = new ZNodeProductBase(this._Product);
                if (this._Product.ZNodeAttributeTypeCollection.Count == 0)
                {
                    ZNodeSKU SKU = ZNodeSKU.CreateByProductDefault(this._Product.ProductID, ExternalAccountNo);
                    this._Product.SelectedSKU = SKU;
                }

                // Add item to cart
                this.shoppingCart.AddToCart(Item);
            }
            return true;
        }

        /// <summary>
        ///  Bind Quantity List Method
        /// </summary>
        private void BindQuantityList()
        {
            // Bind the MaxQuantity value to the Dropdown list

            // If Min quantity is not set in admin, set it to 1
            int minQty = this._Product.MinQty == 0 ? 1 : this._Product.MinQty;

            // If Max quantity is not set in admin , set it to 10
            int maxQty = this._Product.MaxQty == 0 ? 10 : this._Product.MaxQty;

            ArrayList quantityList = new ArrayList();

            for (int itemIndex = minQty; itemIndex <= maxQty; itemIndex++)
            {
                quantityList.Add(itemIndex);
            }

            Qty.DataSource = quantityList;
            Qty.DataBind();

            Qty.SelectedIndex = Qty.Items.IndexOf(Qty.Items.FindByValue(this.Quantity.ToString()));
        }

        /// <summary>
        /// Bind Products Method
        /// </summary>
        private void BindProducts()
        {
            int currentPageIndex = int.Parse(PageIndex.Value);

            int totalPages = 0;

            if (!string.IsNullOrEmpty(txtProductName.Text.Trim()) || !string.IsNullOrEmpty(txtProductSku.Text.Trim()) || !string.IsNullOrEmpty(txtProductNum.Text.Trim()) || !string.IsNullOrEmpty(txtBrand.Text.Trim()) || !string.IsNullOrEmpty(txtCategory.Text.Trim()))
            {
                // Get Products            
                DataTable productList = ZNodeProductList.GetPagedProductListByPortalID(this.CurrentPortalID, Server.HtmlEncode(txtProductName.Text.Trim()), txtProductNum.Text.Trim(), txtProductSku.Text.Trim(), txtBrand.Text.Trim(), Server.HtmlEncode(txtCategory.Text.Trim()), currentPageIndex, uxGrid.PageSize, this.CatalogId, null, out totalPages, ExternalAccountNo);

                TotalPages.Value = totalPages.ToString();

                if (productList.Rows.Count > 0)
                {
                    pnlProductList.Visible = true;

                    uxGrid.DataSource = productList;
                    uxGrid.DataBind();

                    if (totalPages > 1)
                    {
                        NextPageLink.Enabled = true;
                        LastPageLink.Enabled = true;
                    }
                }
                else
                {
                    lblSearhError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorValidNoProducts").ToString();
                }
            }
            else
            {
                lblSearhError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidEnterProductNameorSKU").ToString();
            }
        }
        #endregion
    }
}