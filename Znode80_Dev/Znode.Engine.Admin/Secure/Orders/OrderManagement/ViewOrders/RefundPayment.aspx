<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.OrderManagement.ViewOrders.RefundPayment" Title="Untitled Page" CodeBehind="RefundPayment.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <h1><asp:Localize ID="TitleRefundCreditcard" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleRefundCreditcard %>'></asp:Localize></h1>
        <asp:Panel ID="pnlEdit" runat="server" Visible="true">
            <p>
               <asp:Localize ID="TextRefundCreditCard" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextRefundCreditCard %>'></asp:Localize>
            </p>
            <p>
                <asp:Localize ID="TextRefundCreditCardGateway" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextRefundCreditCardGateway %>'></asp:Localize>
            </p>
            <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
            <div style="margin-bottom: 20px; margin-top: 20px;">
                <div>
                    <b><asp:Localize ID="OrderID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOrderID %>'></asp:Localize></b>
                    <asp:Label ID="lblOrderID" runat="server" />
                </div>
                <div>
                    <b><asp:Localize ID="TransactionID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTransactionID %>'></asp:Localize> </b>
                    <asp:Label ID="lblTransactionID" runat="server" />
                </div>
                <div>
                    <b><asp:Localize ID="CustomerName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCustomerName %>'></asp:Localize></b>
                    <asp:Label ID="lblCustomerName" runat="server" />
                </div>
                <div>
                    <b><asp:Localize ID="OrderTotal" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOrderTotal %>'></asp:Localize> </b>
                    <asp:Label ID="lblTotal" runat="server" />
                </div>
            </div>
            <div>
                <div class="FieldStyle">
                    <asp:Label ID="lblCardNumber" runat="server"><asp:Localize ID="CreditCardNoText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCreditCard %>'></asp:Localize> </asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtCardNumber" runat="server" Width="130" Columns="30" MaxLength="100" />
                    <br />
                    <asp:RequiredFieldValidator ID="reqCardNumber" runat="server" ControlToValidate="txtCardNumber" ValidationGroup="refund"
                        ErrorMessage="Enter Credit Card Number" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>

                </div>
                <asp:Panel ID="pnlCreditCardInfo" runat="server" Visible="false">
                    <div class="FieldStyle">
                       <asp:Localize ID="ExpirationDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleExpirationDate %>'></asp:Localize> 
                    </div>
                    <div class="ValueStyle">
                        <div>
                            <asp:Localize ID="SelectMonth" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextSelectMonth %>'></asp:Localize>
                             <span style="margin-left: 15px;"> <asp:Localize ID="SelectYear" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextSelectYear %>'></asp:Localize></span>
                        </div>
                        <asp:DropDownList ID="lstMonth" runat="server" Width="80">
                            <asp:ListItem Value="01" Text='<%$ Resources:ZnodeAdminResource, DropDownTextJan %>'></asp:ListItem>
                            <asp:ListItem Value="02" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFeb %>'></asp:ListItem>
                            <asp:ListItem Value="03" Text='<%$ Resources:ZnodeAdminResource, DropDownTextMar %>'></asp:ListItem>
                            <asp:ListItem Value="04" Text='<%$ Resources:ZnodeAdminResource, DropDownTextApr %>'></asp:ListItem>
                            <asp:ListItem Value="05" Text='<%$ Resources:ZnodeAdminResource, DropDownTextMay %>'></asp:ListItem>
                            <asp:ListItem Value="06" Text='<%$ Resources:ZnodeAdminResource, DropDownTextJun %>'></asp:ListItem>
                            <asp:ListItem Value="07" Text='<%$ Resources:ZnodeAdminResource, DropDownTextJul %>'></asp:ListItem>
                            <asp:ListItem Value="08" Text='<%$ Resources:ZnodeAdminResource, DropDownTextAug %>'></asp:ListItem>
                            <asp:ListItem Value="09" Text='<%$ Resources:ZnodeAdminResource, DropDownTextSep %>'></asp:ListItem>
                            <asp:ListItem Value="10" Text='<%$ Resources:ZnodeAdminResource, DropDownTextOct %>'></asp:ListItem>
                            <asp:ListItem Value="11" Text='<%$ Resources:ZnodeAdminResource, DropDownTextNov %>'></asp:ListItem>
                            <asp:ListItem Value="12" Text='<%$ Resources:ZnodeAdminResource, DropDownTextDec %>'></asp:ListItem>
                        </asp:DropDownList>
                        <span style="margin-left: 5px;">
                            <asp:DropDownList ID="lstYear" runat="server" Width="80">
                            </asp:DropDownList>
                        </span>
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="SecurityCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSecurityCode %>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtSecurityCode" runat="server" Columns="3" MaxLength="4" />
                        <asp:RequiredFieldValidator ControlToValidate="txtSecurityCode" CssClass="Error" ValidationGroup="refund"
                            Display="Dynamic" ID="SecurityCodeValidator" runat="server" ErrorMessage="Enter Security Code"></asp:RequiredFieldValidator>
                    </div>
                </asp:Panel>
                <div class="FieldStyle">
                  <asp:Localize ID="AmountToRefund" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAmountToRefund %>'></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtAmount" runat="server" Width="130" Columns="30" MaxLength="100" /><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtAmount" ValidationGroup="refund"
                        runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAmount %>' CssClass="Error"></asp:RequiredFieldValidator><br />
                    <asp:RangeValidator
                        ID="RangeValidator1" runat="server" ControlToValidate="txtAmount" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareAffiliateAmount %>' ValidationGroup="refund"
                        MaximumValue="99999999" Type="Currency" MinimumValue="0.01" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
                </div>
            </div>
            <div class="ClearBoth"></div>
            <div>
               

                <zn:Button runat="server" ID="Void" ButtonType="EditButton"  Width="100px" OnClick="Void_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonVoid %>' />
                <zn:Button runat="server" ID="Refund" ButtonType="EditButton"  Width="100px" OnClick="Refund_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonRefund %>'  ValidationGroup="refund"/>


                 <zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton" OnClick="Cancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlConfirm" runat="server" Visible="false">
            <div id="OrderReceipt" runat="server">
                <%=ReceiptTemplate%>
            </div>
            <div style="margin-top: 20px;">
                <a id="A1" href="~/Secure/Orders/OrderManagement/ViewOrders/Default.aspx" runat="server">
                     <asp:Localize ID="Back" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkBackToOrder %>'></asp:Localize>
             </a>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
