using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Orders.OrderManagement.ViewOrders
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Orders.Sales.Orders - View class
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Protected variables
        private int OrderID = 0;
        private bool AdvancedShipping = false;
        private string StatusPage = "Orderstatus.aspx?itemid=";
        private string RefundPage = "RefundPayment.aspx?itemid=";
        private string ListPage = "Default.aspx";
        private string AccountListPage = "~/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=order&itemid={0}";
        private string VendorListPage = "~/Secure/Vendors/VendorAccounts/View.aspx?mode=order&pagefrom=vendor&itemid=";
        private string FranchiseListPage = "~/Secure/Vendors/FranchiseAdministrators/View.aspx?mode=order&pagefrom=franchise&itemid=";
        private string AdminListPage = "~/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?mode=order&pagefrom=admin&itemid=";
        private string OrderTrackingNumber = string.Empty;
        private string filepath = ZNodeConfigManager.EnvironmentConfig.DataPath + "/ShippingLabels//FedEx//";
        private string _RoleName = string.Empty;
        private string folderName = string.Empty;
        private string statusText = string.Empty;
		
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string RoleName
        {
            get
            {
                return this._RoleName;
            }

            set
            {
                this._RoleName = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Sets grid button Text
        /// </summary>
        /// <param name="trackingNumber"> The value of trackingnumber</param>
        /// <param name="orderlineItemId">The value of orderlineitemid</param>
        /// <returns> Returns the Button Text</returns>
        public string ButtonText(object trackingNumber, object orderlineItemId)
        {
            if (orderlineItemId != null)
            {
                // If the value for this row matches the just estimated line item we set the command to create
                if (orderlineItemId.ToString() == EstimatedLineItemID.Value)
                {
                    return "Create Shipment";
                }
                if (trackingNumber != null)
                {
                    if (trackingNumber.ToString().Length > 0)
                    {
                        return "Cancel Shipment";
                    }
                }
                return "Estimate Dimensions";
            }
            return string.Empty;
        }

        /// <summary>
        /// Sets the Command for the Ship seperately grid
        /// </summary>
        /// <param name="trackingNumber"> The value of trackingnumber</param>
        /// <param name="orderlineItemId">The value of orderlineitemid</param>
        /// <returns>Returns the Shipping command</returns>
        public string ShippingCommand(object trackingNumber, object orderlineItemId)
        {
            if (orderlineItemId != null)
            {
                // If the value for this row matches the just estimated line item we set the command to create
                if (orderlineItemId.ToString() == EstimatedLineItemID.Value)
                {
                    return "CreateShipment";
                }
                if (trackingNumber != null)
                {
                    if (trackingNumber.ToString().Length > 0)
                    {
                        return "CancelShipment";
                    }
                }
                return "EstimateDimensions";
            }
            return string.Empty;
        }

        /// <summary>
        /// Checks to see if a label exists before displaying the label button
        /// </summary>
        /// <param name="value">Represents a value</param>
        /// <returns>Returns true or false</returns>
        public bool ShowLabelButton(object value)
        {
            if (value != null)
            {
                if (value.ToString().Length > 0)
                {
                    if (File.Exists(Server.MapPath(this.filepath) + value.ToString() + ".pdf") && this.AdvancedShipping)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Format Price Method
        /// </summary>
        /// <param name="fieldValue">The value of fieldValue</param>
        /// <returns>Returns the formatted price</returns>
        public string Formatprice(object fieldValue)
        {
            string Price = String.Empty;

            if (fieldValue != null)
            {
                Price = String.Format("{0:c}", fieldValue);
            }
            return Price;
        }

        /// <summary>
        /// Get Order State
        /// </summary>
        /// <param name="fieldValue">The value of fieldValue</param>
        /// <returns>Returns the string</returns>
        public string GetOrderState(object fieldValue)
        {
            string OrderStatus = " ";
            if (fieldValue != null)
            {
                ZNode.Libraries.Admin.OrderAdmin _OrderStateAdmin = new ZNode.Libraries.Admin.OrderAdmin();
                OrderState _orderStateList = _OrderStateAdmin.GetByOrderStateID(int.Parse(fieldValue.ToString()));

                OrderStatus = _orderStateList.OrderStateName.ToString();
            }
            return OrderStatus;
        }

        /// <summary>
        /// Returns shipping option name for this shipping Id
        /// </summary>
        /// <param name="shippingId">The value of shippingId</param>
        /// <returns>Returns the Shipping option Name</returns>
        public string GetShippingOptionName(int shippingId)
        {
            string Name = string.Empty;
            ZNode.Libraries.Admin.ShippingAdmin shippingAdmin = new ZNode.Libraries.Admin.ShippingAdmin();
			ZNode.Libraries.DataAccess.Entities.Shipping entity = shippingAdmin.GetShippingOptionById(shippingId);
            if (entity != null)
            {
                Name = entity.Description;
            }
            return Name;
        }

        /// <summary>
        /// Returns payment type name for this payment type id
        /// </summary>
        /// <param name="paymentTypeId">The value of paymentTypeId</param>
        /// <returns>Returns the Payment type name</returns>
        public string GetPaymentTypeName(int paymentTypeId)
        {
            string Name = string.Empty;
            ZNode.Libraries.Admin.StoreSettingsAdmin settingsAdmin = new ZNode.Libraries.Admin.StoreSettingsAdmin();
            PaymentType entity = settingsAdmin.GetPaymentTypeById(paymentTypeId);
            if (entity != null)
            {
                Name = entity.Name;
            }
            return Name;

        }

        /// <summary>
        /// Check Is Advanced Shipping Method
        /// </summary>
        /// <returns>Returns bool value true or false</returns>
        public bool IsAdvancedShipping()
        {
            return this.AdvancedShipping;
        }

        #region RMA Request Methods
        /// <summary>
        /// Bind RMA Request
        /// </summary>
        public void BindRMARequest()
        {
             string PortalIds = UserStoreAccess.GetAvailablePortals;
            if (PortalIds.Length > 0)
            {
                RMAHelper _rmaHelper = new RMAHelper();
                DataSet rmaDataSet = _rmaHelper.SearchRMARequest(null, this.OrderID, string.Empty, string.Empty, null, null, null, null, PortalIds);
               DataView dv = new DataView(rmaDataSet.Tables[0]);
                dv.Sort = "RMARequestId Desc";
                uxRMAGrid.DataSource = dv;
                uxRMAGrid.DataBind();
                if (dv.Count > 0)
                    pnlrma.Visible = true;
                else
                    pnlrma.Visible = false;
                //Added the code to display the void or refund button based on RMA request status
                // If Autorized / Returned / Refunded then do not display the Button else if the status is Void then display the button.
                DataView dvRefund = new DataView(rmaDataSet.Tables[0]);
                dvRefund.RowFilter = string.Format("RequestStatusID = {0} OR RequestStatusID = {1}",(int)ZNodeRequestState.Authorized, (int)ZNodeRequestState.ReturnedOrRefunded);
                Refund.Visible = (dvRefund.Count == 0) && Refund.Visible;
            }
           
        }

      
        /// <summary>
        /// Display the Order State name for a Order state
        /// </summary>
        /// <param name="fieldValue">The value of fieldValue</param>
        /// <returns>Returns the order status</returns>
        protected string DisplayRequestStatus(object fieldValue)
        {
            ZNode.Libraries.Admin.RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            RequestStatus _RequestStatus = rmaConfigAdmin.GetByRequestStatusID(int.Parse(fieldValue.ToString()));
            return _RequestStatus.Name;
        }

       
        #endregion
        /// <summary>
        /// Bind Data method
        /// </summary>
        public void BindData()
        {
            // Declarations
            OrderAdmin _OrderAdmin = new OrderAdmin();
			//OrderShipments _OrderShip = new OrderShipments();
            Order _orderList = _OrderAdmin.DeepLoadByOrderID(this.OrderID);

            TList<OrderLineItem> orderLineItem = new TList<OrderLineItem>();
            var orderShipmentService = new OrderShipmentService();
            var orderShipment = new OrderShipment();
            var orderLineItemService = new OrderLineItemService();

            ArrayList arrayList = new ArrayList();
            StringBuilder builder = new StringBuilder();

            orderLineItem = orderLineItemService.GetByOrderID(this.OrderID);
            foreach (var orderItem in orderLineItem)
            {
                if (orderItem.OrderShipmentID.HasValue)
                {
                    orderShipment = orderShipmentService.GetByOrderShipmentID(int.Parse(orderItem.OrderShipmentID.ToString()));
                    if (orderShipment.ShippingID.HasValue)
                    {
                        if (!arrayList.Contains(orderShipment.ShippingID))
                        {
                            arrayList.Add(orderShipment.ShippingID);
                        }
                    }
                }
            }

            ViewState["AccountID"] = _orderList.AccountID;
            if (_orderList != null)
            {
                UserStoreAccess.CheckStoreAccess(_orderList.PortalId.GetValueOrDefault(0), true);
                this.AdvancedShipping = false;
                AccountListPage = string.Format(AccountListPage, _orderList.AccountID);

                if (RoleName.ToLower() == "admin")
                {
                    AdminListPage = AdminListPage + ViewState["AccountID"];
                }
                else if (RoleName.ToLower() == "franchise")
                {
                    FranchiseListPage = FranchiseListPage + ViewState["AccountID"];
                }
                else if (RoleName.ToLower() == "vendor")
                {
                    VendorListPage = VendorListPage + ViewState["AccountID"];
                }
                else
                {
                    ViewState["AccountListPage"] = string.Format("~/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=order&itemid={0}&pagefrom={1}", ViewState["AccountID"], RoleName);
                }
                // FedEx shipping is the only advanced shipping currently supported.
                if (_orderList.ShippingID.HasValue && _orderList.ShippingIDSource.ShippingTypeID == 3)
                {
                    if (_orderList.ShippingIDSource.ShippingCode.ToLower().Contains("international"))
                    {
                        this.AdvancedShipping = false;
                    }
                }
                StringBuilder Build = new StringBuilder();
                Build.Append(this.CheckNull(_orderList.BillingFirstName) + " ");
                Build.Append(this.CheckNull(_orderList.BillingLastName) + "<br>");
                Build.Append(this.CheckNull(_orderList.BillingCompanyName) + "<br>");
                Build.Append(this.CheckNull(_orderList.BillingStreet) + " ");
                Build.Append(this.CheckNull(_orderList.BillingStreet1) + "<br>");
                Build.Append(this.CheckNull(_orderList.BillingCity) + ", ");
                Build.Append(this.CheckNull(_orderList.BillingStateCode) + " ");
                Build.Append(this.CheckNull(_orderList.BillingPostalCode) + "<br>");
                Build.Append(this.CheckNull(_orderList.BillingCountry) + "<br>");
                Build.Append("Tel: " + this.CheckNull(_orderList.BillingPhoneNumber) + "<br>");
                Build.Append("Email: " + this.CheckNull(_orderList.BillingEmailId));

                lblBillingAddress.Text = Build.ToString();
                Build.Remove(0, Build.Length);
				
                lblOrderDate.Text = _orderList.OrderDate.Value.ToString("MM/dd/yyyy hh:mm tt");
                if (_orderList.OrderStateIDSource != null)
                {
                    lblOrderStatus.Text = _orderList.OrderStateIDSource.Description;
                    if (lblOrderStatus.Text == "Pending Approval")
                    {
                        lblOrderStatus.ForeColor = System.Drawing.Color.Red;
                    }
                    statusText = lblOrderStatus.Text;
                    lblOrderStatus.Text = statusText.ToUpper();
                    statusText = string.Empty;
                }
                lblShipAmount.Text = this.Formatprice(_orderList.ShippingCost);
                lblOrderAmount.Text = this.Formatprice(_orderList.Total);
                lblTaxAmount.Text = this.Formatprice(_orderList.TaxCost);

                if (_orderList.DiscountAmount.HasValue)
                {
                    lblDiscountAmt.Text = _orderList.DiscountAmount.Value.ToString("c");
                }
                lblCouponCode.Text = _orderList.CouponCode;

                if (_orderList.PaymentTypeId.HasValue)
                {
                    string paymentTypeName = this.GetPaymentTypeName(_orderList.PaymentTypeId.Value);

                    // If purchase order payment
                    if (_orderList.PaymentTypeId.Value == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PURCHASE_ORDER)
                    {
                        lblPurchaseOrder.Text = _orderList.PurchaseOrderNumber;
                        Refund.Visible = false;
                    }
                    lblPaymentType.Text = paymentTypeName;
                }
                lblTransactionId.Text = _orderList.CardTransactionID;

                if (orderLineItem != null)
                {
                    foreach (int item in arrayList)
                    {
                        if (!string.IsNullOrEmpty(builder.ToString()))
                        {
                            builder.Append(", " + this.GetShippingOptionName(item));
                        }
                        else
                        {
                            builder.Append(this.GetShippingOptionName(item));
                        }
                        lblShippingMethod.Text = builder.ToString();
                    }
                }

                lblTrackingNumber.Text = _orderList.TrackingNumber;
                if (_orderList.PaymentStatusIDSource != null)
                {
                 
					lblPaymentStatus.Text = _orderList.PaymentStatusIDSource.Description;

					if (_orderList.PaymentStatusIDSource.PaymentStatusName == "CC_PENDING" || _orderList.PaymentStatusIDSource.PaymentStatusName == "PO_PENDING" || _orderList.PaymentStatusIDSource.PaymentStatusName == "COD_PENDING")
					{
						lblPaymentStatus.ForeColor = System.Drawing.Color.Red;
					}

					statusText = lblPaymentStatus.Text;
					lblPaymentStatus.Text = statusText.ToUpper();
					statusText = string.Empty;
                }
                // Bind custom additional instructions to label field
                
                lblAdditionalInstructions.Text = this.CheckNull(_orderList.AdditionalInstructions);

                if (_orderList.OrderStateIDSource.OrderStateName == "RETURNED" || _orderList.OrderStateIDSource.OrderStateName == "CANCELLED" || _orderList.PaymentTypeId == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT)
                {
                    Refund.Visible = false;
                }
                // Display gift card amount if any.
                GiftCardAdmin giftCardAdmin = new GiftCardAdmin();
                TList<GiftCardHistory> giftCardHistoryList = giftCardAdmin.GetGiftCardHistoryByAccountID(Convert.ToInt32(_orderList.AccountID), _orderList.OrderID);
                decimal giftCardAmount = 0;
                if (giftCardHistoryList.Count > 0)
                {
                    giftCardAmount = giftCardHistoryList[0].TransactionAmount;
                }
                lblGiftCardAmount.Text = this.Formatprice(giftCardAmount);
                this.OrderTrackingNumber = _orderList.TrackingNumber;
                if (this.OrderTrackingNumber == null)
                {
                    this.OrderTrackingNumber = string.Empty;
                }
                if (!this.AdvancedShipping)
                {
                    ShippingErrorPanel.Visible = false;
                }
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoOrderRequest").ToString());
            }
        }
        #endregion

        #region Page Load

        /// <summary>
        /// Page Pre Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreInit(object sender, EventArgs e)
        {
            // Change the master file, if the user is Order Approver.
            if (HttpContext.Current.User.IsInRole("ORDER APPROVER"))
            {
                this.MasterPageFile = "~/Themes/Standard/order.master";
                this.ListPage = "Default.aspx";
            }
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["pagefrom"] != null)
            {
                this.RoleName = Request.Params["pagefrom"].ToString();
            }
            if ((Request.Params["itemid"] != null) && (Request.Params["itemid"].Length != 0))
            {
                this.OrderID = int.Parse(Request.Params["itemid"].ToString());
                lblOrderHeader.Text = this.OrderID.ToString();
            }
            // Change the ChangeStatus button status, if the user is Order Approver.
            ChangeStatus.Enabled = true;
            
            if (!Page.IsPostBack)
            {
                this.BindData();
                this.BindGrid();
            }
            // Build the javascript block
            StringBuilder sb = new StringBuilder();
            sb.Append("<script language=JavaScript>");
            sb.Append("    function Back() {");
            sb.Append("        javascript:history.go(-1);");
            sb.Append("    }");
            sb.Append("<" + "/script>");
            if (!ClientScript.IsStartupScriptRegistered("GoBack"))
            {
                ClientScript.RegisterStartupScript(GetType(), "GoBack", sb.ToString());
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Change Status Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChangeStatus_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.StatusPage + this.OrderID);
        }

        /// <summary>
        /// Refund Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Refund_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.RefundPage + this.OrderID);
        }

        /// <summary>
        /// List Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void List_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["from"] == "account")
            {
                if (RoleName.ToLower() == "admin")
                {
                    Response.Redirect(this.AdminListPage + ViewState["AccountID"]);
                }
                else if (RoleName.ToLower() == "franchise")
                {
                    Response.Redirect(this.FranchiseListPage + ViewState["AccountID"]);
                }
                else if (RoleName.ToLower() == "vendor")
                {
                    Response.Redirect(this.VendorListPage + ViewState["AccountID"]);
                }
                else
                {
                    Response.Redirect(ViewState["AccountListPage"].ToString() + ViewState["AccountID"]);
                }
            }
            else
            {
                Response.Redirect(this.ListPage);
            }

        }
        

        /// <summary>
        /// Order Items Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        /// <summary>
        /// Grid Button Command for Estimating Package Size, Creating Shipments, Cancelling Shipments, and Printing Labels
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
                ShippingErrors.Text = string.Empty;
                ZNode.Libraries.Admin.ShippingAdmin _ShippingAdmin = new ShippingAdmin();
                OrderAdmin _OrderAdmin = new OrderAdmin();
                TList<OrderLineItem> items = _OrderAdmin.GetOrderLineItemByOrderID(this.OrderID);
                TList<OrderLineItem> singleshipitemlist = new TList<OrderLineItem>();
                foreach (OrderLineItem i in items)
                {
                    if (i.OrderLineItemID.ToString() == e.CommandArgument.ToString())
                    {
                        singleshipitemlist.Add(i);
                    }
                }
                if (_ShippingAdmin.ErrorCode != "0")
                {
                    ShippingErrors.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCodeMessage").ToString(), _ShippingAdmin.ErrorCode, _ShippingAdmin.ErrorDescription);
                }			
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Check Null Method
        /// </summary>
        /// <param name="value">The value of sValue</param>
        /// <returns>Returns the string </returns>
        private string CheckNull(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Bind grid with Order line items
        /// </summary>
        private void BindGrid()
        {
            ZNode.Libraries.Admin.OrderAdmin _OrderLineItemAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            ShippingAdmin _ShippingAdmin = new ShippingAdmin();
            TList<OrderLineItem> fullorderlist = _OrderLineItemAdmin.GetOrderLineItemByOrderID(this.OrderID);
            TList<OrderLineItem> shiptogetherlist = new TList<OrderLineItem>();
            TList<OrderLineItem> shipseperatelist = new TList<OrderLineItem>();
            shiptogetherlist = fullorderlist.FindAll(delegate(OrderLineItem item) { return item.ShipSeparately != true; });
			shipseperatelist = fullorderlist.FindAll(delegate(OrderLineItem item) { return item.ShipSeparately == true; });
			if (shiptogetherlist.Any())
			{
				bool isMultipleAddress = shiptogetherlist.Select(x => x.OrderShipmentID).Distinct().Count() > 1;
				StringBuilder Build = new StringBuilder();
				
				if (shiptogetherlist.Count > 1 && isMultipleAddress)
				{
                    lblShippingAddress.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextShipMultipleAddress").ToString();
				}
				else
				{
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToFirstName) + " ");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToLastName) + "<br>");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToCompanyName) + "<br>");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToStreet) + " ");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToStreet1) + "<br>");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToCity) + ", ");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToStateCode) + " ");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToPostalCode) + "<br>");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToCountry) + "<br>");
					Build.Append("Tel: " + this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToPhoneNumber) + "<br>");
					Build.Append("Email: " + this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToEmailID));
					lblShippingAddress.Text = Build.ToString();
				}
			}
            uxGrid2.DataSource = shipseperatelist;
            uxGrid.DataSource = shiptogetherlist;
            uxGrid.DataBind();
            uxGrid2.DataBind();
            this.BindRMARequest();
        }
        #endregion

    }
}