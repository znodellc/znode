using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Xsl;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Admin;

namespace  Znode.Engine.Admin.Secure.Orders.OrderManagement.ViewOrders
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Orders.Sales.Orders - Refund class
    /// </summary>
    public partial class RefundPayment : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int OrderID = 0;
        private string _ReceiptTemplate = string.Empty;
        private string ReceiptText = string.Empty;
        private Order order;
        private decimal rmaamount = 0.0m;
        private int rmaid = 0;
        private string mode = string.Empty;
        private string rmaitems = string.Empty;
        private string qtylist = string.Empty;
        /// <summary>
        /// Gets or sets the Product Review Title
        /// </summary>
        public string ReceiptTemplate
        {
            get
            {
                return this._ReceiptTemplate;
            }

            set
            {
                this._ReceiptTemplate = value;
            }
        }

        /// <summary>
        /// Gets the customer feedback url
        /// </summary>
        public string FeedBackUrl
        {
            get
            {
                return Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath + "/CustomerFeedback.aspx";
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Enable Refund Method
        /// </summary>
        /// <param name="gatewayTypeID"> The value of gatewayTypeID</param>
        /// <returns>Returns true or false</returns>
        public bool EnableRefund(int gatewayTypeID)
        {
            GatewayHelper helper = new GatewayHelper();
            return helper.GetCapability((GatewayType)gatewayTypeID, GatewayTransactionType.REFUND);
        }

        /// <summary>
        /// Enable Void Button Method
        /// </summary>
        /// <param name="gatewayTypeID"> The value of GatewayTypeID</param>
        /// <returns>Returns bool value</returns>
        public bool EnableVoid(int gatewayTypeID)
        {
            GatewayHelper helper = new GatewayHelper();
            return helper.GetCapability((GatewayType)gatewayTypeID, GatewayTransactionType.VOID);
        }

        #endregion

        #region Page Load Event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.Params["itemid"] != null) && (Request.Params["itemid"].Length != 0))
            {
                this.OrderID = int.Parse(Request.Params["itemid"].ToString());
                lblOrderID.Text = this.OrderID.ToString();
            }
            else
                Response.Redirect("Default.aspx");

            if (Request.Params["mode"] != null)
            {
                mode = Request.Params["mode"].ToString();
                if (Request.Params["rmaid"] != null)
                    int.TryParse(Request.Params["rmaid"].ToString(), out rmaid);
                if (Request.Params["amount"] != null)
                    decimal.TryParse(Request.Params["amount"].ToString(), out rmaamount);
                if (Request.Params["items"] != null)
                    rmaitems = Request.Params["items"].ToString();
                if (Request.Params["qty"] != null)
                    this.qtylist = Request.Params["qty"].ToString();

            }

            if (!Page.IsPostBack)
            {
                if (mode == "RMA")
                    this.BindRMAData();
                else
                this.BindData();

            }
        }

        #endregion

        /// <summary>
        /// Refund button clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Refund_Click(object sender, EventArgs e)
        {
            // Retrieve order
            ZNode.Libraries.Admin.OrderAdmin orderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            this.order = orderAdmin.GetOrderByOrderID(this.OrderID);
            ZNode.Libraries.Framework.Business.ZNodeEncryption enc = new ZNode.Libraries.Framework.Business.ZNodeEncryption();
            
            // Get payment settings
            int paymentSettingID = (int)this.order.PaymentSettingID;
            PaymentSettingService pss = new PaymentSettingService();
            PaymentSetting ps = pss.GetByPaymentSettingID(paymentSettingID);

            // Set gateway info
            GatewayInfo gi = new GatewayInfo();
            gi.GatewayLoginID = enc.DecryptData(ps.GatewayUsername);
            gi.GatewayPassword = enc.DecryptData(ps.GatewayPassword);
            gi.TransactionKey = enc.DecryptData(ps.TransactionKey);
            gi.Vendor = ps.Vendor;
            gi.Partner = ps.Partner;
            gi.TestMode = ps.TestMode;
            gi.Gateway = (GatewayType)ps.GatewayTypeID;

            string creditCardExp = Convert.ToString(this.order.CardExp);
            if (creditCardExp == null)
            {
                creditCardExp = string.Empty;
            }

            // Set credit card
            CreditCard cc = new CreditCard();
            cc.Amount = Decimal.Parse(txtAmount.Text);
            cc.CardNumber = txtCardNumber.Text.Trim();
            cc.CreditCardExp = creditCardExp;
            cc.OrderID = this.order.OrderID;
            cc.TransactionID = this.order.CardTransactionID;

            GatewayResponse resp = new GatewayResponse();

            if ((GatewayType)ps.GatewayTypeID == GatewayType.AUTHORIZE)
            {
                GatewayAuthorize auth = new GatewayAuthorize();
                resp = auth.RefundPayment(gi, cc);
            }
            else if ((GatewayType)ps.GatewayTypeID == GatewayType.VERISIGN)
            {
                GatewayPayFlowPro pp = new GatewayPayFlowPro();
                resp = pp.RefundPayment(gi, cc);
            }
            else if ((GatewayType)ps.GatewayTypeID == GatewayType.PAYMENTECH)
            {
                GatewayOrbital pmt = new GatewayOrbital();
                resp = pmt.ReversePayment(gi, cc);
            }            
            else if ((GatewayType)ps.GatewayTypeID == GatewayType.CYBERSOURCE)
            {
                GatewayCyberSource cybersource = new GatewayCyberSource();
                cc.TransactionID = lblTransactionID.Text;
                cc.CardDataToken = this.order.CardAuthCode;
                cc.Amount = Decimal.Parse(txtAmount.Text.Trim());
                gi.TransactionType = Enum.GetName(typeof(ZNode.Libraries.ECommerce.Entities.ZNodePaymentStatus), this.order.PaymentStatusID.Value);
                resp = cybersource.RefundPayment(gi, cc);
            }
            else
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorRefundNoGatewaySupport").ToString();
                return;
            }

            if (resp.IsSuccess)
            {
                if (mode == "RMA")
                {
                    RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
                    RMARequest rmaRequest = rmaRequestAdmin.GetByRMARequestID(this.rmaid);

                    if (rmaRequest != null)
                    {
                        rmaRequest.RequestStatusID = Convert.ToInt32(ZNodeRequestState.ReturnedOrRefunded); ;

                    }
                    rmaRequestAdmin.Update(rmaRequest);


                    string[] lineitems = rmaitems.Split(',');
                    string[] qtyItems = qtylist.Split(',');
                    //Add RMAREquestItem with Refund Transaction id
                    for (int i = 0; i < lineitems.Length;i++)
                    {
                        RMARequestItem item = new RMARequestItem();
                        int orderlineitemid = Convert.ToInt32(lineitems[i]);
                        item.RMARequestID = this.rmaid;
                        item.Quantity = Convert.ToInt32(qtyItems[i]);
                        item.OrderLineItemID = orderlineitemid;
                        item.IsReturnable = true;
                        item.TansactionId = resp.TransactionId;
                        item.IsReceived = true;
                        rmaRequestAdmin.Add(item);

                    }
                   
                  SendStatusMail(this.rmaid);
                   // Response.Redirect("~/Secure/Sales/RMA/RMARequest.aspx?mode=edit&itemid=" + rmaid.ToString() + "&orderid=" + OrderID.ToString());
                  Response.Redirect("~/Secure/Orders/ReturnsManagement/RMAManager/Default.aspx");
                }

                // Update order status
                // Returned status
                this.order.OrderStateID = 30;

                // Refund status
                this.order.PaymentStatusID = 3;

                OrderService os = new OrderService();
                os.Update(this.order);

                this._ReceiptTemplate = this.GetGlobalResourceObject("ZnodeAdminResource", "TextSuccessTransactionRefund").ToString();
                pnlEdit.Visible = false;
                pnlConfirm.Visible = true;
            }
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCouldNotCompleteRequest").ToString());
                sb.Append(resp.ResponseText);
                lblError.Text = sb.ToString();
            }
        }

        /// <summary>
        /// Send mail to user with 
        /// </summary>
        protected void SendStatusMail(int RMAItemId)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = ZNodeConfigManager.SiteConfig.StoreName;
            string ReceiverMailId = "";
            RMARequestAdmin rmaAdmin = new RMARequestAdmin();
            DataSet reportds = rmaAdmin.GetRMARequestReport(RMAItemId);
            if (reportds.Tables[0].Rows.Count > 0)
            {
                string sign = string.Empty;
                if (reportds.Tables[0].Rows[0]["EnableEmailNotification"].ToString() == "True" && reportds.Tables[0].Rows[0]["CustomerNotification"].ToString().Length > 0)
                {
                    sign += "Regards," + "<br />";
                    sign += ZNodeConfigManager.SiteConfig.StoreName + "<br />";
                    sign += reportds.Tables[0].Rows[0]["DepartmentName"].ToString() + "<br />";
                    sign += reportds.Tables[0].Rows[0]["DepartmentAddress"].ToString() + "<br />";
                    sign += reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString() + "<br />";

                    if (reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString().Length > 0)
                        senderEmail = reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString();
                    ReceiverMailId = reportds.Tables[0].Rows[0]["BillingEmailId"].ToString();

                    string messageText = "";


                    messageText = "Dear " + reportds.Tables[0].Rows[0]["CustomerName"].ToString() + "<br />";



                    messageText += "<br />";

                    messageText += reportds.Tables[0].Rows[0]["CustomerNotification"].ToString() + "<br />";

                    messageText += "<br />";
                    messageText += sign;


                    try
                    {
                        // Send mail
                        ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(ReceiverMailId, senderEmail, String.Empty, subject, messageText, true);
                    }
                    catch 
                    {
                        //lblErrorMsg.Text = "Unable to send mail";
                    }
                }
            }
        }


        /// <summary>
        /// Void button clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Void_Click(object sender, EventArgs e)
        {
            // Retrieve order
            ZNode.Libraries.Admin.OrderAdmin orderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            this.order = orderAdmin.GetOrderByOrderID(this.OrderID);

            ZNode.Libraries.Framework.Business.ZNodeEncryption enc = new ZNode.Libraries.Framework.Business.ZNodeEncryption();

            // Get payment settings
            int paymentSettingID = (int)this.order.PaymentSettingID;
            PaymentSettingService pss = new PaymentSettingService();
            PaymentSetting ps = pss.GetByPaymentSettingID(paymentSettingID);

            // Set gateway info
            GatewayInfo gi = new GatewayInfo();
            gi.GatewayLoginID = enc.DecryptData(ps.GatewayUsername);
            gi.GatewayPassword = enc.DecryptData(ps.GatewayPassword);
            gi.TransactionKey = enc.DecryptData(ps.TransactionKey);
            gi.Vendor = ps.Vendor;
            gi.Partner = ps.Partner;
            gi.TestMode = ps.TestMode;
            gi.Gateway = (GatewayType)ps.GatewayTypeID;

            string creditCardExp = Convert.ToString(this.order.CardExp);
            if (creditCardExp == null)
            {
                creditCardExp = string.Empty;
            }

            // Set credit card
            CreditCard cc = new CreditCard();
            cc.Amount = Decimal.Parse(txtAmount.Text);
            cc.CardNumber = txtCardNumber.Text.Trim();
            cc.CreditCardExp = creditCardExp;
            cc.OrderID = this.order.OrderID;
            cc.TransactionID = this.order.CardTransactionID;
            cc.ProcTxnId = this.order.CardTransactionID;
            GatewayResponse resp = new GatewayResponse();

            if ((GatewayType)ps.GatewayTypeID == GatewayType.AUTHORIZE)
            {
                GatewayAuthorize auth = new GatewayAuthorize();
                resp = auth.VoidPayment(gi, cc);
            }
            else if ((GatewayType)ps.GatewayTypeID == GatewayType.VERISIGN)
            {
                GatewayPayFlowPro pp = new GatewayPayFlowPro();
                resp = pp.VoidPayment(gi, cc);
            }
            else if ((GatewayType)ps.GatewayTypeID == GatewayType.PAYMENTECH)
            {
                GatewayOrbital pmt = new GatewayOrbital();
                resp = pmt.ReversePayment(gi, cc);
            }         
            else
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorVoidNoGatewaySupport").ToString();
                return;
            }

            if (resp.IsSuccess)
            {
                // update order status
                // cancelled status
                this.order.OrderStateID = 40;

                // Refund status
                this.order.PaymentStatusID = 4; 

                OrderService os = new OrderService();
                os.Update(this.order);

                pnlEdit.Visible = false;
                pnlConfirm.Visible = true;
               
                pnlEdit.Visible = false;
                pnlConfirm.Visible = true;
            }
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCouldNotCompleteRequest").ToString());
                sb.Append(resp.ResponseText);
                lblError.Text = sb.ToString();
            }
        }

        /// <summary>
        /// Cancel button clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            if (mode == "RMA" && Request.QueryString["flag"] != null)
                Response.Redirect("~/Secure/Orders/ReturnsManagement/RMA/RMARequest.aspx?mode=edit&itemid=" + rmaid + "&orderid=" + OrderID + "&flag=append");
            if (mode == "RMA")
                Response.Redirect("~/Secure/Orders/ReturnsManagement/RMA/RMARequest.aspx?mode=edit&itemid=" + rmaid + "&orderid=" + OrderID);
            Response.Redirect("View.aspx?itemid=" + this.OrderID.ToString());
        }

        #region Bind Methods

        /// <summary>
        /// Bind fields
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.OrderAdmin _OrderAdminAccess = new ZNode.Libraries.Admin.OrderAdmin();
            ZNode.Libraries.DataAccess.Entities.Order order = _OrderAdminAccess.DeepLoadByOrderID(this.OrderID);

            lblCustomerName.Text = order.BillingFirstName + " " + order.BillingLastName;
            lblTotal.Text = order.Total.Value.ToString("c");
            lblTransactionID.Text = order.CardTransactionID;

            txtAmount.Text = ((decimal)order.Total.Value).ToString("N");
            txtAmount.Enabled = false;
            if (order.PaymentSettingIDSource != null && order.PaymentSettingIDSource.GatewayTypeID !=null)
            {
                lblCardNumber.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnTitleEnterCreditCard").ToString();
                this.BindYearList();

                string currentMonth = System.DateTime.Now.Month.ToString();
                if (currentMonth.Length == 1)
                {
                    currentMonth = "0" + currentMonth;
                }

                // Pre-select item in the list
                lstMonth.SelectedValue = currentMonth;
                pnlCreditCardInfo.Visible = true;
            }

            Refund.Visible = this.EnableRefund(order.PaymentSettingIDSource.GatewayTypeID.GetValueOrDefault());
            Void.Visible = this.EnableVoid(order.PaymentSettingIDSource.GatewayTypeID.GetValueOrDefault());
        }
        /// <summary>
        /// Bind RMA data
        /// </summary>
        private void BindRMAData()
        {
            ZNode.Libraries.Admin.OrderAdmin _OrderAdminAccess = new ZNode.Libraries.Admin.OrderAdmin();
            ZNode.Libraries.DataAccess.Entities.Order order = _OrderAdminAccess.DeepLoadByOrderID(this.OrderID);

            lblCustomerName.Text = order.BillingFirstName + " " + order.BillingLastName;
            lblTotal.Text = rmaamount.ToString("c");
            lblTransactionID.Text = order.CardTransactionID;

            txtAmount.Text = rmaamount.ToString("N");
            txtAmount.Enabled = false;

            Refund.Visible = this.EnableRefund(order.PaymentSettingIDSource.GatewayTypeID.GetValueOrDefault());
       
            Void.Visible = this.EnableVoid(order.PaymentSettingIDSource.GatewayTypeID.GetValueOrDefault());
            if (mode == "RMA")
                Void.Visible = false;
        }

        /// <summary>
        /// Binds the expiration year list based on current year
        /// </summary>
        private void BindYearList()
        {
            int currentYear = System.DateTime.Now.Year;
            int counter = 25;

            do
            {
                string itemtext = currentYear.ToString();

                lstYear.Items.Add(new ListItem(itemtext));

                currentYear = currentYear + 1;

                counter = counter - 1;
            } 
            while (counter > 0);
        }
        #endregion

        #region Helper Functions
        /// <summary>
        /// Generate the receipt using XSL Transformation
        /// </summary>
        private void GenerateReceipt()
        {
            string templatePath = Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "IPCReceipt.xsl");

            XmlDocument xmlDoc = new XmlDocument();
            XmlElement rootElement = this.GetElement(xmlDoc, "Order", string.Empty);
            rootElement.AppendChild(this.GetElement(xmlDoc, "ReceiptText", this.ReceiptText));
            rootElement.AppendChild(this.GetElement(xmlDoc, "SiteName", ZNodeConfigManager.SiteConfig.CompanyName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "AccountId", this.order.AccountID.ToString()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "CustomerServiceEmail", ZNodeConfigManager.SiteConfig.CustomerServiceEmail));
            rootElement.AppendChild(this.GetElement(xmlDoc, "CustomerServicePhoneNumber", ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber));

            if (this.order.CardTransactionID != null)
            {
                rootElement.AppendChild(this.GetElement(xmlDoc, "TransactionID", this.order.CardTransactionID));
            }

            rootElement.AppendChild(this.GetElement(xmlDoc, "PromotionCode", this.order.CouponCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "PONumber", this.order.PurchaseOrderNumber));
            rootElement.AppendChild(this.GetElement(xmlDoc, "OrderId", this.order.OrderID.ToString()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "OrderDate", this.order.OrderDate.ToString()));

            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingName", this.order.ShippingIDSource.Description));
            rootElement.AppendChild(this.GetElement(xmlDoc, "PaymentName", string.Empty));

			// Commenting this code as we need to rewrite the code for #16485
			//rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressCompanyName", this.order.ShipCompanyName));
			//rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressFirstName", this.order.ShipFirstName));
			//rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressLastName", this.order.ShipLastName));
			//rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressStreet1", this.order.ShipStreet));
			//rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressStreet2", this.order.ShipStreet1));
			//rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressCity", this.order.ShipCity));
			//rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressStateCode", this.order.ShipStateCode));
			//rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressPostalCode", this.order.ShipPostalCode));
			//rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressCountryCode", this.order.ShipCountry));
			//rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressPhoneNumber", this.order.ShipPhoneNumber));

            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressCompanyName", this.order.BillingCompanyName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressFirstName", this.order.BillingFirstName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressLastName", this.order.BillingLastName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressStreet1", this.order.BillingStreet));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressStreet2", this.order.BillingStreet1));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressCity", this.order.BillingCity));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressStateCode", this.order.BillingStateCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressPostalCode", this.order.BillingPostalCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressCountryCode", this.order.BillingCountry));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressPhoneNumber", this.order.BillingPhoneNumber));

            rootElement.AppendChild(this.GetElement(xmlDoc, "SubTotal", this.order.SubTotal.GetValueOrDefault(0).ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "TaxCost", this.order.TaxCost.GetValueOrDefault(0).ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()));

            rootElement.AppendChild(this.GetElement(xmlDoc, "DiscountAmount", "-" + this.order.DiscountAmount.GetValueOrDefault(0).ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingName", string.Empty));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingCost", this.order.ShippingCost.GetValueOrDefault(0).ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "TotalCost", this.order.Total.GetValueOrDefault(0).ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "AdditionalInstructions", this.order.AdditionalInstructions));

            XmlElement items = xmlDoc.CreateElement("Items");

            foreach (OrderLineItem lineItem in this.order.OrderLineItemCollection)
            {
                XmlElement item = xmlDoc.CreateElement("Item");

                item.AppendChild(this.GetElement(xmlDoc, "Quantity", lineItem.Quantity.GetValueOrDefault(1).ToString()));
                item.AppendChild(this.GetElement(xmlDoc, "Name", lineItem.Name));
                item.AppendChild(this.GetElement(xmlDoc, "SKU", lineItem.SKU));
                item.AppendChild(this.GetElement(xmlDoc, "Description", lineItem.Description));
                item.AppendChild(this.GetElement(xmlDoc, "Note", string.Empty));
                item.AppendChild(this.GetElement(xmlDoc, "Price", lineItem.Price.GetValueOrDefault(0).ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()));

                decimal extPrice = lineItem.Price.GetValueOrDefault(0) * lineItem.Quantity.GetValueOrDefault(1);
                item.AppendChild(this.GetElement(xmlDoc, "Extended", extPrice.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()));

                items.AppendChild(item);
            }

            rootElement.AppendChild(items);
            xmlDoc.AppendChild(rootElement);

            // Use a memorystream to store the result into the memory
            MemoryStream ms = new MemoryStream();

            XslCompiledTransform xsl = new XslCompiledTransform();
            xsl.Load(templatePath);
            xsl.Transform(xmlDoc, null, ms);

            // Move to the begining 
            ms.Seek(0, SeekOrigin.Begin);

            // Pass the memorystream to a streamreader
            StreamReader sr = new StreamReader(ms);

            this._ReceiptTemplate = sr.ReadToEnd();
        }
        
        /// <summary>
        /// Creates an XML Element
        /// </summary>
        /// <param name="xmlDoc">The value of xmlDoc</param>
        /// <param name="elementName">The value of elementName</param>
        /// <param name="elementValue">The value of elementValue</param>
        /// <returns>Returns the XmlElement</returns>
        private XmlElement GetElement(XmlDocument xmlDoc, string elementName, string elementValue)
        {
            XmlElement elmt = xmlDoc.CreateElement(elementName);
            if (elementValue.Length > 0)
            {
                elmt.InnerText = elementValue;
            }
            
            return elmt;
        }

        /// <summary>
        /// Adds double quotes around a string.
        /// </summary>
        /// <param name="strInput">The string to quote</param>
        /// <returns>The quoted string</returns>
        private string CreateQuotedString(string strInput)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\"");
            sb.Append(strInput);
            sb.Append("\"");

            return sb.ToString();
        }

        #endregion
    }
}