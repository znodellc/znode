<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.OrderManagement.ViewOrders.Default" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div>
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>
                    <asp:Localize ID="ViewOrders" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextViewOrders %>'></asp:Localize>
                </h1>
                <p>
                    <asp:Localize ID="TextViewOrders" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextViewOrders %>'></asp:Localize>
                </p>
            </div>
            <div align="left">
                <h4 class="SubTitle">
                    <asp:Localize ID="SearchOrders" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchOrders %>'></asp:Localize>
                </h4>
                <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
                    <div class="SearchForm">
                        <div class="RowStyle">
                            <div class="ItemStyle">
                                <span class="FieldStyle">
                                    <asp:Localize ID="StoreName" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextStoreName %>'></asp:Localize></span><br />
                                <span class="ValueStyle">
                                    <asp:DropDownList ID="ddlPortal" runat="server">
                                    </asp:DropDownList>
                                </span>
                            </div>
                            <div class="RowStyle">
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="OrderID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderID %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtorderid" runat="server" MaxLength="9"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtorderid"
                                            Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredOrderID %>' SetFocusOnError="true"
                                            ValidationExpression="(\d)+" ValidationGroup="grpReports" CssClass="Error"></asp:RegularExpressionValidator>
                                    </span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="FirstName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFirstName %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtfirstname" runat="server"></asp:TextBox></span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="LastName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLastName %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtlastname" runat="server"></asp:TextBox></span>
                                </div>
                            </div>
                            <div class="RowStyle">
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="CompanyName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCompanyName %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtcompanyname" runat="server"></asp:TextBox></span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="AccountID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAccountID %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtaccountnumber" runat="server" MaxLength="9"></asp:TextBox><br />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtaccountnumber"
                                            Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidNumber %>' SetFocusOnError="true"
                                            ValidationExpression="(\d)+" ValidationGroup="grpReports" CssClass="Error"></asp:RegularExpressionValidator></span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="OrderStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderStatus %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:DropDownList ID="ListOrderStatus" runat="server">
                                        </asp:DropDownList>
                                    </span>
                                </div>
                            </div>
                            <div class="RowStyle">
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="BeginDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBeginDate %>'></asp:Localize></span>
                                    <br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtStartDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="imgbtnStartDt"
                                            runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" /><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredBeginDate %>'
                                            ControlToValidate="txtStartDate" ValidationGroup="grpReports" CssClass="Error"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidDate %>'
                                            ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                                            ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                                        <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                                            runat="server" TargetControlID="txtStartDate">
                                        </ajaxToolKit:CalendarExtender>
                                    </span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="EndDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEndDate %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtEndDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="ImgbtnEndDt"
                                            runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" /><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEndDate"
                                            ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEndDate %>' ValidationGroup="grpReports" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
                                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidDate %>'
                                            ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                                            ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                                        <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt"
                                            runat="server" TargetControlID="txtEndDate">
                                        </ajaxToolKit:CalendarExtender>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
                                            ValidationGroup="grpReports"
                                            ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareBeginDate %>'
                                            Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                                    </span>
                                </div>
                            </div>
                            <div class="ClearBoth">
                                <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClearSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                                <zn:Button runat="server" ID="btnSearch" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' ValidationGroup="grpReports" />
                         </div>
                        </div>
                    </div>
                </asp:Panel>
                <br />

            </div>
            <div>
                <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
            </div>
            <div>
                <h4 class="SubTitle" style="margin-bottom: -8px;">
                    <asp:Localize ID="OrderList" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleOrderList %>'></asp:Localize>
                </h4>
                <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundOrders %>'
                    EnableSortingAndPagingCallbacks="False" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                    OnRowCommand="UxGrid_RowCommand" PageSize="25" Width="100%" OnRowDataBound="uxGrid_RowDataBound">
                    <Columns>
                        <asp:HyperLinkField DataNavigateUrlFields="orderid" DataNavigateUrlFormatString="view.aspx?itemid={0}"
                            DataTextField="orderid" HeaderText="ID" SortExpression="orderid" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, TextStoreName %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# GetStoreName(DataBinder.Eval(Container.DataItem,"PortalID").ToString()) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="OrderStateID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderStatus %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="OrderStatus" Text='<%# Eval("OrderStatus") %>'
                                    runat="server" Font-Bold="true" Font-Size="Smaller"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePaymentStatus %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="PaymentStatus" Text='<%# Eval("PaymentStatusName") %>'
                                    runat="server" Font-Bold="true" Font-Size="Smaller"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="BillingFirstName" HeaderStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                                <asp:Label ID="headerTotal" Text='<%$ Resources:ZnodeAdminResource, TitleName %>' runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="CustomerName" Text='<%# ReturnName(Eval("BillingFirstName"),Eval("BillingLastName")) %>'
                                    runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Orderdate" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDate %>' SortExpression="OrderDate" ItemStyle-Width="120"
                            DataFormatString="{0:MM/dd/yyyy hh:mm tt}" HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField SortExpression="total" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAmount %>'  HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "total","{0:c}").ToString()%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="PaymentTypeID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePaymentType %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="PaymentType" Text='<%# Eval("PaymentTypeName") %>'
                                    runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnActions %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="220px">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CausesValidation="False" ID="ViewOrder" Text='<%$ Resources:ZnodeAdminResource, LinkManageOrder %>'
                                    CommandArgument='<%# Eval("orderid") %>' CommandName="ViewOrder" />
                                <asp:LinkButton runat="server" Visible='<%# EnableCapture(Eval("PaymentTypeID"), Eval("PaymentStatusID")) %>'
                                    CausesValidation="false" ID="Capture" Text='<%$ Resources:ZnodeAdminResource, LinkCapture %>' CommandArgument='<%# Eval("orderid") %>'
                                    CommandName="Capture" />
                                <%-- <asp:LinkButton runat="server" CausesValidation="false" ID="ChangeStatus" Text="Status &raquo"
                                    CommandArgument='<%# Eval("orderid") %>' CommandName="Status" />--%>
                                <asp:LinkButton runat="server" CausesValidation="false" ID="RMA" Text='<%$ Resources:ZnodeAdminResource, LinkRma %>' Visible='<%# EnableRMA(Eval("orderid"),Eval("OrderStatus"),Eval("Orderdate"),Eval("PaymentSettingId")) %>'
                                    CommandArgument='<%# Eval("orderid") %>' CommandName="RMA" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </div>
            <div align="left">
                <h4 class="SubTitle">
                    <asp:Localize ID="DownloadOrders" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDownloadOrders %>'></asp:Localize>
                </h4>
                <div align="left">
                    <div>
                        <div class="FieldStyle">
                            <asp:Localize ID="StartingOrderID" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextStartingOrderID %>'></asp:Localize>
                        </div>
                        <small>
                            <asp:Localize ID="HigherOrderID" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextHigherOrderID %>'></asp:Localize></small>
                    </div>
                    <div>
                        <asp:TextBox ID="OrderNumber" runat="server" MaxLength="9"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="OrderNumber"
                            ValidationGroup="Download" CssClass="Error" Display="Dynamic" ErrorMessage="You must specify a starting Order ID"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="OrderNumber"
                            ValidationGroup="Download" CssClass="Error" Display="Dynamic" ErrorMessage="Enter Valid starting Order ID"
                            ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    </div>
                    <div>
                        <uc1:Spacer ID="Spacer2" runat="server" SpacerHeight="10" />
                    </div>
                    <div>
                        <asp:Label ID="lblError" runat="server" Text="" CssClass="Error"></asp:Label>
                        <div class="Error">
                            <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
                        </div>

                        <zn:Button ID="ButDownload" runat="server" Width="175px" ButtonType="EditButton" OnClick="ButDownload_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonOrdersToCSV%>' CssClass="Size175" ValidationGroup="Download" />&nbsp;&nbsp;
                        <zn:Button ID="ButOrderLineItems" runat="server" Width="175px" ButtonType="EditButton" OnClick="ButOrderLineItemsDownload_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonOrderLineItems%>' CssClass="Size175" ValidationGroup="Download" /><br />
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
