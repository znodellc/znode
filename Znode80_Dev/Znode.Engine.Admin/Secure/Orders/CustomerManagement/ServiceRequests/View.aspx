<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.CustomerManagement.ServiceRequests.View" ValidateRequest="false" Codebehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div  class="FormView">
        <div style="float: right">
            <zn:Button runat="server" ID="CaseList" OnClick="CaseList_Click " ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, LinkBack %>' Width="100px" />
            <zn:Button runat="server" ID="EditCase" OnClick="CaseEdit_Click" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonEditCase %>' Width="100px" />
            <zn:Button runat="server" ID="ReplyToCase" OnClick="ReplyToCase_Click" Width="170px" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonReplyToCustomer %>' />
        </div>
        <div class="LeftFloat">
            <h1>
                <asp:Localize ID="ServiceRequest" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleServiceRequest %>'></asp:Localize>
                <asp:Label ID="lblTitle" runat="server" Text="Label" /></h1>
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="GeneralInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralInformation %>'></asp:Localize></h4>
        <div class="ServiceRequest">
            <div class="FieldStyleA"><asp:Localize ID="TitleColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitle %>'></asp:Localize></div>
            <div class="ValueStyleA">
                <asp:Label ID="lblCaseTitle" runat="server" Text="Label"></asp:Label>
            </div>
            <div class="FieldStyle"><asp:Localize ID="Status" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStatus %>'></asp:Localize></div>
            <div class="ValueStyle">
                <asp:Label ID="lblCaseStatus" runat="server" Text="Label"></asp:Label>
            </div>
            <div class="FieldStyleA"><asp:Localize ID="Priority" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPriority %>'></asp:Localize></div>
            <div class="ValueStyleA">
                <asp:Label ID="lblCasePriority" runat="server" Text="Label"></asp:Label>
            </div>
            <div class="FieldStyle"><asp:Localize ID="Message" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMessage %>'></asp:Localize></div>
            <div class="ValueStyle">
                <asp:Label runat="server" ID="lblCaseDescription" Width="519px"></asp:Label>
            </div>
            <div class="FieldStyleA"><asp:Localize ID="CreatedDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCreatedDate %>'></asp:Localize></div>
            <div class="ValueStyleA">
                <asp:Label ID="lblCreatedDate" runat="server"></asp:Label>
            </div>
        </div>
        <h4 class="SubTitle">
            <asp:Localize runat="server" ID="CustomerInformation" Text='<%$ Resources:ZnodeAdminResource, SubTitleCustomerInformation %>'></asp:Localize>
        </h4>
       <div class="ServiceRequest">
            <div class="FieldStyleA"><asp:Localize ID="FirstName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFirstName %>'></asp:Localize></div>
            <div class="ValueStyleA"><asp:Label ID="lblFirstName" runat="server" Text="Label"></asp:Label>
            </div>
            <div class="FieldStyle"><asp:Localize ID="LastName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLastName %>'></asp:Localize></div>
            <div class="ValueStyle"><asp:Label ID="lblLastName" runat="server" Text="Label"></asp:Label>
            </div>
            <div class="FieldStyleA"><asp:Localize ID="CompanyName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCompanyName %>'></asp:Localize></div>
            <div class="ValueStyleA"><asp:Label ID="lblCompanyName" runat="server" Text="Label"></asp:Label>&nbsp;
            </div>
            <div class="FieldStyle"><asp:Localize ID="EmailID" runat="server" Text='<%$ Resources:ZnodeAdminResource,  ColumnEmailID %>'></asp:Localize></div>
            <div class="ValueStyle"><asp:Label ID="lblEmailID" runat="server" Text="Label"></asp:Label>
            </div>
            <div class="FieldStyleA"><asp:Localize ID="PhoneNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPhoneNumber %>'></asp:Localize></div>
            <div class="ValueStyleA"><asp:Label ID="lblPhoneNumber" runat="server" Text="Label"></asp:Label>&nbsp;
            </div>
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="Notes" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleNotes %>'></asp:Localize>
        </h4>
        <div style="text-align: right">
            <zn:Button Width="80px" runat="server" ID="AddNewNote" OnClick="AddNewNote_Click" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonAddNote %>' />
        </div>
        <div class="ServiceRequest">
        <asp:Repeater ID="CustomerNotes" runat="server">
            <ItemTemplate>
            <div class="NotesA"><asp:Label ID="Label1"  runat="Server" Text='<%# FormatCustomerNote(Eval("NoteTitle"),Eval("CreateUser"),Eval("CreateDte")) %>' />
               </div><br />
            <div class="NotesB"><asp:Label ID="Label2"  runat="Server" Text='<%# Eval("NoteBody") %>' /></div>
                <div>
                <uc1:Spacer ID="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <br />      
            </ItemTemplate>
        </asp:Repeater>
       </div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
