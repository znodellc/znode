<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.CustomerManagement.ServiceRequests.AddNote" ValidateRequest="false" Title="Untitled Page"
    CodeBehind="AddNote.aspx.cs" %>

<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div style="float: right">
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="True" ID="btnSubmitTop" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" ID="btnCancelTop"/>
        </div>
        <div class="LeftFloat">
            <h1><asp:Label ID="lblHeading" runat="server" Text="Label"></asp:Label>
            </h1>
        </div>
        <div class="ClearBoth">
        </div>
        <div class="FormView">
            <div class="FieldStyle">
                <asp:Localize ID="TitleColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitle %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='txtNoteTitle' runat='server' MaxLength="50" Columns="20"></asp:TextBox>
					<asp:Label runat="server" ID="lblNoteTitle" CssClass="Error" Visible="False"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="NoteColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnNote %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
				<uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
				<asp:Label runat="server" ID="lblHtmlTextError" CssClass="Error" Visible="False"></asp:Label>
            </div>
            <div class="ClearBoth">
                <asp:Label ID="lblError" CssClass="Error" ForeColor="DarkRed" runat="server" Text="Label" Visible="False"></asp:Label>
            </div>
        </div>
        <div>
            <zn:Button runat="server" ID="btnCancel" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnSubmit" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
    </div>
</asp:Content>
