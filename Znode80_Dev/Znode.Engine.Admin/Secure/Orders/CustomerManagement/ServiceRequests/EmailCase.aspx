<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.CustomerManagement.ServiceRequests.EmailCase"
    ValidateRequest="false" CodeBehind="EmailCase.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">

    <div>
        <asp:Label ID="lblErrorMessage" runat="server" CssClass="Error"></asp:Label><br /><br />
    </div>

    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1><asp:Localize ID="Localize11" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleReplyToCustomer %>'></asp:Localize></h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ID="btnSubmitTop" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancelTop" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
        <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>

        <h4 class="SubTitle"><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCustomerInformation %>'></asp:Localize></h4>
        <div class="FormView">
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle"><asp:Localize ID="FullName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFullName %>'></asp:Localize></span><span class="ValueStyle">
                        <asp:Label ID="lblCustomerName" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>
            <div class="AlternatingRowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle"><asp:Localize ID="CompanyName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCompanyName %>'></asp:Localize></span><span class="ValueStyle">
                        <asp:Label ID="lblCompanyName" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle"><asp:Localize ID="EmailID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEmailID %>'></asp:Localize></span><span class="ValueStyle">
                        <asp:Label ID="lblEmailTo" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>
            <div class="AlternatingRowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle"><asp:Localize ID="PhoneNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPhoneNumber %>'></asp:Localize></span><span class="ValueStyle">
                        <asp:Label ID="lblPhoneNumber" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>
        </div>
        <h4 class="SubTitle"><asp:Localize ID="EmailServiceRequest" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleEmailServiceRequest %>'></asp:Localize>
        </h4>

        <div class="FormView">
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle"><asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitle %>' ></asp:Localize></span> <span class="ValueStyle">
                        <asp:Label ID="lblCaseTitle" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle"> <asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>'  ></asp:Localize></span> <span class="ValueStyle">
                        <asp:Label ID="lblCaseStatus" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle"><asp:Localize ID="Localize5" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPriority %>' ></asp:Localize> </span><span class="ValueStyle">
                        <asp:Label ID="lblCasePriority" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>

            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle"><asp:Localize ID="Localize9" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMessage %>' ></asp:Localize></span><span class="ValueStyle">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtCaseDescription" Height="59px"
                            TextMode="MultiLine" Width="600px" />
                    </span>
                </div>
            </div>
        </div>

        <h4 class="SubTitle"><asp:Localize ID="ComposeEmail" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleComposeEmail %>'></asp:Localize>
        </h4>
        <div class="FormView">
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle"><asp:Localize ID="EmailSubject" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEmailSubject %>'></asp:Localize></span><span class="ValueStyle">
                        <asp:TextBox ID="txtEmailSubj" runat="server" Width="279px" />
                    </span>
                </div>
            </div>
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle"><asp:Localize ID="EmailMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEmailMessage %>'></asp:Localize></span><span class="ValueStyle">
                        <uc1:HtmlTextBox ID="ctrlHtmlText" Mode="1" runat="server"></uc1:HtmlTextBox>
                    </span>
                </div>
            </div>
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span valign="top" class="FieldStyle"><asp:Localize ID="Attachments" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEmailAttachments %>'></asp:Localize></span><span class="ValueStyle">
                        <input id="FileBrowse" type="file" size="47" name="File1" runat="server" tabindex="8" />
                    </span>
                </div>
            </div>
        </div>
        <asp:Label ID="lblEmailid" runat="server" Visible="false" />
        <div class="ClearBoth">
            <zn:Button runat="server" ID="btnSubmitBottom" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancelBottom" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
        <asp:HiddenField ID="txtAccountId" runat="server" />
    </div>
</asp:Content>
