using System;
using System.Data;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Orders.CustomerManagement.ServiceRequests
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_sales_cases_list class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string AddLink = "~/Secure/Orders/CustomerManagement/ServiceRequests/Add.aspx";        
        private string ViewLink = "~/Secure/Orders/CustomerManagement/ServiceRequests/view.aspx?itemid=";                
        private string portalIds = string.Empty;
        private DataSet MyDataSet = null;
        #endregion

        #region Protected properties
        /// <summary>
        /// Gets or sets the GridViewSortDirection
        /// </summary>
        private string GridViewSortDirection
        {
            get 
            { 
                return ViewState["SortDirection"] as string ?? "ASC"; 
            }

            set 
            { 
                ViewState["SortDirection"] = value; 
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get Portals 
            this.portalIds = UserStoreAccess.GetAvailablePortals;

            if (!Page.IsPostBack)
            {
                this.BindPortal();
                this.BindList();
                this.BindSearchData();
                
                if (this.portalIds.Length == 0)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorServiceRequest").ToString();
                    btnAdd.Enabled = false;
                    uxGrid.Visible = false;
                    return;
                }
            }
        }

        /// <summary>
        /// Sort Data Table Method
        /// </summary>
        /// <param name="dataSet">The value of DataSet to be sorted</param>
        /// <param name="gridViewSortExpression">The value of gridViewSortExpression</param>
        /// <param name="isPageIndexChanging">The value of isPageIndexChanging</param>
        /// <returns>Returns the sorted Data View</returns>
        protected DataView SortDataTable(DataSet dataSet, string gridViewSortExpression, bool isPageIndexChanging)
        {
            if (dataSet != null)
            {
                DataView dataView = new DataView(dataSet.Tables[0]);

                if (gridViewSortExpression.Length > 0)
                {
                    if (isPageIndexChanging)
                    {
                        dataView.Sort = string.Format("{0} {1}", gridViewSortExpression, this.GridViewSortDirection);
                    }
                    else
                    {
                        dataView.Sort = string.Format("{0} {1}", gridViewSortExpression, this.GetSortDirection());
                    }
                }
                
                return dataView;
            }
            else
            {
                return new DataView();
            }
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Clear Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            txtcaseid.Text = string.Empty;
            txtfirstname.Text = string.Empty;
            txtlastname.Text = string.Empty;
            txtcompanyname.Text = string.Empty;
            txttitle.Text = string.Empty;
            ddlPortal.SelectedIndex = 0;
            this.BindList();
            this.BindSearchData();
        }

        /// <summary>
        /// Add Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddLink);
        }
       
        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // Get the values from the appropriate 
                // Cell in the GridView control.
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "View")
                {
                    Response.Redirect(this.ViewLink + Id);
                }
            }
        }
        
        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        /// <summary>
        /// Occurs when the hyperlink to sort a column is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            CaseAdmin _CaseAdminAccess = new CaseAdmin();
            if (Roles.IsUserInRole("ADMIN"))
            {
                this.MyDataSet = _CaseAdminAccess.SearchCase(int.Parse(ListCaseStatus.SelectedValue), txtcaseid.Text.Trim(), txtfirstname.Text.Trim(), txtlastname.Text.Trim(), txtcompanyname.Text.Trim(), txttitle.Text.Trim(), Convert.ToInt32(ddlPortal.SelectedValue), this.portalIds);
            }
            else
            {
                this.MyDataSet = _CaseAdminAccess.SearchCase(int.Parse(ListCaseStatus.SelectedValue), txtcaseid.Text.Trim(), txtfirstname.Text.Trim(), txtlastname.Text.Trim(), txtcompanyname.Text.Trim(), txttitle.Text.Trim(), ZNodeConfigManager.SiteConfig.PortalID, this.portalIds);
            }

            uxGrid.DataSource = this.SortDataTable(this.MyDataSet, e.SortExpression, true);
            uxGrid.DataBind();

            if (this.GetSortDirection() == "DESC")
            {
                e.SortDirection = SortDirection.Descending;
            }
            else
            {
                e.SortDirection = SortDirection.Ascending;
            }
        }

        protected void uxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            foreach (GridViewRow gvC in uxGrid.Rows)
            {
                Label lblCaseStatus = (Label)gvC.FindControl("lblCaseStatus");
                if (lblCaseStatus.Text == this.GetGlobalResourceObject("ZnodeAdminResource", "SubTextPending").ToString())
                {
                    lblCaseStatus.ForeColor = System.Drawing.Color.Red;
                }
                lblCaseStatus.Text = lblCaseStatus.Text.ToUpper();
            }
        }
        #endregion

        #region Bind Data

        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                if (profiles.StoreAccess != string.Empty)
                {
                    portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
                }
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = this.GetGlobalResourceObject("ZnodeAdminResource","DropDownTextAllStores").ToString().ToUpper();
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
        }

        /// <summary>
        /// Bind Searched Data
        /// </summary>
        private void BindSearchData()
        {
            CaseAdmin _CaseAdminAccess = new CaseAdmin();
            this.MyDataSet = _CaseAdminAccess.SearchCase(int.Parse(ListCaseStatus.SelectedValue), txtcaseid.Text.Trim(), Server.HtmlEncode(txtfirstname.Text.Trim()), Server.HtmlEncode(txtlastname.Text.Trim()), Server.HtmlEncode(txtcompanyname.Text.Trim()), Server.HtmlEncode(txttitle.Text.Trim()), Convert.ToInt32(ddlPortal.SelectedValue), this.portalIds);

            DataView dv = new DataView(this.MyDataSet.Tables[0]);
            dv.Sort = "CaseID Desc";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Grid Sort Direction Method
        /// </summary>
        /// <returns>Returns the string</returns>
        private string GetSortDirection()
        {
            switch (this.GridViewSortDirection)
            {
                case "ASC":
                    this.GridViewSortDirection = "DESC";
                    break;

                case "DESC":
                    this.GridViewSortDirection = "ASC";
                    break;
            }
            
            return this.GridViewSortDirection;
        }

        /// <summary>
        /// Bind List Method
        /// </summary>
        private void BindList()
        {
            CaseAdmin _AdminAccess = new CaseAdmin();
            ListCaseStatus.DataSource = _AdminAccess.GetAllCaseStatus();
            ListCaseStatus.DataTextField = "CaseStatusNme";
            ListCaseStatus.DataValueField = "CaseStatusID";
            ListCaseStatus.DataBind();
            ListItem newItem = new ListItem();
            newItem.Text = this.GetGlobalResourceObject("ZnodeAdminResource","DropdownTextAll").ToString();
            newItem.Value = "-1";
            ListCaseStatus.Items.Insert(0, newItem);
            ListCaseStatus.SelectedIndex = 1;
        }

        #endregion
    }
}