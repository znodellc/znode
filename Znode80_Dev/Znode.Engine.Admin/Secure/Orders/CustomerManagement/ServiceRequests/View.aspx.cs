using System;
using System.Text;
using System.Web.UI;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Orders.CustomerManagement.ServiceRequests
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_sales_cases_view class
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private string ListLink = "~/Secure/Orders/CustomerManagement/ServiceRequests/Default.aspx";
        private string EditLink = "~/Secure/Orders/CustomerManagement/ServiceRequests/add.aspx?itemid=";
        private string EmailLink = "~/Secure/Orders/CustomerManagement/ServiceRequests/EmailCase.aspx?itemid=";
        private string AddNoteLink = "~/Secure/Orders/CustomerManagement/ServiceRequests/AddNote.aspx?itemid=";
        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                if (this.ItemId > 0)
                {
                    this.BindNotes();
                    this.BindValues();
                }
            }
        }

        /// <summary>
        /// Add New Note Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddNewNote_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddNoteLink + this.ItemId.ToString());
        }

        /// <summary>
        /// Case List Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CaseList_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListLink);
        }

        /// <summary>
        /// Edit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CaseEdit_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.EditLink + this.ItemId.ToString());
        }

        /// <summary>
        /// Reply to Customer Button  Click Event 
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ReplyToCase_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.EmailLink + this.ItemId);
        }
        
        /// <summary>
        /// Format Note Description 
        /// </summary>
        /// <param name="field1">The value of field1</param>
        /// <param name="field2">The value of field2</param>
        /// <param name="field3">The value of field3</param>
        /// <returns>Returns the Formatted Note Description</returns>
        protected string FormatCustomerNote(object field1, object field2, object field3)
        {
            StringBuilder Build = new StringBuilder();
            Build.Append(field1);
            Build.Append(" " + this.GetGlobalResourceObject("ZnodeAdminResource","Hyphen").ToString() + " ");
            Build.Append(field2);
            Build.Append(this.GetGlobalResourceObject("ZnodeAdminResource","ColumnOn").ToString());
            Build.Append(field3);
            return Build.ToString();
        }

        /// <summary>
        /// //Bind Repeater
        /// </summary>
        protected void BindNotes()
        {
            NoteAdmin _NoteAdmin = new NoteAdmin();
            TList<Note> note = _NoteAdmin.GetByCaseID(this.ItemId);
            note.Sort("CreateDte Desc");
            CustomerNotes.DataSource = note;
            CustomerNotes.DataBind();
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Bind Values Method
        /// </summary>
        private void BindValues()
        {
            CaseAdmin _CaseAdmin = new CaseAdmin();
            CaseRequest _CaseList = _CaseAdmin.GetByCaseID(this.ItemId);

            if (_CaseList != null)
            {
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = profiles.StoreAccess.Split(',');
                    Array Stores = (Array)stores;
                    int found = Array.IndexOf(Stores, _CaseList.PortalID.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("Default.aspx", true);
                    }
                }

                // Set General Case Information
                lblCreatedDate.Text = _CaseList.CreateDte.ToShortDateString();
                lblTitle.Text = _CaseList.Title;
                lblCaseTitle.Text = _CaseList.Title;
                lblCaseStatus.Text = this.GetCaseStatusByCaseID(_CaseList.CaseStatusID);
                if (lblCaseStatus.Text == "Pending")
                {
                    lblCaseStatus.ForeColor = System.Drawing.Color.Red;
                }
                lblCasePriority.Text = this.GetCasePriorityByCaseID(_CaseList.CasePriorityID);
                lblCaseDescription.Text =Server.HtmlDecode(_CaseList.Description);

                // Set Customer Information
                lblFirstName.Text = _CaseList.FirstName;
                lblLastName.Text= _CaseList.LastName;
                lblCompanyName.Text = _CaseList.CompanyName;
                lblEmailID.Text = _CaseList.EmailID;
                lblPhoneNumber.Text = _CaseList.PhoneNumber;
            }
        }

        /// <summary>
        /// Get Acccount Type By Account ID
        /// </summary>
        /// <param name="fieldValue">The value of fieldValue</param>
        /// <returns>Returns the Account Type</returns>
        private string GetAccountTypeByAccountID(object fieldValue)
        {
            if (fieldValue == null)
            {
                return string.Empty;
            }
            else
            {
                CaseAdmin _CaseAdmin = new CaseAdmin();
                AccountType _AccountTypeList = _CaseAdmin.GetByAccountTypeID(int.Parse(fieldValue.ToString()));

                if (_AccountTypeList != null)
                {
                    return _AccountTypeList.AccountTypeNme;
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        /// <summary>
        /// Get Case Status By CaseID
        /// </summary>
        /// <param name="fieldValue">The value of fieldValue</param>
        /// <returns>The value of Case status</returns>
        private string GetCaseStatusByCaseID(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                CaseAdmin _CaseStatusAdmin = new CaseAdmin();
                CaseStatus _caseStatusList = _CaseStatusAdmin.GetByCaseStatusID(int.Parse(fieldValue.ToString()));
                if (_caseStatusList == null)
                {
                    return string.Empty;
                }
                else
                {
                    return _caseStatusList.CaseStatusNme;
                }
            }
        }

        /// <summary>
        /// Get Case Priority By CaseID
        /// </summary>
        /// <param name="fieldValue">The value of fieldvalue</param>
        /// <returns>Returns the case priority</returns>
        private string GetCasePriorityByCaseID(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                CaseAdmin _CasePriorityAdmin = new CaseAdmin();
                CasePriority _CasePriority = _CasePriorityAdmin.GetByCasePriorityID(int.Parse(fieldValue.ToString()));
                if (_CasePriority == null)
                {
                    return string.Empty;
                }
                else
                {
                    return _CasePriority.CasePriorityNme;
                }
            }
        }
        #endregion
    }
}