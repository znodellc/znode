using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Orders.CustomerManagement.ServiceRequests
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_sales_cases_case_email class
    /// </summary>
    public partial class EmailCase : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private string RedirectLink = "~/Secure/Orders/CustomerManagement/ServiceRequests/Default.aspx";
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }
            
            if (!Page.IsPostBack)
            {
                this.BindData();
                if (this.ItemId > 0)
                {
                    this.BindValues();
                }
            }
        }

        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            string strFileName = string.Empty;
            int portalID = 0;
            if (Session["PortalID"] != null)
            {
                portalID = int.Parse(Session["PortalID"].ToString());
            }
            
            MailMessage EmailContent = new MailMessage(ZNodeConfigManager.SiteConfig.AdminEmail, lblEmailid.Text);
            EmailContent.Subject = txtEmailSubj.Text;
            EmailContent.Body = ctrlHtmlText.Html.ToString();
            EmailContent.IsBodyHtml = true;

            // Attachment Steps
            // Get the file name 
            strFileName = Path.GetFileName(FileBrowse.PostedFile.FileName);

            if (!strFileName.Equals(string.Empty))
            {
                // Email Attachment
                Attachment attach = new Attachment(FileBrowse.PostedFile.InputStream, strFileName);

                // Attach the created email attachment 
                EmailContent.Attachments.Add(attach);
            }

            ZNodeEncryption encrypt = new ZNodeEncryption();

            string SMTPServer = string.Empty;
            string SMTPUsername = string.Empty;
            string SMTPPassword = string.Empty;

            // To get the SMTP settings based on PortalID
            ZNodeConfigManager.AliasSiteConfig(portalID);

            if (ZNodeConfigManager.SiteConfig.SMTPServer != null)
            {
                SMTPServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            }
            
            if (ZNodeConfigManager.SiteConfig.SMTPUserName != null)
            {
                SMTPUsername = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.SMTPUserName);
            }
            
            if (ZNodeConfigManager.SiteConfig.SMTPPassword != null)
            {
                SMTPPassword = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.SMTPPassword);
            }

            try
            {
                // Create mail client and send email
                System.Net.Mail.SmtpClient emailClient = new System.Net.Mail.SmtpClient();
                emailClient.Host = SMTPServer;
                emailClient.Credentials = new NetworkCredential(SMTPUsername, SMTPPassword);

                // Send MailContent
                emailClient.Send(EmailContent);

                EmailContent.Attachments.Dispose();

                /* Delete the attachements if any */
                if (strFileName != null && strFileName != string.Empty)
                {
                    File.Delete(Server.MapPath(strFileName));
                }

                NoteAdmin _Noteadmin = new NoteAdmin();
                Note _NoteAccess = new Note();

                _NoteAccess.CaseID = this.ItemId;

                if (txtAccountId.Value.Length > 0)
                {
                    _NoteAccess.AccountID = int.Parse(txtAccountId.Value);
                }

                _NoteAccess.CreateDte = System.DateTime.Now;
                _NoteAccess.CreateUser = HttpContext.Current.User.Identity.Name;
                _NoteAccess.NoteTitle = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEmailReplySubjectTo").ToString() + lblCaseTitle.Text.Trim() + ")";
                _NoteAccess.NoteBody = ctrlHtmlText.Html;

                bool Check = _Noteadmin.Insert(_NoteAccess);

                // Log Activity
                AccountAdmin _accountAdmin = new AccountAdmin();
                Account _account = new Account();
                if (txtAccountId.Value.Length > 0)
                {
                    _account = _accountAdmin.GetByAccountID(int.Parse(txtAccountId.Value));
                    MembershipUser _user = Membership.GetUser(_account.UserID);
                    string UserName = _user.UserName;
                    string AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEmailReply").ToString(), UserName);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, UserName);
                }
               
                Response.Redirect(this.RedirectLink);
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorEmailReply").ToString();

                // Log exception
                ExceptionPolicy.HandleException(ex, this.GetGlobalResourceObject("ZnodeAdminResource", "TextZnodeGlobalExceptionPolicy").ToString());
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.RedirectLink);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Bind Data Method
        /// </summary>
        private void BindData()
        {
            CaseAdmin _CaseAdmin = new CaseAdmin();
            CaseRequest _CaseList = _CaseAdmin.GetByCaseID(this.ItemId);

            if (_CaseList != null)
            {
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = profiles.StoreAccess.Split(',');
                    Array Stores = (Array)stores;
                    int found = Array.IndexOf(Stores, _CaseList.PortalID.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("Default.aspx", true);
                    }
                }

                txtEmailSubj.Text = Server.HtmlDecode(_CaseList.Title);
                lblEmailid.Text = _CaseList.EmailID;
                Session.Add("PortalID", _CaseList.PortalID);
            }
        }

        /// <summary>
        /// Bind Values Method
        /// </summary>
        private void BindValues()
        {
            CaseAdmin _CaseAdmin = new CaseAdmin();
            CaseRequest _CaseList = _CaseAdmin.GetByCaseID(this.ItemId);

            if (_CaseList != null)
            {
                // Set General Case Information     
                if (_CaseList.AccountID.HasValue)
                {
                    txtAccountId.Value = _CaseList.AccountID.Value.ToString();
                }

                lblCaseTitle.Text = _CaseList.Title;
                lblCaseStatus.Text = this.GetCaseStatusByCaseID(_CaseList.CaseStatusID);
                lblCasePriority.Text = this.GetCasePriorityByCaseID(_CaseList.CasePriorityID);
                txtCaseDescription.Text = Server.HtmlDecode(_CaseList.Description).Replace("<br>", "\r\n");

                // Set Customer Information
                lblCustomerName.Text = _CaseList.FirstName + " " + _CaseList.LastName;
                lblCompanyName.Text = _CaseList.CompanyName;
                lblEmailTo.Text = _CaseList.EmailID;
                lblPhoneNumber.Text = _CaseList.PhoneNumber;
            }
        }
       
        /// <summary>
        /// Get AccountType By AccountID
        /// </summary>
        /// <param name="fieldValue">The value of fieldvalue</param>
        /// <returns>Returns the AccountID</returns>
        private string GetAccountTypeByAccountID(object fieldValue)
        {
            if (fieldValue == null)
            {
                return string.Empty;
            }
            else
            {
                CaseAdmin _CaseAdmin = new CaseAdmin();
                AccountType _AccountTypeList = _CaseAdmin.GetByAccountTypeID(int.Parse(fieldValue.ToString()));

                if (_AccountTypeList != null)
                {
                    return _AccountTypeList.AccountTypeNme;
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        /// <summary>
        /// Get Case Status By CaseID
        /// </summary>
        /// <param name="fieldValue">The value of fieldvalue</param>
        /// <returns>Returns the Case Status</returns>
        private string GetCaseStatusByCaseID(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                CaseAdmin _CaseStatusAdmin = new CaseAdmin();
                CaseStatus _caseStatusList = _CaseStatusAdmin.GetByCaseStatusID(int.Parse(fieldValue.ToString()));
                if (_caseStatusList == null)
                {
                    return string.Empty;
                }
                else
                {
                    return _caseStatusList.CaseStatusNme;
                }
            }
        }

        /// <summary>
        /// Get Case Priority By CaseID
        /// </summary>
        /// <param name="fieldValue">The value of fieldValue</param>
        /// <returns>Returns the Case Priority</returns>
        private string GetCasePriorityByCaseID(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                CaseAdmin _CasePriorityAdmin = new CaseAdmin();
                CasePriority _CasePriority = _CasePriorityAdmin.GetByCasePriorityID(int.Parse(fieldValue.ToString()));
                if (_CasePriority == null)
                {
                    return string.Empty;
                }
                else
                {
                    return _CasePriority.CasePriorityNme;
                }
            }
        }
        #endregion
    }
}