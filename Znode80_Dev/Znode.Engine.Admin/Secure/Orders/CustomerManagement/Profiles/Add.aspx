<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" ValidateRequest="false" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.CustomerManagement.Profiles.Add" CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/spacer.ascx" TagName="Spacer" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ID="btnSubmitTop" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancelTop" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
        <div class="ClearBoth">
            <asp:Label ID="lblErrorMsg" CssClass="Error" runat="server"></asp:Label>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ProfileNameColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProfile %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="ProfileName" runat="server" Columns="25" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ProfileName"
                Display="Dynamic" CssClass="Error" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProfileName %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ProfileCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProfileCode %>'></asp:Localize><br />
            <small><asp:Localize ID="ProfileCodeText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextProfileCode %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="ExternalAccountNum" runat="server" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
           <asp:Localize ID="Weighting" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleWeighting %>'></asp:Localize><span class="Asterix">*</span><br />
            <small><asp:Localize ID="WeightingText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextWeighting %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtWeighting" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" CssClass="Error" runat="server" Display="Dynamic"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredWeighting %>' ControlToValidate="txtWeighting"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator2" CssClass="Error" runat="server" ControlToValidate="txtWeighting"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnteraWholeNumber %>' MaximumValue="999999999"
                MinimumValue="1" Type="double"></asp:RangeValidator>
        </div>
        <div class="FieldStyle"><asp:Localize ID="Settings" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSettings %>'></asp:Localize></div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkShowPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayProductPrice %>' />
        </div>
        <div class="FieldStyle"></div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkUseWholesalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableWhoelSalePrice %>' />
        </div>
        <div class="FieldStyle"></div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkShowOnPartner" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableAffiliate %>' />
        </div>
        <div class="FieldStyle"></div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkTaxExempt" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTaxExempt %>' />
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmitBottom" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancelBottom" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
    </div>
</asp:Content>
