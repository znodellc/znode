<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.CustomerManagement.Profiles.Default" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1 class="LeftFloat"><asp:Localize ID="Profiles" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextProfiles %>'></asp:Localize></h1>
            <div class="tooltip ProfileToolTip">
                <a href="javascript:void(0);" class="learn-more"><span>
                    <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                <div class="content">
                    <h6><asp:Localize ID="Help" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleHelp %>'></asp:Localize></h6>
                    <p>
                        <asp:Localize ID="ProfileHintStep" runat="server" Text='<%$ Resources:ZnodeAdminResource, ProfileHintStep %>'></asp:Localize>
                    </p>
                </div>
            </div>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddProfile" runat="server" CausesValidation="False" ButtonType="Button" OnClick="BtnAddProfile_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddNewProfile %>' ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth">
            <p>
                <asp:Localize ID="TextProfiles" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextProfiles %>'></asp:Localize>
            </p>

        </div>
        <h4 class="GridTitle"><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleProfile %>'></asp:Localize></h4>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" 
            OnPageIndexChanging="UxGrid_PageIndexChanging" CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" AllowSorting="True"
            EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundProfiles %>'>
            <Columns>
                <asp:BoundField DataField="ProfileID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProfilesName %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='add.aspx?ItemID=<%# DataBinder.Eval(Container.DataItem, "ProfileID").ToString()%>'>
                            <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleShowPrice %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img11" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowPricing").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleWholeSalePricing %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img51" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "UseWholesalePricing").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTaxExempt %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img31" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "TaxExempt").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAffiliate %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img id="Img15" runat="server" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowOnPartnerSignup").ToString()))%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleActions %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div class="LeftFloat" style="width: 40%; text-align: left">
                            <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("ProfileID") %>' CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CssClass="LinkButton" />
                        </div>
                        <div class="LeftFloat" style="width: 50%; text-align: left">
                            <asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("ProfileID") %>' CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CssClass="LinkButton" />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
    </div>
</asp:Content>
