<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.CustomerManagement.Profiles.Delete" Codebehind="Delete.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5><asp:Localize ID="PleaseConfirmText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm %>'></asp:Localize></h5>
    <p><asp:label ID="DeleteConfirmText" runat="server"></asp:label></p>
    <div>
        <asp:Label ID="lblErrorMessage" CssClass="Error" runat="server" Text=''></asp:Label></div>
    <div>
        <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnDelete_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete%>' CausesValidation="True" ID="btnDelete" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>'  ID="btnCancel" />
      </div>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="25" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
