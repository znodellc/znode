using System;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.Admin.Secure.Orders.CustomerManagement.Profiles
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_settings_Profile_Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string AddLink = "~/Secure/Orders/CustomerManagement/Profiles/Add.aspx";
        private string DeleteLink = "~/Secure/Orders/CustomerManagement/Profiles/Delete.aspx?ItemID=";
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.Bind();
            }
        }

        /// <summary>
        /// Add Profile Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddProfile_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddLink);
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Grid Method
        /// </summary>
        protected void Bind()
        {
            ProfileService profileService = new ProfileService();
            ProfileAdmin profileAdmin = new ProfileAdmin();
            TList < ZNode.Libraries.DataAccess.Entities.Profile> ProfileList = profileService.GetAll();

            if (ProfileList.Count > 0)
            {
                uxGrid.DataSource = ProfileList;
                uxGrid.DataBind();
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Grid page Index Change Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.Bind();
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Edit")
                {
                    this.AddLink = this.AddLink + "?ItemID=" + Id;
                    Response.Redirect(this.AddLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + Id);
                }
            }
        }
        #endregion
    }
}