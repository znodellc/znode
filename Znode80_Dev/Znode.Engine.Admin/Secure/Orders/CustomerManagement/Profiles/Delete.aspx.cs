using System;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Orders.CustomerManagement.Profiles
{
    /// <summary>
    /// Represents the SiteAdmin -  Admin_Secure_settings_Profile_Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId;
        private string _ProfileName = string.Empty;
        private string CancelLink = "~/Secure/Orders/CustomerManagement/Profiles/Default.aspx";
        #endregion

        /// <summary>
        /// Gets or sets the Profile Name
        /// </summary>
        public string ProfileName
        {
            get
            {
                return this._ProfileName;
            }

            set
            {
                this._ProfileName = value;
            }
        }

        #region Bind Data
        /// <summary>
        /// Bind the data for a Product id
        /// </summary>
        public void BindData()
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            ZNode.Libraries.DataAccess.Entities.Profile profileEntity = profileAdmin.GetByProfileID(this.ItemId);

            if (profileEntity != null)
            {
                this.ProfileName = profileEntity.Name;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring   
            if (Request.Params["Itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["Itemid"].ToString());
                this.BindData();
            }
            else
            {
                this.ItemId = 0;
            }

            DeleteConfirmText.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextDeleteProfile").ToString(), ProfileName);
        }
        #endregion
       
        #region Events
        /// <summary>
        /// Delete Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            ZNode.Libraries.DataAccess.Entities.Profile profileEntity = profileAdmin.GetByProfileID(this.ItemId);

            bool status = false;
            string message = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorDeleteProfile").ToString();

            // Check the profile IsDefualt or not
            ZNode.Libraries.DataAccess.Service.PortalProfileService portalProfileService = new ZNode.Libraries.DataAccess.Service.PortalProfileService();
            ZNode.Libraries.DataAccess.Service.PortalService portalService = new ZNode.Libraries.DataAccess.Service.PortalService();
            TList<PortalProfile> portalProfileList = portalProfileService.GetByProfileID(this.ItemId);

            if (portalProfileList.Count == 0)
            {
                string AssociateName = this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogDeleteProfile").ToString() + profileEntity.Name;
                string ProfileName = profileEntity.Name;
                status = profileAdmin.Delete(profileEntity);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, ProfileName);
            }

            if (status)
            {
                Response.Redirect(this.CancelLink);
            }
            else
            {
                lblErrorMessage.Text = message;
            }
        }

        /// <summary>
        /// Cancel Button click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelLink);
        }
        #endregion
    }
}