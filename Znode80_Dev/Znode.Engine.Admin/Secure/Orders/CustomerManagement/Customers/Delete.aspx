<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.CustomerManagement.Customers.Delete" CodeBehind="Delete.aspx.cs" %>

<%@ Register Src="~/Controls/Default/Accounts/AccountDelete.ascx" TagName="AccountDelete" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <ZNode:AccountDelete ID="uxAccountDelete" runat="server" />
</asp:Content>
