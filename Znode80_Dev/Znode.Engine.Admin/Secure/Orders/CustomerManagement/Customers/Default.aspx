﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" CodeBehind="Default.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Orders.CustomerManagement.Customers.Default" %>

<%@ Register Src="~/Controls/Default/Accounts/Customer.ascx" TagName="CustomerList" TagPrefix="ZNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div align="center">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1><asp:Localize ID="TitleCustomers" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleCustomers %>'></asp:Localize></h1>
        </div>
        <div class="ClearBoth">
        </div>
        <div class="LeftFloat">
            <p>
                <asp:Localize ID="TextCustomer" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCustomer %>'></asp:Localize>
            </p>
        </div>

        <div style="float: right;">
            <zn:LinkButton ID="AddContact" runat="server" CausesValidation="false" Text='<%$ Resources:ZnodeAdminResource, ButtonAddCustomer %>'
                CommandArgument="CUSTOMER" OnCommand="AddContact_Command"
                ButtonType="Button" ButtonPriority="Primary" />
        </div>
    </div>
    <ZNode:CustomerList ID="uxCustomer" runat="server" RoleName="" />
</asp:Content>
