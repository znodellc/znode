﻿using System;
using System.Web.UI.WebControls;

namespace  Znode.Engine.Admin.Secure.Orders.CustomerManagement.Customers
{
    public partial class Default : System.Web.UI.Page
    {
        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Add Contact Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddContact_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/EditCustomer.aspx?pagefrom=" + e.CommandArgument.ToString());
        }
        #endregion
    }
}
