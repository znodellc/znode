<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.CustomerManagement.Customers.Disable" CodeBehind="Disable.aspx.cs" %>

<%@ Register Src="~/Controls/Default/Accounts/AccountDisable.ascx" TagName="AccountDisable" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <ZNode:AccountDisable ID="uxAccountDelete" runat="server" />
</asp:Content>
