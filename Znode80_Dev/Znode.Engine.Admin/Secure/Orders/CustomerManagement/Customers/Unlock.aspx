<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.CustomerManagement.Customers.Unlock" CodeBehind="Unlock.aspx.cs" %>

<%@ Register Src="~/Controls/Default/Accounts/AccountUnlock.ascx" TagName="AccountUnlock" TagPrefix="ZNode" %>


<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <ZNode:AccountUnlock ID="uxAccountUnlock" runat="server" />
</asp:Content>
