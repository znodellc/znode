<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Orders.CustomerManagement.Customers.AddPermission"
    Title="Untitled Page" Codebehind="AddPermission.aspx.cs" %>

<%@ Register Src="~/Controls/Default/Accounts/Permission.ascx" TagName="Permission" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <ZNode:Permission ID="uxPermission" runat="server" RoleName = "" />
</asp:Content>
