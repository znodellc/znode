﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/content.master"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Help.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div class="LeftMargin">
        <h1>
            <asp:Localize runat="server" ID="TitleHelp" Text='<%$ Resources:ZnodeAdminResource, TitleHelp %>'></asp:Localize>
        </h1>
        <hr />
        <div class="ImageAlign">
            <h1>
                <asp:Localize runat="server" ID="TitleMerchants" Text='<%$ Resources:ZnodeAdminResource, TitleMerchants %>'></asp:Localize>
            </h1>
            <br />
            <div class="Shortcuts">
                <a href="http://www.znode.com/downloads/7.2/Znode_multifront_merchant_guide_7.2.pdf" target="_blank">
                    <asp:Localize runat="server" ID="LinkQuickStartGuide" Text='<%$ Resources:ZnodeAdminResource, LinkQuickStartGuide %>'></asp:Localize>
                </a>
            </div>
        </div>
    </div>
</asp:Content>
