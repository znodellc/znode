﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PopularSearchReport.ascx.cs" Inherits="Znode.Engine.Admin.Secure.Reports.PopularSearchReport" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<div class="ReportLayout">
    <h4 class="SubTitle">
        <asp:Localize runat="server" ID="SubtitleStoreReport" Text='<%$ Resources:ZnodeAdminResource, SubtitleStoreReport %>'></asp:Localize>
    </h4>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div id="pnlCustom" runat="server" class="SearchForm">
        <div class="RowStyle">
            <div class="ItemStyle">
                <span class="FieldStyle">
                    <asp:Localize runat="server" ID="TitleGetSearches" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleGetSearches %>'></asp:Localize>
                </span>
                <br />
                <span class="ValueStyle">
                    <asp:DropDownList ID="ListOrderStatus" runat="server">
                        <asp:ListItem Value="0" Text='<%$ Resources:ZnodeAdminResource, DropDownTextSelect %>'></asp:ListItem>
                        <asp:ListItem Value="1" Text='<%$ Resources:ZnodeAdminResource, DropDownTextDay %>'></asp:ListItem>
                        <asp:ListItem Value="2" Text='<%$ Resources:ZnodeAdminResource, DropDownTextWeek %>'></asp:ListItem>
                        <asp:ListItem Value="3" Text='<%$ Resources:ZnodeAdminResource, DropDownTextMonth %>'></asp:ListItem>
                        <asp:ListItem Value="4" Text='<%$ Resources:ZnodeAdminResource, DropDownTextQuarter %>'></asp:ListItem>
                        <asp:ListItem Value="5" Text='<%$ Resources:ZnodeAdminResource, DropDownTextYear %>'></asp:ListItem>
                    </asp:DropDownList>
                </span>
            </div>
        </div>
        <div align="left" class="ClearBoth">
            <br />
        </div>
        <div align="left">
            <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
            <zn:Button runat="server" ID="btnOrderFilter" ButtonType="SubmitButton" OnClick="BtnOrderFilter_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="True" />
        </div>
    </div>
    <div align="right">
        <zn:Button CausesValidation="false" ID="btnBack" CssClass="Button" runat="server" Text='<%$ Resources:ZnodeAdminResource, ButtonPreviousPage %>' OnClick="BtnBack_Click" Visible="False"/>
    </div>
    <div>
        <uc1:Spacer ID="Spacer3" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <asp:Label ID="lblErrorMsg" CssClass="Error" runat="server" EnableViewState="false"></asp:Label>
    </div>
    <rsweb:ReportViewer ShowBackButton="False" ID="objReportViewer" runat="server" Width="100%" AsyncRendering="false"
        Font-Names="Verdana" Font-Size="8pt" ShowExportControls="true" ShowPageNavigationControls="true"
        ShowCredentialPrompts="false" ShowDocumentMapButton="false" ShowFindControls="false"
        ShowPrintButton="true" ShowRefreshButton="false" ShowZoomControl="false">
    </rsweb:ReportViewer>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:HiddenField runat="server" ID="hdnIsPostBack" Value="false" />
    <asp:HiddenField runat="server" ID="hdnMode" Value="23" />

</div>
