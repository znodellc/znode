﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;

namespace  Znode.Engine.Admin
{
    /// <summary>
    /// Represents the SiteAdmin - VendorRevenueReport class
    /// </summary>
    public partial class VendorRevenueReport : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private DataTable subReportDataSource = new DataTable();
        private string Year = string.Empty;
        private string _ReportTitle;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the IsPostBack
        /// </summary>
        public string IsPostback
        {
            get
            {
                return hdnIsPostBack.Value;
            }

            set
            {
                hdnIsPostBack.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the Report Title
        /// </summary>
        public string ReportTitle
        {
            get 
            { 
                return this._ReportTitle; 
            }

            set
            {
                this._ReportTitle = value;
                lblTitle.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the Mode Property
        /// </summary>
        public ZnodeReport Mode
        {
            get
            {
                string mode = hdnMode.Value;
                ZnodeReport report = ZnodeReport.ServiceRequest;
                if (Enum.IsDefined(typeof(ZnodeReport), mode))
                {
                    report = (ZnodeReport)Enum.Parse(typeof(ZnodeReport), hdnMode.Value, true);
                }
                
                return report;
            }

            set 
            { 
                hdnMode.Value = value.ToString(); 
            }
        }

        #endregion

        #region Page Load Event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param> 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
            {
                objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
            }
        }

        /// <summary>
        /// Page Pre Render Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param> 
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(hdnIsPostBack.Value))
            {
                hdnIsPostBack.Value = "true";
                txtEndDate.Text = string.Empty;
                txtStartDate.Text = string.Empty;
                this.objReportViewer.Visible = false;
            }
        }
        #endregion

        #region Events
         
        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            hdnIsPostBack.Value = "false";
            txtEndDate.Text = string.Empty;
            txtStartDate.Text = string.Empty;
            this.objReportViewer.Visible = false;
        }

        /// <summary>
        /// Get Order details click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnOrderFilter_Click(object sender, EventArgs e)
        {
            this.ShowReport();
        }

        /// <summary>
        /// Show Report Function
        /// </summary>
        private void ShowReport()
        {
            objReportViewer.Visible = true;
            
            // Get Filetered Orders in DataSet
            ReportAdmin reportAdmin = new ReportAdmin();

            DataSet reportDataSet = null;
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;

            if (!string.IsNullOrEmpty(txtStartDate.Text))
            {
                startDate = DateTime.Parse(txtStartDate.Text.Trim());
            }

            if (!string.IsNullOrEmpty(txtEndDate.Text))
            {
                endDate = DateTime.Parse(txtEndDate.Text.Trim());
            }
            
            reportDataSet = reportAdmin.ReportList(this.Mode, startDate, endDate, string.Empty, "0", string.Empty);
            
            if (reportDataSet.Tables.Count == 0 || reportDataSet.Tables[0].Rows.Count == 0)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                objReportViewer.Visible = false;
                return;
            }
            else
            {
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.ActivityLogReport, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.Mode.ToString() + this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogInventoryReport").ToString(), this.Mode.ToString());

                objReportViewer.Visible = true;
                objReportViewer.LocalReport.DataSources.Clear();
                if (this.Mode == ZnodeReport.VendorRevenue)
                {
                    this.objReportViewer.LocalReport.ReportPath = "Secure/Reports/VendorRevenue.rdlc";
                    objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZNodeReports_ZNode_ReportVendorRevenue", reportDataSet.Tables[0]));
                }
                else if (this.Mode == ZnodeReport.VendorProductRevenue)
                {
                    this.objReportViewer.LocalReport.ReportPath = "Secure/Reports/VendorProductRevenue.rdlc";
                    objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZNode_ReportVendorProductRevenue", reportDataSet.Tables[1]));
                }
                else
                {
                    return;
                }
				
                objReportViewer.LocalReport.Refresh();
            }
        }

        #endregion
    }
}