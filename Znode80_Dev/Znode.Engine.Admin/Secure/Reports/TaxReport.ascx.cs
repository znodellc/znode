﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Reports_TaxReport class
    /// </summary>
    public partial class Admin_Secure_Reports_TaxReport : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private DataSet ds = null;
        private string Year = string.Empty;
        private string _ReportTitle;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the IsPostBack Property
        /// </summary>
        public string IsPostback
        {
            get 
            { 
                return hdnIsPostBack.Value; 
            }
            
            set 
            { 
                hdnIsPostBack.Value = value; 
            }
        }

       /// <summary>
       /// Gets or sets the Mode Property
       /// </summary>
       public ZnodeReport Mode
       {
            get
            {
                string mode = hdnMode.Value;
                ZnodeReport report = ZnodeReport.ServiceRequest;
                if (Enum.IsDefined(typeof(ZnodeReport), mode))
                {
                    report = (ZnodeReport)Enum.Parse(typeof(ZnodeReport), hdnMode.Value, true);
                }
                
                return report;
            }
            
           set 
           { 
               hdnMode.Value = value.ToString(); 
           }
        }
       
        /// <summary>
        /// Gets or sets the ReportTitle
        /// </summary>
        public string ReportTitle
        {
            get 
            { 
                return this._ReportTitle; 
            }

            set
            {
                this._ReportTitle = value;
                lblTitle.Text = value;
            }
        }

        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //lblGroupBy.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnTitleGroupBy").ToString();
            if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
            {
                objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
            }
            
            if (!IsPostBack)
            {
                this.BindStores();
            }
        }

        #endregion

        #region Events Methods
        /// <summary>
        /// Sub Report Processing Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("ZNodeSalesTaxDataSet_SalesTax", this.ds.Tables[0]));
        }
       
        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Reports/default.aspx");
        }

        /// <summary>
        /// Order Filter Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnOrderFilter_Click(object sender, EventArgs e)
        {
            this.ShowReport();
        }

        /// <summary>
        /// Portal Dropdown control Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlPortal_SelectedIndexChanged(object sender, EventArgs e)
        {
            objReportViewer.Visible = false;
        }
        
        /// <summary>
        /// Report Group By Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlReportGroupBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            objReportViewer.Visible = false;
        }
        
        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            hdnIsPostBack.Value = "false";
            ddlPortal.SelectedIndex = -1;
            ddlReportGroupBy.SelectedIndex = -1;
            this.objReportViewer.Visible = false;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind store names
        /// </summary>
        private void BindStores()
        {
            // Load StoresList
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = UserStoreAccess.CheckStoreAccess(portalAdmin.GetAllPortals());
            
            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
            ddlPortal.SelectedIndex = 0;

            this.objReportViewer.Visible = false;
        }

        /// <summary>
        /// Get Report Name Method
        /// </summary>
        /// <returns>Returns the Report Name</returns>
        private ZnodeReport GetZnodeReportName()
        {
            switch (ddlReportGroupBy.SelectedIndex)
            {
                case 1:
                    return ZnodeReport.SalesTaxByMonth;
                case 2:
                    return ZnodeReport.SalesTaxByQuarter;
                default:
                    return ZnodeReport.SalesTax;
            }
        }
       
        /// <summary>
        /// Display the activity log report based on parameter.
        /// </summary>
        private void ShowReport()
        {
            OrderAdmin _OrderAdmin = new OrderAdmin();
            ReportAdmin reportAdmin = new ReportAdmin();
            string portalId = ddlPortal.SelectedValue;
            ZnodeReport report = this.GetZnodeReportName();
            this.ds = reportAdmin.ReportList(report, DateTime.Today, DateTime.Today, string.Empty, portalId, string.Empty);

            if (this.ds.Tables[0].Rows.Count == 0)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                objReportViewer.Visible = false;
                return;
            }
            else
            {
                this.objReportViewer.Visible = true;
                objReportViewer.LocalReport.DataSources.Clear();
                this.objReportViewer.Reset();
                this.objReportViewer.LocalReport.ReportPath = "Secure/Reports/TaxReport.rdlc";

                ReportParameter param2 = new ReportParameter("CurencyFormat", ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix());
                this.objReportViewer.LocalReport.SetParameters(new ReportParameter[] { param2 });
                ReportParameter param1 = new ReportParameter("CurrentLanguage", System.Globalization.CultureInfo.CurrentCulture.Name);
                this.objReportViewer.LocalReport.SetParameters(new ReportParameter[] { param1 });

           
                this.objReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(this.LocalReport_SubreportProcessing);
                this.objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZNodeSalesTaxDataSet_SalesTax", this.ds.Tables[0]));
                this.objReportViewer.PageCountMode = PageCountMode.Actual; 
                this.objReportViewer.LocalReport.Refresh();
            }
        }
        #endregion
    }
}  