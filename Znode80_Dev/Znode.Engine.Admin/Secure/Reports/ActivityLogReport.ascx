﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Admin_Secure_Reports_CActivityLogReport"
    CodeBehind="ActivityLogReport.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<div class="ReportLayout">
    <h4 class="SubTitle">
        <asp:Label ID="lblTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleActivityLogReport %>'></asp:Label>
    </h4>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:Panel ID="pnlCustom" runat="server">
        <div class="SearchForm">
            <div class="RowStyle">
                <div class="ItemStyle" style="width: 190px;">
                    <span class="FieldStyle">
                        <asp:Localize runat="server" ID="TitleStoreName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName %>'></asp:Localize>
                    </span>
                    <br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlPortal" runat="server">
                        </asp:DropDownList>
                    </span>
                </div>
                <div class="ItemStyle" style="width: 140px;">
                    <span class="FieldStyle">
                        <asp:Localize runat="server" ID="TitleBeginDate" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleReportBeginDate %>'></asp:Localize>
                    </span>
                    <br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtStartDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="imgbtnStartDt"
                            runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredReportBeginDate %>'
                            ControlToValidate="txtStartDate" ValidationGroup="grpReports" CssClass="Error"
                            Text="" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularReportValidDate %>'
                            Text="" ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidReportDate %>'
                            ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                    </span>
                </div>
                <div class="ItemStyle" style="width: 140px;">
                    <span class="FieldStyle">
                        <asp:Label ID="Label1" Text="Category" runat="server"></asp:Label>
                    </span>
                    <br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlLogCategory" runat="server">
                            <asp:ListItem Value="0" Text='<%$ Resources:ZnodeAdminResource, DropdownTextAll %>'></asp:ListItem>
                            <asp:ListItem Value="1" Text='<%$ Resources:ZnodeAdminResource, DropdownTextError %>'></asp:ListItem>
                            <asp:ListItem Value="2" Text='<%$ Resources:ZnodeAdminResource, DropdownTextNotice %>'></asp:ListItem>
                            <asp:ListItem Value="3" Text='<%$ Resources:ZnodeAdminResource, DropdownTextSearch %>'></asp:ListItem>
                            <asp:ListItem Value="4" Text='<%$ Resources:ZnodeAdminResource, DropdownTextWarning %>'></asp:ListItem>
                        </asp:DropDownList>
                    </span>
                </div>
            </div>
        </div>
        <div align="left" class="ClearBoth">
            <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                PopupPosition="TopLeft" runat="server" TargetControlID="txtStartDate">
            </ajaxToolKit:CalendarExtender>
        </div>
        <asp:PlaceHolder ID="pnlSearch" runat="server" Visible="true">
            <div align="right" style="padding-right: 10px;">
                <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                <zn:Button runat="server" ID="btnOrderFilter" ButtonType="SubmitButton" OnClick="BtnOrderFilter_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="True" ValidationGroup="grpReports" />
            </div>
        </asp:PlaceHolder>
    </asp:Panel>
    <div>
    </div>
    <div>
        <asp:Label ID="lblErrorMsg" CssClass="Error" runat="server" EnableViewState="false"></asp:Label>
    </div>
    <br />
    <div>
        <rsweb:ReportViewer ShowBackButton="False" ID="objReportViewer" runat="server" Width="100%" AsyncRendering="false"
            Font-Names="Verdana" Font-Size="8pt" ShowExportControls="true" ShowPageNavigationControls="true"
            ShowCredentialPrompts="false" ShowDocumentMapButton="false" ShowFindControls="false"
            ShowPrintButton="true" ShowRefreshButton="false" ShowZoomControl="false">
            <LocalReport DisplayName="Report" ReportPath="Secure/Reports/ActivityLog.rdlc">
            </LocalReport>
        </rsweb:ReportViewer>
    </div>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:HiddenField runat="server" ID="hdnIsPostBack" Value="false" />
    <asp:HiddenField runat="server" ID="hdnMode" Value="12" />
</div>
