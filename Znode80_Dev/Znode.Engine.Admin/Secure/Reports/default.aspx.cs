using System;

namespace  Znode.Engine.Admin
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Reports_default class
    /// </summary>
    public partial class Admin_Secure_Reports_default : System.Web.UI.Page
    {
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["SelectedMenuItem"] = null;
            Response.Redirect("Report.aspx");
        }
    }
}