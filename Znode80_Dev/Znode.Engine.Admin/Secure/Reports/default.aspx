<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Admin_Secure_Reports_default" CodeBehind="default.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h1>
        <asp:Localize runat="server" ID="TitleZnodeReports" Text='<%$ Resources:ZnodeAdminResource, TitleZnodeReports %>'></asp:Localize>
    </h1>
    <p>
        <asp:Localize runat="server" ID="TextZnodeReports" Text='<%$ Resources:ZnodeAdminResource, TextZnodeReports %>'></asp:Localize>
    </p>

    <div class="LandingPage">
        <hr />
        <div style="margin-right: 10px;">
            <div class="Shortcut">
                <a id="A3" href="~/Secure/reports/ReportList.aspx?filter=12" runat="server">
                    <asp:Localize runat="server" ID="TextOrders" Text='<%$ Resources:ZnodeAdminResource, LinkTextOrders %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A12" href="~/Secure/reports/ReportList.aspx?filter=21" runat="server">
                    <asp:Localize runat="server" ID="TextAccounts" Text='<%$ Resources:ZnodeAdminResource, LinkTextAccounts %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A21" href="~/Secure/reports/InventoryReports.aspx?filter=20" runat="server">
                    <asp:Localize runat="server" ID="TextBestSellers" Text='<%$ Resources:ZnodeAdminResource, LinkTextBestSellers %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A20" href="~/Secure/reports/InventoryReports.aspx?filter=19" runat="server">
                    <asp:Localize runat="server" ID="TextServiceRequests" Text='<%$ Resources:ZnodeAdminResource, LinkTextServiceRequests %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A15" href="~/Secure/reports/InventoryReports.aspx?filter=14" runat="server">
                    <asp:Localize runat="server" ID="TextEmailOptInCustomers" Text='<%$ Resources:ZnodeAdminResource, LinkTextEmailOptInCustomers %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A19" href="~/Secure/reports/InventoryReports.aspx?filter=18" runat="server">
                    <asp:Localize runat="server" ID="TextInventoryReOrder" Text='<%$ Resources:ZnodeAdminResource, LinkTextInventoryReOrder %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A16" href="~/Secure/reports/InventoryReports.aspx?filter=15" runat="server">
                    <asp:Localize runat="server" ID="TextMostFrequentCustomers" Text='<%$ Resources:ZnodeAdminResource, LinkTextMostFrequentCustomers %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A17" href="~/Secure/reports/InventoryReports.aspx?filter=16" runat="server">
                    <asp:Localize runat="server" ID="TextTopSpendingCustomers" Text='<%$ Resources:ZnodeAdminResource, LinkTextTopSpendingCustomers %>'></asp:Localize>
                </a>
            </div>
        </div>

        <div>
            <div class="Shortcut">
                <a id="A18" href="~/Secure/reports/InventoryReports.aspx?filter=17" runat="server">
                    <asp:Localize runat="server" ID="TextTopEarningProducts" Text='<%$ Resources:ZnodeAdminResource, LinkTextTopEarningProducts %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A14" href="~/Secure/reports/InventoryReports.aspx?filter=13" runat="server">
                    <asp:Localize runat="server" ID="TextOrderPickList" Text='<%$ Resources:ZnodeAdminResource, LinkTextOrderPickList %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A1" href="~/Secure/reports/ActivityLogReport.aspx?filter=22" runat="server">
                    <asp:Localize runat="server" ID="TextActivityLogs" Text='<%$ Resources:ZnodeAdminResource, LinkTextActivityLogs %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A2" href="~/Secure/reports/InventoryReports.aspx?filter=24" runat="server">
                    <asp:Localize runat="server" ID="TextCouponUsage" Text='<%$ Resources:ZnodeAdminResource, LinkTextCouponUsage %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A4" href="~/Secure/reports/TaxReport.aspx?filter=25" runat="server">
                    <asp:Localize runat="server" ID="TextSalesTax" Text='<%$ Resources:ZnodeAdminResource, LinkTextSalesTax %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A5" href="~/Secure/reports/InventoryReports.aspx?filter=26" runat="server">
                    <asp:Localize runat="server" ID="TextAffiliateOrders" Text='<%$ Resources:ZnodeAdminResource, LinkTextAffiliateOrders %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A6" href="~/Secure/reports/SupplierReport.aspx?filter=27" runat="server">
                    <asp:Localize runat="server" ID="TextSupplierList" Text='<%$ Resources:ZnodeAdminResource, LinkTextSupplierList %>'></asp:Localize>
                </a>
            </div>
            <div class="Shortcut">
                <a id="A7" href="~/Secure/reports/PopularSearchReport.aspx?filter=23" runat="server">
                    <asp:Localize runat="server" ID="TextPopularSearch" Text='<%$ Resources:ZnodeAdminResource, LinkTextPopularSearch %>'></asp:Localize>
                </a>
            </div>
        </div>
    </div>
</asp:Content>
