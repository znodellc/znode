﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductsSoldOnVendorSitesReport.ascx.cs" Inherits="Znode.Engine.Admin.ProductsSoldOnVendorSitesReport" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>


<div class="ReportLayout">
    <h4 class="SubTitle">
        <asp:Label ID="lblTitle" runat="server"></asp:Label>
    </h4>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:Panel ID="pnlCustom" runat="server">
        <div>
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle" style="width: 140px;">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="TitleBeginDate" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleReportBeginDate %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtStartDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="imgbtnStartDt"
                                runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredReportBeginDate %>'
                                ControlToValidate="txtStartDate" ValidationGroup="grpReports" CssClass="Error"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                                CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularReportValidDate %>'
                                ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidReportDate %>'
                                ValidationGroup="grpReports"></asp:RegularExpressionValidator></span>
                    </div>
                    <div class="ItemStyle" style="width: 140px;">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="TitleEndDate" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEndDate %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtEndDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="ImgbtnEndDt"
                                runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEndDate"
                                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEndDate %>' ValidationGroup="grpReports" CssClass="Error"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
                                CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularReportValidDate %>'
                                ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidReportDate %>'
                                ValidationGroup="grpReports"></asp:RegularExpressionValidator></span>
                    </div>

                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="TitleStoreName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlPortal" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div align="left" class="ClearBoth">
        </div>
        <div class="Buttons">
            <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
            <zn:Button runat="server" ID="btnOrderFilter" ButtonType="SubmitButton" OnClick="BtnOrderFilter_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="True" ValidationGroup="grpReports" />
        </div>

        <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt" PopupPosition="TopLeft"
            runat="server" TargetControlID="txtStartDate">
        </ajaxToolKit:CalendarExtender>


        <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt" PopupPosition="TopLeft"
            runat="server" TargetControlID="txtEndDate">
        </ajaxToolKit:CalendarExtender>
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
            ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareEndDateBeginDate %>'
            Operator="GreaterThanEqual" Type="Date" ValidationGroup="grpReports"></asp:CompareValidator>
    </asp:Panel>
    <div>
        <uc1:Spacer ID="Spacer3" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <asp:Label ID="lblErrorMsg" CssClass="Error" runat="server" EnableViewState="false"></asp:Label>
    </div>
    <div class="ReportViewer">
        <rsweb:ReportViewer ShowBackButton="False" ID="objReportViewer" runat="server" Width="100%" AsyncRendering="false" Font-Names="Verdana"
            Font-Size="8pt" ShowExportControls="true" ShowPageNavigationControls="true" ShowCredentialPrompts="false"
            ShowDocumentMapButton="false" ShowFindControls="false" ShowPrintButton="true"
            ShowRefreshButton="false" ShowZoomControl="false">
            <LocalReport DisplayName="Report">
            </LocalReport>
        </rsweb:ReportViewer>
    </div>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:HiddenField runat="server" ID="hdnIsPostBack" Value="false" />
    <asp:HiddenField runat="server" ID="hdnMode" Value="12" />
</div>
