﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Admin_Secure_Reports_TaxReport" CodeBehind="TaxReport.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<div class="ReportLayout">
    <h4 class="SubTitle">
        <asp:Label ID="lblTitle" runat="server"></asp:Label>
    </h4>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:Panel ID="pnlCustom" runat="server">
        <div class="SearchForm">
            <div class="RowStyle">
                <div class="ItemStyle" style="width: 190px;">
                    <span class="FieldStyle">
                        <asp:Localize runat="server" ID="TextGenerateKeySettings" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName %>'></asp:Localize>
                    </span>
                    <br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlPortal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlPortal_SelectedIndexChanged">
                        </asp:DropDownList>
                    </span>
                </div>
                <div class="ItemStyle" style="width: 160px;">
                    <span class="FieldStyle">GROUP BY 
                    </span>
                    <br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlReportGroupBy" runat="server" Width="130px" AutoPostBack="true" OnSelectedIndexChanged="DdlReportGroupBy_SelectedIndexChanged">
                            <asp:ListItem Selected="True" Enabled="False" Text='<%$ Resources:ZnodeAdminResource, DropdownTextNone %>'></asp:ListItem>
                            <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextMonth %>'></asp:ListItem>
                            <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextQuarter %>'></asp:ListItem>
                        </asp:DropDownList>
                    </span>
                </div>

            </div>
        </div>
        <div align="left" class="ClearBoth">
        </div>
        <asp:PlaceHolder ID="pnlSearch" runat="server" Visible="true">
            <div align="right" style="padding-right: 10px;">
                <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                <zn:Button runat="server" ID="btnOrderFilter" ButtonType="SubmitButton" OnClick="BtnOrderFilter_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="True" ValidationGroup="grpReports" />
            </div>
        </asp:PlaceHolder>
    </asp:Panel>
    <div>

        <uc1:Spacer ID="Spacer3" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <asp:Label ID="lblErrorMsg" CssClass="Error" runat="server" EnableViewState="false"></asp:Label>
    </div>
    <br />
    <div>
        <rsweb:ReportViewer ShowBackButton="False" ID="objReportViewer" runat="server" Width="100%" AsyncRendering="false" Font-Names="Verdana"
            Font-Size="8pt" ShowExportControls="true" ShowPageNavigationControls="true" ShowCredentialPrompts="false"
            ShowDocumentMapButton="false" ShowFindControls="false" ShowPrintButton="true"
            ShowRefreshButton="false" ShowZoomControl="false">
            <LocalReport DisplayName="Report" ReportPath="Secure/Reports/ActivityLog.rdlc">
            </LocalReport>
        </rsweb:ReportViewer>
    </div>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:HiddenField runat="server" ID="hdnIsPostBack" Value="false" />
    <asp:HiddenField runat="server" ID="hdnMode" Value="12" />
</div>
