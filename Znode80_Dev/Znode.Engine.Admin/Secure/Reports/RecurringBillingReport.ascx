﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecurringBillingReport.ascx.cs" Inherits="Znode.Engine.Admin.Admin_Secure_Reports_RecurringBillingReport" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<%@ Register Src="~/Controls/Default/Profilelist.ascx" TagName="ProfileList"
    TagPrefix="ZNode" %>
<div class="ReportLayout">
    <h4 class="SubTitle">
        <asp:Label ID="lblTitle" runat="server"></asp:Label>
    </h4>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:Panel ID="pnlCustom" runat="server">
        <div class="SearchForm">
            <div class="RowStyle">
                <div class="ItemStyle" style="width: 140px">
                    <span class="FieldStyle">
                        <asp:Localize runat="server" ID="TitleBeginDate" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleReportBeginDate %>'></asp:Localize>
                    </span>
                    <br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtStartDate" CssClass="Size100" Text='' runat="server" />&nbsp;<asp:ImageButton
                            ID="imgbtnStartDt" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredReportBeginDate %>'
                            ControlToValidate="txtStartDate" ValidationGroup="grpReports" CssClass="Error"
                            Text="" Display="Dynamic"></asp:RequiredFieldValidator>

                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularReportValidDate %>'
                            Text="" ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidReportDate %>'
                            ValidationGroup="grpReports"></asp:RegularExpressionValidator>

                    </span>
                </div>
                <div class="ItemStyle" style="width: 140px">
                    <span class="FieldStyle">
                        <asp:Localize runat="server" ID="TitleEndDate" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEndDate %>'></asp:Localize>
                    </span>
                    <br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtEndDate" Text='' CssClass="Size100" runat="server" />&nbsp;<asp:ImageButton
                            ID="ImgbtnEndDt" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" /><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEndDate"
                            Text="" ErrorMessage="Enter End date" ValidationGroup="grpReports" CssClass="Error"
                            Display="Dynamic"></asp:RequiredFieldValidator>

                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularReportValidDate %>'
                            Text="" ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidReportDate %>'
                            ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                    </span>
                </div>
                <div class="ItemStyle" style="width: 130px; position: relative;">
                    <span class="FieldStyle">
                        <asp:Localize runat="server" ID="ColumnTitleStoreName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName %>'></asp:Localize>
                    </span>
                    <br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlPortal" Width="150px" runat="server">
                        </asp:DropDownList>
                    </span>
                </div>
                <div class="ItemStyle" style="padding-left: 50px; width: 100px">
                    <asp:PlaceHolder ID="pnlOrderStatus" runat="server"><span class="FieldStyle">
                        <asp:Localize runat="server" ID="ColumnTitleOrderState" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderState %>'></asp:Localize>
                    </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlOrderStatus" runat="server" />
                        </span></asp:PlaceHolder>
                </div>
                <div class="ItemStyle" visible="false">
                    <span class="FieldStyle">
                        <asp:Label ID="lblProfileName" runat="server" Visible="false"></asp:Label>
                    </span><span class="ValueStyle">
                        <asp:PlaceHolder ID="pnlprofile" runat="server" Visible="false"><span class="ValueStyle">
                            <ZNode:ProfileList ID="ProfileList" runat="server"></ZNode:ProfileList>
                        </span></asp:PlaceHolder>
                    </span>
                </div>
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <div class="Buttons">
            <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
            <zn:Button runat="server" ID="btnOrderFilter" ButtonType="SubmitButton" OnClick="BtnOrderFilter_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="True" ValidationGroup="grpReports" />
        </div>
    </asp:Panel>
    <div class="ClearBoth">
        <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
            PopupPosition="TopLeft" runat="server" TargetControlID="txtStartDate">
        </ajaxToolKit:CalendarExtender>


        <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt"
            PopupPosition="TopLeft" runat="server" TargetControlID="txtEndDate">
        </ajaxToolKit:CalendarExtender>

        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
            Text="" ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareBeginDate %>'
            Operator="GreaterThanEqual" Type="Date" ValidationGroup="grpReports"></asp:CompareValidator>
    </div>
    <div>
        <uc1:Spacer ID="Spacer3" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <asp:Label ID="lblErrorMsg" CssClass="Error" runat="server" EnableViewState="false"></asp:Label>
    </div>
    <div>
        <rsweb:ReportViewer ShowBackButton="False" ID="objReportViewer" runat="server" Width="100%" AsyncRendering="false"
            Font-Names="Verdana" Font-Size="8pt" ShowExportControls="true" ShowPageNavigationControls="true"
            ShowCredentialPrompts="false" ShowDocumentMapButton="false" ShowFindControls="false"
            ShowPrintButton="true" ShowRefreshButton="false" ShowZoomControl="false">
        </rsweb:ReportViewer>
    </div>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:HiddenField runat="server" ID="hdnIsPostBack" Value="false" />
    <asp:HiddenField runat="server" ID="hdnMode" Value="12" />
</div>
