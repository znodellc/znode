<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Advanced.Accounts.StoreAdministrators.Default" Title="Untitled Page" CodeBehind="Default.aspx.cs" %>

<%@ Register Src="~/Controls/Default/Accounts/Customer.ascx" TagName="CustomerList" TagPrefix="ZNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div align="center">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Localize runat="server" ID="TitleStoreAdministrators" Text='<%$ Resources:ZnodeAdminResource, TitleStoreAdministrators %>'></asp:Localize>
            </h1>
        </div>
        <div class="ClearBoth">
        </div>
        <div class="LeftFloat">
            <p>
                <asp:Localize runat="server" ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, TextAdministratorAccount %>'></asp:Localize>
            </p>
        </div>

        <div style="float: right;">
            <zn:LinkButton ID="AddContact" runat="server" CausesValidation="false" Text='<%$ Resources:ZnodeAdminResource, ButtonAddStoreAdmin %>'
                CommandArgument="ADMIN" OnCommand="AddContact_Command"
                ButtonType="Button" ButtonPriority="Primary" />
        </div>
    </div>
    <ZNode:CustomerList ID="uxCustomer" runat="server" RoleName="admin" />
</asp:Content>

