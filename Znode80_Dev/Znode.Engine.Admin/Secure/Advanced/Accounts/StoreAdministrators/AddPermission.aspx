<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Advanced.Accounts.StoreAdministrators.AddPermission"
    Title="Untitled Page" Codebehind="AddPermission.aspx.cs" %>

<%@ Register Src="~/Controls/Default/Accounts/Permission.ascx" TagName="Permission" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <ZNode:Permission ID="uxPermission" runat="server" RoleName = "admin" />
</asp:Content>
