using System;
using System.Web.UI.WebControls;

namespace  Znode.Engine.Admin.Secure.Advanced.Accounts.StoreAdministrators
{
    /// <summary>
    /// Represents the Admin_Secure_sales_customers_list class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Add Contact Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddContact_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/Edit.aspx?pagefrom=" + e.CommandArgument.ToString());
        }
        #endregion
    }
}