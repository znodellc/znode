<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Advanced.Accounts.StoreAdministrators.AddAffiliatePayment" Codebehind="AddAffiliatePayment.aspx.cs" %>

<%@ Register Src="~/Controls/Default/Accounts/AffiliatePayment.ascx" TagName="AffiliatePayment" TagPrefix="ZNode" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
      <ZNode:AffiliatePayment ID="uxAffiliatePayment" runat="server" RoleName = "admin" />
</asp:Content>
