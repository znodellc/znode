﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="AddProfile.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Advanced.Accounts.StoreAdministrators.AddProfile" %>

<%@ Register Src="~/Controls/Default/Accounts/Profile.ascx" TagName="Profile" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
 <ZNode:Profile ID="uxProfile" runat="server" RoleName = "admin" />
</asp:Content>
