﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" CodeBehind="View.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Advanced.Accounts.StoreAdministrators.View" %>
<%@ Register Src="~/Controls/Default/Accounts/View.ascx" TagName="SiteadminList" TagPrefix="ZNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
 <ZNode:SiteadminList ID="uxCustomer" runat="server" RoleName = "admin" />
</asp:Content>
