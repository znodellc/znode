<%@ Page Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Advanced.Accounts.RotateKey.Key"
    MasterPageFile="~/Themes/Standard/edit.master" CodeBehind="Key.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h1>
        <asp:Localize runat="server" ID="TitleRotateKey" Text='<%$ Resources:ZnodeAdminResource, TitleRotateKey %>'></asp:Localize>
    </h1>
    <p>
        <asp:Localize runat="server" ID="TextGenerateKey" Text='<%$ Resources:ZnodeAdminResource, TextGenerateKey %>'></asp:Localize>
    </p>

    <p>
        <asp:Localize runat="server" ID="TextConfirmGenerateKey" Text='<%$ Resources:ZnodeAdminResource, TextConfirmGenerateKey %>'></asp:Localize>
    </p>
    <br />
    <b>
        <asp:Localize runat="server" ID="TextCaution" Text='<%$ Resources:ZnodeAdminResource, TextCaution %>'></asp:Localize>
    </b>
    <p>
        <asp:Localize runat="server" ID="TextGenerateKeySettings" Text='<%$ Resources:ZnodeAdminResource, TextGenerateKeySettings %>'></asp:Localize>
    </p>
    <p>
        <asp:Localize runat="server" ID="TextGenerateKeyImpact" Text='<%$ Resources:ZnodeAdminResource, TextGenerateKeyImpact %>'></asp:Localize>
    </p>
    <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnGenerate_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonGenerate %>'
        CausesValidation="False" ID="btnGenerate" Width="100px" />
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>'
        CausesValidation="False" ID="btnCancel" />
    <div style="margin-top: 20px; margin-bottom: 50px; font-weight: bold; color: #009900;">
        <asp:Label runat="server" ID="lblMsg"></asp:Label>
    </div>
    <a id="A1" href="~/Secure/Advanced/Default.aspx?mode=ipcommerce" runat="server">
        <asp:Localize runat="server" ID="LinkBackToAdvancedPage" Text='<%$ Resources:ZnodeAdminResource, LinkBackToAdvancedPage %>'></asp:Localize>
    </a>
</asp:Content>
