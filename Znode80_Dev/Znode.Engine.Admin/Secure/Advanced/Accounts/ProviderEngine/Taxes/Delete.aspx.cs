﻿using System;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Taxes
{
	public partial class Delete : System.Web.UI.Page
	{
		private ProviderTypeAdmin _providerTypeAdmin;
		private TaxRuleType _taxType;

		public string TaxTypeClassName { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			_providerTypeAdmin = new ProviderTypeAdmin();
			_taxType = _providerTypeAdmin.GetTaxRuleTypeById(int.Parse(Request.Params["itemid"]));

			if (_taxType != null)
			{
				TaxTypeClassName = _taxType.ClassName;
			}
			else
			{
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoTaxType").ToString();
				lblErrorMsg.Visible = true;
				btnDelete.Enabled = false;
			}
		}

		protected void BtnDelete_Click(object sender, EventArgs e)
		{
			try
			{
				var success = _providerTypeAdmin.DeleteTaxRuleType(int.Parse(Request.Params["itemid"]));
				var source = this.GetGlobalResourceObject("ZnodeAdminResource", "TextDeleteTaxType") + _taxType.ClassName;

				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, source, _taxType.ClassName);

				if (!success)
				{
					lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnableToDeleteTaxType") + _taxType.ClassName + this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAssociatedTaxRules");
					lblErrorMsg.Visible = true;
				}
				else
				{
					RedirectBacktoList();
				}
			}
			catch (Exception ex)
			{
				ZNodeLogging.LogMessage(ex.Message);
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnableToDeleteTaxType") + _taxType.ClassName + this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAssociatedTaxRules");
				lblErrorMsg.Visible = true;
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			RedirectBacktoList();
		}

		private void RedirectBacktoList()
		{
			Response.Redirect("~/Secure/Advanced/Accounts/ProviderEngine/Default.aspx?type=taxes");
		}
	}
}