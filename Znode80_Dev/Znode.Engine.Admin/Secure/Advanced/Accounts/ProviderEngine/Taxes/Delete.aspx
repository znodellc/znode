﻿<%@ Page Language="C#" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Taxes.Delete" MasterPageFile="~/Themes/Standard/edit.master" CodeBehind="Delete.aspx.cs" %>

<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
    <h1>
        <asp:Label ID="lblDeleteTaxType" runat="server">
            <asp:Localize runat="server" ID="TitleDeleteTaxType" Text='<%$ Resources:ZnodeAdminResource, TitleDeleteTaxType %>'></asp:Localize><%=TaxTypeClassName%>
        </asp:Label>
    </h1>

    <div>
        <zn:Spacer ID="Spacer1" runat="server" SpacerHeight="10" SpacerWidth="3" />
    </div>

    <p>
        <asp:Localize runat="server" ID="TextTaxTypeDelete" Text='<%$ Resources:ZnodeAdminResource, TextTaxTypeDelete %>'></asp:Localize>
    </p>
    <p>
        <asp:Localize runat="server" ID="TextTaxTypeDeletionFailed" Text='<%$ Resources:ZnodeAdminResource, TextTaxTypeCannotDelete %>'></asp:Localize>
    </p>

    <p>
        <asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error" />
    </p>

    <div>
        <zn:Spacer ID="Spacer2" runat="server" SpacerHeight="10" SpacerWidth="3" />
    </div>

    <div>
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnDelete_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>'
            CausesValidation="true" ID="btnDelete" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>'
            CausesValidation="False" ID="btnCancel" />
    </div>

    <div>
        <zn:Spacer ID="Spacer3" runat="server" SpacerHeight="15" SpacerWidth="3" />
    </div>
</asp:Content>
