﻿using System;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;

namespace Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Taxes
{
	public partial class List : System.Web.UI.UserControl
	{
		private ProviderTypeAdmin _providerTypeAdmin = new ProviderTypeAdmin();
		private string _addPageLink = "~/Secure/Advanced/Accounts/ProviderEngine/Taxes/Add.aspx";
		private string _deletePageLink = "~/Secure/Advanced/Accounts/ProviderEngine/Taxes/Delete.aspx";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				BindTaxTypes();
			}
		}

		protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "Page")
			{
				var index = Convert.ToInt32(e.CommandArgument);
				var selectedRow = uxGrid.Rows[index];
				var tableCell = selectedRow.Cells[0];
				var id = tableCell.Text;

				if (e.CommandName == "Edit")
				{
					Response.Redirect(_addPageLink + "?itemid=" + id + "&type=taxes");
				}
				else if (e.CommandName == "Delete")
				{
					Response.Redirect(_deletePageLink + "?itemid=" + id);
				}
			}
		}

		protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			uxGrid.PageIndex = e.NewPageIndex;
			BindTaxTypes();
		}

		protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
		{
			BindTaxTypes();
		}

		protected void BtnAddTaxType_Click(object sender, EventArgs e)
		{
			Response.Redirect(_addPageLink + "?type=taxes");
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/Secure/Advanced/Accounts/ProviderEngine/Default.aspx?type=taxes");
		}

		private void BindTaxTypes()
		{
			var taxTypes = _providerTypeAdmin.GetTaxRuleTypes();
			taxTypes.Sort("ClassName");

			uxGrid.DataSource = taxTypes;
			uxGrid.DataBind();
		}
	}
}