﻿using System;

namespace Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine
{
	public partial class Default : System.Web.UI.Page
	{
		private string _type = String.Empty;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.Params["type"] != null)
			{
				_type = Request.Params["type"];
			}


			if (!Page.IsPostBack)
			{
				ResetTab();
			}
		}

		private void ResetTab()
		{
			if (_type.Equals("promotions"))
			{
				tabProviderEngine.ActiveTab = pnlPromotionTypes;
			}
			else if (_type.Equals("shipping"))
			{
				tabProviderEngine.ActiveTab = pnlShippingTypes;
			}
			else if (_type.Equals("suppliers"))
			{
				tabProviderEngine.ActiveTab = pnlSupplierTypes;
			}
			else if (_type.Equals("taxes"))
			{
				tabProviderEngine.ActiveTab = pnlTaxTypes;
			}
			else
			{
				tabProviderEngine.ActiveTabIndex = 0;
			}
		}
	}
}