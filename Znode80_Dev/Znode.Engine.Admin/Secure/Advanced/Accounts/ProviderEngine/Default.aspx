﻿<%@ Page Language="C#" AutoEventWireup="True" ValidateRequest="false" CodeBehind="Default.aspx.cs" MasterPageFile="~/Themes/Standard/content.master" Inherits="Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Default" %>

<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>
<%@ Register TagPrefix="zn" TagName="PromotionTypes" Src="~/Secure/Advanced/Accounts/ProviderEngine/Promotions/List.ascx" %>
<%@ Register TagPrefix="zn" TagName="ShippingTypes" Src="~/Secure/Advanced/Accounts/ProviderEngine/Shipping/List.ascx" %>
<%@ Register TagPrefix="zn" TagName="SupplierTypes" Src="~/Secure/Advanced/Accounts/ProviderEngine/Suppliers/List.ascx" %>
<%@ Register TagPrefix="zn" TagName="TaxTypes" Src="~/Secure/Advanced/Accounts/ProviderEngine/Taxes/List.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <script type="text/javascript">
        function ActiveTabChanged(sender, e) {
            var tab = document.getElementById('<%=tabProviderEngine.ClientID%>');
		    var activeTabIndex = document.getElementById('<%=hdnActiveTabIndex.ClientID%>');
		    activeTabIndex.value = sender.get_activeTabIndex();
		}
    </script>
    <style type="text/css">
        .ContentPane .BottomImage {
            margin-left: -5px;
        }
    </style>

    <asp:HiddenField ID="hdnActiveTabIndex" runat="server" Value="0" />

    <div>
        <div class="LeftFloat" style="width: 50%">
            <h1>
                <asp:Localize ID="TitleProviderEngine" Text='<%$ Resources:ZnodeAdminResource, TitleProviderEngine%>' runat="server"></asp:Localize>
            </h1>
        </div>

        <div class="ClearBoth">
            <p>
                <asp:Localize ID="TextProviderEngine" Text='<%$ Resources:ZnodeAdminResource, TextProviderEngine %>' runat="server"></asp:Localize>
            </p>
        </div>

        <div>
            <zn:Spacer ID="Spacer1" runat="server" SpacerHeight="5" SpacerWidth="3" />
        </div>
    </div>

    <div>
        <ajaxToolKit:TabContainer ID="tabProviderEngine" runat="server" ActiveTabIndex="0" OnClientActiveTabChanged="ActiveTabChanged">
            <ajaxToolKit:TabPanel ID="pnlPromotionTypes" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="TabTitlePromotions" Text='<%$ Resources:ZnodeAdminResource, TabTitlePromotions %>' runat="server"></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <zn:PromotionTypes ID="PromotionTypes" runat="server" />
                </ContentTemplate>
            </ajaxToolKit:TabPanel>

            <ajaxToolKit:TabPanel ID="pnlShippingTypes" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="TabTitleShipping" Text='<%$ Resources:ZnodeAdminResource, TabTitleShipping %>' runat="server"></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <zn:ShippingTypes ID="ShippingTypes" runat="server" />
                </ContentTemplate>
            </ajaxToolKit:TabPanel>

            <ajaxToolKit:TabPanel ID="pnlSupplierTypes" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="TabTitleSuppliers" Text='<%$ Resources:ZnodeAdminResource, TabTitleSuppliers %>' runat="server"></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <zn:SupplierTypes ID="SupplierTypes" runat="server" />
                </ContentTemplate>
            </ajaxToolKit:TabPanel>

            <ajaxToolKit:TabPanel ID="pnlTaxTypes" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="TabTitleTaxes" Text='<%$ Resources:ZnodeAdminResource, TabTitleTaxes %>' runat="server"></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <zn:TaxTypes ID="TaxTypes" runat="server" />
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
        </ajaxToolKit:TabContainer>
    </div>
</asp:Content>
