<%@ Page Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Promotions.Add" MasterPageFile="~/Themes/Standard/edit.master" ValidateRequest="false" CodeBehind="Add.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="ProviderType" Src="~/Controls/Default/Providers/ProviderType.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:ProviderType ID="ProviderType" runat="server" />
</asp:Content>
