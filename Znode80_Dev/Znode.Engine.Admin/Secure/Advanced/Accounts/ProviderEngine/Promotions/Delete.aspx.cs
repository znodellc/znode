using System;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Promotions
{
	public partial class Delete : System.Web.UI.Page
	{
		private ProviderTypeAdmin _providerTypeAdmin;
		private DiscountType _promoType;

		public string PromotionTypeClassName { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			_providerTypeAdmin = new ProviderTypeAdmin();
			_promoType = _providerTypeAdmin.GetDiscountTypeById(int.Parse(Request.Params["itemid"]));

			if (_promoType != null)
			{
				PromotionTypeClassName = _promoType.ClassName;
			}
			else
			{
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoPromotionType").ToString();
				lblErrorMsg.Visible = true;
				btnDelete.Enabled = false;
			}
		}

		protected void BtnDelete_Click(object sender, EventArgs e)
		{
			try
			{
				var success = _providerTypeAdmin.DeleteDiscountType(int.Parse(Request.Params["itemid"]));
				var source = this.GetGlobalResourceObject("ZnodeAdminResource", "TextDeletePromotionType") + _promoType.ClassName;

				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, source, _promoType.ClassName);

				if (!success)
				{
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnableToDeletePromotionType") + _promoType.ClassName + this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPromotionAssociations");
					lblErrorMsg.Visible = true;
				}
				else
				{
					RedirectBacktoList();
				}
			}
			catch (Exception ex)
			{
				ZNodeLogging.LogMessage(ex.Message);
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnableToDeletePromotionType") + _promoType.ClassName + this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPromotionAssociations");
				lblErrorMsg.Visible = true;
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			RedirectBacktoList();
		}

		private void RedirectBacktoList()
		{
			Response.Redirect("~/Secure/Advanced/Accounts/ProviderEngine/Default.aspx?type=promotions");
		}
	}
}