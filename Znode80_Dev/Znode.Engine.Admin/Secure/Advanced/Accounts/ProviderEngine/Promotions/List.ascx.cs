﻿using System;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;

namespace Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Promotions
{
	public partial class List : System.Web.UI.UserControl
	{
		private ProviderTypeAdmin _providerTypeAdmin = new ProviderTypeAdmin();
		private string _addPageLink = "~/Secure/Advanced/Accounts/ProviderEngine/Promotions/Add.aspx";
		private string _deletePageLink = "~/Secure/Advanced/Accounts/ProviderEngine/Promotions/Delete.aspx";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				BindPromotionTypes();
			}
		}

		protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "Page")
			{
				var index = Convert.ToInt32(e.CommandArgument);
				var selectedRow = uxGrid.Rows[index];
				var tableCell = selectedRow.Cells[0];
				var id = tableCell.Text;
					
				if (e.CommandName == "Edit")
				{
					Response.Redirect(_addPageLink + "?itemid=" + id + "&type=promotions");
				}
				else if (e.CommandName == "Delete")
				{
					Response.Redirect(_deletePageLink + "?itemid=" + id);
				}
			}
		}

		protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			uxGrid.PageIndex = e.NewPageIndex;
			BindPromotionTypes();
		}

		protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
		{
			BindPromotionTypes();
		}

		protected void BtnAddPromotionType_Click(object sender, EventArgs e)
		{
			Response.Redirect(_addPageLink + "?type=promotions");
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/Secure/Advanced/Accounts/ProviderEngine/Default.aspx?type=promotions");
		}

		private void BindPromotionTypes()
		{
			var promotionTypes = _providerTypeAdmin.GetDiscountTypes();
			promotionTypes.Sort("ClassName");

			uxGrid.DataSource = promotionTypes;
			uxGrid.DataBind();
		}
	}
}