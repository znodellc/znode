﻿using System;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;

namespace Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Shipping
{
	public partial class List : System.Web.UI.UserControl
	{
		private ProviderTypeAdmin _providerTypeAdmin = new ProviderTypeAdmin();
		private string _addPageLink = "~/Secure/Advanced/Accounts/ProviderEngine/Shipping/Add.aspx";
		private string _deletePageLink = "~/Secure/Advanced/Accounts/ProviderEngine/Shipping/Delete.aspx";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				BindShippingTypes();
			}
		}

		protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "Page")
			{
				var index = Convert.ToInt32(e.CommandArgument);
				var selectedRow = uxGrid.Rows[index];
				var tableCell = selectedRow.Cells[0];
				var id = tableCell.Text;

				if (e.CommandName == "Edit")
				{
					Response.Redirect(_addPageLink + "?itemid=" + id + "&type=shipping");
				}
				else if (e.CommandName == "Delete")
				{
					Response.Redirect(_deletePageLink + "?itemid=" + id);
				}
			}
		}

		protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			uxGrid.PageIndex = e.NewPageIndex;
			BindShippingTypes();
		}

		protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
		{
			BindShippingTypes();
		}

		protected void BtnAddShippingType_Click(object sender, EventArgs e)
		{
			Response.Redirect(_addPageLink + "?type=shipping");
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/Secure/Advanced/Accounts/ProviderEngine/Default.aspx?type=shipping");
		}

		private void BindShippingTypes()
		{
			var shippingTypes = _providerTypeAdmin.GetShippingTypes();
			shippingTypes.Sort("ClassName");

			uxGrid.DataSource = shippingTypes;
			uxGrid.DataBind();
		}
	}
}