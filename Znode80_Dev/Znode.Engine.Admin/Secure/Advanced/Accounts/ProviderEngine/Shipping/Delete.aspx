﻿<%@ Page Language="C#" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Shipping.Delete" MasterPageFile="~/Themes/Standard/edit.master" CodeBehind="Delete.aspx.cs" %>

<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
    <h1>
        <asp:Label ID="lblDeleteShippingType" runat="server">
            <asp:Localize runat="server" ID="TitleDeleteShippingType" Text='<%$ Resources:ZnodeAdminResource, TitleDeleteShippingType %>'></asp:Localize><%=ShippingTypeClassName%>
        </asp:Label>
    </h1>

    <div>
        <zn:Spacer ID="Spacer1" runat="server" SpacerHeight="10" SpacerWidth="3" />
    </div>

    <p>
        <asp:Localize runat="server" ID="TextConfirmDeleteShippingType" Text='<%$ Resources:ZnodeAdminResource, TextConfirmDeleteShippingType %>'></asp:Localize>
    </p>
    <p>
        <asp:Localize runat="server" ID="TextShippingTypeAssociation" Text='<%$ Resources:ZnodeAdminResource, TextShippingTypeAssociation %>'></asp:Localize>
    </p>

    <p>
        <asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error" /></p>

    <div>
        <zn:Spacer ID="Spacer2" runat="server" SpacerHeight="10" SpacerWidth="3" />
    </div>

    <div>
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnDelete_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' CausesValidation="true" ID="btnDelete" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" ID="btnCancel" />
    </div>

    <div>
        <zn:Spacer ID="Spacer3" runat="server" SpacerHeight="15" SpacerWidth="3" />
    </div>
</asp:Content>
