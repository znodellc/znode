﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Shipping {
    
    
    public partial class Delete {
        
        /// <summary>
        /// lblDeleteShippingType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblDeleteShippingType;
        
        /// <summary>
        /// TitleDeleteShippingType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize TitleDeleteShippingType;
        
        /// <summary>
        /// Spacer1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Znode.Engine.Admin.Controls.Default.Spacer Spacer1;
        
        /// <summary>
        /// TextConfirmDeleteShippingType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize TextConfirmDeleteShippingType;
        
        /// <summary>
        /// TextShippingTypeAssociation control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize TextShippingTypeAssociation;
        
        /// <summary>
        /// lblErrorMsg control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblErrorMsg;
        
        /// <summary>
        /// Spacer2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Znode.Engine.Admin.Controls.Default.Spacer Spacer2;
        
        /// <summary>
        /// btnDelete control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Znode.Engine.Common.CustomClasses.WebControls.Button btnDelete;
        
        /// <summary>
        /// btnCancel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Znode.Engine.Common.CustomClasses.WebControls.Button btnCancel;
        
        /// <summary>
        /// Spacer3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Znode.Engine.Admin.Controls.Default.Spacer Spacer3;
    }
}
