﻿using System;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Shipping
{
	public partial class Delete : System.Web.UI.Page
	{
		private ProviderTypeAdmin _providerTypeAdmin;
		private ShippingType _shippingType;

		public string ShippingTypeClassName { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			_providerTypeAdmin = new ProviderTypeAdmin();
			_shippingType = _providerTypeAdmin.GetShippingTypeById(int.Parse(Request.Params["itemid"]));

			if (_shippingType != null)
			{
				ShippingTypeClassName = _shippingType.ClassName;
			}
			else
			{
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoShippingType").ToString();
				lblErrorMsg.Visible = true;
				btnDelete.Enabled = false;
			}
		}

		protected void BtnDelete_Click(object sender, EventArgs e)
		{
			try
			{
				var success = _providerTypeAdmin.DeleteShippingType(int.Parse(Request.Params["itemid"]));
				var source = this.GetGlobalResourceObject("ZnodeAdminResource", "TextDeleteShippingType") + _shippingType.ClassName;

				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, source, _shippingType.ClassName);

				if (!success)
				{
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnableToDeleteShippingType") + _shippingType.ClassName + this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorShippingAssociations");
					lblErrorMsg.Visible = true;
				}
				else
				{
					RedirectBacktoList();
				}
			}
			catch (Exception ex)
			{
				ZNodeLogging.LogMessage(ex.Message);
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnableToDeleteShippingType") + _shippingType.ClassName + this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorShippingAssociations");
				lblErrorMsg.Visible = true;
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			RedirectBacktoList();
		}

		private void RedirectBacktoList()
		{
			Response.Redirect("~/Secure/Advanced/Accounts/ProviderEngine/Default.aspx?type=shipping");
		}
	}
}