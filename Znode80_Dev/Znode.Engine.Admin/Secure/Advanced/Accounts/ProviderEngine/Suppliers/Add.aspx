﻿<%@ Page Language="C#" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Suppliers.Add" MasterPageFile="~/Themes/Standard/edit.master" CodeBehind="Add.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="ProviderType" Src="~/Controls/Default/Providers/ProviderType.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:ProviderType ID="ProviderType" runat="server" />
</asp:Content>
