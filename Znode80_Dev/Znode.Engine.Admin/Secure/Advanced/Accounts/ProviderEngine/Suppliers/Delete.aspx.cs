﻿using System;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Suppliers
{
	public partial class Delete : System.Web.UI.Page
	{
		private ProviderTypeAdmin _providerTypeAdmin;
		private SupplierType _supplierType;

		public string SupplierTypeClassName { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			_providerTypeAdmin = new ProviderTypeAdmin();
			_supplierType = _providerTypeAdmin.GetSupplierTypeById(int.Parse(Request.Params["itemid"]));

			if (_supplierType != null)
			{
				SupplierTypeClassName = _supplierType.ClassName;
			}
			else
			{
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoSupplierType").ToString();
				lblErrorMsg.Visible = true;
				btnDelete.Enabled = false;
			}
		}

		protected void BtnDelete_Click(object sender, EventArgs e)
		{
			try
			{
				var success = _providerTypeAdmin.DeleteSupplierType(int.Parse(Request.Params["itemid"]));
				var source = this.GetGlobalResourceObject("ZnodeAdminResource", "TextDeleteSupplierType") + _supplierType.ClassName;

				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, source, _supplierType.ClassName);

				if (!success)
				{
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnableToDeleteSupplierType") + _supplierType.ClassName + this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSupplierAssociations");
					lblErrorMsg.Visible = true;
				}
				else
				{
					RedirectBacktoList();
				}
			}
			catch (Exception ex)
			{
				ZNodeLogging.LogMessage(ex.Message);
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnableToDeleteSupplierType") + _supplierType.ClassName + this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSupplierAssociations");
				lblErrorMsg.Visible = true;
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			RedirectBacktoList();
		}

		private void RedirectBacktoList()
		{
			Response.Redirect("~/Secure/Advanced/Accounts/ProviderEngine/Default.aspx?type=suppliers");
		}
	}
}