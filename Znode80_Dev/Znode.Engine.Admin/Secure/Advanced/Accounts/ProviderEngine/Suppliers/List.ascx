﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Znode.Engine.Admin.Secure.Advanced.Accounts.ProviderEngine.Suppliers.List" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:UpdatePanel ID="updSupplierList" runat="server">
    <ContentTemplate>
        <div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAddSupplierType" runat="server" CausesValidation="False" ButtonType="Button" OnClick="BtnAddSupplierType_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddSupplierType %>' ButtonPriority="Primary" />
            </div>
        </div>

        <div class="ClearBoth" align="left"></div>

        <div class="Form">
            <div>
                <asp:Label ID="lblErrorMsg" EnableViewState="false" runat="server" CssClass="Error" />
            </div>

            <div>
                <zn:Spacer ID="Spacer1" runat="server" SpacerHeight="5" SpacerWidth="3" />
            </div>

            <h4 class="GridTitle">
                <asp:Localize runat="server" ID="GridTitleSupplierTypes" Text='<%$ Resources:ZnodeAdminResource, GridTitleSupplierTypes %>'></asp:Localize>
            </h4>

            <asp:GridView ID="uxGrid" runat="server"
                CssClass="Grid"
                AllowSorting="True"
                AllowPaging="True"
                PageSize="20"
                AutoGenerateColumns="False"
                CellPadding="4"
                Width="100%"
                GridLines="None"
                CaptionAlign="Left"
                OnPageIndexChanging="UxGrid_PageIndexChanging"
                OnRowCommand="UxGrid_RowCommand"
                OnRowDeleting="UxGrid_RowDeleting"
                EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyTextSupplierType %>'>
                <Columns>
                    <asp:BoundField DataField="SupplierTypeId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="ClassName" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleClassName %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="30%" />
                    <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="30%" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleEnabled %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img runat="server" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>' alt="" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' ButtonType="Link">
                        <ControlStyle CssClass="actionlink" />
                    </asp:ButtonField>
                    <asp:ButtonField CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' ButtonType="Link">
                        <ControlStyle CssClass="actionlink" />
                    </asp:ButtonField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
