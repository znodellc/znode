﻿<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="true"
    Title="Maintenance" %>

<script runat="server">
    protected void Page_Init(object sender, EventArgs e)
    {
        Session["SelectedMenuItem"] = Request.Url.PathAndQuery;
    }    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h1>
        <asp:Localize ID="TitleAccounts" Text='<%$ Resources:ZnodeAdminResource,TitleAccounts%>' runat="server"></asp:Localize></h1>
    <hr />
    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/SITE-admin.png" />
        </div>
        <div class="Shortcut"><a id="A4" href="~/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx" runat="server">
            <asp:Localize ID="LinkStore" Text='<%$ Resources:ZnodeAdminResource,TitleStoreAdministrators%>' runat="server"></asp:Localize></a></div>
        <div class="LeftAlign">
            <p>
                <asp:Localize ID="TextAdministratorAccount" Text='<%$ Resources:ZnodeAdminResource,TextAdministratorAccount%>' runat="server"></asp:Localize></p>
        </div>
    </div>

    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/RotateKey.png" />
        </div>
        <div class="Shortcut">
            <a id="A3" href="~/Secure/Advanced/Accounts/RotateKey/Key.aspx" runat="server">
                <asp:Localize ID="LinkRotate" Text='<%$ Resources:ZnodeAdminResource,LinkRotate%>' runat="server"></asp:Localize></a>
        </div>
        <p>
            <asp:Localize ID="TextGenerateKey" Text='<%$ Resources:ZnodeAdminResource,TextGenerateKey%>' runat="server"></asp:Localize>
        </p>
    </div>

    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/TaxRuleEngine.png" />
        </div>
        <div class="Shortcut">
            <a id="A2" href="~/Secure/Advanced/Accounts/ProviderEngine/Default.aspx" runat="server">
                <asp:Localize ID="TitleProviderEngine" Text='<%$ Resources:ZnodeAdminResource,TitleProviderEngine%>' runat="server"></asp:Localize></a>
        </div>
        <p>
            <asp:Localize ID="TextBusinessProviders" Text='<%$ Resources:ZnodeAdminResource,TextBusinessProviders%>' runat="server"></asp:Localize>
        </p>
    </div>

    <h1>
        <asp:Localize ID="TitleSearch" Text='<%$ Resources:ZnodeAdminResource,TitleSearch%>' runat="server"></asp:Localize></h1>
    <hr />
    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/ManageIndex.png" />
        </div>
        <div class="LeftAlign">
            <div class="Shortcut"><a id="A5" href="~/Secure/Advanced/Search/SearchIndex/Default.aspx" runat="server">
                <asp:Localize ID="TitleManage" Text='<%$ Resources:ZnodeAdminResource,TitleManageIndex%>' runat="server"></asp:Localize></a></div>
            <p>
                <asp:Localize ID="TextSearchEngine" Text='<%$ Resources:ZnodeAdminResource,TextSearchEngine%>' runat="server"></asp:Localize></p>
        </div>
    </div>
</asp:Content>
