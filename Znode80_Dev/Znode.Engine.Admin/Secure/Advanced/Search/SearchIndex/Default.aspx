<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/Themes/Standard/content.master" Inherits="Znode.Engine.Admin.Secure.Advanced.Search.SearchIndex.Default" Title="Manage Index" CodeBehind="Default.aspx.cs" %>

<%@ Register Src="~/Secure/Advanced/Search/SearchIndex/IndexStatusList.ascx" TagPrefix="znode" TagName="IndexStatusList" %>
<asp:Content ID="IndexManagement" ContentPlaceHolderID="uxMainContent" runat="Server">
    <script type="text/javascript">
        function activeTabChanged(sender, e) {
            var activeTabIndex = document.getElementById('<%=hdnActiveTabIndex.ClientID%>');
		    activeTabIndex.value = sender.get_activeTabIndex();
		    document.getElementById('<%= statusMessage.ClientID %>').innerHTML = '';
        }
    </script>

    <div id="ManageIndex">
        <div>
        <div class="LeftFloat">
              <h1>
                  <asp:Localize runat="server" ID="TitleManageIndex" Text='<%$ Resources:ZnodeAdminResource, TitleManageIndex %>'></asp:Localize>
              </h1>
        </div>
        <div class="RightFloat">
        <div>
            <zn:Button runat="server" Width="150px" ButtonType="EditButton" OnClick="List_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToSearch %>' ID="List" />
        </div>
        </div>
     </div>
    <div class="ClearBoth" align="left">
        <hr />
    </div>
        <p>
            <asp:Localize runat="server" ID="TextIndexNote" Text='<%$ Resources:ZnodeAdminResource, TextIndexNote %>'></asp:Localize>
        </p>
    
        <div>
            <asp:HiddenField ID="hdnActiveTabIndex" runat="server" Value="0" />
                        <asp:UpdatePanel runat="server" ID="upIndexes">
                            <ContentTemplate>
                                <div class="CustomMessage">
                                    <label runat="server" id="statusMessage" />
                                </div>
                               
                                <div class="ImageAlign">
                                    <h2>
                                        <asp:Localize runat="server" ID="SubTitleCreateIndex" Text='<%$ Resources:ZnodeAdminResource, SubTitleCreateIndex %>'></asp:Localize>
                                    </h2>
                                    <p>
                                        <asp:Localize runat="server" ID="TextCreateIndex" Text='<%$ Resources:ZnodeAdminResource, SubTextCreateIndex %>'></asp:Localize>
                                    </p>
                                    <div>
                                        <zn:Button runat="server" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCreateIndex %>' ID="btnCreate"
                                            OnCommand="BtnCreate_Click" Width="150px" />
                                    </div>
                                </div>
                                <div class="ImageAlign">
                                    <h2>
                                        <asp:Localize runat="server" ID="SubTitleEnableTriggers" Text='<%$ Resources:ZnodeAdminResource, SubTitleEnableTriggers %>'></asp:Localize>
                                    </h2>
                                    <p>
                                        <asp:Localize runat="server" ID="SubTextEnableTriggers" Text='<%$ Resources:ZnodeAdminResource, SubTextEnableTriggers %>'></asp:Localize>
                                    </p>
                                    <div>
                                        <zn:Button runat="server" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonDisableTriggers %>' ID="btnDisableTriggers"
                                            OnCommand="BtnDisableTriggers_Click" Width="150px"/>
                                    </div>
                                </div>
                                <div class="ImageAlign">
                                    <h2>
                                        <asp:Localize runat="server" ID="SubTitleEnableWinService" Text='<%$ Resources:ZnodeAdminResource, SubTitleEnableWinService %>'></asp:Localize>
                                    </h2>
                                    <p>
                                        <asp:Localize runat="server" ID="SubTextEnableWinService" Text='<%$ Resources:ZnodeAdminResource, SubTextEnableWinService %>'></asp:Localize>
                                    </p>
                                    <div>
                                        <zn:Button runat="server" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonDisableWinService %>' ID="btnDisableServices"
                                            OnCommand="BtnDisableWinservice_Click" Width="150px"/>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <h4 class="GridTitle">
                                <div style="float:left;width:70%">
                                    <asp:Localize runat="server" ID="GridTitleServiceMonitor" Text='<%$ Resources:ZnodeAdminResource, GridTitleServiceMonitor %>'></asp:Localize>
                                </div>  
                                <div style="float:right">
                                     <div>
                                         <zn:Button runat="server" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonRefresh %>' ID="btnRefresh"
                                            OnCommand="btnRefresh_Click" Width="150px"/>
                                     </div>   
                                </div>
                                    <h4></h4>
                                    <div class="ClearBoth">
                                    </div>
                                    <znode:IndexStatusList ID="ucIndexStatusList" runat="server" />
                                    </label>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                    <h4></h4>
                                </h4>
                            </ContentTemplate>
                             <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ucIndexStatusList" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <asp:UpdateProgress ID="uxIndexUpdateProgress" runat="server" AssociatedUpdatePanelID="upIndexes" DisplayAfter="10">
                            <ProgressTemplate>
                                <div id="ajaxProgressBg"></div>
                                <div id="ajaxProgress"></div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
             
        </div>
    </div> 
    <script language="javascript" type="text/javascript">
        var is_ie = (navigator.userAgent.indexOf('MSIE') >= 0) ? 1 : 0;
        var is_ie5 = (navigator.appVersion.indexOf("MSIE 5.5") != -1) ? 1 : 0;
        var xmlHttp;

        /* This function requests the HTTPRequest, will be used to render the Dynamic content html markup 
        * and it will call HandleResponse to handle the response
        */
        function GetChildGrid(Id) {

            // Get the Div
            var childGridDiv = document.getElementById("div" + Id);
            var img = document.getElementById('img' + Id);

            if (childGridDiv) {
                // Is already ChildGrid shown. If not shown
                if (childGridDiv.style.display == "none") {

                    // If the Child Grid is not fetched, then go and fetch it.
                    if (document.getElementById('hid' + Id).value == '0') {
                        // Calling with dummy parameter Time for calling server
                        var startDateTime = new Date();

                        var url = 'StatusGridBuilder.aspx?ID=' + Id + '&Time=' + startDateTime.getTime();
                        xmlHttp = createAjaxObject();
                        if (xmlHttp) {
                            xmlHttp.open('get', url, true);
                            xmlHttp.onreadystatechange = function () {
                                HandleResponse(Id);
                            }
                            xmlHttp.send(null);
                        }
                    }
                    else {
                        childGridDiv.style.display = "block";
                        img.src = "../../../../../Themes/Images/minus.png";
                    }
                }
                else { // Already Child Grid Shown
                    childGridDiv.style.display = "none";
                    img.src = "../../../../../Themes/Images/plus.png";
                }
            }
        }
        /* This function is used to handler the http response */
        function HandleResponse(Id) {

            var childGridDiv = document.getElementById("div" + Id);
            var img = document.getElementById('img' + Id);

            // If Response completed
            if (xmlHttp.readyState == 4) {

                if (xmlHttp.status == 200) {

                    // Here is the response
                    var str = xmlHttp.responseText;

                    childGridDiv.innerHTML = str;

                    xmlHttp.abort();

                    // Mark the flag, Child Grid is fetched from server
                    document.getElementById('hid' + Id).value = '1';
                }
                else {
                    childRow.style.display = "none";
                    img.src = "../../../../../Themes/Images/plus.png";
                }
            }
            else {
                childGridDiv.style.display = "block";
                img.src = "../../../../../Themes/Images/minus.png";
            }
        }
        /* function to create Ajax object */
        function createAjaxObject() {
            var ro;
            var browser = navigator.appName;
            if (browser == "Microsoft Internet Explorer") {
                if (xmlHttp != null) {
                    xmlHttp.abort();
                }
                ro = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else {
                if (xmlHttp != null) {
                    xmlHttp.abort();
                }
                ro = new XMLHttpRequest();
            }
            return ro;
        }

        /* Get the XML Http Object */
        function GetXmlHttpObject(handler) {
            var objXmlHttp = null;
            if (is_ie) {
                var strObjName = (is_ie5) ? 'Microsoft.XMLHTTP' : 'Msxml2.XMLHTTP';

                try {
                    objXmlHttp = new ActiveXObject(strObjName);
                    objXmlHttp.onreadystatechange = handler;
                }
                catch (e) {
                    alert('Object could not be created');
                    return;
                }
            }
            return objXmlHttp;
        }

        function xmlHttp_Get(xmlhttp, url) {
            xmlhttp.open('GET', url, true);
            xmlhttp.send(null);
        }
    </script>
</asp:Content>
