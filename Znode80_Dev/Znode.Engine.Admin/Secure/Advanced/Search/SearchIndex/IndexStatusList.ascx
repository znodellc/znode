﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IndexStatusList.ascx.cs" Inherits="Znode.Engine.Admin.Secure.Advanced.Search.SearchIndex.IndexStatusList" %>

<div class="ManageIndexText">
    <asp:UpdatePanel ID="pnl" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvIndexStatusList" CellPadding="4" runat="server" CssClass="IndexGrid" AutoGenerateColumns="False" PageSize="10"
                AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="LuceneIndexMonitorID" ViewStateMode="Enabled" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyIndexText %>'>
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <input id='hid<%# Eval("LuceneIndexMonitorID") %>') value='0' type="hidden" />
                            <a href="javascript:GetChildGrid('<%# Eval("LuceneIndexMonitorID") %>');">
                                <img id='img<%# Eval("LuceneIndexMonitorID") %>' alt="plus" border="0" src="../../../../../Themes/Images/plus.png" />
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleMonitorID %>' DataField="LuceneIndexMonitorID" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10" HeaderStyle-Width="10" />
                    <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSourceID %>' DataField="SourceID" HeaderStyle-HorizontalAlign="Left" NullDisplayText=" " />
                    <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSourceType %>' DataField="SourceType" HeaderStyle-HorizontalAlign="Left" NullDisplayText=" " />
                    <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSourceTransactionType %>' DataField="SourceTransactionType" HeaderStyle-HorizontalAlign="Left" NullDisplayText=" " ItemStyle-Width="50" HeaderStyle-Width="50" />
                    <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTransactionDate %>' DataField="TransactionDateTime" HeaderStyle-HorizontalAlign="Left" NullDisplayText=" " />
                    <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsDuplicate %>' DataField="IsDuplicate" HeaderStyle-HorizontalAlign="left" NullDisplayText=" " />
                    <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleColumnChange %>' DataField="AffectedType" HeaderStyle-HorizontalAlign="Left" NullDisplayText=" " />
                    <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleModifiedBy %>' DataField="IndexerStatusChangedBy" HeaderStyle-HorizontalAlign="Left" NullDisplayText=" " />

                    <asp:TemplateField>
                        <ItemTemplate>
                            </td></tr>
                        <tr>
                            <td id="td<%# Eval("LuceneIndexMonitorID") %>" colspan="9">
                                <div id="div<%# Eval("LuceneIndexMonitorID") %>" style="width: 100%; display: none;">
                                </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EditRowStyle CssClass="EditRowStyle" />
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <SelectedRowStyle CssClass="SelectedRowStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
            <br />
            <asp:HiddenField ID="hdnpage" runat="server" />
            <asp:HiddenField ID="hdnTotal" runat="server" />
            <asp:LinkButton ID="lbfirst" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkFirst %>' CssClass="Button" OnClick="lbfirst_Click"></asp:LinkButton>
            <asp:LinkButton ID="lbprev" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkPrevious %>' CssClass="Button" OnClick="lbprev_Click"></asp:LinkButton>
            <asp:LinkButton ID="lbnext" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkNext %>' CssClass="Button" OnClick="lbnext_Click"></asp:LinkButton>
            <asp:LinkButton ID="lblast" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkLast %>' CssClass="Button" OnClick="lblast_Click"></asp:LinkButton>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
