using System;
using System.Web.UI;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services;

namespace  Znode.Engine.Admin.Secure.Advanced.Search.SearchIndex
{
    /// <summary>
    /// Represents the SiteAdmin - Znode.Engine.Admin.Secure.Advanced.Search.Default class
    /// </summary>
	public partial class Default : System.Web.UI.Page
	{
		private const string IndexStarted = "Started";
		private const string IndexCompleted = "Completed";
		private const string IndexFailed = "Failed";
        private string ListPage = "~/Secure/Advanced/Default.aspx";

		protected void Page_Load(object sender, EventArgs e)
		{
			
            LuceneIndexService luceneIndexService = new LuceneIndexService();
            btnCreate.Enabled = true;

			if (!IsPostBack)
			{
				BindButtons();
			}

			ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Reset", "javascript:_isset=0;", true);
		}
        protected void List_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListPage);
        }

       

		public void BtnCreate_Click(object sender, EventArgs e)
		{
			var account = Session["AccountObject"] as Account;
			LuceneIndexService luceneIndexService = new LuceneIndexService();
			luceneIndexService.CreateLuceneIndexButton(0);
            statusMessage.InnerText = this.GetGlobalResourceObject("ZnodeAdminResource", "TextCreateIndex").ToString();
            ucIndexStatusList.BindData(0);
           
		}

		public void BtnDisableTriggers_Click(object sender, EventArgs e)
		{
			int intFlag = 0;
			LuceneIndexService luceneIndexService = new LuceneIndexService();

			if (btnDisableTriggers.Text == this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonEnableTriggers").ToString())
			{
                btnDisableTriggers.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonDisableTriggers").ToString();
				intFlag = 1;
				statusMessage.InnerText = this.GetGlobalResourceObject("ZnodeAdminResource", "TextTriggersEnabled").ToString();
			}
			else
			{
                btnDisableTriggers.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonEnableTriggers").ToString();
			    statusMessage.InnerText = this.GetGlobalResourceObject("ZnodeAdminResource", "TextTriggersDisabled").ToString();
			}

			luceneIndexService.SwitchLuceneTrigger(intFlag);
		}

		public void BtnDisableWinservice_Click(object sender, EventArgs e)
		{
			var intFlag = 0;
			var luceneIndexService = new LuceneIndexService();

			intFlag = luceneIndexService.IsLucceneWinserviceDisable();
			luceneIndexService.SwitchWindowsService(intFlag);

			if (intFlag == 1)
			{
                statusMessage.InnerText = this.GetGlobalResourceObject("ZnodeAdminResource", "TextEnableWinService").ToString();
                btnDisableServices.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonDisableWinService").ToString();
			}
			else
			{
				statusMessage.InnerText = this.GetGlobalResourceObject("ZnodeAdminResource", "TextDisableWinService").ToString();
				btnDisableServices.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonEnableWinService").ToString();
			}
		}

		public void BindButtons()
		{
			LuceneIndexService luceneIndexService = new LuceneIndexService();
			int triggerFlag = 1;

			triggerFlag = luceneIndexService.IsLuceneTriggersDisabled();

			if (triggerFlag == 0)
                btnDisableTriggers.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonDisableTriggers").ToString();
			else
                btnDisableTriggers.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonEnableTriggers").ToString();

			int serviceFlag = 1;
			serviceFlag = luceneIndexService.IsLucceneWinserviceDisable();

			if (serviceFlag == 1)
                btnDisableServices.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonEnableWinService").ToString();
			else if (serviceFlag == 0)
                btnDisableServices.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonDisableWinService").ToString();
			else
			{
                btnDisableServices.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonEnableWinService").ToString();
				btnDisableServices.Enabled = false;
				statusMessage.InnerText = this.GetGlobalResourceObject("ZnodeAdminResource", "TextNoWindowsService").ToString();
			}
		}

		public void ClearStatusMessage(object sender, EventArgs e)
		{
			statusMessage.InnerText = "";
		}
        public void btnRefresh_Click(object sender, EventArgs e)
        {
            ucIndexStatusList.BindData(0);
        }
	}
}