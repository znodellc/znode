﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StatusGridBuilder.aspx.cs" Inherits="Znode.Engine.Common.Znode.Engine.Admin.Secure.Advanced.Search.SearchIndex.StatusGridBuilder" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="gvServerStatus" CssClass="IndexGrid" CellPadding="2" runat="server"
                EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyIndexText %>' AutoGenerateColumns="false" DataKeyNames="LuceneIndexMonitorID"
                Width="80%">
                <Columns>
                    <asp:BoundField DataField="ServerName" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleServerName %>' />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="IndexServerStatus" Text='<%#(ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services.ZNodeStatusNames)Enum.Parse(typeof(ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services.ZNodeStatusNames), Eval("Status").ToString()) %>'
                                runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="StartTime" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStartTime %>' />
                    <asp:BoundField DataField="EndTime" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleEndTime %>' />
                </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
