using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    public partial class Admin_Default : System.Web.UI.Page
    {
        #region Variables
        //Znode Version 7.2.2
        //Vendor Section, Error message for account gets locked - Start 
        //Gets the membership provider details for the key "ZNodeMembershipProvider".

        #region Private Variables
        private MembershipProvider _membershipProvider = Membership.Providers["ZNodeMembershipProvider"];
        #endregion

        #region Public Variables
        public MembershipProvider MembershipProvider
        {
            get { return _membershipProvider; }
            set { _membershipProvider = value; }
        }
        #endregion

        //Vendor Section, Error message for account gets locked - End
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {                
                Session.Abandon();

                FormsAuthentication.SignOut();
            }

            //instantiated just to trigger licensing > Do not remove!
            ZNodeHelper hlp = new ZNodeHelper();

            //Set input focus on the page load
            UserName.Focus();

            string inURL = Request.Url.ToString();
            //check if SSL is required
            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {             
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }
            //Forgot password link
            string link = "~/MallAdmin/ForgotPassword.aspx";
            forgotPasswordLink.HRef = link.ToLower().Replace("https://", "http://");

            link = Request.Url.GetLeftPart(UriPartial.Authority) + Response.ApplyAppPathModifier("Signup.aspx");
            SignupLink.HRef = link.ToLower().Replace("https://", "http://");

            if (!Page.IsPostBack)
            {
                //clear Session and Cookies
                 Session.RemoveAll();
                 Request.Cookies.Clear();
            }
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            ZNodeUserAccount userAcct = new ZNodeUserAccount();
            bool loginSuccess = userAcct.Login(ZNodeConfigManager.SiteConfig.PortalID, UserName.Text.Trim(), Password.Text.Trim());

            if (loginSuccess)
            {
                string retValue = string.Empty;

                bool isLastPasswordChanged = ZNodeUserAccount.CheckLastPasswordChangeDate((Guid)userAcct.UserID, out retValue);

                if (!isLastPasswordChanged)
                {
                    ZNode.Libraries.DataAccess.Service.AccountService acctService = new ZNode.Libraries.DataAccess.Service.AccountService();
                    ZNode.Libraries.DataAccess.Entities.Account account = acctService.GetByAccountID(userAcct.AccountID);

                    // Set Error Code to session Object
                    Session.Add("ErrorCode", retValue);

                    // Get account and set to session
                    Session.Add("AccountObject", account);

                    Response.Redirect("~/MallAdmin/ResetPassword.aspx");
                }
                else
                {
                    bool isLoginEnabled = false;
                    int profileId = userAcct.GetAccountPortalProfileID(userAcct.AccountID, ZNodeConfigManager.SiteConfig.PortalID);

                    if (profileId != 0)
                    {
                        ProfileService profileService = new ProfileService();
                        ZNode.Libraries.DataAccess.Entities.Profile ProfileEntity = profileService.GetByProfileID(profileId);

                        if (ProfileEntity != null)
                        {
                            // Hold this profile object in the session state
                            HttpContext.Current.Session["ProfileCache"] = ProfileEntity;

                            // Set CurrentUserProfile ProfileID
                            userAcct.ProfileID = ProfileEntity.ProfileID;

                            isLoginEnabled = true;
                        }
                    }

                    if (isLoginEnabled)
                    {
                        //get account and set to session
                        Session.Add(ZNodeSessionKeyType.UserAccount.ToString(), userAcct);

                        FormsAuthentication.SetAuthCookie(UserName.Text.Trim(), false);

                        string returnUrl = "~/MallAdmin/Secure/Product/list.aspx";

                        if (Request.QueryString["ReturnUrl"] != null)
                        {
                            returnUrl = Request.QueryString["ReturnUrl"];
                        }

                        Response.Redirect(returnUrl);
                    }
                    else
                    {
                        FailureText.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorLoginFailure").ToString(); 
                    }
                }
            }
            else
            {
                //Znode Version 7.2.2
                //Vendor Section, Error message for account gets locked - Start 
                //Set the Error messages in case of Account gets locked. And Also display warning messages before the final last two attempts.


                //Gets the user details based on username.
                MembershipUser membershipUser = Membership.GetUser(UserName.Text.Trim());

                if (membershipUser != null)
                {
                    //Check whether user accoung is locked or not.
                    if (membershipUser.IsLockedOut)
                    {
                        FailureText.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorVendorAccountLockedOut").ToString();
                    }
                    else
                    {
                        //Gets current failed password atttemt count for setting the message.
                        AccountHelper accountHelper = new AccountHelper();
                        int inValidAttemtCount = accountHelper.GetUserFailedPasswordAttemptCount((Guid)membershipUser.ProviderUserKey);
                        if (inValidAttemtCount > 0)
                        {
                            //Gets Maximum failed password atttemt count from web.config
                            int maxInvalidPasswordAttemptCount = _membershipProvider.MaxInvalidPasswordAttempts;

                            //Set warning error at (MaxFailureAttemptCount - 2) & (MaxFailureAttemptCount - 1) attempt.
                            FailureText.Text = ((maxInvalidPasswordAttemptCount - inValidAttemtCount) == 2)
                                ? this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountLockedOutAfterTwoAttempt").ToString()
                                : ((maxInvalidPasswordAttemptCount - inValidAttemtCount) == 1)
                                    ? this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountLockedOutAfterOneAttempt").ToString()
                                    : this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorLoginFailure").ToString();

                        }
                        else
                        {
                            FailureText.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorLoginFailure").ToString();
                        }
                    }
                }//Vendor Section, Error message for account gets locked - End
                else
                {
                    FailureText.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorLoginFailure").ToString();
                }
            }
        }
    }
}