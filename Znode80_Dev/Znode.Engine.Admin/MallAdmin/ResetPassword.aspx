﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MallAdmin/Themes/Standard/login.master" CodeBehind="ResetPassword.aspx.cs" Inherits="WebApp.MallAdmin.ResetPassword" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" Namespace="Znode.Engine.Common.CustomClasses.WebControls" Assembly="Znode.Engine.Common" %>
<%@ Register TagPrefix ="ZNode" TagName="ResetPassword" Src="~/MallAdmin/Controls/Default/ResetPassword.ascx" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Login ResetPwdLogin"> 
        <div>
            <ZNode:ResetPassword ID="ResetPassword1" runat="server"></ZNode:ResetPassword>
        </div>
    </div>
</asp:Content>
