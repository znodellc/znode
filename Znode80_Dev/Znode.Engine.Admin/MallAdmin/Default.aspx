<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/Login.master" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Admin_Default" Title="Site Administration" CodeBehind="Default.aspx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <div class="Login">
        <h1>
            <asp:Localize ID="TitleVendor" Text='<%$ Resources:ZnodeAdminResource, TitleVendor%>' runat="server"></asp:Localize></h1>
        <p>
            <asp:Localize runat="server" ID="TextLoginVendor" Text='<%$ Resources:ZnodeAdminResource, TextLoginVendor%>'></asp:Localize>
        </p>
        <div style="margin-bottom: 30px;">
        </div>
        <asp:Panel ID="LoginPanel" runat="server" DefaultButton="LoginButton">
            <div class="FormView">
                <div class="FieldStyle" style="width: 100px;">
                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName"><b>
                        <asp:Localize runat="server" ID="ColumnTitleUsername" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleUsername%>'></asp:Localize></b></asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="UserName" runat="server" autocomplete="off" Width="140px"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredLoginUserName%>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredLoginUserName%>' ValidationGroup="uxLogin"
                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
                <div class="FieldStyle" style="width: 100px;">
                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password"><b>
                        <asp:Localize runat="server" ID="ColumnPassword" Text='<%$ Resources:ZnodeAdminResource, ColumnPassword%>'></asp:Localize></b></asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="Password" runat="server" autocomplete="off" TextMode="Password" Width="140px"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPassword%>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredPassword%>' ValidationGroup="uxLogin"
                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
                <div class="ClearBoth">
                </div>
                <div class="Error">
                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                </div>
                <div>
                    <ZNode:Spacer ID="Spacer4" SpacerHeight="5" SpacerWidth="10" runat="server" />
                </div>
                <div style="padding-left: 105px; height: 20px;">
                    <div>
                        <zn:LinkButton ID="LoginButton" runat="server" CommandName="Login"
                            OnClick="LoginButton_Click" ButtonType="LoginButton" ValidationGroup="uxLogin"
                            ButtonPriority="Normal" Text='<%$ Resources:ZnodeAdminResource, ButtonLogin%>' />
                    </div>
                    <div>
                        <div>
                            <div>
                                <ZNode:Spacer ID="Spacer51" SpacerHeight="15" SpacerWidth="10" runat="server" />
                            </div>
                            <div>
                                <a id="forgotPasswordLink" href='' runat="server">
                                    <asp:Localize runat="server" ID="LinkTextPassword" Text='<%$ Resources:ZnodeAdminResource, LinkTextForgotPassword%>'></asp:Localize></a>
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer6" SpacerHeight="15" SpacerWidth="10" runat="server" />
                            </div>
                            <div>
                                <a id="SignupLink" href='' runat="server">
                                    <asp:Localize runat="server" ID="LinkTextRequestVendor" Text='<%$ Resources:ZnodeAdminResource, LinkTextRequestVendor%>'></asp:Localize></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
