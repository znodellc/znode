using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Forgot Password user control in marketplace.
    /// </summary>
    public partial class Themes_Default_Account_ForgotPassword : System.Web.UI.UserControl
    {
        #region Protected Methods and Events

        /// <summary>
        /// Passwrod Recovery Wizard step change
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PasswordRecoveryWizard_ActiveStepChanged(object sender, EventArgs e)
        {
            // Check active wizard step index
            if (PasswordRecoveryWizard.ActiveStepIndex == 1)
            {
                this.VerifyUser();
            }
            else if (PasswordRecoveryWizard.ActiveStepIndex == 2)
            {
                this.SendMail();
            }
        }
        #endregion

        #region Page Load Methods
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Label lblPageTitle = this.Page.Master.FindControl("lblPageTitle") as Label;
            if (lblPageTitle != null)
            {
                lblPageTitle.Text = "Login";
            }

            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtPasswordTitle.Text");
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Verify the user account
        /// </summary>
        private void VerifyUser()
        {
            string loginName = (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("UserName") as TextBox).Text.Trim();
            string emailID = (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("Email") as TextBox).Text.Trim();

            MembershipUser user = Membership.GetUser(loginName);

            if (user == null)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetFailed, loginName, Request.UserHostAddress.ToString(), null, "Invalid user name", null);

                (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("FailureText") as Literal).Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFailureText").ToString();
                PasswordRecoveryWizard.ActiveStepIndex = 0;
                return;
            }
            else
            {
                ZNodeUserAccount userAcct = new ZNodeUserAccount();
                Account acc = userAcct.GetByUserID(new Guid(user.ProviderUserKey.ToString()));

                if (acc != null)
                {
                    int profileId = userAcct.GetAccountPortalProfileID(acc.AccountID, ZNodeConfigManager.SiteConfig.PortalID);

                    if (profileId == 0)
                    {
                        (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("FailureText") as Literal).Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFailureText").ToString();
                        PasswordRecoveryWizard.ActiveStepIndex = 0;
                        return;
                    }
                }

                // verify user email id
                if (user.Email != emailID)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetFailed, loginName, Request.UserHostAddress.ToString(), null, "Invalid email", null);

                    (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("FailureText") as Literal).Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFailureText").ToString();
                    PasswordRecoveryWizard.MoveTo(PasswordRecoveryWizard.WizardSteps[0]);
                    return;
                }

                // If Security Question and Answer are not set we give them an error message. 
                if (string.IsNullOrEmpty(user.PasswordQuestion))
                {
                    (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("FailureText") as Literal).Text =
                        this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFailureTextSecurityNotSet").ToString();
                    PasswordRecoveryWizard.MoveTo(PasswordRecoveryWizard.WizardSteps[0]);
                }
                else
                {
                    (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("Question") as Literal).Text = user.PasswordQuestion;
                    PasswordRecoveryWizard.MoveTo(PasswordRecoveryWizard.WizardSteps[1]);
                }
            }
        }

        /// <summary>
        /// Send mail to user with the reseted username and password
        /// </summary>
        private void SendMail()
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = ZNodeConfigManager.SiteConfig.StoreName;
            string loginName = (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("UserName") as TextBox).Text.Trim();
            string passwordAnswer = (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("Answer") as TextBox).Text.Trim();

            string CurrentCulture = string.Empty;
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;

            MembershipUser user = Membership.GetUser(loginName);

            string LoginPassword = string.Empty;

            try
            {
                // Resets a user's password to a new one,automatically generated password
                LoginPassword = user.ResetPassword(passwordAnswer);

                // Delete the password
                AccountHelper helperAccess = new AccountHelper();
                helperAccess.DeletePasswordLogByUserId(((Guid)user.ProviderUserKey).ToString());
            }
            catch
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetFailed, loginName, Request.UserHostAddress.ToString(), null, "Invalid secret question/answer", null);

                (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("FailureText") as Literal).Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFailureAnswer").ToString();
                PasswordRecoveryWizard.ActiveStepIndex = 1;
                return;
            }

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetSuccess, loginName, Request.UserHostAddress.ToString(), null, "Invalid secret question/answer", null);

            CurrentCulture = ZNodeCatalogManager.CultureInfo;
            defaultTemplatePath = Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ResetPassword_en.htm");
            if (CurrentCulture != string.Empty)
            {
                templatePath = Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ResetPassword_" + CurrentCulture + ".htm");
            }
            else
            {
                templatePath = Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ResetPassword_en.htm");
            }

            if (!File.Exists(templatePath))
            {
                (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("FailureText") as Literal).Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorGeneralError").ToString();
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetFailed, loginName, Request.UserHostAddress.ToString(), null, "Reset password mail template file is missing in " + ZNodeConfigManager.EnvironmentConfig.ConfigPath, null);
                return;
            }

            StreamReader rw = new StreamReader(templatePath);
            string messageText = rw.ReadToEnd();

			Regex rx1 = new Regex("#UserName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, loginName);

            Regex rx2 = new Regex("#Password#", RegexOptions.IgnoreCase);
            messageText = rx2.Replace(messageText, LoginPassword);

            try
            {
                // Send mail
                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(user.Email, senderEmail, String.Empty, subject, messageText, true);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetSuccess, loginName, Request.UserHostAddress.ToString(), null, ex.Message, null);
                (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("FailureText") as Literal).Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorGeneralError").ToString();
                PasswordRecoveryWizard.ActiveStepIndex = 1;
            }
        }


        /// <summary>
        /// Event is raised when Clear button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param> 
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MallAdmin/default.aspx");
        }


        #endregion
    }
}