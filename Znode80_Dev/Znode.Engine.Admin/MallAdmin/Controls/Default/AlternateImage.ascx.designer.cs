﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Znode.Engine.MallAdmin {
    
    
    public partial class Admin_Controls_Default_AlternateImage {
        
        /// <summary>
        /// Spacer13 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Znode.Engine.MallAdmin.Spacer Spacer13;
        
        /// <summary>
        /// btnAccept control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Znode.Engine.Common.CustomClasses.WebControls.Button btnAccept;
        
        /// <summary>
        /// btnReject control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Znode.Engine.Common.CustomClasses.WebControls.Button btnReject;
        
        /// <summary>
        /// Spacer2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Znode.Engine.MallAdmin.Spacer Spacer2;
        
        /// <summary>
        /// lblCaption control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblCaption;
        
        /// <summary>
        /// Spacer1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Znode.Engine.MallAdmin.Spacer Spacer1;
        
        /// <summary>
        /// GridThumb control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView GridThumb;
    }
}
