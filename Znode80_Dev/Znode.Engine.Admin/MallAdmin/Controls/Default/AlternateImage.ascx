﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Znode.Engine.MallAdmin.Admin_Controls_Default_AlternateImage" CodeBehind="AlternateImage.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>

<script language="javascript" type="text/javascript">
    function DeleteConfirmation() {
        return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDelete")) %>');
    }
</script>

<div>
    <uc1:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
</div>
<div>
    <zn:Button runat="server" ID="btnAccept" ButtonType="EditButton" OnClick="BtnAccept_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAccept %>' />
    <zn:Button runat="server" ID="btnReject" ButtonType="EditButton" OnClick="BtnReject_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonReject %>' />
    <br />
    <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    <asp:Label ID="lblCaption" runat="server" Text="" />
</div>
<div>
    <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
</div>
<div>
    <asp:GridView ID="GridThumb" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
        runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
        CssClass="Grid" Width="100%" GridLines="None" PageSize="10"
        OnRowCommand="GridThumb_RowCommand" OnRowDataBound="GridThumb_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:CheckBox ID="chkSelect" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>' ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img id="imgStatus" src='<%# GetStatusImageIcon(DataBinder.Eval(Container.DataItem, "ReviewStateID"))%>' runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ProductImageId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleImage %>' HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <img id="SwapImage" alt="" src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductPage %>' HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryPage %>' HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <img id="Img2" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowOnCategoryPage").ToString()))%>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton CssClass="actionlink" ID="EditProductView" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CommandArgument='<%# Eval("productimageid") %>'
                        CommandName="Edit" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CommandArgument='<%# Eval("productimageid") %>'
                        CommandName="RemoveItem" CssClass="actionlink" runat="server" OnClientClick="return DeleteConfirmation();" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            <asp:Localize ID="GridImageEmptyData" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridImageEmptyData%>'></asp:Localize>
        </EmptyDataTemplate>
        <RowStyle CssClass="RowStyle" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        <FooterStyle CssClass="FooterStyle" />
    </asp:GridView>
</div>
