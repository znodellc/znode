using System;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Footer user control in marketplace.
    /// </summary>
    public partial class Admin_Secure_Controls_Footer : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private string _FooterCopyrightText = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the directory the images should be pulled from (thumbnail, small, medium or large). You do not need to specify the whole path.
        /// </summary>
        public string FooterCopyrightText
        {
            get { return this._FooterCopyrightText; }
            set { this._FooterCopyrightText = value; }
        }
        #endregion

        #region Page Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["FooterCopyrightText"] != null)
            {
                this.FooterCopyrightText = System.Configuration.ConfigurationManager.AppSettings["FooterCopyrightText"].ToString();
            }
            else
            {
                this.FooterCopyrightText = this.GetGlobalResourceObject("ZnodeAdminResource", "TextFooter").ToString();
            }
        }
        #endregion
    }
}