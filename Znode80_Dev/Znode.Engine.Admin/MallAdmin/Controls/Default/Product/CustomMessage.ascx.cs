using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Custom Message user control in marketplace.
    /// </summary>
    public partial class Controls_CustomMessage : System.Web.UI.UserControl
    {
        /// <summary>
        /// Sets the Message Key
        /// </summary>
        public string MessageKey
        {
            set
            {
                string key = value;
                string msg = ZNodeCatalogManager.MessageConfig.GetMessage(key, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

                if (msg != null)
                {
                    lblMsg.Text = msg;
                }
            }
        }
    }
}