<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Controls_ProductAddOns" Codebehind="ProductAddOns.ascx.cs" %>
<div id="Add-Ons" align="right">
<asp:Panel ID="pnlAddOns" runat="server" Visible="true">
   <asp:PlaceHolder runat="server" ID="ControlsPlaceHolder"></asp:PlaceHolder> 
</asp:Panel>
</div>

<!--Information : If you change Add-Ons <div> tag ID [EX:<div ID ="Add-Ons">] , then you need to update that
    name in this javascript [File:"thumbnailviewer.js"] on line no :43 and 111. If we did't update 
    enlarge image won't work.-->