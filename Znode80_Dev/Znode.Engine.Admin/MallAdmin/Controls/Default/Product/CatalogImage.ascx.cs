﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Catalog Image user control in marketplace.
    /// </summary>
    public partial class Controls_Default_Product_CatalogImage : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct product;
        private int ProductId = 0;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["zpid"]);
            }
            else if (Request.Params["itemid"] != null)
            {
                this.ProductId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }
            else if (Session["itemid"] != null)
            {
                this.ProductId = int.Parse(Session["itemid"].ToString());
            }
            else
            {
                return;
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            this.Bind();
        }

        /// <summary>
        /// Bind Method
        /// </summary>
        protected void Bind()
        {
            ZNodeImage znodeImage = new ZNodeImage();
            if (this.product != null)
            {
                string element = "<div class=\"CatalogImage_element\">{0}</div>";

                uxPlaceHolder.Controls.Add(ParseControl(string.Format(element, "<img border='0'  EnableViewState='false' ID='CatalogItemImage' alt='" + this.product.ImageAltTag + "' src=\"" + Page.ResolveClientUrl(znodeImage.GetImageHttpPathMedium(this.product.ImageFile)) + "\" />")));

                ZNode.Libraries.DataAccess.Service.ProductImageService service = new ZNode.Libraries.DataAccess.Service.ProductImageService();

                // AlternateImages
                TList<ProductImage> productImages = service.GetByProductIDActiveIndProductImageTypeID(this.product.ProductID, true, 1);

                productImages.Sort("DisplayOrder");

                foreach (ProductImage productImage in productImages)
                {
                    uxPlaceHolder.Controls.Add(ParseControl(string.Format(element, "<img border='0' EnableViewState='false' alt='" + this.GetImageAltTag(productImage.ImageAltTag, this.product.Name) + "' src=\"" + Page.ResolveClientUrl(znodeImage.GetImageHttpPathMedium(productImage.ImageFile)) + "\" />")));
                }
            }
        }

        /// <summary>
        /// Get product image alternative text
        /// </summary>
        /// <param name="altTagText">Alternative Tag Text</param>
        /// <param name="productName"> Represents the product name</param>
        /// <returns>Returns the Alternative Image Tag</returns>
        protected string GetImageAltTag(string altTagText, string productName)
        {
            if (string.IsNullOrEmpty(altTagText))
            {
                return productName;
            }
            else
            {
                return altTagText;
            }
        }
    }
}