using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

/// <summary>
/// Displays the related products
/// </summary>
namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Product Related user control class
    /// </summary>
    public partial class Controls_ShoppingCartProduct_ProductRelated : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProductBase _Product;
        private bool _HasItems = false;
        private bool _ShowDescription = false;
        private bool _ShowName = false;
        private bool _ShowImage = false;
        private bool _IsQuickWatch = false;
        private ZNodeImage znodeImage = new ZNodeImage();
        #endregion

        #region Public Properties
        
        /// <summary>
        /// Gets or sets the catalog item object. The control will bind to the data from this object
        /// </summary>
        public ZNodeProductBase Product
        {
            get
            {
                return this._Product;
            }
            
            set
            {
                this._Product = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether Product Has Items
        /// </summary>
        public bool HasItems
        {
            get
            {
                return this._HasItems;
            }
            
            set
            {
                this._HasItems = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Name
        /// </summary>
        public bool ShowName
        {
            get 
            { 
                return this._ShowName; 
            }

            set 
            { 
                this._ShowName = value; 
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Image
        /// </summary>
        public bool ShowImage
        {
            get 
            { 
                return this._ShowImage; 
            }

            set 
            { 
                this._ShowImage = value; 
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Description
        /// </summary>
        public bool ShowDescription
        {
            get 
            { 
                return this._ShowDescription; 
            }
           
            set 
            { 
                this._ShowDescription = value; 
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether QuickWatch enabled or not
        /// </summary>
        public bool IsQuickwatch
        {
            get
            {
                return this._IsQuickWatch;
            }

            set
            {
                this._IsQuickWatch = value;
            }
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            if (this.Product != null && this.Product.ZNodeCrossSellItemCollection.Count > 0)
            {
                pnlRelated.Visible = true;

                this._HasItems = true;

                DataListRelated.DataSource = this.Product.ZNodeCrossSellItemCollection;
                DataListRelated.DataBind();

                // Disable view state for data list item
                foreach (DataListItem item in DataListRelated.Items)
                {
                    item.EnableViewState = false;
                }
            }
            else
            {
                pnlRelated.Visible = false;
            }
        }

        /// <summary>
        /// Get the thumbnail image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the thumbnail image relative path.</returns>
        public string GetImageHttpPathThumbnail(string imageFileName)
        {
            return this.znodeImage.GetRelativeImageUrl(ZNodeConfigManager.SiteConfig.MaxCatalogItemThumbnailWidth, imageFileName, false);
        }

        #endregion

        #region Helper Functions

        /// <summary>
        /// Get View Product Page Url
        /// </summary>
        /// <param name="link">The value of link</param>
        /// <returns>Returns the Product Page Url</returns>
        protected string GetViewProductPageUrl(string link)
        {
            if (this._IsQuickWatch)
            {
                return "javascript:self.parent.location ='" + ResolveUrl(link) + "';";
            }

            return link;
        }

        /// <summary>
        /// Get Product Url
        /// </summary>
        /// <param name="productId">Product Id value</param>
        /// <returns>Returns the product url</returns>
        protected string GetProductUrl(string productId)
        {
            ZNodeUrl url = new ZNodeUrl();
            string PagePath = "~/product.aspx?zpid=" + productId;
            return PagePath;
        }

        /// <summary>
        /// Returns properties value for PlugIn Control
        /// </summary>
        /// <param name="reviewCollection">Review Collection</param>
        /// <param name="viewProductLink">View Product Link</param>
        /// <returns>Returns the hashtable</returns>
        protected Hashtable GetParameterList(object reviewCollection, object viewProductLink)
        {
            // Create Instance for hashtable
            Hashtable collection = new Hashtable();

            // Add property name and value to it
            collection.Add("ViewProductLink", viewProductLink);
            collection.Add("ReviewCollection", reviewCollection);

            return collection;
        }

        #endregion
    }
}