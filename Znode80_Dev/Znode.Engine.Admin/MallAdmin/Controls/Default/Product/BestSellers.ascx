<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Controls_BestSellers" Codebehind="BestSellers.ascx.cs" %>

 <asp:Panel ID="pnlBestSellerList" runat="server" 
    meta:resourcekey="pnlBestSellerListResource1">
 	 <script language="javascript" type="text/javascript">
 	 	var bestSellers;
 		var prm = Sys.WebForms.PageRequestManager.getInstance();
		prm.add_pageLoaded(PageLoadedEventHandler);

		//
		function PageLoadedEventHandler() {			
			window.addEvents({
				'domready': function() {
					/* thumbnails example , div containers */
					bestSellers = new SlideItMoo({
						overallContainer: 'BestSellers_outer',
						elementScrolled: 'BestSellers_inner',
						thumbsContainer: 'BestSellers_Items',
						itemsVisible: 3,
						elemsSlide: 1,
						duration: 500,
						itemsSelector: '.BestSellerItem',
						showControls: 1
					});
				}
			});
		}
	</script>
    
 	<div class="BestSeller">
		<div id="BestSellers_outer">
			<div id="BestSellers_inner">
				<div id="BestSellers_Items">
					<asp:DataList ID="DataListBestSeller" runat="server"  
                        RepeatDirection="Horizontal" RepeatLayout="Flow" 
                        meta:resourcekey="DataListBestSellerResource1" EnableViewState="false">
						<ItemStyle CssClass="ItemStyle" />               
					    <ItemTemplate>
                            <div class="BestSellerItem">
                                <div class="DetailLink">
                                    <asp:HyperLink ID="hlName" runat="server" CssClass="DetailLink" 
                                        meta:resourcekey="hlNameResource1" 
                                        NavigateUrl='<%# GetViewProductPageUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' 
                                        Text='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>' 
                                        Visible="<%# ShowName %>"></asp:HyperLink>
                                    <asp:Image ID="NewItemImage" runat="server" 
                                        ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.gif") %>' 
                                        meta:resourcekey="NewItemImageResource1" 
                                        Visible='<%# DataBinder.Eval(Container.DataItem, "NewProductInd") %>' />
                                </div>
                                <div class="Image">
                                    <div>
                                        <a ID="A1" runat="server" 
                                            HRef='<%# GetViewProductPageUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'>
                                        <img ID="Img1" runat="server" 
                                            Alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>' border="0" 
                                            Src='<%# GetImageHttpPathThumbnail(DataBinder.Eval(Container.DataItem, "ImageFile").ToString()) %>'></img>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
					</asp:DataList>
				</div>
			</div>
		</div>
	</div>
 </asp:Panel>
