<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Controls_ProductTabs" Codebehind="ProductTabs.ascx.cs" %>

<div id="Tab">
    <ajaxToolKit:TabContainer runat="server" ID="ProductTabs" CssClass="CustomTabStyle"
        ScrollBars="Vertical" ActiveTabIndex="0">
        <ajaxToolKit:TabPanel ID="tabDescription" runat="server">
            <HeaderTemplate>
                <asp:Localize ID="lblHeaderDescription" runat="server" Text="DETAILS"></asp:Localize>
            </HeaderTemplate>
            <ContentTemplate>
                <div class="Features">
                    <asp:Label ID="lblProductDescription" EnableViewState="false" runat="server"></asp:Label></div>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
      
        <ajaxToolKit:TabPanel ID="tabShippingInfo" runat="server">
            <HeaderTemplate>
                <asp:Localize ID="lblHeaderShippingInfo" runat="server" Text="SHIPPING"></asp:Localize>
            </HeaderTemplate>
            <ContentTemplate>
                <div class="ShippingInfo">
                    <asp:Label ID="lblShippingInfo" EnableViewState="false" runat="server"></asp:Label></div>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
    </ajaxToolKit:TabContainer>
</div>
