using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the ProductTabs user control class
    /// </summary>
    public partial class Controls_ProductTabs : System.Web.UI.UserControl
    {
        #region Private Variables
        private int ProductId;
        private Product product = new Product();
        private string Mode = string.Empty;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Bind Method
        /// <summary>
        /// Bind Method
        /// </summary>
        public void Bind()
        {
            lblProductDescription.Text = Server.HtmlDecode(this.product.FeaturesDesc);
            lblShippingInfo.Text = Server.HtmlDecode(this.product.AdditionalInformation);

            ProductTabs.Visible = true;
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["mode"] != null)
            {
                this.Mode = Request.Params["mode"];
            }

            if (Request.Params["zpid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["zpid"]);
            }
            else if (Request.Params["itemid"] != null)
            {
                this.ProductId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }
            else if (Session["itemid"] != null)
            {
                this.ProductId = int.Parse(Session["itemid"].ToString());
            }
            else
            {
                return;
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            ProductAdmin pdtadmin = new ProductAdmin();
            this.product = pdtadmin.GetByProductId(this.ProductId);

            this.Bind();
        }

        #endregion
    }
}