﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Controls_Default_ReviewStateLegend class.
    /// </summary>
    public partial class Admin_Controls_Default_ReviewStateLegend : System.Web.UI.UserControl
    {
        /// <summary>
        /// Gets the review state icon by review state Id. 
        /// </summary>
        /// <param name="reviewStateId">Product review state Id</param>
        /// <returns>Returns the review state icon path</returns> 
        public string GetImageUrl(string reviewStateId)
        {
            string imageSrc = "~/MallAdmin/Themes/Images/223-point_pendingapprove.gif";
            if (reviewStateId != string.Empty)
            {
                int stateId = Convert.ToInt32(reviewStateId);

                if (stateId == Convert.ToInt32(ZNodeProductReviewState.Approved))
                {
                    imageSrc = "~/MallAdmin/Themes/Images/222-point_approve.gif";
                }

                if (stateId == Convert.ToInt32(ZNodeProductReviewState.Declined))
                {
                    imageSrc = "~/MallAdmin/Themes/Images/221-point_decline.gif";
                }

                if (stateId == Convert.ToInt32(ZNodeProductReviewState.ToEdit))
                {
                    imageSrc = "~/MallAdmin/Themes/Images/220-point_toedit.gif";
                }
            }

            return imageSrc;
        }
    }
}