﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Category Auto Complete user control in marketplace.
    /// </summary>
    public partial class Admin_Controls_Default_CategoryAutoComplete : System.Web.UI.UserControl
    {
        #region Properties

        private int dropdownItemCount = int.Parse(ConfigurationManager.AppSettings["DropdownMaxItemCount"]);

        /// <summary>
        /// Gets or sets the Width
        /// </summary>
        public string Width
        {
            get { return txtCategory.Width.ToString(); }
            set { txtCategory.Width = new Unit(value); }
        }

        /// <summary>
        /// Gets or sets the Value
        /// </summary>
        public string Value
        {
            get
            {
                if (ddlCategory.Visible)
                {
                    return ddlCategory.SelectedValue;
                }
                else
                {
                    bool assigned = false;

                    // empty, if hidden value is 0 or -1
                    if (string.Equals(hdnCategoryId.Value, "0") && string.Equals(hdnCategoryId.Value, "-1"))
                    {
                        hdnCategoryId.Value = string.Empty;
                    }

                    // check the name is in value;
                    if (!string.IsNullOrEmpty(txtCategory.Text.Trim()) && string.IsNullOrEmpty(hdnCategoryId.Value))
                    {
                        foreach (DataRow drow in this.CategoryListTable.Select(string.Concat("Name='", txtCategory.Text.Trim(), "'")))
                        {
                            hdnCategoryId.Value = drow["CategoryId"].ToString();
                            assigned = true;
                            break;
                        }
                    }
                    else if (!string.IsNullOrEmpty(txtCategory.Text.Trim()) && !string.IsNullOrEmpty(hdnCategoryId.Value))
                    {
                        foreach (DataRow drow in this.CategoryListTable.Select(string.Concat("Name='", txtCategory.Text.Trim().Replace("'", "''"), "' AND CategoryID=", hdnCategoryId.Value)))
                        {
                            hdnCategoryId.Value = drow["CategoryId"].ToString();
                            assigned = true;
                            break;
                        }
                    }

                    if (string.IsNullOrEmpty(txtCategory.Text.Trim()))
                    {
                        hdnCategoryId.Value = "0";
                        assigned = true;
                    }

                    // if it is not valid, assign to -1 to select nil record.
                    if (!assigned)
                    {
                        hdnCategoryId.Value = this.ForceAutoCompleteTextBox ? "0" : "-1";
                    }
                }

                return hdnCategoryId.Value;
            }

            set
            {
                if (ddlCategory.Visible)
                {
                    ListItem item = ddlCategory.Items.FindByValue(value);
                    if (item != null)
                    {
                        ddlCategory.SelectedValue = value;
                    }
                    else
                    {
                        ddlCategory.SelectedIndex = 0;
                    }
                }
                else 
                { 
                    hdnCategoryId.Value = value; 
                }
            }
        }

        /// <summary>
        /// Gets or sets the Znodeproduct Text
        /// </summary>
        public string Text
        {
            get { return txtCategory.Text; }
            set { txtCategory.Text = value; }
        }

        /// <summary>
        /// Gets the ClientId
        /// </summary>
        public string ClientId
        {
            get
            {
                return hdnCategoryId.ClientID;
            }
        }

        /// <summary>
        /// Gets the txtClientId
        /// </summary>
        public string TxtClientId
        {
            get
            {
                if (ddlCategory.Visible)
                {
                    return ddlCategory.ClientID;
                }

                return txtCategory.ClientID;
            }
        }

        /// <summary>
        /// Sets the Context Key
        /// </summary>
        public string ContextKey
        {
            set
            {
                AutoCompleteExtender2.ContextKey = value;
                hdnCatalogID.Value = value;
                System.Web.HttpContext.Current.Session["CategoryList"] = null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to field is required or not.
        /// </summary>
        public bool IsRequired
        {
            get
            {
                return RequiredFieldValidator9.Visible;
            }

            set
            {
                if (ddlCategory.Visible)
                {
                    RequiredFieldValidator9.ControlToValidate = "ddlCategory";
                }

                RequiredFieldValidator9.Visible = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Force Auto Complete text box.
        /// </summary>
        public bool ForceAutoCompleteTextBox
        {
            get { return bool.Parse(ForceAutoComplete.Value); }
            set { ForceAutoComplete.Value = value.ToString(); }
        }

        /// <summary>
        /// Gets the category list table
        /// </summary>
        public DataTable CategoryListTable
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["CategoryList"] == null)
                {
                    ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new ZNode.Libraries.DataAccess.Custom.CategoryHelper();
                    System.Data.DataSet ds = categoryHelper.GetNavigationItems(ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID, ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.LocaleID);

                    for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                    {
                        ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                    }

                    System.Web.HttpContext.Current.Session["CategoryList"] = ds.Tables[0];
                    return ds.Tables[0];
                }
                else
                {
                    return (DataTable)System.Web.HttpContext.Current.Session["CategoryList"] as DataTable;
                }
            }
        }
        #endregion

        #region Page Events
        /// <summary>
        /// Page init event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                System.Web.HttpContext.Current.Session["CategoryList"] = null;
            }

            ddlCategory.Visible = false;

            if (!this.ForceAutoCompleteTextBox && this.CategoryListTable.Rows.Count < this.dropdownItemCount && !this.Page.IsPostBack)
            {
                ddlCategory.Visible = true;
                txtCategory.Visible = false;
                AutoCompleteExtender2.Enabled = false;
                ddlCategory.DataSource = this.CategoryListTable;
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "CategoryID";
                ddlCategory.DataBind();

                if (!this.IsRequired)
                {
                    ListItem allItem = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), string.Empty);
                    ddlCategory.Items.Insert(0, allItem);
                }
            }
        }
        #endregion
    }
}