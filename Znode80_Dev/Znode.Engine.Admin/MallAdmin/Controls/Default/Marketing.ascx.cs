﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Marketing user control in marketplace.
    /// </summary>
    public partial class Admin_Controls_Default_Marketing : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private int itemId = 0;
        private string managePageLink = "~/MallAdmin/Secure/product/EditMarketing.aspx?itemid=";
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Protected Methods and Events
        /// <summary> 
        /// Redirect to manage page for the edit button event.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditMarketing_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.managePageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        protected void BtnNotify_Click(object sender, EventArgs e)
        {
            this.SendMail();
        }

        #endregion

        #region page load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId (Product Id) from QueryString        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.itemId = 0;
            }

            if (!this.IsPostBack)
            {
                // Hide the 'Notify Marketing for Review' from vendor.
                UserStoreAccess uss = new UserStoreAccess();
				btnNotify.Visible = uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer ? true : false;

                this.Bind();
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind the data
        /// </summary>
        private void Bind()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin prodAdmin = new ProductAdmin();
            Product product = prodAdmin.GetByProductId(this.itemId);

            chkIsNewItem.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.NewProductInd));

            // seo 
            lblSEODescription.Text = product.SEODescription;
            lblSEOKeywords.Text = product.SEOKeywords;
            lblSEOTitle.Text = product.SEOTitle;
            lblSEOURL.Text = product.SEOURL;
        }

        /// <summary>
        /// Validate for Null Values and return a Boolean Value
        /// </summary>
        /// <param name="fieldvalue">Value of the field as object</param>
        /// <returns>Retuns the field to display or not</returns>
        private bool DisplayVisible(object fieldvalue)
        {
            if (fieldvalue == DBNull.Value)
            {
                return false;
            }
            else
            {
                if (Convert.ToInt32(fieldvalue) == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Send the notification details to Marketing team about the product SEO details.
        /// </summary>
        private void SendMail()
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = this.GetGlobalResourceObject("ZnodeAdminResource", "TextSubject").ToString();

            string body = string.Empty;
            string toAddress = ConfigurationManager.AppSettings["SEONotificationMail"];
            string templateFile = Path.Combine(HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath), "MarketingReviewEmailTemplate.htm");

            // Check whether the template file exist or not.
            if (!File.Exists(templateFile))
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSEOMail").ToString();
                return;
            }

            using (StreamReader rw = new StreamReader(templateFile))
            {
                body = rw.ReadToEnd();
            }
            Product product = this.GetProduct();

            Regex name = new Regex("#Name#", RegexOptions.IgnoreCase);
            body = name.Replace(body, product.Name);

            Regex title = new Regex("#Title#", RegexOptions.IgnoreCase);
            body = title.Replace(body, product.SEOTitle == null ? string.Empty : product.SEOTitle);

            Regex keyword = new Regex("#Keyword#", RegexOptions.IgnoreCase);
            body = keyword.Replace(body, product.SEOKeywords == null ? string.Empty : product.SEOKeywords);

            Regex description = new Regex("#Description#", RegexOptions.IgnoreCase);
            body = description.Replace(body, product.SEODescription == null ? string.Empty : product.SEODescription);

            Regex url = new Regex("#Url#", RegexOptions.IgnoreCase);
            body = url.Replace(body, product.SEOURL == null ? string.Empty : product.SEOURL);

            Regex productUrl = new Regex("#ProductUrl#", RegexOptions.IgnoreCase);
            body = productUrl.Replace(body, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath + "/Admin/Secure/product/view.aspx?itemid=" + product.ProductID.ToString() + "&mode=marketing");

            Regex rx4 = new Regex("#LOGO#", RegexOptions.IgnoreCase);
            body = rx4.Replace(body, ZNodeConfigManager.SiteConfig.StoreName);

            Regex rx5 = new Regex("#CUSTOMERSERVICEPHONE#", RegexOptions.IgnoreCase);
            body = rx5.Replace(body, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

            Regex rx6 = new Regex("#CUSTOMERSERVICEMAIL#", RegexOptions.IgnoreCase);
            body = rx6.Replace(body, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

            try
            {
                ZNodeEmail.SendEmail(toAddress, senderEmail, String.Empty, subject, body, true);
            }
            catch
            {
                string msg = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSEOEmailAddress").ToString(), toAddress);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(msg);
                lblError.Text = msg;
            }
        }

        /// <summary>
        /// Get the Product details
        /// </summary>
        /// <returns>Returns the product details</returns>
        private Product GetProduct()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            Product product = ProdAdmin.GetByProductId(this.itemId);
            return product;
        }
        #endregion
    }
}