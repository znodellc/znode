﻿using System;
using System.Configuration;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Manufacturer Auto Complete user control in marketplace.
    /// </summary>
    public partial class Admin_Controls_ManufacturerAutoComplete : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private int dropdownItemCount = int.Parse(ConfigurationManager.AppSettings["DropdownMaxItemCount"]);
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the width
        /// </summary>
        public string Width
        {
            get
            {
                if (ddlManufacturer.Visible)
                {
                    return ddlManufacturer.Width.ToString();
                }

                return txtManufacturer.Width.ToString();
            }

            set
            {
                if (ddlManufacturer.Visible)
                {
                    ddlManufacturer.Width = new Unit(value);
                }
                else
                { 
                    txtManufacturer.Width = new Unit(value); 
                }
            }
        }

        /// <summary>
        /// Gets or sets the value
        /// </summary>
        public string Value
        {
            get
            {
                if (ddlManufacturer.Visible)
                {
                    return ddlManufacturer.SelectedValue;
                }

                return hdnManufacturerId.Value;
            }

            set
            {
                if (ddlManufacturer.Visible)
                {
                    ListItem item = ddlManufacturer.Items.FindByValue(value);
                    if (item != null)
                    {
                        ddlManufacturer.SelectedValue = value;
                    }
                    else
                    {
                        ddlManufacturer.SelectedIndex = 0;
                    }
                }
                else 
                { 
                    hdnManufacturerId.Value = value; 
                }
            }
        }

        /// <summary>
        /// Sets the context key
        /// </summary>
        public string ContextKey
        {
            set
            {
                AutoCompleteExtender2.ContextKey = value;
                hdnPortalId.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the text
        /// </summary>
        public string Text
        {
            get { return txtManufacturer.Text; }
            set { txtManufacturer.Text = value; }
        }

        /// <summary>
        /// Gets the Manufacturer list table
        /// </summary>
        private TList<Manufacturer> ManufacturerListTable
        {
            get
            {
                ManufacturerService manufacturerService = new ManufacturerService();
                ManufacturerQuery query = new ManufacturerQuery();
                query.Append(ManufacturerColumn.ActiveInd, "TRUE");

                if (hdnPortalId.Value != "0")
                {
                    query.Append(ManufacturerColumn.PortalID, hdnPortalId.Value);
                }

                TList<Manufacturer> manufacturer = manufacturerService.Find(query.GetParameters());

                return manufacturer;
            }
        }
        #endregion

        #region Page load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.ManufacturerListTable.Count < this.dropdownItemCount && !this.Page.IsPostBack)
            {
                ddlManufacturer.Visible = true;
                txtManufacturer.Visible = false;
                ddlManufacturer.DataSource = this.ManufacturerListTable;
                ddlManufacturer.DataTextField = "Name";
                ddlManufacturer.DataValueField = "ManufacturerID";
                ddlManufacturer.DataBind();

                ListItem allItem = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), string.Empty);
                ddlManufacturer.Items.Insert(0, allItem);

                ddlManufacturer.SelectedIndex = 0;
            }
        }
        #endregion
    }
}