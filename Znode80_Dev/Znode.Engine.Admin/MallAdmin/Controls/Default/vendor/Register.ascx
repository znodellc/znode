<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Controls_Default_Vendor_Register" CodeBehind="Register.ascx.cs" %>
<%@ Register Src="~/MallAdmin/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>

<asp:Panel runat="server" ID="pnlCreateAccount">
    <div class="LoginPage">
        <div>
            <h1>
                <asp:Localize ID="TitleRequestVendor" Text='<%$ Resources:ZnodeAdminResource, TitleRequestVendor%>' runat="server"></asp:Localize></h1>
            <p>
                <asp:Localize runat="server" ID="TextOnlineMall" Text='<%$ Resources:ZnodeAdminResource, TextOnlineMall%>'></asp:Localize>
            </p>
        </div>
        <div class="FormView">
            <div>
                <ZNode:spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:spacer>
            </div>
            <div>
                <div align="left" colspan="2" class="Error">
                    <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                </div>
            </div>
            <div class="h1Style">
                <asp:Localize runat="server" ID="SubTitleLogin" Text='<%$ Resources:ZnodeAdminResource, SubTitleLogin%>'></asp:Localize>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleVendor" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendorAccount%>'></asp:Localize><span class="Asterix">*</span><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextVendorAccount" Text='<%$ Resources:ZnodeAdminResource, HintTextVendorAccount%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="FranchiseNumber" runat="server" CssClass="TextField"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="FranchiseNumberRequired" runat="server" ControlToValidate="FranchiseNumber"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredVendorAccount%>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredVendorAccount%>'
                        ValidationGroup="uxCreateUserWizard" CssClass="Error" Display="Dynamic">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleUsername" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleUsername%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="UserName" runat="server" autocomplete="off" CssClass="TextField"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredLoginUserName%>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredLoginUserName%>' ValidationGroup="uxCreateUserWizard"
                        CssClass="Error" Display="Dynamic">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnPassword" Text='<%$ Resources:ZnodeAdminResource, ColumnPassword%>'></asp:Localize><span class="Asterix">*</span><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextPassword" Text='<%$ Resources:ZnodeAdminResource, HintTextPassword%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Password" runat="server" autocomplete="off" TextMode="Password" CssClass="TextField"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPassword%>'
                        ToolTip='<%$ Resources:ZnodeAdminResource, RequiredPassword%>' ControlToValidate="Password" ValidationGroup="uxCreateUserWizard"
                        CssClass="Error" Display="Dynamic">
                      
                    </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ValidationGroup="uxCreateUserWizard" ID="PasswordRegularExpression"
                        ControlToValidate="Password" runat="server" ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularValidExpression%>'
                        Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularPasswordExpression%>'
                        ToolTip='<%$ Resources:ZnodeAdminResource, RegularPasswordExpression%>'
                        CssClass="Error"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleConfirm" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleConfirmPassword%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="ConfirmPassword" runat="server" autocomplete="off" TextMode="Password" CssClass="TextField"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredConfirmPassword%>'
                        ToolTip='<%$ Resources:ZnodeAdminResource, RequiredConfirmPassword%>' runat="server" ControlToValidate="ConfirmPassword"
                        ValidationGroup="uxCreateUserWizard" CssClass="Error" Display="Dynamic">
                    </asp:RequiredFieldValidator>
                </div>
                <div>
                    <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                        ControlToValidate="ConfirmPassword" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareConfirmationPassword%>'
                        ToolTip='<%$ Resources:ZnodeAdminResource, CompareConfirmationPassword%>' ValidationGroup="uxCreateUserWizard"
                        CssClass="Error" Display="Dynamic"></asp:CompareValidator>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSecret" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSecretQuestion%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlSecretQuestions" runat="server">
                    <asp:ListItem Enabled="true" Selected="True" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFavoritePet %>'></asp:ListItem>
                    <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextCityBorn %>'></asp:ListItem>
                    <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextHighSchool %>'></asp:ListItem>
                    <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFavoriteMovie %>'></asp:ListItem>
                    <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextMotherMaiden %>'></asp:ListItem>
                    <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFirstCar %>'></asp:ListItem>
                    <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFavoriteColor %>'></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSecretAnswer" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSecretAnswer%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Answer" runat="server" CssClass="TextField"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="SecurityAnswerRequired" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSecurityAnswer %>'
                        ToolTip='<%$ Resources:ZnodeAdminResource, RequiredSecurityAnswer %>' ControlToValidate="Answer" ValidationGroup="uxCreateUserWizard"
                        CssClass="Error" Display="Dynamic">                      
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="h1Style">
                <asp:Localize runat="server" ID="SubTitleContact" Text='<%$ Resources:ZnodeAdminResource, SubTitleContactInformation%>'></asp:Localize>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleFirstName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFirstName%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingFirstName" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredFirstName%>'
                        ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingFirstName" runat="server"
                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleLastName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLastName%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingLastName" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredLastName%>'
                        ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingLastName" runat="server"
                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleCompany" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCompanyName%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingCompanyName" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnEmailAddress" Text='<%$ Resources:ZnodeAdminResource, ColumnEmailAddress%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingEmail" runat="server" CssClass="TextField"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEmail%>'
                        ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingEmail" runat="server"
                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regemailID" runat="server" ControlToValidate="txtBillingEmail"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularEmailAddress%>' Display="Dynamic" ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularValidEmailExpression%>'
                        CssClass="Error" ValidationGroup="uxCreateUserWizard"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitlePhoneNumber" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePhoneNumber%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingPhoneNumber" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox><div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPhoneNumber%>'
                            ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingPhoneNumber"
                            runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnStreet1" Text='<%$ Resources:ZnodeAdminResource, ColumnStreet1%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingStreet1" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox><div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredStreet1%>'
                            ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingStreet1" runat="server"
                            Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnStreet2" Text='<%$ Resources:ZnodeAdminResource, ColumnStreet2%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingStreet2" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleCity" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCity%>' />
                <span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingCity" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox><div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredCity%>'
                            ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingCity" runat="server"
                            Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleState" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleState%>'></asp:Localize>
                <span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingState" runat="server" Width="40" Columns="10"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredState%>'
                        ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingState" runat="server"
                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnPostalCode" Text='<%$ Resources:ZnodeAdminResource, ColumnPostalCode%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingPinCode" runat="server" Width="40" Columns="10"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPostalCode%>'
                        ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingPinCode" runat="server"
                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
                <ZNode:spacer ID="Spacer1" SpacerHeight="30" SpacerWidth="10" runat="server"></ZNode:spacer>
                <div class="ValueStyle">
                    <zn:Button runat="server" ID="ibRegister" ButtonType="SubmitButton" OnClick="LbRegister_Click" ValidationGroup="uxCreateUserWizard" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="True" />
                    <zn:Button ID="btnClear" runat="server" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlConfirm" runat="server" Visible="False" CssClass="SuccessMsg">
    <br />
    <br />
    <p>
        <asp:Localize runat="server" ID="TextActivateRequest" Text='<%$ Resources:ZnodeAdminResource, TextActivateRequest%>'></asp:Localize>
    </p>
    <div class="Clear">
        <ZNode:spacer ID="Spacer9" EnableViewState="false" SpacerHeight="20" SpacerWidth="10"
            runat="server"></ZNode:spacer>
    </div>
</asp:Panel>
