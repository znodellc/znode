using System;
using System.Web.Security;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Register user control in marketplace.
    /// </summary>
    public partial class Controls_Default_Vendor_Register : System.Web.UI.UserControl
    {
        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Check if SSL is required
            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }

            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtAccountTitle.Text");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            // ibRegister.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/Register.gif";        
        }
        #endregion

        #region Events
        /// <summary>
        /// Register Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LbRegister_Click(object sender, EventArgs e)
        {
            this.RegisterUser();
        }
        
        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            string link = Request.Url.GetLeftPart(UriPartial.Authority) + Response.ApplyAppPathModifier("Default.aspx");
            Response.Redirect(link);
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// This function registers a new user 
        /// </summary>
        private void RegisterUser()
        {
            TransactionManager transaction = ConnectionScope.CreateTransaction();
            try
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();
                log.LogActivityTimerStart();

                int profileId = ZNodeProfile.DefaultRegisteredProfileId;

                if (profileId == 0)
                {
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountNotCreated").ToString(); 
                    return;
                }

                AccountService accountService = new AccountService();

                TList<Account> account = accountService.Find(string.Format("ExternalAccountNo='{0}'", FranchiseNumber.Text.Trim()));
                MembershipUser user;
               
                if (account.Count > 0)
                {
                    if (account[0].UserID != null)
                    {
                        user = Membership.GetUser(account[0].UserID);

                        ErrorMessage.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFranchiseNum").ToString(), FranchiseNumber.Text, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);
                        return;
                    }
                }
                
                // Check if loginName already exists in DB
                ZNodeUserAccount userAccount = new ZNodeUserAccount();
                if (!userAccount.IsLoginNameAvailable(ZNodeConfigManager.SiteConfig.PortalID, UserName.Text.Trim()))
                {
                    log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text, null, null, "User name already exists", null);
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUserNameExist").ToString(); 
                    return;
                }
                
                MembershipCreateStatus status = MembershipCreateStatus.ProviderError;
                user = Membership.CreateUser(UserName.Text.Trim(), Password.Text.Trim(), txtBillingEmail.Text.Trim(), ddlSecretQuestions.SelectedItem.Text, Answer.Text.Trim(), false, out status);

                if (user == null)
                {
                    log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text);
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCreateFailed").ToString(); 

                    return;
                }

                log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, UserName.Text);

                // create the user account
                Account newUserAccount = new Account();
                newUserAccount.UserID = (Guid)user.ProviderUserKey;
                newUserAccount.CompanyName = txtBillingCompanyName.Text;
                newUserAccount.ExternalAccountNo = FranchiseNumber.Text.Trim();
                newUserAccount.Email = txtBillingEmail.Text.Trim();

                newUserAccount.CreateDte = DateTime.Now;
                newUserAccount.UpdateDte = DateTime.Now;

                // Register account
                if (newUserAccount.AccountID > 0)
                {
                    accountService.Update(newUserAccount);
                }
                else
                {
                    accountService.Insert(newUserAccount);
                }

                AccountAdmin accountAdmin = new AccountAdmin();
                AddressService addressService = new AddressService();
                Address address = accountAdmin.GetDefaultBillingAddress(newUserAccount.AccountID);
                if (address == null)
                {
                    address = new Address();
                    address.IsDefaultBilling = true;
                    address.IsDefaultShipping = true;
                    address.AccountID = newUserAccount.AccountID;
                }
                
                // Copy info to billing                
                address.FirstName = txtBillingFirstName.Text;
                address.LastName = txtBillingLastName.Text;
                address.PhoneNumber = txtBillingPhoneNumber.Text.Trim();
                address.Street = txtBillingStreet1.Text.Trim();
                address.Street1 = txtBillingStreet2.Text.Trim();
                address.City = txtBillingCity.Text.Trim();
                address.CompanyName = txtBillingCompanyName.Text.Trim();
                address.StateCode = txtBillingState.Text.Trim();
                address.PostalCode = txtBillingPinCode.Text;
                
                // Default country code for United States
                address.CountryCode = this.GetGlobalResourceObject("ZnodeAdminResource", "TextUS").ToString();
                address.Name = this.GetGlobalResourceObject("ZnodeAdminResource", "TextDefaultAddress").ToString(); 
                
                // Insert shipping address as default billing address.
                if (address.AddressID > 0)
                {
                    addressService.Update(address);
                }
                else
                {
                    addressService.Insert(address);
                }                
              
                // Add Account Profile
                AccountProfile accountProfile = new AccountProfile();
                accountProfile.AccountID = newUserAccount.AccountID;
                accountProfile.ProfileID = profileId;

                AccountProfileService accountProfileServ = new AccountProfileService();
                accountProfileServ.Insert(accountProfile);

                Roles.AddUserToRole(UserName.Text.Trim(), "VENDOR");

                // Log password
                ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, Password.Text.Trim());

                // Create an Profile for the selected user
                ProfileCommon newProfile = (ProfileCommon)ProfileCommon.Create(UserName.Text, true);
                
                // Properties Value
                newProfile.StoreAccess = ZNodeConfigManager.SiteConfig.PortalID.ToString();
                
                // Save profile - must be done since we explicitly created it 
                newProfile.Save();

                // Create a new record in the supplier table
                AddSupplier();

                pnlCreateAccount.Visible = false;
                pnlConfirm.Visible = true;

                transaction.Commit();
            }
            catch (Exception ex)
            {
                if (transaction.IsOpen)
                {
                    transaction.Rollback();
                }

                ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCreateFailed").ToString();
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message.ToString());
            }
        }

        private bool AddSupplier()
        {
            SupplierAdmin supplierAdmin = new SupplierAdmin();
            Supplier supplier = new Supplier();

            supplier.Name = txtBillingCompanyName.Text.Trim();
            supplier.Description = string.Empty;
            supplier.ContactFirstName = txtBillingFirstName.Text.Trim();
            supplier.ContactLastName = txtBillingLastName.Text.Trim();
            supplier.ContactPhone = txtBillingPhoneNumber.Text;
            supplier.ContactEmail = txtBillingEmail.Text;
            supplier.NotificationEmailID = txtBillingEmail.Text;
            supplier.EmailNotificationTemplate = string.Empty;
            supplier.EnableEmailNotification = true;
            supplier.DisplayOrder = 500;
            supplier.ActiveInd = true;
            supplier.Custom1 = string.Empty;
            supplier.Custom2 = string.Empty;
            supplier.Custom3 = string.Empty;
            supplier.Custom4 = string.Empty;
            supplier.Custom5 = string.Empty;
            supplier.ExternalSupplierNo = FranchiseNumber.Text.Trim();
            // Assign Default Supplier Type Id
            supplier.SupplierTypeID = 1; 

            bool check = false;

            check = supplierAdmin.Insert(supplier);
                
            // Log Activity
            string AssociateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCreateSupplier").ToString() + txtBillingCompanyName.Text;
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, txtBillingCompanyName.Text);

            return check;
        }
        #endregion
    }
}