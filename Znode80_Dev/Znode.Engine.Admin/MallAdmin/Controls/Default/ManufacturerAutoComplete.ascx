﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Znode.Engine.MallAdmin.Admin_Controls_ManufacturerAutoComplete" Codebehind="ManufacturerAutoComplete.ascx.cs" %>

<script type="text/javascript">
    var hiddenTextValue; //alias to the hidden field: hideValue

    function AutoComplete_BrandSelected(source, eventArgs) {
        hiddenTextValue = $get("<%=hdnManufacturerId.ClientID%>");
        hiddenTextValue.value = eventArgs.get_value();
    }

    function AutoComplete_BrandShowing(source, eventArgs) {
        hiddenTextValue = $get("<%=hdnManufacturerId.ClientID%>");
        hiddenTextValue.value = "";
    }

    function Manufacturer_OnBlur(obj) {
        hiddenTextValue = $get("<%=hdnManufacturerId.ClientID %>");
        if (obj.value == "") {
            hiddenTextValue.value = "";
        }
        if (hiddenTextValue.value == "") {
            obj.value = "";
        }
    }
</script>

<asp:HiddenField ID="hdnManufacturerId" runat="server"></asp:HiddenField>
<asp:TextBox ID="txtManufacturer" runat="server" onblur="Manufacturer_OnBlur(this)"></asp:TextBox>
<ajaxToolKit:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtManufacturer"
    ServicePath="ZNodeMultifrontService.asmx" ServiceMethod="GetManufacturer" UseContextKey="true"
    MinimumPrefixLength="1" EnableCaching="false" CompletionSetCount="100" CompletionInterval="1"
    DelimiterCharacters=";, :" OnClientShowing="AutoComplete_BrandShowing"
    OnClientItemSelected="AutoComplete_BrandSelected" />    
<asp:DropDownList ID="ddlManufacturer" runat="server" Visible="false" Width="160px"></asp:DropDownList>
<asp:HiddenField runat="server" ID="hdnPortalId" Value="0" />