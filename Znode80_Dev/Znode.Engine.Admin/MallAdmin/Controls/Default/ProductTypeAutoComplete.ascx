﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Znode.Engine.MallAdmin.Admin_Controls_Default_ProductTypeAutoComplete" Codebehind="ProductTypeAutoComplete.ascx.cs" %>

<script type="text/javascript">
    
    function AutoComplete_ProductTypeSelected(source, eventArgs) {
        var hiddenTextValue = $get("<%= ProductTypeId.ClientID %>");
        hiddenTextValue.value = eventArgs.get_value();


        var hAutoPostBack = $get("<%= hdnAutoPostBack.ClientID %>");
        if (hAutoPostBack.value == "True") {
            __doPostBack('AutoComplete_OnSelectedIndexChanged', hiddenTextValue, eventArgs);
        }
    }

    function AutoComplete_ProductTypeShowing(source, eventArgs) {
        var hiddenTextValue = $get("<%=ProductTypeId.ClientID %>");
        hiddenTextValue.value = "";
    }

    function ProductType_OnBlur(obj) {
        var hiddenTextValue = $get("<%=ProductTypeId.ClientID %>");        
        if (obj.value == "") {            
            hiddenTextValue.value = "";
        }
        if (hiddenTextValue.value == "") {
            obj.value = "";
        }        
    }
</script>

<asp:TextBox ID="txtProductType" runat="server" onblur="ProductType_OnBlur(this)"></asp:TextBox>
<asp:HiddenField runat="server" ID="ProductTypeId" />
<asp:HiddenField runat="server" ID="hdnAutoPostBack" Value="false" />
<ajaxToolKit:AutoCompleteExtender ID="autoCompleteExtender3" runat="server" TargetControlID="txtProductType"
    ServicePath="ZNodeMultifrontService.asmx" ServiceMethod="GetProductTypes" UseContextKey="true"
    MinimumPrefixLength="1" EnableCaching="false" CompletionSetCount="10" CompletionInterval="1"
    FirstRowSelected="false" OnClientItemSelected="AutoComplete_ProductTypeSelected"
    OnClientShowing="AutoComplete_ProductTypeShowing"  />
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtProductType"
    ErrorMessage='<%$ Resources:ZnodeAdminResource, SelectProductType %>' CssClass="Error" Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
<asp:DropDownList runat="server" ID="ddlProductType" Visible="false" Width="160px"></asp:DropDownList>
<asp:HiddenField runat="server" ID="hdnPortalId" Value="0" />
