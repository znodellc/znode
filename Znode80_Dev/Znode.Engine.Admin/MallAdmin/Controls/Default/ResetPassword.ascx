<%@ Control Language="C#" AutoEventWireup="true"
	Inherits="Znode.Engine.MallAdmin.Themes_Default_Account_ResetPassword" CodeBehind="ResetPassword.ascx.cs" %>
<%@ Register Src="~/MallAdmin/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
<%@ Register TagPrefix="znode" Namespace="Znode.Engine.Common.CustomClasses.WebControls" Assembly="Znode.Engine.Common" %>
<div class="Form ResetPwd" id="MallAdminResetPwd">
	<h1><asp:Label ID="Localize8" Text="Reset Password" runat="server" /></h1>
	<!-- Reset password Panel -->
	<asp:Panel ID="pnlResetpassword" runat="server">
		<div id="PasswordExpired">
			<asp:Label ID="lblIntroMessage" runat="server"></asp:Label>
		</div>
		<div class="Error">
			<asp:Literal ID="PasswordFailureText" runat="server" EnableViewState="false"></asp:Literal>
		</div>
		<div class="inputs">
			<div class="inputsRow" runat="server" id="tblRowUserId">
				<div class="FieldStyle LeftContent">
					<asp:Label ID="UserID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNewUserName %>'></asp:Label>
				</div>
				<div class="ValueStyle">
					<div>
						<asp:TextBox ID="UserName" autocomplete="off" runat="server" Width="155px"></asp:TextBox>
						<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserName"
							ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredUsername %>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredUsername %>' 
                            ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic" />
						<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UserName"
							CssClass="Error" ToolTip="User Name is required." ValidationExpression="[a-zA-Z0-9_-]{4,}"
							ValidationGroup="ChangePassword1" />
					</div>
					<div class="HintText">
						<asp:Label ID="Localize4" Text='<%$ Resources:ZnodeAdminResource, HintTextNewUserName %>'  runat="server" />
					</div>
				</div>
			</div>
			<div class="inputsRow" runat="server" id="pnlCurrentpassword">
				<div class="FieldStyle LeftContent">
					<asp:Label ID="CurrentPasswordLabel" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCurrentPassword %>'></asp:Label>
				</div>
				<div class="ValueStyle">
					<asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" Width="155px"></asp:TextBox>
					<asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
						ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPassword %>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredPassword %>' ValidationGroup="ChangePassword1"
						CssClass="Error" Display="Dynamic" />
				</div>
			</div>
			<div class="inputsRow">
				<div class="FieldStyle LeftContent">
					<asp:Label ID="NewPasswordLabel" runat="server" Text="New Password"></asp:Label>
				</div>
				<div class="ValueStyle">
					<div>
						<asp:TextBox ID="NewPassword" runat="server" TextMode="Password" Width="155px"></asp:TextBox>
						<asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
							ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredNewPassword %>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredNewPassword %>'
							ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic" />
						<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="NewPassword"
							CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularNewPassword %>'
							SetFocusOnError="True" ToolTip='<%$ Resources:ZnodeAdminResource, RegularNewPassword %>'
							ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,})" ValidationGroup="ChangePassword1" />
					</div>
					<div class="HintText">
						<asp:Label ID="Localize11" Text='<%$ Resources:ZnodeAdminResource, HintTextNewPassword %>'
							runat="server" />
					</div>
				</div>
			</div>
			<div class="inputsRow">
				<div class="FieldStyle LeftContent">
					<asp:Label ID="ConfirmNewPasswordLabel" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleConfirmNewPassword %>'></asp:Label>
				</div>
				<div class="ValueStyle">
					<asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" Width="155px"></asp:TextBox>
					<asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
						ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredConfirmPassword %>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredConfirmPassword %>'
						ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic" />
					<asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
						ControlToValidate="ConfirmNewPassword" CssClass="Error" Display="Dynamic" Text='<%$ Resources:ZnodeAdminResource, ComparePassword %>'
						ValidationGroup="ChangePassword1"></asp:CompareValidator>
				</div>
			</div>
			<div class="inputsRow" runat="server" id="tblRowEmail" visible="false">
				<div class="FieldStyle LeftContent">
					<asp:Label ID="EmailLabel" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEmail %>'></asp:Label>
				</div>
				<div class="ValueStyle">
					<asp:TextBox ID="Email" runat="server"></asp:TextBox>
					<asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
						ValidationGroup="ChangePassword1" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEmail %>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredEmail %>'
						CssClass="Error" Display="Dynamic" />
					<asp:RegularExpressionValidator ID="ValidateEmail" runat="server" ControlToValidate="Email"
						ValidationGroup="ChangePassword1" CssClass="Error" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidEmail %>'
						ToolTip='<%$ Resources:ZnodeAdminResource, ValidEmail %>' ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"
						Display="Dynamic"></asp:RegularExpressionValidator>
				</div>
			</div>
			<div class="inputsRow" id="tblRowSecurityQuestion" runat="server">
				<div class="FieldStyle LeftContent">
					<asp:Label ID="lblSecretQuestion" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSecretQuestion %>'></asp:Label>
				</div>
				<div class="ValueStyle">
					<asp:DropDownList ID="ddlSecretQuestions" runat="server">
						<asp:ListItem Enabled="true" Selected="True" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFavoritePet %>' />
						<asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextCityBorn %>' />
						<asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextHighSchool %>' />
						<asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFavoriteMovie %>' />
						<asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextMotherMaiden %>' />
						<asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFirstCar %>'  />
						<asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFavoriteColor %>' />
					</asp:DropDownList>
				</div>
			</div>
			<div class="inputsRow" id="tblRowPasswordAnswer" runat="server">
				<div class="FieldStyle LeftContent">
					<asp:Label ID="AnswerLabel" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSecretAnswer %>'></asp:Label>
				</div>
				<div class="ValueStyle">
					<asp:TextBox ID="Answer" Width="155px" runat="server"></asp:TextBox>
					<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Answer"
						ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSecurityAnswer %>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredSecurityAnswer %>'
						ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic" />
				</div>
			</div>
		</div>
		<div class="ImageButtons">
			<zn:LinkButton ID="btnSubmit" runat="server" ButtonType="LoginButton" ValidationGroup="ChangePassword1" OnClick="ResetPasswordPushButton_Click"
				CausesValidation="True" ButtonPriority="Normal" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>'  />
			&nbsp; &nbsp;  
				<zn:LinkButton ID="btnClear" runat="server" OnClick="BtnClear_Click" ButtonType="LoginButton"
					ButtonPriority="Normal" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
		</div>
	</asp:Panel>
	<!-- Success message section -->
	<asp:Panel ID="pnlSuccess" runat="server" Visible="false">
		<div class="SuccessText">
			<asp:Label ID="Localize19" Text='<%$ Resources:ZnodeAdminResource, SubTextPasswordChanged %>' runat="server"></asp:Label>
		</div>
		<div class="ContinueButton">
			<asp:LinkButton ID="ContinuePushButton" OnClick="ContinuePushButton_Click" runat="server"
				CausesValidation="False" CommandName="Continue" CssClass="Button" Text='<%$ Resources:ZnodeAdminResource, ButtonContinue %>' />
		</div>
	</asp:Panel>
</div>
