﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Alternate Image user control in marketplace.
    /// </summary>
    public partial class Admin_Controls_Default_AlternateImage : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private string addImageLink = "~/MallAdmin/Secure/product/add_view.aspx?itemid=";
        private int itemId;
        private ZNodeProductImageType _imageType = ZNodeProductImageType.AlternateImage;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the ImageType
        /// </summary>
        public ZNodeProductImageType ImageType
        {
            get { return this._imageType; }
            set { this._imageType = value; }
        }
        #endregion
  
        #region Public Methods
        /// <summary>
        /// Get the ImagePath
        /// </summary>
        /// <param name="imagefile">Name of the image file</param>
        /// <returns>Returns the image path</returns>
        public string GetImagePath(string imagefile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(imagefile);
        }
        #endregion

        #region Protected Methods and Events
        protected void BtnReject_Click(object sender, EventArgs e)
        {
            UserStoreAccess uss = new UserStoreAccess();
			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Vendor)
            {
                Response.Redirect(this.addImageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())) + "&typeid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(Convert.ToString((int)this._imageType))) + "&mode=alternateimages");
            }
            else
            {
                this.UpdateImageStatus(false);
            }
        }

        protected void BtnAccept_Click(object sender, EventArgs e)
        {
            UserStoreAccess uss = new UserStoreAccess();

			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Vendor)
            {
                Response.Redirect(this.addImageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())) + "&typeid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(Convert.ToString((int)this._imageType))) + "&mode=alternateimages");
            }
            else
            {
                this.UpdateImageStatus(true);
            }
        }

        /// <summary>
        /// Row command 
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GridThumb_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                Response.Redirect(this.addImageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())) + "&productimageid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(e.CommandArgument.ToString())) + "&typeid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(Convert.ToString((int)this._imageType))) + "&mode=alternateimages");
            }

            if (e.CommandName == "RemoveItem")
            {
                ZNode.Libraries.Admin.ProductViewAdmin prodadmin = new ProductViewAdmin();
                bool status = prodadmin.Delete(int.Parse(e.CommandArgument.ToString()));
                if (status)
                {
                    this.BindAlternateImages();
                }
            }
        }

        protected string GetStatusImageIcon(object state)
        {
            string imageSrc = "~/MallAdmin/Themes/Images/223-point_pendingapprove.gif";

            if (state.ToString() != string.Empty)
            {
                int stateId = Convert.ToInt32(state);

                if (stateId == Convert.ToInt32(ZNodeProductReviewState.Approved))
                {
                    imageSrc = "~/MallAdmin/Themes/Images/222-point_approve.gif";
                }

                if (stateId == Convert.ToInt32(ZNodeProductReviewState.Declined))
                {
                    imageSrc = "~/MallAdmin/Themes/Images/221-point_decline.gif";
                }

                if (stateId == Convert.ToInt32(ZNodeProductReviewState.ToEdit))
                {
                    imageSrc = "~/MallAdmin/Themes/Images/220-point_toedit.gif";
                }
            }

            return imageSrc;
        }

        protected void GridThumb_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            UserStoreAccess uss = new UserStoreAccess();

			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
            {
                // If Revier then hide the Action (Edit and Remove) column
                if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Footer)
                {
                    e.Row.Cells[8].Visible = false;
                }
            }
            else
            {
                // If Vendor then hide the checkbox column
                if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Footer)
                {
                    e.Row.Cells[0].Visible = false;
                }
            }
        }

        #endregion

        #region Page Load Methods
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId (Product Id) from QueryString        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                // Set the button caption based on the user and Image type.
                UserStoreAccess uss = new UserStoreAccess();
				if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
                {
                    btnReject.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonReject").ToString();
                }
                else
                {
                    // Hide the Accept Checked button from vendor.
                    btnAccept.Visible = false;
                    if (this.ImageType == ZNodeProductImageType.AlternateImage)
                    {
                        btnReject.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonAddNewAlternateImage").ToString();
                        btnReject.CssClass = "Size175";
                        lblCaption.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAddImage").ToString();
                    }
                    else
                    {
                        btnReject.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonAddNewSwatchImage").ToString();
                        btnReject.CssClass = "Size175";
                        lblCaption.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAddProductImage").ToString();
                    }
                }

                if (this.itemId > 0)
                {
                    this.BindAlternateImages();
                }
                else
                {
                    throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductNotFound").ToString());
                }
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Alternate images
        /// </summary>
        private void BindAlternateImages()
        {
            ProductViewAdmin imageadmin = new ProductViewAdmin();
            DataSet ds = imageadmin.GetAllAlternateImage(this.itemId);
            string filterExpression = string.Empty;

            // Filter based on image types
            filterExpression = "ProductImageTypeID=" + Convert.ToString((int)this._imageType);

            ds.Tables[0].DefaultView.RowFilter = filterExpression;
            GridThumb.DataSource = ds.Tables[0].DefaultView;
            GridThumb.DataBind();

            UserStoreAccess uss = new UserStoreAccess();

            // If there are not product alternate image or swatch image then hide both buttons.
			if (ds.Tables[0].DefaultView.Count == 0 && uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
            {
                btnAccept.Enabled = false;
                btnReject.Enabled = false;
            }

            btnReject.Enabled = ds.Tables[0].DefaultView.Count >= 5 ? false : true;
        }

        /// <summary>
        /// Update (Approved/Decline) the selected image status.
        /// </summary>
        /// <param name="status">Status to update.
        /// False: to update image as declined
        /// True: to update image as approved.
        /// </param>
        private void UpdateImageStatus(bool status)
        {
            List<string> selectedProductImagesIdList = new List<string>();
            for (int rowIndex = 0; rowIndex <= GridThumb.Rows.Count - 1; rowIndex++)
            {
                CheckBox chk = GridThumb.Rows[rowIndex].Cells[0].FindControl("chkSelect") as CheckBox;
                string productImageId = GridThumb.Rows[rowIndex].Cells[2].Text;
                if (chk.Checked)
                {
                    selectedProductImagesIdList.Add(productImageId);
                }
            }

            ZNode.Libraries.Admin.ProductViewAdmin productAdmin = new ProductViewAdmin();
            TList<ProductImage> productImageList = productAdmin.GetImageByProductID(this.itemId);
            foreach (string productImageId in selectedProductImagesIdList)
            {
                int imageId = Convert.ToInt32(productImageId);
                int reviewStateId = 10;
                TList<ProductImage> filteredProductImageList = productImageList.FindAll(delegate(ProductImage pi) { return pi.ProductImageID == imageId; });
                if (filteredProductImageList.Count == 1)
                {
                    ProductImage currentItem = filteredProductImageList[0];
                    reviewStateId = status == true ? Convert.ToInt32(ZNodeProductReviewState.Approved) : Convert.ToInt32(ZNodeProductReviewState.Declined);

                    // Update review state only if existing state is not same as current state.
                    if (reviewStateId != currentItem.ReviewStateID)
                    {
                        // Set the review state ID from enumeration
                        currentItem.ReviewStateID = reviewStateId;

                        // Set the product image as rejected.
                        productAdmin.Update(currentItem);
                    }
                }
            }

            // Reload the image list.
            this.BindAlternateImages();
        }
        #endregion
    }
}