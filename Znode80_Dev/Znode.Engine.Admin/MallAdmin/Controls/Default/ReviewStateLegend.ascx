﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Znode.Engine.MallAdmin.Admin_Controls_Default_ReviewStateLegend" Codebehind="ReviewStateLegend.ascx.cs" %>    
<div class="Legend">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/MallAdmin/Themes/Images/222-point_approve.gif" /><span>Approved</span>
    <asp:Image CssClass="Item" ID="Image3" runat="server" ImageUrl="~/MallAdmin/Themes/Images/221-point_decline.gif" /><span>Declined</span>
    <asp:Image ID="Image2" runat="server" ImageUrl="~/MallAdmin/Themes/Images/223-point_pendingapprove.gif" /><span>Pending Approval</span>
    <asp:Image ID="Image4" runat="server" ImageUrl="~/MallAdmin/Themes/Images/220-point_toedit.gif" /><span>To Edit</span>
</div>
