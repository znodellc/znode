﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Admin_Controls_Default_Marketing" Codebehind="Marketing.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<div align="left">
    <zn:Button runat="server" ID="btnEditSEO" OnClick="EditMarketing_Click" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonEditInformation %>' Width="150px"/>
</div>
<br />
<h4 class="SubTitle">
    <asp:Localize ID="SubTitleDisplay" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDisplay %>'></asp:Localize></h4>
<div class="Display">
    <div>
        <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
    </div>
    <div class="FieldStyle">
         <asp:Localize ID="NewIcon" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNewIcon %>'></asp:Localize></div>
    <div class="ValueStyle">
        <img id="chkIsNewItem" runat="server" alt="" src="" /></div>
    <div class="ClearBoth">
    </div>
    <br />
    <h4 class="SubTitle">
        <asp:Localize ID="TitleSEO" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSEO %>'></asp:Localize></h4>
    <uc1:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    <div>
        <asp:Localize ID="TextSEO" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextSEO %>'></asp:Localize></div>
    <uc1:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    <div class="FieldStyle">
     <asp:Localize ID="SEOTitleText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSETitle %>'></asp:Localize></div>
    <div class="ValueStyle">
        <asp:Label ID="lblSEOTitle" runat="server"></asp:Label></div>
    <div class="FieldStyleA">
       <asp:Localize ID="SEOKeyword" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEKeyword %>'></asp:Localize></div>
    <div class="ValueStyleA">
        <asp:Label ID="lblSEOKeywords" runat="server" CssClass="Price"></asp:Label>&nbsp;</div>
    <div class="FieldStyle">
       <asp:Localize ID="SEODescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEDescription %>'></asp:Localize></div>
    <div class="ValueStyle">
        <asp:Label ID="lblSEODescription" runat="server"></asp:Label>&nbsp;</div>
    <div class="FieldStyleA">
       <asp:Localize ID="SEOUrl" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOURL %>'></asp:Localize></div>
    <div class="ValueStyleA">
        <asp:Label ID="lblSEOURL" runat="server"></asp:Label>&nbsp;</div>
    <div class="ClearBoth">
        <br />
    </div>
</div>
<div align="left" style="display: none;">
    <zn:Button runat="server" ID="btnNotify" OnClick="BtnNotify_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonNotify %>'/>
</div>
