﻿using System;
using System.Configuration;
using System.Text;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Controls_Default_SupplierAutoComplete class.
    /// </summary>
    public partial class Admin_Controls_Default_SupplierAutoComplete : System.Web.UI.UserControl
    {
        private int dropdownItemCount = int.Parse(ConfigurationManager.AppSettings["DropdownMaxItemCount"]);

        /// <summary>
        /// Occurs when the content of the auto complete text box changes between posts to the server.
        /// </summary>
        private event System.EventHandler SelectedIndexChangedHandler;
        
        #region Public Properties       
        /// <summary>
        ///  Gets or sets the width of the auto complete control.
        /// </summary>
        public string Width
        {
            get { return txtSupplier.Width.ToString(); }
            set { txtSupplier.Width = new Unit(value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to enable auto postback or not.
        /// </summary>
        public bool AutoPostBack
        {
            get
            {
                if (ddlSupplier.Visible)
                {
                    return ddlSupplier.AutoPostBack;
                }

                return bool.Parse(hdnAutoPostBack.Value);
            }

            set
            {
                if (ddlSupplier.Visible)
                {
                    ddlSupplier.AutoPostBack = value;
                }

                hdnAutoPostBack.Value = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the text content of the hidden field control.
        /// </summary>
        public string Value
        {
            get
            {
                if (ddlSupplier.Visible)
                {
                    return ddlSupplier.SelectedValue;
                }

                return SupplierId.Value;
            }

            set
            {
                if (ddlSupplier.Visible)
                {
                    ListItem item = ddlSupplier.Items.FindByValue(value);
                    if (item != null)
                    {
                        ddlSupplier.SelectedValue = value;
                    }
                    else
                    {
                        ddlSupplier.SelectedIndex = 0;
                    }
                }

                SupplierId.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the text content of the auto complete control.
        /// </summary>
        public string Text
        {
            get { return txtSupplier.Text; }
            set { txtSupplier.Text = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the auto complete control is required.
        /// </summary>
        public bool IsRequired
        {
            get
            {
                return RequiredFieldValidator2.Visible;
            }

            set
            {
                if (ddlSupplier.Visible)
                {
                    RequiredFieldValidator2.ControlToValidate = "ddlSupplier";
                }
                else
                {
                    RequiredFieldValidator2.ControlToValidate = "txtSupplier";
                }

                RequiredFieldValidator2.Visible = value;
            }
        }       

        /// <summary>
        /// Sets the value for select index changed handler
        /// </summary>
        public System.EventHandler OnSelectedIndexChanged
        {
            set
            {
                if (ddlSupplier.Visible)
                {
                    ddlSupplier.SelectedIndexChanged += value;
                }
                else
                {
                    this.SelectedIndexChangedHandler = value;
                }
            }
        }

        /// <summary>
        /// Gets the supplier
        /// </summary>
        private TList<Supplier> SupplierListTable
        {
            get
            {
                SupplierService SupplierService = new SupplierService();
                TList<Supplier> Suppliers = SupplierService.GetAll();
                return Suppliers;
            }
        }      
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ddlSupplier.Visible = false;

            if (this.SupplierListTable.Count < this.dropdownItemCount && !this.Page.IsPostBack)
            {
                ddlSupplier.Visible = true;
                ddlSupplier.AutoPostBack = bool.Parse(hdnAutoPostBack.Value);
                txtSupplier.Visible = false;
                autoCompleteExtender3.Enabled = false;
                ddlSupplier.DataSource = this.SupplierListTable;
                ddlSupplier.DataTextField = "Name";
                ddlSupplier.DataValueField = "SupplierID";
                ddlSupplier.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder script = new StringBuilder();

            script.Append("<script language=\"JavaScript\" type=\"text/javascript\">\n");
            script.Append("<!--\n");
            script.Append("function documentOnKeyPress()\n");
            script.Append("{\n");
            script.Append(" var charCode = window.event.keyCode;\n");
            script.Append(" var elementType = window.event.srcElement.type;\n");
            script.Append("\n");
            script.Append(" if ( (charCode == 13) && (elementType == \"text\") )\n");
            script.Append("   {\n");
            script.Append("        // Cancel the keystroke completely\n");
            script.Append("        window.event.returnValue = false;\n");
            script.Append("        window.event.cancel = true;\n");
            script.Append("        // Or change it to a tab\n");
            script.Append("  //window.event.keyCode = 9;\n");
            script.Append("   }\n");
            script.Append("}\n");
            script.Append("document.onkeypress = documentOnKeyPress;\n");
            script.Append("// -->\n");
            script.Append("</script>\n");

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "OnKeyPressScript", script.ToString());

            if (ddlSupplier.Visible && !this.IsRequired && !this.Page.IsPostBack)
            {
                ListItem allItem = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), string.Empty);
                ddlSupplier.Items.Insert(0, allItem);
                ddlSupplier.SelectedIndex = 0;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the content of the auto complete text box changes
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AutoComplete_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SelectedIndexChangedHandler != null && !ddlSupplier.Visible)
            {
                this.SelectedIndexChangedHandler(sender, e);
            }
        }
        #endregion
    }
}