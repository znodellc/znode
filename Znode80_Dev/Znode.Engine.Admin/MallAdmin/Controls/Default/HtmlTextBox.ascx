<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Controls_HtmlTextBox" Codebehind="HtmlTextBox.ascx.cs" %>

<script language="javascript" type="text/javascript">
    tinyMCE.init({
	// General options
	mode : "exact",	
	elements: <%= "\"" + ZnodeEditBox.ClientID + "\"" %>,
	theme : "advanced",
	plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
	
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,bullist,numlist,|,cleanup,|,spellchecker",
    theme_advanced_buttons2 :"",
    theme_advanced_buttons3 : "",
    theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "",
	theme_advanced_resizing : true,
	spellchecker_rpc_url : "TinyMCE.ashx?module=SpellChecker",
	// Example content CSS (should be your site CSS)
	content_css : "css/content.css"

    });    
</script>

<asp:TextBox ID="ZnodeEditBox" runat="server" Rows="10" TextMode="MultiLine" Width="100%" Height="267px"></asp:TextBox> 