using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_products_delete class.
    /// </summary>
    public partial class Admin_Secure_products_delete : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId;        
        private string cancelPageLink = "list.aspx";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        private string _ProductName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the product name.
        /// </summary>
        public string ProductName
        {
            get { return this._ProductName; }
            set { this._ProductName = value; }
        } 
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring   
            if (Request.Params["Itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));

                this.BindData();
            }
            else
            {
                this.itemId = 0;
            }
        }
        #endregion       

        #region General Events

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.cancelPageLink);
        }

        /// <summary>
        /// Delete button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = productAdmin.GetByProductId(this.itemId);
            this.ProductName = product.Name;
            bool isDeleted = productAdmin.DeleteByProductID(this.itemId);

            if (isDeleted)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Product - " + this.ProductName, this.ProductName);
                Response.Redirect(this.cancelPageLink);
            }
            else
            {
                lblErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorItemsRemoved").ToString();
            }
        }
        #endregion
        #region Bind Data

        /// <summary>
        /// Bind the data for a Product id
        /// </summary>
        private void BindData()
        {
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = productAdmin.GetByProductId(this.itemId);
            if (product != null)
            {
                this.ProductName = product.Name;
            }
        }
        #endregion
    }
}