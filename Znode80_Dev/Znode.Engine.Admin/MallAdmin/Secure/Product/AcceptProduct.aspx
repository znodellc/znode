﻿<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/content.master" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Admin_Secure_Product_AcceptProduct"
    Title="Untitled Page" CodeBehind="AcceptProduct.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="clear: both">
        </div>
        <div class="FormView">
            <div>
                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="TitleProductID" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductID %>'></asp:Localize>:
                <asp:Label ID="lblProductID" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendor %>'></asp:Localize>:
                <asp:Label ID="lblVendor" runat="server"></asp:Label>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div style="text-align: center">
                <asp:Label ID="lblApprovedMsg" runat="server" Text=""></asp:Label>
            </div>
            <div>
                <br />
            </div>
            <div>
                <div class="FieldStyle" style="text-align: right; width: 50%">
                    <div>
                        <zn:Button runat="server" ID="btnBack" ButtonType="EditButton" OnClick="BtnBack_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToProduct %>'
                            CausesValidation="false" Width="175px"  />
                    </div>
                </div>
                <div class="ValueStyle" style="padding-top: 3px;">
                    <div runat="server" id="divNextProduct">
                        <zn:Button runat="server" ID="btnNextProduct" ButtonType="EditButton" OnClick="BtnNextProduct_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonReview %>'
                            CausesValidation="false" Width="175px" />
                    </div>
                </div>
            </div>
            <div>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
