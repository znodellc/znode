<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/content.master"
    AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Admin_Secure_products_list" ValidateRequest="false"
    CodeBehind="list.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="ProductReviewStateAutoComplete" Src="~/MallAdmin/Controls/Default/ProductReviewStateAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ReviewStateLegend" Src="~/MallAdmin/Controls/Default/ReviewStateLegend.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1>
                    <asp:Localize ID="Products" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleProducts %>'></asp:Localize>
                </h1>
            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAddProduct" runat="server" ButtonType="Button"
                    OnClick="BtnAddProduct_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, LinkButtonAddNewProduct %>'
                    ButtonPriority="Primary" />
            </div>
        </div>
        <div class="ClearBoth" align="left">
            <p>
                <asp:Localize ID="TextMallProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextMallProduct %>'></asp:Localize>
            </p>
            <br />
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="SearchProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleSearchProducts %>'></asp:Localize></h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle" runat="server" id="ReviewerSearch">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="Vendor" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendor %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtVendor" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="VendorId" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendorId %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtVendorId" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle" runat="server" id="div2CO" visible="false">
                        <span class="FieldStyle">
                            <asp:Localize ID="Id2COVendor" runat="server" Text='<%$ Resources:ZnodeAdminResource, Text2coVendorId %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txt2coid" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ProductName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductName %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductname" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ProductID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductID %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtVendorproductId" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ProductStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductStatus %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <ZNode:ProductReviewStateAutoComplete ID="txtProductStatus" runat="server" />
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>
                <div>
                    <zn:Button runat="server" ID="btnClear" OnClick="BtnClear_Click" CausesValidation="False" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' />
                    <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                    <div style="float: right" >
                        <zn:Button runat="server" ID="btnDeclineall" OnClick="BtnDeclineall_Click" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonDeclineSelected %>' Width="125px"/>
                        <zn:Button runat="server" ID="btnApproveAll" OnClick="BtnApproveAll_Click" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonApproveSelected %>' Width="125px" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <br />
        <div class="GridTitle"></div>
        <div class="ItemStyle">
            <div style="float: left; padding-top: 3px;">
                <h4 class="Title">
                    <asp:Localize ID="GridProductList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleProductList %>'></asp:Localize></h4>
            </div>
            <div style="float: right;">
                <ZNode:ReviewStateLegend ID="ReviewStateLegendTop" runat="server" />
            </div>
        </div>
        <div style="clear: both;"></div>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <asp:UpdatePanel ID="upProductList" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                    GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
                    OnRowCommand="UxGrid_RowCommand" EmptyDataText='<%$ Resources:ZnodeAdminResource, ErrorNoProducts %>'
                    Width="100%" CellPadding="4" OnRowDataBound="UxGrid_RowDataBound" OnDataBound="UxGrid_DataBound">
                    <Columns>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAll" AutoPostBack="true" runat="server" OnCheckedChanged="ChkSelectAll_CheckedChanged" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkProduct" runat="server" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="productid" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleImage %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <a href='<%# "view.aspx?itemid=" + GetEncryptId(DataBinder.Eval(Container.DataItem,"productid"))%>'
                                    id="LinkView">
                                    <img alt=" " src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                        runat="server" style="border: none" />
                                </a>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnName %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <a href='<%# GetViewPageLink(DataBinder.Eval(Container.DataItem, "productid"))%>'><%# Eval("name")%></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnSellingPrice %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# FormatPrice(DataBinder.Eval(Container.DataItem,"RetailPrice")) %>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleVendor %>' DataField="Vendor" HtmlEncode="false" HeaderStyle-HorizontalAlign="Left">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Changed Fields" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# GetChangedFields(DataBinder.Eval(Container.DataItem, "ProductID"))%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleInStock %>' DataField="Quantityonhand" HeaderStyle-HorizontalAlign="Left">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <img alt="" id="Img1" src='<%# ReviewStateLegendTop.GetImageUrl(DataBinder.Eval(Container.DataItem, "ReviewStateId").ToString())%>'
                                    runat="server" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <asp:LinkButton ID="Button1" CommandName="Manage" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                                    Text='<%$ Resources:ZnodeAdminResource, LinkManage %>' runat="server" CssClass="actionlink" />
                                &nbsp;<asp:LinkButton ID="Button5" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                                    Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' runat="server" CssClass="actionlink" />
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
