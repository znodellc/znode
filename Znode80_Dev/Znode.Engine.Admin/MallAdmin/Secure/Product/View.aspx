<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/content.master" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Admin_Secure_Product_View" Title="Product View" CodeBehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="AlternateImage" Src="~/MallAdmin/Controls/Default/AlternateImage.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Marketing" Src="~/MallAdmin/Controls/Default/Marketing.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Options" Src="~/MallAdmin/Controls/Default/AddOn.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Product" Src="~/MallAdmin/Secure/Product/Product.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ReviewStateLegend" Src="~/MallAdmin/Controls/Default/ReviewStateLegend.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductTags" Src="~/MallAdmin/Secure/Product/ProductTags.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ProductFacets" Src="~/MallAdmin/Secure/Product/ProductFacets.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 70%">
            <h1>
                <asp:Label ID="lblTitle" runat="server" Text="Label"></asp:Label></h1>
        </div>
        <div class="LeftFloat" style="width: 29%" align="right">
            <zn:Button runat="server" Width="175px" ID="btnBack" ButtonType="EditButton" CausesValidation="False" OnClick="BtnBack_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToListingPage %>' />
        </div>
    </div>
    <div class="ClearBoth">
    </div>
    <div class="Display" style="display: none;">
        <div class="ValueStyle">
            <asp:Label ID="VendorProductID" runat="server" Text="ProductID: {0}"></asp:Label>
            <br />
            <asp:Label ID="VendorName" runat="server" Text="Vendor: {0}"></asp:Label>
            <uc1:Spacer ID="Spacer8" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
    </div>
    <div class="ClearBoth">
    </div>
    <div>
        <ajaxToolKit:TabContainer ID="tabProductDetails" runat="server">
            <ajaxToolKit:TabPanel ID="pnlGeneral" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="TabTitleProductInfo" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleProductInfo%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <asp:UpdatePanel ID="upProductInfo" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div>
                                <div>
                                    <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
                                </div>
                                <div>
                                    <uc1:Spacer ID="Spacer7" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                                </div>
                                <div align="left">
                                    <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnTopEditProduct_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonEditInformation%>' ID="btnTopEditProduct" Width="150px" />
                                    <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnReject_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonReject%>' ID="btnTopReject" />
                                    <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnAccept_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAccept%>' ID="btnTopAccept" />
                                </div>
                                <br />
                                <h4 class="SubTitle">
                                    <asp:Localize ID="SubTitlePortalDisplay" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitlePortalDisplay%>'></asp:Localize></h4>
                                <div class="Display">
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnProductName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductName%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblProdName" runat="server"></asp:Label>
                                        &#160;
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="ColumnProductType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductType%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblProdType" runat="server"></asp:Label>&nbsp;
                                    </div>
                                    <div class="MainStyle">
                                        <div class="FieldStyle">
                                            <asp:Localize ID="ColumnProductDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductDescription%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyle">
                                            <asp:Label ID="lblProductDescription" runat="server"></asp:Label>
                                            &#160;
                                        </div>
                                    </div>
                                    <div class="MainStyle">
                                        <div class="FieldStyleA">
                                            <asp:Localize ID="ColumnProductDetails" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductDetails%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyleA">
                                            <asp:Label ID="lblProductDetails" runat="server"></asp:Label>
                                            &#160;
                                        </div>
                                    </div>
                                    <div class="MainStyle">
                                        <div class="FieldStyle">
                                            <asp:Localize ID="ColumnShippingInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingInformation%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyle">
                                            <asp:Label ID="lblShippingDescription" runat="server"></asp:Label>
                                            &#160;
                                        </div>
                                    </div>
                                    <div class="MainStyleA">
                                        <div class="FieldStyleA">
                                            <asp:Localize ID="ColumnProductImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductImage%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyleA">
                                            <asp:Image ID="ItemImage" runat="server" />
                                            &#160;
                                        </div>
                                    </div>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnSellingPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSellingPrice%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblRetailPrice" runat="server" CssClass="Price"></asp:Label>
                                        &#160;
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="ColumnShowProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShowProduct%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Image ID="imgShowProduct" runat="server" />
                                    </div>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnAffiliateUrl" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAffiliateUrl%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblAffiliateUrl" runat="server"></asp:Label>
                                        &#160;
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="ColumnCategories" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCategories%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA" style="padding-top: 10px; overflow: auto; height: 250px;">
                                        <div class="FieldStyle" style="width: 300px;">
                                            <asp:TreeView ID="CategoryTreeView" runat="server" Enabled="False" ImageSet="Arrows"
                                                ShowCheckBoxes="All" CssClass="TreeView" ShowLines="True" ExpandDepth="0">
                                                <ParentNodeStyle Font-Bold="False" />
                                                <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                                                <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px"
                                                    VerticalPadding="0px" />
                                                <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="0px"
                                                    NodeSpacing="0px" VerticalPadding="0px" Font-Bold="false" />
                                            </asp:TreeView>
                                        </div>
                                        <div class="ValueStyle" style="width: 300px; display: none;">
                                            <asp:Label ID="lblCategories" runat="server"></asp:Label>
                                        </div>
                                        &#160;
                                    </div>
                                    <h4 class="SubTitle">
                                        <asp:Localize ID="SubTitleShippingSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleShippingSettings%>'></asp:Localize></h4>
                                    <div class="FieldStyle">

                                        <asp:Localize ID="ColumnWeight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWeight%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblshipweight" runat="server"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyleA">

                                        <asp:Localize ID="ColumnHeight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnHeight%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblHeight" runat="server"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyle">

                                        <asp:Localize ID="ColumnWidth" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWidth%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblWidth" runat="server"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyleA">

                                        <asp:Localize ID="ColumnLength" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLength%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblLength" runat="server"></asp:Label>&nbsp;
                                    </div>
                                    <div class="ClearBoth">
                                        <br />
                                    </div>
                                    <h4 class="SubTitle">

                                        <asp:Localize ID="Localize7" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleInventorySettings%>'></asp:Localize></h4>
                                    <div class="FieldStyle">

                                        <asp:Localize ID="Localize6" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnQuantityOnHand%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblQuantityOnHand" runat="server" CssClass="Price"></asp:Label>
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Image ID="imgTrackInventory" runat="server" />
                                        &#160;
                                    </div>
                                    <div class="ValueStyleA">

                                        <asp:Localize ID="ColumnTrackInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTrackInventory%>'></asp:Localize>
                                        &#160;
                                    </div>
                                    <div>
                                        <div class="FieldStyle">
                                            <asp:Image ID="imgAllowBackOrder" runat="server" />
                                        </div>
                                        <div class="ValueStyle">

                                            <asp:Localize ID="ColumnBackOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBackOrder%>'></asp:Localize>
                                            &#160;
                                        </div>
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Image ID="imgDontTrackInventory" runat="server" />
                                    </div>
                                    <div class="ValueStyleA">

                                        <asp:Localize ID="ColumnNoTrackInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnNoTrackInventory%>'></asp:Localize>
                                        &#160;
                                    </div>
                                    <div class="ClearBoth">
                                        <br />
                                    </div>
                                    <div style="display: none">
                                        <div class="FieldStyleA">

                                            <asp:Localize ID="ColumnInStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnInStockMessage%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyleA">
                                            <asp:Label ID="lblInStockMessage" runat="server"></asp:Label>
                                            &#160;
                                        </div>
                                    </div>
                                    <div class="FieldStyle">

                                        <asp:Localize ID="ColumnTitleOutofStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOutofStockMessage%>'></asp:Localize>
                                        &#160;
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblOutofStockMessage" runat="server"></asp:Label>
                                        &#160;
                                    </div>
                                    <div class="FieldStyleA">

                                        <asp:Localize ID="ColumnTitleBackOrderMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBackOrderMessage%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblBackOrderMessage" runat="server"></asp:Label>
                                        &#160;
                                    </div>
                                    <div class="ClearBoth">
                                        <br />
                                    </div>
                                    <div style="display: none">
                                        <h4 class="SubTitle">
                                            <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCheckoutOptions%>'></asp:Localize></h4>
                                        <div class="FieldStyle">
                                            <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTangible%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyle">
                                            <img id="imgTangible" runat="server" />
                                            &#160;
                                        </div>
                                        <div class="ClearBoth">
                                            <br />
                                        </div>
                                        <div class="FieldStyleA">
                                            <asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWeight%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyleA">
                                            <asp:Label ID="lblWeight" runat="server"></asp:Label>
                                            &#160;
                                        </div>
                                        <div class="FieldStyle">
                                            <asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnHandling%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyle">
                                            <asp:Label ID="lblHandling" runat="server" CssClass="Price"></asp:Label>
                                            &#160;
                                        </div>
                                        <div class="FieldStyleA">
                                            <asp:Localize ID="Localize5" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRecurrring%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyleA">
                                            <img id="imgRecurring" runat="server" />
                                            &#160;
                                        </div>
                                        <div class="FieldStyle">
                                            <asp:Localize ID="Localize8" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStartupFee%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyle">
                                            <asp:Label ID="lblStartupFee" runat="server"></asp:Label>
                                            &#160;
                                        </div>
                                        <div class="FieldStyleA">
                                            <asp:Localize ID="Localize9" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBillEvery%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyleA">
                                            <asp:Label ID="lblBillingPeriod" runat="server"></asp:Label>
                                            &#160;
                                        </div>
                                    </div>
                                    <div class="ClearBoth">
                                        <br />
                                    </div>
                                    <div runat="server" id="divReviewHistory">
                                        <h4 class="SubTitle">
                                            <asp:Localize ID="Localize10" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleHistory%>'></asp:Localize></h4>
                                        <div>
                                            <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                                        </div>
                                        <div>
                                            <asp:GridView ID="gvReviewHistory" ShowFooter="True" CaptionAlign="Left" runat="server"
                                                ForeColor="Black" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid"
                                                Width="100%" GridLines="None" AllowPaging="True" OnRowCommand="GvReviewHistory_RowCommand"
                                                OnPageIndexChanging="GvReviewHistory_PageIndexChanging">
                                                <Columns>
                                                    <asp:BoundField DataField="LogDate" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDate%>'>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UserName" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleUser%>'>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Status" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus%>'>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnRejectedReason%>' HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblReason" runat="server" Text='<%# FormatReason(DataBinder.Eval(Container.DataItem,"Reason")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton CssClass="actionlink" ID="ViewReviewDetails" Text="Link Text View" CommandArgument='<%# Eval("ProductReviewHistoryID") %>'
                                                                CommandName="View" runat="Server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <asp:Localize ID="RecordNotFoundReviewHistory" runat="server" Text='<%$ Resources:ZnodeAdminResource, RecordNotFoundReviewHistory%>'></asp:Localize>
                                                </EmptyDataTemplate>
                                                <RowStyle CssClass="RowStyle" />
                                                <HeaderStyle CssClass="HeaderStyle" />
                                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                                <FooterStyle CssClass="FooterStyle" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="ClearBoth">
                                    <br />
                                </div>
                                <div align="left">
                                    <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnTopEditProduct_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonEditInformation%>' ID="btnBottomEditProduct" Width="150px" />
                                    <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnReject_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonReject%>' ID="btnBottomReject" />
                                    <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnAccept_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAccept%>' ID="btnBottomAccept" />
                                </div>
                                <br />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlManageinventory" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="TabTitleSKUs" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleSKUs%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <div class="Form">
                        <div>
                            <uc1:Spacer ID="Spacer15" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                        </div>
                        <asp:Panel ID="pnlSKUAttributes" runat="server">
                            <div class="ButtonStyle">
                                <zn:LinkButton ID="butAddNewSKU" runat="server" ButtonType="Button"
                                    OnClick="BtnAddSKU_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, LinkAddSKUorPart%>'
                                    ButtonPriority="Primary" />
                            </div>
                            <div>
                                <uc1:Spacer ID="Spacer21" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                            </div>
                            <div class="SearchForm">
                                <div class="FieldStyle">
                                    <asp:Localize ID="Localize11" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchSkus%>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:PlaceHolder ID="ControlPlaceHolder" runat="server"></asp:PlaceHolder>
                                </div>
                                <div>
                                    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear%>' ID="btnClear" />
                                    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch%>' CausesValidation="False" ID="btnSearch" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <h4 class="GridTitle">
                        <asp:Localize ID="SubTitleCurrentInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCurrentInventory%>'></asp:Localize></h4>
                    <asp:UpdatePanel ID="updPnlInventoryDisplayGrid" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="uxGridInventoryDisplay" Width="100%" CssClass="Grid" CellPadding="4"
                                CaptionAlign="Left" GridLines="None" runat="server" AutoGenerateColumns="False"
                                AllowPaging="True" ForeColor="Black" OnPageIndexChanging="UxGridInventoryDisplay_PageIndexChanging"
                                OnRowCommand="UxGridInventoryDisplay_RowCommand" OnRowDeleting="UxGridInventoryDisplay_RowDeleting"
                                PageSize="25">
                                <FooterStyle CssClass="FooterStyle" />
                                <Columns>
                                    <asp:BoundField DataField="sku" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSkuorPart%>' HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="skuid" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnQuantityOnHand%>'>
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="quantity" Text='<%# GetSkuQuantity((int)Eval("skuid")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnReOrderLevel%>'>
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="reorderlevel" Text='<%# GetSkuReorderLevel((int)Eval("skuid")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnIsActive%>' HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <img alt="" id="Img3" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div id="Test" runat="server">
                                                <asp:LinkButton CssClass="actionlink" ID="EditSKU" Text='<%$ Resources:ZnodeAdminResource, LinkEdit%>' CommandArgument='<%# Eval("skuid") %>'
                                                    CommandName="Edit" runat="Server" />
                                                <asp:LinkButton ID="RemoveSKU" CssClass="actionlink" Text='<%$ Resources:ZnodeAdminResource, LinkRemove%>' CommandArgument='<%# Eval("skuid") %>'
                                                    CommandName="Delete" runat="Server" Visible='<%# HasAttributes  %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="RowStyle" />
                                <EditRowStyle CssClass="EditRowStyle" />
                                <PagerStyle CssClass="PagerStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            </asp:GridView>
                            <uc1:Spacer ID="Spacer10" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                            <asp:Label ID="lblSkuErrorMsg" runat="server" CssClass="Error"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlAlternateImages" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="TabTileAlternateImages" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTileAlternateImages%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <div>
                        <uc1:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <h1 class="SubTitle">
                        <asp:Localize ID="SubTitleAlternateImages" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleAlternateImages%>'></asp:Localize></h1>
                    <div>
                        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <asp:UpdatePanel ID="updPnlGridThumb" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ZNode:AlternateImage ID="uxAlternateImage" UserType="Reviewer" ImageType="AlternateImage"
                                runat="server"></ZNode:AlternateImage>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div>
                        <uc1:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <h1 class="SubTitle">
                        <asp:Localize ID="SubTitleColorSwatches" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleColorSwatches%>'></asp:Localize></h1>
                    <div>
                        <uc1:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ZNode:AlternateImage ID="uxSwatchImages" UserType="Reviewer" ImageType="Swatch"
                                runat="server"></ZNode:AlternateImage>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="float: right; padding-top: 15px;">
                        <ZNode:ReviewStateLegend ID="uxReviewStateLegend" runat="server" />
                    </div>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlBundleProduct" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="TabTitleBundle" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleBundle%>'></asp:Localize>

                </HeaderTemplate>
                <ContentTemplate>
                    <uc1:Spacer ID="Spacer9" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                    <div class="ButtonStyle">
                        <zn:LinkButton ID="AddBundleProducts" runat="server" ButtonType="Button"
                            OnClick="AddBundleProducts_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, LinkAssociateProduct%>'
                            ButtonPriority="Primary" />
                    </div>
                    <br />
                    <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                    <asp:UpdatePanel ID="updPnlBundleProductGrid" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="uxBundleProductGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                                runat="server" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid" Width="100%"
                                OnPageIndexChanging="UxBundleProductGrid_PageIndexChanging" OnRowCommand="UxBundleProductGrid_RowCommand"
                                GridLines="None" AllowPaging="True" PageSize="10">
                                <Columns>
                                    <asp:BoundField DataField="ChildProductId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID%>' HeaderStyle-HorizontalAlign="Left" />
                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName%>' HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProductName" runat="server" Text='<%# GetProductName(Eval("ChildProductId")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive%>' HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(GetProductIsActive(Eval("ChildProductId")))%>'
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkRemove%>' CommandArgument='<%# Eval("ParentChildProductId") %>'
                                                CommandName="RemoveItem" CssClass="actionlink" runat="server" OnClientClick="return DeleteConfirmation();" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Localize ID="RecordNotFoundProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, RecordNotFoundProduct%>'></asp:Localize>
                                </EmptyDataTemplate>
                                <RowStyle CssClass="RowStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                <FooterStyle CssClass="FooterStyle" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlProductOptions" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="TabTitleAddOns" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleAddOns%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <div>
                        <uc1:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <h1 class="SubTitle">
                        <asp:Localize ID="ProductAddOnsSubTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleProductAddOns%>'></asp:Localize></h1>
                    <p>
                        <asp:Localize ID="ProductAddOnsSubText" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextProductAddOns%>'></asp:Localize></p>
                    <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                    <asp:UpdatePanel ID="updPnlProductAddOnsGrid" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <%-- Place Add On User control here --%>
                            <ZNode:Options ID="Options1" runat="server" UserType="Reviewer"></ZNode:Options>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="tabpnlTags" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="TabTitleFacets" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleFacets%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <uc1:ProductFacets runat="server" />
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlTag" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="TabTitleTags" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleTags%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <ZNode:ProductTags runat="server" ID="ProductTag"></ZNode:ProductTags>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlMarketing" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="TabTitleMarketing" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleMarketing%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <div>
                        <uc1:Spacer ID="Spacer20" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <ZNode:Marketing runat="server" ID="Marketing"></ZNode:Marketing>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="tabPreview" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="Localize14" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitlePreview%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <div>
                        <uc1:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <ZNode:Product runat="server" ID="uxProduct"></ZNode:Product>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
        </ajaxToolKit:TabContainer>
    </div>

    <script language="javascript" type="text/javascript">
        function DeleteConfirmation() {
            return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDelete")) %>');
    }
    </script>

</asp:Content>
