﻿<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/content.master" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Admin_Secure_Product_ViewProductReview"
    Title="Untitled Page" CodeBehind="ViewProductReview.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="clear: both">
        </div>
        <div class="FormView">
            <div>
                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnProductName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductName%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblProductName" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnVendor" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnVendor%>'></asp:Localize>
            </div>
            <br />
            <div class="ValueStyle">
                <asp:Label ID="lblVendor" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDate%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblDateModified" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnEditedBy" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEditedBy%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblUserName" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStatus%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblStatus" runat="server"></asp:Label>
            </div>
            <br />
            <div class="FieldStyle">
                <asp:Localize ID="ColumnReasonForRejection" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnReasonForRejection%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblReason" runat="server"></asp:Label>
            </div>
            <br />
            <div class="FieldStyle">
                <asp:Localize ID="ColumnRejectionDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRejectionDescription%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </div>
            <div class="ClearBoth">
            </div>
            <div>
                <zn:Button runat="server" ButtonType="CancelButton" CssClass="Size100" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancel" />
            </div>
        </div>
    </div>
</asp:Content>
