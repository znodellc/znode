<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/edit.master" AutoEventWireup="true"
    ValidateRequest="false" MaintainScrollPositionOnPostback="true" Inherits="Znode.Engine.MallAdmin.Admin_Secure_products_add"
    CodeBehind="add.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>

<%@ Register Src="~/MallAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<%@ Register Src="~/MallAdmin/Controls/Default/ImageUploader.ascx" TagName="UploadImage"
    TagPrefix="ZNode" %>
<%@ Register Src="~/MallAdmin/Controls/Default/ProductTypeAutoComplete.ascx" TagName="ProductTypeAutoComplete" TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">

    <script language="javascript" type="text/javascript">
        function getSelectedNodeText(obj) {
            var selectedNodes = '<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ColumnCurrentCategories")) %>';
            // Get all TD elements from treeview
            var tdCollection = obj.getElementsByTagName("td");

            var selectedCategoryCount = 0;
            for (var index = 0; index <= tdCollection.length - 1; index++) {
                var node = tdCollection[index].innerHTML;

                if (node.search('CHECKED') >= 0) {
                    selectedNodes += tdCollection[index].innerText + '<BR>';
                    selectedCategoryCount = selectedCategoryCount + 1;
                }

            }

            // Check is category node selected in treeview
            if (selectedCategoryCount == 0) {
                selectedNodes += '<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ErrorSelectCategory")) %>';
            }
            document.getElementById('<%=lblCategories.ClientID %>').innerHTML = selectedNodes;
        }
        //to validate category on client side
        function ClientValidateCategory(source, arguments) {
            var treeView = document.getElementById("<%= CategoryTreeView.ClientID %>");
            var checkBoxes = treeView.getElementsByTagName("input");
            var checkedCount = 0;
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].checked) {
                    checkedCount++;
                }
            }
            if (checkedCount > 0) {
                arguments.IsValid = true;
            } else {
                arguments.IsValid = false;
            }
        }

    </script>

    <asp:HiddenField ID="existSKUvalue" runat="server" />
    <div class="Form">
        <div class="LeftFloat" style="width: 50%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ID="btnSubmit" ButtonType="EditButton" OnClick="BtnNext_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonApproval %>' />
            <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="false" />
        </div>
        <div style="clear: both">
            <asp:Label ID="Label1" CssClass="Error" runat="server"></asp:Label>
        </div>
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="FormView">
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="GeneralInformation" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralInformation %>'></asp:Localize>
            </h4>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleProductName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductName" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipProductName %>' runat="server" Width="252px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProductName"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductName%>' CssClass="Error" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>
            <div id="Div1" runat="server">
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnSKUorPart" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSKUorPart %>'></asp:Localize><span class="Asterix">*</span><br />
                    <small>
                        <asp:Localize ID="ColumnTextSKUorPart" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextSKUorPart%>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSku" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipSKU %>' runat="server" Width="252px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSku"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSKUorPart %>' CssClass="Error" Display="dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnProductCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductCode %>'></asp:Localize><span class="Asterix">*</span><br />
                <small>
                    <asp:Localize ID="ColumnTextProductCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextMallProductCode %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductNum" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipProductNumber %>' runat="server" Width="252px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ProductName" runat="server" ControlToValidate="txtProductNum"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductNumber %>' CssClass="Error" Display="dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>

            <asp:Panel ID="pnlProdType" runat="server">
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnProductType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductType %>'></asp:Localize><br />
                    <small>
                        <asp:Localize ID="ColumnTextProductType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductType %>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <ZNode:ProductTypeAutoComplete ID="ProductTypeList" runat="server" Width="152px" IsRequired="true" AutoPostBack="true" />
                    <br />
                    <small>
                        <asp:Literal runat="server" ID="lblProductTypeHint"></asp:Literal></small>
                </div>
                <div class="FieldStyle" id="DivAttributes" runat="Server" visible="false">
                    <asp:Localize ID="ColumnProductAttributes" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductAttributes %>'></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:PlaceHolder ID="ControlPlaceHolder" runat="server"></asp:PlaceHolder>
                </div>
            </asp:Panel>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnShortDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShortDescription %>'></asp:Localize><br />
                <small>
                    <asp:Localize ID="ColumnTitleShortDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleShortDescription %>'></asp:Localize></small>
                <br />
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtShortDesc" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipShortDescription %>' MaxLength="255" Height="100px"
                    Rows="5" runat="server" Width="645px" TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnLongDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLongDescription %>'></asp:Localize><br />
                <small>
                    <asp:Localize ID="TextCharacterLimit" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextCharacterLimit %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="htmlPortalDesc" runat="server"></uc1:HtmlTextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnProductFeatures" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductFeatures %>'></asp:Localize><br />
                <small>
                    <asp:Localize ID="ColumnTextProductFeatures" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductFeatures %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="htmlProductDetail" runat="server"></uc1:HtmlTextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ShippingDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleShippingDescription %>'></asp:Localize><br />
                <small>
                    <asp:Localize ID="ShippingInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextShippingInformation %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="htmlShippingDesc" runat="server"></uc1:HtmlTextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnSalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSalePrice %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtproductRetailPrice" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipSellingPrice %>' runat="server" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSellingPrice %>'
                    ControlToValidate="txtproductRetailPrice" CssClass="Error" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtproductRetailPrice"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareSellingPrice %>'
                    CssClass="Error" Display="Dynamic" /><br />
                <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtproductRetailPrice"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeSellingPrice %>'
                    MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
            </div>
            <div visible="false" runat="server">
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnMSRP" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleMsrp %>'></asp:Localize>
                    <br />
                    <small>
                        <asp:Localize ID="TextMsrp" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextMsrp %>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtproductSalePrice" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipMSRP %>' runat="server" MaxLength="10"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtproductSalePrice"
                        Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareSalePrice %>'
                        CssClass="Error" Display="Dynamic" />
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ShowProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleShowProduct %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkActiveInd" Checked="true" ToolTip='<%$ Resources:ZnodeAdminResource, TooltTipYourProduct %>' Text='<%$ Resources:ZnodeAdminResource, CheckboxMallProduct %>'
                    runat="server" />
            </div>

            <div class="FieldStyle">
                <asp:Localize ID="AffiliateUrl" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAffiliateUrl %>'></asp:Localize><br />
                <small>
                    <asp:Localize ID="TextAffiliateURL" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextAffiliateURL %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtAffiliateLink" Enabled="true" Columns="50" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvAffliateUrl" Enabled="false" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAffiliateUrl %>'
                    ControlToValidate="txtAffiliateLink" CssClass="Error" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexValidator" runat="server" ControlToValidate="txtAffiliateLink"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidAffiliateUrl %>' CssClass="Error"
                    Display="dynamic" ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularExpressionValidURL %>'></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="Categories" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCategories %>'></asp:Localize><span class="Asterix" />*<br />
                <small>
                    <asp:Localize ID="TextCategories" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextCategories %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:UpdatePanel runat="server" ID="TreeViewUpdatePanel">
                    <ContentTemplate>
                        <div class="TreeFieldStyle">
                            <asp:TreeView ID="CategoryTreeView" runat="server" ImageSet="Arrows" ShowCheckBoxes="All"
                                ShowLines="True" ExpandDepth="0">
                                <ParentNodeStyle ForeColor="Black" />
                                <HoverNodeStyle Font-Underline="True" ForeColor="Black" />
                                <RootNodeStyle ForeColor="Black" />
                                <SelectedNodeStyle HorizontalPadding="0px" VerticalPadding="0px" />
                                <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="0px"
                                    NodeSpacing="0px" VerticalPadding="0px" />
                            </asp:TreeView>
                        </div>
                        <br />
                        <div class="ValueStyle">
                            <asp:CustomValidator ID="CustomValidator1" runat="server" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ErrorSelectCategory %>' ClientValidationFunction="ClientValidateCategory" SetFocusOnError="True"></asp:CustomValidator>
                        </div>
                        <div class="ValueStyle" style="width: 300px;">
                            <asp:Label ID="lblCategories" runat="server" Visible="false"></asp:Label>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="SelectanImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectanImage %>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextImage" Text='<%$ Resources:ZnodeAdminResource, HintTextImage%>'></asp:Localize></small>
            </div>
            <div id="tblShowImage" runat="server" style="margin: 10px 0px 10px 0px;">
                <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;" runat="server"
                    id="pnlImage">
                    <asp:Image ID="Image1" runat="server" />
                </div>
                <div class="LeftFloat" style="margin-left: -1px;" runat="server" id="pnlUploadSection">
                    <asp:Panel runat="server" ID="pnlShowImage">
                        <div>
                            <asp:Localize ID="SelectanOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectanOption %>'></asp:Localize>
                        </div>
                        <div>
                            <asp:RadioButton ID="RadioProductCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage %>' runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioProductNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage %>' runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductNewImage_CheckedChanged" />
                        </div>
                    </asp:Panel>UploadImage
                    <br />
                    <div id="tblProductDescription" runat="server" visible="false">
                        <div>
                            <ZNode:UploadImage ID="uxProductImage" runat="server"></ZNode:UploadImage>
                        </div>
                        <asp:Label runat="server" CssClass="Error" ForeColor="Red" ID="lblProductImageError"
                            Visible="False"></asp:Label>
                    </div>
                    <asp:Panel ID="pnlImageName" runat="server" Visible="false">
                        <div class="FieldStyle">
                            <asp:Localize ID="ImageFileName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnImageFileName %>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtimagename" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipImageFileName %>' runat="server"></asp:TextBox>
                        </div>
                    </asp:Panel>
                </div>
            </div>

            <h4 class="SubTitle">
                <asp:Localize ID="ProductAttributes" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleProductAttributes%>'></asp:Localize>
            </h4>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnWeight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWeight %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtproductshipweight" runat="server" Width="46px" MaxLength="7"> </asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.WeightUnit %>
                <asp:RangeValidator Enabled="false" ID="RangeValidator1" runat="server"
                    ControlToValidate="txtproductshipweight" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeProductWeight %>'
                    MaximumValue="999999" MinimumValue="0.1" CultureInvariantValues="true" Type="Double"></asp:RangeValidator>
                <asp:RequiredFieldValidator Enabled="false" ID="RequiredFieldValidator4" runat="server"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductWeight %>' ControlToValidate="txtproductshipweight"
                    CssClass="Error" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtproductshipweight"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareProductWeight %>' CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnHeight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnHeight%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductHeight" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtProductHeight"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareProductHeight %>'
                    CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnWidth" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWidth %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductWidth" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txtProductWidth"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareProductWidth %>'
                    CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnLength" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLength %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductLength" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txtProductLength"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareProductLength %>'
                    CssClass="Error" Display="Dynamic" />
            </div>
            <h4 class="SubTitle">
                <asp:Localize ID="InventorySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleInventorySettings %>'></asp:Localize>
            </h4>
            <asp:Panel ID="pnlquantity" runat="server">
                <div class="FieldStyle">
                    <asp:Localize ID="QuantityOnHand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnQuantityOnHand %>'></asp:Localize><span class="Asterix">*</span><br />
                    <small>
                        <asp:Localize ID="ColumnTextQuantityOnHand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextQuantityOnHand%>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtProductQuantity" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipQuantityOnHand %>' runat="server" Rows="3">999</asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtProductQuantity"
                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' MaximumValue="999999"
                        MinimumValue="-999999" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtProductQuantity"
                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductQuantity %>' SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
            </asp:Panel>
            <div class="FieldStyle">
                <asp:Localize ID="OutofStockOptions" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOutofStockOptions %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:RadioButtonList ID="InvSettingRadiobtnList" runat="server">
                    <asp:ListItem Selected="True" Value="1" Text='<%$ Resources:ZnodeAdminResource, RadioButtonDisableOutofstockProducts %>'></asp:ListItem>
                    <asp:ListItem Value="2" Text='<%$ Resources:ZnodeAdminResource, RadioButtonAllowBackOrderProducts %>'></asp:ListItem>
                    <asp:ListItem Value="3" Text='<%$ Resources:ZnodeAdminResource, RadioButtonDontTrackInventory %>'></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div visible="false" runat="server">
                <div class="FieldStyle">
                    <asp:Localize ID="InStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleInStockMessage %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtInStockMsg" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipInStock %>' runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="OutofStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOutofStockMessage %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtOutofStock" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipOutofStock %>' runat="server"
                    Text='<%$ Resources:ZnodeAdminResource, TextBoxOutofStock %>'></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="BackOrderMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBackOrderMessage %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBackOrderMsg" runat="server"></asp:TextBox>
            </div>
            <div visible="false" runat="server">
                <h4 class="SubTitle">
                    <asp:Localize ID="Checkout" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCheckoutOptions %>'></asp:Localize>
                </h4>
                <div class="FieldStyle">
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="chkShipInd" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipTangible %>' Text='<%$ Resources:ZnodeAdminResource, CheckBoxTangible %>' runat="server" />
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="Weight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWeight %>'></asp:Localize><br />
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtProductWeight" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipWeight %>' Enabled="false" runat="server"
                        Width="46px" MaxLength="7"> </asp:TextBox>&nbsp;<asp:RangeValidator Enabled="false"
                            ID="weightBasedRangeValidator" runat="server" ControlToValidate="txtProductWeight"
                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeProductWeight %>'
                            MaximumValue="999999" MinimumValue="0.1" CultureInvariantValues="true" Type="Double"></asp:RangeValidator><asp:RequiredFieldValidator
                                Enabled="false" ID="RequiredForWeightBasedoption" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterWeight %>'
                                ControlToValidate="txtProductWeight" CssClass="Error" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator><div>
                                    <asp:CompareValidator ID="WeightFieldCompareValidator" runat="server" ControlToValidate="txtProductWeight"
                                        Type="Currency" Operator="DataTypeCheck" ErrorMessage='' CssClass="Error" Display="Dynamic" />
                                </div>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnHandling" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleHandling %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtHandling" runat="server" Enabled="false" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipHandling %>' Width="46px"></asp:TextBox>&nbsp;&nbsp;&nbsp;<asp:CompareValidator
                        ID="CompareValidator5" runat="server" ControlToValidate="txtHandling" Type="Currency"
                        Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareProductHandling %>'
                        CssClass="Error" Display="Dynamic" />
                </div>
                <div class="FieldStyle">
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="chkRecurringInd" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipRecurringCharge %>' Text='<%$ Resources:ZnodeAdminResource, CheckBoxRecurring %>'
                        runat="server" />
                </div>
                <div id="divRecurringBilling">
                    <div class="FieldStyle">
                        <asp:Localize ID="StartupFee" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStartupFee %>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtStartupfee" Enabled="false" ToolTip='<%$ Resources:ZnodeAdminResource, ToolTipStartupFee %>' runat="server"
                            MaxLength="7"></asp:TextBox>
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="BillEvery" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBillEvery %>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtBillevery" Enabled="false" runat="server" MaxLength="7"></asp:TextBox>
                        <asp:DropDownList ID="ddlBillEvery" Enabled="false" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnTitleBillEvery %>' runat="server">
                            <asp:ListItem Value="Week" Text='<%$ Resources:ZnodeAdminResource, DropDownTextWeek %>'></asp:ListItem>
                            <asp:ListItem Value="Month" Text='<%$ Resources:ZnodeAdminResource, DropDownTextMonth %>' Selected="True"></asp:ListItem>
                            <asp:ListItem Value="Year" Text='<%$ Resources:ZnodeAdminResource, DropDownTextYear %>'></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="ContinueBilling" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBilling %>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtContinueBill" Enabled="false" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnTitleBilling %>' runat="server"
                            MaxLength="7"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="2" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div runat="server" id="dvBottomButtons">
            <zn:Button runat="server" ID="btnBottomSubmit" ButtonType="EditButton" OnClick="BtnNext_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonApproval %>'/>
            <zn:Button runat="server" ID="btnBottomCancel" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>'
                CausesValidation="false" />
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function setAffiliateSectionEnabled() {
            var txtAffiliateLink = document.getElementById('<%=txtAffiliateLink.ClientID %>');
            var rfvAffliateUrl = document.getElementById("<%=rfvAffliateUrl.ClientID %>");
            var regexValidator = document.getElementById("<%=regexValidator.ClientID %>");


        }

        function setTangibleSectionEnabled() {

            var chkShipInd = document.getElementById('<%=chkShipInd.ClientID %>');
            var txtProductWeight = document.getElementById('<%=txtProductWeight.ClientID %>');
            var txtHandling = document.getElementById('<%=txtHandling.ClientID %>');
            txtProductWeight.disabled = !chkShipInd.checked;
            txtHandling.disabled = !chkShipInd.checked;
            if (chkShipInd.checked) txtProductWeight.focus();

        }

        function setRecurringSectionEnabled() {

            var chkRecurringInd = document.getElementById('<%=chkRecurringInd.ClientID %>');
            var txtStartupfee = document.getElementById('<%=txtStartupfee.ClientID %>');
            var txtBillevery = document.getElementById('<%=txtBillevery.ClientID %>');
            var ddlBillEvery = document.getElementById('<%=ddlBillEvery.ClientID %>');
            var txtContinueBill = document.getElementById('<%=txtContinueBill.ClientID %>');

            txtStartupfee.disabled = !chkRecurringInd.checked;
            txtBillevery.disabled = !chkRecurringInd.checked;
            ddlBillEvery.disabled = !chkRecurringInd.checked;
            txtContinueBill.disabled = !chkRecurringInd.checked;
            if (chkRecurringInd.checked) txtStartupfee.focus();

        }

    </script>
</asp:Content>
