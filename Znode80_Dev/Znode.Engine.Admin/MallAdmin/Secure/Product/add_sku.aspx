<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="Znode.Engine.MallAdmin.Admin_Secure_catalog_product_add_sku" ValidateRequest="false"
    CodeBehind="add_sku.aspx.cs" %>

<%@ Register TagPrefix="znode" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/MallAdmin/Controls/Default/ImageUploader.ascx" TagName="UploadImage"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <asp:HiddenField ID="existSKUvalue" runat="server" />
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblHeading" runat="server" />
            </h1>
        </div>
        <div style="text-align: right">  
            <zn:Button runat="server"  ID="btnSubmitTop" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
            <zn:Button runat="server" ID="btnCancelTop"  ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="false" />
       </div>
        <div class="ClearBoth" align="left">
            <asp:Panel ID="pnlSKULocale" runat="server">
                <div class="SubTitle">
                     <asp:Localize ID="ColumnAssociatedLocales" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAssociatedLocales %>'></asp:Localize> &nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList runat="server" ID="ddlLocales"
                        AutoPostBack="true" OnSelectedIndexChanged="DdlLocales_SelectedIndexChanged">
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, ColumnSelectLanguage %>'></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </asp:Panel>
        </div>
        <br />
        <div>
            <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
        </div>
        <div class="FieldStyle">
             <asp:Localize ID="ColumnSKUorPart" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSKUorPart %>'></asp:Localize><span class="Asterix">*</span></div>
        <div>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="SKU" runat="server" Columns="30" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="SKU"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSKU %>' SetFocusOnError="True" CssClass="Error"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
             <asp:Localize ID="ColumnQuantityOnHand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnQuantityOnHand %>'></asp:Localize><span class="Asterix">*</span><br />
            <small> <asp:Localize ID="textInventoryLevel" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextskuInventoryLevel %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="Quantity" runat="server" Columns="5" MaxLength="4">1</asp:TextBox>&nbsp;
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="Quantity"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSkuQuantity %>' CssClass="Error"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="Quantity"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeBillingCycle %>' MaximumValue="9999"
                MinimumValue="0" Type="Integer" CssClass="Error"></asp:RangeValidator>
        </div>
        <asp:Panel ID="pnlProductAttributes" runat="server">
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnRetailPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRetailPrice %>'></asp:Localize>
                <br />
                <small> <asp:Localize ID="SKURetailPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextSKURetailPrice %>'></asp:Localize></small></div>
            <div class="ValueStyle">
                <asp:TextBox ID="RetailPrice" runat="server" Columns="10" MaxLength="7"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="RetailPrice"
                Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeSKURetailPrice %>'
                CssClass="Error" Display="Dynamic" />
                <div style="margin-left: 95px;">
                <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="RetailPrice"
							ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareSKURetailPrice %>'
							MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
						</div>
            </div>
            <div class="FieldStyle">
               <asp:Localize ID="EnableInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableInventory %>'></asp:Localize></div>
            <div class="ValueStyle">
                <asp:CheckBox ID='VisibleInd' runat='server' Text= '<%$ Resources:ZnodeAdminResource, CheckBoxinventory %>'
                    Checked="true"></asp:CheckBox></div>
            <div class="FieldStyle" id="DivAttributes" runat="Server">
                 <asp:Localize ID="ProductAttributes" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductAttributes %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:PlaceHolder ID="ControlPlaceHolder" runat="server"></asp:PlaceHolder>
            </div>
            <asp:Panel runat="server" Visible="false" ID="pnlSupplier">
                <div class="FieldStyle">
                     <asp:Localize ID="Supplier" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSupplier %>'></asp:Localize>
                    <br />
                    <small> <asp:Localize ID="SelectWhereUgetproduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextSelectWhereUgetproduct %>'></asp:Localize></small></div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlSupplier" runat="server" />
                </div>
            </asp:Panel>
            <div class="ClearBoth">
            </div>
            <h4 class="SubTitle">
                 <asp:Localize ID="SubTitleSKUImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSKUImage %>'></asp:Localize></h4>
            <small> <asp:Localize ID="SubTextSKUImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextSKUImage %>'></asp:Localize></small>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server">
                </ZNode:Spacer>
            </div>
            <div id="tblShowImage" visible="false" runat="server">
                <div>
                    <asp:Image ID="SKUImage" runat="server" />
                </div>
                <div>
                    <ZNode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server">
                    </ZNode:Spacer>
                </div>
                <div>
                    <div class="FieldStyle">
                        Select an Option<asp:Localize ID="Localize13" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextDeleteProductConfirmation %>'></asp:Localize></div>
                    <div class="ValueStyle">
                        <asp:RadioButton ID="RadioSKUCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage %>' runat="server"
                            GroupName="SKU Image" AutoPostBack="True" OnCheckedChanged="RadioSKUCurrentImage_CheckedChanged"
                            Checked="True" />
                        <asp:RadioButton ID="RadioSKUNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage %>' runat="server" GroupName="SKU Image"
                            AutoPostBack="True" OnCheckedChanged="RadioSKUNewImage_CheckedChanged" />
                        <asp:RadioButton ID="RadioSKUNoImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNoImage %>' runat="server" GroupName="SKU Image"
                            AutoPostBack="True" OnCheckedChanged="RadioSKUNoImage_CheckedChanged" />
                    </div>
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <div id="tblSKUDescription" runat="server" visible="false">
                <div>
                    <div>
                        <div>
                            <asp:Label ID="lblSKUImageError" runat="server" CssClass="Error" ForeColor="Red"
                                Text="" Visible="False"></asp:Label>
                        </div>
                        <div class="FieldStyle">
                             <asp:Localize ID="ColumnSelectImageColon" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectImageColon %>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <ZNode:UploadImage ID="UploadSKUImage" runat="server"></ZNode:UploadImage>
                        </div>
                    </div>
                </div>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnProductImageALTText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductImageALTText %>'></asp:Localize>
                <br />
                <small> <asp:Localize ID="TextShortDescriptive" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextShortDescriptive %>'></asp:Localize></small></div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtImageAltTag" runat="server"></asp:TextBox></div>
            <asp:Panel ID="pnlRecurringBilling" runat="server" Visible="false">
                <h4>
                     <asp:Localize ID="ColumnRecurringBilling" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRecurringBilling %>'></asp:Localize></h4>
                <div class="FieldStyle">
                     <asp:Localize ID="Localize19" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBillingPeriod %>'></asp:Localize><span class="Asterix">*</span>
                     <small> <asp:Localize ID="ColumnTextRecurringBilling" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextRecurringBilling %>'></asp:Localize></small></div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlBillingPeriods" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DdlBillingPeriods_SelectedIndexChanged">
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, TextDays %>' Value="DAY"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextWeekly %>' Value="WEEK"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, ColumnBillingPeriodMonth %>' Value="MONTH"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, ColumnBillingPeriodYear %>' Value="YEAR"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="FieldStyle">
                     <asp:Localize ID="ColumnBillingFrequency" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBillingFrequency %>'></asp:Localize><span class="Asterix">*</span> 
                    <small> <asp:Localize ID="ColumnTextBillingFrequency" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextBillingFrequency %>'></asp:Localize></small></div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlBillingFrequency" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="FieldStyle">
                     <asp:Localize ID="ColumnBillingCycles" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBillingCycles %>'></asp:Localize><span class="Asterix">*</span>
                     <small> <asp:Localize ID="ColumnTextBillingCycles" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextBillingCycles %>'></asp:Localize></small></div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSubscrptionCycles" runat="server">1</asp:TextBox></div>
            </asp:Panel>
        </asp:Panel>
        <div class="ClearBoth">
        </div>
        <div>  
             <zn:Button runat="server"  ID="btnSubmit" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
            <zn:Button runat="server" ID="btnCancel"  ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="false" />
 
        </div>
    </div>
    <!-- hided this section for MallAdmin for now. -->
    <div id="Div1" runat="server" visible="false">
        <div class="FieldStyle">
             <asp:Localize ID="Localize24" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleReOrderLevel %>'></asp:Localize></div>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="ReOrder"
            Display="Dynamic" ErrorMessage= '<%$ Resources:ZnodeAdminResource, RequiredSKUReOrderLevel %>'
            SetFocusOnError="true" ValidationExpression="(^N/A$)|(^[-]?(\d+)(\.\d{0,3})?$)|(^[-]?(\d{1,3},(\d{3},)*\d{3}(\.\d{1,3})?|\d{1,3}(\.\d{1,3})?)$)"
            CssClass="Error"></asp:RegularExpressionValidator>
        <div class="ValueStyle">
            <asp:TextBox ID="ReOrder" runat="server" Columns="5" MaxLength="3"></asp:TextBox>
        </div>
        <div class="FieldStyle">
             <asp:Localize ID="Column2COProductID" runat="server" Text='<%$ Resources:ZnodeAdminResource, Column2COProductID %>'></asp:Localize><br />
            <small> <asp:Localize ID="ColumnText2COProductID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnText2COProductID %>'></asp:Localize></small></div>
        <div class="ValueStyle">
            <asp:TextBox ID="TwoCOProductID" Columns="50" runat="server"></asp:TextBox></div>
        <div class="FieldStyle">
             <asp:Localize ID="ColumnAdditionalWeight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAdditionalWeight %>'></asp:Localize></div>
        <asp:CompareValidator ID="CompareValidator4" SetFocusOnError="true" runat="server"
            ControlToValidate="WeightAdditional" Type="Double" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeAdditionalWeight %>'
            CssClass="Error" Display="Dynamic" />
        <div class="ValueStyle">
            <asp:TextBox ID="WeightAdditional" runat="server" Columns="10" MaxLength="7"></asp:TextBox></div>
        <div class="FieldStyle">
             <asp:Localize ID="ColumnSalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSalePrice %>'></asp:Localize>
            <br />
            <small> <asp:Localize ID="ColumnTextSKUSalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextSKUSalePrice %>'></asp:Localize></small></div>
        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="SalePrice"
            Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareSKUSalePrice %>'
            CssClass="Error" Display="Dynamic" />
        <div class="ValueStyle">
            <asp:TextBox ID="SalePrice" runat="server" Columns="10" MaxLength="7"></asp:TextBox></div>
        <div class="FieldStyle">
             <asp:Localize ID="ColumnTitleWholesalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleWholesalePrice %>'></asp:Localize>
            <br />
            <small> <asp:Localize ID="ColumnTextSKUWholeSalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextSKUWholeSalePrice %>'></asp:Localize></small>
        </div>
        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="WholeSalePrice"
            Type="Currency" Operator="DataTypeCheck" ErrorMessage= '<%$ Resources:ZnodeAdminResource, CompareSKUWholeSalePrice %>'
            CssClass="Error" Display="Dynamic" />
        <div class="ValueStyle">
            <asp:TextBox ID="WholeSalePrice" runat="server" Columns="10" MaxLength="7"></asp:TextBox></div>
    </div>
</asp:Content>
