<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/content.master" AutoEventWireup="true" ValidateRequest="false" Inherits="Znode.Engine.MallAdmin.Admin_Secure_catalog_product_add_view" Codebehind="add_view.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/MallAdmin/Controls/Default/ImageUploader.ascx" TagName="UploadImage"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblHeading" runat="server" />
            </h1>
        </div>
        <div style="text-align: right"> 
            <zn:Button runat="server"  ID="btnSubmitTop" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
            <zn:Button runat="server" ID="btnCancelTop"  ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="false" />
         </div>
        <div class="ClearBoth">
            <p>  
            <asp:Localize ID="TextThumbNil" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextThumbNil %>'></asp:Localize> 
            </p>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <asp:Label  ID="lblError"  CssClass="Error" runat="server"></asp:Label>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="FormView">
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnTitleTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTitle %>'></asp:Localize><br />
                <small> 
                <asp:Localize ID="HintSubTextTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintSubTextTitle %>'></asp:Localize>

                </small></div>
            <div class="ValueStyle">
                <asp:TextBox ID="txttitle" runat="server" Columns="30" MaxLength="30"></asp:TextBox></div>
                
            <div id="divImageType" runat="server">
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnTitleImageType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleImageType %>'></asp:Localize><br />
                    <small>  <asp:Localize ID="TextAdminAccess" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAdminAccess %>'></asp:Localize> 
                    </small></div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlImageType" runat="server" 
                        AutoPostBack="false">
                    </asp:DropDownList>
                </div>
            </div>
            
            <div class="ClearBoth" style="width: 100%">
                <span style="font-size: 10.5pt; font-weight: bold; color: #404040; font-family: Calibri, arial;">
                    <asp:Label ID="lblImage" runat="server"></asp:Label></span><span class="Asterix">*</span><br />
                <div id="ProductHint" runat="server" visible="false" style="width: 100%">
                    <small>  
            <asp:Localize ID="ColumnTextProductImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductImage %>'></asp:Localize> 
                    </small></div>
                <div id="SwatchHint" runat="server" visible="false" style="width: 100%">
                    <small>  
                    <asp:Localize ID="HintSubtextUploadSuitableSwatchImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintSubtextUploadSuitableSwatchImage %>'></asp:Localize>

                    </small></div>
            </div>
            <div class="ClearBoth">
            </div>
            <div id="tblShowImage" runat="server" visible="true" style="margin: 10px;">
                <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;">
                    <asp:Image ID="Image1" runat="server" />
                </div>
                <div class="LeftFloat" style="padding-left: 10px; margin-left: -1px;">
                    <asp:Panel runat="server" ID="pnlShowOption">
                        <div>
                             <asp:Localize ID="TTextOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextOption %>'></asp:Localize></div>
                        <div>
                            <asp:RadioButton ID="RadioProductCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage %>' runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioProductNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage %>' runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductNewImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblProductDescription" runat="server" visible="false">
                        <div class="FieldStyle">
                             <asp:Localize ID="ColumnSelectImageColon" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectImageColon %>'></asp:Localize></div>
                        <div class="ValueStyle">
                            <ZNode:UploadImage ID="UploadProductImage" runat="server"></ZNode:UploadImage>
                        </div>
                    </div>
                    <div class="FieldStyle">
                         <asp:Localize ID="ColumnProductImageALTText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductImageALTText %>'></asp:Localize><br />
                        <small> 
                    <asp:Localize ID="TextShortDescriptive" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextShortDescriptive %>'></asp:Localize>

                        </small></div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtImageAltTag" runat="server"></asp:TextBox></div>
                    <div>
                        <asp:Label ID="lblProductImageError" runat="server" CssClass="Error" ForeColor="Red"
                            Text="" Visible="False"></asp:Label>
                    </div>
                    <div id="AlternateThumbnail" runat="server" visible="false">
                       <%-- <div class="FieldStyle">
                            Product Image File Name</div>
                        <div class="ValueStyle">--%>
                            <asp:TextBox ID="txtAlternateThumbnail" Visible="false" runat="server" Columns="30" MaxLength="30"></asp:TextBox>
                    <%--</div>--%>
                    </div>
                    <asp:Panel ID="ImagefileName" runat="server" Visible="false">
                        <div class="FieldStyle">
                            <asp:Label ID="lblImageName" runat="server"></asp:Label></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtimagename" runat="server"></asp:TextBox></div>
                    </asp:Panel>
                </div>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <asp:Panel ID="ProductSwatchPanel" runat="server" Visible="false">
                <div class="FieldStyle">
                    <asp:Label ID="lblProductSwatchImage" runat="server" Text="Product Image"></asp:Label></div>
                <div class="ClearBoth">
                </div>
                <div>
                    <small> <asp:Localize ID="HintTextUploadProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextUploadProduct %>'></asp:Localize></small></div>
                <div class="ClearBoth">
                </div>
                <div id="tblShowProductSwatchImage" runat="server" visible="false" style="margin: 10px;">
                    <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;">
                        <asp:Image ID="ProductSwatchImage" runat="server" />
                    </div>
                    <div class="LeftFloat" style="padding-left: 10px; margin-left: -1px;">
                        <asp:Panel runat="server" ID="pnlShowSwatchOption">
                            <div>
                                 <asp:Localize ID="TextOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextOption %>'></asp:Localize></div>
                            <div>
                                <asp:RadioButton ID="RbtnProductSwatchCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage %>' runat="server"
                                    GroupName="Product Swatch Image" AutoPostBack="True" OnCheckedChanged="RbtnProductSwatchCurrentImage_CheckedChanged"
                                    Checked="True" /><br />
                                <asp:RadioButton ID="RbtnProductSwatchNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioTextUploadNewImage %>' runat="server"
                                    GroupName="Product Swatch Image" AutoPostBack="True" OnCheckedChanged="RbtnProductSwatchNewImage_CheckedChanged" /><br />
                                <asp:RadioButton ID="RbtnProductSwatchNoImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNoImage %>' runat="server" GroupName="Product Swatch Image"
                                    AutoPostBack="True" OnCheckedChanged="RbtnProductSwatchNoImage_CheckedChanged" />
                            </div>
                        </asp:Panel>
                        <br />
                        <div id="tblProductSwatchDescription" runat="server" visible="false">
                            <div class="FieldStyle">
                                 <asp:Localize ID="TextColumnSelectImageColon" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectImageColon %>'></asp:Localize></div>
                            <div class="ValueStyle">
                                <ZNode:UploadImage ID="UploadProductSwatchImage" runat="server"></ZNode:UploadImage>
                            </div>
                        </div>
                        <div class="ClearBoth">
                            <asp:Label ID="Label1" runat="server" CssClass="Error" ForeColor="Red" Text="" Visible="False"></asp:Label>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="ClearBoth">
                <br />
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnTitleDisplayonProductPage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayonProductPage %>'></asp:Localize></div>
            <div class="ValueStyle">
                <asp:CheckBox ID='VisibleInd' runat='server' Checked="true" Text='<%$ Resources:ZnodeAdminResource, CheckBoxShowThumbnail %>'>
                </asp:CheckBox></div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnTitleDisplayonCategoryPage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayonCategoryPage %>'></asp:Localize></div>
            <div class="ValueStyle">
                <asp:CheckBox ID='VisibleCategoryInd' runat='server' Text="Check this box to display a thumbnail of this image on the Category Page" /></div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnTitleDisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>'></asp:Localize><span class="Asterix">*</span><br />
                <small> 
                    <asp:Localize ID="HintTextEnteranumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextDisplayWeighting %>'></asp:Localize>

                </small></div>
            <div class="ValueStyle">
                <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="5">500</asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="DisplayOrder"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrderwithStar %>'></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="DisplayOrder"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                 
            </div>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div> 
             <zn:Button runat="server"  ID="btnSubmitBottom" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
            <zn:Button runat="server" ID="btnCancelBottom"  ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="false" />
       </div>
    </div>
</asp:Content>
