﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_Product_EditMarketing class.
    /// </summary>
    public partial class Admin_Secure_Product_EditMarketing : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId = 0;
        private string managePageLink = "~/MallAdmin/Secure/product/view.aspx?itemid=";
        private string cancelPageLink = "~/MallAdmin/Secure/product/view.aspx?itemid=";
        private string associateName = string.Empty;
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                if (this.itemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextEditMarketingInfo").ToString();

                    // Bind Sku Details
                    this.Bind();
                }
            }
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Submit Button Click Event - Fires when Submit button is triggered
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            this.SaveMarketing();
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>        
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.cancelPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())) + "&mode=" + HttpUtility.UrlEncode(this.encryption.EncryptData("marketing")));
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Save or Update the marketing data.
        /// </summary>
        private void SaveMarketing()
        {
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = new Product();
            string mappedSEOUrl = string.Empty;

            // If edit mode then get all the values first
            if (this.itemId > 0)
            {
                product = productAdmin.GetByProductId(this.itemId);
                if (product.SEOURL != null)
                {
                    mappedSEOUrl = product.SEOURL;
                }
            }

            // Set properties
            product.NewProductInd = chkIsNewItem.Checked;
            product.SEOTitle = Server.HtmlEncode(txtSEOTitle.Text.Trim());
            product.SEOKeywords = Server.HtmlEncode(txtSEOMetaKeywords.Text.Trim());
            product.SEODescription = Server.HtmlEncode(txtSEOMetaDescription.Text.Trim());
            product.SEOURL = null;

            if (txtSEOUrl.Text.Trim().Length > 0)
            {
                product.SEOURL = txtSEOUrl.Text.Trim().Replace(" ", "-");
            }

            if (string.Compare(product.SEOURL, mappedSEOUrl, true) != 0)
            {
                if (urlRedirectAdmin.SeoUrlExists(product.SEOURL, product.ProductID))
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSEOFriendlyURLAlreadyExist").ToString();
                    return;
                }
            }

            bool isSuccess = false;

            if (this.itemId > 0)
            {
                isSuccess = productAdmin.Update(product);
            }

            if (isSuccess)
            {
                urlRedirectAdmin.UpdateUrlRedirect(mappedSEOUrl);

                product = productAdmin.GetByProductId(this.itemId);

                this.associateName = "Edit Marketing Info for  - " + product.Name;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, product.Name);

                product.ReviewStateID = productAdmin.UpdateReviewStateID(UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId, "Marketing");
                productAdmin.Update(product);

                Response.Redirect(this.managePageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())) + "&mode=" + HttpUtility.UrlEncode(this.encryption.EncryptData("marketing")));
            }
            else
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductSEOSetting").ToString();
            }
        }
        #endregion

        #region Bind Methods
        private void Bind()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin productAdmin = new ProductAdmin();
            Product product = productAdmin.GetByProductId(this.itemId);

            if (product != null)
            {
                // Set Display Options value
                chkIsNewItem.Checked = product.NewProductInd == null ? false : Convert.ToBoolean(product.NewProductInd.Value);

                // Set SEO Options value
                txtSEOTitle.Text = Server.HtmlDecode(product.SEOTitle);
                txtSEOMetaKeywords.Text = Server.HtmlDecode(product.SEOKeywords);
                txtSEOMetaDescription.Text = Server.HtmlDecode(product.SEODescription);
                txtSEOUrl.Text = product.SEOURL;
                lblTitle.Text += "\"" + product.Name + "\"";
            }
        }
        #endregion
    }
}