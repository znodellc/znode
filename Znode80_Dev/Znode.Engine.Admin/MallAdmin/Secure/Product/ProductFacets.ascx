﻿<%@ Control Language="C#" AutoEventWireup="true"
	Inherits="Znode.Engine.MallAdmin.Admin_Secure_catalog_product_ProductFacets" CodeBehind="ProductFacets.ascx.cs" %>
<%@ Register TagPrefix="znode" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<asp:Panel ID="pnlCategories" runat="server">
</asp:Panel>
<!-- Product List -->
<asp:UpdatePanel runat="server" ID="upProductFacets">
	<ContentTemplate>
		<asp:Panel ID="pnlFacetsList" runat="server">
			<h4 class="GridTitle"> <asp:Localize ID="TitleFacets" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleFacets %>'></asp:Localize></h4>
			 <div>
                <div class="TabDescription" style="width: 80%;">
                     <p>
                       <asp:Localize ID="Facets" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextFacet %>'></asp:Localize>
                    </p>
                   <asp:Label runat="server" ID="lblEmptyGrid"> 
                        <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextNotAssociatedFacet %>'></asp:Localize> <%=ItemType.Replace("Id","") %></asp:Label>
                </div>
              <div class="ButtonStyle" style="clear: right; float: right; padding-top: 10px; padding-right: 10px;"> 
                    <zn:LinkButton ID="LinkButton1" runat="server"
                        ButtonType="Button" OnClick="Add_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, TitleAssociateFacet %>'
                        ButtonPriority="Primary" />
                </div>
            </div>
			<znode:Spacer ID="Spacer5" runat="server" SpacerHeight="10" />
			<div style="clear: both;">
			<asp:GridView ID="uxGrid" runat="server" AllowPaging="false" AutoGenerateColumns="False"
				CaptionAlign="Left" CellPadding="4" CssClass="Grid" GridLines="None" Width="100%"
				OnRowCommand="UxGrid_RowCommand" OnDataBound="UxGrid_DataBound" OnRowDataBound="UxGrid_RowDataBound" 
				 OnRowDeleting="UxGrid_RowDeleting">
				<Columns>
					<asp:BoundField DataField="FacetGroupLabel" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnFacetGroupName %>' HeaderStyle-HorizontalAlign="Left" />
					<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleFacetNames %>' ItemStyle-Width="60%">
						<ItemTemplate>
							<asp:DataList runat="server" ID="dlFacetNames" DataSource='<%# GetFacets(Eval("FacetGroupId")) %>'
								RepeatDirection="Horizontal" RepeatLayout="Flow">
								<ItemTemplate>
									<asp:Label runat="server" ID="lblFacetNames" Text='<%# Eval("FacetName") %>'></asp:Label>,
								</ItemTemplate>
							</asp:DataList>
						</ItemTemplate>
					</asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="10%">
						<ItemTemplate>
							<asp:LinkButton ID="EditFacet" CommandName="FacetEdit" CommandArgument='<%# Eval("FacetGroupID") %>'
								runat="server" CssClass="Button" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField ItemStyle-Width="10%">
						<ItemTemplate>
							<asp:LinkButton ID="DeleteFacet" CommandName="FacetDelete" CommandArgument='<%# Eval("FacetGroupID") %>'
								runat="server" CssClass="Button" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>'  />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
				<RowStyle CssClass="RowStyle" />
				<EditRowStyle CssClass="EditRowStyle" />
				<HeaderStyle CssClass="HeaderStyle" />
				<AlternatingRowStyle CssClass="AlternatingRowStyle" />
				<EmptyDataTemplate>
					No facets associated with this 
                    <%=ItemType.Replace("Id","") %>
				</EmptyDataTemplate>
			</asp:GridView>
			
				 </div>
		</asp:Panel>
		<div>
			<znode:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
		</div>
		<div>
			<znode:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
		</div>
		<asp:Panel runat="server" ID="pnlEditFacets" Visible="false" CssClass="FormView">
			<h4 class="SubTitle">
				<asp:Label runat="server" ID="lblAddEdit"></asp:Label>
                 <asp:Localize ID="SelectFacetstoAssociate" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSelectFacetstoAssociate %>'></asp:Localize></h4>

			<div>
				<asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
					ForeColor="red"></asp:Label>
			</div>
			<znode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
			<div class="FieldStyle">
				 <asp:Localize ID="ColumnTitleFacetGroups" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFacetGroups %>'></asp:Localize><br />
				<small><asp:Localize ID="ColumnTextFacetGroups" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextFacetGroups %>'></asp:Localize></small>
			</div>
			<div class="ValueStyle">
				<asp:DropDownList ID="ddlFacetGroups" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlFacetGroups_SelectedIndexChanged">
				</asp:DropDownList>
			</div>
			<div class="FieldStyle">
				<asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFacets %>'></asp:Localize><br />
			</div>
			<div class="ValueStyle">
				<asp:CheckBoxList ID="chklstFacets" RepeatColumns="5" RepeatLayout="Table" RepeatDirection="Horizontal"
					runat="server">
				</asp:CheckBoxList>
				<asp:Label runat="server" ID="lblNoFacets" Visible="false" Text='<%$ Resources:ZnodeAdminResource, RecordNotFoundFacet %>'></asp:Label>
			</div>
			<znode:Spacer ID="Spacer1" runat="server" SpacerHeight="10" />
			<div class="ClearBoth">
				<br />
			</div>
			<div>
				
                <zn:Button runat="server" ID="btnSubmitBottom" OnClick="Update_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>'/>
            <zn:Button runat="server" ID="btnCancelBottom" OnClick="Cancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>'/>


			</div>
			<znode:Spacer ID="Spacer4" runat="server" SpacerHeight="10" />
		</asp:Panel>
	</ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="uxFacetUpdateProgress" runat="server" AssociatedUpdatePanelID="upProductFacets" DisplayAfter="10">
	<ProgressTemplate>
		<div id="ajaxProgressBg"></div>
		<div id="ajaxProgress"></div>
	</ProgressTemplate>
</asp:UpdateProgress>
