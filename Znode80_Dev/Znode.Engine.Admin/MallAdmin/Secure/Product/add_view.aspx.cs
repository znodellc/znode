using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_catalog_product_add_view class.
    /// </summary>
    public partial class Admin_Secure_catalog_product_add_view : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId = 0;
        private int productImageId = 0;
        private int productTypeId = 0;
        private string associateName = string.Empty;
        private string addPageLink = "~/MallAdmin/Secure/product/add.aspx?itemid=";
        private string viewPageLink = "~/MallAdmin/Secure/product/view.aspx?itemid=";
        private ZNode.Libraries.Admin.ProductViewAdmin imageAdmin = new ProductViewAdmin();
        private ZNode.Libraries.DataAccess.Entities.ProductImage productImage = new ProductImage();
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region Page load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Product ID
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
            }
            else if (Session["itemid"] != null)
            {
                this.itemId = int.Parse(Session["itemid"].ToString());
            }

            // Product Image ID
            if (Request.Params["productimageid"] != null)
            {
                this.productImageId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["productimageid"].ToString()));
            }

            // Ptoduct Image Type Id(1- Alternate Image View, 2- Swatch
            if (Request.Params["typeid"] != null)
            {
                this.productTypeId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["typeid"].ToString()));                
            }

            if (!Page.IsPostBack)
            {
                divImageType.Visible = false;

                this.BindImageType();

                if ((this.itemId > 0) && (this.productImageId > 0))
                {
                    if (ddlImageType.SelectedValue == "2")
                    {
                        lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditSwatchImage").ToString();
                    }
                    else
                    {
                        lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditProductImage").ToString();
                    }

                    this.BindData();

                    tblShowImage.Visible = true;
                    tblShowProductSwatchImage.Visible = true;
                    txtimagename.Visible = true;                    
                }
                else
                {
                    // Show the image upload control, when adding new product.
                    tblShowImage.Visible = true;
                    tblProductDescription.Visible = true;
                    pnlShowOption.Visible = false;
                    Image1.Visible = false;
                    if (ddlImageType.SelectedValue == "2")
                    {
                        lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddSwatchImage").ToString();
                    }
                    else
                    {
                        lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddProductImage").ToString();
                    }
                }

                ddlImageType.SelectedIndex = ddlImageType.Items.IndexOf(ddlImageType.Items.FindByValue(this.productTypeId.ToString()));

                this.SetVisibleData();
            }
        }
        #endregion       

        #region General events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = new Product();

            bool isSuccess = false;
            product = productAdmin.GetByProductId(this.itemId);

            // Add up to 10 additional images of product such as front and back views or closeups that show extra detail.
            DataSet ds = this.imageAdmin.GetAllAlternateImage(this.itemId);

            // Filter based on image types
            string filterExpression = "ProductImageTypeID=" + Convert.ToString(this.productTypeId);
            ds.Tables[0].DefaultView.RowFilter = filterExpression;

            System.IO.FileInfo fileInfo = null;
            System.IO.FileInfo swatchfileInfo = null;
            bool swatchSelected = false;
            bool newProductImage = false;

            if (this.productImageId > 0)
            {
                this.productImage = this.imageAdmin.GetByProductImageID(this.productImageId);
            }
            else
            {
                // Check image validation only for new image
                if (ds.Tables[0].DefaultView.Count >= 5)
                {
                    lblError.Text =  this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorOnlyfivealternateImagesAllowed").ToString();
                    return;
                }
            }

            if (this.productImage.ProductImageTypeID.GetValueOrDefault(0) != Convert.ToInt32(ddlImageType.SelectedValue))
            {
                // If swatch is selected
                swatchSelected = true && Convert.ToInt32(ddlImageType.SelectedValue) == 2;
            }

            this.productImage.Name = Server.HtmlEncode(txttitle.Text);
            this.productImage.ActiveInd = VisibleInd.Checked;
            this.productImage.ShowOnCategoryPage = VisibleCategoryInd.Checked;
            this.productImage.ProductID = this.itemId;
            this.productImage.ProductImageTypeID = Convert.ToInt32(ddlImageType.SelectedValue);

            this.productImage.DisplayOrder = int.Parse(DisplayOrder.Text.Trim());
            this.productImage.ImageAltTag = txtImageAltTag.Text.Trim();
            this.productImage.AlternateThumbnailImageFile = txtAlternateThumbnail.Text.Trim();
            this.productImage.ReviewStateID = productAdmin.UpdateReviewStateID(product, UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId);
            UploadProductImage.AppendToFileName = "-" + this.itemId.ToString();
            UploadProductSwatchImage.AppendToFileName = "-" + this.itemId.ToString();
            
            if ((this.productImageId == 0) || (RadioProductNewImage.Checked == true))
            {
                if (this.GetCurrentProductImageType() == ZNodeProductImageType.Swatch)
                {
                    if (!UploadProductSwatchImage.IsFileNameValid())
                    {
                        return;
                    }
                    
                    // Check for Product View Image
                    fileInfo = UploadProductSwatchImage.FileInformation;
                }
                else
                {
                    if (!UploadProductImage.IsFileNameValid())
                    {
                        return;
                    }

                    // Check for Product View Image
                    fileInfo = UploadProductImage.FileInformation;
                }

                if (fileInfo != null)
                {
                    this.productImage.ImageFile = "Mall/" + product.AccountID.ToString() + "/" + fileInfo.Name;
                }
            }
            else
            {
                this.productImage.ImageFile = this.productImage.ImageFile;
            }

            if (this.GetCurrentProductImageType() == ZNodeProductImageType.Swatch)
            {
                if (!RbtnProductSwatchNoImage.Checked)
                {
                    // Validate Product Swatch image
                    if ((this.productImageId == 0) || this.productImage.AlternateThumbnailImageFile.Length == 0 || RbtnProductSwatchNewImage.Checked)
                    {
                        UploadProductSwatchImage.AppendToFileName = DateTime.Now.ToString("ddMMyyyyhhmmss");
                        if (!UploadProductSwatchImage.IsFileNameValid())
                        {
                            return;
                        }

                        if (UploadProductSwatchImage.PostedFile.FileName.Trim().Length != 0)
                        {
                            swatchfileInfo = UploadProductSwatchImage.FileInformation;

                            if (swatchfileInfo != null)
                            {
                                this.productImage.AlternateThumbnailImageFile = "Mall/" + product.AccountID.ToString() + "/" + swatchfileInfo.Name;
                                newProductImage = true;
                            }

                            RbtnProductSwatchNewImage.Checked = true;
                        }
                    }
                    else
                    {
                        this.productImage.AlternateThumbnailImageFile = this.productImage.AlternateThumbnailImageFile;
                    }
                }
                else
                {
                    this.productImage.AlternateThumbnailImageFile = string.Empty;
                    this.productImage.ImageFile = string.Empty;
                    this.imageAdmin.Update(this.productImage);
                }
            }

            // Upload File if this is a new product or the New Image option was selected for an existing product
            if (RadioProductNewImage.Checked || this.productImageId == 0)
            {
                if (fileInfo != null)
                {
                    if (this.GetCurrentProductImageType() == ZNodeProductImageType.AlternateImage)
                    {
                        UploadProductImage.SaveImage("madmin", product.AccountID.ToString());
                    }                    

                    if (this.productImage.ProductImageTypeID.GetValueOrDefault() == 2)
                    {
                        // Create fileInfo for Original product View Image
                        fileInfo = UploadProductSwatchImage.FileInformation;                        
                    }

                    // Release all resources used
                    UploadProductImage.Dispose();
                }
            }

            // Upload File if this is a new product or the New Image option was selected for an existing product
            if (RbtnProductSwatchNewImage.Checked || this.productImageId == 0)
            {
                if (swatchfileInfo != null)
                {
                    UploadProductSwatchImage.SaveImage("madmin", product.AccountID.ToString());

                    // Create fileInfo for Original product View Image
                    swatchfileInfo = UploadProductSwatchImage.FileInformation;
                    this.productImage.AlternateThumbnailImageFile = "Mall/" + product.AccountID.ToString() + "/" + swatchfileInfo.Name;
                    this.productImage.ImageFile = "Mall/" + product.AccountID.ToString() + "/" + swatchfileInfo.Name;

                    // Release all resources used
                    UploadProductSwatchImage.Dispose();
                }
            }

            if (swatchSelected && this.productImageId > 0 && !newProductImage)
            {
                this.productImage.AlternateThumbnailImageFile = this.productImage.ImageFile;

                // Create fileInfo for Original product View Image
                fileInfo = UploadProductImage.FileInformation;                
            }

           
            
            if (this.productImageId > 0)
            {
                // Update the Imageview
                this.associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAlternateImage").ToString() + this.productImage.ImageFile + " - " + product.Name;
                isSuccess = this.imageAdmin.Update(this.productImage);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, product.Name);
            }
            else
            {
                this.associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddAlternateImage").ToString() + this.productImage.ImageFile + " - " + product.Name;
                isSuccess = this.imageAdmin.Insert(this.productImage);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, product.Name);
            }

            if (isSuccess)
            {
                productAdmin.UpdateReviewHistory("Alternate Image",(ZNodeProductReviewState)product.ReviewStateID, this.itemId);
                productAdmin.Update(product);

                Response.Redirect(this.GetReturnPageUrl());
            }
            else
            {
                // Display error message
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteAdd").ToString();
            }
        }
       
        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.GetReturnPageUrl());
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = true;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RbtnProductSwatchCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductSwatchDescription.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RbtnProductSwatchNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductSwatchDescription.Visible = true;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RbtnProductSwatchNoImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductSwatchDescription.Visible = false;
            txtAlternateThumbnail.Text = string.Empty;
            txtimagename.Text = string.Empty;
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Get the return page Url using query string parameter.
        /// </summary>
        /// <returns>Returns thr redirect page Url</returns>
        private string GetReturnPageUrl()
        {
            string returnUrl = string.Empty;

            // Mode value represetnts the which tab to be highlighted in view page.
            if (Request.Params["pageid"] != null)
            {
                Session["stepid"] = 1;
                Session["itemid"] = this.itemId;

                // Request comes from Add Product Wizard Page.
                returnUrl = this.addPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString()));
            }
            else
            {
                // Set Product Wizard Step Index
                returnUrl = this.viewPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())) + "&mode=" + HttpUtility.UrlEncode(this.encryption.EncryptData("alternateimages"));
            }

            return returnUrl;
        }

        /// <summary>
        /// Get the current product image type using query string value. If query string null it will return alternate image type.
        /// </summary>
        /// <returns>Returns ZNodeProductImageType enum object</returns>
        private ZNodeProductImageType GetCurrentProductImageType()
        {
            // Alternate image is Default
            int productImageTypeId = 1;

            // Ptoduct Image Type Id(1- Alternate Image View, 2- Swatch
            if (Request.Params["typeid"] != null)
            {
                productImageTypeId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["typeid"].ToString()));
            }

            ZNodeProductImageType imageType = (ZNodeProductImageType)Enum.ToObject(typeof(ZNodeProductImageType), productImageTypeId);
            return imageType;
        }

        /// <summary>
        /// Binds ImageType Type drop-down list
        /// </summary>
        private void BindImageType()
        {
            ddlImageType.DataSource = this.imageAdmin.GetImageType();
            ddlImageType.DataTextField = "Name";
            ddlImageType.DataValueField = "ProductImageTypeID";
            ddlImageType.DataBind();

            this.SetVisibleData();
        }

        /// <summary>
        /// Set the controls visibility based on the product image type.
        /// </summary>
        private void SetVisibleData()
        {
            AlternateThumbnail.Visible = false;
            SwatchHint.Visible = false;
            ProductHint.Visible = false;
            ProductSwatchPanel.Visible = false;
            if (this.GetCurrentProductImageType() == ZNodeProductImageType.Swatch)
            {
                ddlImageType.SelectedValue = Convert.ToInt32(ZNodeProductImageType.Swatch).ToString();
            }
            else
            {
                ddlImageType.SelectedValue = Convert.ToInt32(ZNodeProductImageType.AlternateImage).ToString();
            }

            if (this.GetCurrentProductImageType() == ZNodeProductImageType.Swatch)
            {
                AlternateThumbnail.Visible = true;
                lblImage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnSwatchImage").ToString();
                lblImageName.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextSwatchImageFileName").ToString();
                ProductSwatchPanel.Visible = true;
                tblShowImage.Visible = false;
                ProductHint.Visible = false;
                lblProductSwatchImage.Visible = false;
                if (this.productImageId <= 0)
                {
                    pnlShowSwatchOption.Visible = false;
                    ProductSwatchImage.Visible = false;
                    tblShowProductSwatchImage.Visible = true;
                }

                tblProductSwatchDescription.Visible = true;

                if (this.productImageId > 0)
                {
                    tblProductSwatchDescription.Visible = false;
                }
            }
            else
            {
                AlternateThumbnail.Visible = false;
                lblImage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleProductImages").ToString();
                lblImageName.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextImageName").ToString();
                ProductHint.Visible = true;
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind Datas
        /// </summary>
        private void BindData()
        {
            this.productImage = this.imageAdmin.GetByProductImageID(this.productImageId);

            ZNodeImage znodeImage = new ZNodeImage();
            if (this.productImage != null)
            {
                txtimagename.Text = Server.HtmlDecode(this.productImage.ImageFile);
                txttitle.Text = Server.HtmlDecode(this.productImage.Name);
                VisibleInd.Checked = this.productImage.ActiveInd;
                VisibleCategoryInd.Checked = this.productImage.ShowOnCategoryPage;

                Image1.ImageUrl = znodeImage.GetImageHttpPathSmall(this.productImage.ImageFile);

                ddlImageType.SelectedValue = this.productImage.ProductImageTypeID.ToString();
                DisplayOrder.Text = this.productImage.DisplayOrder.GetValueOrDefault().ToString();
                txtImageAltTag.Text = this.productImage.ImageAltTag;
                txtAlternateThumbnail.Text = this.productImage.AlternateThumbnailImageFile;

                if (txtAlternateThumbnail.Text != string.Empty)
                {
                    ProductSwatchImage.ImageUrl = znodeImage.GetImageHttpPathSmall(this.productImage.AlternateThumbnailImageFile);
                    tblShowProductSwatchImage.Visible = true;
                }
                else
                {
                    RbtnProductSwatchNoImage.Checked = true;
                    RbtnProductSwatchCurrentImage.Visible = false;
                }
            }
        }

        #endregion
    }
}