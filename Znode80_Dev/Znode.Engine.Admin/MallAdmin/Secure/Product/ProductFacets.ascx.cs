﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_catalog_product_ProductFacets class.
    /// </summary>
    public partial class Admin_Secure_catalog_product_ProductFacets : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private int itemId = 0;
        private int skuId = 0;
        private string associateName = string.Empty;
        private ZNodeEncryption encryption = new ZNodeEncryption();
        private string _ItemType = "ProductId";

        /// <summary>
        /// Gets or sets the item type.
        /// </summary>
        public string ItemType
        {
            get { return this._ItemType; }
            set { this._ItemType = value; }
        }

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
            }

            // Get skuid from querystring        
            if (Request.Params["skuid"] != null)
            {
                this.skuId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["skuid"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                this.BindGrid();
            }
        }

        #endregion       

        #region Control Events

        protected void Update_Click(object sender, EventArgs e)
        {
			int flag = 0;
            StringBuilder sb = new StringBuilder();
            FacetProductSKUService service = new FacetProductSKUService();
            TList<FacetProductSKU> facetProductSkuList;
            if (this._ItemType == "SkuId")
            {
                facetProductSkuList = service.GetBySKUID(this.skuId);
            }
            else
            {
                facetProductSkuList = service.GetByProductID(this.itemId);
            }

            foreach (ListItem listItem in chklstFacets.Items)
            {
                if (listItem.Selected == true)
                {
					flag = 1;
					lblError.Visible = false;
                    if (facetProductSkuList.Find("FacetID", Convert.ToInt32(listItem.Value)) == null)
                    {
                        FacetProductSKU tps = new FacetProductSKU();
                        tps.FacetID = Convert.ToInt32(listItem.Value);
                        if (this._ItemType == "SkuId")
                        {
                            tps.SKUID = this.skuId;
                        }
                        else
                        {
                            tps.ProductID = this.itemId;
                        }

                        service.Save(tps);
                    }

                    sb.Append(listItem.Text + ",");
                }
                else
                {
                    FacetProductSKU facetProductSku;
					if ((facetProductSku = facetProductSkuList.Find("FacetID", Convert.ToInt32(listItem.Value))) != null)
                    {
                        service.Delete(facetProductSku);
                    }
                }
            }
			if (flag == 0)
			{
				lblError.Visible = true;
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSelectAnyFacet").ToString();
				return;
			}

            ProductAdmin productAdmin = new ProductAdmin();
            Product product = productAdmin.GetByProductId(this.itemId);
            sb.Remove(sb.Length - 1, 1);

            this.associateName = String.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogFacetToProduct").ToString(), sb, product.Name);
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, product.Name);
			pnlFacetsList.Visible = true;
            facetProductSkuList.Dispose();
            service = null;

            this.BindGrid();

			this.pnlEditFacets.Visible = false;
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
			this.pnlEditFacets.Visible = false;
			pnlFacetsList.Visible = true;
			lblError.Visible = false;
        }

        protected void Add_Click(object sender, EventArgs e)
        {
			this.pnlEditFacets.Visible = true;
			pnlFacetsList.Visible = false;
			if (ddlFacetGroups.Items.Count > 0)
			{
				this.ddlFacetGroups.SelectedIndex = 0;
			}
			this.BindFacets();
        }

        protected void DdlFacetGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindFacets();
        }

        #endregion

        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = productAdmin.GetByProductId(this.itemId);

            if (e.CommandName == "FacetEdit")
            {
                ListItem listItem = this.ddlFacetGroups.Items.FindByValue(e.CommandArgument as string);
                if (listItem != null)
                {
                    this.ddlFacetGroups.SelectedIndex = this.ddlFacetGroups.Items.IndexOf(listItem);

                    this.BindFacets();

                    this.pnlEditFacets.Visible = true;
					pnlFacetsList.Visible = false;
                }

                this.associateName = String.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogFacetToProduct").ToString(), listItem.Text, product.Name);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, product.Name);
            }
            else if (e.CommandName == "FacetDelete")
            {
                FacetService facetService = new FacetService();
                TList<Facet> facetList = facetService.GetByFacetGroupID(Convert.ToInt32(e.CommandArgument));

                StringBuilder sb = new StringBuilder();
                foreach (Facet facet in facetList)
                {
                    sb.Append(facet.FacetName + ",");
                    this.RemoveFacetProductSku(facet);
                }

                //this.associateName = "Delete Association between Facet " + sb + " and Product " + product.Name;
                this.associateName = String.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteFacetAssociation").ToString(), sb) + product.Name;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, product.Name);
            }
        }      

        protected void UxGrid_DataBound(object sender, EventArgs e)
        {
            bool isVisibled = false;
            bool row = true;

            foreach (GridViewRow gvr in uxGrid.Rows)
            {
                gvr.Visible = (gvr.FindControl("dlFacetNames") as DataList).Items.Count > 0;

                if (gvr.Visible)
                {
                    gvr.CssClass = row ? "RowStyle" : "AlternatingRowStyle";
                    row = row ? false : true;
                }

                isVisibled = isVisibled || gvr.Visible;
            }
			uxGrid.Visible = isVisibled;

			lblEmptyGrid.Visible = !uxGrid.Visible;
        }

		/// <summary>
		/// Add Client side event to the Delete Button in the Grid.
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				// Retrieve the Button control from the Fourth column.
				LinkButton DeleteButton = (LinkButton)e.Row.Cells[3].FindControl("DeleteFacet");

				// Add Client Side confirmation
				DeleteButton.OnClientClick = "return confirm('Are you sure you want to delete this item?');";
			}
		}

		protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
		{
			this.BindFacets();
		}

        #region Helper Methods

        /// <summary>
		/// Remove facet product Sku
        /// </summary>
        /// <param name="facet">Facet object to get the list of product SKU</param>
        private void RemoveFacetProductSku(Facet facet)
        {
            FacetProductSKUService service = new FacetProductSKUService();
			TList<FacetProductSKU> facetProductSkuList = service.GetByFacetID(facet.FacetID);

			foreach (FacetProductSKU facetProductSku in facetProductSkuList)
            {
				if (facetProductSku.ProductID == this.itemId)
                {
					service.Delete(facetProductSku);
                }
            }

            this.BindGrid();
        }

        /// <summary>
		/// Get the associated facet groups.
        /// </summary>
		/// <returns>Returns the list of facet group.</returns>
        private TList<FacetGroup> GetAssociatedFacetGroups()
        {
			FacetGroupService facetGroupService = new FacetGroupService();
			TList<FacetGroup> facetGroupList = new TList<FacetGroup>();

            if (this.itemId > 0)
            {
                ProductService productService = new ProductService();
                Product product = productService.DeepLoadByProductID(this.itemId, true, ZNode.Libraries.DataAccess.Data.DeepLoadType.IncludeChildren, typeof(TList<ProductCategory>));

                if (product != null && product.ProductCategoryCollection != null)
                {
                    foreach (ProductCategory productCategory in product.ProductCategoryCollection)
                    {
                        foreach (FacetGroup facetGroup in facetGroupService.GetByCategoryIDFromFacetGroupCategory(productCategory.CategoryID))
                        {
                            if (facetGroupList.IndexOf(facetGroup) == -1)
                            {
                                facetGroupList.Add(facetGroup);
                            }
                        }
                    }
                }
            }

            return facetGroupList;
        }

        /// <summary>
        /// Bind the list of facet group list.
        /// </summary>
        /// <param name="facetGroupList">List if facet group to bind</param>
        private void BindFacetGroup(TList<FacetGroup> facetGroupList)
        {
            this.ddlFacetGroups.DataSource = facetGroupList;
            this.ddlFacetGroups.DataTextField = "FacetGroupLabel";
            this.ddlFacetGroups.DataValueField = "FacetGroupId";
            this.ddlFacetGroups.DataBind();

            if (facetGroupList.Count == 0)
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotAssociatedFacetCategory").ToString();
                lblError.Visible = true;
            }
        }

        /// <summary>
        /// Bind the Facets
        /// </summary>
        private void BindFacets()
        {
            if (!string.IsNullOrEmpty(ddlFacetGroups.SelectedValue))
            {
                FacetService facetService = new FacetService();
				this.chklstFacets.DataSource = facetService.GetByFacetGroupID(Convert.ToInt32(ddlFacetGroups.SelectedValue));
				this.chklstFacets.DataTextField = "FacetName";
				this.chklstFacets.DataValueField = "FacetId";
                this.chklstFacets.DataBind();

				FacetProductSKUService facetProductService = new FacetProductSKUService();
				TList<FacetProductSKU> facetProductSkuList;
                if (this._ItemType == "SkuId")
                {
					facetProductSkuList = facetProductService.GetBySKUID(this.skuId);
                }
                else
                {
					facetProductSkuList = facetProductService.GetByProductID(this.itemId);
                }

				if (facetProductSkuList.Count > 0)
                {
                    foreach (ListItem listItem in chklstFacets.Items)
                    {
						listItem.Selected = facetProductSkuList.Find("FacetID", Convert.ToInt32(listItem.Value)) != null;
                    }
                }
            }
        }

        /// <summary>
		/// Bind the facet group list
        /// </summary>
        private void BindGrid()
        {
			TList<FacetGroup> facetGroupList = this.GetAssociatedFacetGroups();
			this.BindFacetGroup(facetGroupList);

			uxGrid.Visible = true;

			uxGrid.DataSource = facetGroupList;
			uxGrid.DataBind();
        }

        /// <summary>
		/// Get the facet name
        /// </summary>
		/// <param name="facetGroupId">Facet Group Id</param>
		/// <returns>Returns the facet name.</returns>
		private string GetFacetNames(object facetGroupId)
        {
			FacetProductSKUService facetProductService = new FacetProductSKUService();
			TList<FacetProductSKU> facetProductSkuList;
            if (this._ItemType == "SkuId")
            {
				facetProductSkuList = facetProductService.GetBySKUID(this.skuId);
            }
            else
            {
				facetProductSkuList = facetProductService.GetByProductID(this.itemId);
            }

			FacetService facetService = new FacetService();
			string facetNames = string.Empty;
			if (facetProductSkuList.Count > 0)
            {
				foreach (FacetProductSKU tps in facetProductSkuList)
                {
					Facet facet = facetService.GetByFacetID(tps.FacetID.Value);
					if (facet.FacetGroupID == Convert.ToInt32(facetGroupId))
                    {
						if (!string.IsNullOrEmpty(facetNames))
                        {
							facetNames += ",";
                        }

						facetNames += facet.FacetName;
                    }
                }
            }

			return facetNames;
        }

        /// <summary>
		/// Get the list of product facets.
        /// </summary>
		/// <param name="facetGroupId">Facet Group Id</param>
		/// <returns>Returns the list of facets.</returns>
		protected TList<Facet> GetFacets(object facetGroupId)
        {
			FacetProductSKUService facetProductService = new FacetProductSKUService();
			TList<FacetProductSKU> facetProductSkuList;
            if (this._ItemType == "SkuId")
            {
                facetProductSkuList = facetProductService.GetBySKUID(this.skuId);
            }
            else
            {
                facetProductSkuList = facetProductService.GetByProductID(this.itemId);
            }

            FacetService facetService = new FacetService();
            TList<Facet> facetList = new TList<Facet>();
            if (facetProductSkuList.Count > 0)
            {
                foreach (FacetProductSKU tps in facetProductSkuList)
                {
                    Facet facet = facetService.GetByFacetID(tps.FacetID.Value);
                    if (facet.FacetGroupID == Convert.ToInt32(facetGroupId))
                    {
                        facetList.Add(facet);
                    }
                }
            }

            return facetList;
        }

        #endregion
    }
}