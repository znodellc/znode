﻿using System;
using System.Web;
using System.Web.UI;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_Product_AcceptProduct class.
    /// </summary>
    public partial class Admin_Secure_Product_AcceptProduct : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private string listPageLink = "~/MallAdmin/Secure/product/list.aspx";
        private string viewPageLink = "~/MallAdmin/Secure/product/view.aspx?ItemId=";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region Private Property
        /// <summary>
        /// Gets or sets the review next product Id.
        /// </summary>
        private string NextItemID
        {
            get
            {
                return ViewState["ViewState"].ToString();
            }

            set
            {
                ViewState["ViewState"] = value;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(HttpUtility.UrlDecode(this.encryption.DecryptData(Request.Params["itemid"].ToString())));
            }

            if (!Page.IsPostBack)
            {
                if (this.itemId > 0)
                {
                    // Bind Product Details
                    this.Bind();

                    this.SetNextReviewableProduct();
                }
            }
        }       

        #region Event Methods
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listPageLink);
        }

        protected void BtnNextProduct_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.viewPageLink + this.NextItemID);
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Set next product to review.
        /// </summary>
        private void SetNextReviewableProduct()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            Product product = ProdAdmin.GetNextProductToReview(ZNodeProductReviewState.ToEdit);

            if (product == null)
            {
                // There is no reviewable product available. So Hide the "Review Next Product" button
                divNextProduct.Visible = false;
            }
            else
            {
                // Store the review next product Id in viewstate.
                this.NextItemID = product.ProductID.ToString();
            }
        }

        /// <summary>
        /// Bind the review product
        /// </summary>
        private void Bind()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            Product product = ProdAdmin.GetByProductId(this.itemId);

            if (product != null)
            {
                lblTitle.Text += string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAcceptProduct").ToString(), product.Name);
                    //"Accept Product - \"" + product.Name + "\"";

                lblApprovedMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextMallProductAccepted").ToString(), product.Name);

                // Set Product and Vendor Details
                lblProductID.Text = product.ProductID.ToString();

                if (product.AccountID != null)
                {
                    AccountAdmin accountAdmin = new AccountAdmin();
                    Address address = accountAdmin.GetDefaultBillingAddress(Convert.ToInt32(product.AccountID));
                    if (address != null)
                    {
                        lblVendor.Text = address.FirstName + " " + address.LastName;
                    }
                }
                else
                {
                    lblVendor.Text = string.Empty;
                }
            }

            // Create Instance for Product Admin and Product entity
            bool isSuccess = false;

            if (product != null)
            {
                product.ReviewStateID = Convert.ToInt32(ZNodeProductReviewState.Approved);

                // Update product state in ZNodeProduct
                isSuccess = ProdAdmin.Update(product);
                ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();

                // Add entry to Product Review history table if product update is success.
                if (isSuccess)
                {
                    ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
                    ProductReviewHistory productReviewHistory = new ProductReviewHistory();
                    productReviewHistory.ProductID = this.itemId;
                    productReviewHistory.VendorID = Convert.ToInt32(product.AccountID);
                    productReviewHistory.LogDate = DateTime.Now;
                    productReviewHistory.Username = this.Page.User.Identity.Name;
                    productReviewHistory.Status = ZNodeProductReviewState.Approved.ToString();
                    productReviewHistory.Reason = string.Empty;
                    productReviewHistory.Description = string.Empty;

                    isSuccess = productReviewHistoryAdmin.Add(productReviewHistory);
                }
                else
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateProductReview").ToString();
                }
            }
        }

        #endregion
    }
}