﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Xml;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_Product_RejectProduct class.
    /// </summary>
    public partial class Admin_Secure_Product_RejectProduct : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private string managePageLink = "~/MallAdmin/Secure/product/view.aspx?ItemId=";
        private string listPageLink = "~/MallAdmin/Secure/product/List.aspx";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(HttpUtility.UrlDecode(this.encryption.DecryptData(Request.Params["itemid"].ToString())));
            }

            xmlRejectMessageDataSource.DataFile = ZNodeStorageManager.HttpPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "/RejectionMessageConfig.xml");

            if (!Page.IsPostBack)
            {
                this.BindRejectionReason();

                // Bind requested product detals 
                this.Bind();
            }
        }

        #endregion

        #region Event Methods
        /// <summary>
        /// Submit Button Click Event - Fires when Submit button is triggered
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            this.RejectProduct();
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["ReturnUrl"] != null)
            {
                // Request comes from product List page
                Response.Redirect("List.aspx");
            }
            else
            {
                // Request comes from product view page.
                Response.Redirect(this.managePageLink + this.itemId);
            }
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Bind the rejection reason.
        /// </summary>
        private void BindRejectionReason()
        {
            XmlNodeList nodeList = xmlRejectMessageDataSource.GetXmlDocument().SelectNodes("//Messages/Message[@PortalID='" + ZNodeConfigManager.SiteConfig.PortalID + "'][@LocaleID='" + ZNodeConfigManager.SiteConfig.LocaleID + "']");
            List<string> reasonList = new List<string>();
            foreach (XmlNode node in nodeList)
            {
                reasonList.Add(node.Attributes["MsgValue"].Value);
            }

            ddlReason.DataSource = reasonList;
            ddlReason.DataBind();
        }

        /// <summary>
        /// Update the product status as rejected.
        /// </summary>
        private void RejectProduct()
        {
            bool isSuccess = false;
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = new Product();
            string updateFailed = this.GetGlobalResourceObject("ErrorUpdateProductReview", "ErrorUpdateProductReview").ToString();
            StringBuilder historyUpdateFailedMsg = new StringBuilder();
            StringBuilder reviewStateUpdateFailedMsg = new StringBuilder();

            if (this.itemId > 0)
            {
                product = productAdmin.GetByProductId(this.itemId);

                if (product.AccountID == null)
                {
                    lblError.Text = this.GetGlobalResourceObject("ErrorUpdateProductReview", "ErrorVendorID").ToString();
                    return;
                }

                // This is approval reject page, so set product review state as Rejected
                product.ReviewStateID = Convert.ToInt32(ZNodeProductReviewState.Declined);

                if (this.itemId > 0)
                {
                    isSuccess = productAdmin.Update(product);
                    if (isSuccess)
                    {
                        isSuccess = this.SaveProductHistory(this.itemId, Convert.ToInt32(product.AccountID));
                    }

                    if (isSuccess)
                    {
                        Response.Redirect(this.managePageLink + this.itemId, false);
                    }
                    else
                    {
                        lblError.Text = updateFailed;
                    }
                }
            }
            else
            {
                ProductReviewStateAdmin reviewstateAdmin = new ProductReviewStateAdmin();

                // This is Product Reject page, So directly assign the state
                ProductReviewState reviewState = reviewstateAdmin.GetProductReviewStateByName(ZNodeProductReviewState.Declined.ToString());
                StringBuilder sb = new StringBuilder();
                Dictionary<int, string> productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];

                // Loop through the grid values
                if (productIdList != null && reviewState != null)
                {
                    foreach (KeyValuePair<int, string> pair in productIdList)
                    {
                        // Get ProductId
                        int productId = pair.Key;

                        Product entity = productAdmin.GetByProductId(productId);

                        // Update only if existing status is not same as current status
                        if (entity.ReviewStateID != reviewState.ReviewStateID)
                        {
                            entity.ReviewStateID = reviewState.ReviewStateID;
                            isSuccess = productAdmin.Update(entity);
                            if (!isSuccess)
                            {
                                reviewStateUpdateFailedMsg.Append("<BR>" + product.ShortDescription);
                            }
                        }

                        isSuccess = this.SaveProductHistory(productId, Convert.ToInt32(entity.AccountID));

                        if (!isSuccess)
                        {
                            historyUpdateFailedMsg.Append("<BR>" + product.Name);
                        }
                    }
                }

                if (reviewStateUpdateFailedMsg.Length > 0)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorReviewState").ToString() + reviewStateUpdateFailedMsg.ToString();
                    return;
                }

                if (historyUpdateFailedMsg.Length > 0)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorReviewHistory").ToString() + historyUpdateFailedMsg.ToString();
                    return;
                }

                // Clear the selection.
                Session["CHECKEDITEMS"] = null;

                Response.Redirect(this.listPageLink);
            }
        }

        /// <summary>
        /// Save the entry to product history table.
        /// </summary>
        /// <param name="productId">Product ID to save into history</param>
        /// <param name="vendorId">Vendor ID to save into history</param>
        /// <returns>Returns true if success else False.</returns>
        private bool SaveProductHistory(int productId, int vendorId)
        {
            // Add entry to Product Review history table.
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            ProductReviewHistory productReviewHistory = new ProductReviewHistory();
            productReviewHistory.ProductID = productId;
            productReviewHistory.VendorID = vendorId;
            productReviewHistory.LogDate = DateTime.Now;
            productReviewHistory.Username = this.Page.User.Identity.Name;
            productReviewHistory.Status = ZNodeProductReviewState.Declined.ToString();
            productReviewHistory.Reason = ddlReason.SelectedItem.Text;
            productReviewHistory.Description = HttpUtility.HtmlEncode(txtDescription.Text);
            return productReviewHistoryAdmin.Add(productReviewHistory);
        }
        #endregion

        #region Bind Methods
        private void Bind()
        {
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            AccountAdmin accountAdmin = new AccountAdmin();
            Product product = null;
            string unknownVendor = this.GetGlobalResourceObject("ZnodeAdminResource", "TextUnknown").ToString();
            if (this.itemId > 0)
            {
                // Reject request comes from view page Create Instance for Product Admin and Product entity 
                product = ProdAdmin.GetByProductId(this.itemId);
                if (product != null)
                {
                    lblTitle.Text += string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TitleMallRejectProduct").ToString(), product.Name);

                    // Set Product and Vendor Details
                    lblProductID.Text = product.ProductID.ToString();

                    if (product.AccountID != null)
                    {
                        Address address = accountAdmin.GetDefaultBillingAddress(Convert.ToInt32(product.AccountID));
                        if (address != null)
                        {
                            lblVendor.Text = address.FirstName + " " + address.LastName;
                        }
                    }
                    else
                    {
                        lblVendor.Text = unknownVendor;
                    }
                }
            }
            else
            {
                // Reject request comes from List page
                Dictionary<int, string> productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];
                if (productIdList != null)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleRejectMultipleProductSelect").ToString();
                    StringBuilder commaSeparatedIdList = new StringBuilder();
                    List<int> distinctSupplierIdList = new List<int>();
                    StringBuilder vendorList = new StringBuilder();
                    foreach (KeyValuePair<int, string> pair in productIdList)
                    {
                        commaSeparatedIdList.Append(pair.Key + ", ");
                    }

                    // Find distinct account Ids of the product
                    foreach (KeyValuePair<int, string> pair in productIdList)
                    {
                        product = ProdAdmin.GetByProductId(pair.Key);
                        if (product != null && product.AccountID != null)
                        {
                            int vendorId = Convert.ToInt32(product.AccountID);
                            if (!distinctSupplierIdList.Contains(vendorId))
                            {
                                distinctSupplierIdList.Add(vendorId);
                            }
                        }
                    }

                    foreach (int vendorId in distinctSupplierIdList)
                    {
                        Address address = accountAdmin.GetDefaultBillingAddress(vendorId);
                        if (address != null)
                        {
                            vendorList.Append(address.FirstName + " " + address.LastName + ", ");
                        }
                    }

                    lblProductID.Text = commaSeparatedIdList.ToString().Substring(0, commaSeparatedIdList.ToString().LastIndexOf(","));
                    if (vendorList.ToString().IndexOf(",") > 1)
                    {
                        lblVendor.Text = vendorList.ToString().Substring(0, vendorList.ToString().LastIndexOf(","));
                    }
                    else
                    {
                        lblVendor.Text = unknownVendor;
                    }
                }
            }
        }
        #endregion
    }
}
