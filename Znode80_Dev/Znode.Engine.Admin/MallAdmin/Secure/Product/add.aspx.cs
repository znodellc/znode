using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_products_add class.
    /// </summary>
    public partial class Admin_Secure_products_add : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId;
        private int stepId = 0;
        private int skuId = 0;
        private int productId = 0;
        private int productTypeId = 0;
        private string accountID = string.Empty;
        private string cancelPageLink = "view.aspx?itemid=";
        private string listPageLink = "list.aspx";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        private bool isSKUUpdated;
        private Image userName;
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id value from query string
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));

                this.productId = this.itemId;
            }
            else if (Session["itemid"] != null)
            {
                this.itemId = int.Parse(Session["itemid"].ToString());

                this.productId = this.itemId;
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["returnurl"] != null)
                {
                    this.itemId = 0;
                    Session["itemid"] = null;
                }

                if (Session["stepid"] != null)
                {
                    this.stepId = int.Parse(Session["stepid"].ToString());
                    Session["stepid"] = null;
                }

                // Dynamic error message for weight field compare validator
                WeightFieldCompareValidator.ErrorMessage = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorEnterValidProduct").ToString();

                // Toggle Is Tangible section
                chkShipInd.Attributes.Add("onclick", "setTangibleSectionEnabled()");

                // Toggle Recurring Billing section
                chkRecurringInd.Attributes.Add("onclick", "setRecurringSectionEnabled()");

                this.BindTreeViewCategory();

                this.DisplaySelectedCategories();

                // If edit func then bind the data fields
                if (this.itemId > 0)
                {
                    tblShowImage.Visible = true;
                    pnlShowImage.Visible = true;
                    tblProductDescription.Visible = false;

                    this.BindEditData();
                }
                else
                {
                    tblShowImage.Visible = true;
                    pnlShowImage.Visible = false;
                    pnlImage.Visible = false;
                    tblProductDescription.Visible = true;
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEnterProduct").ToString();
                    InvSettingRadiobtnList.Items[2].Selected = true;
                }

                btnSubmit.Visible = true;
                btnClear.Visible = true;

                dvBottomButtons.Visible = true;
            }

            // Bind SKU Attributes Dynamically
            if (!string.IsNullOrEmpty(ProductTypeList.Value))
            {
                this.Bind(int.Parse(ProductTypeList.Value));

                if (this.itemId > 0)
                {
                    // Bind Edit SKU Attributes
                    this.BindAttributes(int.Parse(ProductTypeList.Value));
                }
            }
        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
        }
        #endregion

        #region General Events
        /// <summary> 
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.itemId > 0)
            {
                Response.Redirect(this.cancelPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())));
            }
            else
            {
                Response.Redirect(this.listPageLink);
            }
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = true;
        }

        /// <summary>
        /// Shipping rule type selected index change event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ShippingTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnableValidators();
        }

        /// <summary>
        /// Redirecting to Next step
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnNext_Click(object sender, EventArgs e)
        {
            this.SaveProductInformation();
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Display selected categories.
        /// </summary>
        private void DisplaySelectedCategories()
        {
            StringBuilder categories = new StringBuilder();
            categories.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "TextCurrentCategory"));
            TreeNodeCollection nodeCollection = CategoryTreeView.CheckedNodes;
            if (nodeCollection.Count > 0)
            {
                foreach (TreeNode node in nodeCollection)
                {
                    categories.Append(node.Text + "<BR>");
                }
            }
            else
            {
                categories.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "ValidCategoryAssociation"));
            }

            lblCategories.Text = categories.ToString();
        }

        /// <summary>
        /// Enable the weight control validators.
        /// </summary>
        private void EnableValidators()
        {
            weightBasedRangeValidator.Enabled = false;
            RequiredForWeightBasedoption.Enabled = false;
        }

        /// <summary>
        /// Save or Update the product
        /// </summary>
        private void SaveProductInformation()
        {
            if (string.IsNullOrEmpty(ProductTypeList.Value))
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextInformSiteAdmin").ToString();
                return;
            }
            SKUAdmin skuAdminAccess = new SKUAdmin();
            ProductAdmin productAdmin = new ProductAdmin();
            SKUAttribute skuAttribute = new SKUAttribute();

            SKU sku = new SKU();
            SKUAdmin skuAdmin = new SKUAdmin();

            ProductCategory productCategory = new ProductCategory();
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            Product product = new Product();
            System.IO.FileInfo fileInfo = null;
            System.IO.FileInfo skufileInfo = null;

            bool isSuccess = false;

            // Set Product Properties
            product.ProductID = this.itemId;

            // if edit mode then get all the values first
            if (this.itemId > 0)
            {
                product = productAdmin.GetByProductId(this.itemId);

                if (ViewState["productSkuId"] != null)
                {
                    sku = skuAdminAccess.GetBySKUID(Convert.ToInt32(ViewState["productSkuId"].ToString()));
                }
                else
                {
                    // Inventory
                    TList<SKU> skuList = skuAdmin.GetByProductID(this.itemId);
                    if (skuList.Count > 0)
                    {
                        sku = skuList[0];
                        ViewState["productSkuId"] = sku.SKUID;
                    }
                }

                bool isSkuExist = skuAdminAccess.IsSkuExist(txtSku.Text.Trim(), this.itemId);

                if (isSkuExist)
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSkuExists").ToString();
                    return;
                }

                productAdmin.DeleteProductCategories(this.itemId);

                // Add Product Categories
                foreach (TreeNode Node in CategoryTreeView.CheckedNodes)
                {
                    ProductCategory prodCategory = new ProductCategory();
                    ProductAdmin prodAdmin = new ProductAdmin();
                    prodCategory.ActiveInd = true;
                    prodCategory.CategoryID = int.Parse(Node.Value);
                    prodCategory.ProductID = this.itemId;
                    prodAdmin.AddProductCategory(prodCategory);
                }
            }

            // Product general Info
            product.Name = Server.HtmlEncode(txtProductName.Text);

            AccountService accountService = new AccountService();
            TList<Account> accountList = accountService.GetByUserID((Guid)Membership.GetUser().ProviderUserKey);
            if (accountList != null && accountList.Count > 0)
            {
                accountID = accountList[0].AccountID.ToString();
            }

            if (this.itemId > 0)
            {
                product.ImageFile = txtimagename.Text;
            }
            else
            {
                product.ImageFile = "Mall/" + accountID + "/" + txtimagename.Text;
            }

            product.ProductNum = Server.HtmlEncode(txtProductNum.Text);

            // ProductType
            product.ProductTypeID = int.Parse(ProductTypeList.Value);

            // Display Settings
            product.DisplayOrder = 500;

            product.AffiliateUrl = txtAffiliateLink.Text.Trim();
            product.Description = Server.HtmlEncode(htmlPortalDesc.Html);
            product.ShortDescription = Server.HtmlEncode(txtShortDesc.Text);
            product.AdditionalInformation = Server.HtmlEncode(htmlShippingDesc.Html);
            product.FeaturesDesc = Server.HtmlEncode(htmlProductDetail.Html);
            product.RetailPrice = Convert.ToDecimal(txtproductRetailPrice.Text);
            product.InStockMsg = Server.HtmlEncode(txtInStockMsg.Text);
            product.OutOfStockMsg = Server.HtmlEncode(txtOutofStock.Text);
            product.BackOrderMsg = Server.HtmlEncode(txtBackOrderMsg.Text);

            if (txtproductSalePrice.Text.Trim().Length > 0)
            {
                product.SalePrice = Convert.ToDecimal(txtproductSalePrice.Text.Trim());
            }
            else
            {
                product.SalePrice = null;
            }

            if (txtproductshipweight.Text.Trim().Length > 0)
            {
                product.Weight = Convert.ToDecimal(txtproductshipweight.Text.Trim());
            }
            else
            {
                product.Weight = null;
            }

            // Product Height - Which will be used to determine the shipping cost
            if (txtProductHeight.Text.Trim().Length > 0)
            {
                product.Height = decimal.Parse(txtProductHeight.Text.Trim());
            }
            else
            {
                product.Height = null;
            }

            if (txtProductWidth.Text.Trim().Length > 0)
            {
                product.Width = decimal.Parse(txtProductWidth.Text.Trim());
            }
            else
            {
                product.Width = null;
            }

            if (txtProductLength.Text.Trim().Length > 0)
            {
                product.Length = decimal.Parse(txtProductLength.Text.Trim());
            }
            else
            {
                product.Length = null;
            }


            // Quantity Available
            int quantity;
            decimal handling;
            decimal amount;
            decimal.TryParse(txtStartupfee.Text, out amount);
            int.TryParse(txtProductQuantity.Text, out quantity);
            decimal.TryParse(txtHandling.Text, out handling);
            product.RecurringBillingInd = chkRecurringInd.Checked;
            if (chkRecurringInd.Checked)
            {
                product.RecurringBillingInitialAmount = amount;
                product.RecurringBillingPeriod = txtContinueBill.Text;
                product.RecurringBillingFrequency = txtBillevery.Text + "-" + ddlBillEvery.SelectedItem.Text;
            }

            product.ActiveInd = chkActiveInd.Checked;
            product.IsShippable = chkShipInd.Checked;

            // Inventory Setting - Out of Stock Options
            if (InvSettingRadiobtnList.SelectedValue.Equals("1"))
            {
                // Only Sell if Inventory Available - Set values
                product.TrackInventoryInd = true;
                product.AllowBackOrder = false;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("2"))
            {
                // Allow Back Order - Set values
                product.TrackInventoryInd = true;
                product.AllowBackOrder = true;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("3"))
            {
                // Don't Track Inventory - Set property values
                product.TrackInventoryInd = false;
                product.AllowBackOrder = false;
            }

            product.ShippingRate = handling;

            // Stock
            sku.ProductID = this.itemId;
            sku.SKU = txtSku.Text.Trim();
            sku.ActiveInd = true;

            this.isSKUUpdated = SKUAdmin.UpdateQuantity(sku, Convert.ToInt32(txtProductQuantity.Text));

            // Database & Image Updates                
            DataSet MyAttributeTypeDataSet = productAdmin.GetAttributeTypeByProductTypeID(int.Parse(product.ProductTypeID.ToString()));

            // Create transaction
            TransactionManager transactionManager = ConnectionScope.CreateTransaction();

            try
            {
                if (this.itemId > 0)
                {
                    if (uxProductImage.PostedFile != null)
                    {
                        // Validate image
                        if ((this.itemId == 0 || RadioProductNewImage.Checked == true) && uxProductImage.PostedFile.FileName.Length > 0)
                        {
                            uxProductImage.AppendToFileName = "-" + this.productId.ToString();
                        }
                    }

                    // Upload File if this is a new product or the New Image option was selected for an existing product
                    if (RadioProductNewImage.Checked || this.itemId == 0)
                    {
                        if (uxProductImage.PostedFile != null && !string.IsNullOrEmpty(uxProductImage.PostedFile.FileName))
                        {
                            isSuccess = uxProductImage.SaveImage("madmin", accountID.ToString());
                            fileInfo = uxProductImage.FileInformation;
                            skufileInfo = uxProductImage.FileInformation;
                            if (!isSuccess)
                            {
                                if (uxProductImage.FileInformation.Name != string.Empty)
                                {
                                    product.ImageFile = "Mall/" + accountID + "/" + uxProductImage.FileInformation.Name;
                                    sku.SKUPicturePath = "Mall/" + accountID + "/" + skufileInfo.Name;
                                }

                                return;
                            }
                            else
                            {
                                product.ImageFile = "Mall/" + accountID + "/" + uxProductImage.FileInformation.Name;
                                sku.SKUPicturePath = "Mall/" + accountID + "/" + skufileInfo.Name;
                            }
                        }
                    }

                    // Update product Sku and Product values
                    // If ProductType has SKU's
                    if (MyAttributeTypeDataSet.Tables[0].Rows.Count > 0)
                    {
                        // For this product already SKU if on exists
                        if (sku.SKUID > 0)
                        {
                            sku.UpdateDte = System.DateTime.Now;

                            // Check whether Duplicate attributes is created
                            string Attributes = String.Empty;

                            DataSet MyAttributeTypeDataSet1 = productAdmin.GetAttributeTypeByProductTypeID(int.Parse(product.ProductTypeID.ToString()));

                            foreach (DataRow MyDataRow in MyAttributeTypeDataSet1.Tables[0].Rows)
                            {
                                System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                                if (lstControl != null)
                                {
                                    int selValue = int.Parse(lstControl.SelectedValue);

                                    Attributes += selValue.ToString() + ",";
                                }
                            }

                            // Split the string
                            string Attribute = Attributes.Substring(0, Attributes.Length - 1);

                            // To check SKU combination is already exists.
                            bool RetValue = skuAdminAccess.CheckSKUAttributes(this.itemId, sku.SKUID, Attribute);

                            if (!RetValue)
                            {
                                product.ReviewStateID = productAdmin.UpdateReviewStateID(product, UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId);

                                product.UpdateDte = System.DateTime.Now;

                                // Then Update the database with new property values
                                isSuccess = productAdmin.Update(product, sku);
                            }
                            else
                            {
                                // Throw error if duplicate attribute
                                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAttributeExist").ToString();
                                return;
                            }
                        }
                        else
                        {
                            product.ReviewStateID = productAdmin.UpdateReviewStateID(product, UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId);

                            product.UpdateDte = System.DateTime.Now;

                            isSuccess = productAdmin.Update(product);

                            // If Product doesn't have any SKUs yet,then create new SKU in the database
                            skuAdminAccess.Add(sku);
                        }
                    }
                    else
                    {
                        product.ReviewStateID = productAdmin.UpdateReviewStateID(product, UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId);

                        if (this.isSKUUpdated)
                        {
                            product.ReviewStateID = productAdmin.UpdateReviewStateID(UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId, "Quantity");
                        }

                        product.UpdateDte = System.DateTime.Now;

                        isSuccess = productAdmin.Update(product);

                        // If User selectes Default product type for this product,
                        // then Remove all the existing SKUs for this product
                        skuAdminAccess.DeleteByProductId(this.itemId);

                        // Add default product sku
                        skuAdminAccess.Add(sku);
                    }

                    if (!isSuccess)
                    {
                        throw new ApplicationException();
                    }

                    // Delete existing SKUAttributes
                    skuAdminAccess.DeleteBySKUId(sku.SKUID);
                }
                else
                {
                    // Product Add
                    product.ActiveInd = true;

                    product.ReviewStateID = productAdmin.UpdateReviewStateID(product, UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId);

                    product.UpdateDte = System.DateTime.Now;

                    // Update Account ID reference for Vendor.
                    if (accountList != null && accountList.Count > 0)
                    {
                        product.AccountID = accountList[0].AccountID;

                        TList<Supplier> supplier = DataRepository.SupplierProvider.GetByExternalSupplierNoName(accountList[0].ExternalAccountNo, accountList[0].CompanyName);

                        if (supplier != null && supplier.Count > 0)
                        {
                            product.SupplierID = supplier[0].SupplierID;
                        }
                    }

                    // If ProductType has SKUs, then insert sku with Product
                    isSuccess = productAdmin.Add(product, sku, out this.productId, out this.skuId);

                    if (!isSuccess)
                    {
                        throw new ApplicationException();
                    }

                    // Add SKU Attributes
                    foreach (DataRow MyDataRow in MyAttributeTypeDataSet.Tables[0].Rows)
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                        int selValue = int.Parse(lstControl.SelectedValue);

                        if (selValue > 0)
                        {
                            skuAttribute.AttributeId = selValue;
                        }

                        skuAttribute.SKUID = this.skuId;

                        skuAdminAccess.AddSKUAttribute(skuAttribute);
                    }

                    // Add Category List for the Product
                    foreach (TreeNode Node in CategoryTreeView.CheckedNodes)
                    {
                        ProductCategory prodCategory = new ProductCategory();
                        ProductAdmin prodAdmin = new ProductAdmin();
                        prodCategory.ActiveInd = true;
                        prodCategory.CategoryID = int.Parse(Node.Value);
                        prodCategory.ProductID = this.productId;
                        prodAdmin.AddProductCategory(prodCategory);
                    }

                    // Image Validation
                    if (uxProductImage.PostedFile != null)
                    {
                        if ((this.itemId == 0 || RadioProductNewImage.Checked == true) && uxProductImage.PostedFile.FileName.Length > 0)
                        {
                            uxProductImage.AppendToFileName = "-" + this.productId.ToString();
                        }
                    }

                    // Upload File if this is a new product or the New Image option was selected for an existing product
                    if (RadioProductNewImage.Checked || this.itemId == 0)
                    {
                        if (uxProductImage.PostedFile != null && !string.IsNullOrEmpty(uxProductImage.PostedFile.FileName))
                        {
                            isSuccess = uxProductImage.SaveImage("madmin", accountID.ToString());
                            fileInfo = uxProductImage.FileInformation;
                            skufileInfo = uxProductImage.FileInformation;
                            if (!isSuccess)
                            {
                                if (uxProductImage.FileInformation.Name != string.Empty)
                                {
                                    product.ImageFile = "Mall/" + accountID + "/" + uxProductImage.FileInformation.Name;
                                    sku.SKUPicturePath = "Mall/" + accountID + "/" + skufileInfo.Name;
                                }
                            }
                            else
                            {
                                product.ImageFile = "Mall/" + accountID + "/" + uxProductImage.FileInformation.Name;
                                sku.SKUPicturePath = "Mall/" + accountID + "/" + skufileInfo.Name;
                            }
                        }
                    }

                    productAdmin.Update(product);
                }

                // Commit transaction
                transactionManager.Commit();
            }
            catch (Exception exp)
            {
                // Error occurred so rollback transaction
                if (transactionManager.IsOpen)
                {
                    transactionManager.Rollback();
                }

                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exp.Message.ToString());
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductUpdate").ToString();
                return;
            }

            //Send Mail function
            this.SendMailByVendor(productId.ToString(), product.Name.ToString());

            // Redirect to next page
            UserStoreAccess uss = new UserStoreAccess();
            if (this.itemId > 0)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEdit") + txtProductName.Text, txtProductName.Text);
                Response.Redirect(this.cancelPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())));
            }
            else
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCreateProduct") + txtProductName.Text, txtProductName.Text);

                Session["itemid"] = this.productId;
                this.itemId = this.productId;

                Response.Redirect(this.cancelPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())));
            }
        }

        #endregion

        #region Bind Category Tree view
        /// <summary>
        /// Bind TreeView with Categories
        /// </summary>
        private void BindTreeViewCategory()
        {
            this.PopulateAdminTreeView(string.Empty);
        }

        /// <summary>
        /// Populates a treeview with store categories for a Product
        /// </summary>
        /// <param name="selectedCategoryId">selectedCategoryId to populate.</param>
        private void PopulateEditTreeView(string selectedCategoryId)
        {
            foreach (TreeNode treeNode in CategoryTreeView.Nodes)
            {
                if (treeNode.Value.Equals(selectedCategoryId))
                {
                    treeNode.Checked = true;
                }

                this.RecursivelyEditPopulateTreeView(treeNode, selectedCategoryId);
            }
        }

        /// <summary>
        /// Recursively populate a particular node with it's children for a product
        /// </summary>
        /// <param name="childNodes">TreeNode object</param>
        /// <param name="selectedCategoryid">SelectedCategoryid to poulate the treeview.</param>
        private void RecursivelyEditPopulateTreeView(TreeNode childNodes, string selectedCategoryid)
        {
            foreach (TreeNode CNodes in childNodes.ChildNodes)
            {
                if (CNodes.Value.Equals(selectedCategoryid))
                {
                    CNodes.Checked = true;
                    this.RecursivelyExpandTreeView(CNodes);
                }

                this.RecursivelyEditPopulateTreeView(CNodes, selectedCategoryid);
            }
        }

        /// <summary>
        /// Populates a treeview with store categories
        /// </summary>
        /// <param name="selectedCategoryId">Selected category Id</param>
        private void PopulateAdminTreeView(string selectedCategoryId)
        {
            CategoryHelper categoryHelper = new CategoryHelper();
            System.Data.DataSet ds = categoryHelper.GetNavigationItems(ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID);

            // Add the hierarchical relationship to the dataset
            ds.Relations.Add("NodeRelation", ds.Tables[0].Columns["CategoryNodeId"], ds.Tables[0].Columns["ParentCategoryNodeId"]);

            foreach (DataRow dataRow in ds.Tables[0].Rows)
            {
                if (dataRow.IsNull("ParentCategoryNodeID"))
                {
                    if ((bool)dataRow["VisibleInd"])
                    {
                        // Create new tree node
                        TreeNode treeNode = new TreeNode();
                        treeNode.Text = dataRow["Name"].ToString();
                        treeNode.Value = dataRow["categoryid"].ToString();

                        string categoryId = dataRow["CategoryId"].ToString();

                        // Add Root Node to Category Tree view
                        CategoryTreeView.Nodes.Add(treeNode);

                        if (selectedCategoryId.Equals(categoryId))
                        {
                            treeNode.Selected = true;
                            this.RecursivelyExpandTreeView(treeNode);
                        }

                        this.RecursivelyPopulateTreeView(dataRow, treeNode, selectedCategoryId);
                    }
                }
            }
        }

        /// <summary>
        /// Recursively populate a particular node with it's children
        /// </summary>
        /// <param name="dataRow">DataRow object that contains data.</param>
        /// <param name="parentNode">Parenet Node object</param>
        /// <param name="selectedCategoryId">Selected category Id</param>
        private void RecursivelyPopulateTreeView(DataRow dataRow, TreeNode parentNode, string selectedCategoryId)
        {
            bool isLeafNode = true;

            foreach (DataRow childRow in dataRow.GetChildRows("NodeRelation"))
            {
                if ((bool)childRow["VisibleInd"])
                {
                    isLeafNode = false;

                    // Create new tree node
                    TreeNode treeNode = new TreeNode();
                    treeNode.Text = childRow["Name"].ToString();
                    treeNode.Value = childRow["categoryid"].ToString();

                    string categoryId = childRow["CategoryId"].ToString();

                    parentNode.ChildNodes.Add(treeNode);

                    if (selectedCategoryId.Equals(categoryId))
                    {
                        treeNode.Selected = true;
                        this.RecursivelyExpandTreeView(treeNode);
                    }

                    this.RecursivelyPopulateTreeView(childRow, treeNode, selectedCategoryId);
                }
            }

            if (isLeafNode)
            {
                parentNode.SelectAction = TreeNodeSelectAction.None;
            }
        }

        /// <summary>
        /// Recursively expands parent nodes of a selected tree node
        /// </summary>
        /// <param name="nodeItem">TreeNode object to expand.</param>
        private void RecursivelyExpandTreeView(TreeNode nodeItem)
        {
            if (nodeItem.Parent != null)
            {
                nodeItem.Parent.ExpandAll();
                this.RecursivelyExpandTreeView(nodeItem.Parent);
            }
            else
            {
                nodeItem.Expand();
                return;
            }
        }

        #endregion

        #region Bind Edit Data

        /// <summary>
        /// Bind value for Particular Product
        /// </summary>
        private void BindEditData()
        {
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = new Product();
            ProductCategory productCategory = new ProductCategory();
            SKUAdmin skuAdmin = new SKUAdmin();

            if (this.itemId > 0)
            {
                // Checking the user profile for roles and permission and then loading the data based on roles
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                UserStoreAccess uss = new UserStoreAccess();
                product = productAdmin.GetByProductId(this.itemId);
                if (product != null)
                {
                    ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();

                    if (product.AccountID != userAccount.AccountID && uss.UserType == Znode.Engine.Common.ZNodeUserType.Vendor)
                    {
                        Response.Redirect(this.listPageLink);
                    }
                }

                ProductService productService = new ProductService();
                productService.DeepLoad(product);
                productCategory = productAdmin.GetProductCategoryByProductID(this.itemId);

                // General Information - Section1
                if (this.itemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditProduct") + product.Name;
                }

                txtProductName.Text = Server.HtmlDecode(product.Name);
                txtProductNum.Text = Server.HtmlDecode(product.ProductNum);

                if (product.ProductTypeID > 0)
                {
                    this.productTypeId = product.ProductTypeID;
                    ProductTypeList.Value = product.ProductTypeID.ToString();
                }

                // Product Description and Image - Section2
                if (product.Description != null)
                {
                    htmlPortalDesc.Html = Server.HtmlDecode(product.Description);
                }

                if (product.ShortDescription != null)
                {
                    txtShortDesc.Text = Server.HtmlDecode(product.ShortDescription);
                }

                if (product.AdditionalInformation != null)
                {
                    htmlShippingDesc.Html = Server.HtmlDecode(product.AdditionalInformation);
                }

                if (product.FeaturesDesc != null)
                {
                    htmlProductDetail.Html = Server.HtmlDecode(product.FeaturesDesc);
                }

                ZNodeImage znodeImage = new ZNodeImage();
                Image1.ImageUrl = znodeImage.GetImageHttpPathMedium(product.ImageFile);

                // Displaying the Image file name in a textbox           
                txtimagename.Text = product.ImageFile;

                txtAffiliateLink.Text = product.AffiliateUrl;
                chkActiveInd.Checked = product.ActiveInd;
                chkRecurringInd.Checked = product.RecurringBillingInd;
                if (product.RecurringBillingInd)
                {
                    if (product.RecurringBillingInitialAmount.HasValue)
                    {
                        txtStartupfee.Text = product.RecurringBillingInitialAmount.Value.ToString("N");
                    }

                    chkActiveInd.Checked = product.RecurringBillingInd;

                    if (product.RecurringBillingFrequency.Length > 0)
                    {
                        string[] freq = product.RecurringBillingFrequency.Split('-');
                        txtBillevery.Text = freq[0];
                        ddlBillEvery.Text = freq[1];
                    }

                    if (product.RecurringBillingPeriod.Length > 0)
                    {
                        string[] period = product.RecurringBillingPeriod.Split('-');
                        txtContinueBill.Text = period[0];
                    }

                    txtStartupfee.Enabled = true;
                    txtBillevery.Enabled = true;
                    ddlBillEvery.Enabled = true;
                    txtContinueBill.Enabled = true;
                }

                if (product.IsShippable.HasValue)
                {
                    chkShipInd.Checked = product.IsShippable.Value;
                    txtProductWeight.Enabled = true;
                    txtHandling.Enabled = true;
                }

                if (product.ShippingRate.HasValue)
                {
                    txtHandling.Text = product.ShippingRate.Value.ToString("N");
                }

                // Product properties
                if (product.RetailPrice.HasValue)
                {
                    txtproductRetailPrice.Text = product.RetailPrice.Value.ToString("N");
                }

                if (product.SalePrice.HasValue)
                {
                    txtproductSalePrice.Text = product.SalePrice.Value.ToString("N");
                }

                txtOutofStock.Text = Server.HtmlDecode(product.OutOfStockMsg);
                txtInStockMsg.Text = Server.HtmlDecode(product.InStockMsg);
                txtBackOrderMsg.Text = Server.HtmlDecode(product.BackOrderMsg);
                chkActiveInd.Checked = product.ActiveInd;

                if (product.Weight.HasValue)
                {
                    txtproductshipweight.Text = product.Weight.Value.ToString("N2");
                }

                if (product.Height.HasValue)
                {
                    txtProductHeight.Text = product.Height.Value.ToString("N2");
                }

                if (product.Width.HasValue)
                {
                    txtProductWidth.Text = product.Width.Value.ToString("N2");
                }

                if (product.Length.HasValue)
                {
                    txtProductLength.Text = product.Length.Value.ToString("N2");
                }


                // Inventory Setting - Out of Stock Options
                if (product.AllowBackOrder.HasValue && product.TrackInventoryInd.HasValue)
                {
                    if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value == false)
                    {
                        InvSettingRadiobtnList.Items[0].Selected = true;
                    }
                    else if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value)
                    {
                        InvSettingRadiobtnList.Items[1].Selected = true;
                    }
                    else if ((product.TrackInventoryInd.Value == false) && (product.AllowBackOrder.Value == false))
                    {
                        InvSettingRadiobtnList.Items[2].Selected = true;
                    }
                }

                if (product.Weight.HasValue)
                {
                    txtProductWeight.Text = product.Weight.Value.ToString("N2");
                }

                this.BindEditCategoryList();

                // Display the current categories
                this.DisplaySelectedCategories();

                // Inventory
                TList<SKU> skuList = skuAdmin.GetByProductID(this.itemId);

                if (skuList != null && skuList.Count > 0)
                {
                    ViewState["productSkuId"] = skuList[0].SKUID.ToString();

                    // Set the existing value in hidden
                    existSKUvalue.Value = skuList[0].SKU;

                    SKUInventory skuInventory = skuAdmin.GetSkuInventoryBySKU(skuList[0].SKU);

                    if (skuInventory != null)
                    {
                        txtProductQuantity.Text = skuInventory.QuantityOnHand.GetValueOrDefault(0).ToString();
                        txtSku.Text = skuInventory.SKU;
                    }

                    // This method will enable or disable validators based on the shippingtype
                    this.EnableValidators();

                    // Release the Resources
                    skuList.Dispose();

                    this.BindAttributes(product.ProductTypeID);
                }

                ProductReviewHistoryAdmin reviewAdmin = new ProductReviewHistoryAdmin();
                string changedFields = reviewAdmin.GetChangedFields(this.itemId);

                // Remove unwanted Commas
                while (!string.IsNullOrEmpty(changedFields) && changedFields.Substring(0, 1) == ",")
                {
                    changedFields = changedFields.Substring(1);
                }

                // Disable the rate, inventory level and recurring billing settings from reviewer.                
                if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
                {
                    txtproductRetailPrice.Enabled = false;
                    txtproductSalePrice.Enabled = false;
                    txtProductQuantity.Enabled = false;
                    chkRecurringInd.Enabled = false;
                    txtStartupfee.Enabled = false;
                    txtBillevery.Enabled = false;
                    ddlBillEvery.Enabled = false;
                    txtContinueBill.Enabled = false;
                }
            }
        }

        /// <summary>
        /// Check the Category to the particular Product
        /// </summary>
        private void BindEditCategoryList()
        {
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();

            if (productCategoryAdmin != null)
            {
                DataSet productCategoryDataset = productCategoryAdmin.GetByProductID(this.itemId);
                foreach (DataRow dr in productCategoryDataset.Tables[0].Rows)
                {
                    this.PopulateEditTreeView(dr["categoryid"].ToString());
                }
            }
        }

        /// <summary>
        /// Bind control display based on properties set-Dynamically adds DropDownList for Each AttributeType
        /// </summary>
        /// <param name="typeId">Product Type Id.</param>
        private void Bind(int typeId)
        {
            this.productTypeId = typeId;
            StringBuilder attributeNames = new StringBuilder();

            // Bind Attributes
            ProductAdmin adminAccess = new ProductAdmin();

            DataSet MyDataSet = adminAccess.GetAttributeTypeByProductTypeID(this.productTypeId);

            // Repeat until Number of Attributetypes for this Product
            foreach (DataRow dr in MyDataSet.Tables[0].Rows)
            {
                // Get all the Attribute for this Attribute
                DataSet attributeDataSet = new DataSet();
                attributeDataSet = adminAccess.GetByAttributeTypeID(int.Parse(dr["attributetypeid"].ToString()));

                DataView dv = new DataView(attributeDataSet.Tables[0]);

                // Concat Attributes Names
                if (attributeNames.Length > 0)
                {
                    attributeNames.Append(" and ");
                }

                attributeNames.Append(dr["Name"].ToString());

                // Create Instance for the DropDownlist
                System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                lstControl.ID = "lstAttribute" + dr["AttributeTypeId"].ToString();

                ListItem li = new ListItem(dr["Name"].ToString(), "0");
                li.Selected = true;
                dv.Sort = "DisplayOrder ASC";
                lstControl.DataSource = dv;
                lstControl.DataTextField = "Name";
                lstControl.DataValueField = "AttributeId";
                lstControl.DataBind();
                lstControl.Items.Insert(0, li);

                if (!Convert.ToBoolean(dr["IsPrivate"]))
                {
                    // Add the Control to Place Holder
                    ControlPlaceHolder.Controls.Add(lstControl);

                    RequiredFieldValidator FieldValidator = new RequiredFieldValidator();
                    FieldValidator.ID = "Validator" + dr["AttributeTypeId"].ToString();
                    FieldValidator.ControlToValidate = "lstAttribute" + dr["AttributeTypeId"].ToString();
                    FieldValidator.ErrorMessage = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSelect").ToString(), dr["Name"]);
                    FieldValidator.Display = ValidatorDisplay.Dynamic;
                    FieldValidator.CssClass = "Error";
                    FieldValidator.InitialValue = "0";

                    ControlPlaceHolder.Controls.Add(FieldValidator);
                    Literal lit1 = new Literal();
                    lit1.Text = "&nbsp;&nbsp;";
                    ControlPlaceHolder.Controls.Add(lit1);
                }
            }

            if (MyDataSet.Tables[0].Rows.Count == 0)
            {
                DivAttributes.Visible = false;
                pnlquantity.Visible = true;
                lblProductTypeHint.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "HintTextSimpleProductType").ToString();
            }
            else
            {
                DivAttributes.Visible = true;
                pnlquantity.Visible = false;
                lblProductTypeHint.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "HintTextComplexProductType").ToString() + attributeNames;
            }
        }

        /// <summary>
        /// Binds the Sku Attributes for this Product
        /// </summary>
        /// <param name="typeId">Product type Id</param>
        private void BindAttributes(int typeId)
        {
            SKUAdmin adminSKU = new SKUAdmin();
            ProductAdmin adminAccess = new ProductAdmin();

            if (ViewState["productSkuId"] != null)
            {
                // Get SKUID from the ViewState
                DataSet SkuDataSet = adminSKU.GetBySKUId(int.Parse(ViewState["productSkuId"].ToString()));
                DataSet MyDataSet = adminAccess.GetAttributeTypeByProductTypeID(typeId);

                foreach (DataRow dr in MyDataSet.Tables[0].Rows)
                {
                    foreach (DataRow Dr in SkuDataSet.Tables[0].Rows)
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + dr["AttributeTypeId"].ToString());

                        if (lstControl != null)
                        {
                            lstControl.SelectedValue = Dr["Attributeid"].ToString();
                        }
                    }
                }
            }
        }
        #endregion


        #region Znode version 7.2.2 Send Mail

        /// <summary>
        /// Znode version 7.2.2 
        /// This function will send  mail notification to site admin.
        /// For approval of new added product by Mall Admin.
        /// </summary>
        /// <param name="productId">string productId to send productId in email</param>
        /// <param name="productName">string productName to send productName in email</param>
        protected void SendMailByVendor(string productId, string productName)
        {
            //Get vendor Name
            string vendorName = GetVendorName();

            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;

            //Get Vendor EmailId
            string senderEmail = GetVendorEmailId();

            //Subject is set from global resources of admin.
            string subject = productName + " " + HttpContext.GetGlobalResourceObject("CommonCaption", "SubjectProductAddedByVendor");
            string receiverEmail = ZNodeConfigManager.SiteConfig.AdminEmail;
            string currentCulture = string.Empty;
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;
            string shortDesc = txtShortDesc.Text;

            // Template selection
            currentCulture = ZNodeCatalogManager.CultureInfo;
            defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ProductAddedByVendor.html"));

            templatePath = (!string.IsNullOrEmpty(currentCulture))
                ? Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ProductAddedByVendor_" + currentCulture + ".html")
                : Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ProductAddedByVendor.html"));

            // Check the default template file existence.
            if (!File.Exists(defaultTemplatePath))
            {
                return;
            }
            else
            {
                // If language specific template not available then load default template.
                templatePath = defaultTemplatePath;
            }

            StreamReader rw = new StreamReader(templatePath);
            string messageText = rw.ReadToEnd();

            Regex rx1 = new Regex("#PRODUCTID#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, productId);

            Regex rx2 = new Regex("#PRODUCTNAME#", RegexOptions.IgnoreCase);
            messageText = rx2.Replace(messageText, productName);

            Regex rx3 = new Regex("#SHORTDESCRIPTION#", RegexOptions.IgnoreCase);
            messageText = rx3.Replace(messageText, shortDesc);

            Regex rx4 = new Regex("#VENDORNAME#", RegexOptions.IgnoreCase);
            messageText = rx4.Replace(messageText, vendorName);

            Regex rx5 = new Regex("#VENDOREMAIL#", RegexOptions.IgnoreCase);
            messageText = rx5.Replace(messageText, senderEmail);

            Regex RegularExpressionLogo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
            messageText = RegularExpressionLogo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

            try
            {
                // Send mail
                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(receiverEmail, senderEmail, string.Empty, subject, messageText, true);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralMessage, receiverEmail, Request.UserHostAddress.ToString(), null, ex.Message, null);
            }
        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function will return vendor name for sending mail to vendor.
        /// </summary>
        /// <returns>vendorName for mail notification</returns>      
        private string GetVendorName()
        {
            string vendorName = string.Empty;
            AccountService accountService = new AccountService();
            int accountID = 0;

            if (int.TryParse((ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount) as ZNodeUserAccount).AccountID.ToString(), out accountID))
            {
                if (accountID > 0)
                {
                    AddressService addressService = new AddressService();
                    TList<Address> addressList = addressService.GetByAccountID(accountID);
                    if (!addressList.Equals(null) && addressList.Count > 0)
                    {
                        vendorName = addressList[0].FirstName + " " + addressList[0].LastName;
                    }
                }
            }

            return vendorName;
        }

        /// <summary>
        /// Znode version 7.2.2
        /// This function will return vendor EmailId for sending mail to vendor.
        /// </summary>
        /// <returns>EmailId for mail notification</returns>
        private string GetVendorEmailId()
        {
            string vendorEmailId = string.Empty;
            SupplierService supplierService = new SupplierService();
            ProductService productService = new ProductService();
            Product product = productService.GetByProductID(productId);

            if (!product.Equals(null) && !product.SupplierID.Equals(null) && product.SupplierID > 0)
            {
                vendorEmailId = supplierService.GetBySupplierID(int.Parse(product.SupplierID.ToString())).NotificationEmailID;
            }

            return vendorEmailId;
        }
        #endregion
    }
}