﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Data;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_Product_View class.
    /// </summary>
    public partial class Admin_Secure_Product_View : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId;
        private string mode = string.Empty;
        private string editPageLink = "add.aspx?itemid=";
        private string listPageLink = "~/MallAdmin/Secure/product/list.aspx";
        private string viewProductReviewPageLink = "~/MallAdmin/Secure/Product/ViewProductReview.aspx?itemid=";
        private string rejectProductPageLink = "~/MallAdmin/Secure/Product/RejectProduct.aspx?itemid=";
        private string acceptProductPageLink = "~/MallAdmin/Secure/Product/AcceptProduct.aspx?itemid=";
        private string AddSKULink = "~/MallAdmin/Secure/product/add_sku.aspx?itemid=";
        private string addBundleProductsLink = "addbundleproduct.aspx?itemid=";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        private bool _HasAttributes = false;
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether product has attributes.
        /// </summary>
        public bool HasAttributes
        {
            get { return this._HasAttributes; }
            set { this._HasAttributes = value; }
        }

        /// <summary>
        /// Gets or sets a value of Product ProductTypeID.
        /// </summary>
        private int ProductTypeID
        {
            get
            {
                int value;
                int.TryParse(ViewState["ProductTypeID"].ToString(), out value);
                return value;
            }

            set
            {
                ViewState["ProductTypeID"] = value;
            }
        }

        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get mode value from querystring        
            if (Request.Params["mode"] != null)
            {
                this.mode = HttpUtility.UrlDecode(this.encryption.DecryptData(Request.Params["mode"].ToString()));
            }

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(HttpUtility.UrlDecode(this.encryption.DecryptData(Request.Params["itemid"].ToString())));
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                if (this.itemId > 0)
                {
                    this.BindViewData();

                    // Bind Grid - Inventory
                    this.BindSKU();
                }
                else
                {
                    throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductNotFound").ToString()); ;
                }

                this.ResetTab();

                UserStoreAccess uss = new UserStoreAccess();
				if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer || uss.UserType == Znode.Engine.Common.ZNodeUserType.ReviewerManager)
                {
                    btnTopAccept.Visible = true;
                    btnTopReject.Visible = true;
                    btnBottomAccept.Visible = true;
                    btnBottomReject.Visible = true;
                }
                else
                {
                    // Hide the reivew history from Vendor
                    divReviewHistory.Visible = true;
                    btnTopAccept.Visible = false;
                    btnTopReject.Visible = false;
                    btnBottomAccept.Visible = false;
                    btnBottomReject.Visible = false;
                }

                Session.Remove("itemid");
            }

            // Bind the Attribute List
            this.BindSkuAttributes();
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Redirecting to Product Edit Page
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnTopEditProduct_Click(object sender, EventArgs e)
        {
            // Clear the Add Product Session
            Session["stepid"] = null;
            Session["itemid"] = null;
            Response.Redirect(this.editPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Back Button Click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listPageLink);
        }

        /// <summary>
        /// Add New product sku button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddSKU_Click(object sender, EventArgs e)
        {
            ZNode.Libraries.Admin.ProductAdmin prodAdmin = new ProductAdmin();
            Product product = prodAdmin.GetByProductId(this.itemId);
            Response.Redirect(this.AddSKULink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())) + "&typeid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(product.ProductTypeID.ToString())));
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            ProductAdmin _adminAccess = new ProductAdmin();
            DataSet ds = _adminAccess.GetProductDetails(this.itemId);

            // Check for Number of Rows
            if (ds.Tables[0].Rows.Count != 0)
            {
                // Check For Product Type
                this.ProductTypeID = int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString());
            }

            // For Attribute value.
            string Attributes = String.Empty;

            // GetAttribute for this ProductType
            DataSet MyAttributeTypeDataSet = _adminAccess.GetAttributeTypeByProductTypeID(this.ProductTypeID);

            foreach (DataRow MyDataRow in MyAttributeTypeDataSet.Tables[0].Rows)
            {
                System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());
                lstControl.SelectedIndex = 0;
            }

            this.BindSKU();
        }

        protected void GvReviewHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "View")
            {
                Response.Redirect(this.viewProductReviewPageLink + this.GetEncryptId(this.itemId) + "&historyid=" + this.GetEncryptId(e.CommandArgument.ToString()));
            }
        }

        protected void BtnReject_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.rejectProductPageLink + this.GetEncryptId(this.itemId));
        }

        protected void BtnAccept_Click(object sender, EventArgs e)
        {
            this.ApproveProduct();
        }

        protected void GvReviewHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvReviewHistory.PageIndex = e.NewPageIndex;
            this.BindReviewHistory();
        }

        /// <summary>
        /// Add New bundle button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddBundleProducts_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addBundleProductsLink + this.GetEncryptId(this.itemId));
        }

        #endregion

        #region Events for Bundle Product Grid
        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxBundleProductGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                ProductService _prodService = new ProductService();
                ProductAdmin _prodAdmin = new ProductAdmin();
                Product _prodParentEntity = _prodAdmin.GetByProductId(this.itemId);
                bool Check = false;

                if (e.CommandName == "RemoveItem")
                {
                    ParentChildProductAdmin _parentChildProdAdmin = new ParentChildProductAdmin();

                    Check = _parentChildProdAdmin.Delete(int.Parse(e.CommandArgument.ToString()));

                    if (Check)
                    {
                        this.BindBundleProducts();
                    }
                }
            }
        }

        /// <summary>
        /// Bundle Product Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxBundleProductGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxBundleProductGrid.PageIndex = e.NewPageIndex;
            this.BindBundleProducts();
        }
        #endregion

        #region InventoryDisplay Grid related Methods
        /// <summary>
        /// Event triggered when a command button is clicked on the grid (InventoryDisplay Grid)
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridInventoryDisplay_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandArgument.ToString() == "page")
            {
            }
            else
            {
                if (e.CommandName == "Edit")
                {
                    // Redirect Edit SKUAttrbute Page
                    Response.Redirect(this.AddSKULink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())) + "&skuid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(e.CommandArgument.ToString())) + "&typeid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.ProductTypeID.ToString())));
                }
                else if (e.CommandName == "Delete")
                {
                    // Delete SKU and SKU Attribute
                    SKUAdmin _AdminAccess = new SKUAdmin();

                    int skuId = int.Parse(e.CommandArgument.ToString());
                    TList<SKU> skuList = _AdminAccess.GetByProductID(this.itemId);

                    // If product has only one SKU then do not delete.
                    if (skuList.Count == 1)
                    {
                        lblSkuErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCouldnotdeletelastsku").ToString();
                        return;
                    }

                    bool check = _AdminAccess.Delete(skuId);
                    if (check)
                    {
                        _AdminAccess.DeleteBySKUId(skuId);
                    }
                }
            }
        }

        /// <summary>
        /// Event triggered when the Grid Row is Deleted
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridInventoryDisplay_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Event triggered when the grid(Inventory) page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridInventoryDisplay_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGridInventoryDisplay.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        #endregion

        #region Helper Functions
        /// <summary>
        /// Get the product name by product Id
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <returns>Return the name of the product</returns>
        protected string GetProductName(object productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            return productAdmin.GetByProductId(Convert.ToInt32(productId)).Name;
        }

        /// <summary>
        /// Get the details product is active 
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <returns>Returns Boolean value</returns>
        protected bool GetProductIsActive(object productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            return productAdmin.GetByProductId(Convert.ToInt32(productId)).ActiveInd;
        }

        /// <summary>
        /// Get SKU Quantity method
        /// </summary>
        /// <param name="skuId">The value of SKU Id</param>
        /// <returns>Returns the SKU Quantity</returns>
        protected string GetSkuQuantity(int skuId)
        {
            ZNode.Libraries.DataAccess.Service.SKUService ss = new ZNode.Libraries.DataAccess.Service.SKUService();
            return SKUAdmin.GetQuantity(ss.GetBySKUID(skuId)).ToString();
        }

        /// <summary>
        /// Get Sku Reorder Level method
        /// </summary>
        /// <param name="skuId">The value of SKU Id</param>
        /// <returns>Returns the SKU Reorder level</returns>
        protected string GetSkuReorderLevel(int skuId)
        {
            ZNode.Libraries.DataAccess.Service.SKUService ss = new ZNode.Libraries.DataAccess.Service.SKUService();
            return SKUAdmin.GetInventory(ss.GetBySKUID(skuId)).ReOrderLevel.ToString();
        }

        /// <summary>
        /// Format the reason. If empty then return N/A
        /// </summary>
        /// <param name="reason">Reason object string</param>
        /// <returns>Returns the formatted reason.</returns>
        protected string FormatReason(object reason)
        {
            if (reason != null)
            {
                return reason.ToString() == string.Empty ? "N/A" : reason.ToString();
            }
            else
            {
                return "N/A";
            }
        }

        /// <summary>
        /// To get the encrypted productId in the grid
        /// </summary>
        /// <param name="productId">Product Id to encrypt.</param>
        /// <returns>Returns the formatted product Id</returns>
        private string GetEncryptId(object productId)
        {
            return HttpUtility.UrlEncode(this.encryption.EncryptData(productId.ToString()));
        }

        /// <summary>
        /// Display the selected node in label.
        /// </summary>
        private void DisplaySelectedCategories()
        {
            StringBuilder categories = new StringBuilder();
            categories.Append("<B>Current Categories</B><BR>");
            TreeNodeCollection nodeCollection = CategoryTreeView.CheckedNodes;
            if (nodeCollection.Count > 0)
            {
                foreach (TreeNode node in nodeCollection)
                {
                    categories.Append(node.Text + "<BR>");
                }
            }
            else
            {
                categories.Append("(Not associated with any category)");
            }

            lblCategories.Text = categories.ToString();
        }

        /// <summary>
        /// Bind the review history
        /// </summary>
        private void BindReviewHistory()
        {
            // Add entry to Product Review history table.
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            TList<ProductReviewHistory> productReviewHistoryList = productReviewHistoryAdmin.GetByProductID(this.itemId);
            productReviewHistoryList.Sort("ProductReviewHistoryID DESC");

            gvReviewHistory.DataSource = productReviewHistoryList;
            gvReviewHistory.DataBind();
        }

        /// <summary>
        /// Populates a treeview with store categories for a Product
        /// </summary>
        /// <param name="selectedCategoryId">Selected Category Id</param>
        private void PopulateEditTreeView(string selectedCategoryId)
        {
            foreach (TreeNode treeNode in CategoryTreeView.Nodes)
            {
                if (treeNode.Value.Equals(selectedCategoryId))
                {
                    treeNode.Checked = true;
                }

                this.RecursivelyEditPopulateTreeView(treeNode, selectedCategoryId);
            }
        }

        /// <summary>
        /// Recursively populate a particular node with it's children for a product
        /// </summary>
        /// <param name="childNodes">Child Nodes</param>
        /// <param name="selectedCategoryid">Selected category Id</param>
        private void RecursivelyEditPopulateTreeView(TreeNode childNodes, string selectedCategoryid)
        {
            foreach (TreeNode CNodes in childNodes.ChildNodes)
            {
                if (CNodes.Value.Equals(selectedCategoryid))
                {
                    CNodes.Checked = true;
                    this.RecursivelyExpandTreeView(CNodes);
                }

                this.RecursivelyEditPopulateTreeView(CNodes, selectedCategoryid);

                CategoryTreeView.Enabled = false;
            }
        }

        /// <summary>
        /// Populates a treeview with store categories
        /// </summary>
        /// <param name="categoryList">List of categories.</param>
        private void PopulateAdminTreeView(List<string> categoryList)
        {
            CategoryHelper categoryHelper = new CategoryHelper();
            System.Data.DataSet ds = categoryHelper.GetNavigationItems(ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID);

            // Add the hierarchical relationship to the dataset
            ds.Relations.Add("NodeRelation", ds.Tables[0].Columns["CategoryNodeId"], ds.Tables[0].Columns["ParentCategoryNodeId"], false);

            foreach (DataRow dataRow in ds.Tables[0].Rows)
            {
                if (dataRow.IsNull("ParentCategoryNodeID"))
                {
                    if ((bool)dataRow["VisibleInd"])
                    {
                        // Create new tree node
                        TreeNode treeNode = new TreeNode();
                        treeNode.Text = dataRow["Name"].ToString();
                        treeNode.Value = dataRow["CategoryId"].ToString();

                        string categoryId = dataRow["CategoryId"].ToString();

                        // Add Root Node to Category Tree view
                        CategoryTreeView.Nodes.Add(treeNode);

                        if (categoryList.Contains(categoryId))
                        {
                            treeNode.Selected = true;
                            treeNode.Checked = true;

                            treeNode.Expand();

                            this.RecursivelyExpandTreeView(treeNode);
                        }

                        this.RecursivelyPopulateTreeView(dataRow, treeNode, categoryList);
                    }
                }
            }
        }

        /// <summary>
        /// Recursively populate a particular node with it's children
        /// </summary>
        /// <param name="dataRow">Data Rows that contains the data</param>
        /// <param name="parentNode">Parent Tree Node</param>
        /// <param name="categoryList">List of categories</param>
        private void RecursivelyPopulateTreeView(DataRow dataRow, TreeNode parentNode, List<string> categoryList)
        {
            foreach (DataRow childRow in dataRow.GetChildRows("NodeRelation"))
            {
                if ((bool)childRow["VisibleInd"])
                {
                    // Create new tree node
                    TreeNode treeNode = new TreeNode();
                    treeNode.Text = childRow["Name"].ToString();
                    treeNode.Value = childRow["CategoryId"].ToString();

                    string categoryId = childRow["CategoryId"].ToString();

                    parentNode.ChildNodes.Add(treeNode);

                    if (categoryList.Contains(categoryId))
                    {
                        treeNode.Checked = true;
                        treeNode.Expand();

                        this.RecursivelyExpandTreeView(treeNode);
                    }

                    this.RecursivelyPopulateTreeView(childRow, treeNode, categoryList);
                }
            }
        }

        /// <summary>
        /// Recursively expands parent nodes of a selected tree node
        /// </summary>
        /// <param name="nodeItem">Node item to expand.</param>
        private void RecursivelyExpandTreeView(TreeNode nodeItem)
        {
            if (nodeItem.Parent != null)
            {
                nodeItem.Parent.ExpandAll();
                this.RecursivelyExpandTreeView(nodeItem.Parent);
            }
            else
            {
                nodeItem.Expand();
                return;
            }
        }

        /// <summary>
        /// This will automatically pre-select the tab according the query string value(mode)
        /// </summary>
        private void ResetTab()
        {
            switch (this.mode.ToLower())
            {
                case "alternateimages":
                    tabProductDetails.ActiveTab = pnlAlternateImages;
                    break;
                case "bundle":
                    tabProductDetails.ActiveTab = pnlBundleProduct;
                    break;
                case "options":
                    tabProductDetails.ActiveTab = pnlProductOptions;
                    break;
                case "tagging":
                    tabProductDetails.ActiveTab = tabpnlTags;
                    break;
				case "tags":
					tabProductDetails.ActiveTab = pnlTag;
                    break; 
                case "marketing":
                    tabProductDetails.ActiveTab = pnlMarketing;
                    break;
                case "inventory":
                    tabProductDetails.ActiveTab = pnlManageinventory;
                    break;
                default:
                    tabProductDetails.ActiveTab = pnlGeneral;
                    break;
            }
        }

        /// <summary>
        /// Returns a Format Weight string
        /// </summary>
        /// <param name="weight">Prdocuct weight value to format.</param>
        /// <returns>Returns the formatted weight value with unit.</returns>
        private string FormatProductWeight(object weight)
        {
            if (weight == null)
            {
                return string.Empty;
            }
            else
            {
                if (Convert.ToDecimal(weight.ToString()) == 0)
                {
                    return string.Empty;
                }
                else
                {
                    return weight.ToString() + " " + ZNodeConfigManager.SiteConfig.WeightUnit;
                }
            }
        }

        /// <summary>
        /// Format the Price of a Product
        /// </summary>
        /// <param name="price">Price value to format.</param>
        /// <returns>Returns the formatted price value.</returns>
        private string FormatPrice(object price)
        {
            if (price == null)
            {
                return string.Empty;
            }
            else
            {
                if (Convert.ToDecimal(price) == 0)
                {
                    return string.Empty;
                }
                else
                {
                    return string.Format("{0:c}", price);
                }
            }
        }

        /// <summary>
        /// Validate for Null Values and return a Boolean Value
        /// </summary>
        /// <param name="inventoryDisplay">Product Inventory display value</param>
        /// <returns>Returns true if product inventory if enabled else false.</returns>
        private bool DisplayVisible(object inventoryDisplay)
        {
            if (inventoryDisplay == DBNull.Value)
            {
                return false;
            }
            else
            {
                if (Convert.ToInt32(inventoryDisplay) == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Return cross-mark image path
        /// </summary>
        /// <returns>Returns the diabled check mark image.</returns>
        private string SetCheckMarkImage()
        {
            return ZNode.Libraries.Admin.Helper.GetCheckMark(false);
        }

        /// <summary>
        /// Approve the product state.
        /// </summary>
        private void ApproveProduct()
        {
            Response.Redirect(this.acceptProductPageLink + this.GetEncryptId(this.itemId), false);
        }
        #endregion

        #region Bind Product View
        /// <summary>
        /// Bind Bundle Products
        /// </summary>
        private void BindBundleProducts()
        {
            ParentChildProductAdmin parentChildProduct = new ParentChildProductAdmin();
            TList<ParentChildProduct> parentChildProductList = parentChildProduct.GetParentChildProductByProductId(this.itemId);
            uxBundleProductGrid.DataSource = parentChildProductList;
            uxBundleProductGrid.DataBind();

            // Hide the Add button from Reviewer.
            UserStoreAccess uss = new UserStoreAccess();
			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
            {
                AddBundleProducts.Visible = false;
            }
        }

        /// <summary>
        /// Binding Product Values into label Boxes
        /// </summary>
        private void BindViewData()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin productAdmin = new ProductAdmin();
            Product product = productAdmin.GetByProductId(this.itemId);
			int Count = 0;
            DataSet ds = productAdmin.GetProductDetails(this.itemId);

            // Check for Number of Rows
            if (ds.Tables[0].Rows.Count != 0)
            {
                // Checking the user profile for roles and permission and then loading the data based on roles
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (!UserStoreAccess.CheckUserRoleInProduct(profiles, this.itemId))
                {
                    Response.Redirect("List.aspx", true);
                }

                // Bind ProductType,Manufacturer,Supplier
                lblProdType.Text = ds.Tables[0].Rows[0]["producttype name"].ToString();

                // Check For Product Type
                this.ProductTypeID = int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString());

				Count = productAdmin.GetAttributeCount(int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString()));
				var bundleProductList = DataRepository.ParentChildProductProvider.GetByChildProductID(this.itemId);

				// If the attribute count is not zero, then its not default product type 
				// So we will hide the bundle tab, if the product doesn't have default product type
				if (Count != 0 || bundleProductList.Count > 0)
				{
					pnlBundleProduct.Enabled = false;
				}
				 
            }

            if (product != null)
            {
                VendorProductID.Text = string.Format(VendorProductID.Text, product.ProductNum);

                ZNode.Libraries.DataAccess.Service.SupplierService supplierService = new ZNode.Libraries.DataAccess.Service.SupplierService();
                Supplier supplier = supplierService.GetBySupplierID(product.SupplierID.GetValueOrDefault(0));
                if (product.AccountID != null)
                {
                    // Get Vendor Name
                    AccountAdmin accountAdmin = new AccountAdmin();
                    Address address = accountAdmin.GetDefaultBillingAddress(Convert.ToInt32(product.AccountID));
                    if (address != null)
                    {
                        VendorName.Text = string.Format(VendorName.Text, address.FirstName + " " + address.LastName);
                    }
                }
                else
                {
                    VendorName.Visible = false;
                }

                // General Information
                lblProdName.Text = product.Name;
                lblTitle.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource","TitleMallProductDetails").ToString(), product.Name);
                lblProductDescription.Text = Server.HtmlDecode(product.Description);
                lblProductDetails.Text = Server.HtmlDecode(product.FeaturesDesc);
                lblShippingDescription.Text = Server.HtmlDecode(product.AdditionalInformation);

                ZNodeImage znodeImage = new ZNodeImage();
                ItemImage.ImageUrl = znodeImage.GetImageHttpPathMedium(product.ImageFile);

                // Product Attributes
                if (product.RetailPrice.HasValue)
                {
                    lblRetailPrice.Text = this.FormatPrice(product.RetailPrice);
                }

                imgShowProduct.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(product.ActiveInd);
                lblAffiliateUrl.Text = product.AffiliateUrl;

                lblshipweight.Text = this.FormatProductWeight(product.Weight);

                if (product.Height.HasValue)
                {
                    lblHeight.Text = product.Height.Value.ToString("N2") + " " + ZNodeConfigManager.SiteConfig.DimensionUnit;
                }

                if (product.Width.HasValue)
                {
                    lblWidth.Text = product.Width.Value.ToString("N2") + " " + ZNodeConfigManager.SiteConfig.DimensionUnit;
                }

                if (product.Length.HasValue)
                {
                    lblLength.Text = product.Length.Value.ToString("N2") + " " + ZNodeConfigManager.SiteConfig.DimensionUnit;
                }

                // Bind Inventory Options
                SKUAdmin skuAdmin = new SKUAdmin();
                TList<SKU> skuList = skuAdmin.GetByProductID(product.ProductID);

                if (skuList != null && skuList.Count > 0)
                {
                    SKUInventory skuInventory = skuAdmin.GetSkuInventoryBySKU(skuList[0].SKU);

                    if (skuInventory != null)
                    {
                        lblQuantityOnHand.Text = skuInventory.QuantityOnHand.GetValueOrDefault(0).ToString();
                        VendorProductID.Text = string.Format(VendorProductID.Text, skuInventory.SKU);
                    }

                    // Release the Resources
                    skuList.Dispose();
                }

                // Inventory Options
                if (product.TrackInventoryInd.HasValue && product.AllowBackOrder.HasValue)
                {
                    if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value == false)
                    {
                        imgTrackInventory.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                        imgAllowBackOrder.ImageUrl = this.SetCheckMarkImage();
                        imgDontTrackInventory.ImageUrl = this.SetCheckMarkImage();
                    }
                    else if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value)
                    {
                        imgTrackInventory.ImageUrl = this.SetCheckMarkImage();
                        imgAllowBackOrder.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                        imgDontTrackInventory.ImageUrl = this.SetCheckMarkImage();
                    }
                    else if ((product.TrackInventoryInd.Value == false) && (product.AllowBackOrder.Value == false))
                    {
                        imgTrackInventory.ImageUrl = this.SetCheckMarkImage();
                        imgAllowBackOrder.ImageUrl = this.SetCheckMarkImage();
                        imgDontTrackInventory.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                    }
                }
                else
                {
                    imgTrackInventory.ImageUrl = this.SetCheckMarkImage();
                    imgAllowBackOrder.ImageUrl = this.SetCheckMarkImage();
                    imgDontTrackInventory.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                }

                // Checkout Optons
                imgTangible.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(Convert.ToBoolean(product.FreeShippingInd));
                lblWeight.Text = this.FormatProductWeight(product.Weight);
                lblHandling.Text = this.FormatPrice(product.ShippingRate);
                imgRecurring.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(product.RecurringBillingInd);
                lblStartupFee.Text = this.FormatPrice(product.RecurringBillingInitialAmount);
                if (product.RecurringBillingInd)
                {
                    lblBillingPeriod.Text = product.RecurringBillingPeriod;
                }

                // Inventory Setting - Stock Messages
                lblInStockMessage.Text = product.InStockMsg;
                lblOutofStockMessage.Text = product.OutOfStockMsg;
                lblBackOrderMessage.Text = product.BackOrderMsg;

                ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
                DataTable productCategoryTable = productCategoryAdmin.GetByProductID(this.itemId).Tables[0];

                List<string> categoryList = new List<string>();
                foreach (DataRow row in productCategoryTable.Rows)
                {
                    categoryList.Add(row["CategoryID"].ToString());
                }

                // Bind Categories.
                this.PopulateAdminTreeView(categoryList);

                this.DisplaySelectedCategories();

                // Bind the review history.
                this.BindReviewHistory();

                // Bind Grid - Bundle Products
                this.BindBundleProducts();

                // Set the buttons enabled state.
                if (product.ReviewStateID != null)
                {
                    // Product already approved state. Disable the Approve button
                    if (Convert.ToInt32(product.ReviewStateID) == (int)ZNodeProductReviewState.Approved)
                    {
                        btnTopAccept.Enabled = btnBottomAccept.Enabled = false;
                    }

                    // Product already declined state. Disable the Reject button
                    if (Convert.ToInt32(product.ReviewStateID) == (int)ZNodeProductReviewState.Declined)
                    {
                        btnTopReject.Enabled = btnBottomReject.Enabled = false;
                    }
                }
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductNotFound").ToString());
            }
        }

        /// <summary>
        /// Bind Attributes List.
        /// </summary>
        private void BindSkuAttributes()
        {
            ProductAdmin adminAccess = new ProductAdmin();

            DataSet MyDataSet = adminAccess.GetAttributeTypeByProductTypeID(this.ProductTypeID);

            // Repeats until Number of AttributeType for this Product
            foreach (DataRow dr in MyDataSet.Tables[0].Rows)
            {
                // Bind Attributes
                DataSet AttributeDataSet = adminAccess.GetAttributesByAttributeTypeIdandProductID(int.Parse(dr["attributetypeid"].ToString()), this.itemId);

                System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                lstControl.ID = "lstAttribute" + dr["AttributeTypeId"].ToString();

                ListItem li = new ListItem(dr["Name"].ToString(), "0");

                lstControl.DataSource = AttributeDataSet;
                lstControl.DataTextField = "Name";
                lstControl.DataValueField = "AttributeId";
                lstControl.DataBind();
                lstControl.Items.Insert(0, li);  
                // Add Dynamic Attribute DropDownlist in the Placeholder
                ControlPlaceHolder.Controls.Add(lstControl);

                Literal lit1 = new Literal();
                lit1.Text = "&nbsp;&nbsp;";
                ControlPlaceHolder.Controls.Add(lit1);
            }

            this._HasAttributes = MyDataSet.Tables[0].Rows.Count > 0;
            pnlSKUAttributes.Visible = MyDataSet.Tables[0].Rows.Count > 0;
        }

        /// <summary>
        /// Bind Inventory Grid
        /// </summary>
        private void BindSKU()
        {
            SKUAdmin SkuAdmin = new SKUAdmin();
            TList<SKU> skuList = SkuAdmin.GetByProductID(this.itemId);

            skuList.Sort("SKU");

            uxGridInventoryDisplay.DataSource = skuList;
            uxGridInventoryDisplay.DataBind();
        }

        /// <summary>
        /// Binds the Search data
        /// </summary>
        private void BindSearchData()
        {
            ProductAdmin adminAccess = new ProductAdmin();
            DataSet ds = adminAccess.GetProductDetails(this.itemId);

            // Check for Number of Rows
            if (ds.Tables[0].Rows.Count != 0)
            {
                // Check For Product Type
                this.ProductTypeID = int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString());
            }

            // For Attribute value.
            string Attributes = String.Empty;

            // GetAttribute for this ProductType
            DataSet MyAttributeTypeDataSet = adminAccess.GetAttributeTypeByProductTypeID(this.ProductTypeID);

            foreach (DataRow MyDataRow in MyAttributeTypeDataSet.Tables[0].Rows)
            {
                System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                if (lstControl != null)
                {
                    int selValue = int.Parse(lstControl.SelectedValue);

                    if (selValue > 0)
                    {
                        Attributes += selValue.ToString() + ",";
                    }
                }
            }

            // If Attributes length is more than zero.
            if (Attributes.Length > 0)
            {
                // Split the string
                string Attribute = Attributes.Substring(0, Attributes.Length - 1);

                if (Attribute.Length > 0)
                {
                    SKUAdmin _SkuAdmin = new SKUAdmin();
                    DataSet MyDatas = _SkuAdmin.GetBySKUAttributes(this.itemId, Attribute);

                    DataView Sku = new DataView(MyDatas.Tables[0]);
                    Sku.Sort = "SKU";
                    uxGridInventoryDisplay.DataSource = Sku;
                    uxGridInventoryDisplay.DataBind();
                }
            }
            else
            {
                this.BindSKU();
            }
        }
        #endregion
    }
}