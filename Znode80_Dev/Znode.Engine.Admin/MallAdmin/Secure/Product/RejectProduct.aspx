﻿<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/content.master" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Admin_Secure_Product_RejectProduct"
    Title="Untitled Page" CodeBehind="RejectProduct.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <asp:XmlDataSource ID="xmlRejectMessageDataSource" runat="server"></asp:XmlDataSource>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="clear: both">
        </div>
        <div class="FormView">
            <div>
                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
            </div>

            <div class="FieldStyle">
                <asp:Localize ID="ColumnPostalCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductID%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblProductID" runat="server"></asp:Label>
            </div>

            <div class="FieldStyle">
                <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnVendor%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblVendor" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnReasonForRejection%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlReason" runat="server">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRejectionDetail%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox Height="250px" Width="450px" TextMode="MultiLine" ID="txtDescription"
                    runat="server"></asp:TextBox>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="FieldStyle">
            </div>
            <div class="ValueStyle">
                <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmit" />
                <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancel" />
            </div>
        </div>
    </div>
</asp:Content>
