using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_products_list class.
    /// </summary>
    public partial class Admin_Secure_products_list : System.Web.UI.Page
    {
        #region Private Variables
        private string addPageLink = "add.aspx";
        private string previewPageLink = string.Empty;
        private string deletePageLink = "delete.aspx?itemid=";
        private string detailsPageLink = "~/MallAdmin/Secure/product/view.aspx?itemid=";
        private string portalIds = string.Empty;
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            this.previewPageLink = "http://" + UserStoreAccess.DomainName + "/product.aspx";
            this.portalIds = UserStoreAccess.GetAvailablePortals;

            if (!Page.IsPostBack)
            {
                UserStoreAccess uss = new UserStoreAccess();
				if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer || uss.UserType == Znode.Engine.Common.ZNodeUserType.ReviewerManager)
                {
                    btnApproveAll.Visible = true;
                    btnDeclineall.Visible = true;
                    ReviewerSearch.Visible = true;
                }
                else
                {
                    btnApproveAll.Visible = false;
                    btnDeclineall.Visible = false;
                    ReviewerSearch.Visible = false;
                    div2CO.Visible = false;
                }

                this.BindSearchProduct();

                Session.Remove("itemid");
            }
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.RememberOldValues();

            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchProduct();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Manage")
                {
                    Response.Redirect(this.detailsPageLink + this.GetEncryptId(e.CommandArgument));
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.deletePageLink + this.GetEncryptId(e.CommandArgument));
                }
            }
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_DataBound(object sender, EventArgs e)
        {
            UserStoreAccess uss = new UserStoreAccess();
			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer || uss.UserType == Znode.Engine.Common.ZNodeUserType.Admin || uss.UserType == Znode.Engine.Common.ZNodeUserType.ReviewerManager)
            {
                uxGrid.Columns[0].Visible = true;
                uxGrid.Columns[6].Visible = true;
                uxGrid.Columns[7].Visible = true;
            }
            else
            {
                uxGrid.Columns[0].Visible = false;
                uxGrid.Columns[6].Visible = false;
                uxGrid.Columns[7].Visible = false;
            }
        }

        /// <summary>
        /// Row Data Bound event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Dictionary<int, string> productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];

                if (productIdList != null)
                {
                    CheckBox check = (CheckBox)e.Row.Cells[0].FindControl("chkProduct") as CheckBox;
                    int id = int.Parse(e.Row.Cells[1].Text);

                    if (productIdList.ContainsKey(id) && check != null)
                    {
                        check.Checked = true;
                    }
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// AddNew Product button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddProduct_Click(object sender, EventArgs e)
        {
            Session["itemid"] = null;
            Response.Redirect(this.addPageLink);
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            Session["CHECKEDITEMS"] = null;
            this.BindSearchProduct();
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnApproveAll_Click(object sender, EventArgs e)
        {
            this.ChangeProductStatus(ZNodeProductReviewState.Approved);
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDeclineall_Click(object sender, EventArgs e)
        {
            this.ChangeProductStatus(ZNodeProductReviewState.Declined);
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtproductname.Text = string.Empty;
            txtVendor.Text = string.Empty;
            txtVendorId.Text = string.Empty;
            txtVendorproductId.Text = string.Empty;
            txtProductStatus.Value = "0";

            this.BindSearchProduct();
        }

        /// <summary>
        /// Toggle checkbox selection
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkSelectAll = sender as CheckBox;
            for (int rowIndex = 0; rowIndex <= uxGrid.Rows.Count - 1; rowIndex++)
            {
                CheckBox chkProduct = uxGrid.Rows[rowIndex].Cells[0].FindControl("chkProduct") as CheckBox;
                if (chkProduct != null)
                {
                    chkProduct.Checked = chkSelectAll.Checked;
                }
            }
        }

        #endregion       

        #region FormatPrice Helper method
        /// <summary>
        /// Returns formatted price to the grid
        /// </summary>
        /// <param name="productPrice">Product price to format.</param>
        /// <returns>Returns the formatted product price.</returns>
        protected string FormatPrice(object productPrice)
        {
            if (productPrice != null)
            {
                if (productPrice.ToString().Length > 0)
                {
                    return decimal.Parse(productPrice.ToString()).ToString("c");
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Get the products changes field
        /// </summary>
        /// <param name="currentProductId">Current Product Id</param>
        /// <returns>Returns changed fields, If there are no changes then returns empty string.</returns>
        protected string GetChangedFields(object currentProductId)
        {
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            return productReviewHistoryAdmin.GetChangedFields(Convert.ToInt32(currentProductId));
        }

        #endregion       

        #region Helper Functions
        /// <summary>
        /// Change the product status as Approved or Declined.
        /// </summary>
        /// <param name="state">ZNodeProductReviewState enum</param>    
        protected void ChangeProductStatus(ZNodeProductReviewState state)
        {
            this.RememberOldValues();

            Dictionary<int, string> productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];
            if (productIdList == null || productIdList.Count == 0)
            {
                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoProductSelected").ToString();
                return;
            }

            if (state == ZNodeProductReviewState.Approved)
            {
                ProductAdmin AdminAccess = new ProductAdmin();
                ProductReviewStateAdmin reviewstateAdmin = new ProductReviewStateAdmin();
                ProductReviewState reviewState = reviewstateAdmin.GetProductReviewStateByName(state.ToString());
                StringBuilder sb = new StringBuilder();

                // Loop through the grid values
                if (productIdList != null && reviewState != null)
                {
                    foreach (KeyValuePair<int, string> pair in productIdList)
                    {
                        // Get ProductId
                        int productId = pair.Key;
                        Product entity = AdminAccess.GetByProductId(productId);

                        // Update only if existing status is not same as current status
                        if (entity.ReviewStateID != reviewState.ReviewStateID)
                        {
                            entity.ReviewStateID = reviewState.ReviewStateID;
                            AdminAccess.Update(entity);

                            this.UpdateReviewHistory(productId, (ZNodeProductReviewState)entity.ReviewStateID, Convert.ToInt32(entity.AccountID));
                        }
                    }
                }

                // Clear the selection.
                Session["CHECKEDITEMS"] = null;

                this.BindSearchProduct();
            }
            else
            {
                // If product review state is Declined then redirect to Reject product page. List if product ID values stored in session.            
                Response.Redirect("RejectProduct.aspx?ReturnUrl=List.aspx");
            }
        }

        /// <summary>
        /// Get the thumbnail image path
        /// </summary>
        /// <param name="Imagefile">Image file name</param>
        /// <returns>Returns the thumbnail image path.</returns>
        protected string GetImagePath(string Imagefile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(Imagefile);
        }

        /// <summary>
        /// Get the encrypted productId in the grid
        /// </summary>
        /// <param name="productId">Product Id to encryption</param>
        /// <returns>Returns the encrypted product Id.</returns>
        protected string GetEncryptId(object productId)
        {
            ZNodeEncryption encryption = new ZNodeEncryption();
            return HttpUtility.UrlEncode(encryption.EncryptData(productId.ToString()));
        }

        /// <summary>
        /// Get the view page liink for the product
        /// </summary>
        /// <param name="productId">Product Id to encrypt</param>
        /// <returns>Returns the product view page link with encrypted product Id.</returns>
        protected string GetViewPageLink(object productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = new Product();
            product = productAdmin.GetByProductId(Convert.ToInt32(productId));

            return "view.aspx?itemid=" + this.GetEncryptId(productId);
        }

        /// <summary>
        /// Remember the selected list of product Id
        /// </summary>
        private void RememberOldValues()
        {
            Dictionary<int, string> productIdList = new Dictionary<int, string>();

            // Loop through the grid values
            foreach (GridViewRow row in uxGrid.Rows)
            {
                CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProduct") as CheckBox;

                // Check in the Session
                if (Session["CHECKEDITEMS"] != null)
                {
                    productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];
                }

                int id = int.Parse(row.Cells[1].Text);
                if (check.Checked)
                {
                    if (!productIdList.ContainsKey(id))
                    {
                        productIdList.Add(id, row.Cells[3].Text);
                    }
                }
                else
                {
                    productIdList.Remove(id);
                }
            }

            if (productIdList.Count > 0)
            {
                Session["CHECKEDITEMS"] = productIdList;
            }
        }

        /// <summary>
        /// Search a Product by name,productnumber,sku,manufacturer,category,producttype
        /// </summary>
        private void BindSearchProduct()
        {
            ProductAdmin prodadmin = new ProductAdmin();
            string vendorId = string.Empty;
            UserStoreAccess uss = new UserStoreAccess();

            AccountService accountService = new AccountService();
            bool isVendorIdValid = true;

			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Vendor)
            {
                TList<Account> accountList = accountService.GetByUserID((Guid)Membership.GetUser().ProviderUserKey);
                if (accountList != null && accountList.Count > 0)
                {
                    vendorId = accountList[0].AccountID.ToString();
                }
            }
            else if (!string.IsNullOrEmpty(txtVendorId.Text))
            {
                TList<Account> accountList = accountService.Find(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextExternalAccountNo").ToString(), txtVendorId.Text));
                if (accountList != null && accountList.Count > 0)
                {
                    vendorId = accountList[0].AccountID.ToString();
                }
                else
                {
                    // Given Account number doesn't found.
                    isVendorIdValid = false;
                }
            }

            DataSet ds = null;
            DataView dv = null;
            if (isVendorIdValid)
            {
                ds = prodadmin.SearchVendorProduct(Server.HtmlEncode(txtproductname.Text.Trim()), txtVendorproductId.Text.Trim(), txtVendor.Text.Trim(), vendorId, txtProductStatus.Value, txt2coid.Text);
                dv = new DataView(ds.Tables[0]);
                dv.Sort = "DisplayOrder";
            }
            else
            {
                dv = new DataView();
            }

            btnApproveAll.Enabled = btnDeclineall.Enabled = dv.Count > 0 ? true : false;
            uxGrid.DataSource = dv;
            uxGrid.DataBind(); 
        }

        /// <summary>
        /// Update the product review history
        /// </summary>
        /// <param name="productId">Product Id to update.</param>
        /// <param name="state">Updated state if the product</param>
        /// <param name="vendorId">Vendor Id to update in product history.</param>
        private void UpdateReviewHistory(int productId, ZNodeProductReviewState state, int vendorId)
        {
            // Add entry to Product Review history table.
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            ProductReviewHistory productReviewHistory = new ProductReviewHistory();
            productReviewHistory.ProductID = productId;
            productReviewHistory.VendorID = vendorId;
            productReviewHistory.LogDate = DateTime.Now;
            productReviewHistory.Username = this.Page.User.Identity.Name;
            productReviewHistory.Status = state.ToString();
            productReviewHistory.Reason = state.ToString();
            productReviewHistory.Description = state.ToString();
            if (state == ZNodeProductReviewState.Approved)
            {
                productReviewHistory.Reason = string.Empty;
                productReviewHistory.Description = string.Empty;
            }

            productReviewHistory.Reason = state.ToString();
            productReviewHistory.Description = state.ToString();
            bool isSuccess = productReviewHistoryAdmin.Add(productReviewHistory);
        }

        #endregion
    }
}