<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/content.master" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Admin_Secure_products_delete" Codebehind="delete.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5>
           <asp:Localize ID="TextPleaseConfirm" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm %>'></asp:Localize>
    </h5>
    <p>
           <asp:Localize ID="DeleteProductConfirmation" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextDeleteProductConfirmation %>'></asp:Localize> <b>
            <%=ProductName%></b>   <asp:Localize ID="DeleteProductCannotUndone" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextDeleteProductCannotUndone %>'></asp:Localize></p>
    <asp:Label CssClass="Error" ID="lblErrorMessage" runat="server" Text=''></asp:Label>
    <br />
    <br />
    <div>   
          <zn:Button runat="server"  ID="btnDelete" ButtonType="CancelButton" OnClick="BtnDelete_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete%>' CausesValidation="False" />
          <zn:Button runat="server" ID="btnCancel"  ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
 
        </div>
</asp:Content>
