<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Public_Templates_Professional_product_Product" CodeBehind="Product.ascx.cs" %>
<%@ Register Src="~/MallAdmin/Controls/Default/Product/SwatchImages.ascx" TagName="Swatches"
    TagPrefix="ZNode" %>
<%@ Register Src="~/MallAdmin/Controls/Default/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<%@ Register Src="~/MallAdmin/Controls/Default/Product/ProductTabs.ascx" TagName="ProductTabs"
    TagPrefix="ZNode" %>
<%@ Register Src="~/MallAdmin/Controls/Default/Product/ProductAddOns.ascx" TagName="ProductAddOns"
    TagPrefix="ZNode" %>
<%@ Register Src="~/MallAdmin/Controls/Default/Product/BestSellers.ascx" TagName="BestSellers" TagPrefix="ZNode" %>
<%@ Register Src="~/MallAdmin/Controls/Default/Product/ProductPrice.ascx" TagName="ProductPrice" TagPrefix="ZNode" %>
<%@ Register Src="~/MallAdmin/Controls/Default/Product/ProductRelated.ascx" TagName="ProductRelated" TagPrefix="ZNode" %>
<%@ Register Src="~/MallAdmin/Controls/Default/Product/CatalogImage.ascx" TagName="CatalogItemImage" TagPrefix="ZNode" %>

<div id="MiddleColumn">
    <asp:UpdatePanel ID="UpdatePnlProductDetail" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="ProductDetail">
                <div class="box">
                    <!-- Left Content -->
                    <div class="LeftContent">
                        <div class="Title">
                            <asp:Label ID="ProductTitle" Text="{0}" EnableViewState="false" runat="server"></asp:Label>
                        </div>
                        <div class="Description">
                            <asp:Label ID="ProductDescription" EnableViewState="false" runat="server"></asp:Label>
                        </div>
                        <div class="Label">
                            <asp:Localize ID="Item" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleItem %>'></asp:Localize>:&nbsp;#<asp:Label
                                ID="lblProductId" runat="server" EnableViewState="false" Text="ProductID"></asp:Label>
                        </div>
                        <div id="BrandLabel" runat="server" visible="false">
                            <asp:Localize ID="Brand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBrand %>'></asp:Localize>:&nbsp;<asp:Label
                                ID="lblBrand" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <!--Ajax Tabs-->
                        <div class="Tabs">
                            <ZNode:ProductTabs ID="uxProductTabs" runat="server" />
                        </div>
                        <asp:UpdatePanel ID="UpdPnlOrderingOptions" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="OrderingOptions">
                                    <ZNode:ProductAddOns ID="uxProductAddOns" runat="server" ShowCheckBoxesVertically="true"
                                        ShowRadioButtonsVerically="true"></ZNode:ProductAddOns>
                                    <asp:Panel ID="pnlQty" runat="server" Visible="False">
                                    </asp:Panel>
                                    <div class="StockMsg">
                                        <asp:Label ID="lblstockmessage" runat="server"></asp:Label>
                                    </div>
                                    <div class="PriceContent">
                                        <ZNode:ProductPrice ID="uxProductPrice" runat="server" Visible="true" />
                                        <asp:Image ID="FeaturedItemImage" EnableViewState="false" runat="server" Visible="False" ImageAlign="AbsMiddle" />
                                    </div>
                                    <div class="CallForPrice">
                                        <asp:Label ID="uxCallForPricing" EnableViewState="false" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCallForPricing %>' Visible="False"></asp:Label>
                                    </div>
                                    <div class="SalePrice">
                                        <asp:Literal EnableViewState="False" ID="AdditionalPrice" runat="server"></asp:Literal>
                                    </div>
                                    <div class="StatusMsg">
                                        <asp:Label ID="uxStatus" EnableViewState="False" runat="server" CssClass="StatusMsg"></asp:Label>
                                    </div>
                                    <div class="Error" id="cookiediv">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <!-- Right Content -->
                    <div class="RightContent">
                        <div class="Image">
                            <div>
                                <ZNode:CatalogItemImage ID="CatalogItemImage" runat="server" />
                                </a><div class="ProductPageNewItem">
                                    <asp:Image ID="NewItemImage" runat="server" Visible="False" />
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="pnlSwatches" runat="server">
                            <div class="ProductSwatches">
                                <span class="Text">
                                    <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleColorOptions %>'></asp:Localize></span>
                                <ZNode:Swatches ID="uxProductSwatches" IncludeProductImage="true" runat="server"
                                    ControlType="Swatches" />
                            </div>
                        </asp:Panel>
                        <div id="RelatedTabs">
                            <ajaxToolKit:TabContainer OnClientActiveTabChanged="ActiveTabChanged" runat="server"
                                ID="ProductTabs" CssClass="RelatedProductTabStyle" ScrollBars="Auto">
                                <ajaxToolKit:TabPanel ID="pnlProductRelated" runat="server">
                                    <HeaderTemplate>
                                        <span class="Text">
                                            <asp:Localize ID="lblProductRelated" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleYMAL %>'></asp:Localize>
                                        </span>
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <div class="CrossSell">
                                            <ZNode:ProductRelated ID="uxProductRelated" runat="server" ShowName="false" ShowImage="true"
                                                ShowDescription="false" />
                                        </div>
                                    </ContentTemplate>
                                </ajaxToolKit:TabPanel>

                            </ajaxToolKit:TabContainer>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
        document.cookie = 'ZNode' + escape('nothing')
        function ActiveTabChanged(sender, e) {
            PageLoadedEventHandler();
        }

        function detect() {
            document.cookie = 'ZNodeCookie';
            if (document.cookie.indexOf("ZNodeCookie") < 0) {
                alert(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorEnableCookies").ToString());
                return false;
            }
            else {
                return true;
            }
        }
    </script>

</div>
