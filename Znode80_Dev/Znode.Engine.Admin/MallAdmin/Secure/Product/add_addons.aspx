<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/edit.master" AutoEventWireup="true" ValidateRequest="false" Inherits="Znode.Engine.MallAdmin.Admin_Secure_catalog_product_add_addons" Codebehind="add_addons.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <h1>
            <asp:Label ID="lblTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleAddaProductAddOns %>'></asp:Label></h1>
        <div class="LeftFloat" style="width: 75%; text-align: left">
            <div> 
                 <asp:Localize ID="TextProductAssonstab" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextProductAddonstab %>'></asp:Localize>
            </div>
        </div>        
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <asp:Label CssClass="Error" ID="lblAddOnErrorMessage" runat="server" EnableViewState="false"
                Visible="false"></asp:Label></div>
         <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <h4 class="SubTitle">
             
             <asp:Localize ID="SubTitleAddOnSearch" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleAddOnSearch %>'></asp:Localize>
        </h4>
        <small> 
             <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextSearchforAddon %>'></asp:Localize>
        </small>
        <asp:Panel ID="pnlAddOnSearch" DefaultButton="btnAddOnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">  <asp:Localize ID="ColumnTitleName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAddonName" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">  <asp:Localize ID="ColumnTitleTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTitle %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAddOnTitle" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle"> <asp:Localize ID="ColumnSKUorProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSKUorProduct %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAddOnsku" runat="server"></asp:TextBox></span>
                    </div>
                    
                </div>
                <div class="ClearBoth">
                </div>
                <div class="LeftFloat" style="width: 83%">
                 
                    <zn:Button runat="server"  ID="btnAddOnClear" ButtonType="CancelButton" OnClick="BtnAddOnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear%>' CausesValidation="False" />
                    <zn:Button runat="server" ID="btnAddOnSearch"  ButtonType="SubmitButton" OnClick="BtnAddOnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch%>' CausesValidation="true" />
 
                </div>
                <div >
                <asp:LinkButton CssClass="AddButton" ID="btnAddAddon" CausesValidation="False"
                    Text='<%$ Resources:ZnodeAdminResource, linkNewAddon %>' runat="server" OnClick="BtnAddAddon_Click"></asp:LinkButton>
            </div>

            </div>
        </asp:Panel>
        <div class="ClearBoth"></div>
        <br />
        <h4 class="GridTitle">
              <asp:Localize ID="GridTitleProductAddOnList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleProductAddOnList %>'></asp:Localize></h4><br />
        <small>  <asp:Localize ID="AddonsAssotiats" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextSelectAddonsAssotiats %>'></asp:Localize></small>
        <!-- Update Panel for grid paging that are used to avoid the postbacks -->
        <asp:UpdatePanel ID="updPnlAddOnGrid" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="uxAddOnGrid" runat="server" CssClass="Grid" AllowPaging="True"
                    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None"
                    OnPageIndexChanging="UxAddOnGrid_PageIndexChanging" CaptionAlign="Left" Width="100%"
                    EnableSortingAndPagingCallbacks="False" PageSize="15" AllowSorting="True" PagerSettings-Visible="true"
                    OnRowCommand="UxAddOnGrid_RowCommand" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundAddOnTypes %>'>
                    <Columns>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSelect %>'  HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkProductAddon" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AddOnId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Title" HtmlEncode="false" HeaderText="Title" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>'  HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Name") %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="DisplayType" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayType %>' HeaderStyle-HorizontalAlign="Left" />
                          <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAddOnValues %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# GetAddOnValues(DataBinder.Eval(Container.DataItem, "AddOnId"))%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <uc1:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
         <div> 
            <zn:Button runat="server"  ID="btnAddSelectedAddons" ButtonType="EditButton" OnClick="BtnAddSelectedAddons_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddSelectedItem%>' CausesValidation="true" Width="150px" />
            <zn:Button runat="server" ID="btnBottomCancel"  ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="false" />
 
        </div>        
    </div>
</asp:Content>
