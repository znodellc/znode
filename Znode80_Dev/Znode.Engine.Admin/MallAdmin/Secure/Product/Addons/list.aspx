<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.MallAdmin.Admin_Secure_product_addons_list" Title="Untitled Page" CodeBehind="list.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Localize ID="TextAddOns" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAddOns%>'></asp:Localize></h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnAddCategory_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCreateNewAddOn%>' ID="btnAddCategory" CausesValidation="False" />
        </div>
        <div class="ClearBoth">
            <p>
                <asp:Localize ID="SubTextAddOns" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextAddOns%>'></asp:Localize></p>
        </div>
        <br />
        <h4 class="SubTitle">
            <asp:Localize ID="SubTitleAddOns" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleAddOns%>'></asp:Localize></h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName%>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAddonName" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitle%>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAddOnTitle" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleSkuorPart" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSkuorPart%>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtsku" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle" runat="server" id="lblLocale">
                            <asp:Localize ID="ColumnTitleLocale" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLocale%>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlLocales" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Value="" Text='<%$ Resources:ZnodeAdminResource, DropdownTextAll %>'></asp:ListItem>
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnClear" CausesValidation="False" />
                    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' ID="btnSearch" />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">
            <asp:Localize ID="GridTitleAddOnList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleAddOnList%>'></asp:Localize></h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" EnableSortingAndPagingCallbacks="False"
            PageSize="25" AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundAddOnTypes%>'>
            <Columns>
                <asp:BoundField DataField="AddOnId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID%>' HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="Title" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitle%>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID%>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='view.aspx?itemid=<%# DataBinder.Eval(Container.DataItem, "AddOnId").ToString()%>'>
                            <%# Eval("Name") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder%>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsOptional%>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "OptionalInd").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField CommandName="Manage" Text='<%$ Resources:ZnodeAdminResource, LinkManage%>' ButtonType="Link">
                    <ControlStyle CssClass="Button" />
                </asp:ButtonField>
                <asp:ButtonField CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete%>' ButtonType="Link">
                    <ControlStyle CssClass="Button" />
                </asp:ButtonField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
