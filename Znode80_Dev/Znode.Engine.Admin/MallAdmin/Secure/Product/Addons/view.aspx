<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.MallAdmin.Admin_Secure_product_addons_view" ValidateRequest="false" Codebehind="view.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Display">
        <div class="LeftFloat" style="width: 50%; text-align: left">
            <h1>
                <asp:Localize ID="TitleProductAddOn" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleProductAddOn%>'></asp:Localize>
                <asp:Label ID="lblTitle" runat="server" Text="Label"></asp:Label></h1>
        </div>
        <div style="text-align: right" class="ImageButtons">
            <asp:Button ID="btneditAddon" runat="server" OnClick="EditAddOn_Click" Text="Edit Add-On" />
            <asp:Button ID="btnBack" runat="server" OnClick="BacktoAddOn_Click" Text="< Back to Add-On" />
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralInformation%>'></asp:Localize></h4>
        <div class="FieldStyle" nowrap="nowrap">
            <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnName%>'></asp:Localize></div>
        <div class="ValueStyle">
            <asp:Label ID="lblName" runat="server"></asp:Label>&nbsp;
        </div>
        <div class="FieldStyleA" nowrap="nowrap">
            <asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitle%>'></asp:Localize></div>
        <div class="ValueStyleA">
            <asp:Label ID="lblAddOnTitle" runat="server"></asp:Label>&nbsp;
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder%>'></asp:Localize></div>
        <div class="ValueStyle">
            <asp:Label ID="lblDisplayOrder" runat="server"></asp:Label></div>
        <div class="FieldStyleA">
            <asp:Localize ID="Localize5" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayType%>'></asp:Localize></div>
        <div class="ValueStyleA">
            <asp:Label ID="lblDisplayType" runat="server"></asp:Label>&nbsp;</div>
        <div class="FieldStyle">
            <asp:Localize ID="Localize6" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOptionalAddOn%>'></asp:Localize></div>
        <div class="ValueStyle">
            <img id="chkOptionalInd" runat="server" alt="" src="" /></div>
        <div class="FieldStyleA">
            <asp:Localize ID="Localize7" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLocale%>'></asp:Localize></div>
        <div class="ValueStyleA">
            <asp:Label ID="lblLocale" runat="server"></asp:Label>&nbsp;</div>
        
        <div class="ClearBoth"><br /></div>
                
        <h4 class="SubTitle">
            <asp:Localize ID="Localize8" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleInventorySettings%>'></asp:Localize></h4>
        <div class="FieldStyleImg">
            <img id="chkCartInventoryEnabled" runat="server" alt="" src='' /></div>
        <div class="ValueStyleImg">
            <asp:Localize ID="ColumnTextTrackInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextTrackInventory%>'></asp:Localize>
        </div>
        <div class="FieldStyleImgA">
            <img id="chkIsBackOrderEnabled" runat="server" alt="" src='' /></div>
        <div class="ValueStyleImgA">
            <asp:Localize ID="ColumnTextBackOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextBackOrder%>'></asp:Localize></div>
        <div class="FieldStyleImg">
            <img id="chkIstrackInvEnabled" runat="server" alt="" src="" /></div>
        <div class="ValueStyleImg">
            <asp:Localize ID="ColumnNoTrackInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnNoTrackInventory%>'></asp:Localize></div>
        <div class="FieldStyleA">
            <asp:Localize ID="ColumnInStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnInStockMessage%>'></asp:Localize></div>
        <div class="ValueStyleA">
            <asp:Label ID="lblInStockMsg" runat="server"></asp:Label>&nbsp;</div>
        <div class="FieldStyle" nowrap="nowrap">
            <asp:Localize ID="ColumnOutOfStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOutOfStockMessage%>'></asp:Localize></div>
        <div class="ValueStyle">
            <asp:Label ID="lblOutofStock" runat="server"></asp:Label>&nbsp;</div>
        <div class="FieldStyleA" nowrap="nowrap">
            <asp:Localize ID="ColumnBackOrderMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBackOrderMessage%>'></asp:Localize></div>
        <div class="ValueStyleA">
            <asp:Label ID="lblBackOrderMsg" runat="server"></asp:Label>&nbsp;</div>
        <div class="ClearBoth"><br />
            <div style="width:100%; text-align:right; display:inline-table;">
                    <asp:LinkButton CssClass="AddButton" ID="btnAddNewAddOnValues" runat="server" Text="Add Value"
                        OnClick="BtnAddNewAddOnValues_Click" /></div>
            <h4 class="GridTitle">
                <asp:Localize ID="GridTitleAddOnValues" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleAddOnValues%>'></asp:Localize>
            </h4>
            <asp:GridView OnRowDataBound="UxGrid_RowDataBound" ID="uxGrid" runat="server" CssClass="Grid"
                AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                OnPageIndexChanging="UxGrid_PageIndexChanging" CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand"
                Width="100%" EnableSortingAndPagingCallbacks="False" PageSize="25" AllowSorting="True"
                EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundAddOnTypes%>' OnRowDeleting="UxGrid_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="AddOnValueId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID%>'  HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName%>'  HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <a href='add_Addonvalues.aspx?itemid=<%=ItemId %>&AddOnValueId=<%# DataBinder.Eval(Container.DataItem, "AddOnvalueId").ToString()%>'>
                                <%# Eval("Name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SKU" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSKU%>'  HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantityOnHand%>'>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="quantity" Text='<%# GetQuantity((int)Eval("AddOnvalueId")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleReOrderLevel%>'>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="ReOrderLevel" Text='<%# GetReOrderLevel((int)Eval("AddOnvalueId")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder%>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePrice%>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "Retailprice","{0:c}").ToString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDefault%>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "DefaultInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkEdit%>' ButtonType="Link">
                        <ControlStyle CssClass="Button" />
                    </asp:ButtonField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton CommandName="Delete" CausesValidation="false" ID="btnDelete" runat="server"
                                Text='<%$ Resources:ZnodeAdminResource, LinkDelete%>' CssClass="Button" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>

    </div>
</asp:Content>
