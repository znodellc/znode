<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.MallAdmin.Admin_Secure_product_addons_add" ValidateRequest="false" Title="Untitled Page"
    CodeBehind="add.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/mallAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
            <div>
                <asp:Label ID="lblMsg" CssClass="Error" EnableViewState="false" runat="server"></asp:Label>
            </div>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ID="btnSubmitTop" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ValidationGroup="grpAddOn" />
            <zn:Button ID="btnCancelTop" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />

        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <h4 class="SubTitle">
            <asp:Localize runat="server" ID="SubTitleGeneral" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings%>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName%>'></asp:Localize><span class="Asterix">*</span><br />
            <small>
                <asp:Localize runat="server" ID="HintTextAddOn" Text='<%$ Resources:ZnodeAdminResource, HintTextAddOn%>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAddOnName%>' ValidationGroup="grpAddOn" CssClass="Error"
                Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitle" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTitle%>'></asp:Localize><span class="Asterix">*</span><br />
            <small>
                <asp:Localize runat="server" ID="HintTextTitle" Text='<%$ Resources:ZnodeAdminResource, HintTextTitle%>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnTitle" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAddOnTitle"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAddOnTitle%>' ValidationGroup="grpAddOn" CssClass="Error"
                Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnAddOnDescription%>'></asp:Localize><br />
            <small>
                <asp:Localize runat="server" ID="ColumnTextAddOnDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnTextAddOnDescription%>'></asp:Localize></small>
        </div>
        <div class="ValueStyle" style="width: 75%">
            <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleDisplayOrder" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder%>'></asp:Localize><span class="Asterix">*</span><br />
            <small>
                <asp:Localize runat="server" ID="ColumnTextDisplayOrder" Text='<%$ Resources:ZnodeAdminResource, ColumnTextAddOnDisplayOrder%>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Display="Dynamic"
                ValidationGroup="grpAddOn" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrderwithStar%>' ControlToValidate="txtDisplayOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtDisplayOrder"
                Display="Dynamic" ValidationGroup="grpAddOn" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber%>'
                MaximumValue="999999999" MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleDisplayType" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayType%>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlDisplayType" runat="server">
                <asp:ListItem Value="DropDownList" Text='<%$ Resources:ZnodeAdminResource, DropDownTextDropDownList%>'></asp:ListItem>
                <asp:ListItem Value="RadioButton" Text='<%$ Resources:ZnodeAdminResource, DropDownTextRadioButtons %>'></asp:ListItem>
                <asp:ListItem Value="CheckBox" Text='<%$ Resources:ZnodeAdminResource, DropDownTextCheckBox %>'></asp:ListItem>
                <asp:ListItem Value="TextBox" Text='<%$ Resources:ZnodeAdminResource, DropDownTextTextBox %>'></asp:ListItem>
            </asp:DropDownList>
        </div>

        <div class="FieldStyle"></div>
        <div class="ValueStyleText">
            <asp:CheckBox ID="chkOptionalInd" CssClass="HintStyle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAddOnOptional %>' Height="30px" />
        </div>

        <h4 class="SubTitle">
            <asp:Localize ID="InventorySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleInventorySettings %>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize ID="OutOfStockOptions" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOutOfStockOptions %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:RadioButtonList ID="InvSettingRadiobtnList" runat="server">
                <asp:ListItem Selected="True" Value="1" Text='<%$ Resources:ZnodeAdminResource, ColumnTrackInventory %>'></asp:ListItem>
                <asp:ListItem Value="2" Text='<%$ Resources:ZnodeAdminResource, ColumnBackOrder %>'></asp:ListItem>
                <asp:ListItem Value="3" Text='<%$ Resources:ZnodeAdminResource, ColumnNoTrackInventory %>'></asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="InStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnInStockMessage %>'></asp:Localize><br />
            <small>
                <asp:Localize ID="InStockMessageText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextInStockMessage %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtInStockMsg" runat="server"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="OutOfStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOutOfStockMessage %>'></asp:Localize><br />
            <small>
                <asp:Localize ID="OutOfStockMessageText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextOutOfStockMessage %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtOutofStock" runat="server" Text='<%$ Resources:ZnodeAdminResource, ValueOutOfStock %>'></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="BackOrderMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBackOrderMessage %>'></asp:Localize><br />
            <small>
                <asp:Localize ID="BackOrderMessageText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextBackOrderMessage %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtBackOrderMsg" runat="server"></asp:TextBox>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmitBottom" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true" ValidationGroup="grpAddOn" />
            <zn:Button runat="server" ID="btnCancelBottom" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
    </div>
</asp:Content>
