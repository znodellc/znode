using System;
using System.Data;
using System.Web;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_product_addons_add_Addonvalues class.
    /// </summary>
    public partial class Admin_Secure_product_addons_add_Addonvalues : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        private int productId = 0;
        private int addPage = -1;
        private int addOnValueId = 0;
        private string viewPageLink = "~/MallAdmin/Secure/product/addons/view.aspx?itemid=";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event - fires while page is loading
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.itemId = 0;
            }

            if (Request.Params["zpid"] != null)
            {
                this.productId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["zpid"].ToString()));
            }

            if (Request.Params["AddPage"] != null)
            {
                this.addPage = Convert.ToInt32(this.encryption.DecryptData(Request.Params["AddPage"].ToString()));
            }

            if (Request.Params["AddOnValueId"] != null)
            {
                this.addOnValueId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["AddOnValueId"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                this.BindTaxClasses();

                // If edit func then bind the data fields
                if (this.addOnValueId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditAddOnValue").ToString();
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddAddOnValue").ToString(); 
                }

                // Bind Locale dropdown for the this AddOnValue.
                this.BindLocaleDropdown();
            }
        }

        #endregion       

        #region General Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddOnValueSubmit_Click(object sender, EventArgs e)
        {
            bool isUpdated = this.UpdateAddOnValue();

            if (isUpdated)
            {
                Response.Redirect(this.viewPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())) + "&zpid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.productId.ToString())));
            }
            else
            {
                lblAddonValueMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAddOnValue").ToString(); 
                return;
            }
        }

        /// <summary>
        /// Cancel Button Click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.viewPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())) + "&zpid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.productId.ToString())));
        }

        /// <summary>
        /// Redirect to Locale based AddOnValue edit page.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlLocales_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool isUpdated = this.UpdateAddOnValue();

            if (isUpdated)
            {
                int id = 0;
                int.TryParse(ddlLocales.SelectedValue, out id);

                // Get the AddOnId for the selected AddOnValueId.
                ZNode.Libraries.DataAccess.Service.AddOnValueService aovs = new ZNode.Libraries.DataAccess.Service.AddOnValueService();
                AddOnValue addOnValue = aovs.GetByAddOnValueID(id);
                if (addOnValue != null)
                {
                    // Redirect, if user select other language of same add on.
                    Response.Redirect(string.Concat(Request.Path, "?itemId=", HttpUtility.UrlEncode(this.encryption.EncryptData(addOnValue.AddOnID.ToString())), "&AddOnValueId=", HttpUtility.UrlEncode(this.encryption.EncryptData(id.ToString()))));
                }
            }
            else
            {
                lblAddonValueMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAddOnValue").ToString(); 
                return;
            }
        }

        /// <summary>
        /// Update the input fields to AddOnValue object/database.
        /// </summary>
        /// <returns>Returns true if Addon updated else false.</returns>
        private bool UpdateAddOnValue()
        {
            try
            {
                ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
                AddOnValue addOnValue = new AddOnValue();

                if (this.addOnValueId > 0)
                {
                    addOnValue = productAddOnAdmin.GetByAddOnValueID(this.addOnValueId);
                }

                // Set Properties
                // General Settings
                addOnValue.AddOnID = this.itemId;
                addOnValue.Name = Server.HtmlEncode(txtAddOnValueName.Text.Trim());
                addOnValue.Description = Server.HtmlEncode(txtDescription.Text.Trim());
                addOnValue.RetailPrice = decimal.Parse(txtAddOnValueRetailPrice.Text.Trim());
                addOnValue.SalePrice = null;
                addOnValue.WholesalePrice = null;

                // Display Settings
                addOnValue.DisplayOrder = int.Parse(txtAddonValueDispOrder.Text.Trim());
                addOnValue.DefaultInd = chkIsDefault.Checked;

                // Inventory Settings
                addOnValue.SKU = txtAddOnValueSKU.Text.Trim();

                int qty = 0, reorder = 0;
                int.TryParse(txtAddOnValueQuantity.Text.Trim(), out qty);
                int.TryParse(txtReOrder.Text.Trim(), out reorder);
                ProductAdmin.UpdateQuantity(addOnValue, qty, reorder);

                // Tax Class
                if (ddlTaxClass.SelectedIndex != -1)
                {
                    addOnValue.TaxClassID = int.Parse(ddlTaxClass.SelectedValue);
                }

                bool isUpdated = false;

                if (this.addOnValueId > 0)
                {
                    // Set update date
                    addOnValue.UpdateDte = System.DateTime.Now;

                    // Update option values
                    isUpdated = productAddOnAdmin.UpdateAddOnValue(addOnValue);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAddOnValue").ToString() + txtAddOnValueName.Text, txtAddOnValueName.Text);
                }
                else
                {
                    // Add new option values
                    isUpdated = productAddOnAdmin.AddNewAddOnValue(addOnValue);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddAddOnValue").ToString() + txtAddOnValueName.Text, txtAddOnValueName.Text);
                }

                return isUpdated;
            }
            catch (Exception e)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(e.ToString());
            }

            return false;
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind tax classes list
        /// </summary>
        private void BindTaxClasses()
        {
            // Bind Tax Class
            TaxRuleAdmin taxRuleAdmin = new TaxRuleAdmin();
            TList<TaxClass> taxClassList = taxRuleAdmin.GetAllTaxClass();
            taxClassList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.TaxClass tax)
            {
                return tax.ActiveInd == true;
            });

            ddlTaxClass.DataSource = taxClassList;
            ddlTaxClass.DataTextField = "name";
            ddlTaxClass.DataValueField = "TaxClassID";
            ddlTaxClass.DataBind();
        }

        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        private void BindEditData()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOnValue addOnValue = productAddOnAdmin.DeepLoadByAddOnValueID(this.addOnValueId);

            if (addOnValue != null)
            {
                // General Settings
                lblTitle.Text += addOnValue.Name;
                txtAddOnValueName.Text = Server.HtmlDecode(addOnValue.Name);
                txtDescription.Text = Server.HtmlEncode(addOnValue.Description);
                txtAddOnValueRetailPrice.Text = addOnValue.RetailPrice.ToString("N");

                if (ddlTaxClass.SelectedIndex != -1)
                {
                    ddlTaxClass.SelectedValue = addOnValue.TaxClassID.GetValueOrDefault(0).ToString();
                }

                // Display Settings
                txtAddonValueDispOrder.Text = addOnValue.DisplayOrder.ToString();
                chkIsDefault.Checked = addOnValue.DefaultInd;

                // Inventory Settings
                txtAddOnValueSKU.Text = addOnValue.SKU;

                // Load the inventory record and assign to text boxes.
                SKUInventory skuInventory = ProductAdmin.GetAddOnInventory(addOnValue);

                if (skuInventory != null)
                {
                    txtAddOnValueQuantity.Text = skuInventory.QuantityOnHand.ToString();

                    if (skuInventory.ReOrderLevel.HasValue)
                    {
                        txtReOrder.Text = skuInventory.ReOrderLevel.ToString();
                    }
                }

                if (addOnValue.AddOnIDSource != null &&
                    string.Compare(addOnValue.AddOnIDSource.DisplayType, "TextBox", true) == 0)
                {
                    pnlAddOnTextBox.Visible = true;
                }
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundAddOnValue").ToString());
            }
        }

        /// <summary>
        /// Bind the locale dropdown, if this add on have different locale.
        /// </summary>
        private void BindLocaleDropdown()
        {
            // Get the Locale information with product id.
            DataSet localeDataSet = ProductAddOnAdmin.GetLocaleIdsByAddOnValueId(this.addOnValueId);

            // Bind the dropdown.
            ddlLocales.DataSource = localeDataSet.Tables[0];
            ddlLocales.DataTextField = "LocaleDescription";
            ddlLocales.DataValueField = "AddOnValueId";
            ddlLocales.DataBind();

            if (ddlLocales.Items.Count == 0)
            {
                ddlLocales.Visible = false;
            }
            else
            {
                // Select the current language.
                ddlLocales.SelectedValue = this.addOnValueId.ToString();
            }
        }

        #endregion
    }
}