<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.MallAdmin.Admin_Secure_product_addons_add_Addonvalues" CodeBehind="add_Addonvalues.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
            <div>
                <uc1:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <div>
                <asp:Label ID="lblAddonValueMsg" CssClass="Error" EnableViewState="false" runat="server"></asp:Label>
            </div>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnAddOnValueSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmitTop" ValidationGroup="grpAddOnValue" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelTop" />
        </div>
        <div class="ClearBoth" align="left">
            <div class="SubTitle">
                <asp:Localize ID="ColumnAssociatedLocales" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAssociatedLocales%>'></asp:Localize>
                &nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList runat="server" ID="ddlLocales"
                    AutoPostBack="true" OnSelectedIndexChanged="DdlLocales_SelectedIndexChanged">
                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, ColumnSelectLanguage%>'></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <br />
        <h4 class="SubTitle">
            <asp:Localize runat="server" ID="SubTitleGeneral" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings%>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnLabel" Text='<%$ Resources:ZnodeAdminResource, ColumnLabel%>'></asp:Localize><span class="Asterix">*</span><br />
            <small>
                <asp:Localize runat="server" ID="ColumnTextLabel" Text='<%$ Resources:ZnodeAdminResource, ColumnTextLabel%>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnValueName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                ValidationGroup="grpAddOnValue" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAddOnValueName"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredLabel%>' CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
        </div>

        <asp:Panel ID="pnlAddOnTextBox" runat="server" Visible="false">
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDescription%>'></asp:Localize><span class="Asterix">*</span><br />
                <small>
                    <asp:Localize runat="server" ID="ColumnDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnDescriptionOptionValue%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                    ValidationGroup="grpAddOnValue" runat="server" ControlToValidate="txtDescription"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDescription%>' CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </asp:Panel>

        <div class="FieldStyle">
            <asp:Localize ID="ColumnRetailPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRetailPrice%>'></asp:Localize><span class="Asterix">*</span><br />
            <small>
                <asp:Localize ID="ColumnTextRetailPriceAddOn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextRetailPriceAddOn%>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                Text="0" ID="txtAddOnValueRetailPrice" runat="server" MaxLength="7"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="grpAddOnValue"
                runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredRetailPrice%>' ControlToValidate="txtAddOnValueRetailPrice"
                CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" ValidationGroup="grpAddOnValue" runat="server"
                ControlToValidate="txtAddOnValueRetailPrice" Type="Currency" Operator="DataTypeCheck"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidRetailPrice%>' CssClass="Error"
                Display="Dynamic" />
            <asp:RangeValidator ID="RangeValidator1" ValidationGroup="grpAddOnValue" runat="server"
                ControlToValidate="txtAddOnValueRetailPrice" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangePrice%>'
                MaximumValue="999999" MinimumValue="-999999" Type="Currency" Display="Dynamic"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTaxClass" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTaxClass%>'></asp:Localize><br />
            <small>
                <asp:Localize ID="ColumnTextTaxClass" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextTaxClass%>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlTaxClass" runat="server" />
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="SubTitleDisplaySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDisplaySettings%>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleDisplayOrder" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder%>'></asp:Localize><span class="Asterix">*</span><br />
            <small>
                <asp:Localize runat="server" ID="ColumnTextDisplayOrder" Text='<%$ Resources:ZnodeAdminResource, ColumnTextAddOnDisplayOrder%>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddonValueDispOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                ValidationGroup="grpAddOnValue" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrderwithStar%>' ControlToValidate="txtAddonValueDispOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtAddonValueDispOrder"
                Display="Dynamic" ValidationGroup="grpAddOnValue" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber%>'
                MaximumValue="999999999" MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle"></div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkIsDefault" runat="server" Text="" />
            <asp:Localize ID="CheckBoxSelectItem" runat="server" Text='<%$ Resources:ZnodeAdminResource, CheckBoxSelectItem%>'></asp:Localize><br />
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="InventorySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleInventorySettings %>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleSkuorPart" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSkuorPart %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnValueSKU" runat="server" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtAddOnValueSKU"
                CssClass="Error" ValidationGroup="grpAddOnValue" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSKUorPart%>'></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleQuantityOnHand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantityOnHand %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnValueQuantity" runat="server" Rows="3">9999</asp:TextBox>
            <asp:RangeValidator ValidationGroup="grpAddOnValue" ID="RangeValidator2" runat="server"
                ControlToValidate="txtAddOnValueQuantity" CssClass="Error" Display="Dynamic"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeQuantity%>' MaximumValue="999999" MinimumValue="0"
                SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
            <asp:RequiredFieldValidator ValidationGroup="grpAddOnValue" ID="RequiredFieldValidator6"
                runat="server" ControlToValidate="txtAddOnValueQuantity" CssClass="Error" Display="Dynamic"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredQuantity%>'></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleReOrderLevel" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleReOrderLevel%>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtReOrder" runat="server"></asp:TextBox>
            <asp:RangeValidator ValidationGroup="grpAddOnValue" ID="RangeValidator5" runat="server"
                ControlToValidate="txtReOrder" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeQuantity%>'
                MaximumValue="999999" MinimumValue="0" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
        </div>
    </div>
</asp:Content>
