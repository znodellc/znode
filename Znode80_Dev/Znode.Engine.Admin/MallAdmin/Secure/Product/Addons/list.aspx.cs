using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_product_addons_list class.
    /// </summary>
    public partial class Admin_Secure_product_addons_list : System.Web.UI.Page
    {
        #region Private  Variables
        private static bool isSearchEnabled = false;
        private string addPageLink = "~/Secure/catalog/product_addons/add.aspx";
        private string viewPageLink = "~/Secure/catalog/product_addons/view.aspx";
        private string deletePageLink = "~/Secure/catalog/product_addons/delete.aspx";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindLocaleDropdown();
                this.BindGridData();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Add New Product AddOn Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddCategory_Click(object sender, System.EventArgs e)
        {
            Response.Redirect(this.addPageLink);
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.SearchAddOns();
            isSearchEnabled = true;
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // Check if search is Enabled
            if (isSearchEnabled)
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.SearchAddOns();
            }
            else
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindGridData();
            }
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell tableCell = selectedRow.Cells[0];
                string Id = tableCell.Text;

                if (e.CommandName == "Manage")
                {
                    this.viewPageLink = this.viewPageLink + "?itemid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(Id.ToString()));
                    Response.Redirect(this.viewPageLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.deletePageLink + "?itemid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(Id.ToString())));
                }
            }
        }
        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind the locale dropdown
        /// </summary>
        private void BindLocaleDropdown()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();

            // Bind the dropdown.
            ddlLocales.DataSource = productAddOnAdmin.GetAllLocaleId();
            ddlLocales.DataTextField = "LocaleDescription";
            ddlLocales.DataValueField = "LocaleId";
            ddlLocales.DataBind();

            if (ddlLocales.Items.Count == 0)
            {
                lblLocale.Visible = false;
                ddlLocales.Visible = false;
            }
        }

        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(productAddOnAdmin.GetAllAddOns(), true);
            uxGrid.DataBind();
        }

        /// <summary>
        /// Search AddOn
        /// </summary>
        private void SearchAddOns()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            int localeId = 0;
            int.TryParse(ddlLocales.SelectedValue, out localeId);
            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(productAddOnAdmin.SearchAddOns(Server.HtmlEncode(txtAddonName.Text.Trim()), Server.HtmlEncode(txtAddOnTitle.Text.Trim()), txtsku.Text.Trim(), localeId).Tables[0], true);
            uxGrid.DataBind();
        }
        #endregion
    }
}