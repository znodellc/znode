using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_product_addons_view class.
    /// </summary>
    public partial class Admin_Secure_product_addons_view : System.Web.UI.Page
    {
        #region Private Variables
        private int _ItemId;
        private int productId = 0;
        private int addPage = -1;
        private string addOnValuePageLink = "~/MallAdmin/Secure/product/addons/add_Addonvalues.aspx?itemid=";
        private string editPageLink = "~/MallAdmin/Secure/product/addons/add.aspx?itemid=";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        /// <summary>
        /// Gets or sets the item Id
        /// </summary>
        public int ItemId
        {
            get { return this._ItemId; }
            set { this._ItemId = value;   }
        }

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this._ItemId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this._ItemId = 0;
            }

            if (Request.Params["AddPage"] != null)
            {
                this.addPage = Convert.ToInt32(this.encryption.DecryptData(Request.Params["AddPage"].ToString()));
            }

            if (Request.Params["zpid"] != null)
            {
                this.productId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["zpid"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }
        #endregion        

        #region General Events

        /// <summary>
        /// Create New AddOnValue Button Click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddNewAddOnValues_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addOnValuePageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this._ItemId.ToString())) + "&zpid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.productId.ToString())));
        }

        /// <summary>
        /// Edit Add-On Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditAddOn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MallAdmin/Secure/Product/Addons/add.aspx?itemid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this._ItemId.ToString())) + "&zpid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.productId.ToString())));            
        }

        /// <summary>
        /// BAck to Add-On Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BacktoAddOn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MallAdmin/Secure/Product/add_addons.aspx?itemid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.productId.ToString())));            
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                LinkButton deleteButton = (LinkButton)e.Row.Cells[7].FindControl("btnDelete");

                // Set the Button's CommandArgument property with the row's index.
                deleteButton.CommandArgument = e.Row.RowIndex.ToString();

                // Add Client Side confirmation
                deleteButton.OnClientClick = "return confirm('Are you sure you want to delete this Add-On Value?');";
            }
        }

        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        /// <summary>
        /// Even triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell tableCell = selectedRow.Cells[0];
                string Id = tableCell.Text;

                if (e.CommandName == "Edit")
                {
                    this.editPageLink = this.addOnValuePageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this._ItemId.ToString())) + "&AddOnValueId=" + HttpUtility.UrlEncode(this.encryption.EncryptData(Id.ToString()));
                    if (this.addPage >= 0)
                    {
                        this.editPageLink = this.editPageLink + "&AddPage=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.addPage.ToString())) + "&zpid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.productId.ToString()));
                    }

                    Response.Redirect(this.editPageLink);
                }

                if (e.CommandName == "Delete")
                {
                    ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();

                    // Get the Addon name from ItemId
                    AddOn addon = new AddOn();
                    addon = productAddOnAdmin.GetByAddOnId(this._ItemId);
                    string addOnName = addon.Name;

                    // Get AddOnValue name from Id
                    AddOnValue addOnValue = new AddOnValue();
                    addOnValue = productAddOnAdmin.GetByAddOnValueID(int.Parse(Id));

                    string associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteAddOnValues").ToString() + addOnValue.Name;

                    bool isDeleted = productAddOnAdmin.DeleteAddOnValue(int.Parse(Id));
                    if (isDeleted)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, addOnName);

                        this.BindGrid();
                    }
                }
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Get the disabled checkmark image
        /// </summary>
        /// <returns>Returns the disabled checkmark image</returns>
        protected string SetImage()
        {
            return ZNode.Libraries.Admin.Helper.GetCheckMark(false);
        }

        /// <summary>
        /// Get the addOn quantity
        /// </summary>
        /// <param name="addOnValueId">AddOnValueId to get the addon quantity</param>
        /// <returns>Returns the quantity value</returns>
        protected string GetQuantity(int addOnValueId)
        {
            ZNode.Libraries.DataAccess.Service.AddOnValueService avs = new ZNode.Libraries.DataAccess.Service.AddOnValueService();
            return ProductAdmin.GetQuantity(avs.GetByAddOnValueID(addOnValueId)).ToString();
        }

        /// <summary>
        /// Get the add on reorder value
        /// </summary>
        /// <param name="addOnValueId">AddOnValueId to get the Addon reorder level value.</param>
        /// <returns>Returns the reorder level value</returns>
        protected string GetReOrderLevel(int addOnValueId)
        {
            ZNode.Libraries.DataAccess.Service.AddOnValueService avs = new ZNode.Libraries.DataAccess.Service.AddOnValueService();
            return ProductAdmin.GetAddOnInventory(avs.GetByAddOnValueID(addOnValueId)).ReOrderLevel.ToString();
        }
        #endregion
        #region Bind Methods
        /// <summary>
        ///  Bind data to grid
        /// </summary>
        private void BindGrid()
        {
            ProductAddOnAdmin addOnValueAdmin = new ProductAddOnAdmin();
            TList<AddOnValue> addOnValueList = addOnValueAdmin.GetAddOnValuesByAddOnId(this._ItemId);
            if (addOnValueList != null)
            {
                addOnValueList.Sort("DisplayOrder");
            }

            uxGrid.DataSource = addOnValueList;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        private void BindData()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOn addOn = productAddOnAdmin.GetByAddOnId(this._ItemId);

            if (addOn != null)
            {
                lblTitle.Text = addOn.Name;
                lblName.Text = addOn.Name;
                lblAddOnTitle.Text = addOn.Title;
                lblDisplayOrder.Text = addOn.DisplayOrder.ToString();
                lblDisplayType.Text = addOn.DisplayType.ToString();

                // Display Settings
                chkOptionalInd.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(addOn.OptionalInd.ToString()));

                // Inventory Setting - Out of Stock Options
                if (addOn.TrackInventoryInd && addOn.AllowBackOrder == false)
                {
                    chkCartInventoryEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                    chkIsBackOrderEnabled.Src = this.SetImage();
                    chkIstrackInvEnabled.Src = this.SetImage();
                }
                else if (addOn.TrackInventoryInd && addOn.AllowBackOrder)
                {
                    chkCartInventoryEnabled.Src = this.SetImage();
                    chkIsBackOrderEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                    chkIstrackInvEnabled.Src = this.SetImage();
                }
                else if ((addOn.TrackInventoryInd == false) && (addOn.AllowBackOrder == false))
                {
                    chkCartInventoryEnabled.Src = this.SetImage();
                    chkIsBackOrderEnabled.Src = this.SetImage();
                    chkIstrackInvEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                }

                this.BindGrid();

                // Inventory Setting - Stock Messages
                lblInStockMsg.Text = addOn.InStockMsg;
                lblOutofStock.Text = addOn.OutOfStockMsg;
                lblBackOrderMsg.Text = addOn.BackOrderMsg;

                ZNode.Libraries.DataAccess.Service.LocaleService localeService = new ZNode.Libraries.DataAccess.Service.LocaleService();
                if (addOn.LocaleId > 0)
                {
                    lblLocale.Text = localeService.GetByLocaleID(addOn.LocaleId).LocaleDescription;
                }
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource","RecordNotFoundAddOnRequested").ToString());
            }
        }
        #endregion
    }
}