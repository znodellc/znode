<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.MallAdmin.Admin_Secure_product_addons_delete" Title="Untitled Page" CodeBehind="delete.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h5>
        <asp:Localize ID="TextPleaseConfirm" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm%>'></asp:Localize></h5>
    <p>
        <asp:Localize ID="TextConfirmDeleteAddOn" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextConfirmDeleteAddOn%>'></asp:Localize>
    </p>
    <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label><br />
    <br />
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnDelete_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete%>' CausesValidation="true" ID="btnDelete" />
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancel" />
    <br />
    <br />
    <br />
</asp:Content>

