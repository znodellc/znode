using System;
using System.Web;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_product_addons_delete class.
    /// </summary>
    public partial class Admin_Secure_product_addons_delete : System.Web.UI.Page
    {
        #region Private Variables
        private string _ProductAddOnName = string.Empty;
        private int itemId;        
        private string addonName = string.Empty;
        private ZNodeEncryption encryption = new ZNodeEncryption();
        private int pageId = 0;
        private int productId = 0;
        #endregion

        /// <summary>
        /// Gets or sets the product Addon name
        /// </summary>
        public string ProductAddOnName
        {
            get { return this._ProductAddOnName; }
            set { this._ProductAddOnName = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));          
            }
            else
            {
                this.itemId = 0;
            }

            if (Request.Params["zpid"] != null)
            {
                this.productId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["zpid"].ToString()));
            }

            if (Request.Params["AddPage"] != null)
            {
                this.pageId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["AddPage"].ToString()));
            }

            this.BindData();
        }        

        #region Events
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Malladmin/Secure/Product/add_addons.aspx?itemid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.productId.ToString())));
        }

        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            bool isDeleted = false;

            try
            {
                ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
                AddOn addOn = new AddOn();
                addOn.AddOnID = this.itemId;
                this.addonName = addOn.Name;

                isDeleted = productAddOnAdmin.DeleteAddOn(addOn);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }

            if (isDeleted)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Addon Values - " + this.addonName, this.addonName);
                Response.Redirect("~/Malladmin/Secure/Product/add_addons.aspx?itemid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.productId.ToString())));
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteAddOnValues").ToString(); 
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the screen
        /// </summary>
        private void BindData()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOn addOn = productAddOnAdmin.GetByAddOnId(this.itemId);

            if (addOn != null)
            {
                this.ProductAddOnName = addOn.Name;
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundProductAddOn").ToString());
            }
        }
        #endregion
    }
}