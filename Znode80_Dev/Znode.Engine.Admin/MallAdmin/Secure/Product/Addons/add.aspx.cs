using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_product_addons_add class.
    /// </summary>
    public partial class Admin_Secure_product_addons_add : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        private int productId = 0;
        private bool isToRedirect = false;
        private int addPage = -1;
        private string listPageLink = "~/MallAdmin/Secure/product/addons/list.aspx";
        private string viewPageLink = "~/MallAdmin/Secure/product/addons/view.aspx?itemid=";
        private string cancelPageLink = "~/MallAdmin/Secure/product/addons/view.aspx?itemid=";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.itemId = 0;
            }

            // Get productid from querystring        
            if (Request.Params["zpid"] != null)
            {
                this.productId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["zpid"].ToString()));
            }

            // Get AddPage from querystring        
            if (Request.Params["AddPage"] != null)
            {
                this.addPage = Convert.ToInt32(this.encryption.DecryptData(Request.Params["AddPage"].ToString()));
            }

            // Reset the edit fields
            if (Request.Params["mode"] != null)
            {
                this.isToRedirect = bool.Parse(Request.Params["mode"]);
            }

            // Redirect directly to product details page
            if (this.isToRedirect)
            {
                // Reset the URL with Product details page
                this.listPageLink = "~/MallAdmin/Secure/product/list.aspx?itemid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.productId.ToString())) + "&mode=addons";
                this.viewPageLink = this.listPageLink;
                this.cancelPageLink = this.listPageLink;
            }

            if (Page.IsPostBack == false)
            {
                // If edit func then bind the data fields
                if (this.itemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditAddOn").ToString();
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddAddOn").ToString();
                }
            }
        }
        #endregion

        #region Bind Data

        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        protected void BindEditData()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOn addOn = productAddOnAdmin.GetByAddOnId(this.itemId);

            if (addOn != null)
            {
                lblTitle.Text += addOn.Name;
                txtName.Text = Server.HtmlDecode(addOn.Name);
                txtAddOnTitle.Text = Server.HtmlDecode(addOn.Title);
                txtDisplayOrder.Text = addOn.DisplayOrder.ToString();
                chkOptionalInd.Checked = addOn.OptionalInd;
                ctrlHtmlText.Html = addOn.Description;
                ddlDisplayType.SelectedValue = addOn.DisplayType;

                // Inventory Setting - Out of Stock Options
                if (addOn.TrackInventoryInd && addOn.AllowBackOrder == false)
                {
                    InvSettingRadiobtnList.Items[0].Selected = true;
                }
                else if (addOn.TrackInventoryInd && addOn.AllowBackOrder)
                {
                    InvSettingRadiobtnList.Items[1].Selected = true;
                }
                else if ((addOn.TrackInventoryInd == false) && (addOn.AllowBackOrder == false))
                {
                    InvSettingRadiobtnList.Items[2].Selected = true;
                }

                // Inventory Setting - Stock Messages
                txtInStockMsg.Text = Server.HtmlDecode(addOn.InStockMsg);
                txtOutofStock.Text = Server.HtmlDecode(addOn.OutOfStockMsg);
                txtBackOrderMsg.Text = Server.HtmlDecode(addOn.BackOrderMsg);
            }
            else
            {
                throw new ApplicationException(GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundAddOnRequested").ToString());
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOn addOn = new AddOn();

            if (this.itemId > 0)
            {
                addOn = productAddOnAdmin.GetByAddOnId(this.itemId);
            }

            // Set properties - General settings
            addOn.Name = Server.HtmlEncode(txtName.Text.Trim());
            addOn.Title = Server.HtmlEncode(txtAddOnTitle.Text.Trim());
            addOn.DisplayOrder = int.Parse(txtDisplayOrder.Text.Trim());
            addOn.OptionalInd = chkOptionalInd.Checked;
            addOn.Description = ctrlHtmlText.Html;
            addOn.DisplayType = ddlDisplayType.SelectedItem.Value;

            // Set properties - Inventory settings
            // Out of Stock Options
            if (InvSettingRadiobtnList.SelectedValue.Equals("1"))
            {
                // Only Sell if Inventory Available - Set values
                addOn.TrackInventoryInd = true;
                addOn.AllowBackOrder = false;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("2"))
            {
                // Allow Back Order - Set values
                addOn.TrackInventoryInd = true;
                addOn.AllowBackOrder = true;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("3"))
            {
                // Don't Track Inventory - Set property values
                addOn.TrackInventoryInd = false;
                addOn.AllowBackOrder = false;
            }

            // Inventory Setting - Stock Messages
            if (txtOutofStock.Text.Trim().Length == 0)
            {
                addOn.OutOfStockMsg = this.GetGlobalResourceObject("ZnodeAdminResource", "TextBoxOutofStock").ToString(); 
            }
            else
            {
                addOn.OutOfStockMsg = Server.HtmlEncode(txtOutofStock.Text.Trim());
            }

            addOn.InStockMsg = Server.HtmlEncode(txtInStockMsg.Text.Trim());
            addOn.BackOrderMsg = Server.HtmlEncode(txtBackOrderMsg.Text.Trim());
            addOn.LocaleId = ZNodeConfigManager.SiteConfig.LocaleID;

            AccountService accountService = new AccountService();
            TList<Account> accountList = accountService.GetByUserID((Guid)Membership.GetUser().ProviderUserKey);
            if (accountList != null && accountList.Count > 0)
            {
                addOn.AccountID = accountList[0].AccountID;
            }

            bool isSuccess = false;
            if (this.itemId > 0)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAddOns").ToString() + txtName.Text.Trim(), txtName.Text.Trim());
                isSuccess = productAddOnAdmin.UpdateNewProductAddOn(addOn);
            }
            else
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddAddOns").ToString() + txtName.Text.Trim(), txtName.Text.Trim());
                isSuccess = productAddOnAdmin.CreateNewProductAddOn(addOn, out this.itemId);
            }

            if (isSuccess)
            {
                if (this.isToRedirect)
                {
                    Response.Redirect(this.viewPageLink);
                }

                Response.Redirect(this.viewPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())) + "&zpid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.productId.ToString())));
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAddOn").ToString();
                lblMsg.CssClass = "Error";
                return;
            }
        }

        /// <summary>
        /// Cancel Button Click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MallAdmin/Secure/Product/add_addons.aspx?itemid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.productId.ToString())));
        }
        #endregion
    }
}