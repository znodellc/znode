using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Public_Templates_Professional_product_Product class.
    /// </summary>
    public partial class Public_Templates_Professional_product_Product : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct product;
        private ZNodeProfile profile = new ZNodeProfile();
        private int productId;        
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets Shopping cart quantity
        /// </summary>
        public int ShoppingCartQuantity
        {
            get
            {
                if (ViewState["Quantity"] != null)
                {
                    return (int)ViewState["Quantity"];
                }

                return 1;
            }

            set
            {
                ViewState["Quantity"] = value;
            }
        }

        /// <summary>
        /// Gets the product category title.
        /// </summary>        
        private string ProductCategoryTitle
        {
            get
            {
                int categoryId = 0;

                if (Request.QueryString["zpid"] != null)
                {
                    ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();
                    TList<ProductCategory> productCategoryList = service.GetByProductID(this.productId);

                    if (productCategoryList.Count > 0)
                    {
                        categoryId = productCategoryList[0].CategoryID;
                        Session["BreadCrumzcid"] = categoryId;
                    }
                }
                else
                {
                    categoryId = 0;
                }

                ZNode.Libraries.DataAccess.Service.CategoryService categoryService = new ZNode.Libraries.DataAccess.Service.CategoryService();
                Category category = categoryService.GetByCategoryID(Convert.ToInt32(categoryId));

                if (category != null)
                {
                    return category.Title;
                }

                return string.Empty;
            }
        }
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this.productId = int.Parse(Request.Params["zpid"]);
            }
            else if (Request.Params["itemid"] != null)
            {
               this.productId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
            }
            else if (Session["itemid"] != null)
            {
                this.productId = int.Parse(Session["itemid"].ToString());
            }
            else
            {
                return;
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.product = ZNodeProduct.Create(this.productId, 0, 0, true, null, true);
            HttpContext.Current.Items["Product"] = this.product;
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // This code must be placed here for SEO Url Redirect to work and fix
            // postback problem with URL rewriting
            if (this.Page.Form != null)
            {
                this.Page.Form.Action = Request.RawUrl;
            }

            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this.productId = int.Parse(Request.Params["zpid"]);
            }
            else if (Request.Params["itemid"] != null)
            {
                this.productId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
            }
            else if (Session["itemid"] != null)
            {
                this.productId = int.Parse(Session["itemid"].ToString());
            }
            else
            {
                return;
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.product = ZNodeProduct.Create(this.productId,0,0,true,null,true);
            HttpContext.Current.Items["Product"] = this.product;
            NewItemImage.ImageUrl = "~/MallAdmin/themes/Images/New.gif";

            // Bind all controls
            this.Bind();
        }
        #endregion        

        #region General Events

        /// <summary>
        /// Send a Email Link Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EmailLink_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/EmailFriend.aspx?zpid=" + this.productId.ToString());
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Update and Bind inventory status message and product price
        /// </summary>
        private void UpdateStatus()
        {
            // Hide Product price
            uxProductPrice.Visible = !this.product.CallForPricing;            
            bool isErrorFound = false;            

            if (!isErrorFound)
            {
                // Don't track Inventory - Items can always be added to the cart,TrackInventory is disabled
                if (!this.product.TrackInventoryInd && !this.product.AllowBackOrder && this.product.QuantityOnHand > 0)
                {
                    lblstockmessage.Text = this.product.InStockMsg;
                }
                else if (!this.product.TrackInventoryInd && !this.product.AllowBackOrder && this.product.QuantityOnHand <= 0)
                {
                    lblstockmessage.Text = string.Empty;
                }
                else if (this.product.TrackInventoryInd && !this.product.AllowBackOrder && this.product.QuantityOnHand >= this.ShoppingCartQuantity)
                {
                    lblstockmessage.Text = this.product.InStockMsg;
                }                
                else if (this.product.AllowBackOrder && this.product.QuantityOnHand >= this.ShoppingCartQuantity)
                {
                    // Set Inventory stock message
                    lblstockmessage.Text = this.product.InStockMsg;
                }
                else if (this.product.AllowBackOrder && this.product.QuantityOnHand < this.ShoppingCartQuantity)
                {
                    lblstockmessage.Text = this.product.BackOrderMsg;
                }
                else
                {
                    // Display Out of stock message
                    lblstockmessage.Text = this.product.OutOfStockMsg;
                    isErrorFound = true;
                }
            }            

            bool hasAddOns = false;

            // Check if product has attributes
            if (this.product.ZNodeAddOnCollection.Count > 0)
            {
                hasAddOns = true;
            }

            // If product has Add-Ons, then validate that all Add-Ons Values have been selected by
            // the customer
            if (hasAddOns)
            {
                bool isAddonSelected = false;
                string statusMessage = string.Empty;

                // Get Selected Add-ons
                this.product.SelectedAddOnItems = uxProductAddOns.GetSelectedAddOns(out isAddonSelected, out statusMessage);

                if (isAddonSelected && !isErrorFound)
                {
                    uxStatus.Text = statusMessage;
                }
            }

            // Check for 'Call for Pricing'
            if (this.product.CallForPricing)
            {
                lblstockmessage.Visible = false;
                uxCallForPricing.Visible = true;
                uxStatus.Visible = false;
            }
            else
            {
                lblstockmessage.Visible = true;
            }

            // Bind selected sku product price if one exists
            uxProductPrice.Quantity = this.ShoppingCartQuantity;
            uxProductPrice.Product = this.product;
            uxProductPrice.Bind();
        }      
        #endregion

        #region Bind method
        /// <summary>
        /// Bind all the controls to the data
        /// </summary>
        private void Bind()
        {
            uxProductSwatches.ProductId = this.productId;
            pnlSwatches.Visible = uxProductSwatches.HasImages;

            uxProductPrice.Visible = this.profile.ShowPrice && !this.product.CallForPricing;
            pnlQty.Visible = this.profile.ShowPrice;

            bool showCartButton = this.profile.ShowAddToCart;

            // If out of stock and backorder disabled then hide the AddToCart button
            if (this.product.ZNodeAttributeTypeCollection.Count > 0)
            {
                // If product has attributes,then display the AddToCart button
                showCartButton &= true;
            }
            else if (this.product.AllowBackOrder && this.product.QuantityOnHand <= 0)
            {
                // Allow back Order
                showCartButton &= true;

                // Set back order message
                lblstockmessage.Text = this.product.BackOrderMsg;
            }
            else if (this.product.AllowBackOrder && this.product.QuantityOnHand > 0)
            {
                showCartButton &= true;

                // Set Item In-Stock message
                lblstockmessage.Text = this.product.InStockMsg;
            }
            else if (this.product.QuantityOnHand > 0 && this.product.TrackInventoryInd == true && !this.product.AllowBackOrder)
            {
                // Track Inventory
                showCartButton &= true;

                // Set In stock message
                lblstockmessage.Text = this.product.InStockMsg;
            }
            else if (this.product.AllowBackOrder == false && this.product.TrackInventoryInd == false && this.product.QuantityOnHand > 0)
            {
                // Don't track Inventory
                // Items can always be added to the cart,TrackInventory is disabled
                showCartButton &= true;

                // Set In stock message
                lblstockmessage.Text = this.product.InStockMsg;
            }
            else if (this.product.AllowBackOrder == false && this.product.TrackInventoryInd == false && this.product.QuantityOnHand <= 0)
            {
                // Don't track Inventory
                // Items can always be added to the cart,TrackInventory is disabled
                showCartButton &= true;

                // Set OutOf Stock message
                lblstockmessage.Text = string.Empty;
            }
            else
            {
                // Set OutOf Stock message
                lblstockmessage.Text = this.product.OutOfStockMsg;
                showCartButton &= false;
            }

            // Set Visible true for NewItem.
            if (this.product.NewProductInd)
            {
                NewItemImage.Visible = true;
            }

            // Set Visible true for FeaturedItem.
            if (this.product.FeaturedInd)
            {
                FeaturedItemImage.Visible = true;
            }

            uxCallForPricing.Text = ZNodeCatalogManager.MessageConfig.GetMessage("ProductCallForPricing");

            // Add to cart button
            if (this.product.CallForPricing)
            {
                // If call for pricing is enabled,then hide AddToCart Button
                showCartButton &= false;
                FeaturedItemImage.Visible = false;
                lblstockmessage.Text = string.Empty;
            }

            uxCallForPricing.Visible = !showCartButton;

            // Set product properties       
            ProductTitle.Text = string.Format(ProductTitle.Text, this.product.Name);
            lblProductId.Text = this.product.ProductNum;
            ProductDescription.Text = Server.HtmlDecode(this.product.Description);

            // Set Manufacturer Name
            if (this.product.ManufacturerName != string.Empty)
            {
                BrandLabel.Visible = true;
                lblBrand.Text = this.product.ManufacturerName;
            }

            uxProductRelated.Product = this.product;
            uxProductRelated.Bind();
        }

        #endregion
    }
}