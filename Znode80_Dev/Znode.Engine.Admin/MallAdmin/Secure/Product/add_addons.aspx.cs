using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_catalog_product_add_addons class.
    /// </summary>
    public partial class Admin_Secure_catalog_product_add_addons : System.Web.UI.Page
    {
        #region Private Variables
        private static bool isSearchEnabled = false;
        private int itemId = 0;
        private int pageId = 0;
        private string detailsPageLink = "~/MallAdmin/Secure/product/view.aspx?mode=";
        private string editPageLink = "~/MallAdmin/Secure/product/addons/view.aspx?itemid=";
        private string deletePageLink = "~/MallAdmin/Secure/product/addons/delete.aspx?itemid=";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.itemId = 0;
            }

            if (Request.Params["pageid"] != null)
            {
                this.pageId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["pageid"].ToString()));
            }

            if (Request.Params["AddPage"] != null)
            {
                this.pageId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["AddPage"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                this.SearchAddons();
                this.BindProductName();
            }
        }
        #endregion       

        #region Related Addon Grid Events
        /// <summary>
        /// AddOn Grid items page event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddOnGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxAddOnGrid.PageIndex = e.NewPageIndex;

            if (isSearchEnabled)
            {
                this.SearchAddons();
            }
            else
            {
                this.BindAddons();
            }
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddOnGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Manage")
                {
                    Response.Redirect(this.editPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(e.CommandArgument.ToString())) + "&zpid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())));
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.deletePageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(e.CommandArgument.ToString())) + "&zpid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())));
                }
            }
        }

        #endregion       

        #region Events
        /// <summary>
        /// Add AddOn button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddAddon_Click(object sender, EventArgs e)
        {
            if (this.pageId > 0)
            {
                Response.Redirect("~/MallAdmin/Secure/Product/Addons/add.aspx?zpid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())));
            }
            else
            {
                Response.Redirect("~/MallAdmin/Secure/Product/Addons/add.aspx?zpid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())));
            }
        }

        /// <summary>
        /// Add selected AddOn button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddSelectedAddons_Click(object sender, EventArgs e)
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            StringBuilder sb = new StringBuilder();
            StringBuilder addonName = new StringBuilder();

            // Loop through the grid values
            foreach (GridViewRow row in uxAddOnGrid.Rows)
            {
                CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProductAddon") as CheckBox;

                // Get AddOnId
                int addOnId = int.Parse(row.Cells[1].Text);

                if (check.Checked)
                {
                    ProductAddOn productAddOn = new ProductAddOn();

                    // Set Properties
                    productAddOn.ProductID = this.itemId;
                    productAddOn.AddOnID = addOnId;

                    if (productAddOnAdmin.IsAddOnExists(productAddOn))
                    {
                        productAddOnAdmin.AddNewProductAddOn(productAddOn);
                        addonName.Append(this.GetAddOnName(addOnId) + ",");
                        check.Checked = false;
                    }
                    else
                    {
                        sb.Append(this.GetAddOnName(addOnId) + ",");
                        lblAddOnErrorMessage.Visible = true;
                    }
                }
            }

            if (sb.ToString().Length > 0)
            {
                sb.Remove(sb.ToString().Length - 1, 1);

                // Display Error message
                lblAddOnErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductAddon").ToString() + sb.ToString();
            }
            else
            {
                ProductAdmin productAdmin = new ProductAdmin();
                Product product = productAdmin.GetByProductId(this.itemId);
                if (addonName.Length > 0)
                {
                    product.ReviewStateID = productAdmin.UpdateReviewStateID(UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId, "AddOns Changed");

                    productAdmin.Update(product);

                    addonName.Remove(addonName.Length - 1, 1); 
                    string associateName =       string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogProductAddon").ToString(), addonName, product.Name);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, product.Name);

                    ProductReviewHistoryAdmin reviewAdmin = new ProductReviewHistoryAdmin();
                    reviewAdmin.UpdateEditReviewHistory(this.itemId, "AddOn");
                    Response.Redirect(this.detailsPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData("options")) + "&itemid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())));
                }
                else
                {
                    lblAddOnErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorValidProductAddon").ToString();
                    lblAddOnErrorMessage.Visible = true;
                }
            }
        }

        /// <summary>
        /// Addon Search Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddOnSearch_Click(object sender, EventArgs e)
        {
            this.SearchAddons();

            isSearchEnabled = true;
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.detailsPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData("options")) + "&itemid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Clear Search Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddOnClear_Click(object sender, EventArgs e)
        {
            // Reset Text fields & Bool variable
            txtAddonName.Text = string.Empty;
            txtAddOnTitle.Text = string.Empty;
            txtAddOnsku.Text = string.Empty;
            isSearchEnabled = false;

            this.SearchAddons();
        }

        /// <summary>
        /// Gets the Addon values for this addOnId
        /// </summary>
        /// <param name="addOnId">AddOn Id to get the add on object</param>
        /// <returns>Returns the add on name.</returns>
        protected string GetAddOnValues(object addOnId)
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            TList<AddOnValue> addOnValueList = new TList<AddOnValue>();
            addOnValueList = productAddOnAdmin.GetAddOnValuesByAddOnId(int.Parse(addOnId.ToString()));

            AddOnValueService _addOnValueService = new AddOnValueService();
            string addonValueNames = string.Empty;
            if (addOnValueList.Count > 0)
            {
                foreach (AddOnValue tps in addOnValueList)
                {
                    AddOnValue _addonValue = _addOnValueService.GetByAddOnValueID(tps.AddOnValueID);
                    if (_addonValue.AddOnID == Convert.ToInt32(addOnId))
                    {
                        if (!string.IsNullOrEmpty(addonValueNames))
                        {
                            addonValueNames += "<br>";
                        }

                        addonValueNames += _addonValue.Name + " ( " + this.FormatPrice(_addonValue.RetailPrice.ToString()) + " )";
                    }
                }
            }

            return addonValueNames;
        }

        /// <summary>
        /// Returns formatted price to the grid
        /// </summary>
        /// <param name="productPrice">Product price to format.</param>
        /// <returns>Returns the formatted product price.</returns>
        protected string FormatPrice(string productPrice)
        {
            if (productPrice != null)
            {
                if (productPrice.ToString().Length > 0)
                {
                    return decimal.Parse(productPrice.ToString()).ToString("c");
                }
            }

            return string.Empty;
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Gets the name of the Addon for this addOnId
        /// </summary>
        /// <param name="addOnId">AddOn Id to get the add on object</param>
        /// <returns>Returns the add on name.</returns>
        private string GetAddOnName(object addOnId)
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOn addOn = productAddOnAdmin.GetByAddOnId(int.Parse(addOnId.ToString()));

            if (addOn != null)
            {
                return addOn.Name;
            }

            return string.Empty;
        }

        #endregion

        #region Bind Events
        /// <summary>
        /// Bind AddOn grid with filtered data
        /// </summary>
        private void SearchAddons()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            int localeId = 0;
            int accountId = 0;
            AccountService accountService = new AccountService();
            TList<Account> accountList = accountService.GetByUserID((Guid)Membership.GetUser().ProviderUserKey);
            if (accountList != null && accountList.Count > 0)
            {
                accountId = accountList[0].AccountID;
            }

            uxAddOnGrid.DataSource = productAddOnAdmin.SearchAddOns(Server.HtmlEncode(txtAddonName.Text.Trim()), Server.HtmlEncode(txtAddOnTitle.Text.Trim()), txtAddOnsku.Text.Trim(), localeId, accountId);
            uxAddOnGrid.DataBind();
        }

        /// <summary>
        /// Bind the product name.
        /// </summary>
        private void BindProductName()
        {
            ProductAdmin ProductAdminAccess = new ProductAdmin();
            Product product = ProductAdminAccess.GetByProductId(this.itemId);
            if (product != null)
            {
                lblTitle.Text = lblTitle.Text + this.GetGlobalResourceObject("ZnodeAdminResource", "TextFor").ToString() + "\"" + product.Name + "\"";
            }
        }

        /// <summary>
        /// Bind Addon Grid - all Addons
        /// </summary>
        private void BindAddons()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            int accountId = 0;
            AccountService accountService = new AccountService();
            TList<Account> accountList = accountService.GetByUserID((Guid)Membership.GetUser().ProviderUserKey);
            if (accountList != null && accountList.Count > 0)
            {
                accountId = accountList[0].AccountID;
            }

            // List of Addons
            uxAddOnGrid.DataSource = productAddOnAdmin.GetAddonsByAccountId(accountId);
            uxAddOnGrid.DataBind();
        }
        #endregion
    }
}