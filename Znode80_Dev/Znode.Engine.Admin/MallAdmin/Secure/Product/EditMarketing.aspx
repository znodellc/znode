﻿<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/content.master" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Admin_Secure_Product_EditMarketing"
    ValidateRequest="false" CodeBehind="EditMarketing.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ID="btnSubmitTop" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true" />
            <zn:Button runat="server" ID="btnCancelTop" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
        <div style="clear: both">
            <asp:Label ID="Label1" CssClass="Error" runat="server"></asp:Label>
        </div>
        <div class="FormView">
            <div>
                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
            </div>
            <h4 class="SubTitle">
                <asp:Localize ID="Display" Text='<%$ Resources:ZnodeAdminResource, SubTitleDisplay %>' runat="server"></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="NewIcon" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNewIcon %>'></asp:Localize><br />
                <small>
                    <asp:Localize ID="TextNewItemIcon" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextNewItemIcon %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkIsNewItem" ToolTip="New Item Icon" runat="server" />
            </div>
            <div class="ClearBoth">
            </div>
            <br />
            <h4 class="SubTitle">
                <asp:Localize ID="TitleSEO" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSEO %>'></asp:Localize></h4>
            <uc1:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            <div>
                <asp:Localize ID="TextSEO" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextSEO %>'></asp:Localize>
            </div>
            <uc1:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            <div class="FieldStyle">
                <asp:Localize ID="SEOTitleText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSETitle %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOTitle" runat="server" ToolTip="Search Engines Title" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="SEOKeyword" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEKeyword %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaKeywords" runat="server" ToolTip="Search Engine Keywords" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="SEODescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEDescription %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaDescription" runat="server" ToolTip="Search Engine Description" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="SEOUrl" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOURL %>'></asp:Localize><br />
                <small>
                    <asp:Localize ID="SEOHintText" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextSEOfriendlyPageName %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOUrl" ToolTip="Search Engine URL" runat="server" MaxLength="100" Columns="50"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSEOUrl"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter a valid SEO friendly name"
                    SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <div style="text-align: left">
            <zn:Button runat="server" ID="btnSubmitBottom" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource,ButtonSubmit%>' CausesValidation="True" />
            <zn:Button runat="server" ID="btnCancelBottom" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource,ButtonCancel%>' CausesValidation="False" />

        </div>
    </div>


</asp:Content>
