﻿using System;
using System.Web.UI.WebControls;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_Reports_Report class.
    /// </summary>
    public partial class Admin_Secure_Reports_Report : System.Web.UI.Page
    {
        private const string ReportProductsSoldOnVendorSites = "Products Sold on Vendors Sites";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Mode"] != null)
            {
                Session["SelectedMenuItem"] = Request.QueryString["Mode"].ToString();
                Response.Redirect("Report.aspx");
            }

            if (Session["SelectedMenuItem"] != null)
            {
                this.BindReports(Session["SelectedMenuItem"].ToString());
                Session["SelectedMenuItem"] = null;
            }
            
            if (!IsPostBack)
            {
                hdnSelectedReport.Value = "Orders";
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            ctrlSubMenu.DataBind();

            GC.Collect();
        }

        /// <summary>
        /// Get the submenu Css
        /// </summary>
        /// <param name="url">The url value to check</param>
        /// <returns>Returns the css class name.</returns>
        protected string GetSubmenuCss(object url)
        {
            if (hdnSelectedReport.Value == url.ToString())
            {
                return "ReportSelectedStyle";
            }
            else
            {
                return "ReportMenuItemStyle";
            }
        }

        /// <summary>
        /// Get the submenu name
        /// </summary>
        /// <param name="url">The Url to check</param>
        /// <returns>Returns the formatted url.</returns>
        protected string GetSubmenuName(object url)
        {
            if (hdnSelectedReport.Value == url.ToString())
            {
                return url.ToString() + " &raquo";
            }
            else
            {
                return url.ToString();
            }
        }

        protected void SubMenu_OnClick(object sender, EventArgs e)
        {
            this.ClearReportSession();
            LinkButton obj = sender as LinkButton;
            this.BindReports(obj.CommandArgument);
        }

        /// <summary>
        /// Bind the report.
        /// </summary>
        /// <param name="reportName">Report name to check.</param>
        private void BindReports(string reportName)
        {
            reportName = "Products Sold on Vendors Sites";
            uxProductsSoldOnVendorSitesReport.Visible = false;
            switch (reportName)
            {
                case ReportProductsSoldOnVendorSites:
                    uxProductsSoldOnVendorSitesReport.Visible = true;
                    uxProductsSoldOnVendorSitesReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.ProductSoldOnVendorSites;
                    uxProductsSoldOnVendorSitesReport.IsPostback = "false";
                    uxProductsSoldOnVendorSitesReport.DataBind();
                    hdnSelectedReport.Value = "Products Sold on Vendors Sites";
                    uxProductsSoldOnVendorSitesReport.ReportTitle = hdnSelectedReport.Value;
                    break;
            }
        }

        /// <summary>
        /// Clear the report data session
        /// </summary>
        private void ClearReportSession()
        {
            if (Session.Count > 0 && Session != null)
            {
                for (int reportSessionIndex = 0; reportSessionIndex < Session.Count; reportSessionIndex++)
                {
                    if (Session[reportSessionIndex] != null && Session[reportSessionIndex].GetType().ToString().Contains("Microsoft.Reporting.WebForms.ReportHierarchy"))
                    {
                        Session.RemoveAt(reportSessionIndex);
                    }
                }
            }

            GC.Collect();
        }
    }
}