﻿using Microsoft.Reporting.WebForms;
using System;
using System.Data;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.UserAccount;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Franchise Admin ProductsSoldOnVendorSitesReport class.
    /// </summary>
    public partial class ProductsSoldOnVendorSitesReport : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private string _ReportTitle; 
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the conrol postback settings
        /// </summary>
        public string IsPostback
        {
            get { return hdnIsPostBack.Value; }
            set { hdnIsPostBack.Value = value; }
        }
        
        /// <summary>
        /// Gets or sets the report title
        /// </summary>
        public string ReportTitle
        {
            get 
            {
                return this._ReportTitle; 
            }

            set
            {
                this._ReportTitle = value;
                lblTitle.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the report mode.
        /// </summary>
        public ZnodeReport Mode
        {
            get
            {
                string mode = hdnMode.Value;
                ZnodeReport report = ZnodeReport.ServiceRequest;
                if (Enum.IsDefined(typeof(ZnodeReport), mode))
                {
                    report = (ZnodeReport)Enum.Parse(typeof(ZnodeReport), hdnMode.Value, true);
                }

                return report;
            }

            set 
            { 
                hdnMode.Value = value.ToString(); 
            }
        }

        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
            {
                objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
            }

            if (string.IsNullOrEmpty(txtStartDate.Text))
            {
                txtStartDate.Text = string.Format("{0}/01/{1}", DateTime.Today.Month, DateTime.Today.Year);
            }

            if (string.IsNullOrEmpty(txtEndDate.Text))
            {
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(hdnIsPostBack.Value))
            {
                hdnIsPostBack.Value = "true";
            }
        }

        #endregion

        protected void BtnClear_Click(object sender, EventArgs e)
        {
            hdnIsPostBack.Value = "false";
            txtEndDate.Text = string.Empty;
            txtStartDate.Text = string.Empty;
			this.objReportViewer.Visible = false;
        }

        protected void BtnOrderFilter_Click(object sender,EventArgs e)
        {
            this.ShowReport();
        }

        #region Helper Methods

        /// <summary>
        /// Display the report based on the parameters.
        /// </summary>
        private void ShowReport()
        {
            objReportViewer.Visible = true;
            ReportAdmin reportAdmin = new ReportAdmin();
            DataSet reportDataSet = null;
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;

            if (!string.IsNullOrEmpty(txtStartDate.Text))
            {
                startDate = DateTime.Parse(txtStartDate.Text.Trim());
            }

            if (!string.IsNullOrEmpty(txtEndDate.Text))
            {
                endDate = DateTime.Parse(txtEndDate.Text.Trim());
            }

            string accountId = string.Empty;
            ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();
            if (userAccount != null)
            {
                accountId = userAccount.AccountID.ToString();
            }

            reportDataSet = reportAdmin.ReportList(this.Mode, startDate, endDate, string.Empty, "0", accountId);

            if (reportDataSet.Tables[0].Rows.Count == 0)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                objReportViewer.Visible = false;
                return;
            }
            else
            {
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.ActivityLogReport, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.Mode.ToString() + this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogInventoryReport"), this.Mode.ToString());

                objReportViewer.Visible = true;
                objReportViewer.LocalReport.DataSources.Clear();
                if (this.Mode == ZnodeReport.ProductSoldOnVendorSites)
                {
                    this.objReportViewer.LocalReport.ReportPath = "MallAdmin/Secure/Reports/ProductsSoldOnVendorSitesReport.rdlc";
                    objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZNode_ReportsProductSoldOnVendorSites", reportDataSet.Tables[0]));
                }
                this.objReportViewer.PageCountMode = PageCountMode.Actual; 
                objReportViewer.LocalReport.Refresh();
            }
        }
        #endregion
    }
}