﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/content.master"
    AutoEventWireup="True" EnableEventValidation="false" Inherits="Znode.Engine.MallAdmin.Admin_Secure_Reports_Report" CodeBehind="Report.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="ProductsSoldOnVendorSitesReport" Src="~/MallAdmin/Secure/Reports/ProductsSoldOnVendorSitesReport.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h1><asp:Localize runat="server" ID="LinkTextReports" Text='<%$ Resources:ZnodeAdminResource, LinkReports %>'></asp:Localize></h1>

    <div style="height: 5px;">
    </div>
    <div style="display: block;">
        <div class="ReportLeft">
            <asp:SiteMapDataSource ID="SiteMapDataSource2" runat="server" ShowStartingNode="False"
                SiteMapProvider="ZNodeMallAdminSiteMap" StartingNodeUrl="~/MallAdmin/Secure/Reports/Report.aspx" />
            <asp:DataList runat="server" ID="ctrlSubMenu" RepeatDirection="Horizontal" RepeatLayout="Flow"
                CssClass="ReportMenuStyle" RepeatColumns="1" DataSourceID="SiteMapDataSource2"
                ItemStyle-Wrap="true">
                <ItemStyle Width="200px" />
                <ItemTemplate>
                    <span class='<%# GetSubmenuCss(Eval("Title")) %>'>
                        <asp:LinkButton runat="server" ID="SubMenu" onmouseover="this.innerText += ' &raquo;';"
                            onmouseout="this.innerText = this.innerText.replace(' &raquo;','');" Text='<%# GetSubmenuName(Eval("Title")) %>'
                            CommandArgument='<%# Eval("Title") %>' OnClick="SubMenu_OnClick"></asp:LinkButton>
                    </span>
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="ReportRight">
            <div class="Reports">
                <uc1:ProductsSoldOnVendorSitesReport runat="server" ID="uxProductsSoldOnVendorSitesReport" Mode="268" Visible="false" />
            </div>
        </div>
        <div style="clear: both"></div>
        <div class="ReportLeft">&nbsp;</div>
        <div class="ReportRight">
            <div class="FormView">
                <div class="FieldStyle" style="width: 100%; padding-top: 25px;">
                    <small style="width: 100%;">
                        <asp:Localize runat="server" ID="TextReport" Text='<%$ Resources:ZnodeAdminResource, HintTextReport %>'></asp:Localize>
                    </small>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnSelectedReport" Value="Orders" />
</asp:Content>
