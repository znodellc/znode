﻿<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/edit.master" AutoEventWireup="true"
    Inherits="Znode.Engine.MallAdmin.Admin_Secure_Account_EditAccount" ValidateRequest="false"
    CodeBehind="EditAccount.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView" align="center">
        <div>
            <div class="LeftFloat">
                <h1>
                    <asp:Label ID="lblTitle" Text='<%$ Resources:ZnodeAdminResource, TitleEditAccount%>' runat="server"></asp:Label>
                </h1>
            </div>
            <div style="float: right">
                <zn:Button runat="server" ID="btnSubmitTop" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' />
                <zn:Button ID="btnCancelTop" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
            </div>
            <div class="ClearBoth">
            </div>
            <div align="left">
                <div>
                    <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>
                </div>
                <div>
                    <uc1:Spacer ID="Spacer8" SpacerHeight="2" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <h4 class="SubTitle">
                    <asp:Localize runat="server" ID="SubTitleGeneral" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneral%>'></asp:Localize></h4>
                <!-- Update Panel for profiles drop down list to avoid the postbacks -->
                <asp:UpdatePanel ID="updPnlRealtedGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="FieldStyle">
                            <asp:Localize runat="server" ID="ColumnTitleVendor" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendorID1%>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtExternalAccNumber" runat="server" Width="150px" Enabled="false"></asp:TextBox>
                        </div>
                        <div>
                            <div class="FieldStyle">
                                <asp:Localize runat="server" ID="ColumnTitleCompany" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCompanyName%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtCompanyName" runat="server" />
                            </div>
                        </div>
                        <div>
                            <div class="FieldStyle">
                                <asp:Localize runat="server" ID="ColumnTitleWebsite" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleWebsite%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtWebSite" runat="server" />
                            </div>
                        </div>
                        <div>
                            <div class="ClearBoth">
                                <div id="dvLoginInformation" runat="server" class="LeftFloat">
                                    <h4 class="SubTitle">
                                        <asp:Localize runat="server" ID="SubTitleLogin" Text='<%$ Resources:ZnodeAdminResource, SubTitleLoginInformation%>'></asp:Localize></h4>
                                    <div class="FieldStyle">
                                        <asp:Localize runat="server" ID="ColumnTitleUserID" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleUserID%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:TextBox ID="txtUserId" runat="server" Enabled="false" />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="ClearBoth">
                            <div id="shippingaddr" runat="server" class="LeftFloat">
                                <h4 class="SubTitle">
                                    <asp:Localize runat="server" ID="ColumnShippingAddress" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingAddress%>'></asp:Localize></h4>
                                <div class="FieldStyle">
                                    <asp:Localize runat="server" ID="ColumnTitleFirstName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFirstName%>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="txtShippingFirstName" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize runat="server" ID="ColumnTitleLastName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLastName%>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="txtShippingLastName" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize runat="server" ID="ColumnTitleCompanyName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCompanyName%>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="txtShippingCompanyName" runat="server" Width="130" Columns="30"
                                        MaxLength="100"></asp:TextBox>
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize runat="server" ID="ColumnTitlePhone" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePhoneNumber%>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="txtShippingPhoneNumber" runat="server" Width="130" Columns="30"
                                        MaxLength="20"></asp:TextBox>
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize runat="server" ID="ColumnEmailAddress" Text='<%$ Resources:ZnodeAdminResource, ColumnEmailAddress%>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="txtShippingEmail" runat="server" Width="131px"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" ControlToValidate="txtShippingEmail"
                                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularEmailAddress%>' Display="Dynamic" ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularValidEmailExpression%>'
                                        ValidationGroup="EditContact" CssClass="Error"></asp:RegularExpressionValidator>
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize runat="server" ID="ColumnStreet1" Text='<%$ Resources:ZnodeAdminResource, ColumnStreet1%>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="txtShippingStreet1" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize runat="server" ID="ColumnStreet2" Text='<%$ Resources:ZnodeAdminResource, ColumnStreet2%>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="txtShippingStreet2" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize runat="server" ID="ColumnTitleCity" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCity%>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="txtShippingCity" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize runat="server" ID="ColumnTitleState" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleState%>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="txtShippingState" runat="server" Width="30" Columns="10" MaxLength="2"></asp:TextBox>
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize runat="server" ID="ColumnPostalCode" Text='<%$ Resources:ZnodeAdminResource, ColumnPostalCode%>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="txtShippingPostalCode" runat="server" Width="130" Columns="30" MaxLength="20"></asp:TextBox>
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize runat="server" ID="ColumnCountry" Text='<%$ Resources:ZnodeAdminResource, ColumnCountry%>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:DropDownList ID="lstShippingCountryCode" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <div class="FieldStyle">
                                </div>
                                <div class="ValueStyleText">
                                    <asp:CheckBox ID="chkOptIn" runat="server" Text='<%$ Resources:ZnodeAdminResource, CheckBoxEmailOptIn%>' />
                                </div>
                            </div>
                        </div>
                        <uc1:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <uc1:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                <uc1:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
                <div class="ClearBoth">
                    <br />
                    <zn:Button runat="server" ID="btnSubmitBottom" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
                    <zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="ExistingUID" Value="" />
</asp:Content>
