﻿using System;
using System.Data;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_Account_EditAccount class.
    /// </summary>
    public partial class Admin_Secure_Account_EditAccount : System.Web.UI.Page
    {
        #region Private Member Variables
        private int accountId;
        private string sCompanyName;
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();
            MembershipUser user = Membership.GetUser(userAccount.UserID);

            this.accountId = userAccount.AccountID;

            if (!Page.IsPostBack)
            {
                this.BindCountry();
                this.BindData();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            // Validate shipping state code
            if ((lstShippingCountryCode.SelectedValue == "US") && (!string.IsNullOrEmpty(txtShippingState.Text.Trim())))
            {
                if (txtShippingState.Text.Trim().Length > 2 || txtShippingState.Text.Trim().Length < 2)
                {
                    return;
                }
            }

            AccountService accountService = new AccountService();
            TList<Account> accountList = accountService.Find(string.Format("ExternalAccountNo='{0}'", txtExternalAccNumber.Text.Trim()));
            if (accountList.Count > 0 && (ExistingUID.Value != txtExternalAccNumber.Text))
            {
                lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorVendorIdExist").ToString(), txtExternalAccNumber.Text);
                return;
            }

            ZNode.Libraries.Admin.AccountAdmin accountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account account = new Account();

            if (this.accountId > 0)
            {
                account = accountAdmin.GetByAccountID(this.accountId);
                account.CreateDte = System.DateTime.Now;
            }

            account.EmailOptIn = chkOptIn.Checked;
            account.Email = txtShippingEmail.Text;

            sCompanyName = account.CompanyName;

            // Set Account Details
            account.ExternalAccountNo = txtExternalAccNumber.Text.Trim();
            account.CompanyName = Server.HtmlEncode(txtCompanyName.Text);
            account.Website = txtWebSite.Text.Trim();

            account.AccountProfileCode = DBNull.Value.ToString();
            account.UpdateUser = System.Web.HttpContext.Current.User.Identity.Name;
            account.UpdateDte = System.DateTime.Now;

            account.UserID = account.UserID;
            account.AccountID = this.accountId;

            // Custom Information Section
            account.Custom1 = string.Empty;

            bool isSuccess = false;
            bool isUpdate = false;

            if (this.accountId > 0)
            {
                // set update date
                account.UpdateDte = System.DateTime.Now;

                isSuccess = accountAdmin.Update(account);

                // Get Shipping field values
                Address defaultShippingAddress = accountAdmin.GetDefaultShippingAddress(this.accountId);
                defaultShippingAddress.FirstName = Server.HtmlEncode(txtShippingFirstName.Text);
                defaultShippingAddress.LastName = Server.HtmlEncode(txtShippingLastName.Text);
                defaultShippingAddress.CompanyName = Server.HtmlEncode(txtShippingCompanyName.Text);
                defaultShippingAddress.Street = Server.HtmlEncode(txtShippingStreet1.Text);
                defaultShippingAddress.Street1 = Server.HtmlEncode(txtShippingStreet2.Text);
                defaultShippingAddress.City = Server.HtmlEncode(txtShippingCity.Text);
                defaultShippingAddress.StateCode = Server.HtmlEncode(txtShippingState.Text);
                defaultShippingAddress.PostalCode = txtShippingPostalCode.Text;
                if (lstShippingCountryCode.SelectedIndex != -1)
                {
                    defaultShippingAddress.CountryCode = lstShippingCountryCode.SelectedValue;
                }

                defaultShippingAddress.PhoneNumber = txtShippingPhoneNumber.Text;
                AddressService addressService = new AddressService();
                if (defaultShippingAddress.AddressID > 0)
                {
                    addressService.Update(defaultShippingAddress);
                }

                // Log Activity
                string associateName = "Edit of Account - " + account.UserID;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, "Account");
            }

            if (isSuccess)
            {
                SupplierAdmin supplierAdmin = new SupplierAdmin();

                TList<Supplier> supplier = DataRepository.SupplierProvider.GetByExternalSupplierNoName(txtExternalAccNumber.Text, sCompanyName);

                if (supplier != null)
                {
                    supplier[0].Name = txtCompanyName.Text.Trim();
                    supplier[0].Description = string.Empty;
                    supplier[0].ContactFirstName = txtShippingFirstName.Text.Trim();
                    supplier[0].ContactLastName = txtShippingLastName.Text.Trim();
                    supplier[0].ContactPhone = txtShippingPhoneNumber.Text;
                    supplier[0].ContactEmail = txtShippingEmail.Text;
                    supplier[0].NotificationEmailID = txtShippingEmail.Text;
                    supplier[0].EmailNotificationTemplate = string.Empty;
                    supplier[0].EnableEmailNotification = true;
                    supplier[0].DisplayOrder = 500;
                    supplier[0].ActiveInd = true;
                    supplier[0].Custom1 = string.Empty;
                    supplier[0].Custom2 = string.Empty;
                    supplier[0].Custom3 = string.Empty;
                    supplier[0].Custom4 = string.Empty;
                    supplier[0].Custom5 = string.Empty;
                    supplier[0].ExternalSupplierNo = txtExternalAccNumber.Text.Trim();
                    // Assign Default Supplier Type Id
                    supplier[0].SupplierTypeID = 1;

                    //Update the supplier table
                    isUpdate = supplierAdmin.Update(supplier[0]);
                }
            }

            if (isSuccess)
            {
                string script = "alert('" + this.GetGlobalResourceObject("ZnodeAdminResource", "TextAlertSaved").ToString() + "'); location.href='" + Page.ResolveClientUrl("~/MallAdmin/Secure/Product/List.aspx") + "';";

                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Test", script, true);
            }
            else
            {
                lblErrorMsg.Text = string.Empty;
            }
        }

        /// <summary>
        ///  Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.accountId > 0)
            {
                Response.Redirect("~/MallAdmin/Secure/Product/List.aspx");
            }
        }
        
        #endregion

        #region Bind Data

        /// <summary>
        /// Bind Account Details
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.AccountAdmin accountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account account = accountAdmin.GetByAccountID(this.accountId);
            CustomerAdmin customerAdmin = new CustomerAdmin();

            if (account != null)
            {
                // General Information
                txtExternalAccNumber.Text = account.ExternalAccountNo;
                ExistingUID.Value = account.ExternalAccountNo;
                txtCompanyName.Text = Server.HtmlDecode(account.CompanyName);
                txtWebSite.Text = account.Website;
                txtUserId.Text = Membership.GetUser().UserName;
                chkOptIn.Checked = account.EmailOptIn;

                Address defaultShippingAddress = accountAdmin.GetDefaultShippingAddress(this.accountId);
                if (defaultShippingAddress == null)
                {
                    defaultShippingAddress = new Address();
                }

                txtShippingFirstName.Text = Server.HtmlDecode(defaultShippingAddress.FirstName);
                txtShippingLastName.Text = Server.HtmlDecode(defaultShippingAddress.LastName);
                txtShippingCompanyName.Text = Server.HtmlDecode(defaultShippingAddress.CompanyName);
                txtShippingStreet1.Text = Server.HtmlDecode(defaultShippingAddress.Street);
                txtShippingStreet2.Text = Server.HtmlDecode(defaultShippingAddress.Street1);
                txtShippingCity.Text = Server.HtmlDecode(defaultShippingAddress.City);
                txtShippingState.Text = Server.HtmlDecode(defaultShippingAddress.StateCode);
                txtShippingPostalCode.Text = defaultShippingAddress.PostalCode;

                ListItem listItem = lstShippingCountryCode.Items.FindByValue(defaultShippingAddress.CountryCode);
                if (listItem != null)
                {
                    lstShippingCountryCode.SelectedIndex = lstShippingCountryCode.Items.IndexOf(listItem);
                }

                txtShippingPhoneNumber.Text = defaultShippingAddress.PhoneNumber;
                txtShippingEmail.Text = account.Email;
            }
        }

        /// <summary>
        /// Binds country drop-down list
        /// </summary>
        private void BindCountry()
        {
            // Portal Country
            PortalCountryHelper portalCountryHelper = new PortalCountryHelper();
            DataSet countryDataSet = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

            DataView ShippingView = new DataView(countryDataSet.Tables[0]);
            ShippingView.RowFilter = "ShippingActive = 1";

            // Shipping Drop Down List
            lstShippingCountryCode.DataSource = ShippingView;
            lstShippingCountryCode.DataTextField = "Name";
            lstShippingCountryCode.DataValueField = "Code";
            lstShippingCountryCode.DataBind();
            ListItem listItem = lstShippingCountryCode.Items.FindByValue("US");
            if (listItem != null)
            {
                lstShippingCountryCode.SelectedIndex = lstShippingCountryCode.Items.IndexOf(listItem);
            }
        }

        #endregion
    }
}