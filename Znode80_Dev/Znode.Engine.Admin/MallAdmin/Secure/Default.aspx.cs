using System;
using System.Web;
using ZNode.Libraries.Admin;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_Default class.
    /// </summary>
    public partial class Admin_Secure_Default : System.Web.UI.Page
    {
        private DashboardAdmin dashAdmin = new DashboardAdmin();
        private string multifrontUrl = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["SelectedMenuItem"] = null;

            this.BindData();

            Response.Redirect("~/MallAdmin/Secure/Product/List.aspx");
        }

        /// <summary>
        /// Bind data to the dashboard
        /// </summary>
        protected void BindData()
        {
            // Set the IFrame to https if we are on a secure connection.
            string prefix = "http://";
            if (Request.IsSecureConnection)
            {
                prefix = "https://";
            }

            // Gget multifront path
            this.multifrontUrl = prefix + HttpContext.Current.Request.ServerVariables.Get("HTTP_HOST") + HttpContext.Current.Request.ApplicationPath; 
        }        
    }
}