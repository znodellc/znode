﻿using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.UserAccount;
using Znode.Engine.Common;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the MallAdmin - Admin_Secure_sales_orders_list class
    /// </summary>
    public partial class Admin_Secure_Sales_Orders_list : System.Web.UI.Page
    {
        #region Private Variables
        private static bool SearchEnabled = false;
        private string ViewOrderLink = "~/MallAdmin/Secure/sales/orders/view.aspx?itemid=";
        private DataSet MyDataSet = null;
        private string tabDelimeter = ",";
        private string PortalIds = string.Empty;
        private string FirstName = null;
        private string LastName = null;
        private string CompanyName = null;
        private string AccountNumber = null;
        private DateTime? StartDate = null;
        private DateTime? EndDate = null;
        private int? OrderStateID = null;
        private int? PortalID = null;
        private int? OrderId = null;
        private ZNodeUserAccount _userAccount;
        private ZNodeEncryption encryption = new ZNodeEncryption();

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the SortField property is tracked in ViewState
        /// </summary>
        public string SortField
        {
            get
            {
                object o = ViewState["SortField"];
                if (o == null)
                {
                    return String.Empty;
                }
                
                return (string)o;
            }

            set
            {
                if (value == this.SortField)
                {
                    // Same as current sort file, toggle sort direction
                    this.SortAscending = !this.SortAscending;
                }
                
                ViewState["SortField"] = value;
            }
        }

       /// <summary>
       /// Gets or sets a value indicating whether SortAscending property is tracked in ViewState
       /// </summary>
        public bool SortAscending
        {
            get
            {
                object o = ViewState["SortAscending"];
                if (o == null)
                {
                    return true;
                }
                
                return (bool)o;
            }

            set
            {
                ViewState["SortAscending"] = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Includes Javascript file and css file into this page
        /// </summary>
        public void RegisterClientScript()
        {
            // Include the Client Side Script from the resource file
            // The Resource File is named “Calender.js”
            // Located inside the Calendar directory
            HtmlGenericControl Include = new HtmlGenericControl("script");
            Include.Attributes.Add("type", "text/javascript");
            Include.Attributes.Add("src", "Calendar/Calendar.js");

            // The Resource File is named “Calender.css”
            // Located inside the Calendar directory
            HtmlGenericControl Include1 = new HtmlGenericControl("link");
            Include1.Attributes.Add("type", "text/css");
            Include1.Attributes.Add("rel", "stylesheet");
            Include1.Attributes.Add("href", "Calendar/Calendar.css");

            // Add a script reference for Javascript to the head section
            this.Page.Header.Controls.Add(Include);
            this.Page.Header.Controls.Add(Include1);
        }

        /// <summary>
        /// Get Highest OrderId Method
        /// </summary>
        public void GetHighestOrderId()
        {
            OrderAdmin _OrderAdmin = new OrderAdmin();
            int OrderId = _OrderAdmin.GetHighestOrderId(UserStoreAccess.GetAvailablePortals);
            OrderNumber.Text = OrderId.ToString();
        }

        /// <summary>
        /// Bind Search Data Method
        /// </summary>
        public void BindSearchData()
        {
            if (this.PortalIds.Length > 0)
            {
                OrderAdmin _OrderAdmin = new OrderAdmin();
                this.MyDataSet = _OrderAdmin.FindVendorOrders(this.OrderId, this.FirstName, this.LastName, this.CompanyName, this.AccountNumber, this.StartDate, this.EndDate, this.OrderStateID, this.PortalID, this.PortalIds, _userAccount.AccountID.ToString());

                DataView dv = new DataView(this.MyDataSet.Tables[0]);
                dv.Sort = "OrderID Desc";
                uxGrid.DataSource = dv;
                uxGrid.DataBind();
            }
            else
            {
                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDonthavePermission").ToString(); 
                ButOrderLineItems.Enabled = false;
                ButDownload.Enabled = false;
            }
        }

        /// <summary>
        /// Bind Data Method
        /// </summary>
        public void BindData()
        {
            ListOrderStatus.Items[0].Selected = true;
        }

        /// <summary>
        /// Contact first name and last name
        /// </summary>
        /// <param name="firstname">The value of firstname</param>
        /// <param name="lastName">The value of lastname</param>
        /// <returns>Returns the Name</returns>
        public string ReturnName(object firstname, object lastName)
        {
            return firstname.ToString() + " " + lastName.ToString();
        }

        /// <summary>
        /// Format the Price with two decimal
        /// </summary>
        /// <param name="fieldvalue">The value of fieldvalue</param>
        /// <returns>Returns the formatted price</returns>
        public string FormatPrice(object fieldvalue)
        {
            if (fieldvalue == DBNull.Value)
            {
                return string.Empty;
            }
            else
            {
                return "$" + fieldvalue.ToString().Substring(0, fieldvalue.ToString().Length - 2);
            }
        }

        /// <summary>
        /// Display the Order State name for a Order state
        /// </summary>
        /// <param name="fieldValue">The value of fieldValue</param>
        /// <returns>Returns the order status</returns>
        public string DisplayOrderStatus(object fieldValue)
        {
            ZNode.Libraries.Admin.OrderAdmin _OrderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            OrderState _OrderState = _OrderAdmin.GetByOrderStateID(int.Parse(fieldValue.ToString()));
            return _OrderState.OrderStateName.ToString();
        }

        /// <summary>
        /// Display the Payment type for the Order
        /// </summary>
        /// <param name="value">Represents the value</param>
        /// <returns>Returns the Payment Type</returns>
        public string DisplayPaymentType(object value)
        {
            if (value != null)
            {
                if (value.ToString().Length > 0)
                {
                    ZNode.Libraries.Admin.OrderAdmin _OrderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
                    PaymentType _type = _OrderAdmin.GetByPaymentTypeId(int.Parse(value.ToString()));
                    return _type.Name.ToString();
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Retrieves description of payment status
        /// </summary>
        /// <param name="value">Represents a value</param>
        /// <returns>Returns the Payment Status</returns>
        public string DisplayPaymentStatus(object value)
        {
            if (value != null)
            {
                if (value.ToString().Length > 0)
                {
                    PaymentStatusService serv = new PaymentStatusService();
                    PaymentStatus ps = serv.GetByPaymentStatusID(int.Parse(value.ToString()));

                    return ps.PaymentStatusName.ToString();
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Enable or disable refund button
        /// </summary>
        /// <param name="value">Represents a value</param>
        /// <param name="value1">Represents a value1</param>
        /// <returns>Returns enable or disable refund button</returns>
        public bool EnableRefund(object value, object value1)
        {
            bool flag = false;
            if (value != null)
            {
                if (value.ToString().Length > 0)
                {
                    if (int.Parse(value.ToString()) == 0 && (int.Parse(value1.ToString()) != 3))
                    {
                        flag = true;
                    }
                    else
                    {
                        flag = false;
                    }
                }
            }
            
            return flag;
        }

        /// <summary>
        /// Enable or Disable Capture Button
        /// </summary>
        /// <param name="value">Represents a value</param>
        /// <returns>Returns true if Capture enable </returns>
        public bool EnableCapture(object value)
        {
            if (value != null)
            {
                if (value.ToString().Length > 0)
                {
                    if (int.Parse(value.ToString()) == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Returns the order line item status
        /// </summary>
        /// <param name="orderId">Represents a orderid as object</param>
        /// <returns>Returns the order line item status</returns>
        public string GetOrderLineItemStatus(object orderId)
        {
            if (orderId != null)
            {
                foreach (DataRow dr in this.MyDataSet.Tables[1].Select("OrderId=" + orderId.ToString()))
                {
                    if (dr["OrderLineItemStateID"] == System.DBNull.Value ||
                      (ZNode.Libraries.ECommerce.Entities.ZNodeOrderState)dr["OrderLineItemStateID"] ==
                                  ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.SUBMITTED)
                    {
                        //return "<a href=\"view.aspx?itemid=" + HttpUtility.UrlEncode(encryption.EncryptData(dr["OrderId"].ToString())) + "\">In Progress</a>";
                        return this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownListNotCompleted").ToString();
                    }
                }

                return this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownListCompleted").ToString();  
 
            }

            return string.Empty;     
        }

        /// <summary>
        /// Returns the order line item total
        /// </summary>
        /// <param name="orderId">Represents a orderid as object</param>
        /// <returns>Returns the order line item total</returns>
        public string GetOrderLineItemTotal(object orderId)
        {
            decimal orderLineItemTotal = 0;
            if (orderId != null)
            {
                foreach (DataRow dr in this.MyDataSet.Tables[0].Select("OrderId=" + orderId.ToString()))
                {
                    if (dr["Total"] != null)
                    {
                        orderLineItemTotal += (decimal)dr["Total"];
                    }
                }
            }

            return orderLineItemTotal.ToString("C");
        }

        /// <summary>
        /// Get Store Name by PortalId
        /// </summary>
        /// <param name="portalID">The value of portalID</param>
        /// <returns>Returns the Store Name</returns>
        public string GetStoreName(string portalID)
        {
            PortalAdmin portalAdmin = new PortalAdmin();

            return portalAdmin.GetStoreNameByPortalID(portalID);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ltrlError.Text = string.Empty;

            // Get Portals
            this.PortalIds = UserStoreAccess.GetAvailablePortals;

            _userAccount = (ZNodeUserAccount)ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount);

            if (!Page.IsPostBack)
            {
                lblError.Text = string.Empty;
                SearchEnabled = false;
                this.BindData();
                this.GetHighestOrderId();
                this.BindSearchData();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Search Button Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            short shortOrderId;
            if (txtorderid.Text.Length > 0 && Int16.TryParse(txtorderid.Text.Trim(), out shortOrderId))
            {
                this.OrderId = Int16.Parse(txtorderid.Text.Trim());
            }

            if (txtfirstname.Text.Length > 0)
            {
                this.FirstName = txtfirstname.Text.Trim();
            }

            if (txtlastname.Text.Length > 0)
            {
                this.LastName = txtlastname.Text.Trim();
            }

            if (txtcompanyname.Text.Length > 0)
            {
                this.CompanyName = txtcompanyname.Text.Trim();
            }

            if (txtaccountnumber.Text.Length > 0)
            {
                this.AccountNumber = txtaccountnumber.Text.Trim();
            }

            if (txtlastname.Text.Length > 0)
            {
                this.LastName = txtlastname.Text.Trim();
            }

            if (txtStartDate.Text.Length > 0)
            {
                this.StartDate = DateTime.Parse(txtStartDate.Text.Trim());
            }

            if (txtEndDate.Text.Length > 0)
            {
                this.EndDate = DateTime.Parse(txtEndDate.Text.Trim());
            }

            if (ListOrderStatus.SelectedValue != "0")
            {
                this.OrderStateID = Convert.ToInt32(ListOrderStatus.SelectedValue);
            }

            this.BindSearchData();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.ToString());
        }

        /// <summary>
        /// Download Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ButDownload_Click(object sender, EventArgs e)
        {
            lblError.Text = string.Empty;
            DataDownloadAdmin csv = new DataDownloadAdmin();
            OrderAdmin _OrderAdmin = new OrderAdmin();

            string Orderid = Convert.ToString(OrderNumber.Text);
            DataSet Orders = _OrderAdmin.GetVendorOrdersByOrderId(Orderid, _userAccount.AccountID.ToString());

            if (Orders.Tables[0].Rows.Count > 0)
            {
                // Set Formatted Data from DataSet           
                string strData = csv.Export(Orders, true, this.tabDelimeter);

                byte[] data = ASCIIEncoding.ASCII.GetBytes(strData);

                Response.Clear();
                
                // Set as Excel as the primary format
                Response.AddHeader("Content-Type", "application/Excel");

                Response.AddHeader("Content-Disposition", "attachment;filename=Order.csv");
                Response.ContentType = "application/vnd.xls";
                Response.BinaryWrite(data);

                Response.End();
            }
            else
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundOrder").ToString();  
                return;
            }
        }
      
        /// <summary>
        /// Order ListItem Buttton Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ButOrderLineItemsDownload_Click(object sender, EventArgs e)
        {
            DataDownloadAdmin csv = new DataDownloadAdmin();
            OrderAdmin _OrderAdmin = new OrderAdmin();
            DataSet OrderLineItems = new DataSet();
            DataTable dt = new DataTable();

            string Orderid = Convert.ToString(OrderNumber.Text);
            dt = _OrderAdmin.GetVendorOrderLineItemsByOrderId(Orderid, _userAccount.AccountID.ToString()).Tables[0];
            dt.Columns.Remove("PortalId");

            foreach (DataRow dr in dt.Rows)
            {
                dr["description"] = this.StripHTML(dr["description"].ToString());
            }
            
            OrderLineItems.Tables.Add(dt.Copy());

            if (OrderLineItems.Tables[0].Rows.Count > 0)
            {
                string strData = csv.Export(OrderLineItems, true, this.tabDelimeter);

                byte[] data = ASCIIEncoding.ASCII.GetBytes(strData);

                Response.Clear();
                Response.ClearHeaders();
                Response.ClearContent();
                
                // Set as Excel as the primary format
                Response.AddHeader("Content-Disposition", "attachment;filename=OrderLineItems.csv");
                Response.AddHeader("Content-Type", "application/Excel");
                Response.ContentType = "text/csv";
                Response.ContentType = "application/vnd.xls";
                Response.AddHeader("Pragma", "public");
                Response.BinaryWrite(data);
                Response.End();

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download OrderLineItems", "Order Line Items");
            }
            else
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundOrder").ToString();  
                return;
            }
        }
        #endregion
     
        #region Grid Events

        /// <summary>
        /// Method to sort the grid in Ascending and Descending Order
        /// </summary>   
        protected void SortGrid()
        {
            DateTime? StartDate = null;
            DateTime? EndDate = null;

            if (txtStartDate.Text.Length > 0)
            {
                StartDate = DateTime.Parse(txtStartDate.Text.Trim());
            }

            if (txtEndDate.Text.Length > 0)
            {
                EndDate = DateTime.Parse(txtEndDate.Text.Trim());
            }

            if (this.PortalIds.Length > 0)
            {
                OrderAdmin _OrderAdmin = new OrderAdmin();
                DataSet ds;

                ds = _OrderAdmin.FindVendorOrders(this.OrderId, this.FirstName, this.LastName, this.CompanyName, this.AccountNumber, this.StartDate, this.EndDate, this.OrderStateID, this.PortalID, this.PortalIds, _userAccount.AccountID.ToString());

                uxGrid.DataSource = ds;
                
                DataSet dataSet = uxGrid.DataSource as DataSet;

                DataView dataView = new DataView(dataSet.Tables[0]);

                // Apply sort filter and direction
                dataView.Sort = this.SortField;

                // If sortDirection is not Ascending
                if (!this.SortAscending)
                {
                    dataView.Sort += " DESC";
                }
                
                uxGrid.DataSource = null;
                uxGrid.DataSource = dataView;
                uxGrid.DataBind();
            }
            else 
            {
                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDonthavePermission").ToString();  
                ButOrderLineItems.Enabled = false;
                ButDownload.Enabled = false;
            }
        }

        /// <summary>
        /// Grid Paging Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        /// <summary>
        ///  Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {            
            if (e.CommandName == "Page")
            {
                
            }
            else
            {
                if (e.CommandName == "ViewOrder" || e.CommandName == "ViewID")
                {
                    this.ViewOrderLink = this.ViewOrderLink + HttpUtility.UrlEncode(encryption.EncryptData(e.CommandArgument.ToString())) + "&Pg=" + HttpUtility.UrlEncode(encryption.EncryptData("List"));
                    Response.Redirect(this.ViewOrderLink);
                }
            }
        }
       
        #endregion
      
        #region Private Methods
        /// <summary>
        /// Get Order Line Items Method
        /// </summary>
        /// <returns>Returns the Order Line Items Data Set</returns>
        private DataSet GetOrderLineItems()
        {
            OrderAdmin _OrderAdmin = new OrderAdmin();

            string stdate = String.Empty;
            string enddate = String.Empty;

            // Check for Search is enabled or not
            if (SearchEnabled)
            {
                stdate = txtStartDate.Text.Trim();
                enddate = txtEndDate.Text.Trim();
            }

            DataSet MyDataSet = _OrderAdmin.GetOrderLineItems(txtorderid.Text.Trim(), txtfirstname.Text.Trim(), txtlastname.Text.Trim(), txtcompanyname.Text.Trim(), txtaccountnumber.Text.Trim(), stdate, enddate, int.Parse(ListOrderStatus.SelectedValue.ToString()), ZNodeConfigManager.SiteConfig.PortalID);

            return MyDataSet;
        }

        /// <summary>
        /// Strip HTML method
        /// </summary>
        /// <param name="description">The value of description</param>
        /// <returns>Returns the HTMl string</returns>
        private string StripHTML(string description)
        {
            return System.Text.RegularExpressions.Regex.Replace(description, "<[^>]*>", " ");
        }
        #endregion

        protected void uxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            foreach (GridViewRow gvC in uxGrid.Rows)
            {
                Label OrderStatus = (Label)gvC.FindControl("OrderStatus");
                if (OrderStatus.Text == this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownListNotCompleted").ToString()  )
                {
                    OrderStatus.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTextPending").ToString();  
                    OrderStatus.ForeColor = System.Drawing.Color.Red;
                }
                else if (OrderStatus.Text == this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownListCompleted").ToString()  )
                {
                    OrderStatus.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextSUBMITTED").ToString();  
                }
            }
        }

        
    }
}
