<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.MallAdmin.Admin_Secure_sales_orders_OrderStatus" Codebehind="OrderStatus.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <h1>
            <asp:Localize ID="OrderLineItemStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleChangeOrderLineItemStatus %>'></asp:Localize>
</h1>
        <div style="margin-bottom: 20px;">
            <div>
                <b> <asp:Localize ID="ColumnTitleOrderLinItemID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderLineItemID %>'></asp:Localize></b>
                <asp:Label ID="lblOrderID" runat="server" /></div>
            <div>
                <b>  <asp:Localize ID="ColumnCustomerName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCustomerName %>'></asp:Localize> </b>
                <asp:Label ID="lblCustomerName" runat="server" /></div>
            <div>
                <b>  <asp:Localize ID="ColumnOrderTotal" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOrderTotal %>'></asp:Localize> </b>
                <asp:Label ID="lblTotal" runat="server" /></div>
        </div>
        <p>
            <asp:Localize ID="TextOrderStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextOrderStatus %>'></asp:Localize></p>
        <br />
        <div class="FieldStyle">
            <asp:DropDownList ID="ListOrderStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OrderStatus_SelectedIndexChanged" />
        </div>
        <br />
        <asp:Panel ID="pnlTrack" runat="server">
            <div class="FieldStyle">
                <asp:Label ID="TrackTitle" runat="server"></asp:Label></div>
            <div class="ValueStyle">
                <asp:TextBox ID="TrackingNumber" runat="server"></asp:TextBox></div>
            <div>
                <asp:Label ID="trackmessage" runat="server"></asp:Label><br /><br /></div>
        </asp:Panel>
        <div> 
         <zn:Button runat="server"  ID="EmailStatus" ButtonType="SubmitButton" OnClick="EmailStatus_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonEmailStatus%>'  />
         <zn:Button runat="server" ID="UpdateOrderStatus"  ButtonType="SubmitButton" OnClick="UpdateOrderStatus_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonUpdate%>'  />
         <zn:Button runat="server" ID="Cancel"  ButtonType="CancelButton" OnClick="CancelStatus_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' />
        </div>
        <uc1:Spacer ID="LongSpace" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
