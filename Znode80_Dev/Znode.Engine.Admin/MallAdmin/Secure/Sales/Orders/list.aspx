﻿<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/content.master" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Admin_Secure_Sales_Orders_list" CodeBehind="list.aspx.cs"%>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/MallAdmin/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div>
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>
                    <asp:Localize ID="LinkTextViewOrders" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextViewOrders %>'></asp:Localize></h1>
                 <p>
                    <asp:Localize ID="TextViewOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextViewOrder %>'></asp:Localize>
                 </p>
            </div>
            <div align="left">
                <h4 class="SubTitle">
                        <asp:Localize ID="SubTitleSearchOrders" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchOrders %>'></asp:Localize>
                </h4>
                <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
                    <div class="SearchForm">
                        <div class="RowStyle">
                            <div class="RowStyle">
                                <div class="ItemStyle">
                                    <span class="FieldStyle"> <asp:Localize ID="ColumnTitleOrderID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderID %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtorderid" runat="server"></asp:TextBox>
                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtorderid"
                                        Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredOrderID %>' SetFocusOnError="true"
                                        ValidationExpression="(\d)+" ValidationGroup="grpReports" CssClass="Error"></asp:RegularExpressionValidator>
                                        </span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle"> <asp:Localize ID="ColumnTitleFirstName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFirstName %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtfirstname" runat="server"></asp:TextBox></span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle"> <asp:Localize ID="ColumnTitleLastName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLastName %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtlastname" runat="server"></asp:TextBox></span>
                                </div>
                            </div>
                            <div class="RowStyle">
                                <div class="ItemStyle">
                                    <span class="FieldStyle"> <asp:Localize ID="ColumnTitleCompanyName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCompanyName %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtcompanyname" runat="server"></asp:TextBox></span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle"><asp:Localize ID="ColumnAccountID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAccountID %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtaccountnumber" runat="server"></asp:TextBox><br />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtaccountnumber"
                                            Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredBillingCycle %>' SetFocusOnError="true"
                                            ValidationExpression="(\d)+" ValidationGroup="grpReports" CssClass="Error"></asp:RegularExpressionValidator></span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle"> <asp:Localize ID="ColumnTitleOrderStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderStatus %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:DropDownList ID="ListOrderStatus" runat="server">
                                            <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextAll %>' Value="0"></asp:ListItem>
                                            <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownListNotCompleted %>' Value="10"></asp:ListItem>
                                            <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownListCompleted %>' Value="1000"></asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                </div>
                            </div>
                            <div class="RowStyle">
                                <div class="ItemStyle">
                                    <span class="FieldStyle"> <asp:Localize ID="ColumnTitleBeginDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBeginDate %>'></asp:Localize></p></span>
                                    <br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtStartDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="imgbtnStartDt"
                                            runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" /><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredBeginDate %>'
                                            ControlToValidate="txtStartDate" ValidationGroup="grpReports" CssClass="Error"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidDate %>'
                                            ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                                            ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                                        <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                                            runat="server" TargetControlID="txtStartDate">
                                        </ajaxToolKit:CalendarExtender>
                                    </span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle"> <asp:Localize ID="ColumnTitleEndDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEndDate %>'></asp:Localize></p></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtEndDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="ImgbtnEndDt"
                                            runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" /><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEndDate"
                                            ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterEnddate %>' ValidationGroup="grpReports" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
                                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularStartDate %>'
                                            ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                                            ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                                        <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt"
                                            runat="server" TargetControlID="txtEndDate">
                                        </ajaxToolKit:CalendarExtender>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
                                            ValidationGroup="grpReports"
                                            ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareBeginDate %>'
                                            Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                                    </span>
                                </div>
                            </div>
                            <div class="ClearBoth"> 

                                <zn:Button runat="server"  ID="btnClear" ButtonType="CancelButton" OnClick="BtnClearSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear%>' CausesValidation="False" />
                                <zn:Button runat="server" ID="btnSearch"  ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch%>' ValidationGroup="grpReports"/>
  
                            </div>
                        </div>
                    </div>
                </asp:Panel><br />
            </div>
            <div>
                <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label></div>
            <div><h4 class="SubTitle" style="margin-bottom: -8px;"> <asp:Localize ID="SubTitleOrderList" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleOrderList %>'></asp:Localize></h4>
                <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundOrders %>'
                    EnableSortingAndPagingCallbacks="False" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                    OnRowCommand="UxGrid_RowCommand" PageSize="25" Width="100%" OnRowDataBound="uxGrid_RowDataBound"  >
                    <Columns>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" >
                            <ItemTemplate>
                         <asp:LinkButton runat="server" CausesValidation="False" ID="ViewID" Text='<%# Eval("orderid") %>'
                                    CommandArgument='<%# Eval("orderid") %>' CommandName="ViewID" />
                             </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# GetStoreName(DataBinder.Eval(Container.DataItem,"PortalID").ToString()) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="OrderStateID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderStatus %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="OrderStatus" Text='<%# GetOrderLineItemStatus(DataBinder.Eval(Container.DataItem, "OrderId").ToString())%>'
                                    runat="server" Font-Bold="true" Font-Size="Smaller"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePaymentStatus %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="PaymentStatus" Text='<%# Eval("PaymentStatusName") %>'
                                    runat="server" Font-Bold="true" Font-Size="Smaller"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="BillingFirstName" HeaderStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                                <asp:Label ID="headerTotal" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="CustomerName" Text='<%# ReturnName(Eval("BillingFirstName"),Eval("BillingLastName")) %>'
                                    runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Orderdate" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDate %>' SortExpression="OrderDate" ItemStyle-Width="120"
                            DataFormatString="{0:MM/dd/yyyy hh:mm tt}" HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField SortExpression="total" HeaderText="Amount" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                 <%# GetOrderLineItemTotal(DataBinder.Eval(Container.DataItem, "OrderId").ToString())%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="PaymentTypeID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePaymentType %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="PaymentType" Text='<%# Eval("PaymentTypeName") %>'
                                    runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleActions %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="220px">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CausesValidation="False" ID="ViewOrder" Text="View &raquo"
                                    CommandArgument='<%# Eval("orderid") %>' CommandName="ViewOrder" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </div><br/>
             <div align="left">
                  <h4 class="SubTitle"> <asp:Localize ID="SubTitleDownloadOrders" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDownloadOrders %>'></asp:Localize></p>
                </h4>
                 <div align="left">
                    <div>
                        <div class="FieldStyle">
                             <asp:Localize ID="ColumnTitleStartingOrderID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStartingOrderID %>'></asp:Localize></p></div>
                        <small> <asp:Localize ID="SubTextHigherOrderID" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextHigherOrderID %>'></asp:Localize></p></small>
                    </div>
                    <div>
                        <asp:TextBox ID="OrderNumber" runat="server" MaxLength="9"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="OrderNumber"
                            ValidationGroup="Download" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredStartingOrderIdMust %>'></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="OrderNumber"
                            ValidationGroup="Download" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularSrartingOrderId %>'
                            ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    </div>
                    <div>
                        <uc1:Spacer ID="Spacer2" runat="server" SpacerHeight="10" />
                    </div>
                    <div>
                        <asp:Label ID="lblError" runat="server" Text="" CssClass="Error"></asp:Label>
                        <div class="Error">
                            <asp:Literal ID="ltrlError" runat="server"></asp:Literal></div> 
                            <zn:Button runat="server"  ID="ButDownload" ButtonType="EditButton" OnClick="ButDownload_Click"  ValidationGroup="Download" Text='<%$ Resources:ZnodeAdminResource, ButtonOrdersToCSV%>' />
                            <zn:Button runat="server"  ID="ButOrderLineItems" ButtonType="EditButton" OnClick="ButOrderLineItemsDownload_Click"  ValidationGroup="Download" Text='<%$ Resources:ZnodeAdminResource, ButtonDownloadOrderLineitems%>' />
                        <br />
                        <br />
                    </div>
                     
                </div>
                 </div>
        </div>
    </div>
</asp:Content>