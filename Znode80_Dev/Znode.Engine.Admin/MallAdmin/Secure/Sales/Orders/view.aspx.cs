﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.UserAccount;
using Znode.Engine.Common;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_orders_view class.
    /// </summary>
    public partial class Admin_Secure_Sales_Orders_view : System.Web.UI.Page
    {
        #region Protected variables
        private int OrderID = 0;
        private bool AdvancedShipping = false;
        private string ListPage = "list.aspx";
        private string statusPageLink = "~/MallAdmin/Secure/Sales/Orders/OrderStatus.aspx?itemid=";
        private string AccountListPage = "~/MallAdmin/secure/sales/customers/view.aspx?mode=order&itemid={0}";
        private string OrderTrackingNumber = string.Empty;
        private string filepath = ZNodeConfigManager.EnvironmentConfig.DataPath + "/ShippingLabels//FedEx//";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        private ZNodeUserAccount _userAccount;
        private string orderStatus = string.Empty;

        #endregion

        #region Public Methods
        /// <summary>
        /// Sets grid button Text
        /// </summary>
        /// <param name="trackingNumber"> The value of trackingnumber</param>
        /// <param name="orderlineItemId">The value of orderlineitemid</param>
        /// <returns> Returns the Button Text</returns>
        public string ButtonText(object trackingNumber, object orderlineItemId)
        {
            if (orderlineItemId != null)
            {
                // If the value for this row matches the just estimated line item we set the command to create
                if (orderlineItemId.ToString() == EstimatedLineItemID.Value)
                {
                    return "Create Shipment";  
                }

                if (trackingNumber != null)
                {
                    if (trackingNumber.ToString().Length > 0)
                    {
                        return "Cancel Shipment";
                    }
                }

                return "Estimate Dimensions";  
            }

            return string.Empty;
        }

        /// <summary>
        /// Sets the Command for the Ship seperately grid
        /// </summary>
        /// <param name="trackingNumber"> The value of trackingnumber</param>
        /// <param name="orderlineItemId">The value of orderlineitemid</param>
        /// <returns>Returns the Shipping command</returns>
        public string ShippingCommand(object trackingNumber, object orderlineItemId)
        {
            if (orderlineItemId != null)
            {
                // If the value for this row matches the just estimated line item we set the command to create
                if (orderlineItemId.ToString() == EstimatedLineItemID.Value)
                {
                    return "CreateShipment";  
                }

                if (trackingNumber != null)
                {
                    if (trackingNumber.ToString().Length > 0)
                    {
                        return "CancelShipment";  
                    }
                }

                return "EstimateDimensions";  
            }

            return string.Empty;
        }

        /// <summary>
        /// Checks to see if a label exists before displaying the label button
        /// </summary>
        /// <param name="value">Represents a value</param>
        /// <returns>Returns true or false</returns>
        public bool ShowLabelButton(object value)
        {
            if (value != null)
            {
                if (value.ToString().Length > 0)
                {
                    if (File.Exists(Server.MapPath(this.filepath) + value.ToString() + ".pdf") && this.AdvancedShipping)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Format Price Method
        /// </summary>
        /// <param name="fieldValue">The value of fieldValue</param>
        /// <returns>Returns the formatted price</returns>
        public string Formatprice(object fieldValue)
        {
            string Price = String.Empty;

            if (fieldValue != null)
            {
                Price = String.Format("{0:c}", fieldValue);
            }

            return Price;
        }

        /// <summary>
        /// Get Order State
        /// </summary>
        /// <param name="fieldValue">The value of fieldValue</param>
        /// <returns>Returns the string</returns>
        public string GetOrderState(object fieldValue)
        {
            string OrderStatus = " ";
            if (fieldValue != null)
            {
                ZNode.Libraries.Admin.OrderAdmin _OrderStateAdmin = new ZNode.Libraries.Admin.OrderAdmin();
                OrderState _orderStateList = _OrderStateAdmin.GetByOrderStateID(int.Parse(fieldValue.ToString()));

                OrderStatus = _orderStateList.OrderStateName.ToString();
            }

            return OrderStatus;
        }

        /// <summary>
        /// Returns shipping option name for this shipping Id
        /// </summary>
        /// <param name="shippingId">The value of shippingId</param>
        /// <returns>Returns the Shipping option Name</returns>
        public string GetShippingOptionName(int shippingId)
        {
            string Name = string.Empty;

            ZNode.Libraries.Admin.ShippingAdmin shippingAdmin = new ZNode.Libraries.Admin.ShippingAdmin();
			ZNode.Libraries.DataAccess.Entities.Shipping entity = shippingAdmin.GetShippingOptionById(shippingId);

            if (entity != null)
            {
                Name = entity.Description;
            }

            return Name;
        }

        /// <summary>
        /// Returns payment type name for this payment type id
        /// </summary>
        /// <param name="paymentTypeId">The value of paymentTypeId</param>
        /// <returns>Returns the Payment type name</returns>
        public string GetPaymentTypeName(int paymentTypeId)
        {
            string Name = string.Empty;

            ZNode.Libraries.Admin.StoreSettingsAdmin settingsAdmin = new ZNode.Libraries.Admin.StoreSettingsAdmin();
            PaymentType entity = settingsAdmin.GetPaymentTypeById(paymentTypeId);

            if (entity != null)
            {
                Name = entity.Name;
            }

            return Name;
        }

        /// <summary>
        /// Check Is Advanced Shipping Method
        /// </summary>
        /// <returns>Returns bool value true or false</returns>
        public bool IsAdvancedShipping()
        {
            return this.AdvancedShipping;
        }

        /// <summary>
        /// Bind Data method
        /// </summary>
        public void BindData()
        {
            // Declarations
            OrderAdmin _OrderAdmin = new OrderAdmin();
            Order _orderList = _OrderAdmin.DeepLoadByOrderID(this.OrderID);

            if (_orderList != null)
            {
                if (!UserStoreAccess.CheckStoreAccess(_orderList.PortalId.GetValueOrDefault(0)))
                {
                    Response.Redirect(ListPage, true);
                }
                
                this.AdvancedShipping = false;
                AccountListPage = string.Format(AccountListPage, _orderList.AccountID);
                ViewState["AccountListPage"] = AccountListPage;
                // FedEx shipping is the only advanced shipping currently supported.
                if (_orderList.ShippingID.HasValue && _orderList.ShippingIDSource.ShippingTypeID == 3)
                {
                    if (_orderList.ShippingIDSource.ShippingCode.ToLower().Contains("international"))
                    {
                        this.AdvancedShipping = false;
                    }
                }

                StringBuilder Build = new StringBuilder();
                Build.Append(this.CheckNull(_orderList.BillingFirstName) + " ");
                Build.Append(this.CheckNull(_orderList.BillingLastName) + "<br>");
                Build.Append(this.CheckNull(_orderList.BillingCompanyName) + "<br>");
                Build.Append(this.CheckNull(_orderList.BillingStreet) + " ");
                Build.Append(this.CheckNull(_orderList.BillingStreet1) + "<br>");
                Build.Append(this.CheckNull(_orderList.BillingCity) + ", ");
                Build.Append(this.CheckNull(_orderList.BillingStateCode) + " ");
                Build.Append(this.CheckNull(_orderList.BillingPostalCode) + "<br>");
                Build.Append(this.CheckNull(_orderList.BillingCountry) + "<br>");
                Build.Append("Tel: " + this.CheckNull(_orderList.BillingPhoneNumber) + "<br>");
                Build.Append("Email: " + this.CheckNull(_orderList.BillingEmailId));

                lblBillingAddress.Text = Build.ToString();

                Build.Remove(0, Build.Length);

                 string statusText =string.Empty;
                lblOrderDate.Text = _orderList.OrderDate.Value.ToString("MM/dd/yyyy hh:mm tt");
                if (_orderList.OrderStateIDSource != null)
                {
                    lblOrderStatus.Text = _orderList.OrderStateIDSource.Description;
                    if (lblOrderStatus.Text == this.GetGlobalResourceObject("ZnodeAdminResource", "TextPendingApproval").ToString()) 
                    {
                        lblOrderStatus.ForeColor = System.Drawing.Color.Red;
                    }
                    statusText = lblOrderStatus.Text;
                    lblOrderStatus.Text = statusText.ToUpper();
                    statusText = string.Empty;
                }

                lblOrderAmount.Text = this.Formatprice(_orderList.Total);

                // Bind custom additional instructions to label field
                lblAdditionalInstructions.Text = this.CheckNull(_orderList.AdditionalInstructions);

                this.OrderTrackingNumber = _orderList.TrackingNumber;
                if (this.OrderTrackingNumber == null)
                {
                    this.OrderTrackingNumber = string.Empty;
                }
                if (_orderList.PaymentStatusIDSource != null)
                {
                    lblPaymentStatus.Text = _orderList.PaymentStatusIDSource.Description;
                    if (_orderList.PaymentStatusIDSource.PaymentStatusName == "CC_PENDING" || _orderList.PaymentStatusIDSource.PaymentStatusName == "PO_PENDING" || _orderList.PaymentStatusIDSource.PaymentStatusName == "COD_PENDING")
                    {
                        lblPaymentStatus.ForeColor = System.Drawing.Color.Red;
                    }
                    statusText = lblPaymentStatus.Text;
                    lblPaymentStatus.Text = statusText.ToUpper();
                    statusText = string.Empty;
                }
                if (!this.AdvancedShipping)
                {
                    ShippingPanel.Visible = false;
                    LineItemDemensions.Visible = false;
                    ShippingErrorPanel.Visible = false;
                }
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoOrderRequest").ToString());
            }
        }
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.Params["itemid"] != null) || (Request.Params["itemid"].Length != 0))
            {
                this.OrderID = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
                lblOrderHeader.Text = this.OrderID.ToString();
            }

            _userAccount = (ZNodeUserAccount)ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount);

            if (!Page.IsPostBack)
            {
                this.BindData();
                this.BindGrid();

            }

            // Build the javascript block
            StringBuilder sb = new StringBuilder();
            sb.Append("<script language=JavaScript>");
            sb.Append("    function Back() {");
            sb.Append("        javascript:history.go(-1);");
            sb.Append("    }");
            sb.Append("<" + "/script>");

            if (!ClientScript.IsStartupScriptRegistered("GoBack"))
            {
                ClientScript.RegisterStartupScript(GetType(), "GoBack", sb.ToString());
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// List Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void List_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["from"] == "account")
            {
                Response.Redirect(ViewState["AccountListPage"].ToString());
            }
            else
            {
                Response.Redirect(this.ListPage);
            }
        }


        /// <summary>
        /// Order Items Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        /// <summary>
        /// Grid Button Command for Estimating Package Size, Creating Shipments, Cancelling Shipments, and Printing Labels
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            ShippingErrors.Text = string.Empty;
            ZNode.Libraries.Admin.ShippingAdmin _ShippingAdmin = new ShippingAdmin();
            OrderAdmin _OrderAdmin = new OrderAdmin();
            TList<OrderLineItem> items = _OrderAdmin.GetOrderLineItemByOrderID(this.OrderID);
            TList<OrderLineItem> singleshipitemlist = new TList<OrderLineItem>();

            foreach (OrderLineItem i in items)
            {
                if (i.OrderLineItemID.ToString() == e.CommandArgument.ToString())
                {
                    singleshipitemlist.Add(i);
                }

                if (e.CommandName == "Page")
                {
                }
                else
                {
                    if (e.CommandName == "ChangeOrderLineItemStatus")
                    {
                        Response.Redirect(this.statusPageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(e.CommandArgument.ToString())));
                        //Response.Redirect(this.statusPageLink + HttpUtility.UrlEncode(i.OrderLineItemID.ToString())); 
                    }
                }
            }

            if (_ShippingAdmin.ErrorCode != "0")
            {
                ShippingErrors.Text = string.Format("Error Code: {0}. Error Message:{1}", _ShippingAdmin.ErrorCode, _ShippingAdmin.ErrorDescription);
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Check Null Method
        /// </summary>
        /// <param name="value">The value of sValue</param>
        /// <returns>Returns the string </returns>
        private string CheckNull(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Bind grid with Order line items
        /// </summary>
        private void BindGrid()
        {
            ZNode.Libraries.Admin.OrderAdmin _OrderLineItemAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            ShippingAdmin _ShippingAdmin = new ShippingAdmin();
            DataSet dsOrderLineItems = new DataSet();
            
            TList<OrderLineItem> fullorderlist = _OrderLineItemAdmin.GetOrderLineItemByOrderID(this.OrderID);
            dsOrderLineItems = _OrderLineItemAdmin.GetVendorOrderLineItemByOrderID(this.OrderID, _userAccount.AccountID.ToString());


            if (dsOrderLineItems.Tables[0].Rows.Count == 0)
            {
                Response.Redirect("list.aspx", true);
            }
            else
            {
                foreach (DataRow row in dsOrderLineItems.Tables[0].Rows)
                {
                    if (!string.IsNullOrEmpty(row["OrderLineItemStateID"].ToString()))
                    {
                        if (Convert.ToInt32(row["OrderLineItemStateID"]) == 10)
                        {
                            orderStatus = this.GetGlobalResourceObject("ZnodeAdminResource", "TextInprogress").ToString(); 
                        }
                        else
                        {
                            orderStatus = this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownListCompleted").ToString(); 
                        }
                    }
                }
            }

            TList<OrderLineItem> shiptogetherlist = new TList<OrderLineItem>();
            TList<OrderLineItem> shipseperatelist = new TList<OrderLineItem>();

            if (dsOrderLineItems.Tables[0].Rows.Count > 0)
            {
                var multipleAddress = dsOrderLineItems.Tables[0].AsEnumerable().Select(s => s.Field<int>("OrderShipmentID")).ToList().Distinct();
                StringBuilder Build = new StringBuilder();
                if (multipleAddress.Count()>1)
                {
                    lblShippingAddress.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextShipMultipleAddress").ToString(); 
                }
                else
                {
                    Build.Append(this.CheckNull(dsOrderLineItems.Tables[0].Rows[0]["ShipToFirstName"] + " "));
                    Build.Append(this.CheckNull(dsOrderLineItems.Tables[0].Rows[0]["ShipToLastName"] + "<br>"));
                    Build.Append(this.CheckNull(dsOrderLineItems.Tables[0].Rows[0]["ShipToCompanyName"] + "<br>"));
                    Build.Append(this.CheckNull(dsOrderLineItems.Tables[0].Rows[0]["ShipToStreet"] + " "));
                    Build.Append(this.CheckNull(dsOrderLineItems.Tables[0].Rows[0]["ShipToStreet1"] + "<br>"));
                    Build.Append(this.CheckNull(dsOrderLineItems.Tables[0].Rows[0]["ShipToCity"] + ", "));
                    Build.Append(this.CheckNull(dsOrderLineItems.Tables[0].Rows[0]["ShipToStateCode"] + " "));
                    Build.Append(this.CheckNull(dsOrderLineItems.Tables[0].Rows[0]["ShipToPostalCode"] + "<br>"));
                    Build.Append(this.CheckNull(dsOrderLineItems.Tables[0].Rows[0]["ShipToCountry"] + "<br>"));
                    Build.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "TextTel").ToString() + this.CheckNull(dsOrderLineItems.Tables[0].Rows[0]["ShipToPhoneNumber"] + "<br>"));
                    Build.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "TextEmail").ToString() + this.CheckNull(dsOrderLineItems.Tables[0].Rows[0]["ShipToEmailID"] + ""));
                    lblShippingAddress.Text = Build.ToString();
                }
            }
            //shiptogetherlist = fullorderlist.FindAll(delegate(OrderLineItem item) { return item.ShipSeparately != true; });
            //shipseperatelist = fullorderlist.FindAll(delegate(OrderLineItem item) { return item.ShipSeparately == true; });

            uxGrid2.DataSource = shipseperatelist;
            uxGrid.DataSource = dsOrderLineItems;
            uxGrid.DataBind();
            uxGrid2.DataBind();
            if (this.AdvancedShipping)
            {
                decimal weight;
                decimal height;
                decimal length;
                decimal width;
                _ShippingAdmin.EstimatePackageSize(shiptogetherlist, out height, out width, out length, out weight);
                EstimatedHeight.Text = height.ToString();
                EstimatedLength.Text = length.ToString();
                EstimatedWeight.Text = weight.ToString();
                EstimatedWidth.Text = width.ToString();
            }
        }
        #endregion
    }
}

