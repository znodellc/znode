using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the MallAdmin - Admin_Secure_sales_orders_orderStatus class
    /// </summary>
    public partial class Admin_Secure_sales_orders_OrderStatus : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int OrderLineItemId = 0;
        private string ListPage = "view.aspx?itemid=";
        private ZNode.Libraries.DataAccess.Entities.OrderLineItem orderLineItem;
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region General Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.Params["itemid"] != null) || (Request.Params["itemid"].Length != 0))
            {
                this.OrderLineItemId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
                lblOrderID.Text = this.OrderLineItemId.ToString();

                orderLineItem = ZNode.Libraries.DataAccess.Data.DataRepository.OrderLineItemProvider.GetByOrderLineItemID(this.OrderLineItemId);
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
                if (ListOrderStatus.SelectedValue.Equals("20"))
                {
                    EmailStatus.Visible = true;
                }
                else
                {
                    EmailStatus.Visible = false;
                }
            }
        }

        /// <summary>
        /// Update Order Status Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UpdateOrderStatus_Click(object sender, EventArgs e)
        {
            OrderAdmin orderAdmin = new OrderAdmin();

            Order order = orderAdmin.GetOrderByOrderID(orderLineItem.OrderID);

            if (order != null)
            {
                //order.OrderStateID = int.Parse(ListOrderStatus.SelectedValue);
                orderLineItem.OrderLineItemStateID = int.Parse(ListOrderStatus.SelectedValue);
                orderLineItem.TrackingNumber = TrackingNumber.Text.Trim();

                order.OrderID = orderLineItem.OrderID;
                order.TrackingNumber = TrackingNumber.Text.Trim();
                if (int.Parse(ListOrderStatus.SelectedValue) == 20)
                {
                    order.ShipDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                    orderLineItem.ShipDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                }

                if (int.Parse(ListOrderStatus.SelectedValue) == 30)
                {
                    order.ReturnDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                    orderLineItem.ReturnDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                }

                // Delete referral commission entry of order if order status is "RETURNED" (30) or "CANCELLED" (40)
                bool Check = orderAdmin.Update(order);
                bool OrderLineItemCheck = orderAdmin.UpdateOrderLineItem(orderLineItem);
                bool isReferralCommissionDeleted = true;

                if (order.OrderStateID == 30 || order.OrderStateID == 40)
                {
                    ReferralCommissionAdmin referralCommissionAdmin = new ReferralCommissionAdmin();
                    if (referralCommissionAdmin.GetReferralCommissionByOrderID(this.OrderLineItemId))
                    {
                        referralCommissionAdmin.GetReferralCommissionByOrderID(this.OrderLineItemId);
                        isReferralCommissionDeleted = referralCommissionAdmin.DeleteByOrderId(this.OrderLineItemId);
                    }
                }

                if (Check && isReferralCommissionDeleted && OrderLineItemCheck)
                {
                    Response.Redirect(this.ListPage + HttpUtility.UrlEncode(encryption.EncryptData(orderLineItem.OrderID.ToString())));
                }
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CancelStatus_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListPage + HttpUtility.UrlEncode(encryption.EncryptData(orderLineItem.OrderID.ToString())));
        }

        /// <summary>
        /// Dropdown list selected index change
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void OrderStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListOrderStatus.SelectedValue.Equals("20"))
            {
                this.BindData();
                ListOrderStatus.SelectedValue = "20";
                EmailStatus.Visible = true;
            }
            else
            {
                EmailStatus.Visible = false;
            }
        }

        /// <summary>
        /// Email Status Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EmailStatus_Click(object sender, EventArgs e)
        {
            OrderAdmin _OrderAdmin = new OrderAdmin();
            Order _OrderList = _OrderAdmin.DeepLoadByOrderID(orderLineItem.OrderID);

            this.SendEmailReceipt(_OrderList, orderLineItem.OrderLineItemID);
        }

        private void SendEmailReceipt(Order order, int ordLineItemID)
        {
            string recepientEmail = string.Empty;

            try
            {
                recepientEmail = order.BillingEmailId;
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;

                if (string.IsNullOrEmpty(senderEmail))
                {
                    trackmessage.CssClass = "Error";
                    trackmessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextTrackMessageSender").ToString();
                    return;
                }

                if (string.IsNullOrEmpty(recepientEmail))
                {
                    trackmessage.CssClass = "Error";
                    trackmessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextTrackMessageRecipient").ToString();
                    return;
                }

                string subject = this.GetGlobalResourceObject("ZnodeAdminResource", "TextTrackSubject").ToString();

                // Get message text.
                StreamReader rw = new StreamReader(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "TrackingNumber.htm"));
                string messageText = rw.ReadToEnd();
                Regex rx1 = new Regex("#BillingFirstName#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, order.BillingFirstName);

                Regex rx2 = new Regex("#BillingLastName#", RegexOptions.IgnoreCase);
                messageText = rx2.Replace(messageText, order.BillingLastName);

                Regex rx3 = new Regex("#Custom1#", RegexOptions.IgnoreCase);
                messageText = rx3.Replace(messageText, TrackingNumber.Text);

                Regex rx4 = new Regex("#TrackingMessage#", RegexOptions.IgnoreCase);

               
                //Znode Version 7.2.2
                //For fetching  shippingTypeID on the basis of ordLineItemID - Start
                //The GetOrderShippingTypeID() functions return shippingTypeID , This function is member function of "ZNode.Libraries.Admin.OrderAdmin" class
                //3,2 is a shippingTypeID
                OrderAdmin _OrderAdmin = new OrderAdmin();
                int shippingTypeID = 0;

                shippingTypeID = order.ShippingIDSource == null ? _OrderAdmin.GetOrderShippingTypeID(ordLineItemID) : int.Parse(order.ShippingIDSource.ShippingTypeID.ToString());

                if (shippingTypeID.Equals(3))
                {
                    messageText = rx4.Replace(messageText, this.GetGlobalResourceObject("ZnodeAdminResource", "TextYourFedExTrackingNo").ToString());
                }
                else if (shippingTypeID.Equals(2))
                {
                    messageText = rx4.Replace(messageText, this.GetGlobalResourceObject("ZnodeAdminResource", "TextUpsTrackingNumber").ToString());
                }
                else
                {
                    messageText = rx4.Replace(messageText, string.Empty);
                }

                Regex rx5 = new Regex("#Message#", RegexOptions.IgnoreCase);
                if (shippingTypeID.Equals(3))
                {
                    messageText = rx5.Replace(messageText, this.GetGlobalResourceObject("ZnodeAdminResource", "TextTrackMessageShippedTrackNo").ToString());
                }
                else if (shippingTypeID.Equals(2))
                {
                    messageText = rx5.Replace(messageText, this.GetGlobalResourceObject("ZnodeAdminResource", "TextPackageTrackingNo").ToString());
                }
                else
                {
                    messageText = rx5.Replace(messageText, this.GetGlobalResourceObject("ZnodeAdminResource", "TextTrackMessageShipped").ToString());
                }
                //For fetching  shippingTypeID on the basis of ordLineItemID - End

                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(recepientEmail, senderEmail, senderEmail, subject, messageText, true);

                trackmessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextTrackingNoEmail").ToString() + "<a href=\"mailto:" + recepientEmail.ToString() + "\">" + recepientEmail + "</a>";

                EmailStatus.Enabled = false;
            }
            catch (Exception ex)
            {
                trackmessage.CssClass = "Error";
                trackmessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "Textproblemsendingemail").ToString() + "<a href=\"mailto:" + recepientEmail.ToString() + "\">" + recepientEmail + "</a>";

                // Log exception
                ExceptionPolicy.HandleException(ex, this.GetGlobalResourceObject("ZnodeAdminResource", "TextZnodeGlobalExceptionPolicy").ToString());
            }
        }

        #endregion

        #region Bind Data

        /// <summary>
        /// Bind Data Method\
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.OrderAdmin _OrderAdminAccess = new ZNode.Libraries.Admin.OrderAdmin();
            TList<OrderLineItem> orderLineItems = new TList<OrderLineItem>();
            var orderLineItemService = new OrderLineItemService();
            var orderShipment = new OrderShipment();
            var orderShipmentService = new OrderShipmentService();

            // Load Order State Item 
            ListOrderStatus.DataSource = _OrderAdminAccess.GetAllOrderStates();
            ListOrderStatus.DataTextField = "orderstatename";
            ListOrderStatus.DataValueField = "Orderstateid";
            ListOrderStatus.DataBind();

            // Remove Pending Approval Status from the dropdown
            ListItem li = ListOrderStatus.Items.FindByValue(((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL).ToString());
            if (li != null)
            {
                ListOrderStatus.Items.Remove(li);
            }

            ZNode.Libraries.DataAccess.Entities.Order order = _OrderAdminAccess.GetOrderByOrderID(orderLineItem.OrderID);

            orderLineItems = orderLineItemService.GetByOrderID(orderLineItem.OrderID);
            var orderItem = orderLineItems.FirstOrDefault().OrderShipmentID;
            orderShipment = orderShipmentService.GetByOrderShipmentID(int.Parse(orderItem.ToString()));

            int ShippingId = Convert.ToInt32(orderShipment.ShippingID);

            ZNode.Libraries.Admin.ShippingAdmin _shipping = new ZNode.Libraries.Admin.ShippingAdmin();

            int ShipId = _shipping.GetShippingTypeId(ShippingId);

            // Set the Tracking number title based on the ShippingTypeId
            if (ShipId == 1)
            {
                pnlTrack.Visible = true;
                TrackTitle.Visible = false;
                TrackingNumber.Visible = false;
            }
            else if (ShipId == 2)
            {
                pnlTrack.Visible = true;
                TrackTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextFedExTrackingNo").ToString();
            }
            else if (ShipId == 3)
            {
                pnlTrack.Visible = true;
                TrackTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextUPSTrackingNo").ToString();
            }
            else
            {
                pnlTrack.Visible = true;
                TrackTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextUSPSTrackingNo").ToString();
            }

            if (orderLineItem != null)
            {
                ListItem item = ListOrderStatus.Items.FindByValue(orderLineItem.OrderLineItemStateID.ToString());
                if (item != null)
                {
                    item.Selected = true;
                }

                TrackingNumber.Text = order.TrackingNumber;
            }

            lblCustomerName.Text = order.BillingFirstName + " " + order.BillingLastName;
            lblTotal.Text = orderLineItem.Price.Value.ToString("c");
        }

        #endregion
    }
}