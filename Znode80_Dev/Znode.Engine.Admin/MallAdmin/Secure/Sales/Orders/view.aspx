﻿<%@ Page Language="C#" MasterPageFile="~/MallAdmin/Themes/Standard/edit.master" AutoEventWireup="true" Inherits="Znode.Engine.MallAdmin.Admin_Secure_Sales_Orders_view" ValidateRequest="false" CodeBehind="view.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
	<div align="center">
		<div>
			<div class="LeftFloat" style="width: 30%; text-align: left">
				<h1> <asp:Localize ID="ColumnOrderID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOrderID %>'></asp:Localize>
                    <asp:Label ID="lblOrderHeader" runat="server" Text="Label" /></h1>
			</div>
			<div style="float: right"> 
                <zn:Button runat="server"  ID="List" ButtonType="EditButton" OnClick="List_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToOrder%>' Width="150px"/>
			</div>
			<div class="ClearBoth">
			</div>
			<div align="left">
				<div>
					<div class="LeftFloat" style="width: 500px">
						<!-- Order Info -->
						<h4 class="SubTitle"> <asp:Localize ID="ColumnTitleOrderInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderInformation %>'></asp:Localize></h4>
						<div class="ViewForm100">
							<div class="FieldStyle">
								 <asp:Localize ID="ColumnTitleOrderStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderStatus %>'></asp:Localize>
							</div>
							<div class="ValueStyle">
								<asp:Label ID="lblOrderStatus" Font-Bold="true" runat="server" />
							</div>
							<div class="FieldStyleA">
								 <asp:Localize ID="ColumnTitlePaymentStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePaymentStatus %>'></asp:Localize>
							</div>
							<div class="ValueStyleA">
								<asp:Label ID="lblPaymentStatus" Font-Bold="true" runat="server" />&nbsp;
							</div>
							<div class="FieldStyle">
								 <asp:Localize ID="ColumnTitleOrderDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderDate %>'></asp:Localize>
							</div>
							<div class="ValueStyle">
								<asp:Label ID="lblOrderDate" runat="server" />
							</div>
							<div class="FieldStyleA">
								 <asp:Localize ID="ColumnTitleOrderAmount" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderAmount %>'></asp:Localize>
							</div>
							<div class="ValueStyleA">
								<asp:Label ID="lblOrderAmount" runat="server" />&nbsp;
							</div>
						</div>
					</div>
					<div class="LeftFloat" style="width: 100%;">
						<!-- Address -->
						<h4 class="SubTitle"> <asp:Localize ID="SubTitleCustomerInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCustomerInformation %>'></asp:Localize></h4>
						<div>
							<div class="LeftFloat" style="text-align: left; width: 400px; word-wrap: break-word;">
								<div class="FieldStyle" style="text-align: left">
									<b> <asp:Localize ID="ColumnShippingAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingAddress %>'></asp:Localize></b>
								</div>
								<div class="ValueStyle">
									<asp:Label ID="lblShippingAddress" runat="server" Text="Label"></asp:Label>
								</div>
							</div>
							<div style="float: left; width: 400px; word-wrap: break-word;">
								<div class="FieldStyle" style="text-align: left">
									<b> <asp:Localize ID="ColumnBillingAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBillingAddress %>'></asp:Localize></b>
								</div>
								<div class="ValueStyle">
									<asp:Label ID="lblBillingAddress" runat="server" Text="Label"></asp:Label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="ClearBoth">
				</div>
				<br />
				<asp:Panel ID="ShippingErrorPanel" runat="server">
					<h4>
						<asp:Label ID="ErrorHeader" Text="Shipping Errors" runat="server"></asp:Label></h4>
					<div align="justify">
						<asp:Label ID="ShippingErrors" runat="server" CssClass="Error"></asp:Label>
					</div>
				</asp:Panel>
				<h4 class="GridTitle"> <asp:Localize ID="GridTitleShipTogetherOrderItems" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleShipTogetherOrderItems %>'></asp:Localize></h4>
				<asp:GridView ID="uxGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
					runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
					CssClass="Grid" Width="100%" GridLines="None" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridOrderlineEmptyData %>'
					AllowPaging="True" OnPageIndexChanging="UxGrid_PageIndexChanging" OnRowCommand="UxGrid_RowCommand"
					PageSize="10">
					<Columns>
						<asp:BoundField DataField="OrderLineItemID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleLineItemID %>' HeaderStyle-HorizontalAlign="Left" />
						<asp:BoundField DataField="Name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>' HeaderStyle-HorizontalAlign="Left" />
						<asp:BoundField DataField="ProductNum" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnProductCode %>' HeaderStyle-HorizontalAlign="Left" />
						<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDescription %>' HeaderStyle-HorizontalAlign="Left">
							<ItemTemplate>
								<%# DataBinder.Eval(Container.DataItem, "Description") %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="SKU" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSKU %>' HeaderStyle-HorizontalAlign="Left" />
						<asp:BoundField DataField="Quantity" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantity %>' HeaderStyle-HorizontalAlign="Left" />
						<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePrice %>' HeaderStyle-HorizontalAlign="Left">
							<ItemTemplate>
								<%# DataBinder.Eval(Container.DataItem, "price","{0:c}").ToString()%>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="ShipDate" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleShipDate %>' DataFormatString="{0:d}"
							HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleShipAddress %>' HeaderStyle-HorizontalAlign="Left">
                          <ItemTemplate>
                              <div class="remark">
                                  <asp:LinkButton ID="lnkShipName" runat="server"><%# DataBinder.Eval(Container.DataItem, "ShipName")%></asp:LinkButton>
                              <div id="divPopup" class="remarkdetail">
                                <%# DataBinder.Eval(Container.DataItem, "ShipName")%><br />
										<%# DataBinder.Eval(Container.DataItem, "ShipToFirstName")%> and
										<%# DataBinder.Eval(Container.DataItem, "ShipToLastName")%><br />
										<%# DataBinder.Eval(Container.DataItem, "ShipToStreet")%><br />
										<%# DataBinder.Eval(Container.DataItem, "ShipToCity")%>,
										<%# DataBinder.Eval(Container.DataItem, "ShipToStateCode")%>
										<%# DataBinder.Eval(Container.DataItem, "ShipToPostalCode")%><br />
										<%# DataBinder.Eval(Container.DataItem, "ShipToCountry")%><br />
										Tel:<%# DataBinder.Eval(Container.DataItem, "ShipToPhoneNumber")%>  
                              </div>
                                  </div>
                          </ItemTemplate>
                        </asp:TemplateField>
						<asp:BoundField DataField="TrackingNumber" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTrackingNumber %>' HeaderStyle-HorizontalAlign="Left" />
						<asp:BoundField DataField="TransactionNumber" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTransactionNumber %>' HeaderStyle-HorizontalAlign="Left" />
						<asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left">
							<ItemTemplate>
								<asp:LinkButton ID="Button1" CommandName="ChangeOrderLineItemStatus" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"OrderLineItemID")%>'
									Text='<%$ Resources:ZnodeAdminResource, LinkChangeStatus %>' runat="server" CssClass="Button" />

							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
					<EmptyDataTemplate>
						  <asp:Localize ID="GridOrderlineEmptyData" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridOrderlineEmptyData %>'></asp:Localize>
					</EmptyDataTemplate>
					<RowStyle CssClass="RowStyle" />
					<HeaderStyle CssClass="HeaderStyle" />
					<AlternatingRowStyle CssClass="AlternatingRowStyle" />
					<FooterStyle CssClass="FooterStyle" />
					<PagerStyle CssClass="PagerStyle" />
				</asp:GridView>
				 <asp:Panel ID="ShippingPanel" runat="server">
                <asp:Panel ID="DemensionPanel" runat="server">
                <br />
                <div style="width: 50%">
                 <asp:Localize ID="TextPackageInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPackageInformation %>'></asp:Localize>
                </div>
                    <br />
                    <asp:RegularExpressionValidator ID="EstimatedWeightValidator" ControlToValidate="EstimatedWeight"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDecimalValue %>' runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+(\.)?[0-9]?[0-9]?)$"
                        ValidationGroup="OrderEstimate"></asp:RegularExpressionValidator><br />
                    <asp:RegularExpressionValidator ID="EstimatedHeightValidator" ControlToValidate="EstimatedHeight"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularWholeNumber %>' runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+?[0-9]?[0-9]?)$"
                        ValidationGroup="OrderEstimate"></asp:RegularExpressionValidator><br />
                    <asp:RegularExpressionValidator ID="EstimatedLengthValidator" ControlToValidate="EstimatedLength"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularNumberLength %>' runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+?[0-9]?[0-9]?)$"
                        ValidationGroup="OrderEstimate"></asp:RegularExpressionValidator><br />
                    <asp:RegularExpressionValidator ID="EstimatedWidthValidator" ControlToValidate="EstimatedWidth"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularNumberforwidth %>' runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+?[0-9]?[0-9]?)$"
                        ValidationGroup="OrderEstimate"></asp:RegularExpressionValidator><br />
                    <asp:RequiredFieldValidator ID="RequiredEstimatedWidth" ControlToValidate="EstimatedWidth"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularCreateShipment %>' runat="server" ValidationGroup="RequiredEstimate"></asp:RequiredFieldValidator><br />
                    <asp:RequiredFieldValidator ID="RequiredEstimatedLength" ControlToValidate="EstimatedLength"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularLengthMust %>' runat="server" ValidationGroup="RequiredEstimate"></asp:RequiredFieldValidator><br />
                    <asp:RequiredFieldValidator ID="RequiredEstimatedHeight" ControlToValidate="EstimatedHeight"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularHeightMust %>' runat="server" ValidationGroup="RequiredEstimate"></asp:RequiredFieldValidator><br />
                    <asp:RequiredFieldValidator ID="RequiredEstimatedWeight" ControlToValidate="EstimatedWeight"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularWeightMust %>' runat="server" ValidationGroup="RequiredEstimate"></asp:RequiredFieldValidator><br />
                    <div>
                        <div class="LeftFloat" width="25%" valign="top">
                            <div class="FieldStyle">
                                  <asp:Localize ID="ColumnWeight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWeight %>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="EstimatedWeight" runat="server" Width="50"></asp:TextBox>
                            </div>
                        </div>
                        <div class="LeftFloat" width="25%" valign="top">
                            <div class="FieldStyle">
                                <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnHeight %>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="EstimatedHeight" runat="server" Width="50"></asp:TextBox>
                            </div>
                        </div>
                        <div class="LeftFloat" width="25%" valign="top">
                            <div class="FieldStyle">
                                 <asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLength %>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="EstimatedLength" runat="server" Width="50"></asp:TextBox>
                            </div>
                            <div class="LeftFloat" width="25%" valign="top">
                                <div class="FieldStyle">
                                    <asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleWidth %>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="EstimatedWidth" runat="server" Width="50"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                     <br />
                     <br />
                 </asp:Panel>
				<div style="display: none">
					<%--  <h4 class="GridTitle">
                    Ship Separate Order Items</h4>--%>
					<asp:GridView ID="uxGrid2" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
						runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
						CssClass="Grid" Width="100%" GridLines="None" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridOrderlineEmptyData %>'
						AllowPaging="True" OnPageIndexChanging="UxGrid_PageIndexChanging" OnRowCommand="UxGrid_RowCommand"
						PageSize="10" Visible="false">
						<Columns>
							<asp:BoundField DataField="OrderLineItemID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleLineItemID %>' HeaderStyle-HorizontalAlign="Left" />
							<asp:BoundField DataField="Name" HeaderText="Product Name" HeaderStyle-HorizontalAlign="Left" />
							<asp:BoundField DataField="ProductNum" HeaderText="Product Code" HeaderStyle-HorizontalAlign="Left" />
							<asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
								<ItemTemplate>
									<%# DataBinder.Eval(Container.DataItem, "Description") %>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="SKU" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleWidth %>' HeaderStyle-HorizontalAlign="Left" />
							<asp:BoundField DataField="Quantity" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantity %>' HeaderStyle-HorizontalAlign="Left" />
							<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleWidth %>' HeaderStyle-HorizontalAlign="Left">
								<ItemTemplate>
									<%# DataBinder.Eval(Container.DataItem, "price","{0:c}").ToString()%>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="ShipDate" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleShipDate %>' DataFormatString="{0:d}"
								HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
							<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleShipDate %>' HeaderStyle-HorizontalAlign="Left">
								<ItemTemplate>
									<%#DataBinder.Eval(Container.DataItem, "ShippingCost", "{0:c}").ToString()%>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="TrackingNumber" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTrackingNumber %>' HeaderStyle-HorizontalAlign="Left" />
							<asp:TemplateField Visible="false">
								<ItemTemplate>
									<asp:Button runat="server" CausesValidation="true" ID="CancelShipping" Text='<%# ButtonText(Eval("TrackingNumber"),Eval("OrderLineItemID")) %>'
										CssClass="Button" CommandArgument='<%# Eval("OrderLineItemID") %>' CommandName='<%# ShippingCommand(Eval("TrackingNumber"),Eval("OrderLineItemID")) %>'
										Visible="false" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField Visible="false">
								<ItemTemplate>
									<asp:Button runat="server" CausesValidation="false" ID="Label" Text='<%$ Resources:ZnodeAdminResource, TextPrintLabel %>'
										CssClass="Button" CommandArgument='<%# Eval("TrackingNumber") %>' CommandName="Label"
										Visible='<%# ShowLabelButton(Eval("TrackingNumber")) %>' />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<EmptyDataTemplate>
							 <asp:Localize ID="OrderlineEmptyData" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridOrderlineEmptyData %>'></asp:Localize>
							 
						</EmptyDataTemplate>
						<RowStyle CssClass="RowStyle" />
						<HeaderStyle CssClass="HeaderStyle" />
						<AlternatingRowStyle CssClass="AlternatingRowStyle" />
						<FooterStyle CssClass="FooterStyle" />
						<PagerStyle CssClass="PagerStyle" />
					</asp:GridView>
					<asp:HiddenField runat="server" ID="EstimatedLineItemID" />
					<asp:Panel ID="LineItemDemensions" runat="server" Visible="false">
						<br />
                        <div>
                            <div class="LeftFloat"> 
                                <asp:Localize ID="EstimateShippings" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextEstimateShippings %>'></asp:Localize>
                            </div>
                        </div>
                        <br />
						<asp:RequiredFieldValidator ID="LineItemWidthRequired" ControlToValidate="LineItemWidth"
							ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularCreateShipment %>' runat="server" ValidationGroup="RequiredLineItems"></asp:RequiredFieldValidator><br />
						<asp:RequiredFieldValidator ID="LineItemLengthRequired" ControlToValidate="LineItemLength"
							ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularLengthMust %>' runat="server" ValidationGroup="RequiredLineItems"></asp:RequiredFieldValidator><br />
						<asp:RequiredFieldValidator ID="LineItemHeightRequired" ControlToValidate="LineItemHeight"
							ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularHeightMust %>' runat="server" ValidationGroup="RequiredLineItems"></asp:RequiredFieldValidator><br />
						<asp:RequiredFieldValidator ID="LineItemWeightRequired" ControlToValidate="LineItemWeight"
							ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularWeightMust %>' runat="server" ValidationGroup="RequiredLineItems"></asp:RequiredFieldValidator><br />
						<asp:RegularExpressionValidator Display="static" ID="LineItemWeightValidator" ControlToValidate="LineItemWeight"
							ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularEnterDecimalforWeight %>' runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+(\.)?[0-9]?[0-9]?)$"
							ValidationGroup="1OrderEstimate"></asp:RegularExpressionValidator><br />
						<asp:RegularExpressionValidator ID="LineItemHeightValidator" ControlToValidate="LineItemHeight"
							ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularWholeNumber %>' runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+?[0-9]?[0-9]?)$"
							ValidationGroup="1OrderEstimate"></asp:RegularExpressionValidator><br />
						<asp:RegularExpressionValidator ID="LineItemLengthValidator" ControlToValidate="LineItemLength"
							ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularNumberLength %>' runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+?[0-9]?[0-9]?)$"
							ValidationGroup="1OrderEstimate"></asp:RegularExpressionValidator><br />
						<asp:RegularExpressionValidator ID="LineItemWidthValidator" ControlToValidate="LineItemWidth"
							ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularNumberforwidth %>' runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+?[0-9]?[0-9]?)$"
							ValidationGroup="1OrderEstimate"></asp:RegularExpressionValidator><br />
                       <div>
							<div class="LeftFloat" width="25%" valign="top">
								<div class="FieldStyle">
									 <asp:Localize ID="TextColumnWeight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWeight %>'></asp:Localize>
								</div>
								<div class="ValueStyle">
									<asp:TextBox ID="LineItemWeight" runat="server" Width="50"></asp:TextBox>
								</div>
							</div>
							<div class="LeftFloat" width="25%" valign="top">
								<div class="FieldStyle">
									<asp:Localize ID="TextColumnHeight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnHeight %>'></asp:Localize>
								</div>
								<div class="ValueStyle">
									<asp:TextBox ID="LineItemHeight" runat="server" Width="50"></asp:TextBox>
								</div>
							</div>
							<div class="LeftFloat" width="25%" valign="top">
								<div class="FieldStyle">
									  <asp:Localize ID="TextColumnLength" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLength %>'></asp:Localize>
								</div>
								<div class="ValueStyle">
									<asp:TextBox ID="LineItemLength" runat="server" Width="50"></asp:TextBox>
								</div>
							</div>
							<div class="LeftFloat" width="25%" valign="top">
								<div class="FieldStyle">
									 <asp:Localize ID="TextColumnTitleWidth" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleWidth %>'></asp:Localize>
								</div>
								<div class="ValueStyle">
									<asp:TextBox ID="LineItemWidth" runat="server" Width="50"></asp:TextBox>
								</div>
							</div>
						</div>
					</asp:Panel>
				</div>
				<h4 class="SubTitle"> <asp:Localize ID="TitleAdditionalInstructions" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleAdditionalInstructions %>'></asp:Localize></h4>
				<div align="justify">
					<asp:Label ID="lblAdditionalInstructions" runat="server"></asp:Label>
				</div>
			</div>
		</div>
	</div>
</asp:Content>