using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;

namespace Znode.Engine.MallAdmin
{
    /// <summary>
    /// Represents the Mall Admin admin_themes_standard_content class.
    /// </summary>
    public partial class admin_themes_standard_content : ZNodeAdminTemplate
    {
        #region Private Variables
        private HyperLink rootMenuLink = null;
        private HyperLink subMenuLink = null;
        private string subMenuName = string.Empty;
        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ViewStateUserKey = Session.SessionID;

            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }
        }


        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected new void Page_Load(object sender, EventArgs e)
        {
            if (Page.Header != null)
            {
                // Add the page title to the header element
                this.Page.Header.Title = "Store Management";
            }

            SiteMapDataSource2.StartingNodeUrl = this.GetSelectedMenuItem();
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
        }

        /// <summary>
        /// Raised after the user clicks the logout link and the logout process is complete.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            // Clearing a per-user cache of objects here.
            Session.Remove(ZNode.Libraries.Framework.Business.ZNodeSessionKeyType.UserAccount.ToString());

            // Reset profile cache
            Session["ProfileCache"] = null;
        }

        /// <summary>
        /// Raised after the user clicks the logout link and the logout process is complete.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxLoginStatus_LoggedOut(object sender, EventArgs e)
        {
            Session.Remove(ZNode.Libraries.Framework.Business.ZNodeSessionKeyType.UserAccount.ToString());

            // Reset profile cache
            Session["ProfileCache"] = null;

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.UserLogout, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "User Logout", "User Logout");
            Response.Redirect("~/MallAdmin/Default.aspx");
        }

        /// <summary>
        /// Raised after the user clicks the logout link and the logout process is complete.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AdminUserLoginStatus_LoggedOut(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        /// <summary>
        /// Menu item clicked 
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CtrlMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            if (e.Item.Text == "Dashboard" || e.Item.Text == "Reports")
            {
                Session["SelectedMenuItem"] = null;
            }
            else
            {
                Session["SelectedMenuItem"] = e.Item.DataPath;
            }

            Response.Redirect(e.Item.DataPath);
        }

        /// <summary>
        /// Get the selected menu item
        /// </summary>
        /// <returns>Returns the selected menu item string.</returns>
        protected string GetSelectedMenuItem()
        {
            string rootMenu = string.Empty;
            if (this.rootMenuLink != null)
            {
                rootMenu = this.rootMenuLink.NavigateUrl;
            }

            if (Session["SelectedMenuItem"] != null)
            {
                pnlSubmenu.Visible = true;
                return Session["SelectedMenuItem"].ToString();
            }
            else if (rootMenu != string.Empty)
            {
                pnlSubmenu.Visible = false;
                Session["SelectedMenuItem"] = rootMenu;
                return Session["SelectedMenuItem"].ToString();
            }
            else
            {
                pnlSubmenu.Visible = false;
                return string.Empty;
            }
        }

        /// <summary>
        /// Get the menu CSS
        /// </summary>
        /// <param name="url">The Url to check.</param>
        /// <param name="title">Menu title</param>
        /// <returns>Return the submenu css class name</returns>
        protected string GetSubmenuCss(object url, object title)
        {
            int pos = Request.Url.PathAndQuery.IndexOf('?');
            string currentPage = Request.Url.PathAndQuery.Substring(0, pos > 0 ? pos : Request.Url.PathAndQuery.Length);
            string subMenu = string.Empty;
            if (this.subMenuLink != null)
            {
                subMenu = this.subMenuLink.NavigateUrl;
            }

            if (currentPage == url.ToString() || Request.Url.LocalPath.ToString() == url.ToString() || subMenu == url.ToString() || title.ToString() == this.subMenuName)
            {
                return "SubStaticSelectedStyle";
            }
            else
            {
                return "SubStaticMenuItemStyle";
            }
        }

        protected string SelectedStyle(object noteItem)
        {
            if (noteItem is SiteMapNode)
            {
                var item = noteItem as SiteMapNode;

                if (item.Url.ToLower() == Request.Url.AbsolutePath.ToLower())
                {                    
                    return "selected";
                }

                foreach (var subItem in item.ChildNodes)
                {
                    if ((subItem as SiteMapNode).Url.ToLower() == Request.Url.AbsolutePath.ToLower())
                    {                        
                        return "selected";
                    }
                }
            }

            return string.Empty;
        }
    }
}