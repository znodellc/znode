﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Themes/Standard/login.master"  CodeBehind="ResetPassword.aspx.cs" Inherits="Znode.Engine.Common.SiteAdmin.ResetPassword" %>

<%@ Register TagPrefix ="ZNode" TagName="ResetPassword" Src="~/Controls/Default/ResetPassword.ascx" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Login ResetPwdLogin">
        <div>
            <ZNode:ResetPassword runat="server"></ZNode:ResetPassword>
        </div>  
    </div>
</asp:Content>
