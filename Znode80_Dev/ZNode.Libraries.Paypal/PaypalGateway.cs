using System;
using System.Linq;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;
using System.Collections.Generic;
using com.paypal.sdk.profiles;
using com.paypal.sdk.services;
using com.paypal.soap.api;

namespace ZNode.Libraries.Paypal
{
    /// <summary>
    /// PayPal Gateway
    /// </summary>
    public class PaypalGateway : ZNodeBusinessBase
    {
        #region Private Member Variables
        /// <summary>
        /// Read only Instance object for Call Services 
        /// </summary>
        private readonly CallerServices caller;
        private ZNodeShoppingCart _shoppingCart = null;
        private IEnumerable<ZNodeShoppingCartItem> _addressCartItems = null;
        private string _ECRedirectUrl = string.Empty;
        private string _ECCancelUrl = string.Empty;
        private decimal _orderTotal = 0;
        private PaymentSetting _paymentSetting = new PaymentSetting();
        private string CurrencyCode = ZNodeCurrencyManager.CurrencyCode();
        private string _paymentActionTypeCode = "Sale";
        private TList<OrderLineItem> _orderLineItems = new TList<OrderLineItem>();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the PaypalGateway class
        /// </summary>
        /// <param name="PaymentSetting">Payment Setting</param>        
        public PaypalGateway(PaymentSetting PaymentSetting)
            : this(PaymentSetting, string.Empty, string.Empty, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the PaypalGateway class.
        /// </summary>
        /// <param name="PaymentSetting">Payment Setting</param>
        /// <param name="ECRedirectUrl">Redirect URL</param>
        /// <param name="ECReturnUrl">Return URL</param>
        /// <param name="addressCartItems"></param>
        public PaypalGateway(PaymentSetting PaymentSetting, string ECRedirectUrl, string ECReturnUrl, ZNodeShoppingCart portalCart)
        {
            this._addressCartItems = portalCart.ShoppingCartItems.Cast<ZNodeShoppingCartItem>();

            var sessionCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);

            this._shoppingCart = portalCart;

            this._paymentSetting = PaymentSetting;

            this.caller = new CallerServices();
            ZNodeEncryption encryption = new ZNodeEncryption();

            string gatewayLoginID = encryption.DecryptData(this._paymentSetting.GatewayUsername);
            string gatewayPassword = encryption.DecryptData(this._paymentSetting.GatewayPassword);
            string transactionKey = this._paymentSetting.TransactionKey;
            bool IsTestMode = this._paymentSetting.TestMode;
            this.caller.APIProfile = CreateAPIProfile(gatewayLoginID, gatewayPassword, transactionKey, IsTestMode);

            this._ECCancelUrl = ECReturnUrl;
            this._ECRedirectUrl = ECRedirectUrl;
        }

        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets the Shipping Address 
        /// </summary>
        public decimal OrderTotal
        {
            get { return this._orderTotal; }
            set { this._orderTotal = value; }
        }

        /// <summary>
        /// Gets or sets the payment action type code which Specifies whether the transaction is a Sale, an Authorization, or an Order
        /// Use Sale (authorize and capture) or Authorize (authorize only) mode when processing payments.
        /// </summary>
        public string PaymentActionTypeCode
        {
            get { return this._paymentActionTypeCode; }
            set { this._paymentActionTypeCode = value; }
        }

        /// <summary>
        /// Gets or sets the order
        /// </summary>      
        public TList<OrderLineItem> OrderLineItems
        {
            get { return this._orderLineItems; }
            set { this._orderLineItems = value; }
        }
        #endregion

        #region Protected properties

        /// <summary>
        /// Gets or set the paypal server url to post request
        /// </summary>
        private string PaypalServerUrl
        {
            get
            {
                if (PaymentSetting.TestMode)
                {
                    return "www.sandbox.paypal.com"; // Test environment URL
                }
                else
                {
                    return "www.paypal.com"; // Production Site URL
                }
            }
        }

        /// <summary>
        /// Gets the payment settings
        /// </summary>
        private PaymentSetting PaymentSetting
        {
            get
            {
                return this._paymentSetting;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method initiate an Express Checkout transaction
        /// </summary>
        /// <returns>Returns GatewayResponse object</returns>
        public PaypalResponse DoPaypalExpressCheckout()
        {
            PaypalResponse paypalResponse = new PaypalResponse();
            PaymentSetting PaymentSetting = new PaymentSetting();
            PaypalRecurringBillingInfo recurringBillingInfo = new PaypalRecurringBillingInfo();

            try
            {
                // Create the request object
                SetExpressCheckoutRequestType expCheckoutRequest = new SetExpressCheckoutRequestType();

                // Create the request details object
                expCheckoutRequest.SetExpressCheckoutRequestDetails = new SetExpressCheckoutRequestDetailsType();
                expCheckoutRequest.SetExpressCheckoutRequestDetails.PaymentAction = (PaymentActionCodeType)Enum.Parse(typeof(PaymentActionCodeType), this._paymentActionTypeCode);
                expCheckoutRequest.SetExpressCheckoutRequestDetails.PaymentActionSpecified = true;

                // Set Number of Recurring Payments Profile to be created
                int count = 1;
                foreach (ZNodeShoppingCartItem Item in this._shoppingCart.ShoppingCartItems)
                {
                    if (Item.Product.RecurringBillingInd == true)
                    {
                        count = count + 1;
                    }
                }

                BillingAgreementDetailsType[] BADetailsArray = new BillingAgreementDetailsType[count];
                int index = 0;
                foreach (ZNodeShoppingCartItem Item in this._shoppingCart.ShoppingCartItems)
                {
                    if (Item.Product.RecurringBillingInd == true)
                    {
                        BillingAgreementDetailsType BADetailsType = new BillingAgreementDetailsType();
                        BADetailsType.BillingType = BillingCodeType.RecurringPayments;
                        BADetailsType.BillingAgreementDescription = Item.Product.Name;
                        BADetailsArray.SetValue(BADetailsType, index++);
                    }

                    foreach (ZNodeAddOnEntity addon in Item.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity addonValue in addon.AddOnValueCollection)
                        {
                            if (addonValue.RecurringBillingInd == true)
                            {
                                BillingAgreementDetailsType BADetailsType = new BillingAgreementDetailsType();
                                BADetailsType.BillingType = BillingCodeType.RecurringPayments;
                                BADetailsType.BillingAgreementDescription = addonValue.Name;
                                BADetailsArray.SetValue(BADetailsType, index++);
                            }
                        }
                    }
                }

                PaymentDetailsItemType item = null;
                List<PaymentDetailsItemType> paypalItems = new List<PaymentDetailsItemType>();
                foreach (ZNodeShoppingCartItem shoppingCartItem in _shoppingCart.ShoppingCartItems)
                {
                    item = new PaymentDetailsItemType();
                    item.Name = shoppingCartItem.Product.Name;
                    item.Description = shoppingCartItem.Product.ShortDescription;
                    item.Number = shoppingCartItem.Product.SKU;
                    item.Quantity = shoppingCartItem.Quantity.ToString();
                    item.Amount = this.GetAmount(shoppingCartItem.PromotionalPrice);
                    paypalItems.Add(item);

                    foreach (ZNodeAddOnEntity addon in shoppingCartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity addonValue in addon.AddOnValueCollection)
                        {
                            item = new PaymentDetailsItemType();
                            item.Name = addonValue.Name;
                            item.Description = addonValue.Description;
                            item.Number = addonValue.SKU;
                            item.Quantity = shoppingCartItem.Quantity.ToString();
                            item.Amount = this.GetAmount(addonValue.FinalPrice);
                            paypalItems.Add(item);
                        }
                    }
                }

                if (this._shoppingCart.Discount > 0)
                {
                    // Add the discount field as shopping cart item with negative value.
                    item = new PaymentDetailsItemType();
                    item.Name = "Discount";
                    item.Description = string.Empty;
                    item.Number = string.Empty;
                    item.Quantity = "1";
                    item.Amount = this.GetAmount(-this._shoppingCart.Discount);
                    paypalItems.Add(item);
                }

                if (this._shoppingCart.GiftCardAmount > 0)
                {
                    // Add the gift card amount as shopping cart item with negative value.
                    item = new PaymentDetailsItemType();
                    item.Name = "Gift Card";
                    item.Description = string.Empty;
                    item.Number = string.Empty;
                    item.Quantity = "1";
                    item.Amount = this.GetAmount(-this._shoppingCart.GiftCardAmount);
                    paypalItems.Add(item);
                }


                PaymentDetailsType paymentDetails = new PaymentDetailsType();
                paymentDetails.ItemTotal = this.GetSubTotal(paypalItems.ToArray());
                paymentDetails.ShippingTotal = this.GetAmount(_shoppingCart.ShippingCost);
                paymentDetails.TaxTotal = this.GetAmount(_shoppingCart.TaxCost);
                paymentDetails.OrderTotal = this.GetAmount(_shoppingCart.TaxCost + _shoppingCart.ShippingCost + _shoppingCart.SubTotal - _shoppingCart.Discount - _shoppingCart.GiftCardAmount);
                paymentDetails.PaymentDetailsItem = paypalItems.ToArray();

                expCheckoutRequest.SetExpressCheckoutRequestDetails.PaymentDetails = new PaymentDetailsType[] { paymentDetails };

                #region Znode Version 7.2.2 Send Address to PayPal Api

                //For sending shipping & billing address to paypal api  - Start
                //if shipping address does not match with paypal address then it will show error message
                ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount _userAccount = ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount();
                if (_userAccount != null && _userAccount.ShippingAddress != null)
                {
                    AddressType UserAddressType = new AddressType();

                    //Shipping Information
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.AddressOverride = "true";
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address = new AddressType();
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.Name = GetFullNameforPaypal(_userAccount.ShippingAddress);
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.Phone = _userAccount.ShippingAddress.PhoneNumber;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.PostalCode = _userAccount.ShippingAddress.PostalCode;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.StateOrProvince = _userAccount.ShippingAddress.StateCode;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.Street1 = _userAccount.ShippingAddress.Street;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.Street2 = _userAccount.ShippingAddress.Street1;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.CityName = _userAccount.ShippingAddress.City;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.CountryName = _userAccount.ShippingAddress.CountryCode;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.CountrySpecified = true;

                    foreach (CountryCodeType codeType in Enum.GetValues(typeof(CountryCodeType)))
                    {
                        if (codeType.ToString().Equals(_userAccount.ShippingAddress.CountryCode))
                        {
                            expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.Country = codeType;
                            break;
                        }
                    }

                    //Billing Information
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress = new AddressType();
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.Name = GetFullNameforPaypal(_userAccount.BillingAddress);
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.Street1 = _userAccount.BillingAddress.Street;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.Street2 = _userAccount.BillingAddress.Street1;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.PostalCode = _userAccount.BillingAddress.PostalCode;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.Phone = _userAccount.BillingAddress.PhoneNumber;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.StateOrProvince = _userAccount.BillingAddress.StateCode;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.CityName = _userAccount.BillingAddress.City;

                    foreach (CountryCodeType codeType in Enum.GetValues(typeof(CountryCodeType)))
                    {
                        if (codeType.ToString().Equals(_userAccount.BillingAddress.CountryCode))
                        {
                            expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.Country = codeType;
                            break;
                        }
                    }

                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.CountryName = _userAccount.BillingAddress.CountryCode;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.CountrySpecified = true;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BuyerEmail = _userAccount.EmailID;
                }

                //For sending shipping & billing address to paypal api  - End
                #endregion

                expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAgreementDetails = BADetailsArray;
                expCheckoutRequest.SetExpressCheckoutRequestDetails.CancelURL = this._ECCancelUrl;
                expCheckoutRequest.SetExpressCheckoutRequestDetails.ReturnURL = this._ECRedirectUrl;

                SetExpressCheckoutResponseType ECResponsetype = (SetExpressCheckoutResponseType)this.caller.Call("SetExpressCheckout", expCheckoutRequest);

                if (ECResponsetype.Ack == AckCodeType.Success || ECResponsetype.Ack == AckCodeType.SuccessWithWarning)
                {
                    paypalResponse.PayalToken = ECResponsetype.Token;
                    string hostURL = paypalResponse.HostUrl;
                    paypalResponse.HostUrl = String.Format(hostURL, this.PaypalServerUrl, ECResponsetype.Token);
                    paypalResponse.ResponseCode = "0";
                }
                else
                {
                    paypalResponse.ResponseCode = ECResponsetype.Errors[0].ErrorCode;

                    if (decimal.Parse(paymentDetails.ItemTotal.Value) == 0 && paypalResponse.ResponseCode == "10413")
                    {
                        paypalResponse.ResponseText = "Sum of all product(s) amount should be greater than discount amount.";
                    }
                    else
                    {
                        //Znode Version 7.2.2
                        //Add condition to show user friendly message when shipping adrress dont match with paypal address -Start
                        paypalResponse.ResponseText = paypalResponse.ResponseCode.Equals("10736") ? "The shipping address and PayPal address does not match." : ECResponsetype.Errors[0].LongMessage;
                        //Add condition to show user friendly message when shipping adrress dont match with paypal address -End
                    }


                }
            }
            catch (Exception ex)
            {
                paypalResponse.ResponseText = ex.Message;
                paypalResponse.ResponseCode = "-1";
            }

            return paypalResponse;
        }

        #region Znode Version 7.2.2 - Make Overload DoPaypalExpressCheckout for Admin Site
              
        /// <summary>
        /// Znode Version 7.2.2
        /// Overload Method to Express Checkout transaction with shipping address,billing address & buyerEmailId
        /// </summary>
        /// <param name="shippingAddress">shippingAddress of buyer</param>
        /// <param name="billingAddress">billingAddress of buyer</param>
        /// <param name="buyerEmailId">buyerEmailId of buyer</param>
        /// <returns>Returns PayPalResponse</returns>
        public PaypalResponse DoPaypalExpressCheckout(ZNode.Libraries.DataAccess.Entities.Address shippingAddress, ZNode.Libraries.DataAccess.Entities.Address billingAddress, string buyerEmailId)
        {
            PaypalResponse paypalResponse = new PaypalResponse();
            PaymentSetting PaymentSetting = new PaymentSetting();
            PaypalRecurringBillingInfo recurringBillingInfo = new PaypalRecurringBillingInfo();

            try
            {
                // Create the request object
                SetExpressCheckoutRequestType expCheckoutRequest = new SetExpressCheckoutRequestType();

                // Create the request details object
                expCheckoutRequest.SetExpressCheckoutRequestDetails = new SetExpressCheckoutRequestDetailsType();
                expCheckoutRequest.SetExpressCheckoutRequestDetails.PaymentAction = (PaymentActionCodeType)Enum.Parse(typeof(PaymentActionCodeType), this._paymentActionTypeCode);
                expCheckoutRequest.SetExpressCheckoutRequestDetails.PaymentActionSpecified = true;

                // Set Number of Recurring Payments Profile to be created
                int count = 1;
                foreach (ZNodeShoppingCartItem Item in this._shoppingCart.ShoppingCartItems)
                {
                    if (Item.Product.RecurringBillingInd == true)
                    {
                        count = count + 1;
                    }
                }

                BillingAgreementDetailsType[] BADetailsArray = new BillingAgreementDetailsType[count];
                int index = 0;
                foreach (ZNodeShoppingCartItem Item in this._shoppingCart.ShoppingCartItems)
                {
                    if (Item.Product.RecurringBillingInd == true)
                    {
                        BillingAgreementDetailsType BADetailsType = new BillingAgreementDetailsType();
                        BADetailsType.BillingType = BillingCodeType.RecurringPayments;
                        BADetailsType.BillingAgreementDescription = Item.Product.Name;
                        BADetailsArray.SetValue(BADetailsType, index++);
                    }

                    foreach (ZNodeAddOnEntity addon in Item.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity addonValue in addon.AddOnValueCollection)
                        {
                            if (addonValue.RecurringBillingInd == true)
                            {
                                BillingAgreementDetailsType BADetailsType = new BillingAgreementDetailsType();
                                BADetailsType.BillingType = BillingCodeType.RecurringPayments;
                                BADetailsType.BillingAgreementDescription = addonValue.Name;
                                BADetailsArray.SetValue(BADetailsType, index++);
                            }
                        }
                    }
                }

                PaymentDetailsItemType item = null;
                List<PaymentDetailsItemType> paypalItems = new List<PaymentDetailsItemType>();
                foreach (ZNodeShoppingCartItem shoppingCartItem in _shoppingCart.ShoppingCartItems)
                {
                    item = new PaymentDetailsItemType();
                    item.Name = shoppingCartItem.Product.Name;
                    item.Description = shoppingCartItem.Product.ShortDescription;
                    item.Number = shoppingCartItem.Product.SKU;
                    item.Quantity = shoppingCartItem.Quantity.ToString();
                    item.Amount = this.GetAmount(shoppingCartItem.PromotionalPrice);
                    paypalItems.Add(item);

                    foreach (ZNodeAddOnEntity addon in shoppingCartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity addonValue in addon.AddOnValueCollection)
                        {
                            item = new PaymentDetailsItemType();
                            item.Name = addonValue.Name;
                            item.Description = addonValue.Description;
                            item.Number = addonValue.SKU;
                            item.Quantity = shoppingCartItem.Quantity.ToString();
                            item.Amount = this.GetAmount(addonValue.FinalPrice);
                            paypalItems.Add(item);
                        }
                    }
                }

                if (this._shoppingCart.Discount > 0)
                {
                    // Add the discount field as shopping cart item with negative value.
                    item = new PaymentDetailsItemType();
                    item.Name = "Discount";
                    item.Description = string.Empty;
                    item.Number = string.Empty;
                    item.Quantity = "1";
                    item.Amount = this.GetAmount(-this._shoppingCart.Discount);
                    paypalItems.Add(item);
                }

                if (this._shoppingCart.GiftCardAmount > 0)
                {
                    // Add the gift card amount as shopping cart item with negative value.
                    item = new PaymentDetailsItemType();
                    item.Name = "Gift Card";
                    item.Description = string.Empty;
                    item.Number = string.Empty;
                    item.Quantity = "1";
                    item.Amount = this.GetAmount(-this._shoppingCart.GiftCardAmount);
                    paypalItems.Add(item);
                }


                PaymentDetailsType paymentDetails = new PaymentDetailsType();
                paymentDetails.ItemTotal = this.GetSubTotal(paypalItems.ToArray());
                paymentDetails.ShippingTotal = this.GetAmount(_shoppingCart.ShippingCost);
                paymentDetails.TaxTotal = this.GetAmount(_shoppingCart.TaxCost);
                paymentDetails.OrderTotal = this.GetAmount(_shoppingCart.TaxCost + _shoppingCart.ShippingCost + _shoppingCart.SubTotal - _shoppingCart.Discount - _shoppingCart.GiftCardAmount);
                paymentDetails.PaymentDetailsItem = paypalItems.ToArray();

                expCheckoutRequest.SetExpressCheckoutRequestDetails.PaymentDetails = new PaymentDetailsType[] { paymentDetails };

                #region Znode Version 7.2.2 Send Address to PayPal Api

                //For sending shipping & billing address to paypal api  - Start
                //if shipping address does not match with paypal address then it will show error message               
                if (shippingAddress != null)
                {
                    AddressType UserAddressType = new AddressType();
                    //Shipping Information
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.AddressOverride = "true";
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address = new AddressType();
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.Name = GetFullNameforPaypal(shippingAddress); 
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.Phone = shippingAddress.PhoneNumber;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.PostalCode = shippingAddress.PostalCode;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.StateOrProvince = shippingAddress.StateCode;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.Street1 = shippingAddress.Street;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.Street2 = shippingAddress.Street1;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.CityName = shippingAddress.City;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.CountryName = shippingAddress.CountryCode;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.CountrySpecified = true;

                    foreach (CountryCodeType codeType in Enum.GetValues(typeof(CountryCodeType)))
                    {
                        if (codeType.ToString().Equals(shippingAddress.CountryCode))
                        {
                            expCheckoutRequest.SetExpressCheckoutRequestDetails.Address.Country = codeType;
                            break;
                        }
                    }

                    //Billing Information
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress = new AddressType();
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.Name = GetFullNameforPaypal(billingAddress);
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.Street1 = billingAddress.Street;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.Street2 = billingAddress.Street1;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.PostalCode = billingAddress.PostalCode;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.Phone = billingAddress.PhoneNumber;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.StateOrProvince = billingAddress.StateCode;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.CityName = billingAddress.City;

                    foreach (CountryCodeType codeType in Enum.GetValues(typeof(CountryCodeType)))
                    {
                        if (codeType.ToString().Equals(billingAddress.CountryCode))
                        {
                            expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.Country = codeType;
                            break;
                        }
                    }
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.CountryName = billingAddress.CountryCode;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress.CountrySpecified = true;
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BuyerEmail = buyerEmailId;
                }              

                //For sending shipping & billing address to paypal api  - End
                #endregion

                expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAgreementDetails = BADetailsArray;
                expCheckoutRequest.SetExpressCheckoutRequestDetails.CancelURL = this._ECCancelUrl;
                expCheckoutRequest.SetExpressCheckoutRequestDetails.ReturnURL = this._ECRedirectUrl;

                SetExpressCheckoutResponseType ECResponsetype = (SetExpressCheckoutResponseType)this.caller.Call("SetExpressCheckout", expCheckoutRequest);

                if (ECResponsetype.Ack == AckCodeType.Success || ECResponsetype.Ack == AckCodeType.SuccessWithWarning)
                {
                    paypalResponse.PayalToken = ECResponsetype.Token;
                    string hostURL = paypalResponse.HostUrl;
                    paypalResponse.HostUrl = String.Format(hostURL, this.PaypalServerUrl, ECResponsetype.Token);
                    paypalResponse.ResponseCode = "0";
                }
                else
                {
                    paypalResponse.ResponseCode = ECResponsetype.Errors[0].ErrorCode;

                    if (decimal.Parse(paymentDetails.ItemTotal.Value) == 0 && paypalResponse.ResponseCode == "10413")
                    {
                        paypalResponse.ResponseText = "Sum of all product(s) amount should be greater than discount amount.";
                    }
                    else
                    {
                        paypalResponse.ResponseText = paypalResponse.ResponseCode.Equals("10736") ? "The shipping address and PayPal address does not match." : ECResponsetype.Errors[0].LongMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                paypalResponse.ResponseText = ex.Message;
                paypalResponse.ResponseCode = "-1";
            }

            return paypalResponse;
        }
        #endregion

        /// <summary>
        /// Get the BasicAmountType for the amount value.
        /// </summary>
        /// <param name="amount">Amount value to format.</param>
        /// <returns></returns>
        private BasicAmountType GetAmount(decimal amount)
        {
            var moneyAmount = new BasicAmountType();
            moneyAmount.currencyID = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), CurrencyCode);
            moneyAmount.Value = Math.Round(amount, 2).ToString();
            return moneyAmount;
        }

        /// <summary>
        /// Get the item sub-total value.
        /// </summary>
        /// <param name="paymentDetailsItemArray">PaymentDetailsItemType arracy object.</param>
        /// <returns>Returns the formatted BasicAmountType object.</returns>
        private BasicAmountType GetSubTotal(PaymentDetailsItemType[] paymentDetailsItemArray)
        {
            decimal itemSubtotal = 0;
            foreach (PaymentDetailsItemType lineItem in paymentDetailsItemArray)
            {
                itemSubtotal += decimal.Parse(lineItem.Amount.Value) * int.Parse(lineItem.Quantity);
            }

            return this.GetAmount(itemSubtotal);
        }


        /// <summary>
        /// Get information about an Express Checkout transaction
        /// </summary>
        /// <param name="token">Token of the checkout</param>
        /// <returns>Paypal Response</returns>
        public PaypalResponse GetExpressCheckoutDetails(string token)
        {
            // Create the request object
            GetExpressCheckoutDetailsRequestType expCheckoutRequest = new GetExpressCheckoutDetailsRequestType();
            PaypalResponse paypalResponse = new PaypalResponse();

            expCheckoutRequest.Token = token;

            GetExpressCheckoutDetailsResponseType ECDetailsResponse = (GetExpressCheckoutDetailsResponseType)this.caller.Call("GetExpressCheckoutDetails", expCheckoutRequest);

            if (ECDetailsResponse.Ack == AckCodeType.SuccessWithWarning || ECDetailsResponse.Ack == AckCodeType.Success)
            {
                paypalResponse.PayerID = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.PayerID;

                // Set Shipping Address 
                ZNodeAddress _shippingAddress = new ZNodeAddress();
                _shippingAddress.City = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.CityName;
                _shippingAddress.FirstName = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Name;
                if (_shippingAddress.FirstName.Length == 0)
                {
                    _shippingAddress.FirstName = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.PayerName.FirstName;
                }

                _shippingAddress.LastName = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.PayerName.LastName;
                _shippingAddress.CountryCode = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Country.ToString();
                _shippingAddress.EmailId = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Payer;
                _shippingAddress.PhoneNumber = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Phone;
                if (_shippingAddress.PhoneNumber == null)
                {
                    _shippingAddress.PhoneNumber = string.Empty;
                }

                _shippingAddress.StateCode = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.StateOrProvince;
                _shippingAddress.PostalCode = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.PostalCode;

                paypalResponse.ShippingAddress = _shippingAddress;
                paypalResponse.ResponseCode = "0";
            }
            else
            {
                paypalResponse.ResponseText = ECDetailsResponse.Errors[0].LongMessage;
                paypalResponse.ResponseCode = ECDetailsResponse.Errors[0].ErrorCode;
            }

            return paypalResponse;
        }

        /// <summary>
        /// Complete an Express Checkout transaction. Returns ZNodePaymentResponse object
        /// </summary>
        /// <param name="token">token of the express checkout</param>
        /// <param name="payerID">ID of the payer</param>
        /// <returns>returns the payment response</returns>
        public ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse DoExpressCheckoutPayment(string token, string payerID)
        {
            // Create the request object
            DoExpressCheckoutPaymentRequestType expCheckoutRequest = new DoExpressCheckoutPaymentRequestType();
            PaypalResponse response = new PaypalResponse();
            ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse payment_response = new ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse();

            CurrencyCodeType currencyCodeTypeId = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), this.CurrencyCode);

            // Create the request details object
            expCheckoutRequest.DoExpressCheckoutPaymentRequestDetails = new DoExpressCheckoutPaymentRequestDetailsType();
            expCheckoutRequest.DoExpressCheckoutPaymentRequestDetails.Token = token;
            expCheckoutRequest.DoExpressCheckoutPaymentRequestDetails.PayerID = payerID;
            expCheckoutRequest.DoExpressCheckoutPaymentRequestDetails.PaymentAction = (PaymentActionCodeType)Enum.Parse(typeof(PaymentActionCodeType), this._paymentActionTypeCode);

            // Set Order details
            PaymentDetailsItemType item = null;
            List<PaymentDetailsItemType> paypalItems = new List<PaymentDetailsItemType>();
            foreach (ZNodeShoppingCartItem shoppingCartItem in _shoppingCart.ShoppingCartItems)
            {
                item = new PaymentDetailsItemType();
                item.Name = shoppingCartItem.Product.Name;
                item.Description = shoppingCartItem.Product.ShortDescription;
                item.Number = shoppingCartItem.Product.SKU;
                item.Quantity = shoppingCartItem.Quantity.ToString();
                item.Amount = this.GetAmount(shoppingCartItem.PromotionalPrice);
                paypalItems.Add(item);


                foreach (ZNodeAddOnEntity addon in shoppingCartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addonValue in addon.AddOnValueCollection)
                    {
                        item = new PaymentDetailsItemType();
                        item.Name = addonValue.Name;
                        item.Description = addonValue.Description;
                        item.Number = addonValue.SKU;
                        item.Quantity = shoppingCartItem.Quantity.ToString();
                        item.Amount = this.GetAmount(addonValue.FinalPrice);
                        paypalItems.Add(item);
                    }
                }
            }

            if (this._shoppingCart.Discount > 0)
            {
                // Add the discount field as shopping cart item with negative value.
                item = new PaymentDetailsItemType();
                item.Name = "Discount";
                item.Description = string.Empty;
                item.Number = string.Empty;
                item.Quantity = "1";
                item.Amount = this.GetAmount(-this._shoppingCart.Discount);
                paypalItems.Add(item);
            }

            if (this._shoppingCart.GiftCardAmount > 0)
            {
                // Add the gift card amount as shopping cart item with negative value.
                item = new PaymentDetailsItemType();
                item.Name = "Gift Card";
                item.Description = string.Empty;
                item.Number = string.Empty;
                item.Quantity = "1";
                item.Amount = this.GetAmount(-this._shoppingCart.GiftCardAmount);
                paypalItems.Add(item);
            }

            PaymentDetailsType paymentDetails = new PaymentDetailsType();
            paymentDetails.ItemTotal = this.GetSubTotal(paypalItems.ToArray());
            paymentDetails.ShippingTotal = this.GetAmount(_shoppingCart.ShippingCost);
            paymentDetails.TaxTotal = this.GetAmount(_shoppingCart.TaxCost);
            paymentDetails.OrderTotal = this.GetAmount(_shoppingCart.TaxCost + _shoppingCart.ShippingCost + _shoppingCart.SubTotal - _shoppingCart.Discount - _shoppingCart.GiftCardAmount);
            paymentDetails.PaymentDetailsItem = paypalItems.ToArray();

            expCheckoutRequest.DoExpressCheckoutPaymentRequestDetails.PaymentDetails = new PaymentDetailsType[] { paymentDetails };

            // Create the response object
            DoExpressCheckoutPaymentResponseType ECPaymentResponse = (DoExpressCheckoutPaymentResponseType)this.caller.Call("DoExpressCheckoutPayment", expCheckoutRequest);

            if (ECPaymentResponse.Ack == AckCodeType.Success || ECPaymentResponse.Ack == AckCodeType.SuccessWithWarning)
            {
                payment_response.ResponseText = ECPaymentResponse.Ack.ToString();
                response.PaymentStatus = ECPaymentResponse.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].PaymentStatus.ToString();
                payment_response.PaymentStatus = response.GetPaymentStatus();
                payment_response.TransactionId = ECPaymentResponse.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].TransactionID.ToString();
                payment_response.IsSuccess = true;

                // Create Recurring Payments Profile
                if (payment_response.IsSuccess)
                {
                    // Code for Creating Recurring Payments Profile
                    PaymentDetailsItemType[] Ritems = new PaymentDetailsItemType[this._shoppingCart.Count];
                    int index = 0;
                    decimal RecurringTotal = 0.00M;
                    bool subscriptionResult = true;
                    string responseText = "This gateway does not support all of the billing periods that you have set on products. Please be sure to update the billing period on all your products.";
                    int counter = 0;

                    // Loop through the Order Line Items
                    foreach (OrderLineItem orderLineItem in this._orderLineItems)
                    {

                        ZNodeShoppingCartItem cartItem;
                        if (_addressCartItems != null && _addressCartItems.Any())
                            cartItem = _addressCartItems.ElementAt(counter++);
                        else
                            cartItem = this._shoppingCart.ShoppingCartItems[counter++];

                        decimal productPrice = (cartItem.TieredPricing + cartItem.ShippingCost + cartItem.TaxCost) * cartItem.Quantity;

                        if (cartItem.Product.RecurringBillingInd && subscriptionResult)
                        {
                            PaypalRecurringBillingInfo recurringBilling = new PaypalRecurringBillingInfo();
                            recurringBilling.InitialAmount = Math.Round(cartItem.Product.RecurringBillingInitialAmount, 2);
                            recurringBilling.InstallmentInd = cartItem.Product.RecurringBillingInstallmentInd;
                            recurringBilling.Period = cartItem.Product.RecurringBillingPeriod;
                            recurringBilling.TotalCycles = cartItem.Product.RecurringBillingTotalCycles;
                            recurringBilling.Frequency = cartItem.Product.RecurringBillingFrequency;
                            recurringBilling.Amount = Math.Round(productPrice, 2);
                            recurringBilling.ProfileName = cartItem.Product.SKU + " - " + orderLineItem.OrderLineItemID.ToString();
                            RecurringTotal = RecurringTotal + productPrice;

                            ZNode.Libraries.ECommerce.Entities.ZNodeSubscriptionResponse val = this.CreateRecurringPaymentsProfile(token, payerID, recurringBilling, cartItem.Product.Name);

                            if (val.IsSuccess)
                            {
                                val.ReferenceId = orderLineItem.OrderLineItemID;
                                payment_response.RecurringBillingSubscriptionResponse.Add(val);
                            }
                            else
                            {
                                subscriptionResult &= false;
                                responseText = val.ResponseText + val.ResponseCode;
                                break;
                            }
                        }

                        foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                        {
                            index = 0;

                            foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                            {
                                if (addOnValue.RecurringBillingInd && subscriptionResult && addOnValue.FinalPrice > 0)
                                {
                                    productPrice = addOnValue.FinalPrice * cartItem.Quantity;

                                    PaypalRecurringBillingInfo recurringBilling = new PaypalRecurringBillingInfo();
                                    recurringBilling.InitialAmount = Math.Round(addOnValue.RecurringBillingInitialAmount, 2);
                                    recurringBilling.InstallmentInd = addOnValue.RecurringBillingInstallmentInd;
                                    recurringBilling.Period = addOnValue.RecurringBillingPeriod;
                                    recurringBilling.TotalCycles = addOnValue.RecurringBillingTotalCycles;
                                    recurringBilling.Frequency = addOnValue.RecurringBillingFrequency;
                                    recurringBilling.Amount = Math.Round(productPrice, 2);
                                    recurringBilling.ProfileName = addOnValue.SKU + " - " + orderLineItem.OrderLineItemID.ToString();

                                    ZNode.Libraries.ECommerce.Entities.ZNodeSubscriptionResponse val = this.CreateRecurringPaymentsProfile(token, payerID, recurringBilling, addOnValue.Name);

                                    if (val.IsSuccess)
                                    {
                                        val.ParentLineItemId = orderLineItem.OrderLineItemID;
                                        val.ReferenceId = orderLineItem.OrderLineItemCollection[index].OrderLineItemID;
                                        payment_response.RecurringBillingSubscriptionResponse.Add(val);
                                    }
                                    else
                                    {
                                        subscriptionResult &= false;
                                        responseText = val.ResponseText + val.ResponseCode;
                                        break;
                                    }
                                }

                                if (!subscriptionResult)
                                {
                                    break;
                                }

                                index++;
                            }
                        }
                    }

                    // if any subscription fails, then revert all the transactions and Cancel the Subscription Created
                    if (!subscriptionResult)
                    {
                        foreach (ZNode.Libraries.ECommerce.Entities.ZNodeSubscriptionResponse subscriptionResp in payment_response.RecurringBillingSubscriptionResponse)
                        {
                            // Cancel Subscription
                            ManageRecurringPaymentsProfileStatusRequestType recPaymentsRequest = new ManageRecurringPaymentsProfileStatusRequestType();
                            recPaymentsRequest.ManageRecurringPaymentsProfileStatusRequestDetails = new ManageRecurringPaymentsProfileStatusRequestDetailsType();
                            recPaymentsRequest.ManageRecurringPaymentsProfileStatusRequestDetails.ProfileID = subscriptionResp.SubscriptionId;
                            recPaymentsRequest.ManageRecurringPaymentsProfileStatusRequestDetails.Action = StatusChangeActionType.Cancel;

                            // Create the response object
                            ManageRecurringPaymentsProfileStatusResponseType recPaymaentProfileresponse = (ManageRecurringPaymentsProfileStatusResponseType)this.caller.Call("ManageRecurringPaymentsProfileStatus", recPaymentsRequest);
                            if (recPaymaentProfileresponse.Ack == AckCodeType.Success || recPaymaentProfileresponse.Ack == AckCodeType.SuccessWithWarning)
                            {
                                payment_response.ResponseText = recPaymaentProfileresponse.Ack.ToString();
                            }
                        }

                        if (!string.IsNullOrEmpty(payment_response.TransactionId))
                        {
                            // Refund Payment
                            RefundTransactionRequestType concreteRequest = new RefundTransactionRequestType();
                            string refundType = "Partial";
                            decimal RefundAmount = this.OrderTotal - RecurringTotal;
                            if ((RefundAmount > 0 && RefundAmount.ToString().Length > 0) && refundType.Equals("Partial"))
                            {
                                BasicAmountType amtType = new BasicAmountType();
                                amtType.Value = RefundAmount.ToString("N2");
                                amtType.currencyID = CurrencyCodeType.USD;
                                concreteRequest.Amount = amtType;
                                concreteRequest.RefundType = RefundType.Partial;
                            }
                            else
                            {
                                concreteRequest.RefundType = RefundType.Full;
                            }

                            concreteRequest.RefundTypeSpecified = true;
                            concreteRequest.TransactionID = payment_response.TransactionId;
                            concreteRequest.Memo = "Test note";

                            // Execute the API operation and obtain the response.
                            RefundTransactionResponseType refundTransResponse = new RefundTransactionResponseType();
                            refundTransResponse = (RefundTransactionResponseType)this.caller.Call("RefundTransaction", concreteRequest);

                            if (refundTransResponse.Ack == AckCodeType.Success || refundTransResponse.Ack == AckCodeType.SuccessWithWarning)
                            {
                                payment_response.ResponseText = refundTransResponse.Ack.ToString();
                            }
                        }

                        payment_response = new ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse();
                        payment_response.ResponseCode = "-1";
                        payment_response.ResponseText = responseText;
                    }
                }
            }
            else if (ECPaymentResponse.Ack == AckCodeType.Failure || ECPaymentResponse.Ack == AckCodeType.FailureWithWarning)
            {
                payment_response.IsSuccess = false;
                var error = ECPaymentResponse.Errors.FirstOrDefault();
                if (error != null)
                    payment_response.ResponseText = error.LongMessage;
                payment_response.PaymentStatus = ZNode.Libraries.ECommerce.Entities.ZNodePaymentStatus.CREDIT_DECLINED;
            }
            else
            {
                payment_response.IsSuccess = true;
                payment_response.PaymentStatus = ZNode.Libraries.ECommerce.Entities.ZNodePaymentStatus.CREDIT_PENDING;
            }

            return payment_response;
        }

        /// <summary>
        /// Method creates Recurring Payments Profile
        /// </summary>
        /// <param name="token">token of the payment</param>
        /// <param name="payerID">ID of the payer</param>
        /// <param name="recurringBillingInfo">Recurring bill information</param>
        /// <param name="ProfileDesc">profile description</param>
        /// <returns>Returns ZNodeSubscriptionResponse object</returns>
        public ZNode.Libraries.ECommerce.Entities.ZNodeSubscriptionResponse CreateRecurringPaymentsProfile(string token, string payerID, PaypalRecurringBillingInfo recurringBillingInfo, string ProfileDesc)
        {
            // string token,string payerID, DateTime date, string amount, int BF, BillingPeriodType BP, CurrencyCodeType currencyCodeType,PaymentSetting PaymentSetting, string ECRedirectUrl, string ECReturnUrl)
            PaypalResponse response = new PaypalResponse();
            ZNode.Libraries.ECommerce.Entities.ZNodeSubscriptionResponse subscription_response = new ZNode.Libraries.ECommerce.Entities.ZNodeSubscriptionResponse();

            DateTime currentdate = System.DateTime.Now.Date;

            this._shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            this._paymentSetting = PaymentSetting;

            // create the request object
            CreateRecurringPaymentsProfileRequestType recPaymentRequest = new CreateRecurringPaymentsProfileRequestType();
            recPaymentRequest.Version = "51.0";

            // Add request-specific fields to the request object.
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails = new CreateRecurringPaymentsProfileRequestDetailsType();
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.Token = token;
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.RecurringPaymentsProfileDetails = new RecurringPaymentsProfileDetailsType();
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.RecurringPaymentsProfileDetails.BillingStartDate = currentdate;
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails = new ScheduleDetailsType();
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod = new BillingPeriodDetailsType();
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.Amount = new BasicAmountType();
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.Amount.Value = recurringBillingInfo.Amount.ToString("N2");
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.Amount.currencyID = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), this.CurrencyCode);
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.BillingFrequency = int.Parse(recurringBillingInfo.Frequency.ToString());
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.BillingPeriod = (BillingPeriodType)Enum.Parse(typeof(BillingPeriodType), response.GetPaypalBillingPeriod(recurringBillingInfo.Period));
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.TotalBillingCycles = recurringBillingInfo.TotalCycles;
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.Description = ProfileDesc;

            // Execute the API operation and obtain the response.
            CreateRecurringPaymentsProfileResponseType refundTransResponse = new CreateRecurringPaymentsProfileResponseType();
            refundTransResponse = (CreateRecurringPaymentsProfileResponseType)this.caller.Call("CreateRecurringPaymentsProfile", recPaymentRequest);
            if (refundTransResponse.Ack == AckCodeType.SuccessWithWarning || refundTransResponse.Ack == AckCodeType.Success)
            {
                subscription_response.ResponseText = refundTransResponse.Ack.ToString();
                subscription_response.SubscriptionId = refundTransResponse.CreateRecurringPaymentsProfileResponseDetails.ProfileID.ToString();
                subscription_response.IsSuccess = true;
            }
            else
            {
                subscription_response.ResponseText = refundTransResponse.Errors[0].LongMessage;
                subscription_response.ResponseCode = refundTransResponse.Errors[0].ErrorCode;
            }

            return subscription_response;
        }
        #endregion

        #region private methods
        /// <summary>
        /// Initiate Merchant details for an API
        /// </summary>
        /// <param name="apiUsername">API User Name</param>
        /// <param name="apiPassword">API Password</param>
        /// <param name="signature">API Signature</param>
        /// <param name="IsTestMode">Test mode or not</param>
        /// <returns>Returns the Profile</returns>
        private static IAPIProfile CreateAPIProfile(string apiUsername, string apiPassword, string signature, bool IsTestMode)
        {
            IAPIProfile profile = ProfileFactory.createSignatureAPIProfile();
            profile.APIUsername = apiUsername;
            profile.APIPassword = apiPassword;
            profile.APISignature = signature;
            profile.Subject = string.Empty;

            if (!IsTestMode)
            {
                // Test mode is not enabled,then set Profile Environment as "live" - Production Server
                profile.Environment = "live";
            }

            return profile;
        }
        #endregion

        #region Znode Version 7.2.2

        /// <summary>
        /// Znode Version 7.2.2
        /// Get Full Name to Show in PayPal Account
        /// </summary>
        /// <param name="currentAddress">currentAddress for getting  Full Name to Show in PayPal Account </param>
        /// <returns>Customer fullname to show in PayPal Account</returns>
        private string GetFullNameforPaypal(ZNode.Libraries.DataAccess.Entities.Address currentAddress)
        {
            string fullName = string.Empty;
            if (currentAddress != null)
            {
                if (!string.IsNullOrEmpty(currentAddress.FirstName))
                {
                    fullName = currentAddress.FirstName.Trim() + " ";
                }

                if (!string.IsNullOrEmpty(currentAddress.LastName))
                {
                    fullName = fullName + currentAddress.LastName.Trim();
                }
            }
            return fullName.Trim();
        }
        #endregion
    }
}
