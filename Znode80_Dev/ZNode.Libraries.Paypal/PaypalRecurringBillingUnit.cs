﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZNode.Libraries.Paypal
{
    /// <summary>
    /// Paypal recurring billing unit enumerations
    /// </summary>
    public enum PaypalRecurringBillingUnit
    {
        /// <summary>
        /// Represents a Day
        /// </summary>
        Day = 1,

        /// <summary>
        /// Represents a Week
        /// </summary>
        Week = 7,

        /// <summary>
        /// Represents a Semi Month
        /// </summary>
        SemiMonth  = 15,

        /// <summary>
        /// Represents a Month
        /// </summary>
        Month = 30,

        /// <summary>
        /// Represents a Year
        /// </summary>
        Year  = 365,

        /// <summary>
        /// Represents No Billing period
        /// </summary>
        NoBillingPeriodType = 0,
    }
}
