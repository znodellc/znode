using System;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Processes different payment types
    /// </summary>
    [Serializable()]
    public class ZNodePayment : ZNodeBusinessBase
    {
        #region Member Variables
        private bool _IsRecurringBillingExists = false;
        private GatewayToken _GatewayToken = new GatewayToken();
        private CreditCard _CreditCard = new CreditCard();
        private ZNode.Libraries.DataAccess.Entities.Address _BillingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
        private ZNode.Libraries.DataAccess.Entities.Address _ShippingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
        private PaymentSetting _PaymentSetting = new PaymentSetting();
        private ZNodeShoppingCart _ShoppingCart;
        private string _PaymentName = string.Empty;
        private decimal _NonRecurringItemsTotalAmount = 0;
        private int _TokenId = 0;
        private string _SessionId = string.Empty;
        private bool _Is3DSecure = false;
        private string _WorldPayEchodata = string.Empty;
        private string _WorldPayPostData = string.Empty;
        private string _WorldPayHeaderCookie = string.Empty;
        private bool _SaveCardData = false;
        private bool _UseToken = false;
        private string _TransactionID = string.Empty;
        private string _AuthCode = string.Empty;
        private string _SubscriptionID = string.Empty;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the credit card
        /// </summary>
        public CreditCard CreditCard
        {
            get
            {
                return this._CreditCard;
            }

            set
            {
                this._CreditCard = value;
            }
        }

        /// <summary>
        /// Gets or sets the gateway token
        /// </summary>
        public GatewayToken GatewayToken
        {
            get
            {
                return this._GatewayToken;
            }

            set
            {
                this._GatewayToken = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the recurring billing exists.
        /// </summary>
        public bool IsRecurringBillingExists
        {
            get { return this._IsRecurringBillingExists; }
            set { _IsRecurringBillingExists = value; }
        }

        /// <summary>
        /// Gets or sets the payment name.
        /// </summary>
        public string PaymentName
        {
            get { return this._PaymentName; }
            set { this._PaymentName = value; }
        }

        /// <summary>
        /// Gets or sets the token Id.
        /// </summary>
        public int TokenId
        {
            get
            {
                return this._TokenId;
            }

            set
            {
                this._TokenId = value;
            }
        }

        /// <summary>
        /// Gets or sets the billing address 
        /// </summary>
        public ZNode.Libraries.DataAccess.Entities.Address BillingAddress
        {
            get { return this._BillingAddress; }
            set { this._BillingAddress = value; }
        }

        /// <summary>
        /// Gets or sets the Shipping Address 
        /// </summary>
        public ZNode.Libraries.DataAccess.Entities.Address ShippingAddress
        {
            get { return this._ShippingAddress; }
            set { this._ShippingAddress = value; }
        }

        /// <summary>
        /// Gets or sets the payment settings
        /// </summary>
        public PaymentSetting PaymentSetting
        {
            get
            {
                return this._PaymentSetting;
            }

            set
            {
                this._PaymentSetting = value;
            }
        }

        /// <summary>
        /// Gets or sets the shopping cart.
        /// </summary>
        public ZNodeShoppingCart ShoppingCart
        {
            get
            {
                if (this._ShoppingCart == null)
                {
                    // Get the user account from session
                    this._ShoppingCart = (ZNodeShoppingCart)HttpContext.Current.Session[ZNodeSessionKeyType.ShoppingCart.ToString()];
                }

                return this._ShoppingCart;
            }

            set
            {
                this._ShoppingCart = value;
            }
        }

        /// <summary>
        /// Gets or sets the session Id (used for worldpay gateway) 
        /// </summary>
        public string SessionId
        {
            get { return this._SessionId; }
            set { this._SessionId = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the card is 3D secured or not.
        /// </summary>
        public bool Is3DSecure
        {
            get { return this._Is3DSecure; }
            set { this._Is3DSecure = value; }
        }

        /// <summary>
        /// Gets or sets the Echo data from Worldpay response
        /// </summary>
        public string WorldPayEchodata
        {
            get { return this._WorldPayEchodata; }
            set { this._WorldPayEchodata = value; }
        }

        /// <summary>
        /// Gets or sets the Worldpay post data.
        /// </summary>
        public string WorldPayPostData
        {
            get { return this._WorldPayPostData; }
            set { this._WorldPayPostData = value; }
        }

        /// <summary>
        /// Gets or sets the Worldpay header cookie.
        /// </summary>
        public string WorldPayHeaderCookie
        {
            get { return this._WorldPayHeaderCookie; }
            set { this._WorldPayHeaderCookie = value; }
        }

        /// <summary>
        /// Gets or sets the transaction Id.
        /// </summary>
        public string TransactionID
        {
            get { return this._TransactionID; }
            set { this._TransactionID = value; }
        }

        /// <summary>
        /// Gets or sets the authentication code.
        /// </summary>
        public string AuthCode
        {
            get { return this._AuthCode; }
            set { this._AuthCode = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to save the credir card data for furter transaction.
        /// </summary>
        public bool SaveCardData
        {
            get { return this._SaveCardData; }
            set { this._SaveCardData = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use token or not.
        /// </summary>
        public bool UseToken
        {
            get { return this._UseToken; }
            set { this._UseToken = value; }
        }

        /// <summary>
        /// Gets or sets the subscription Id.
        /// </summary>
        public string SubscriptionID
        {
            get { return this._SubscriptionID; }
            set { this._SubscriptionID = value; }
        }

        #endregion

        /// <summary>
        /// Gets the non recurring item total amount.
        /// </summary>
        public decimal NonRecurringItemsTotalAmount
        {
            get
            {
                if (this._NonRecurringItemsTotalAmount == 0 && _ShoppingCart != null)
                {
                    foreach (ZNodeShoppingCartItem cartItem in this._ShoppingCart.ShoppingCartItems)
                    {
                        decimal taxCost = cartItem.Product.SalesTax + cartItem.Product.VAT + cartItem.Product.HST + cartItem.Product.GST + cartItem.Product.PST;

                        if (!cartItem.Product.RecurringBillingInd)
                        {
                            this._NonRecurringItemsTotalAmount += (cartItem.TieredPricing * cartItem.Quantity) + taxCost + cartItem.Product.ShippingCost;
                        }
                        else
                        {
                            this._IsRecurringBillingExists = true;
                        }

                        foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                        {
                            foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                            {
                                if (!addOnValue.RecurringBillingInd)
                                {
                                    this._NonRecurringItemsTotalAmount += (addOnValue.FinalPrice * cartItem.Quantity) + addOnValue.ShippingCost + addOnValue.VAT + addOnValue.SalesTax + addOnValue.HST + addOnValue.PST + addOnValue.GST;
                                }
                                else
                                {
                                    this._IsRecurringBillingExists = true;
                                }
                            }
                        }
                    }
                }

                return this._NonRecurringItemsTotalAmount;
            }
        }
    }
}
