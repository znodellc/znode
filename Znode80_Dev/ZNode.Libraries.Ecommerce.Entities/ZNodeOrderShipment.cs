﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
	[Serializable()]
	public class ZNodeOrderShipment : ZNodeBusinessBase
	{
		public string SlNo { get; set; }
		public int ShippingID { get; set; }
		public int AddressID { get; set; }
		public int Quantity { get; set; }
        public string ItemGUID { get; set; }
        public string ShippingName { get; set; }

        /// <summary>
        /// Generates the SlNo for the New ZnodeOrdershipment
        /// </summary>
		public ZNodeOrderShipment()
		{
            SlNo = System.Guid.NewGuid().ToString();
		}

        /// <summary>
        /// Generates the SlNo for the New ZnodeOrdershipment and assigned the values for AddressID,Quantity and item GUID
        /// </summary>
        /// <param name="addressId"></param>
        /// <param name="quantity"></param>
        /// <param name="itemGuid"></param>
        public ZNodeOrderShipment(int addressId,int quantity,string itemGuid)
        {
            SlNo = System.Guid.NewGuid().ToString();
            this.AddressID = addressId;
            this.Quantity = quantity;
            this.ItemGUID = itemGuid;
        }
       
        public ZNodeOrderShipment(int addressId, int quantity, string itemGuid, int shippingId, string shippingName)
        {
            SlNo = System.Guid.NewGuid().ToString();
            this.AddressID = addressId;
            this.Quantity = quantity;
            this.ItemGUID = itemGuid;
            this.ShippingID = shippingId;
            this.ShippingName = shippingName;
        }
	}
}
