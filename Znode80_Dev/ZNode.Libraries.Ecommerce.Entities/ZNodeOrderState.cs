using System;
using System.Collections.Generic;
using System.Text;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Specifies the status of the Order
    /// </summary>
    public enum ZNodeOrderState 
    { 
        /// <summary>
        /// Order state is Submitted
        /// </summary>
        SUBMITTED = 10, 
        
        /// <summary>
        /// Order state is Shipped
        /// </summary>
        SHIPPED = 20, 

        /// <summary>
        /// Order state is Returned
        /// </summary>       
        RETURNED = 30, 

        /// <summary>
        /// Order state is Cancelled
        /// </summary>
        CANCELLED = 40,

        /// <summary>
        /// Order state is Pending Approval.
        /// </summary>
        PENDING_APPROVAL = 50
    }
}
