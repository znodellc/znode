﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.Ecommerce.Entities
{
    public class ZNodeSearchEngineResult
    {
       public IEnumerable<ZNodeCategoryNavigation> CategoryNavigation { get; set; }
       public List<string> Ids { get; set; }
       public List<ZNodeFacet> Facets { get; set; }


    }
}
