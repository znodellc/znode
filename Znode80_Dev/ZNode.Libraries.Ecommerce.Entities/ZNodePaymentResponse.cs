using System;
using System.Collections.Generic;
using System.Text;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents a payment submission response including response codes and success flags
    /// </summary>
    public class ZNodePaymentResponse : ZNodeBusinessBase
    {
        #region Private Member Variables
        private string _ResponseCode = string.Empty;
        private string _ResponseText = string.Empty;
        private string _RedirectURL = string.Empty;
        private string _TransactionId;
        private bool _IsSuccess = false;
        private ZNodePaymentStatus _PaymentStatus;
        private string _EchoData = string.Empty;
        private string _IssuerPostData = string.Empty;
        private string _WorldPayHeaderCookie = string.Empty;
        private ZNodeGenericCollection<ZNodeSubscriptionResponse> _RecurringBillingSubscriptionResponse = new ZNodeGenericCollection<ZNodeSubscriptionResponse>();
        private string _CardAuthorizationCode = string.Empty;
        private string _SubscriptionID = string.Empty;
        private string _GatewayResponseData = string.Empty;
        private string _TransactionType = string.Empty;
        private string _CardType = string.Empty;
        private string _CardNumber = string.Empty;
        private string _ProfileID = string.Empty;
        private string _PaymentProfileID = string.Empty;
        private string _ShippingAddressID = string.Empty; 
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Gateway Response code 
        /// </summary>
        public string ResponseCode
        {
            get { return this._ResponseCode; }
            set { this._ResponseCode = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway Response Text 
        /// </summary>
        public string ResponseText
        {
            get { return this._ResponseText; }
            set { this._ResponseText = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway redirect URL - which holds Google Post URL 
        /// </summary>
        public string RedirectURL
        {
            get { return this._RedirectURL; }
            set { this._RedirectURL = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway TransactionID 
        /// </summary>
        public string TransactionId
        {
            get { return this._TransactionId; }
            set { this._TransactionId = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the payment response is success or failure.
        /// </summary>
        public bool IsSuccess
        {
            get { return this._IsSuccess; }
            set { this._IsSuccess = value; }
        }

        /// <summary>
        ///  Gets or sets the payment status
        /// </summary>
        public ZNodePaymentStatus PaymentStatus
        {
            get { return this._PaymentStatus; }
            set { this._PaymentStatus = value; }
        }

        /// <summary>
        /// Gets or sets the echo data.
        /// </summary>
        public string EchoData
        {
            get { return this._EchoData; }
            set { this._EchoData = value; }
        }

        /// <summary>
        /// Gets or sets the issuer post data.
        /// </summary>
        public string IssuerPostData
        {
            get { return this._IssuerPostData; }
            set { this._IssuerPostData = value; }
        }

        /// <summary>
        /// Gets or sets the WorldPay header cookie.
        /// </summary>
        public string WorldPayHeaderCookie
        {
            get { return this._WorldPayHeaderCookie; }
            set { this._WorldPayHeaderCookie = value; }
        }

        /// <summary>
        /// Gets or sets the recurring billing subscription response.
        /// </summary>
        public ZNodeGenericCollection<ZNodeSubscriptionResponse> RecurringBillingSubscriptionResponse
        {
            get { return this._RecurringBillingSubscriptionResponse; }
            set { this._RecurringBillingSubscriptionResponse = value; }
        }

        /// <summary>
        /// Gets or sets the card authorization code or request token information from gateway
        /// </summary>
        public string CardAuthorizationCode
        {
            get { return this._CardAuthorizationCode; }
            set { this._CardAuthorizationCode = value; }
        }

        /// <summary>
        /// Gets or sets the Subscription Id from the gateway response 
        /// </summary>
        public string SubscriptionID
        {
            get { return this._SubscriptionID; }
            set { this._SubscriptionID = value; }
        }

        /// <summary>
        /// Gets or sets the gateway response data.
        /// </summary>
        public string GatewayResponseData
        {
            get { return this._GatewayResponseData; }
            set { this._GatewayResponseData = value; }
        }

        /// <summary>
        /// Gets or sets the transaction type.
        /// </summary>
        public string TransactionType
        {
            get { return this._TransactionType; }
            set { this._TransactionType = value; }
        }

        /// <summary>
        /// Gets or sets the card type.
        /// </summary>
        public string CardType
        {
            get { return this._CardType; }
            set { this._CardType = value; }
        }

        /// <summary>
        /// Gets or sets the card number
        /// </summary>
        public string CardNumber
        {
            get { return this._CardNumber; }
            set { this._CardNumber = value; }
        }

        /// <summary>
        /// Gets or sets the customer profile Id
        /// </summary>
        public string ProfileID
        {
            get { return this._ProfileID; }
            set { this._ProfileID = value; }
        }

        /// <summary>
        /// Gets or sets the payment profile Id
        /// </summary>
        public string PaymentProfileID
        {
            get { return this._PaymentProfileID; }
            set { this._PaymentProfileID = value; }
        }

        /// <summary>
        /// Gets or sets the shipping address Id
        /// </summary>
        public string ShippingAddressID
        {
            get { return this._ShippingAddressID; }
            set { this._ShippingAddressID = value; }
        } 
        #endregion
    }
}
