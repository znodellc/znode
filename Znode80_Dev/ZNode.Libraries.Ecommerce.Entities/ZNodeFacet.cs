﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.Ecommerce.Entities
{
   public class ZNodeFacet
    {
            public string AttributeName { set; get; }
            public List<ZNodeFacetValue> AttributeValues { set; get; }
            public int ControlTypeID { get; set; }
            public ZNodeFacet()
            {
              
            }
    }

    
     
}
