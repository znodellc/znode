﻿using Znode.Plugin.Framework.Domain.Models;

namespace Znode.Plugin.Framework.Maps
{
    public class PluginEntityMap
    {
        public PluginEntity ToEntity(IPlugin source)
        {
            return new PluginEntity()
            {
                Name = source.Name,
                Version = source.Version.ToString(),
                Installed = source.IsInstalled
            };

        }
    }
}