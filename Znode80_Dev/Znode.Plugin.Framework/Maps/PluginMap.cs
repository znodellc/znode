﻿using Znode.Plugin.Framework.Models;

namespace Znode.Plugin.Framework.Maps
{
    public class PluginVmMap
    {
        public PluginModel ToVm(IPlugin source)
        {
            return new PluginModel()
            {
                Name = source.Name,
                Title = source.Title,
                Version = source.Version.ToString(),
                IsInstalled = source.IsInstalled,
            };
        }

    }
}
