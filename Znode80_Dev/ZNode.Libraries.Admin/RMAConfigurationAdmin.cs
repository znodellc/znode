﻿using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Service;
using System.Data;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Represent the product review state enum
    /// Enumeration ID value must match with ZNodeProductReviewState table ProductReviewStateID column.
    /// </summary>
    public enum ZNodeRequestState
    {
        
        /// <summary>
        /// RMARequest is awaiting for Approval.
        /// </summary>
        PendingAuthorization = 1,

        /// <summary>
        /// RMARequest is approved.
        /// </summary>
        Authorized = 2,

        /// <summary>
        ///RMARequest is Returned Or Refunded.
        /// </summary>
        ReturnedOrRefunded = 3,

        /// <summary>
        /// RMARequest is cancelled.
        /// </summary>
        Void = 4
    }
    public class RMAConfigurationAdmin:ZNodeBusinessBase
    {
        #region RMA Configuration
        /// <summary>
        /// Get RMA Configuration by Id.
        /// </summary>
        /// <param name="RMAConfigurationID">RMA Configuration ID to get the RMA Configuration object.</param>
        /// <returns>Returns the RMAConfiguration object.</returns>
        public RMAConfiguration GetByRMAConfigurationID(int RMAConfigurationID)
        {
            ZNode.Libraries.DataAccess.Service.RMAConfigurationService RMAConfigurationService = new ZNode.Libraries.DataAccess.Service.RMAConfigurationService();
            ZNode.Libraries.DataAccess.Entities.RMAConfiguration RMAConfiguration = RMAConfigurationService.GetByRMAConfigID(RMAConfigurationID);
            return RMAConfiguration;
        }

        /// <summary>
        /// Returns all RMA Configuration
        /// </summary>
        /// <returns>Returns list of RMA Configuration object.</returns>
        public TList<RMAConfiguration> GetAllRMAConfiguration()
        {
            ZNode.Libraries.DataAccess.Service.RMAConfigurationService rmaConfigurationService = new ZNode.Libraries.DataAccess.Service.RMAConfigurationService();
            TList<ZNode.Libraries.DataAccess.Entities.RMAConfiguration> rmaConfigurationList = rmaConfigurationService.GetAll();

            return rmaConfigurationList;
        }
        /// <summary>
        /// Add New RMA Configuration.
        /// </summary>
        /// <param name="RMAConfiguration">RMA Configuration to insert.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Add(RMAConfiguration rmaConfiguration)
        {
            ZNode.Libraries.DataAccess.Service.RMAConfigurationService rmaConfigurationService = new ZNode.Libraries.DataAccess.Service.RMAConfigurationService();
            return rmaConfigurationService.Insert(rmaConfiguration);
        }


        /// <summary>
        /// Update RMA Configuration
        /// </summary>
        /// <param name="userRMAConfiguration">RMA Configuration object update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(RMAConfiguration rmaConfiguration)
        {
            ZNode.Libraries.DataAccess.Service.RMAConfigurationService rmaConfigurationService = new RMAConfigurationService();
            return rmaConfigurationService.Update(rmaConfiguration);
        }

        /// <summary>
        /// Delete RMA Configuration.
        /// </summary>
        /// <param name="RMAConfigurationId">RMAConfiguration Id to delete the RMAConfiguration object.</param>
        /// <returns>Returns true of deleted else false.</returns>
        public bool Delete(int rmaConfigurationId)
        {
            ZNode.Libraries.DataAccess.Service.RMAConfigurationService rmaConfigurationService = new RMAConfigurationService();
            return rmaConfigurationService.Delete(rmaConfigurationId);
        }

        #endregion

        #region Reason For return
        /// <summary>
        /// Get Reason for Return by Id.
        /// </summary>
        /// <param name="ReasonFoReturnID">Reason For Return ID to get the Reason For Return object.</param>
        /// <returns>Returns the ReasonFoReturn object.</returns>
        public ReasonForReturn GetByReasonFoReturnID(int ReasonFoReturnID)
        {
            ZNode.Libraries.DataAccess.Service.ReasonForReturnService reasonforreturnService = new ZNode.Libraries.DataAccess.Service.ReasonForReturnService();
            ZNode.Libraries.DataAccess.Entities.ReasonForReturn reasonforreturn = reasonforreturnService.GetByReasonForReturnID(ReasonFoReturnID);
            return reasonforreturn;
        }

        /// <summary>
        /// Returns all Reason for Return
        /// </summary>
        /// <returns>Returns list of Reason for Return object.</returns>
        public TList<ReasonForReturn> GetAllReasonForReturn()
        {
            ZNode.Libraries.DataAccess.Service.ReasonForReturnService reasonforreturnService = new ZNode.Libraries.DataAccess.Service.ReasonForReturnService();
            TList<ZNode.Libraries.DataAccess.Entities.ReasonForReturn> reasonForReturnList = reasonforreturnService.GetAll();

            return reasonForReturnList;
        }
        /// <summary>
        /// Returns all Reason for Return
        /// </summary>
        /// <returns>Returns list of Reason for Return object.</returns>
        public DataSet GetReasonForReturn()
        {
            ZNode.Libraries.DataAccess.Custom.RMAHelper rmaHelper = new DataAccess.Custom.RMAHelper();
            return rmaHelper.GetReasonForReturn();          
        }


        /// <summary>
        /// Add New Reason For Return.
        /// </summary>
        /// <param name="reasonForReturn">Reason For Return to insert.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Add(ReasonForReturn reasonForReturn)
        {
            ZNode.Libraries.DataAccess.Service.ReasonForReturnService reasonForReturnService = new ZNode.Libraries.DataAccess.Service.ReasonForReturnService();
            return reasonForReturnService.Insert(reasonForReturn);            
        }

      
        /// <summary>
        /// Update Reason For Return
        /// </summary>
        /// <param name="userReasonForReturn">Reason For Return object update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(ReasonForReturn reasonForReturn)
        {
            ZNode.Libraries.DataAccess.Service.ReasonForReturnService reasonForReturnService = new ReasonForReturnService();
            return reasonForReturnService.Update(reasonForReturn);
        }

        /// <summary>
        /// Delete Reason For Return.
        /// </summary>
        /// <param name="ReasonForReturnId">ReasonForReturn Id to delete the ReasonForReturn object.</param>
        /// <returns>Returns true of deleted else false.</returns>
        public bool DeleteReasonForReturn(int ReasonForReturnId)
        {
            ZNode.Libraries.DataAccess.Service.ReasonForReturnService reasonForReturnService = new ReasonForReturnService();
            return reasonForReturnService.Delete(ReasonForReturnId);
        }

        #endregion

        #region Request Status

        /// <summary>
        /// Get Request Status by Id.
        /// </summary>
        /// <param name="RequestStatusID">Request status ID to get the Request status object.</param>
        /// <returns>Returns the RequestStatus object.</returns>
        public RequestStatus GetByRequestStatusID(int RequestStatusID)
        {
            ZNode.Libraries.DataAccess.Service.RequestStatusService requestStatusService = new ZNode.Libraries.DataAccess.Service.RequestStatusService();
            ZNode.Libraries.DataAccess.Entities.RequestStatus requestStatus = requestStatusService.GetByRequestStatusID(RequestStatusID);
            return requestStatus;
        }

       
        /// <summary>
        /// Returns all Request Status
        /// </summary>
        /// <returns>Returns list of Request Status object.</returns>
        public TList<RequestStatus> GetAllRequestStatus()
        {
            ZNode.Libraries.DataAccess.Service.RequestStatusService requestStatusService = new ZNode.Libraries.DataAccess.Service.RequestStatusService();
            TList<ZNode.Libraries.DataAccess.Entities.RequestStatus> requestStatusList = requestStatusService.GetAll();

            return requestStatusList;
        }


        /// <summary>
        /// Add New Request Status.
        /// </summary>
        /// <param name="requestStatus">Request Status to insert.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Add(RequestStatus requestStatus)
        {
            ZNode.Libraries.DataAccess.Service.RequestStatusService requestStatusService = new ZNode.Libraries.DataAccess.Service.RequestStatusService();
            return requestStatusService.Insert(requestStatus);
        }


        /// <summary>
        /// Update Request Status
        /// </summary>
        /// <param name="userRequestStatus">Request Status object update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(RequestStatus requestStatus)
        {
            ZNode.Libraries.DataAccess.Service.RequestStatusService requestStatusService = new RequestStatusService();
            return requestStatusService.Update(requestStatus);
        }

        /// <summary>
        /// Delete Request Status.
        /// </summary>
        /// <param name="RequestStatusId">RequestStatus Id to delete the RequestStatus object.</param>
        /// <returns>Returns true of deleted else false.</returns>
        public bool DeleteRequestStatus(int RequestStatusId)
        {
            ZNode.Libraries.DataAccess.Service.RequestStatusService requestStatusService = new RequestStatusService();
            return requestStatusService.Delete(RequestStatusId);
        }

        #endregion


    }
}
