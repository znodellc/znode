﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace ZNode.Libraries.Admin
{
    public class SearchAdmin
    {
        /// <summary>
        ///  Add a new product to data source.
        /// </summary>
        /// <param name="productBoost">LuceneGlobalProductBoost object to add.</param>
        /// <param name="luceneGlobalProductBoostId">Return the added Id (out)</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddLuceneGlobalProductBoost(LuceneGlobalProductBoost productBoost, out int luceneGlobalProductBoostId)
        {
            luceneGlobalProductBoostId = 0;

            var service = new LuceneGlobalProductBoostService();

            if (!service.Insert(productBoost))
            {
                return false;
            }

            luceneGlobalProductBoostId = productBoost.LuceneGlobalProductBoostID;

            // Clear all cache memory.
            CacheManager.Clear();

            return true;
        }

        public bool AddLuceneGlobalProductCategoryBoost(LuceneGlobalProductCategoryBoost productCategoryBoost, out int luceneGlobalProductCategoryBoostId)
        {
            luceneGlobalProductCategoryBoostId = 0;

            var service = new LuceneGlobalProductCategoryBoostService();

            if (!service.Insert(productCategoryBoost))
            {
                return false;
            }

            luceneGlobalProductCategoryBoostId = productCategoryBoost.LuceneGlobalProductCategoryBoostID;

            // Clear all cache memory.
            CacheManager.Clear();

            return true;
        }

        public LuceneDocumentMapping GetLuceneFieldLevelBoost(int luceneDocumentMappingId)
        {
            var service = new LuceneDocumentMappingService();
            var luceneFieldLevelBoost = service.GetByLuceneDocumentMappingID(luceneDocumentMappingId);

            return luceneFieldLevelBoost;
        } 
        
        public List<LuceneDocumentMapping> GetLuceneFieldLevelBoosts()
        {
            var service = new LuceneDocumentMappingService();
            var luceneFieldLevelBoosts = service.GetAll();

            var luceneFieldLevelList = luceneFieldLevelBoosts.Where(dm => dm.FieldBoostable).ToList();

            return luceneFieldLevelList;
        } 

        public LuceneGlobalProductBoost GetLuceneGlobalProductBoost(int productId)
        {
            var service = new LuceneGlobalProductBoostService();
            var productBoost = service.GetByProductID(productId);

            return productBoost;
        }

        public LuceneGlobalProductCategoryBoost GetLuceneGlobalProductCategoryBoost(int productCategoryId)
        {
            var service = new LuceneGlobalProductCategoryBoostService();
            var productCategoryBoost = service.GetByProductCategoryID(productCategoryId);

            return productCategoryBoost;
        }

        public bool UpdateLuceneGlobalProductBoost(LuceneGlobalProductBoost luceneGlobalProductBoost)
        {
            var service = new LuceneGlobalProductBoostService();
            var isUpdated = service.Update(luceneGlobalProductBoost);

            // Clear all cache memory.
            CacheManager.Clear();

            return isUpdated;
        }

        public bool UpdateLuceneFieldLevelBoost(LuceneDocumentMapping luceneDocumentMapping)
        {
            var service = new LuceneDocumentMappingService();
            var isUpdated = service.Update(luceneDocumentMapping);

            CacheManager.Clear();

            return isUpdated;
        }

        public bool UpdateLuceneGlobalProductCategoryBoost(LuceneGlobalProductCategoryBoost luceneGlobalProductCategoryBoost)
        {
            var service = new LuceneGlobalProductCategoryBoostService();
            var isUpdated = service.Update(luceneGlobalProductCategoryBoost);

            // Clear all cache memory.
            CacheManager.Clear();

            return isUpdated;
        }
    }
}
