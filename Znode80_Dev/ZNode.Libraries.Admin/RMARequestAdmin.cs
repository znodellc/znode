﻿using System;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;


namespace ZNode.Libraries.Admin
{
    public class RMARequestAdmin : ZNodeBusinessBase
    {
        #region RMARequest Public Methods
        /// <summary>
        /// Get RMA Request by Id.
        /// </summary>
        /// <param name="rmaRequestID">RMA Request ID to get the RMA Request object.</param>
        /// <returns>Returns the RMA Request object.</returns>
        public RMARequest GetByRMARequestID(int rmaRequestID)
        {
            ZNode.Libraries.DataAccess.Service.RMARequestService rmaRequestService = new ZNode.Libraries.DataAccess.Service.RMARequestService();
            ZNode.Libraries.DataAccess.Entities.RMARequest rmaRequest = rmaRequestService.GetByRMARequestID(rmaRequestID);
            return rmaRequest;
        }

        /// <summary>
        /// Returns all RMA Request
        /// </summary>
        /// <returns>Returns list of RMA Request object.</returns>
        public TList<RMARequest> GetAllRMARequest()
        {
            ZNode.Libraries.DataAccess.Service.RMARequestService rmaRequestService = new ZNode.Libraries.DataAccess.Service.RMARequestService();
            TList<ZNode.Libraries.DataAccess.Entities.RMARequest> rmaRequest = rmaRequestService.GetAll();

            return rmaRequest;
        }

        /// <summary>
        /// Get all RMA Request by Reason For Return ID
        /// </summary>
        /// <param name="ReasonForReturnID">Reason For Return ID to search.</param>
        /// <returns>Returns the RMA request search result dataset.</returns>
        public DataSet GetAllRMARequestByReasonID(int ReasonForReturnID)
        {
            ZNode.Libraries.DataAccess.Custom.RMAHelper rmaHelper = new RMAHelper();
            return rmaHelper.GetAllRMARequestByReasonID(ReasonForReturnID);

        }
        /// <summary>
        /// Get all RMA Request Items by OrderID
        /// </summary>
        /// <param name="OrderID">OrderID to search.</param>
        /// <returns>Returns the RMA request Items search result dataset.</returns>
        public DataSet GetAllRMARequestItemByOrderID(int OrderID)
        {
            ZNode.Libraries.DataAccess.Custom.RMAHelper rmaHelper = new RMAHelper();
            return rmaHelper.GetAllRMARequestItemByOrderID(OrderID);

        }


        /// <summary>
        /// Add New RMA Request.
        /// </summary>
        /// <param name="rmaRequest">RMA Request to insert.</param>
        /// <param name="productId">Return the added RMARequest Id (out)</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Add(RMARequest rmaRequest, out int RMARequestId)
        {
            RMARequestId = 0;
            ZNode.Libraries.DataAccess.Service.RMARequestService rmaRequestService = new ZNode.Libraries.DataAccess.Service.RMARequestService();

            bool isAdded = rmaRequestService.Insert(rmaRequest);
            if (!isAdded)
            {
                return false;
            }

            RMARequestId = rmaRequest.RMARequestID;

            // Clear all cache memory.
            CacheManager.Clear();

            return isAdded;
        }

        /// <summary>
        /// Update RMA Request
        /// </summary>
        /// <param name="rmaRequest">RMA Request object update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(RMARequest rmaRequest)
        {
            ZNode.Libraries.DataAccess.Service.RMARequestService rmaRequestService = new RMARequestService();
            return rmaRequestService.Update(rmaRequest);
        }

        /// <summary>
        /// Delete RMA Request.
        /// </summary>
        /// <param name="rmaRequestId">RMARequest Id to delete the RMARequest object.</param>
        /// <returns>Returns true of deleted else false.</returns>
        public bool Delete(int rmaRequestId)
        {
            ZNode.Libraries.DataAccess.Service.RMARequestService rmaRequestService = new RMARequestService();
            return rmaRequestService.Delete(rmaRequestId);
        }
        #endregion

        #region RMARequestItem Public Methods
        /// <summary>
        /// Get RMA Request Item by Id.
        /// </summary>
        /// <param name="rmaRequestItemID">RMA Request Item ID to get the RMA Request Item object.</param>
        /// <returns>Returns the RMA Request Item object.</returns>
        public RMARequestItem GetByRMARequestItemID(int rmaRequestItemID)
        {
            ZNode.Libraries.DataAccess.Service.RMARequestItemService rmaRequestItemService = new ZNode.Libraries.DataAccess.Service.RMARequestItemService();
            ZNode.Libraries.DataAccess.Entities.RMARequestItem rmaRequestItem = rmaRequestItemService.GetByRMARequestItemID(rmaRequestItemID);

            return rmaRequestItem;
        }
      

        /// <summary>
        /// Returns all RMA Request
        /// </summary>
        /// <returns>Returns list of RMA Request object.</returns>
        public TList<RMARequestItem> GetAllRMARequestItem()
        {
            ZNode.Libraries.DataAccess.Service.RMARequestItemService rmaRequestItemService = new ZNode.Libraries.DataAccess.Service.RMARequestItemService();
            TList<ZNode.Libraries.DataAccess.Entities.RMARequestItem> rmaRequestItem = rmaRequestItemService.GetAll();

            return rmaRequestItem;
        }

        /// <summary>
        /// Add New RMA Request Item.
        /// </summary>
        /// <param name="rmaRequestItem">RMA Request Item to insert.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Add(RMARequestItem rmaRequestItem)
        {
            ZNode.Libraries.DataAccess.Service.RMARequestItemService rmaRequestItemService = new ZNode.Libraries.DataAccess.Service.RMARequestItemService();
            return rmaRequestItemService.Insert(rmaRequestItem);
        }

        /// <summary>
        /// Update RMA Request Item
        /// </summary>
        /// <param name="rmaRequestItem">RMA Request Item object update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(RMARequestItem rmaRequestItem)
        {
            ZNode.Libraries.DataAccess.Service.RMARequestItemService rmaRequestItemService = new RMARequestItemService();
            return rmaRequestItemService.Update(rmaRequestItem);
        }

        /// <summary>
        /// Delete RMA Request.
        /// </summary>
        /// <param name="rmaRequestItemId">RMARequest Item Id to delete the RMARequest Item object.</param>
        /// <returns>Returns true of deleted else false.</returns>
        public bool DeleteRequestItemByRMARequestID(int rmaRequestId)
        {
            ZNode.Libraries.DataAccess.Custom.RMAHelper rmaHelper = new RMAHelper();
            return rmaHelper.DeleteItemsByRMARequestID(rmaRequestId);
        }

        /// <summary>
        /// Get all RMA Request Items by RMA Request Item IDs
        /// </summary>
        /// <param name="RMARequestItemIDs">RMA RequestItem IDs ID to search.</param>
        /// <returns>Returns the RMA request Items search result dataset.</returns>
        public DataSet GetRMARequestItem(string RMARequestItemIDs)
        {
            ZNode.Libraries.DataAccess.Custom.RMAHelper rmaHelper = new RMAHelper();
            return rmaHelper.GetRMARequestItem(RMARequestItemIDs);
        }

        /// <summary>
        /// Get GiftCard details for RMA Request Items by RMA Request IDs
        /// </summary>
        /// <param name="RMARequestID">RMA Request ID ID to search.</param>
        /// <returns>Returns the RMA request Items search result dataset.</returns>
        public DataSet GetGiftCardByRMARequest(int RMARequestID)
        {
            ZNode.Libraries.DataAccess.Custom.RMAHelper rmaHelper = new RMAHelper();
            return rmaHelper.GetGiftCardByRMARequest(RMARequestID);
        }

        /// <summary>
        /// Get all RMA Request Report items by RMA Request  ID
        /// </summary>
        /// <param name="RMARequestID">RMA Request ID ID to search.</param>
        /// <returns>Returns the RMA request Items search result dataset.</returns>
        public DataSet GetRMARequestReport(int RMARequestID)
        {
            ZNode.Libraries.DataAccess.Custom.RMAHelper rmaHelper = new RMAHelper();
            return rmaHelper.GetRMARequestReport(RMARequestID);
        }

        /// <summary>
        /// Get Orderline items for RMA request based on Order ID
        /// </summary>
        /// <param name="OrderID"></param>
        /// <param name="RMARequestID"></param>
        /// <returns></returns>
        public DataSet GetRMAOrderLineItemsByOrderID(int OrderID, int RMARequestID)
        {
            ZNode.Libraries.DataAccess.Custom.RMAHelper rmaHelper = new RMAHelper();
            return rmaHelper.GetRMAOrderLineItemsByOrderID(OrderID, RMARequestID, 0,"");
        }
        /// <summary>
        /// Get Orderline items for RMA request based on Order ID
        /// </summary>
        /// <param name="OrderID"></param>
        /// <param name="RMARequestID"></param>
        /// <param name="IsReturnable"></param>
        /// <returns></returns>
        public DataSet GetRMAOrderLineItemsByOrderID(int OrderID, int RMARequestID, int IsReturnable)
        {
            ZNode.Libraries.DataAccess.Custom.RMAHelper rmaHelper = new RMAHelper();
            return rmaHelper.GetRMAOrderLineItemsByOrderID(OrderID, RMARequestID, IsReturnable,"");
        }
        /// <summary>
        /// Get Orderline items for RMA request based on Order ID
        /// </summary>
        /// <param name="OrderID"></param>
        /// <param name="RMARequestID"></param>
        /// <param name="IsReturnable"></param>
        /// <returns></returns>
        public DataSet GetRMAOrderLineItemsByOrderID(int OrderID, int RMARequestID, int IsReturnable,string flag)
        {
            ZNode.Libraries.DataAccess.Custom.RMAHelper rmaHelper = new RMAHelper();
            return rmaHelper.GetRMAOrderLineItemsByOrderID(OrderID, RMARequestID, IsReturnable,flag);
        }
        /// <summary>
        /// Get AppendRMA Flag by RMA request Id.
        /// </summary>
        /// <param name="rmaRequestId">rma RequestId to Get Append RMAFlag.</param>        
        /// <returns>Returns true if APPEND RMA is 1 else returns false.</returns>
        public bool GetAppendRMAFlag(int rmaRequestId)
        {
            ZNode.Libraries.DataAccess.Custom.RMAHelper rmaHelper = new RMAHelper();
            return rmaHelper.GetAppendRMAFlag(rmaRequestId);
        }
        /// <summary>
        /// Get Order RMA Display Flag by Order Id.
        /// </summary>
        /// <param name="OrderId">Order Id to Get RMA display Flag.</param>        
        /// <returns>Returns true if Order RMA  is 1 else returns false.</returns>
        public bool GetOrderRMAFlag(int OrderId)
        {
            ZNode.Libraries.DataAccess.Custom.RMAHelper rmaHelper = new RMAHelper();
            return rmaHelper.GetOrderRMAFlag(OrderId);
        }

        #endregion
    }

}
