using System.Data;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage tax rules
    /// </summary>
    public class TaxRuleAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Get all the tax rules
        /// </summary>
        /// <param name="portalId">Portal Id to get the list of tax rules.</param>
        /// <returns>Returns list of TaxRule object.</returns>
        public TList<TaxRule> GetAllTaxRulesByPortal(int portalId)
        {
            TaxRuleService taxRuleService = new TaxRuleService();
            return taxRuleService.GetByPortalID(portalId);
        }

        /// <summary>
        /// Get all the tax rules  by taxClassId
        /// </summary>
        /// <param name="taxClassId">Tax class Id to get the tax tule.</param>
        /// <returns>Returns the list of TaxRule object.</returns>
        public TList<TaxRule> GetAllTaxRulesByTaxClassID(int taxClassId)
        {
            TaxRuleService taxRuleService = new TaxRuleService();
            return taxRuleService.GetByTaxClassID(taxClassId);
        }

        /// <summary>
        /// Add a new tax rule
        /// </summary>
        /// <param name="taxRule">TaxRule object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddTaxRule(TaxRule taxRule)
        {
            TaxRuleService taxRuleService = new TaxRuleService();
            return taxRuleService.Insert(taxRule);
        }

        /// <summary>
        /// Delete tax rule
        /// </summary>
        /// <param name="taxRule">TaxRule object to insert.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteTaxRule(TaxRule taxRule)
        {
            TaxRuleService taxRuleService = new TaxRuleService();
            return taxRuleService.Delete(taxRule);
        }

        /// <summary>
        /// Update a tax rule
        /// </summary>
        /// <param name="taxRule">TaxRule object to insert.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool UpdateTaxRule(TaxRule taxRule)
        {
            TaxRuleService taxRuleService = new TaxRuleService();
            return taxRuleService.Update(taxRule);
        }

        /// <summary>
        /// Get tax rule by tax rule Id.
        /// </summary>
        /// <param name="taxRuleId">Tax rule Id to get the tax rule object.</param>
        /// <returns>Returns the TaxRule object.</returns>
        public TaxRule GetTaxRule(int taxRuleId)
        {
            TaxRuleService taxRuleService = new TaxRuleService();
            return taxRuleService.GetByTaxRuleID(taxRuleId);
        }

        /// <summary>
        /// Get county code by state code.
        /// </summary>
        /// <param name="stateAbbr">State code abbreviation</param>
        /// <returns>Returns the country code dataset.</returns>
        public DataSet GetCountyCodeByStateAbbr(string stateAbbr)
        {
            ZNode.Libraries.DataAccess.Custom.TaxHelper taxHelper = new ZNode.Libraries.DataAccess.Custom.TaxHelper();
            return taxHelper.GetCountyCodeByStatAbbr(stateAbbr);
        }

        /// <summary>
        /// Get all the Tax Class
        /// </summary>
        /// <returns>Returns list of TaxClass object.</returns>
        public TList<TaxClass> GetAllTaxClass()
        {
            TaxClassService taxClassService = new TaxClassService();
            TList<TaxClass> taxClassList = taxClassService.GetAll();

            return taxClassList;
        }

        /// <summary>
        /// Get all details by taxClassId
        /// </summary>
        /// <param name="taxClassId">Tax class Id to get the tax class object.</param>
        /// <returns>Returns the TaxClass object.</returns>
        public TaxClass GetByTaxClassID(int taxClassId)
        {
            TaxClassService taxClassService = new TaxClassService();
            return taxClassService.GetByTaxClassID(taxClassId);
        }

        /// <summary>
        /// Get the deep loaded tax class by tax class Id.
        /// </summary>
        /// <param name="taxClassId">Tax class Id to get the deep loaded tax class.</param>
        /// <returns>Returns the deep loaded TaxClass object.</returns>
        public TaxClass DeepLoadByTaxClassId(int taxClassId)
        {
            TaxClassService taxClassService = new TaxClassService();
            return taxClassService.DeepLoadByTaxClassID(taxClassId, true, DeepLoadType.IncludeChildren, typeof(TList<TaxRule>));
        }

        /// <summary>
        /// Update the tax class
        /// </summary>
        /// <param name="taxClass">TaxClass object to update.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool UpdateTaxClass(TaxClass taxClass)
        {
            TaxClassService taxRuleService = new TaxClassService();
            return taxRuleService.Update(taxClass);
        }

        /// <summary>
        /// Insert a tax class.
        /// </summary>
        /// <param name="taxClass">TaxClass object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool InsertTaxClass(TaxClass taxClass)
        {
            TaxClassService taxRuleService = new TaxClassService();
            return taxRuleService.Insert(taxClass);
        }

        /// <summary>
        /// Delete a tax class by tax class Id.
        /// </summary>
        /// <param name="taxClassId">Tax class Id to delete the tax class object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteTaxClass(int taxClassId)
        {
            TaxClassService taxRuleService = new TaxClassService();
            return taxRuleService.Delete(taxClassId);
        }

        /// <summary>
        /// Get a county name by countyFIPS 
        /// </summary>
        /// <param name="CountyFIPS">Country FIPS value.</param>
        /// <returns>Reutrns the country name.</returns>
        public string GetNameByCountyFIPS(string CountyFIPS)
        {
            ZipCodeService taxRuleService = new ZipCodeService();
            ZipCodeQuery query = new ZipCodeQuery();
            query.Append(ZipCodeColumn.CountyFIPS, CountyFIPS);

            TList<ZipCode> zipCodeList = taxRuleService.Find(query.GetParameters());

            if (zipCodeList.Count > 0)
            {
                return zipCodeList[0].CountyName;
            }

            return string.Empty;
        }

        /// <summary>
        /// Get all the tax rule types
        /// </summary>
        /// <returns>Returns the list of TaxRuleType object.</returns>
        public TList<TaxRuleType> GetTaxRuleTypes()
        {
            TaxRuleTypeService taxRuleService = new TaxRuleTypeService();
            return taxRuleService.GetAll();
        }

		/// <summary>
		/// Get the tax rule type Id by class name.
		/// </summary>
		/// <param name="className">Class name to get the tax rule type.</param>
		/// <returns>Returns the top 1 tax rule type id. </returns>
		public int GetTaxRuleTypeId(string className)
		{
			TList<TaxRuleType> taxRuleTypeList = DataRepository.TaxRuleTypeProvider.Find("ClassName = '" + className + "'");

			if (taxRuleTypeList.Count > 0)
			{
				taxRuleTypeList.Sort("TaxRuleTypeID Desc");

				return taxRuleTypeList[0].TaxRuleTypeID;
			}

			return 0;
		}
    }
}
