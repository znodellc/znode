using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Represents the AccountProfileAdmin class.
    /// </summary>
    public class AccountProfileAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Get the deep loaded AccountProfile object list
        /// </summary>
        /// <param name="accountId">Account Id to get the AccountProfile object</param>
        /// <returns>Returns deep loaded list of AccountProfile object</returns>
        public TList<AccountProfile> DeepLoadProfilesByAccountID(int accountId)
        {
            AccountProfileService accountProfileService = new AccountProfileService();
            TList<AccountProfile> accountProfileList = accountProfileService.GetByAccountID(accountId);
            accountProfileService.DeepLoad(accountProfileList, true, DeepLoadType.IncludeChildren, typeof(Profile));

            return accountProfileList;
        }

        /// <summary>
        /// Insert the AccountProfile into data source.
        /// </summary>
        /// <param name="accountProfile">AccountProfile object to insert.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Insert(AccountProfile accountProfile)
        {
            AccountProfileService accountProfileService = new AccountProfileService();
            return accountProfileService.Insert(accountProfile);
        }

        /// <summary>
        /// Delete the AccountProfile by account profile Id
        /// </summary>
        /// <param name="accountProfileId">Account profile Id to delete from the data source</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool Delete(int accountProfileId)
        {
            AccountProfileService accountProfileService = new AccountProfileService();
            return accountProfileService.Delete(accountProfileId);
        }

        /// <summary>
        /// Check whether the profile exists or not.
        /// </summary>
        /// <param name="accountId">Account Id to check the profile</param>
        /// <param name="profileId">Profile Id to check the profile.</param>
        /// <returns>Returns true if profile exists else false.</returns>
        public bool ProfileExists(int accountId, int profileId)
        {
            AccountProfileService accountProfileService = new AccountProfileService();
            AccountProfileQuery query = new AccountProfileQuery();
            query.Append(AccountProfileColumn.AccountID, accountId.ToString());
            query.Append(AccountProfileColumn.ProfileID, profileId.ToString());
            TList<AccountProfile> accountProfile = accountProfileService.Find(query);
            if (accountProfile.Count > 0)
            {
                return true;
            }

            return false;
        }
    }
}
