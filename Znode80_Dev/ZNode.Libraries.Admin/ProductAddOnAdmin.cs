using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage product addons, addon Values
    /// </summary>
    public class ProductAddOnAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Get the Locale information for the product that has other languages.
        /// </summary>
        /// <param name="addOnValueId">AddOnValue Id to get locale Id</param>
        /// <returns>Returns locale Id dataset.</returns>
        public static DataSet GetLocaleIdsByAddOnValueId(int addOnValueId)
        {
            ProductHelper productHelper = new ProductHelper();
            return productHelper.GetLocaleIdsByAddOnValueId(addOnValueId, ZNodeConfigManager.SiteConfig.PortalID);
        }

        #region Related to ZNodeProductAddon Table

        /// <summary>
        /// Get all product addons for a portal
        /// </summary>
        /// <returns>Returns list of ProductAddOn object.</returns>
        public TList<ProductAddOn> GetAllProductAddOns()
        {
            ZNode.Libraries.DataAccess.Service.ProductAddOnService productAddOnService = new ZNode.Libraries.DataAccess.Service.ProductAddOnService();
            TList<ZNode.Libraries.DataAccess.Entities.ProductAddOn> productAddonList = productAddOnService.GetAll();

            return productAddonList;
        }

        /// <summary>
        /// Check whether this is Addon is already assocaited with this product
        /// </summary>
        /// <param name="productAddOn">ProductAddOn to check.</param>
        /// <returns>Returns true if ProductAddOn exists otherwise false.</returns>
        public bool IsAddOnExists(ProductAddOn productAddOn)
        {
            ZNode.Libraries.DataAccess.Service.ProductAddOnService productAddOnService = new ZNode.Libraries.DataAccess.Service.ProductAddOnService();
            string whereClause = "ProductId = " + productAddOn.ProductID + " and AddOnId=" + productAddOn.AddOnID;

            int listCount = productAddOnService.GetTotalItems(whereClause, out listCount);

            if (listCount == 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Add new product Addon
        /// </summary>
        /// <param name="productAddOn">Product AddOn to add.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddNewProductAddOn(ProductAddOn productAddOn)
        {
            ZNode.Libraries.DataAccess.Service.ProductAddOnService productAddOnService = new ZNode.Libraries.DataAccess.Service.ProductAddOnService();
            bool isAdded = productAddOnService.Insert(productAddOn);

            return isAdded;
        }

        /// <summary>
        /// Delete a product Addon
        /// </summary>
        /// <param name="productAddOnId">Product AddOn Id to delete the ProductAddOn object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteProductAddOn(int productAddOnId)
        {
            ZNode.Libraries.DataAccess.Service.ProductAddOnService productAddOnService = new ZNode.Libraries.DataAccess.Service.ProductAddOnService();
            bool isDeleted = productAddOnService.Delete(productAddOnId);

            return isDeleted;
        }

        /// <summary>
        ///  Get a ProductAddOn by product Id
        /// </summary>
        /// <param name="productId">Product Id to get the product addon.</param>
        /// <returns>Returns list of ProductAddOn object.</returns>
        public TList<ProductAddOn> GetByProductId(int productId)
        {
            ZNode.Libraries.DataAccess.Service.ProductAddOnService productAddOnService = new ZNode.Libraries.DataAccess.Service.ProductAddOnService();
            TList<ZNode.Libraries.DataAccess.Entities.ProductAddOn> productAddonList = productAddOnService.GetByProductID(productId);

            return productAddonList;
        }
        #endregion

        #region Related to ZNodeAddon table

        /// <summary>
        /// Search the Addons for the given input values
        /// </summary>
        /// <param name="name">AddOn name to search.</param>
        /// <param name="title">AddOn titlt to search.</param>
        /// <param name="productSku">Product SKU  to search.</param>
        /// <param name="localeId">Locale Id to search.</param>
        /// <returns>Returns AddOn search result dataset.</returns>
        public DataSet SearchAddOns(string name, string title, string productSku, int localeId)
        {
            return this.SearchAddOns(name, title, productSku, localeId, 0, 0);
        }

        /// <summary>
        /// Search the Addons for the given input values
        /// </summary>
        /// <param name="name">AddOn name to search.</param>
        /// <param name="title">AddOn titlt to search.</param>
        /// <param name="productSku">Product SKU  to search.</param>
        /// <param name="localeId">Locale Id to search.</param>
        /// <param name="accountId">Account Id to search.</param>
        /// <returns>Returns AddOn search result dataset.</returns>
        public DataSet SearchAddOns(string name, string title, string productSku, int localeId, int accountId)
        {
            return this.SearchAddOns(name, title, productSku, localeId, accountId, 0);
        }

        /// <summary>
        /// Search the Addons for the given input values
        /// </summary>
        /// <param name="name">AddOn name to search.</param>
        /// <param name="title">AddOn titlt to search.</param>
        /// <param name="productSku">Product SKU  to search.</param>
        /// <param name="localeId">Locale Id to search.</param>
        /// <param name="accountId">Account Id to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <returns>Returns AddOn search result dataset.</returns>
        public DataSet SearchAddOns(string name, string title, string productSku, int localeId, int accountId, int portalId)
        {
            ProductHelper productHelper = new ProductHelper();
            return productHelper.SearchAddOns(name, title, productSku, localeId, accountId, portalId);
        }

        /// <summary>
        /// Get all Addons for a portal
        /// </summary>
        /// <returns>Returns list of AddOn object.</returns>
        public TList<AddOn> GetAllAddOns()
        {
            ZNode.Libraries.DataAccess.Service.AddOnService productAddOnService = new ZNode.Libraries.DataAccess.Service.AddOnService();
            TList<ZNode.Libraries.DataAccess.Entities.AddOn> productAddonList = productAddOnService.GetAll();

            return productAddonList;
        }

        /// <summary>
        /// Get Addons by account Id.
        /// </summary>
        /// <param name="accountId">Account Id to get the AddOn.</param>
        /// <returns>Returns list of AddOn object.</returns>
        public TList<AddOn> GetAddonsByAccountId(int accountId)
        {
            ZNode.Libraries.DataAccess.Service.AddOnService productAddOnService = new ZNode.Libraries.DataAccess.Service.AddOnService();
            TList<ZNode.Libraries.DataAccess.Entities.AddOn> productAddonList = productAddOnService.GetByAccountID(accountId);

            return productAddonList;
        }

        /// <summary>
        /// Get a Addon by AddOn Id.
        /// </summary>
        /// <param name="addOnId">AddOn Id to get the AddOn object.</param>
        /// <returns>Returns AddOn object for the AddOn Id.</returns>
        public AddOn GetByAddOnId(int addOnId)
        {
            ZNode.Libraries.DataAccess.Service.AddOnService productAddOnService = new ZNode.Libraries.DataAccess.Service.AddOnService();
            ZNode.Libraries.DataAccess.Entities.AddOn addOn = productAddOnService.GetByAddOnID(addOnId);

            return addOn;
        }

        /// <summary>
        /// Add a new AddOn
        /// </summary>
        /// <param name="addOn">AddOn object to insert.</param>
        /// <param name="productAddOnId">Product AddOn Id to set the AddOnId out param.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool CreateNewProductAddOn(AddOn addOn, out int productAddOnId)
        {
            ZNode.Libraries.DataAccess.Service.AddOnService productAddOnService = new ZNode.Libraries.DataAccess.Service.AddOnService();
            bool isAdded = productAddOnService.Insert(addOn);
            productAddOnId = addOn.AddOnID;

            return isAdded;
        }

        /// <summary>
        /// Update a AddOn 
        /// </summary>
        /// <param name="addOn">AddOn object to update.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool UpdateNewProductAddOn(AddOn addOn)
        {
            ZNode.Libraries.DataAccess.Service.AddOnService productAddOnService = new ZNode.Libraries.DataAccess.Service.AddOnService();
            bool isUpdated = productAddOnService.Update(addOn);

            return isUpdated;
        }

        /// <summary>
        /// Delete a AddOn By AddOn Id.
        /// </summary>
        /// <param name="addOnId">AddOn Id to delete the AddOn object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteAddOn(int addOnId)
        {
            ZNode.Libraries.DataAccess.Service.AddOnService productAddOnService = new ZNode.Libraries.DataAccess.Service.AddOnService();
            bool isDeleted = productAddOnService.Delete(addOnId);

            return isDeleted;
        }

        /// <summary>
        /// Delete a product AddOn (options)
        /// </summary>
        /// <param name="addOn">AddOn object to delete from data source.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteAddOn(AddOn addOn)
        {
            ZNode.Libraries.DataAccess.Service.AddOnService productAddOnService = new ZNode.Libraries.DataAccess.Service.AddOnService();
            bool isDeleted = productAddOnService.Delete(addOn);

            return isDeleted;
        }
        #endregion

        #region Related to ZNodeAddonValue table
        /// <summary>
        /// Get deep loaded AddOnValue object. 
        /// </summary>
        /// <param name="addOnValueId">AddOnValue Id to load AddOnValue.</param>
        /// <returns>Returns the deep loaded AddOnValue object.</returns>
        public AddOnValue DeepLoadByAddOnValueID(int addOnValueId)
        {
            AddOnValueService addOnValueService = new AddOnValueService();
            return addOnValueService.DeepLoadByAddOnValueID(addOnValueId, true, DeepLoadType.IncludeChildren, typeof(AddOn));
        }

        /// <summary>
        /// Returns all AddOn values for the AddOnId
        /// </summary>
        /// <param name="addOnId">AddOn Id to get the AddOn value list.</param>
        /// <returns>Returns list of AddOnValue object.</returns>
        public TList<AddOnValue> GetAddOnValuesByAddOnId(int addOnId)
        {
            AddOnValueService addOnValueService = new AddOnValueService();
            TList<AddOnValue> addonValueList = addOnValueService.GetByAddOnID(addOnId);

            return addonValueList;
        }

        /// <summary>
        /// Get the AddOnValue object by AddOnValue Id.
        /// </summary>
        /// <param name="addOnValueId">AddOnValue Id to get the AddOnValue object.</param>
        /// <returns>Returns the AddOnValue object.</returns>
        public AddOnValue GetByAddOnValueID(int addOnValueId)
        {
            AddOnValueService addOnValueService = new AddOnValueService();
            AddOnValue addOnValue = addOnValueService.GetByAddOnValueID(addOnValueId);

            return addOnValue;
        }

        /// <summary>
        /// Get the AddOnValue list by comma seperated portal Id.
        /// </summary>
        /// <param name="portalIds">Comma seperated portal Id.</param>
        /// <returns>Returns list of AddOnValue object.</returns>
        public TList<AddOnValue> GetAddOnValueByPortalID(string portalIds)
        {
            AddOnValueService addOnValueService = new AddOnValueService();
            AddOnValueQuery query = new AddOnValueQuery();
            query.AppendInQuery(AddOnValueColumn.AddOnID, string.Format("SELECT AddOnID FROM ZNodeAddOn WHERE PortalID IN ({0})", portalIds));
            TList<AddOnValue> addOnValueList = addOnValueService.Find(query.GetParameters());

            return addOnValueList;
        }

        /// <summary>
        /// Add New Addon Value
        /// </summary>
        /// <param name="addOnValue">AddOnValue object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddNewAddOnValue(AddOnValue addOnValue)
        {
            AddOnValueService addOnValueService = new AddOnValueService();
            bool isAdded = addOnValueService.Insert(addOnValue);

            if (addOnValue.DefaultInd)
            {
                this.ResetDefault(addOnValue.AddOnID, addOnValue.AddOnValueID);
            }

            return isAdded;
        }

        /// <summary>
        /// Update AddOn Value
        /// </summary>
        /// <param name="addOnValue">AddOnValue object to update.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool UpdateAddOnValue(AddOnValue addOnValue)
        {
            AddOnValueService addOnValueService = new AddOnValueService();
            bool isUpdated = addOnValueService.Update(addOnValue);

            if (addOnValue.DefaultInd && isUpdated)
            {
                this.ResetDefault(addOnValue.AddOnID, addOnValue.AddOnValueID);
            }

            return isUpdated;
        }

        /// <summary>
        /// Update AddOn Value
        /// </summary>
        /// <param name="addOnValue">AddOnValue object to delete.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteAddOnValue(int addOnValue)
        {
            AddOnValueService addOnValueService = new AddOnValueService();
            bool isDeleted = addOnValueService.Delete(addOnValue);

            return isDeleted;
        }
        #endregion

        #region AddOnValue Locale    

        /// <summary>
        /// Get all the LocaleId associated with Highlights.
        /// </summary>        
        /// <returns>Returns list of locales.</returns>
        public TList<Locale> GetAllLocaleId()
        {
            // Get the Locale information with product id.
            LocaleService localeService = new LocaleService();
            LocaleQuery localeQuery = new LocaleQuery();
            localeQuery.AppendInQuery("LocaleId", "SELECT DISTINCT LOCALEID FROM ZnodeAddOn");

            TList<Locale> localeList = localeService.Find(localeQuery.GetParameters());

            if (localeList.Count > 0)
            {
                localeList.Sort("LocaleDescription");
            }

            return localeList;
        }
        #endregion

        /// <summary>
        /// Reset all other values in this option when making this default.
        /// </summary>
        /// <param name="addOnId">AddOn Id to get the AddOnValue list.</param>
        /// <param name="addOnValueId">AddOnValue Id to set the default Ind.</param>        
        private void ResetDefault(int addOnId, int addOnValueId)
        {
            AddOnValueService addOnValueService = new AddOnValueService();
            TList<AddOnValue> addOnValueList = addOnValueService.GetByAddOnID(addOnId);

            foreach (AddOnValue addOnValue in addOnValueList)
            {
                if (addOnValueId != addOnValue.AddOnValueID)
                {
                    // Reset Default Value
                    addOnValue.DefaultInd = false;
                }
            }

            addOnValueService.Update(addOnValueList);
        }
    }
}
