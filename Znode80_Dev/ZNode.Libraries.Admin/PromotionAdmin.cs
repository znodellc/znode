using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage discount coupons 
    /// </summary>
    public class PromotionAdmin : ZNodeBusinessBase
    {
        #region Public methods - Related to Discount Type

        /// <summary>
        /// Get all discount types.
        /// </summary>
        /// <returns>Returns list of DiscountType object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.DiscountType> GetAllDiscountTypes()
        {
            ZNode.Libraries.DataAccess.Service.DiscountTypeService discountTypeService = new DiscountTypeService();
            TList<ZNode.Libraries.DataAccess.Entities.DiscountType> discountTypeList = discountTypeService.GetAll();

            return discountTypeList;
        }

        /// <summary>
        /// Get the discount type object by discount type Id.
        /// </summary>
        /// <param name="discountTypeId">Discount type Id to get the discoun type object.</param>
        /// <returns>Returns the DiscountType object</returns>
        public ZNode.Libraries.DataAccess.Entities.DiscountType GetByDiscountTypeID(int discountTypeId)
        {
            ZNode.Libraries.DataAccess.Service.DiscountTypeService discountTypeService = new DiscountTypeService();
            ZNode.Libraries.DataAccess.Entities.DiscountType discountType = discountTypeService.GetByDiscountTypeID(discountTypeId);

            return discountType;
        }

        /// <summary>
        /// Get the discount type Id by class name.
        /// </summary>
        /// <param name="className">Class name to get the discount type.</param>
        /// <returns>Returns the top 1 discount type id. </returns>
        public int GetDiscountTypeId(string className)
        {
            TList<DiscountType> discountTypeList = DataRepository.DiscountTypeProvider.Find("ClassName = '" + className + "'");

            if (discountTypeList.Count > 0)
            {
                discountTypeList.Sort("DiscountTypeID Desc");

                return discountTypeList[0].DiscountTypeID;
            }

            return 0;
        }
        #endregion

        #region Public Methods - Related to Promotions
        /// <summary>
        /// Get all promotions list.
        /// </summary>
        /// <returns>Returns list of Promotion object.</returns>
        public TList<Promotion> GetAllPromotions()
        {
            PromotionService promotionService = new PromotionService();
            TList<Promotion> promotionList = promotionService.GetAll();

            return promotionList;
        }

        /// <summary>
        /// Get all promotion dataset.
        /// </summary>
        /// <returns>Returns the promotion dataset.</returns>
        public DataSet GetPromotions()
        {
            PromotionHelper promoHelper = new PromotionHelper();
            return promoHelper.GetAllPromotions();
        }

        /// <summary>
        /// Get all active promotions.
        /// </summary>
        /// <returns>Returns list of active Promotion object.</returns>
        public TList<Promotion> GetActivePromotions()
        {
            PromotionService promotionService = new PromotionService();
            TList<Promotion> promotionList = promotionService.GetAll();
            promotionList.ApplyFilter(delegate(Promotion promo)
            {
                return System.DateTime.Today.Date >= promo.StartDate.Date && System.DateTime.Today.Date <= promo.EndDate.Date;
            });

            return promotionList;
        }

        /// <summary>
        /// Get the promotion by promotion Id
        /// </summary>
        /// <param name="promotionId">Promotion Id to get the promotion object.</param>
        /// <returns>Returns the Promotion object.</returns>
        public Promotion GetByPromotionId(int promotionId)
        {
            PromotionService promotionService = new PromotionService();
            Promotion promotion = promotionService.GetByPromotionID(promotionId);

            return promotion;
        }

        /// <summary>
        /// Returns the deep loaded promotion by promotion Id
        /// </summary>
        /// <param name="promotionId">Promotion Id to get the deep loaded promotion object.</param>
        /// <returns>Returns the list of deep loaded Promotion object.</returns>
        public Promotion DeepLoadByPromotionId(int promotionId)
        {
            PromotionService promotionService = new PromotionService();
            Promotion promotion = promotionService.DeepLoadByPromotionID(promotionId, true, DeepLoadType.IncludeChildren, typeof(DiscountType), typeof(Product));

            return promotion;
        }

        /// <summary>
        /// Add new promotion.
        /// </summary>
        /// <param name="promotion">Promotion object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddPromotion(Promotion promotion)
        {
            PromotionService promotionService = new PromotionService();
            return promotionService.Insert(promotion);
        }

        /// <summary>
        /// Update the promotion.
        /// </summary>
        /// <param name="promotion">Promotion object to insert.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool UpdatePromotion(Promotion promotion)
        {
            PromotionService promotionService = new PromotionService();
            return promotionService.Update(promotion);
        }

        /// <summary>
        /// Delete the promotion.
        /// </summary>
        /// <param name="promotionId">Promotion Id to delete the promotion object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeletePromotion(int promotionId)
        {
            PromotionService promotionService = new PromotionService();
            return promotionService.Delete(promotionId);
        }

        /// <summary>
        /// Check whether the same type of promotion already defined the same date range.
        /// </summary>
        /// <param name="promotion">Promotion object to check against existing promotions.</param>        
        /// <returns>Returns true if promotion type overlapped otherwise false.</returns>
        public bool IsPromotionOverlapped(Promotion currentPromotion)
        {
            PromotionService promotionService = new PromotionService();
            TList<Promotion> promotionList = promotionService.GetAll();

            // Prmotion object Start and EndDate already converted into UTC format.
            promotionList.ApplyFilter(delegate(Promotion existingPromotion)
            {
                // If promotion type is Percent off X if Y Purchased OR Amount off X if Y Purchased then validate the product Id and promotional product Id.
                // If promotion type is Amount off Product then validate the product Id only.
                // For all other promotion type validate only date overlapping.                

                return ((currentPromotion.StartDate == existingPromotion.StartDate) ||
                    (currentPromotion.StartDate > existingPromotion.StartDate ? currentPromotion.StartDate <= existingPromotion.EndDate : existingPromotion.StartDate <= currentPromotion.EndDate)) &&
                    existingPromotion.DiscountTypeID == currentPromotion.DiscountTypeID &&
                    existingPromotion.ProfileID == currentPromotion.ProfileID &&
                    existingPromotion.Name == currentPromotion.Name &&
                    existingPromotion.OrderMinimum == currentPromotion.OrderMinimum &&
                    existingPromotion.Discount == currentPromotion.Discount &&
                    ((currentPromotion.DiscountTypeID == 4 || currentPromotion.DiscountTypeID == 8) ? (existingPromotion.ProductID == currentPromotion.ProductID && existingPromotion.PromotionProductID == currentPromotion.PromotionProductID &&  existingPromotion.QuantityMinimum == currentPromotion.QuantityMinimum) : true) &&
                    (currentPromotion.DiscountTypeID == 6 ? existingPromotion.ProductID == currentPromotion.ProductID : true) &&
                    (currentPromotion.PromotionID > 0 ? existingPromotion.PromotionID != currentPromotion.PromotionID : currentPromotion.PromotionID == 0);
            });

            if (promotionList.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}
