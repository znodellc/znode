using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage customer reviews
    /// </summary>
    public class ReviewAdmin
    {
        /// <summary>
        /// Get all the customer reviews. 
        /// </summary>
        /// <returns>Returns list of Review object.</returns>
        public TList<Review> GetAll()
        {
            ReviewService reviewService = new ReviewService();
            TList<Review> reviewList = reviewService.GetAll();

            return reviewList;
        }

        /// <summary>
        /// Search the customer review based on the search criteria.
        /// </summary>
        /// <param name="reviewTitle">Review title text.</param>
        /// <param name="nickName">Customer name</param>
        /// <param name="productName">Product name.</param>
        /// <param name="status">Status of the customer review.</param>
        /// <returns>Returns the customer review dataset.</returns>
        public DataSet SearchReview(string reviewTitle, string nickName, string productName, string status)
        {
            ReviewHelper reviewHelper = new ReviewHelper();
            return reviewHelper.SearchReview(reviewTitle, nickName, productName, status);
        }

        /// <summary>
        /// Get the review by review Id
        /// </summary>
        /// <param name="reviewId">Review Id to get the review object.</param>
        /// <returns>Returns the Review object.</returns>
        public Review GetByReviewID(int reviewId)
        {
            ReviewService reviewService = new ReviewService();
            Review review = reviewService.GetByReviewID(reviewId);

            return review;
        }

        /// <summary>
        /// Get the deep loaded review object by review Id.
        /// </summary>
        /// <param name="reviewId">Review Id to get the deep loaded review object.</param>
        /// <returns>Returns the deep loaded Review object.</returns>
        public Review DeepLoadByReviewID(int reviewId)
        {
            ReviewService reviewService = new ReviewService();            
            return reviewService.DeepLoadByReviewID(reviewId, true, ZNode.Libraries.DataAccess.Data.DeepLoadType.IncludeChildren, typeof(Product));            
        }

        /// <summary>
        /// Get all the customer reviews by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the list of review object.</param>
        /// <returns>Returns the list of Review object.</returns>
        public TList<Review> GetByProductID(int productId)
        {
            ReviewService reviewService = new ReviewService();
            TList<Review> reviewList = reviewService.GetByProductID(productId);

            return reviewList;
        }

        /// <summary>
        /// Update a review. 
        /// </summary>
        /// <param name="review">Review object to update.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool Update(Review review)
        {
            ReviewService reviewService = new ReviewService();
            return reviewService.Update(review);
        }

        /// <summary>
        /// Delete a review by review Id. 
        /// </summary>
        /// <param name="reviewId">Review Id to get the review object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool Delete(int reviewId)
        {
            ReviewService reviewService = new ReviewService();
            return reviewService.Delete(reviewId);
        }
    }
}
