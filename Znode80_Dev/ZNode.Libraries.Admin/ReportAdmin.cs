﻿using System;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Manage the multifront reports.
    /// </summary>
    public class ReportAdmin
    {
        /// <summary>
        /// Get the dataset based on input values.
        /// </summary>
        /// <param name="filter">ZNodeReport enumeraion object</param>
        /// <param name="fromDate">Get report data from this date.</param>
        /// <param name="toDate">Get report data to this date.</param>
        /// <returns>Returns the result dataset.</returns>
        public DataSet ReportList(ZnodeReport filter, DateTime fromDate, DateTime toDate)
        {
            ReportHelper reportHelper = new ReportHelper();
            return reportHelper.ReportList(filter, fromDate, toDate, string.Empty, string.Empty, string.Empty);
        }

        /// <summary>
        /// Get the dataset based on input values.
        /// </summary>
        /// <param name="filter">ZNodeReport enumeraion object</param>
        /// <param name="fromDate">Get report data from this date.</param>
        /// <param name="toDate">Get report data to this date.</param>
        /// <param name="supplierId">supplier Id. </param>
        /// <param name="custom1">Custom Field 1</param>
        /// <param name="custom2">Custom Field 2</param>
        /// <returns>Returns the result dataset.</returns>
        public DataSet ReportList(ZnodeReport filter, DateTime fromDate, DateTime toDate, string supplierId, string custom1, string custom2)
        {
            ReportHelper reportHelper = new ReportHelper();
            return reportHelper.ReportList(filter, fromDate, toDate, supplierId, custom1, custom2);
        }
    }
}
