using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage product suppliers
    /// </summary>
    public class SupplierAdmin : ZNodeBusinessBase
    {

        #region Znode Version 7.7.2 Properties

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int AccountId { get; set; }
        public string VendorEmail { get; set; }
        #endregion


        #region Public Methods

        /// <summary>
        /// Get all suppliers.
        /// </summary>        
        /// <returns>Returns the list of Supplier object.</returns>
        public TList<Supplier> GetAll()
        {
            ZNode.Libraries.DataAccess.Service.SupplierService supplierService = new SupplierService();
            TList<ZNode.Libraries.DataAccess.Entities.Supplier> supplierList = supplierService.GetAll();

            return supplierList;
        }

        /// <summary>
        /// Get the supplier by supplier Id.
        /// </summary>
        /// <param name="supplierId">Supplier Id to get the supplier object.</param>
        /// <returns>Returns the Supplier object.</returns>
        public ZNode.Libraries.DataAccess.Entities.Supplier GetBySupplierID(int supplierId)
        {
            ZNode.Libraries.DataAccess.Service.SupplierService supplierService = new SupplierService();
            ZNode.Libraries.DataAccess.Entities.Supplier supplier = supplierService.GetBySupplierID(supplierId);

            return supplier;
        }

        /// <summary>
        /// Get the supplier type object by supplier type Id.
        /// </summary>
        /// <param name="supplierTypeId">Supplier type Id to get the supplier type object.</param>
        /// <returns>Returns the SupplierType object</returns>
        public ZNode.Libraries.DataAccess.Entities.SupplierType GetBySupplierTypeID(int supplierTypeId)
        {
            ZNode.Libraries.DataAccess.Service.SupplierTypeService supplierTypeService = new SupplierTypeService();
            ZNode.Libraries.DataAccess.Entities.SupplierType supplierType = supplierTypeService.GetBySupplierTypeID(supplierTypeId);

            return supplierType;
        }

        /// <summary>
        /// Insert a new supplier.
        /// </summary>
        /// <param name="supplier">Supplier object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Insert(ZNode.Libraries.DataAccess.Entities.Supplier supplier)
        {
            ZNode.Libraries.DataAccess.Service.SupplierService supplierService = new SupplierService();
            bool isAdded = supplierService.Insert(supplier);

            return isAdded;
        }

        /// <summary>
        /// update the Supplier
        /// </summary>
        /// <param name="supplier">Supplier object to update.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool Update(ZNode.Libraries.DataAccess.Entities.Supplier supplier)
        {
            ZNode.Libraries.DataAccess.Service.SupplierService supplierService = new SupplierService();
            bool isUpdated = supplierService.Update(supplier);

            return isUpdated;
        }

        /// <summary>
        /// Delete the supplier by Id.
        /// </summary>
        /// <param name="supplierId">Supplier Id to delete the supplier object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool Delete(int supplierId)
        {
            ZNode.Libraries.DataAccess.Service.SupplierService supplierService = new SupplierService();
            bool isDeleted = supplierService.Delete(supplierId);

            return isDeleted;
        }

        /// <summary>
        /// Search the supplier by input values.
        /// </summary>
        /// <param name="name">Supplier name to search.</param>        
        /// <param name="status">Supplier status to search.</param>
        /// <returns>Returns the supplier search result dataset.</returns>
        public DataSet SearchSupplier(string name, string status)
        {
            SupplierHelper supplierHelper = new SupplierHelper();
            DataSet searchResultDataSet = supplierHelper.SearchSupplier(name, status);

            return searchResultDataSet;
        }

        /// <summary>
        /// Get all suppliers type.
        /// </summary>        
        /// <returns>Returns list of SupplierType object.</returns>
        public TList<SupplierType> GetSupplierTypes()
        {
            SupplierTypeService service = new SupplierTypeService();
            return service.GetAll();
        }

        /// <summary>
        /// Get the supplier type Id by class name.
        /// </summary>
        /// <param name="className">Class name to get the supplier type.</param>
        /// <returns>Returns the top 1 supplier type id. </returns>
        public int GetSupplierTypeId(string className)
        {
            TList<SupplierType> supplierTypeList = DataRepository.SupplierTypeProvider.Find("ClassName = '" + className + "'");

            if (supplierTypeList.Count > 0)
            {
                supplierTypeList.Sort("SupplierTypeID Desc");

                return supplierTypeList[0].SupplierTypeID;
            }

            return 0;
        }

        /// <summary>
        /// Returns the deep loaded supplier by supplier Id
        /// </summary>
        /// <param name="supplierId">Supplier Id to get the deep loaded supplier object.</param>
        /// <returns>Returns the list of deep loaded supplier object.</returns>
        public Supplier DeepLoadBySupplierId(int supplierId)
        {
            SupplierService supplierService = new SupplierService();
            Supplier supplier = supplierService.DeepLoadBySupplierID(supplierId, true, DeepLoadType.IncludeChildren, typeof(SupplierType));

            return supplier;
        }
        #endregion
    }
}

