using System;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using System.Linq;
namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// DataManager manages upload / download  methods
    /// </summary>
    public class DataManagerAdmin
    {
        #region Download Methods
        /// <summary>
        /// Returns a list of SKUs, each with it's associated Quantity on Hand & reorder level.
        /// </summary>        
        /// <param name="productType">0 - All, 1 - Products Only, 2 - Attributes Only, 3 - AddOnValues Only</param>
        /// <returns>Dataset contains list of skus</returns>
        public DataSet GetProductQuantities(int productType)
        {
            ProductHelper produtHelper = new ProductHelper();
            return produtHelper.GetSkuInventoryListByFilter(productType);
        }

        /// <summary>
        /// Returns a list of SKUs, each with it's associated Quantity on Hand & reorder level
        /// </summary>        
        /// <param name="productType">0 - All, 1 - Products Only, 2 - Attributes Only, 3 - AddOnValues Only</param>
        /// <param name="portalId">Portal Id to get the product quantities.</param>
        /// <returns>Dataset contains list of skus</returns>
        public DataSet GetProductQuantities(int productType, string portalId)
        {
            ProductHelper produtHelper = new ProductHelper();
            return produtHelper.GetSkuInventoryListByFilter(productType, portalId);
        }

        /// <summary>
        /// Returns a list of SKUs, each with it's associated retai price, sal price & wholesale pirce
        /// </summary>        
        /// <param name="productType">0 - All, 1 - Products Only, 2 - Attributes Only, 3 - AddOnValues Only</param>
        /// <returns>Returns dataset which contains list of skus</returns>
        public DataSet GetProductPrices(int productType)
        {
            ProductHelper produtHelper = new ProductHelper();
            return produtHelper.GetSkuPricingListByFilter(productType);
        }

        /// <summary>
        /// Returns a list of SKUs, each with it's associated retai price, sal price & wholesale pirce
        /// </summary>        
        /// <param name="productType">0 - All, 1 - Products Only, 2 - Attributes Only, 3 - AddOnValues Only</param>
        /// <param name="portalId">Portal Id to get the product quantities.</param>
        /// <returns>Returns dataset which contains list of skus</returns>
        public DataSet GetProductPrices(int productType, string portalId)
        {
            ProductHelper produtHelper = new ProductHelper();
            return produtHelper.GetSkuPricingListByFilter(productType, portalId);
        }

        /// <summary>
        /// Returns a list of SKU Inventory
        /// </summary>               
        /// <returns>Returns dataset which contain all sku inventory details.</returns>
        public DataSet GetAllSkuInventory()
        {
            SKUHelper skuHelper = new SKUHelper();
            return skuHelper.GetAllSKUInventory();
        }

        /// <summary>
        /// Returns a list of Products
        /// </summary>
        /// <param name="portalId">Portal Id to get the product details.</param>
        /// <returns>Dataset contains list of Products</returns>
        public DataSet GetProductListByPortalId(int portalId)
        {
            ProductHelper produtHelper = new ProductHelper();
            return produtHelper.GetProductListByPortalId(portalId);
        }

        /// <summary>
        /// Returns a list of Products
        /// </summary>
        /// <param name="portalIds">Comma seperated portal Ids to get all products.</param>
        /// <returns>Dataset contains list of Products</returns>
        public TList<Product> GetProductsByPortalId(string portalIds)
        {
            ProductService productService = new ProductService();
            TList<Product> productList = new TList<Product>();

            foreach (string sportalId in portalIds.Split(','))
            {
                int portalId = 0;
                int.TryParse(sportalId, out portalId);
                if (portalId > 0)
                {
                    productList.AddRange(productService.GetByPortalID(portalId));
                }
            }

            return productList;
        }

        /// <summary>
        /// Get the tracking data.
        /// </summary>
        /// <param name="startDate">Get tracking from this date.</param>
        /// <param name="endDate">Get tracking to this date.</param>
        /// <returns>Returns tracking dataset.</returns>
        public DataSet GetDownloadTrackingData(string startDate, string endDate)
        {
            DataManagerHelper dataHelper = new DataManagerHelper();
            return dataHelper.GetDownloadTrackingData(startDate, endDate);
        }

        /// <summary>
        /// Get the product facets.
        /// </summary>
        /// <param name="categoryId">Category Id to get the facets.</param>
        /// <param name="catalogId">Catalog Id to get the facets.</param>
        /// <returns>Returns the facets dataset.</returns>
        public DataSet GetDownloadFacets(int categoryId, int catalogId)
        {
            DataManagerHelper dataHelper = new DataManagerHelper();

            return dataHelper.GetDownloadFacets(categoryId, catalogId);
        }

        /// <summary>
        /// Returns a list of SKU Inventory
        /// </summary>               
        /// <param name="portalId">Portal Id to get the SKU inventory dataset.</param>
        /// <returns>Returns the SKU inventory dataset.</returns>
        public DataSet GetSkuInventoryByPortalID(string portalId)
        {
            SKUHelper skuHelper = new SKUHelper();
            return skuHelper.GetSKUInventoryByPortalID(portalId);
        }
     
        #endregion

        #region Upload Methods

        /// <summary>
        /// Update the products
        /// </summary>
        /// <param name="dataTable">Datatable which contains product details.</param>
        /// <param name="accountId">Account Id</param>
        /// <param name="sqlErrorCount">Sql error count.</param>
        /// <returns>Returns true if uploaded else false.</returns>
        public bool UploadProducts(ZNodeProductDataset.ZNodeProductDataTable dataTable, int accountId, out int sqlErrorCount)
        {
            DataManagerHelper helper = new DataManagerHelper();
            return helper.UploadProducts(dataTable, accountId, out sqlErrorCount);
        }

        /// <summary>
        /// Upload the Products
        /// </summary>
        /// <param name="dataTable">Datatable which contains product details.</param>
        /// <param name="sqlErrorCount">Sql error count.</param>
        /// <returns>Returns true if uploaded else false.</returns>
        public bool UploadProducts(ZNodeProductDataset.ZNodeProductDataTable dataTable, out int sqlErrorCount)
        {
            DataManagerHelper helper = new DataManagerHelper();
            return helper.UploadProducts(dataTable, out sqlErrorCount);
        }

        /// <summary>
        /// Upload the Inventory
        /// </summary>
        /// <param name="dataTable">Datatable which contains inventory details.</param>
        /// <param name="sqlErrorCount">Sql error count.</param>
        /// <returns>Returns true if uploaded else false.</returns>
        public bool UploadInventory(ZNodeSKUInventoryDataset.ZNodeSKUInventoryDataTable dataTable, out int sqlErrorCount)
        {
            DataManagerHelper helper = new DataManagerHelper();

            return helper.UploadInventory(dataTable, out sqlErrorCount);
        }

        /// <summary>
        /// Upload the facets.
        /// </summary>
        /// <param name="dataTable">Datatable which contains facet details.</param>
        /// <param name="sqlErrorCount">Sql error count.</param>
        /// <returns>Returns true if uploaded else false.</returns>
        public bool UploadFacets(ZNodeFacetsDataSet.ZNodeFacetDataTable dataTable, out int sqlErrorCount)
        {
            DataManagerHelper helper = new DataManagerHelper();

            return helper.UploadFacets(dataTable, out sqlErrorCount);
        }

        /// <summary>
        /// Upload the SKUs
        /// </summary>
        /// <param name="dataTable">Datatable which contains sku details.</param>
        /// <param name="sqlErrorCount">Sql error count.</param>
        /// <returns>Returns true if uploaded else false.</returns>
        public bool UploadSKU(ZNodeSKUDataSet.ZNodeSKUDataTable dataTable, out int sqlErrorCount)
        {
            DataManagerHelper helper = new DataManagerHelper();

            return helper.UpdateSKU(dataTable, out sqlErrorCount);
        }

        /// <summary>
        /// Upload Attribute
        /// </summary>
        /// <param name="dataTable">Datatable which contains attribute details.</param>
        /// <param name="sqlErrorCount">Sql error count.</param>
        /// <returns>Returns true if uploaded else false.</returns>
        public bool UploadAttribute(ZNodeAttributeDataSet.ZNodeProductAttributeDataTable dataTable, out int sqlErrorCount)
        {
            DataManagerHelper helper = new DataManagerHelper();
            return helper.UpdateAttribute(dataTable, out sqlErrorCount);
        }

        /// <summary>
        /// Upload the Pricing
        /// </summary>
        /// <param name="dataTable">Datatable which contains pricing details.</param>
        /// <param name="sqlErrorCount">Sql error count.</param>
        /// <returns>Returns true if uploaded else false.</returns>
        public bool UploadPricing(ZNodePricingDataSet.ZNodeProductDataTable dataTable, out int sqlErrorCount)
        {
            DataManagerHelper helper = new DataManagerHelper();
            return helper.UpdatePricing(dataTable, out sqlErrorCount);
        }

        /// <summary>
        /// Upload Shipping Status
        /// </summary>
        /// <param name="dataTable">Datatable which contains shipping details.</param>
        /// <param name="sqlErrorCount">Sql error count.</param>
        /// <returns>Returns true if uploaded else false.</returns>
        public bool UploadShippingstatus(ZNodeShippingStatusDataSet.ZNodeShippingStatusDataTable dataTable, out int sqlErrorCount)
        {
            DataManagerHelper helper = new DataManagerHelper();
            return helper.UpdateShippingStatus(dataTable, out sqlErrorCount);
        }

        /// <summary>
        /// Upload the ZipCode
        /// </summary>
        /// <param name="dataTable">Datatable which contains zipcode details.</param>
        /// <param name="sqlErrorCount">Sql error count.</param>
        /// <returns>Returns true if uploaded else false.</returns>
        public bool UploadZipCode(ZNodeZipCodeDataSet.ZNodeZipCodeDataTable dataTable, out int sqlErrorCount)
        {
            DataManagerHelper helper = new DataManagerHelper();
            return helper.UpdateZipCode(dataTable, out sqlErrorCount);
        }

        /// <summary>
        /// Upload the Customer Based Pricing
        /// </summary>
        /// <param name="dataTable">Datatable which contains Customer pricing details.</param>
        /// <param name="sqlErrorCount">Sql error count.</param>
        /// <returns>Returns true if uploaded else false.</returns>
        public bool UploadCustomerPricing(ZNodeCustomerBasedPricingDataSet.ZNodeCustomerPricingDataTable dataTable,
                                          out int sqlErrorCount)
        {
            var helper = new DataManagerHelper();
            return helper.UploadCustomerPricing(dataTable, out sqlErrorCount);
        }
        
        /// <summary>
        /// Get all Customer pricing data
        /// </summary>
        /// <returns></returns>
        public DataSet GetAllCustomerPricing()
        {
            var service=new CustomerPricingService();
            return service.GetAll().ToDataSet(false);
        }

        #endregion
    }
}

