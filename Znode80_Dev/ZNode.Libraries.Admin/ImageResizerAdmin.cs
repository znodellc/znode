using System;
using System.Web;
using System.Collections.Generic;
using System.Text;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Custom;
using System.IO;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// 
    /// </summary>
    public class ImageResizerAdmin : ZNodeBusinessBase
    {
        # region Protected Member Variables
        protected string datapath = ZNodeConfigManager.EnvironmentConfig.DataPath;
        protected string contentpath = ZNodeConfigManager.EnvironmentConfig.ContentPath;
        protected string MaxCatalogItemLargeWidthPath = string.Empty;
        protected string MaxCatalogItemThumbnailWidthPath = string.Empty;
        protected string MaxCatalogItemMediumWidthPath = string.Empty;
        protected string MaxCatalogItemSmallWidthPath = string.Empty;
        protected string MaxCatalogItemSmallThumbnailWidthPath = string.Empty;
        protected string MaxCatalogItemCrossSellWidthPath = string.Empty;
        protected string MaxLogoWidthPath = string.Empty;
        ImageHelper imageHelper = new ImageHelper();
        #endregion

        # region Public Methods
        /// <summary>
        /// Resize Images for all Portals
        /// </summary>
        public void ResizeAllImages()
        {
            // Get By Portal
            PortalService portalService = new PortalService();
            TList<Portal> portals = portalService.GetAll();

            System.IO.DirectoryInfo path = new System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.OriginalImagePath));
            string[] patterns = { "*.jpg", "*.png", "*.gif", "*.jpeg" };



            foreach (Portal portal in portals)
            {
                MaxCatalogItemLargeWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemLargeWidth.ToString());
                MaxCatalogItemThumbnailWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemThumbnailWidth.ToString());
                MaxCatalogItemMediumWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemMediumWidth.ToString());
                MaxCatalogItemSmallWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemSmallWidth.ToString());
                MaxCatalogItemSmallThumbnailWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemSmallThumbnailWidth.ToString());
                MaxCatalogItemCrossSellWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemCrossSellWidth.ToString());

                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemLargeWidthPath));
                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemThumbnailWidthPath));
                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemMediumWidthPath));
                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemSmallWidthPath));
                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemSmallThumbnailWidthPath));
                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemCrossSellWidthPath));

                foreach (string pattern in patterns)
                {
                    System.IO.FileInfo[] file = path.GetFiles(pattern);

                    foreach (System.IO.FileInfo fileInfo in file)
                    {
                        imageHelper.ResizeImage(fileInfo, portal.MaxCatalogItemLargeWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemLargeWidthPath, "/")));
                        imageHelper.ResizeImage(fileInfo, portal.MaxCatalogItemThumbnailWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemThumbnailWidthPath, "/")));
                        imageHelper.ResizeImage(fileInfo, portal.MaxCatalogItemMediumWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemMediumWidthPath, "/")));
                        imageHelper.ResizeImage(fileInfo, portal.MaxCatalogItemSmallWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemSmallWidthPath, "/")));
                        imageHelper.ResizeImage(fileInfo, portal.MaxCatalogItemSmallThumbnailWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemSmallThumbnailWidthPath, "/")));
                        imageHelper.ResizeImage(fileInfo, portal.MaxCatalogItemCrossSellWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemCrossSellWidthPath, "/")));

                        // For Thumbnail Images
                        int maxThumbnailWidthHeight = portal.MaxCatalogItemThumbnailWidth;
                        imageHelper.CropImage(fileInfo, maxThumbnailWidthHeight, maxThumbnailWidthHeight, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemThumbnailWidthPath, "/")));

                        // For Swatches
                        int maxSmallThumnailWidthHeight = portal.MaxCatalogItemSmallThumbnailWidth;
                        imageHelper.CropImage(fileInfo, maxSmallThumnailWidthHeight, maxSmallThumnailWidthHeight, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemThumbnailWidthPath, "/")));
                    }
                }
            }
        }

        /// <summary>
        /// Resize Images for Specific portals
        /// </summary>
        /// <param name="portalID"></param>
        public void ResizeAllImagesByPortal(int portalID)
        {
            // Get By Portal
            PortalService portalService = new PortalService();
            Portal portal = portalService.GetByPortalID(portalID);

            System.IO.DirectoryInfo path = new System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.OriginalImagePath));
            string[] patterns = { "*.jpg", "*.png", "*.gif", "*.jpeg" };

            MaxCatalogItemLargeWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemLargeWidth.ToString());
            MaxCatalogItemThumbnailWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemThumbnailWidth.ToString());
            MaxCatalogItemMediumWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemMediumWidth.ToString());
            MaxCatalogItemSmallWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemSmallWidth.ToString());
            MaxCatalogItemSmallThumbnailWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemSmallThumbnailWidth.ToString());
            MaxCatalogItemCrossSellWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemCrossSellWidth.ToString());

            CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemLargeWidthPath));
            CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemThumbnailWidthPath));
            CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemMediumWidthPath));
            CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemSmallWidthPath));
            CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemSmallThumbnailWidthPath));
            CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemCrossSellWidthPath));

            foreach (string pattern in patterns)
            {
                System.IO.FileInfo[] file = path.GetFiles(pattern);

                foreach (System.IO.FileInfo fileInfo in file)
                {
                    imageHelper.ResizeImage(fileInfo, portal.MaxCatalogItemLargeWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemLargeWidthPath, "/")));
                    imageHelper.ResizeImage(fileInfo, portal.MaxCatalogItemThumbnailWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemThumbnailWidthPath, "/")));
                    imageHelper.ResizeImage(fileInfo, portal.MaxCatalogItemMediumWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemMediumWidthPath, "/")));
                    imageHelper.ResizeImage(fileInfo, portal.MaxCatalogItemSmallWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemSmallWidthPath, "/")));
                    imageHelper.ResizeImage(fileInfo, portal.MaxCatalogItemSmallThumbnailWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemSmallThumbnailWidthPath, "/")));
                    imageHelper.ResizeImage(fileInfo, portal.MaxCatalogItemCrossSellWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemCrossSellWidthPath, "/")));

                    // For Thumbnail Images
                    int maxThumbnailWidthHeight = portal.MaxCatalogItemThumbnailWidth;
                    imageHelper.CropImage(fileInfo, maxThumbnailWidthHeight, maxThumbnailWidthHeight, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemThumbnailWidthPath, "/")));

                    // For Swatches
                    int maxSmallThumnailWidthHeight = portal.MaxCatalogItemSmallThumbnailWidth;
                    imageHelper.CropImage(fileInfo, maxSmallThumnailWidthHeight, maxSmallThumnailWidthHeight, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemThumbnailWidthPath, "/")));
                }
            }
        }


        /// <summary>
        /// Resize Images for all Portals
        /// <param name="FileInfo"></param>
        /// </summary>
        public void ResizeCatalogImage(FileInfo FileInfo)
        {
            // Get By Portal
            PortalService portalService = new PortalService();
            TList<Portal> portals = portalService.GetAll();

            foreach (Portal portal in portals)
            {
                MaxCatalogItemLargeWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemLargeWidth.ToString());
                MaxCatalogItemThumbnailWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemThumbnailWidth.ToString());
                MaxCatalogItemMediumWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemMediumWidth.ToString());
                MaxCatalogItemSmallWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemSmallWidth.ToString());
                MaxCatalogItemSmallThumbnailWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemSmallThumbnailWidth.ToString());
                MaxCatalogItemCrossSellWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemCrossSellWidth.ToString());

                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemLargeWidthPath));
                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemThumbnailWidthPath));
                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemMediumWidthPath));
                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemSmallWidthPath));
                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemSmallThumbnailWidthPath));
                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemCrossSellWidthPath));

                imageHelper.ResizeImage(FileInfo, portal.MaxCatalogItemLargeWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemLargeWidthPath, "/")));
                imageHelper.ResizeImage(FileInfo, portal.MaxCatalogItemThumbnailWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemThumbnailWidthPath, "/")));
                imageHelper.ResizeImage(FileInfo, portal.MaxCatalogItemMediumWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemMediumWidthPath, "/")));
                imageHelper.ResizeImage(FileInfo, portal.MaxCatalogItemSmallWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemSmallWidthPath, "/")));
                imageHelper.ResizeImage(FileInfo, portal.MaxCatalogItemSmallThumbnailWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemSmallThumbnailWidthPath, "/")));
                imageHelper.ResizeImage(FileInfo, portal.MaxCatalogItemCrossSellWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemCrossSellWidthPath, "/")));
            }
        }

        /// <summary>
        /// Resize the mobile splash images. Splash image width not hard-coded as 320 and 640.
        /// </summary>
        /// <param name="fileInfo">File Information</param>
        public void ResizeMobileSplashImage(FileInfo fileInfo)
        {
            // Get By Portal
            PortalService portalService = new PortalService();
            TList<Portal> portals = portalService.GetAll();

            foreach (Portal portal in portals)
            {
                string splashImageWidthSmallDir = String.Concat(datapath, "images/splash/320");
                string splashImageWidthLargeDir = String.Concat(datapath, "images/splash/640");

                this.CreateDirectory(HttpContext.Current.Server.MapPath(splashImageWidthSmallDir));
                this.CreateDirectory(HttpContext.Current.Server.MapPath(splashImageWidthLargeDir));


                imageHelper.ResizeImage(fileInfo, 320, HttpContext.Current.Server.MapPath(splashImageWidthSmallDir + "/"));
                imageHelper.ResizeImage(fileInfo, 640, HttpContext.Current.Server.MapPath(splashImageWidthLargeDir + "/"));
            }
        }


        /// <summary>
        /// Resize Image
        /// <param name="FileInfo"></param>
        /// </summary>
        public void ResizeImage(FileInfo FileInfo, ref string sNewFileName)
        {
            int iMaxLogoWidth = 0;
            int iMaxLogoHeight = 0;
            //Get the Width from config file.
            iMaxLogoWidth = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["LogoWidth"]);
            iMaxLogoHeight = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["LogoHeight"]);

            MaxLogoWidthPath = String.Concat(contentpath);

            sNewFileName = imageHelper.ResizeImage(FileInfo, iMaxLogoHeight, iMaxLogoWidth, HttpContext.Current.Server.MapPath(String.Concat(MaxLogoWidthPath, "/")));
        }

        /// <summary>
        /// Resize Images for all Portals
        /// <param name="FileInfo"></param>
        /// </summary>
        public void CropCatalogImage(FileInfo FileInfo)
        {
            // Get By Portal
            PortalService portalService = new PortalService();
            TList<Portal> portals = portalService.GetAll();

            foreach (Portal portal in portals)
            {
                MaxCatalogItemThumbnailWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemThumbnailWidth.ToString());
                MaxCatalogItemSmallThumbnailWidthPath = String.Concat(datapath, "images/catalog/", portal.MaxCatalogItemSmallThumbnailWidth.ToString());

                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemThumbnailWidthPath));
                CreateDirectory(HttpContext.Current.Server.MapPath(MaxCatalogItemSmallThumbnailWidthPath));

                // For Thumbnail Images
                int maxThumbnailWidthHeight = portal.MaxCatalogItemThumbnailWidth;
                imageHelper.CropImage(FileInfo, maxThumbnailWidthHeight, maxThumbnailWidthHeight, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemThumbnailWidthPath, "/")));

                // For Swatches
                int maxSmallThumnailWidthHeight = portal.MaxCatalogItemSmallThumbnailWidth;
                imageHelper.CropImage(FileInfo, maxSmallThumnailWidthHeight, maxSmallThumnailWidthHeight, HttpContext.Current.Server.MapPath(String.Concat(MaxCatalogItemSmallThumbnailWidthPath, "/")));
            }
        }
        #endregion

        # region Helper Methods
        /// <summary>
        /// Create a Directory here.
        /// </summary>
        /// <param name="Path"></param>
        private void CreateDirectory(string Path)
        {
            if (!System.IO.Directory.Exists(Path))
            {
                System.IO.Directory.CreateDirectory(Path);
            }
        }
        #endregion
    }
}
