using System;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage Product sku 
    /// </summary>
    public class SKUAdmin : ZNodeBusinessBase
    {
        #region Public Static Methods
        /// <summary>
        /// Get the sku inventory by Sku.
        /// </summary>
        /// <param name="sku">Sku to get the sku inventory.</param>
        /// <returns>Returns the SKUInventory object.</returns>
        public static SKUInventory GetInventory(SKU sku)
        {
            if (sku != null)
            {
                SKUInventoryService skuInventoryService = new SKUInventoryService();
                SKUInventory skuInventory = skuInventoryService.GetBySKU(sku.SKU);

                return skuInventory;
            }

            return null;
        }

        /// <summary>
        /// Gets the quantity on hand value for the sku object.
        /// </summary>
        /// <param name="sku">Sku object to get the quntity on hand value.</param>
        /// <returns>Returns the quantity on hand value.</returns>
        public static int? GetQuantity(SKU sku)
        {
            if (sku != null)
            {
                return SKUAdmin.GetQuantity(sku.SKU);
            }

            return 0;
        }

        /// <summary>
        /// Gets the quantity on hand value for the sku.
        /// </summary>
        /// <param name="sku">Sku to get the quntity on hand value.</param>
        /// <returns>Returns the quantity on hand value.</returns>
        public static int? GetQuantity(string sku)
        {
            if (!string.IsNullOrEmpty(sku))
            {
                SKUInventoryService skuInventoryService = new SKUInventoryService();
                SKUInventory skuInventory = skuInventoryService.GetBySKU(sku);

                if (skuInventory != null)
                {
                    return skuInventory.QuantityOnHand;
                }
            }

            return 0;
        }

        /// <summary>
        /// Update the quantity by Sku.
        /// </summary>
        /// <param name="sku">Sku to check.</param>
        /// <param name="quantity">Quantity to update.</param>
        public static void UpdateQuantity(string sku, int quantity)
        {
            // Update Quantity into ProductInventory Table.
            SKUService skuService = new SKUService();
            TList<SKU> skuList = skuService.GetBySKU(sku);

            if (skuList != null && skuList.Count > 0)
            {
                SKUAdmin.UpdateQuantity(skuList[0], quantity);
            }
        }

        /// <summary>
        /// Update the quantity by Sku.
        /// </summary>
        /// <param name="sku">Sku to check.</param>
        /// <param name="quantity">Quantity to update.</param>
        public static bool UpdateQuantity(SKU sku, int quantity)
        {
            bool status = false;

            if (sku != null)
            {
                // Update Quantity into ProductInventory Table.
                SKUInventoryService skuInventoryService = new SKUInventoryService();
                SKUInventory skuInventory = skuInventoryService.GetBySKU(sku.SKU);

                if (skuInventory != null)
                {
                    skuInventory.EntityState = EntityState.Changed;
                }
                else
                {
                    skuInventory = new SKUInventory();
                    skuInventory.EntityState = EntityState.Added;
                }

                if (skuInventory.QuantityOnHand != quantity)
                {
                    if (skuInventory.IsDirty)
                    {
                        status = true;
                    }
                }

                skuInventory.SKU = sku.SKU;
                skuInventory.QuantityOnHand = quantity;

                skuInventoryService.Save(skuInventory);

                return status;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Update the quantity and reorder level by Sku.
        /// </summary>
        /// <param name="sku">Sku to check.</param>
        /// <param name="quantity">Quantity to update.</param>
        /// <param name="reorderLevel">Reorder Level value to udpate.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public static bool UpdateQuantity(SKU sku, int quantity, int reorderLevel)
        {
            try
            {
                bool status = false;

                if (sku != null && !string.IsNullOrEmpty(sku.SKU))
                {
                    // Update Quantity into ProductInventory Table.
                    SKUInventoryService skuInventoryService = new SKUInventoryService();
                    SKUInventory skuInventory = null;
                    skuInventory = skuInventoryService.GetBySKU(sku.SKU);

                    if (skuInventory != null)
                    {
                        skuInventory.EntityState = EntityState.Changed;
                    }
                    else
                    {
                        skuInventory = new SKUInventory();
                        skuInventory.EntityState = EntityState.Added;
                    }

                    if (skuInventory.QuantityOnHand != quantity)
                    {
                        if (skuInventory.IsDirty)
                        {
                            status = true;
                        }
                    }

                    skuInventory.SKU = sku.SKU;
                    skuInventory.QuantityOnHand = quantity;
                    if (reorderLevel > 0)
                    {
                        skuInventory.ReOrderLevel = reorderLevel;
                    }
                    else
                    {
                        skuInventory.ReOrderLevel = null;
                    }
                    
                    skuInventoryService.Save(skuInventory);

                    return status;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Helper adminHelper = new Helper();
                adminHelper.LogMessage(ex.ToString());
                return false;
            }
        } 
        #endregion

        #region SKU Locale
        /// <summary>
        /// Get the Locale information for the product that has other languages.
        /// </summary>
        /// <param name="skuId">Sku Id to get the locale.</param>
        /// <returns>Returns the sku locale.</returns>
        public static DataSet GetLocaleIdsBySkuId(int skuId)
        {
            SKUHelper ph = new SKUHelper();
            return ph.GetLocaleIdsBySkuId(skuId, ZNodeConfigManager.SiteConfig.PortalID);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get all product skus
        /// </summary>
        /// <returns>Returns list of SKU object.</returns>
        public TList<SKU> GetAllSKU()
        {
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new SKUService();
            TList<ZNode.Libraries.DataAccess.Entities.SKU> skuList = skuService.GetAll();

            return skuList;
        }

        /// <summary>
        /// Get all product skus by portal Ids.
        /// </summary>
        /// <param name="portalIds">Comma seperated portal Ids.</param>
        /// <returns>Returns list of SKU object.</returns>
        public TList<SKU> GetSKUByPortalId(string portalIds)
        {
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new SKUService();
            SKUQuery query = new SKUQuery();
            query.AppendInQuery(SKUColumn.ProductID, string.Format("SELECT ProductID FROM ZNodeProduct WHERE PortalID IN ({0})", portalIds));
            TList<ZNode.Libraries.DataAccess.Entities.SKU> skuList = skuService.Find(query.GetParameters());

            return skuList;
        }

        /// <summary>
        /// Get Sku by Sku Id.
        /// </summary>
        /// <param name="skuId">SKU Id to get the sku object.</param>
        /// <returns>Returns the SKU object.</returns>
        public ZNode.Libraries.DataAccess.Entities.SKU GetBySKUID(int skuId)
        {
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new SKUService();
            ZNode.Libraries.DataAccess.Entities.SKU sku = skuService.GetBySKUID(skuId);

            return sku;
        }

        /// <summary>
        /// Get the deep loaded sku by sku Id.
        /// </summary>
        /// <param name="skuId">SKU Id to get the deep loaded sku object.</param>
        /// <returns>Returns the deep loaded SKU object.</returns>
        public ZNode.Libraries.DataAccess.Entities.SKU DeepLoadBySKUID(int skuId)
        {
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new SKUService();
            ZNode.Libraries.DataAccess.Entities.SKU sku = skuService.DeepLoadBySKUID(skuId, true, DeepLoadType.IncludeChildren, typeof(Product));

            return sku;
        }

        /// <summary>
        /// Get the product sku by sku.
        /// </summary>
        /// <param name="sku">SKU to get the product SKU.</param>
        /// <returns>Returns the SKU object.</returns>
        public ZNode.Libraries.DataAccess.Entities.SKU GetBySKU(string sku)
        {
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new SKUService();
            TList<SKU> skuList = skuService.Find("SKU='" + sku + "'");

            SKU skuToReturn = null;
            if (skuList.Count > 0)
            {
                skuToReturn = skuList[0];
            }

            return skuToReturn;
        }

        /// <summary>
        /// Add a new product sku
        /// </summary>
        /// <param name="sku">SKU object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Add(SKU sku)
        {
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new SKUService();

            bool isAdded = skuService.Insert(sku);

            return isAdded;
        }

        /// <summary>
        /// Add a new product SKU
        /// </summary>
        /// <param name="sku">SKU object to insert.</param>
        /// <param name="skuId">Sku Id to to return.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Add(SKU sku, out int skuId)
        {
            skuId = 0;
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new SKUService();
            bool isAdded = skuService.Insert(sku);
            skuId = sku.SKUID;

            return isAdded;
        }

        /// <summary>
        /// Add a new product SKU and SKU Attribute
        /// </summary>
        /// <param name="sku">SKU object to insert.</param>
        /// <param name="skuAttribute">Sku attribute to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Add(SKU sku, SKUAttribute skuAttribute)
        {
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new SKUService();
            bool isAdded = skuService.Insert(sku);

            if (isAdded)
            {
                skuAttribute.SKUID = sku.SKUID;
                isAdded = this.AddSKUAttribute(skuAttribute);
            }

            return isAdded;
        }

        /// <summary>
        /// Update a product SKU by Sku.
        /// </summary>
        /// <param name="sku">SKU object to udpate.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool Update(SKU sku)
        {
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new SKUService();
            bool isUpdated = skuService.Update(sku);

            return isUpdated;
        }

        /// <summary>
        /// Update a product Sku by Sku.
        /// </summary>
        /// <param name="sku">SKU object to udpate.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool DeepUpdate(SKU sku)
        {
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new SKUService();
            bool isUpdated = skuService.DeepSave(sku, DeepSaveType.IncludeChildren, typeof(Product));

            return isUpdated;
        }

        /// <summary>
        /// Update SKU and Sku Attribute.
        /// </summary>
        /// <param name="sku">SKU object to udpate.</param>
        /// <param name="skuAttribute">Sku attribute to update.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Update(SKU sku, SKUAttribute skuAttribute)
        {
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new SKUService();
            bool isUpdated = skuService.Update(sku);

            if (isUpdated)
            {
                isUpdated = this.UpdateSKUAttribute(skuAttribute);
            }

            return isUpdated;
        }

        /// <summary>
        /// Delete a product Sku by Sku Id.
        /// </summary>
        /// <param name="skuId">SKU Id to delete the Sku object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool Delete(int skuId)
        {
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new SKUService();
            bool isDeleted = skuService.Delete(skuId);

            if (!isDeleted)
            {
                return false;
            }

            return isDeleted;
        }

        /// <summary>
        /// Delete product Sku by Product Id.
        /// </summary>
        /// <param name="productId">Product Id to delete the product sku.</param>                
        public void DeleteByProductId(int productId)
        {
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new SKUService();            
            TList<SKU> skuList = skuService.GetByProductID(productId);

            foreach (SKU sku in skuList)
            {
                // delete any profile related records.
                DataRepository.SKUProfileEffectiveProvider.Delete(DataRepository.SKUProfileEffectiveProvider.GetBySkuId(sku.SKUID));
                DataRepository.SKUProfileProvider.Delete(DataRepository.SKUProfileProvider.GetBySKUID(sku.SKUID));
                this.DeleteBySKUId(sku.SKUID);
            }

            skuService.Delete(skuList);
        }

        /// <summary>
        /// Get all skus by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the product sku.</param>
        /// <returns>Returns list of SKU object.</returns>
        public TList<SKU> GetByProductID(int productId)
        {
            ZNode.Libraries.DataAccess.Service.SKUService productSKUService = new SKUService();

            return productSKUService.GetByProductID(productId);
        }

        #endregion

        #region SKUAtribute Public Methods

        /// <summary>
        /// Get product sku attribute by sku attribute Id.
        /// </summary>
        /// <param name="skuAttributeId">Sku attribute Id to get the sku attribute object.</param>
        /// <returns>Returns the SKU attribute object.</returns>
        public SKUAttribute GetBySKUAttributeID(int skuAttributeId)
        {
            ZNode.Libraries.DataAccess.Service.SKUAttributeService skuService = new SKUAttributeService();
            ZNode.Libraries.DataAccess.Entities.SKUAttribute skuAttribute = skuService.GetBySKUAttributeID(skuAttributeId);

            return skuAttribute;
        }

        /// <summary>
        /// Get all the sku attribute dataset by Sku Id
        /// </summary>
        /// <param name="skuId">Sku Id to get the sku attribute dataset.</param>
        /// <returns>Returns the sku attribute dataset.</returns>
        public DataSet GetBySKUId(int skuId)
        {
            ZNode.Libraries.DataAccess.Service.SKUAttributeService skuService = new SKUAttributeService();
            DataSet skuAttributeDataSet = skuService.GetBySKUID(skuId).ToDataSet(true);

            return skuAttributeDataSet;
        }

        /// <summary>
        /// Delete sku attributes by sku Id.
        /// </summary>
        /// <param name="skuId">Sku Id to delete the Sku object.</param>        
        public void DeleteBySKUId(int skuId)
        {
            ZNode.Libraries.DataAccess.Service.SKUAttributeService skuService = new SKUAttributeService();
            TList<SKUAttribute> skuAttribList = skuService.GetBySKUID(skuId);

            skuService.Delete(skuAttribList);
        }     

        /// <summary>
        /// Update the sku attribute.
        /// </summary>
        /// <param name="skuAttribute">Sku attribute object to update.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool UpdateSKUAttribute(SKUAttribute skuAttribute)
        {
            ZNode.Libraries.DataAccess.Service.SKUAttributeService skuService = new SKUAttributeService();
            bool isUpdated = skuService.Update(skuAttribute);

            return isUpdated;
        }

        /// <summary>
        /// Add a new product sku attribute.
        /// </summary>
        /// <param name="skuAttribute">Sku attibute object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddSKUAttribute(SKUAttribute skuAttribute)
        {
            ZNode.Libraries.DataAccess.Service.SKUAttributeService skuService = new SKUAttributeService();
            bool isAdded = skuService.Insert(skuAttribute);

            return isAdded;
        }

        /// <summary>
        /// Check whether the sku attribute is exist.
        /// </summary>
        /// <param name="productId">Product Id to check.</param>
        /// <param name="skuId">Sku Id to check.</param>
        /// <param name="selectAttributes">Selected attributes to check.</param>
        /// <returns>Returns true if exists otherwise false.</returns>
        public bool CheckSKUAttributes(int productId, int skuId, string selectAttributes)
        {
            ZNode.Libraries.DataAccess.Custom.SKUHelper skuService = new SKUHelper();
            bool isExist = skuService.GetSKUAttributes(productId, skuId, selectAttributes);

            return isExist;
        }

        /// <summary>
        /// Get all the sku attribute by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the sku attribute dataset.</param>
        /// <param name="selectAttributes">Selected attributes to get the sku attribute dataset.</param>
        /// <returns>Returns the sku attribute dataset.</returns>
        public DataSet GetBySKUAttributes(int productId, string selectAttributes)
        {
            ZNode.Libraries.DataAccess.Custom.SKUHelper skuService = new SKUHelper();
            DataSet skuAttributeDataSet = skuService.GetSkubyAttributes(productId, selectAttributes);

            return skuAttributeDataSet;
        }

        /// <summary>
        /// Get all the sku. 
        /// </summary>
        /// <returns>Returns the sku dataset.</returns>
        public DataSet GetallSkuData()
        {
            ZNode.Libraries.DataAccess.Custom.SKUHelper skuService = new SKUHelper();
            DataSet skuDataSet = skuService.GetAllSKU();

            return skuDataSet;
        }

        #endregion

        #region SkuInventory
              
        /// <summary>
        /// Check whether the particular SKU exist for some other products.
        /// </summary>
        /// <param name="sku">Sku to find the existence</param>
        /// <param name="productId">Product Id to check.</param>
        /// <returns>Returns true if Sku found otherwise False.</returns>
        public bool IsSkuExist(string sku, int productId)
        {
            bool isExist = false;
            SKUService skuService = new SKUService();
            TList<SKU> skuList = skuService.GetBySKU(sku);

            // In Add mode check all sku for existence
            if (productId == 0 && skuList.Count > 0)
            {
                isExist = true;
            }
            else
            {
                // In product edit mode, Check for other product that has the same Sku.
                TList<SKU> resultList = skuList.FindAll(delegate(SKU skuEntity)
                {
                    return skuEntity.ProductID == productId;
                });

                TList<SKU> resultList1 = skuList.FindAll(delegate(SKU skuEntity)
                {
                    return skuEntity.ProductID != productId;
                });

                isExist = true;
                if (resultList.Count > 0 || resultList1.Count == 0)
                {
                    isExist = false;
                }
            }

            return isExist;
        }       

        /// <summary>
        /// Delete the Sku inventory by Sku object.
        /// </summary>
        /// <param name="sku">Sku object to delete.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteSkuInventory(SKU sku)
        {
            SKUService skuService = new SKUService();
            TList<SKU> list = skuService.GetBySKU(sku.SKU);

            if (list.Count == 0)
            {
                SKUInventoryService skuInvServ = new SKUInventoryService();
                SKUInventory skuInventory = skuInvServ.GetBySKU(sku.SKU);

                return skuInvServ.Delete(skuInventory);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get the sku inventory by Sku.
        /// </summary>
        /// <param name="sku">Sku string to get the sku inventory object.</param>
        /// <returns>Returns the SKUInventory object.</returns>
        public SKUInventory GetSkuInventoryBySKU(string sku)
        {
            SKUInventoryService skuInvServ = new SKUInventoryService();
            return skuInvServ.GetBySKU(sku);
        }
        #endregion        

        /// <summary>
        /// Delete Sku Attribute
        /// </summary>
        /// <param name="skuAttribute">Sku attribute object to delete.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        private bool DeleteSKUAttribute(SKUAttribute skuAttribute)
        {
            ZNode.Libraries.DataAccess.Service.SKUAttributeService skuService = new SKUAttributeService();
            bool isDeleted = skuService.Delete(skuAttribute);

            return isDeleted;
        }

        #region SKU Profiles

        /// <summary>
        /// Get all the sku profile by skuid. 
        /// </summary>
        /// <returns>Returns the sku dataset.</returns>
        public DataSet GetSkuProfileBySkuID(int skuId)
        {
            ZNode.Libraries.DataAccess.Custom.SKUHelper skuService = new SKUHelper();
            DataSet skuDataSet = skuService.GetSkuProfileBySkuID(skuId);

            return skuDataSet;
        }

        /// <summary>
        /// Get all the sku profile by skuid. 
        /// </summary>
        /// <returns>Returns the sku dataset.</returns>
        public DataSet GetSkuProfileEffectiveBySkuID(int skuId)
        {
            ZNode.Libraries.DataAccess.Custom.SKUHelper skuService = new SKUHelper();
            DataSet skuDataSet = skuService.GetSkuProfileEffectiveBySkuID(skuId);

            return skuDataSet;
        }

        #endregion
    }
}
