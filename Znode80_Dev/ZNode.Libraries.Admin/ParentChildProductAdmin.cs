﻿using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Represents the ParentChildProductAdmin class inherited from ZNodeBusinessBase.
    /// </summary>
    public class ParentChildProductAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Add New bundle product item for a Product
        /// </summary>
        /// <param name="parentProductId">Parent product Id</param>
        /// <param name="childProductId">Child product Id.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Insert(int parentProductId, int childProductId)
        {
            // Instance for a ProductCrossSellerHelper class            
            ParentChildProductService parentChildProductService = new ParentChildProductService();
            ParentChildProduct parentChildProduct = new ParentChildProduct();

            parentChildProduct.ParentProductID = parentProductId;
            parentChildProduct.ChildProductID = childProductId;

            return parentChildProductService.Insert(parentChildProduct);
        }

        /// <summary>
        /// Get the bundle item by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the bundle item product.</param>
        /// <returns>Returns the list of child product by parent product Id.</returns>
        public TList<ParentChildProduct> GetParentChildProductByProductId(int productId)
        {
            ParentChildProductService parentChildProductService = new ParentChildProductService();
            return parentChildProductService.GetByParentProductID(productId);
        }

        /// <summary>
        /// Delete a specific bundle product item for a product
        /// </summary>
        /// <param name="parentChildProductId">ParentChildProduct Id to delete.</param>        
        /// <returns>Returns true if deleted else false.</returns>
        public bool Delete(int parentChildProductId)
        {
            // Instance for a ProductCrossSellerHelper class            
            ParentChildProductService parentChildProductService = new ParentChildProductService();
            return parentChildProductService.Delete(parentChildProductId);
        }
    }
}
