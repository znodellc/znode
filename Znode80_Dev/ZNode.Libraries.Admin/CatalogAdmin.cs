using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage Catalogs
    /// </summary>
    public class CatalogAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Returns all Catalogs for a portal
        /// </summary>
        /// <returns>Returns the list of catalog object.</returns>
        public TList<Catalog> GetAllCatalogs()
        {
            ZNode.Libraries.DataAccess.Service.CatalogService catalogService = new ZNode.Libraries.DataAccess.Service.CatalogService();
            TList<ZNode.Libraries.DataAccess.Entities.Catalog> catalogList = catalogService.GetAll();
            return catalogList;
        }

        /// <summary>
        /// Returns all Catalogs for a portal
        /// </summary>
        /// <param name="catalogId">Catalog Id to get the Catalog object.</param>
        /// <returns>Returns the Catalog object for the given catalog Id.</returns>
        public Catalog GetCatalogByCatalogId(int catalogId)
        {
            ZNode.Libraries.DataAccess.Service.CatalogService catalogService = new ZNode.Libraries.DataAccess.Service.CatalogService();
            ZNode.Libraries.DataAccess.Entities.Catalog catalog = catalogService.GetByCatalogID(catalogId);
            return catalog;
        }

        /// <summary>
        /// Update the catalog
        /// </summary>
        /// <param name="catalog">Catalog object to update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(ZNode.Libraries.DataAccess.Entities.Catalog catalog)
        {
            ZNode.Libraries.DataAccess.Service.CatalogService catalogService = new CatalogService();
            return catalogService.Update(catalog);
        }

        /// <summary>
        /// Insert the new catalog into data source.
        /// </summary>
        /// <param name="catalog">Catalog object to insert.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Insert(ZNode.Libraries.DataAccess.Entities.Catalog catalog)
        {
            ZNode.Libraries.DataAccess.Service.CatalogService catalogService = new CatalogService();
            return catalogService.Insert(catalog);
        }

        /// <summary>
        /// Save the catalog into data souce.
        /// </summary>
        /// <param name="catalog">Catalog object to save.</param>
        /// <returns>Return the Catalog object if successfully saved.</returns>
        public Catalog Save(ZNode.Libraries.DataAccess.Entities.Catalog catalog)
        {
            ZNode.Libraries.DataAccess.Service.CatalogService catalogService = new CatalogService();
            return catalogService.Save(catalog);
        }

        /// <summary>
        /// Delete catalog details
        /// </summary>
        /// <param name="catalogId">Catalog Id to delete the Catalog object from data source.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool DeleteCatalog(int catalogId, bool preserve)
        {
            ZNode.Libraries.DataAccess.Custom.CatalogHelper catalogHelper = new ZNode.Libraries.DataAccess.Custom.CatalogHelper();
            return catalogHelper.DeleteCatalog(catalogId, preserve);
        }

        /// <summary>
        /// Delete catalog and portal catalog details
        /// </summary>
        /// <param name="catalogId">Catalog Id to delete from the data source.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Delete(int catalogId)
        {
            ZNode.Libraries.DataAccess.Service.CatalogService catalogService = new CatalogService();

            ZNode.Libraries.DataAccess.Service.PortalCatalogService portalCatalogService = new PortalCatalogService();
            TList<PortalCatalog> portalCatalogList = portalCatalogService.GetByCatalogID(catalogId);
            PortalAdmin portalAdmin = new PortalAdmin();

            bool isDeleted = catalogService.Delete(catalogId);

            if (isDeleted)
            {
                if (portalCatalogList.Count > 0)
                {
                    foreach (PortalCatalog portalcatalog in portalCatalogList)
                    {
                        isDeleted = portalAdmin.DeletePortalCatalog(portalcatalog.PortalID, portalcatalog.LocaleID);
                    }
                }
            }

            return isDeleted;
        }

        /// <summary>
        /// Update the portal catalog.
        /// </summary>
        /// <param name="portalCatalog">Portal catalog to udpate.</param>        
        public void UpdatePortalCatalog(ZNode.Libraries.DataAccess.Entities.PortalCatalog portalCatalog)
        {
            ZNode.Libraries.DataAccess.Service.PortalCatalogService portalcatalogService = new PortalCatalogService();
            portalcatalogService.Update(portalCatalog);

            // Clear all cache memory.
            CacheManager.Clear();
        }

        /// <summary>
        /// Insert a new portal catalog.
        /// </summary>
        /// <param name="portalCatalog">Portal catalog to add into data source.</param>        
        public void AddPortalCatalog(ZNode.Libraries.DataAccess.Entities.PortalCatalog portalCatalog)
        {
            ZNode.Libraries.DataAccess.Service.PortalCatalogService portalcatalogService = new PortalCatalogService();
            portalcatalogService.Save(portalCatalog);

            // Clear all cache memory.
            CacheManager.Clear();
        }

        /// <summary>
        /// Delete the portal catalog.
        /// </summary>
        /// <param name="portalId">Portal Id to get the list of portal catlog to delete.</param>
        public void DeleteportalCatalog(int portalId)
        {
            ZNode.Libraries.DataAccess.Service.PortalCatalogService portalcatalogService = new PortalCatalogService();
            TList<PortalCatalog> portalcatalogList = portalcatalogService.GetByPortalID(portalId);

            portalcatalogService.Delete(portalcatalogList);

            // Clear all cache memory.
            CacheManager.Clear();
        }

        /// <summary>
        /// Get Catalog based by portal Id
        /// </summary>
        /// <param name="portalId">Portal Id to get the catalog Id</param>
        /// <returns>Returns the catalog Id for the portal.</returns>
        public int GetCatalogIDByPortalID(int portalId)
        {
            ZNode.Libraries.DataAccess.Service.PortalCatalogService portalcatalogService = new PortalCatalogService();
            TList<PortalCatalog> portalcatalogList = portalcatalogService.GetByPortalID(portalId);

            int catalogId = 0;

            if (portalcatalogList.Count > 0)
            {
                catalogId = portalcatalogList[0].CatalogID;
            }

            return catalogId;
        }

        /// <summary>
        /// Get Catalog Id list based on Portal Id list.
        /// </summary>
        /// <param name="portalIds">List of portal Id.</param>
        /// <returns>Returns the comma seperated catalog Id.</returns>
        public string GetCatalogIDsByPortalIDs(string portalIds)
        {
            ZNode.Libraries.DataAccess.Service.PortalCatalogService portalcatalogService = new PortalCatalogService();
            TList<PortalCatalog> portalcatalogList = portalcatalogService.GetAll();

            DataSet ds = portalcatalogList.ToDataSet(true);

            DataView dv = new DataView(ds.Tables[0]);

            // If no store associated, then return 0;
            if (string.IsNullOrEmpty(portalIds))
            {
                return "0";
            }

            if (portalIds != "0")
            {
                dv.RowFilter = "PortalId in (" + portalIds + ")";
            }

            string catalogIds = string.Empty;

            foreach (DataRow dr in dv.ToTable().Rows)
            {
                catalogIds += dr["CatalogId"].ToString() + ",";
            }

            catalogIds = catalogIds.Remove(catalogIds.Length - 1);

            return catalogIds;
        }

        /// <summary>
        /// Return Catalog By Portal Id
        /// </summary>
        /// <param name="portalId">Portal Id to get the Catalog.</param>
        /// <returns>Returns the Catalog object for the portal Id.</returns>
        public Catalog GetCatalogByPortalID(int portalId)
        {
            ZNode.Libraries.DataAccess.Service.PortalCatalogService portalcatalogService = new PortalCatalogService();
            TList<PortalCatalog> portalcatalogList = portalcatalogService.GetByPortalID(portalId);

            ZNode.Libraries.DataAccess.Service.CatalogService catalogService = new CatalogService();
            Catalog catalog = catalogService.GetByCatalogID(portalcatalogList[0].CatalogID);

            return catalog;
        }

        /// <summary>
        /// Get the PortalCatalog By Portal Id
        /// </summary>
        /// <param name="portalId">Portal Id to get the PortalCatalog.</param>
        /// <returns>Returns the PortalCatalog object for the portal Id. If there is no portal catalog then null value will be returned.</returns>
        public PortalCatalog GetPortalCatalogByPortalID(int portalId)
        {
            ZNode.Libraries.DataAccess.Service.PortalCatalogService portalcatalogService = new PortalCatalogService();
            TList<PortalCatalog> portalCatalogList = portalcatalogService.GetByPortalID(portalId);

            if (portalCatalogList.Count > 0)
            {
                return portalCatalogList[0];
            }

            return null;
        }
    }
}
