using System;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace ZNode.Libraries.Admin
{
	/// <summary>
	/// Represents the ProviderTypeAdmin class.
	/// </summary>
	public class ProviderTypeAdmin
	{
		private DiscountTypeService _promotionTypeService = new DiscountTypeService();
		private ShippingTypeService _shippingTypeService = new ShippingTypeService();
		private SupplierTypeService _supplierTypeService = new SupplierTypeService();
		private TaxRuleTypeService _taxRuleTypeService = new TaxRuleTypeService();

		#region Promotion Type
		/// <summary>
		/// Get all discount types.
		/// </summary>
		/// <returns>Returns list of DiscountType object</returns>
		public TList<DiscountType> GetDiscountTypes()
		{
			return _promotionTypeService.GetAll();
		}

		/// <summary>        
		/// Get a discount type by dicount type Id
		/// </summary>
		/// <param name="discountTypeId">Discount type Id to the discount type object.</param>
		/// <returns>Returns the DiscountType object.</returns>
		public DiscountType GetDiscountTypeById(int discountTypeId)
		{
			return _promotionTypeService.GetByDiscountTypeID(discountTypeId);
		}

		/// <summary>
		/// Add a new discount type
		/// </summary>
		/// <param name="discountType">DiscountType object to insert.</param>
		/// <returns>Returns true if inserted otherwise false.</returns>
		public bool AddDiscountType(DiscountType discountType)
		{
			return _promotionTypeService.Insert(discountType);
		}

		/// <summary>
		/// Update a discount type.
		/// </summary>
		/// <param name="discountType">DiscountType object to insert.</param>
		/// <returns>Returns true if udpated otherwise false.</returns>
		public bool UpdateDiscountType(DiscountType discountType)
		{
			return _promotionTypeService.Update(discountType);
		}

		/// <summary>
		/// Delete a discount type by Id.
		/// </summary>
		/// <param name="discountTypeId">Discount type Id to get the discount type object.</param>
		/// <returns>Returns true if deleted otherwise false.</returns>
		public bool DeleteDiscountType(int discountTypeId)
		{
			return _promotionTypeService.Delete(discountTypeId);
		}
		#endregion

		#region Shipping Type
		/// <summary>
		/// Get all shipping types.
		/// </summary>
		/// <returns>Returns list of ShippingType object.</returns>
		public TList<ShippingType> GetShippingTypes()
		{
			return _shippingTypeService.GetAll();
		}

		/// <summary>
		/// Get a shipping type by Id.
		/// </summary>
		/// <param name="shippingTypeId">Shipping type Id to get the shipping type object.</param>
		/// <returns>Returns the ShippingType object.</returns>
		public ShippingType GetShippingTypeById(int shippingTypeId)
		{
			return _shippingTypeService.GetByShippingTypeID(shippingTypeId);
		}

		/// <summary>
		/// Add a new shipping type
		/// </summary>
		/// <param name="shippingType">ShippingType object to insert.</param>
		/// <returns>Returns true if inserted otherwise false.</returns>
		public bool AddShippingType(ShippingType shippingType)
		{
			return _shippingTypeService.Insert(shippingType);
		}

		/// <summary>
		/// Update a shipping type
		/// </summary>
		/// <param name="shippingType">ShippingType object to udpate.</param>
		/// <returns>Returns true if updated otherwise false.</returns>
		public bool UpdateShippingType(ShippingType shippingType)
		{
			return _shippingTypeService.Update(shippingType);
		}

		/// <summary>
		/// Delete a shipping type by Id.
		/// </summary>
		/// <param name="shippingTypeId">Shipping type Id to delete the shipping type object.</param>
		/// <returns>Returns true if deleted otherwise false.</returns>
		public bool DeleteShippingType(int shippingTypeId)
		{
			return _shippingTypeService.Delete(shippingTypeId);
		}
		#endregion

		#region Supplier Type
		/// <summary>
		/// Get all supplier types.
		/// </summary>
		/// <returns>Returns list of SupplierType object.</returns>
		public TList<SupplierType> GetSupplierTypes()
		{
			return _supplierTypeService.GetAll();
		}

		/// <summary>        
		/// Get a supplier type by supplier type Id.
		/// </summary>
		/// <param name="supplierTypeId">Supplier type Id to get the supplier type object.</param>
		/// <returns>Returns the SupplierType object.</returns>
		public SupplierType GetSupplierTypeById(int supplierTypeId)
		{
			return _supplierTypeService.GetBySupplierTypeID(supplierTypeId);
		}

		/// <summary>
		/// Add a new supplier type
		/// </summary>
		/// <param name="supplierType">SupplierType object to insert.</param>
		/// <returns>Returns true if inserted otherwise false.</returns>
		public bool AddSupplierType(SupplierType supplierType)
		{
			return _supplierTypeService.Insert(supplierType);
		}

		/// <summary>
		/// Update a supplier type
		/// </summary>
		/// <param name="supplierType">SupplierType object to udapte.</param>
		/// <returns>Returns true if udpated otherwise false.</returns>
		public bool UpdateSupplierType(SupplierType supplierType)
		{
			return _supplierTypeService.Update(supplierType);
		}

		/// <summary>
		/// Delete a supplier type by Id.
		/// </summary>
		/// <param name="supplierTypeId">Supplier type Id to delete the supplier type.</param>
		/// <returns>Returns true if inserted otherwise false.</returns>
		public bool DeleteSupplierType(int supplierTypeId)
		{
			return _supplierTypeService.Delete(supplierTypeId);
		}
		#endregion

		#region Tax Type
		/// <summary>
		/// Get all tax tule types.
		/// </summary>
		/// <returns>Returns list of TaxRuleType object.</returns>
		public TList<TaxRuleType> GetTaxRuleTypes()
		{
			return _taxRuleTypeService.GetAll();
		}

		/// <summary>        
		/// Get a tax rule type by Id.
		/// </summary>
		/// <param name="taxRuleTypeId">Tax rule type Id to get the tax rule object.</param>
		/// <returns>Returns the TaxRule object.</returns>
		public TaxRuleType GetTaxRuleTypeById(int taxRuleTypeId)
		{
			return _taxRuleTypeService.GetByTaxRuleTypeID(taxRuleTypeId);
		}

		/// <summary>
		/// Add a new taxrule type
		/// </summary>
		/// <param name="taxRuleType">TaxRuleType object to insert.</param>
		/// <returns>Returns true if inserted otherwise false.</returns>
		public bool AddTaxRuleType(TaxRuleType taxRuleType)
		{
			return _taxRuleTypeService.Insert(taxRuleType);
		}

		/// <summary>
		/// Update a taxrule type
		/// </summary>
		/// <param name="taxRuleType">TaxRuleType object to update.</param>
		/// <returns>Returns true if udpated otherwise false.</returns>
		public bool UpdateTaxRuleType(TaxRuleType taxRuleType)
		{
			return _taxRuleTypeService.Update(taxRuleType);
		}

		/// <summary>
		/// Delete a tax rule type by Id.
		/// </summary>
		/// <param name="taxRuleTypeId">Tax rule type Id to delete the Tax rule type object.</param>
		/// <returns>Returns true if deleted otherwise false.</returns>
		public bool DeleteTaxRuleType(int taxRuleTypeId)
		{
			return _taxRuleTypeService.Delete(taxRuleTypeId);
		}
		#endregion

		#region Public Helper Method
		/// <summary>
		/// Check whether the class name already exist
		/// </summary>
		/// <param name="itemId">Item Id to check.</param>
		/// <param name="className">Class name to check</param>
		/// <param name="objectType">Type of object to check.</param>
		/// <returns>Returns true if class name already exist otherwise false.</returns>
		public bool IsClassNameExist(int itemId, string className, Type objectType)
		{
			var isExist = false;

			if (objectType == typeof(DiscountType))
			{
				var discountTypeQuery = new DiscountTypeQuery();
				discountTypeQuery.AppendEquals(DiscountTypeColumn.ClassName, className.Trim());

				if (itemId > 0)
				{
					discountTypeQuery.AppendNotEquals(DiscountTypeColumn.DiscountTypeID, itemId.ToString());
				}

				var discountTypeList = _promotionTypeService.Find(discountTypeQuery.GetParameters());
				if (discountTypeList.Count >= 1)
				{
					isExist = true;
				}
			}

			if (objectType == typeof(ShippingType))
			{
				var shippingTypeQuery = new ShippingTypeQuery();
				shippingTypeQuery.AppendEquals(ShippingTypeColumn.ClassName, className.Trim());

				if (itemId > 0)
				{
					shippingTypeQuery.AppendNotEquals(ShippingTypeColumn.ShippingTypeID, itemId.ToString());
				}

				var shippingTypeList = _shippingTypeService.Find(shippingTypeQuery.GetParameters());
				if (shippingTypeList.Count >= 1)
				{
					isExist = true;
				}
			}

			if (objectType == typeof(SupplierType))
			{
				var supplierTypeQuery = new SupplierTypeQuery();
				supplierTypeQuery.AppendEquals(SupplierTypeColumn.ClassName, className.Trim());

				if (itemId > 0)
				{
					supplierTypeQuery.AppendNotEquals(SupplierTypeColumn.SupplierTypeID, itemId.ToString());
				}

				var supplierTypeList = _supplierTypeService.Find(supplierTypeQuery.GetParameters());
				if (supplierTypeList.Count >= 1)
				{
					isExist = true;
				}
			}

			if (objectType == typeof(TaxRuleType))
			{
				var taxRuleTypeQuery = new TaxRuleTypeQuery();
				taxRuleTypeQuery.AppendEquals(TaxRuleTypeColumn.ClassName, className.Trim());

				if (itemId > 0)
				{
					taxRuleTypeQuery.AppendNotEquals(TaxRuleTypeColumn.TaxRuleTypeID, itemId.ToString());
				}

				var taxRuleTypeList = _taxRuleTypeService.Find(taxRuleTypeQuery.GetParameters());
				if (taxRuleTypeList.Count >= 1)
				{
					isExist = true;
				}
			}

			return isExist;
		}
		#endregion

		#region Private Methods
		/// <summary>
		/// Get the next unique discount type Id.
		/// </summary>
		/// <param name="promotionTypeService">PromotionTypeService object to get all promotion types.</param>
		/// <returns>Returns the next unique discount type Id.</returns>
		private int GetDiscountTypeId(DiscountTypeService promotionTypeService)
		{
			var discountTypeList = promotionTypeService.GetAll();

			if (discountTypeList.Count > 0)
			{
				discountTypeList.Sort("DiscountTypeID Desc");
				return discountTypeList[0].DiscountTypeID + 1;
			}

			return 1;
		}

		/// <summary>
		/// Get the next unique shipping type Id.
		/// </summary>
		/// <param name="shippingTypeService">ShippingTypeService object to get all shipping types.</param>
		/// <returns>Returns the next unique shipping type Id.</returns>
		private int GetShippingTypeId(ShippingTypeService shippingTypeService)
		{
			var shippingTypeList = shippingTypeService.GetAll();

			if (shippingTypeList.Count > 0)
			{
				shippingTypeList.Sort("ShippingTypeID Desc");
				return shippingTypeList[0].ShippingTypeID + 1;
			}

			return 1;
		}
		#endregion
	}
}
