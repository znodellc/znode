﻿using System;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Represent the product review state enum
    /// Enumeration ID value must match with ZNodeProductReviewState table ProductReviewStateID column.
    /// </summary>
    public enum ZNodeProductReviewState
    {
        /// <summary>
        /// Product is awaiting for reviewer Aproval.
        /// </summary>
        PendingApproval = 10,

        /// <summary>
        /// Product is approved from Reviewer.
        /// </summary>
        Approved = 20,

        /// <summary>
        /// Product is declined by Reviewer.
        /// </summary>
        Declined = 30,

        /// <summary>
        /// New product added to edit.
        /// </summary>
        ToEdit = 40
    }

    /// <summary>
    /// Provides methods to manage product review history and product review state
    /// </summary>
    public class ProductReviewHistoryAdmin : ZNodeBusinessBase
    {
        #region Public Methods
        /// <summary>
        /// Get product review history details
        /// </summary>
        /// <param name="reviewHistoryId">Product review history Id to get product review state object.</param>
        /// <returns>Returns ProductHistoryReviewHistory object</returns>
        public ProductReviewHistory GetByReviewHistoryID(int reviewHistoryId)
        {
            ProductReviewHistoryService productReviewHistoryService = new ProductReviewHistoryService();
            ProductReviewHistory productReviewHistory = productReviewHistoryService.GetByProductReviewHistoryID(reviewHistoryId);

            return productReviewHistory;
        }

        /// <summary>
        /// Get product review state history by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get product review history.</param>
        /// <returns>Returns the list of ProductReviewHistory object.</returns>
        public TList<ProductReviewHistory> GetByProductID(int productId)
        {
            ProductReviewHistoryService productReviewHistoryService = new ProductReviewHistoryService();
            TList<ProductReviewHistory> productReviewHistoryList = productReviewHistoryService.GetByProductID(productId);

            return productReviewHistoryList;
        }

        /// <summary>
        /// Get all product review state.
        /// </summary>
        /// <returns>Returns list of ProductReviewState object.</returns>
        public TList<ProductReviewState> GetAllProductReviewState()
        {
            ProductReviewStateService productReviewStateService = new ProductReviewStateService();
            TList<ProductReviewState> productReviewStateList = productReviewStateService.GetAll();

            return productReviewStateList;
        }
        
        /// <summary>
        /// Add the new product review history.
        /// </summary>
        /// <param name="productReviewHistory">ProductReviewHistory object to add.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Add(ProductReviewHistory productReviewHistory)
        {
            ZNode.Libraries.DataAccess.Service.ProductReviewHistoryService productReviewHistoryService = new ProductReviewHistoryService();

            int totalCount = 0;
            bool isAdded = false;

            // Get the products last review history object. If last action is 'Pending Approval' then append the current description with the last product review history.
            TList<ProductReviewHistory> productReviewHistoryList = productReviewHistoryService.GetPaged("ProductID=" + productReviewHistory.ProductID.ToString(), "ProductReviewHistoryID DESC", 0, 1, out totalCount);
            ProductReviewHistory lastProductReviewHistory = null;
            if (productReviewHistoryList.Count > 0 && !string.IsNullOrEmpty(productReviewHistory.Description))
            {
                lastProductReviewHistory = productReviewHistoryList[0];

                if (productReviewHistory.VendorID == 0)
                {
                    productReviewHistory.VendorID = lastProductReviewHistory.VendorID;
                }

                // Update changed field name only if doesn't contains.
                if (lastProductReviewHistory != null && lastProductReviewHistory.Status == productReviewHistory.Status && lastProductReviewHistory.Username == productReviewHistory.Username)
                {
                    if (!lastProductReviewHistory.Description.ToLower().Contains(productReviewHistory.Description.ToLower()))
                    {
                        productReviewHistory.Description = lastProductReviewHistory.Description + ", " + productReviewHistory.Description;
                        productReviewHistory.ProductReviewHistoryID = lastProductReviewHistory.ProductReviewHistoryID;
                        isAdded = productReviewHistoryService.Update(productReviewHistory);
                    }
                }
                else
                {
                    isAdded = productReviewHistoryService.Insert(productReviewHistory);
                }
            }
            else
            {
                isAdded = productReviewHistoryService.Insert(productReviewHistory);
            }

            return isAdded;
        }

        /// <summary>
        /// Get the products changed fields detail. 
        /// </summary>
        /// <param name="productId">Product Id to get changed fields.</param>
        /// <returns>Returns changed fields, If there are no changes then returns empty string.</returns>
        public string GetChangedFields(int productId)
        {
            ZNode.Libraries.DataAccess.Service.ProductReviewHistoryService productReviewHistoryService = new ProductReviewHistoryService();

            int totalCount = 0;
            string changedFields = string.Empty;

            // Get the products last review history object. 
            TList<ProductReviewHistory> productReviewHistoryList = productReviewHistoryService.GetPaged("ProductID=" + productId.ToString(), "ProductReviewHistoryID DESC", 0, 1, out totalCount);
            ProductReviewHistory lastProductReviewHistory = null;
            if (productReviewHistoryList.Count > 0)
            {
                lastProductReviewHistory = productReviewHistoryList[0];

                // Get the description if last action is PendingApproval
                if (lastProductReviewHistory != null)
                {
                    if (lastProductReviewHistory.Status.ToLower().Replace(" ", string.Empty) == "pendingapproval")
                    {
                        changedFields = lastProductReviewHistory.Description;
                    }
                }
            }

            return changedFields;
        }

        /// <summary>
        /// Update the product review history
        /// </summary>
        /// <param name="productReviewHistory">ProductReviewHistory object</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool Update(ProductReviewHistory productReviewHistory)
        {
            ZNode.Libraries.DataAccess.Service.ProductReviewHistoryService productReviewHistoryService = new ProductReviewHistoryService();
            bool isUpdated = productReviewHistoryService.Update(productReviewHistory);
            return isUpdated;
        }

        /// <summary>
        /// Update the product review history.
        /// </summary>
        /// <param name="itemId">Item Id to update in product review history.</param>
        /// <param name="changedFields">Comma seperated changed fields.</param>
        public void UpdateEditReviewHistory(int itemId, string changedFields)
        {
            // Add entry to Product Review history table.
            ProductReviewHistory productReviewHistory = new ProductReviewHistory();
            productReviewHistory.ProductID = itemId;
            productReviewHistory.VendorID = this.GetVendorId(itemId);
            productReviewHistory.LogDate = DateTime.Now;
            productReviewHistory.Username = System.Web.HttpContext.Current.User.Identity.Name;
            productReviewHistory.Status = ZNodeProductReviewState.PendingApproval.ToString();
            productReviewHistory.Reason = string.Empty;
            productReviewHistory.Description = System.Web.HttpUtility.HtmlEncode(changedFields);

            bool isSuccess = this.Add(productReviewHistory);
        }

        /// <summary>
        /// Get the supplier Id
        /// </summary>
        /// <param name="productId">Product Id to get the product account Id.</param>
        /// <returns>Returns the products supplier Id</returns>
        private int GetVendorId(int productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = productAdmin.GetByProductId(productId);
            return Convert.ToInt32(product.AccountID);
        }

		/// <summary>
		/// Get the AccountId 
		/// </summary>
		/// <param name="portalId">Portal Id to get the account id.</param>
		/// <returns>Returns the Account ID </returns>
		public int GetFranchiseAccountIDByPortalID(int portalId)
		{
			ProductHelper productHelper = new ProductHelper();
			int productDataSet = productHelper.GetFranchiseAccountIDByPortalID(portalId);
			return productDataSet;
		}



        #endregion
    }
}
