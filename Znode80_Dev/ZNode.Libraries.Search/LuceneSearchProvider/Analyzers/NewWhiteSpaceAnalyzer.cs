﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Analyzers
{
    public sealed class NewWhitespaceAnalyzer : Analyzer
    {
        public override TokenStream TokenStream(String fieldName, System.IO.TextReader reader)
        {
            StandardTokenizer baseTokenizer = new StandardTokenizer(LuceneSearchManager.GetVersion, reader);
            StandardFilter standardFilter = new StandardFilter(baseTokenizer);
            LowerCaseFilter lcFilter = new LowerCaseFilter(standardFilter);
            return lcFilter;
        }


    }
}
