﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneReflectionObject
    {
        public object Boost { get; set; }
        public object PropertyName { get; set; }
        public object PropertyValue { get; set; }
    }
}
