﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneSKU
    {
        public int SKUId { get; set; }
        public string SKUName { get; set; }
    }
}
