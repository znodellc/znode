﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LucenePortal
    {
        public int PortalId { get; set; }
        public string PortalStoreName { get; set; }
    }
}
