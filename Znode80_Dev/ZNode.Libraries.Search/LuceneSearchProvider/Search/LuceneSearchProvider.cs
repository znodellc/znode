﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer;
using ZNode.Libraries.Search.Interfaces;
using ZNode.Libraries.Search.LuceneSearchProvider.Search.Constants;
using Directory = Lucene.Net.Store.Directory;
using Version = Lucene.Net.Util.Version;


namespace ZNode.Libraries.Search.LuceneSearchProvider.Search
{
    public class LuceneSearchProvider : IZnodeSearchProvider
    {
        #region Private and Public variables

        private static string ZnodeIndexDirectory = System.Configuration.ConfigurationManager.AppSettings[LuceneLibraryConstants.indexDirectory];
        #endregion

        

        

        #region Public Methods

      

        /// <summary>
        /// Search by Keyword
        /// </summary>
        /// <param name="request"> </param>
        /// <returns>Returns the list of ProductIDs</returns>
        public IZNodeSearchResponse Search(IZNodeSearchRequest request)
        {
            if (string.IsNullOrEmpty(request.SearchText)) request.SearchText = string.Empty;

            // Replacing the special characters ($,!)
            request.SearchText = request.SearchText.Replace("$", "");
            request.SearchText = request.SearchText.Replace("!", "");
            
           // Search the category / Sub-Cateogry for the given text.
            var response = SearchList(request.SearchText, request);

            // Facet Populating
            if (!string.IsNullOrEmpty(request.Category) && request.GetFacets)
            {
                response.Facets = PopulateFacets(request);
            }

            if (request.GetCategoriesHeirarchy)
            {
                response.CategoryItems = PopulateCategoryFacets(request);
            }

            return response;
        }       

        /// <summary>
        /// Find terms matching the given partial word that appear in the highest number of documents.</summary>
        /// <param name="term">A word or part of a word</param>
        /// <returns>A list of suggested completions</returns>
        public List<TypeAheadResponse> SuggestTermsFor(string term, string category, int portalCatalogID,bool externalIdNullCheck)
        {          
            var suggestions = new LuceneSuggestions();

            return suggestions.SuggestTermsFromExistingIndex(term, category, portalCatalogID, externalIdNullCheck);           
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Populate the facets for the given input
        /// </summary>
        /// <param name="request"></param>
        private List<IZNodeSearchFacet> PopulateFacets(IZNodeSearchRequest request)
        {
            //If Category and Search text has values
            var facetSearch = new FacetLuceneSearch();

            return  facetSearch.FacetSearch(request);
        }

        /// <summary>
        /// Populate the facets for the given input
        /// </summary>
        /// <param name="request"></param>
        private List<IZNodeSearchCategoryItem> PopulateCategoryFacets(IZNodeSearchRequest request)
        {
            //If Category and Search text has values
            var facetSearch = new FacetLuceneSearch();

            return facetSearch.CategoryFacet(request);
        }


        public void ReloadIndex()
        {

            LuceneSearchManager.ReopenIndexReader();
        } 

        /// <summary>
        /// Returns the list of values searched in Index
        /// </summary>
        /// <param name="searchQuery"></param>
        /// <returns></returns>
        private IZNodeSearchResponse SearchList(string searchQuery, IZNodeSearchRequest request)
        {
            if (System.IO.Directory.Exists(ZnodeIndexDirectory))
            {
                //try
                //{
                    var search = new LuceneSearchBase();
                    return search.SearchText(request);
                //}
                //catch (Exception)
                //{
                //    return null;

                //}
            }

         return new LuceneSearchResponse(); ;
        }     

       
       
        #endregion
    }
}
