﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Search
{
    public class SearchFacet
    {
        public string AttributeName { set; get; }
        public List<FacetValue> AttributeValues { set; get; }
        public int ControlTypeID { get; set; }
        public SearchFacet()
        {
            AttributeValues = new List<FacetValue>();
        }
    }

    public class FacetValue
    {
        public string AttributeValue { get; set; }
        public long FacetCount { set; get; }
        public bool Selected { get; set; }
    }    
}
