﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZNode.Libraries.Search.Interfaces;


namespace ZNode.Libraries.Search.LuceneSearchProvider.Search
{
    public class LuceneSearchRequest : IZNodeSearchRequest
    {

        public string SearchText { get; set; }
        public string Category { get; set; }
        public string SearchType { get; set; }
        public int PortalCatalogID { get; set; }
        public string FacetRefineBy { get; set; }
        public List<KeyValuePair<string, IEnumerable<string>>> Facets { get; set; }
        public int SortOrder { get; set; }
        public List<SortCriteria> SortCriteria { get; set; }
        public bool GetFacets { get; set; }
        // If true Returns externalIds instead of ProductId 
        public bool ExternalIdEnabled { get; set; }
        
        public bool GetCategoriesHeirarchy { get; set; }

        public bool ExternalIdNotNull { get; set; }

        public List<string> InnerSearchKeywords { get; set; }

		public string Tags { get; set; }
    }
}
