﻿using System;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Lucene.Net.QueryParsers;
using Lucene.Net.Store;
using Lucene.Net.Index;
using Lucene.Net.Search;
using SpellChecker.Net.Search.Spell;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Analysis.NGram;
using Lucene.Net.Documents;
using ZNode.Libraries.Search.Interfaces;
using ZNode.Libraries.Search.LuceneSearchProvider.Analyzers;
using Version = Lucene.Net.Util.Version;


namespace ZNode.Libraries.Search.LuceneSearchProvider.Search
{
   /// <summary>
   /// Implementation to get type ahead feature 
   /// </summary>
    public class LuceneSuggestions : LuceneSearchBase
    {

        public LuceneSuggestions()
            : base(new MultiFieldQueryParser(LuceneSearchManager.GetVersion, new[] { "Name" }, new PerFieldAnalyzerWrapper(new StandardAnalyzer(LuceneSearchManager.GetVersion))))
       {
            
       
       }


       public List<TypeAheadResponse> SuggestTermsFromExistingIndex(string term, string category,int portalID,bool externalIdNullCheck)
       {
           var indexSearcher = LuceneSearchManager.GetIndexSearcher;
           try
           {

               var topDocs = indexSearcher.Search(GetTypeAheadQuery(term, category, portalID, externalIdNullCheck), 10);
               var categoriesIndex = 1;
               var list = new List<TypeAheadResponse>();
               for (int i = 0; i < topDocs.ScoreDocs.Length; i++)
               {
                   ScoreDoc doc = topDocs.ScoreDocs[i];

                   //var docAdded = false;
                   foreach (var categories in indexSearcher.Doc(doc.Doc).GetFields("CategoryHierarchy"))
                   {

                      if(categories.StringValue.StartsWith(portalID.ToString()+"/"))
                       {
                           string categoryName = GetCategory(categories, portalID);

                           if (!string.IsNullOrEmpty(categoryName) || !string.IsNullOrEmpty(category))
                           {
                               var typeahead = new TypeAheadResponse()
                                   {
                                       Name = HttpUtility.HtmlDecode(indexSearcher.Doc(doc.Doc).Get("Name")),
                                       Category =
                                           (string.IsNullOrEmpty(category) &&
                                            categoriesIndex <= LuceneSearchManager.TypeAheadCategories)
                                               ? HttpUtility.HtmlDecode(categoryName)
                                               : ""

                                   };
                               categoriesIndex++;
                               list.Add(typeahead);
                           }

                           if (!string.IsNullOrEmpty(category) ||
                               categoriesIndex > LuceneSearchManager.TypeAheadCategories)
                           {
                               break;
                           }
                       }

                   }
               }
               return list;
           }
           catch (Exception)
           {
               return new List<TypeAheadResponse>();
           }
       
       }

       private string GetCategory(Field categoryField, int portalCatalogId)
       {
           //if (categoryField.StringValue.Contains("0"))
           //{
              var category = categoryField.StringValue.Split(new [] {'/','|'});

               return category[category.Length -2];
           //}           
       }
    }
}