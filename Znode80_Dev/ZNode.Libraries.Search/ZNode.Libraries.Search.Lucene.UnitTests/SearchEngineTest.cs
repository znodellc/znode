﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZNode.Libraries.Search.Interfaces;
using ZLSLS = ZNode.Libraries.Search.LuceneSearchProvider.Search;

namespace ZNode.Libraries.Search.Lucene.UnitTests
{ 
    [TestClass]
    public class SearchEngineTest
    {
        [TestMethod]
        public void AllCategorySearchText()
        {
            var searchtext = "Apple";
            var productIdExpected = 302;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = string.Empty;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void AllCategorySKU()
        {
            var searchtext = "apr234";
            var productIdExpected = 302;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = string.Empty;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void AllCategoryProductNum()
        {
            var searchtext = "ap231";
            var productIdExpected = 302;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = string.Empty;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void AllCategoryBrand()
        {
            var searchtext = "kroger";
            var productIdExpected = 337;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = string.Empty;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void CategorySearchText()
        {
            var searchtext = "Beans";
            var productIdExpected = 318;
            var category = "Vegetables";

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void CategoryBrand()
        {
            var searchtext = "tropicana";
            var productIdExpected = 335;
            var category = "Vegetables";

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void CategorySKU()
        {
            var searchtext = "pe0978";
            var productIdExpected = 316;
            var category = "Vegetables";
            
            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void CategoryProductNum()
        {
            var searchtext = "kw001";
            var productIdExpected = 334;
            var category = "Organic Produce";

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        // Partial Search With All Categories
        [TestMethod]
        public void PartialAllCategorySearchText()
        {
            var searchtext = "app";
            var productIdExpected = 302;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = string.Empty;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void PartialAllCategorySKU()
        {
            var searchtext = "apr";
            var productIdExpected = 302;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = string.Empty;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void PartialAllCategoryProductNum()
        {
            var searchtext = "gr9";
            var productIdExpected = 305;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = string.Empty;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void PartialAllCategoryBrand()
        {
            var searchtext = "tropic";
            var productIdExpected = 328;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = string.Empty;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }
        
        // Partial Search With Category
        [TestMethod]
        public void PartialCategorySearchText()
        {
            var searchtext = "swi";
            var productIdExpected = 348;
            var category = "Cheese";

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void PartialCategorySKU()
        {
            var searchtext = "st89";
            var productIdExpected = 315;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = string.Empty;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void PartialCategoryProductNum()
        {
            var searchtext = "fu237";
            var productIdExpected = 333;
            var category = "Flowers";

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void PartialCategoryBrand()
        {
            var searchtext = "tropic";
            var productIdExpected = 326;
            var category = "Flowers";

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Any(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void SortByProductNameAscending()
        {
            var categoryName = "Gifts";
            var productIdExpected = 306;
            var searchtext = string.Empty;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;
            request.sortOrder = 4;

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.Products.Select(x => x.ProductID == productIdExpected).First());
        }

        [TestMethod]
        public void SortByProductNameDescending()
        {
            var categoryName = "Gifts";
            var productIdExpected = 306;
            var searchtext = string.Empty;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;
            request.sortOrder = 5;

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.Products.Select(x => x.ProductID == productIdExpected).Last());
        }

        [TestMethod]
        public void SortByReviewRating()
        {
            var categoryName = "Gifts";
            var productIdExpected = 306;
            var searchtext = string.Empty;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;
            request.sortOrder = 6;

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.Products.Select(x => x.ProductID == productIdExpected).First());
        }


        // Negative Test Cases
        [TestMethod]
        public void NegAllCategorySearchText()
        {
            var searchtext = "Apple";
            var productIdExpected = 322;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = string.Empty;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(false, response.Products.Select(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void NegAllCategorySKU()
        {
            var searchtext = "apr234";
            var productIdExpected = 334;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = string.Empty;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(false, response.Products.Select(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void NegAllCategoryProductNum()
        {
            var searchtext = "ap231";
            var productIdExpected = 303;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = string.Empty;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(false, response.Products.Select(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void NegAllCategoryBrand()
        {
            var searchtext = "kroger";
            var productIdExpected = 336;

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = string.Empty;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(false, response.Products.Select(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void NegCategorySearchText()
        {
            var searchtext = "Beans";
            var productIdExpected = 346;
            var category = "Vegetables";

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(false, response.Products.Select(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void NegCategoryBrand()
        {
            var searchtext = "tropicana";
            var productIdExpected = 348;
            var category = "Vegetables";

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(false, response.Products.Select(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void NegCategorySKU()
        {
            var searchtext = "pe0978";
            var productIdExpected = 337;
            var category = "Vegetables";

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(false, response.Products.Select(x => x.ProductID == productIdExpected));
        }

        [TestMethod]
        public void NegCategoryProductNum()
        {
            var searchtext = "kw001";
            var productIdExpected = 341;
            var category = "Organic Produce";

            IZNodeSearchRequest request = new ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.Facets = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.IEnumerable<string>>>();
            request.SortCriteria = new System.Collections.Generic.List<Libraries.Search.LuceneSearchProvider.Search.SortCriteria>();

            ZNode.Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider provider = new Libraries.Search.LuceneSearchProvider.Search.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(false, response.Products.Select(x => x.ProductID == productIdExpected));
        }
    }
}
