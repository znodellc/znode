﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ZNode.Libraries.Search.Lucene.UnitTests.Config
{
   public static class TestUtility
    {


       public static void CreateXmlFile <T>(T obj,string fileLocation)
       {
           using (var stream = new MemoryStream())
           using (var writer = new StreamWriter(stream, System.Text.Encoding.UTF8))
           {
               new XmlSerializer(obj.GetType()).Serialize(writer, obj);

               using (FileStream file = new FileStream(fileLocation, FileMode.Create, FileAccess.Write))
               {
                   stream.WriteTo(file);
               }

           }
       
       }


       public static T CreateObjectFromXmlFile<T>(string fileLocation)
       {
          
           XmlSerializer ser = new XmlSerializer(typeof(T));

           using (XmlReader reader = XmlReader.Create(fileLocation))
           {
               return (T)ser.Deserialize(reader);
           }

       }

    }
}
