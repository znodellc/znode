﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZNode.Libraries.Search.Interfaces;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;
using ZLSLS = ZNode.Libraries.Search.LuceneSearchProvider.Search;

namespace ZNode.Libraries.Search.Lucene.UnitTests
{
    [TestClass]
    public class SearchFacetTest
    {
        #region Positive Test Cases

        [TestMethod]
        public void CategoryFacetTest()
        {
            string category = "Fruit";
            IZNodeSearchRequest request = new LuceneSearchRequest();
            request.searchText = string.Empty;
            request.category = category;

            var facetSearch = new FacetLuceneSearch();

           var result = facetSearch.CategoryFacet(request);

          
            // Assert.AreEqual(true, response.Facets.Count() > 0);
        }

        
        
        
        [TestMethod]
        public void FacetGroupCountTest()
        {
            string category = "Fruit";
            IZNodeSearchRequest request = new LuceneSearchRequest();
            request.searchText = string.Empty;
            request.category = category;

            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Facets.Count() > 0);
        }


        [TestMethod]
        public void FacetsNameTest()
        {
            string category = "Vegetables";
            string facetname = "Calories";

            IZNodeSearchRequest request = new LuceneSearchRequest();
            request.searchText = string.Empty;
            request.category = category;

            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Facets[0].AttributeName.Split('|')[0] == facetname);
        }

        [TestMethod]
        public void FacetsAllcategoriesSearchTextTest()
        {
            string category = string.Empty;

            IZNodeSearchRequest request = new LuceneSearchRequest();
            request.searchText = string.Empty;
            request.category = category;

            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Facets == null);
        }

        [TestMethod]
        public void FacetsControlTypeTest()
        {
            string category = "Fruit";
            string facetname = "Calories";
            int controlType = 2;

            IZNodeSearchRequest request = new LuceneSearchRequest();
            request.searchText = string.Empty;
            request.category = category;

            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true,
                            (response.Facets[0].AttributeName.Split('|')[0] == facetname &&
                             response.Facets[0].ControlTypeID == controlType));
        }

        [TestMethod]
        public void FacetCategorySearchText()
        {
            string category = "Wine";
            string searchtext = "Merlot";

            IZNodeSearchRequest request = new LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;

            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.Facets.Count() > 0);
        }


        [TestMethod]
        public void FacetValueTest()
        {
            string category = "Fruit";
            string facetValue = "< 15";

            IZNodeSearchRequest request = new LuceneSearchRequest();
            request.searchText = string.Empty;
            request.category = category;

            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Facets[0].AttributeValues.Any(x => x.AttributeValue == facetValue));
        }


        [TestMethod]
        public void FacetValueCountTest()
        {
            string category = "Fruit";

            IZNodeSearchRequest request = new LuceneSearchRequest();
            request.searchText = string.Empty;
            request.category = category;

            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Facets[0].AttributeValues[0].FacetCount > 0);
        }

        #endregion

        #region Nagative Test Cases

        [TestMethod]
        public void FacetsNameNotEqualtoNegTest()
        {
            string category = "Fruit";
            string searchText = "Color";
            IZNodeSearchRequest request = new LuceneSearchRequest();
            request.searchText = searchText;
            request.category = category;

            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Facets.Count() == 0);
        }


        [TestMethod]
        public void FacetsCategoryWithBrandNegTest()
        {
            string category = "Nuts";
            string searchText = "Test Brand";
            IZNodeSearchRequest request = new LuceneSearchRequest();

            request.searchText = searchText;
            request.category = category;

            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Facets.Count() == 0);
        }

        [TestMethod]
        public void FacetCategorySearchTextNegTest()
        {
            string category = "Wine";
            string searchText = "Fruit";

            IZNodeSearchRequest request = new LuceneSearchRequest();
            request.searchText = searchText;
            request.category = category;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, (response.Facets.Count() == 0));
        }

        [TestMethod]
        public void FacetOnlySearchTextNegTest()
        {
            string searchText = "Wine";
            IZNodeSearchRequest request = new LuceneSearchRequest();
            request.searchText = searchText;
            request.category = string.Empty;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, (response.Facets == null));
        }


        [TestMethod]
        public void FacetCategoryMismatchSearchTextNegTest()
        {
            string category = "Nuts";
            string searchtext = "Merlot";
            IZNodeSearchRequest request = new LuceneSearchRequest();

            request.searchText = searchtext;
            request.category = category;

            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Facets.Count() == 0);
        }

        #endregion
    }
}
