﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;

namespace ZNode.Libraries.Search.Interfaces
{
    public interface IZNodeSearchRequest
    {
        string SearchText { get; set; }
        /// <summary>
        /// Search within Search Text
        /// </summary>
        List<string> InnerSearchKeywords { get; set; }
        string Category { get; set; }
        string SearchType { get; set; }
        int PortalCatalogID { get; set; }
        string FacetRefineBy { get; set; }
        int SortOrder { get; set; }
        // If true , returns External Ids instead of product Id 
        bool ExternalIdEnabled { get; set; }
        // if true , returns only documents that has external Id populated
        bool ExternalIdNotNull { get; set; }
        bool GetFacets { get; set; }
        bool GetCategoriesHeirarchy { get; set; }

        List<KeyValuePair<string, IEnumerable<string>>> Facets { get; set; }

        List<SortCriteria> SortCriteria { get; set; }
    }
}
