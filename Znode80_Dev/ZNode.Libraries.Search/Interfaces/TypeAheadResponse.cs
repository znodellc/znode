﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.Search.Interfaces
{
    public interface ITypeAheadResponse
    {
        string Name { set; get; }

        string Category { set; get; }
    
    }

    [Serializable]
    public class TypeAheadResponse : ITypeAheadResponse
    {
        public string Name { set; get; }

        public string Category { set; get; }

    }
}
