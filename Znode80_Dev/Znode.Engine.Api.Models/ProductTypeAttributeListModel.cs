﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// List Model for list of Attributes by product Type.
    /// </summary>
    public class ProductTypeAttributeListModel : BaseListModel
    {
        public Collection<ProductTypeAttributeModel> ProductTypeAttribute { get; set; }
        public Collection<AttributeTypeModel> attributeType { get; set; } 

        public ProductTypeAttributeListModel()
		{
            ProductTypeAttribute = new Collection<ProductTypeAttributeModel>();
            attributeType = new Collection<AttributeTypeModel>();
		}
    }
}
