﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class TaxRuleTypeListModel : BaseListModel
	{
		public Collection<TaxRuleTypeModel> TaxRuleTypes { get; set; }

		public TaxRuleTypeListModel()
		{
			TaxRuleTypes = new Collection<TaxRuleTypeModel>();
		}
	}
}