﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class RMAConfigurationModel : BaseModel
    {
        public RMAConfigurationModel()
        {
            DisplayName = null;
            EmailId = null;
            ReturnMailingAddress = null;
            ShippingInstruction = null;
            GiftCardNotification = null;

            RMAConfigId = 1;
            MaxDays = null;
            GiftCardExprirationPeriod = null;

            EnableEmailNotification = true;
        }

        public string DisplayName { get; set; }
        public string EmailId { get; set; }
        public string ReturnMailingAddress { get; set; }
        public string ShippingInstruction { get; set; }
        public string GiftCardNotification { get; set; }

        public int RMAConfigId { get; set; }
        public int? MaxDays { get; set; }
        public int? GiftCardExprirationPeriod { get; set; }

        public bool? EnableEmailNotification { get; set; }
    }
}
