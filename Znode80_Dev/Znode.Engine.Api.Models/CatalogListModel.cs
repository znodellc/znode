﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class CatalogListModel : BaseListModel
	{
		public Collection<CatalogModel> Catalogs { get; set; }

		public CatalogListModel()
		{
			Catalogs = new Collection<CatalogModel>();
		}
	}
}
