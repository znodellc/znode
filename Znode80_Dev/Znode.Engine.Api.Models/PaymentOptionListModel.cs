﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class PaymentOptionListModel : BaseListModel
	{
		public Collection<PaymentOptionModel> PaymentOptions { get; set; }

		public PaymentOptionListModel()
		{
			PaymentOptions = new Collection<PaymentOptionModel>();
		}
	}
}
