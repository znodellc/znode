﻿using System;

namespace Znode.Engine.Api.Models
{
	public class WishListModel : BaseModel
	{
		public int AccountId { get; set; }
		public DateTime CreateDate { get; set; }
		public string Custom { get; set; }
		public int ProductId { get; set; }
		public int WishListId { get; set; }
	    public ProductModel Product { get; set; }
	
		public WishListModel Sample()
		{
			return new WishListModel
			{
				AccountId = 14,
				CreateDate = Convert.ToDateTime("2013-08-01 13:45:09.425"),
				Custom = "",
				ProductId = 302,
				WishListId = 1,
                Product = new ProductModel()
			};
		}
	}
}
