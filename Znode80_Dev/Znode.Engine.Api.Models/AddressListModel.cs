﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class AddressListModel : BaseListModel
	{
		public Collection<AddressModel> Addresses { get; set; }

		public AddressListModel()
		{
			Addresses = new Collection<AddressModel>();
		}
	}
}
