﻿namespace Znode.Engine.Api.Models
{
	public class PaymentGatewayModel : BaseModel
	{
		public string Name { get; set; }
		public int PaymentGatewayId { get; set; }
		public string Url { get; set; }

		public PaymentGatewayModel Sample()
		{
			return new PaymentGatewayModel
			{
				Name = "Authorize.Net",
				PaymentGatewayId = 1,
				Url = "http://www.authorize.net"
			};
		}
	}
}