﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class DomainListModel : BaseListModel
	{
		public Collection<DomainModel> Domains { get; set; }

		public DomainListModel()
		{
			Domains = new Collection<DomainModel>();
		}
	}
}
