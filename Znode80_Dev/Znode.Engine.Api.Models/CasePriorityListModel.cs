﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class CasePriorityListModel : BaseListModel
	{
		public Collection<CasePriorityModel> CasePriorities { get; set; }

		public CasePriorityListModel()
		{
			CasePriorities = new Collection<CasePriorityModel>();
		}
	}
}
