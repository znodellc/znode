﻿namespace Znode.Engine.Api.Models
{
	public class ImageModel : BaseModel
	{
		public string AlternateThumbnailImageFile { get; set; }
		public int? DisplayOrder { get; set; }
		public string ImageAltTag { get; set; }
		public string ImageFile { get; set; }
		public string ImageLargePath { get; set; }
		public string ImageMediumPath { get; set; }
		public string ImageSmallPath { get; set; }
		public string ImageSmallThumbnailPath { get; set; }
		public string ImageThumbnailPath { get; set; }
		public bool IsActive { get; set; }
		public string Name { get; set; }
		public int ProductId { get; set; }
		public int ProductImageId { get; set; }
		public int? ProductImageTypeId { get; set; }
		public int? ReviewStateId { get; set; }
		public bool ShowOnCategoryPage { get; set; }

		public ImageModel Sample()
		{
			return new ImageModel
			{
				AlternateThumbnailImageFile = "",
				DisplayOrder = 1,
				ImageAltTag = "",
				ImageFile = "j0422281.jpg",
				ImageLargePath = "~/data/default/images/catalog/450/j0422281.jpg",
				ImageMediumPath = "~/data/default/images/catalog/250/j0422281.jpg",
				ImageSmallPath = "~/data/default/images/catalog/100/j0422281.jpg",
				ImageSmallThumbnailPath = "~/data/default/images/catalog/37/j0422281.jpg",
				ImageThumbnailPath = "~/data/default/images/catalog/50/j0422281.jpg",
				IsActive = true,
				Name = "Product Image",
				ProductId = 304,
				ProductImageId = 6,
				ProductImageTypeId = 1,
				ReviewStateId = 20,
				ShowOnCategoryPage = false
			};
		}
	}
}