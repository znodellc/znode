﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class SupplierListResponse : BaseListResponse
	{
		public Collection<SupplierModel> Suppliers { get; set; }
	}
}
