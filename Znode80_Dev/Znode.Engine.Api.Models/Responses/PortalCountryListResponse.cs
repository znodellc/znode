﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class PortalCountryListResponse : BaseListResponse
	{
		public Collection<PortalCountryModel> PortalCountries { get; set; }
	}
}
