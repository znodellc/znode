﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class PromotionListResponse : BaseListResponse
	{
		public Collection<PromotionModel> Promotions { get; set; }
	}
}
