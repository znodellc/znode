﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// Response List for Attributes as per Product Type.
    /// </summary>
    public class ProductTypeAttributeListResponse : BaseListResponse
    {
        public Collection<ProductTypeAttributeModel> ProductTypeAttributes { get; set; }

        public ProductTypeAssociatedAttributeTypesListModel ProductTypeAssociatedAttributeTypes { get; set; }
    }
}
