﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class SupplierTypeListResponse : BaseListResponse
	{
		public Collection<SupplierTypeModel> SupplierTypes { get; set; }
	}
}
