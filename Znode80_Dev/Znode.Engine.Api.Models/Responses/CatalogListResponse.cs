﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class CatalogListResponse : BaseListResponse
	{
		public Collection<CatalogModel> Catalogs { get; set; }
	}
}
