﻿namespace Znode.Engine.Api.Models.Responses
{
    public class MessageConfigResponse : BaseResponse
    {
        public MessageConfigModel MessageConfig { get; set; }
    }
}
