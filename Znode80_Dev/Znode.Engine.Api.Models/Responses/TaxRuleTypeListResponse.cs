﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class TaxRuleTypeListResponse : BaseListResponse
	{
		public Collection<TaxRuleTypeModel> TaxRuleTypes { get; set; }
	}
}
