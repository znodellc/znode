﻿namespace Znode.Engine.Api.Models.Responses
{
	public class ManufacturerResponse : BaseResponse
	{
		public ManufacturerModel Manufacturer { get; set; }
	}
}
