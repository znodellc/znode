﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class ReviewListResponse : BaseListResponse
	{
		public Collection<ReviewModel> Reviews { get; set; }
	}
}
