﻿namespace Znode.Engine.Api.Models.Responses
{
	public class ProductCategoryResponse : BaseResponse
	{
		public ProductCategoryModel ProductCategory { get; set; }
	}
}
