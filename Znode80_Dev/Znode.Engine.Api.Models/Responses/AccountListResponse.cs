﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class AccountListResponse : BaseListResponse
	{
		public Collection<AccountModel> Accounts { get; set; }
	}
}
