﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class FacetGroupListResponse : BaseListResponse
    {
        public Collection<FacetGroupModel> FacetGroups { get; set; }
    }
}
