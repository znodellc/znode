﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class PortalListResponse : BaseListResponse
	{
		public Collection<PortalModel> Portals { get; set; }
	}
}
