﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class ThemeListResponse : BaseListResponse
    {
        public Collection<ThemeModel> Themes { get; set; }
    }
}
