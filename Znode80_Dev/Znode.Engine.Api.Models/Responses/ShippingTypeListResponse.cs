﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class ShippingTypeListResponse : BaseListResponse
	{
		public Collection<ShippingTypeModel> ShippingTypes { get; set; }
	}
}
