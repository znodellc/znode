﻿namespace Znode.Engine.Api.Models.Responses
{
	public class AttributeTypeResponse : BaseResponse
	{
		public AttributeTypeModel AttributeType { get; set; }
	}
}
