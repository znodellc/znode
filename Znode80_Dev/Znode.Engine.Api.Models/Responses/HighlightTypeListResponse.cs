﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class HighlightTypeListResponse : BaseListResponse
	{
		public Collection<HighlightTypeModel> HighlightTypes { get; set; }
	}
}
