﻿namespace Znode.Engine.Api.Models.Responses
{
	public class ReviewResponse : BaseResponse
	{
		public ReviewModel Review { get; set; }
	}
}
