﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// List Response model for profile
    /// </summary>
    public class ProfileListResponse : BaseListResponse
    {
        public Collection<ProfileModel> Profiles { get; set; }
    }
}
