﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class InventoryListResponse : BaseListResponse
	{
		public Collection<InventoryModel> Inventories { get; set; }
	}
}
