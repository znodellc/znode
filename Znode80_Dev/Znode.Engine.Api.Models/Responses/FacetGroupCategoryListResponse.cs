﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class FacetGroupCategoryListResponse : BaseListResponse
    {
        public Collection<FacetGroupCategoryModel> FacetGroupCategoryList { get; set; }
    }
}
