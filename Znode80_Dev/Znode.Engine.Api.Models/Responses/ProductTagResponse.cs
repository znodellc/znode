﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class ProductTagResponse:BaseResponse
    {
        public ProductTagsModel ProductTag { get; set; }
    }
}
