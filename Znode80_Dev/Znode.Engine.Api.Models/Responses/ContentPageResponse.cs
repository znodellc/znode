﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Responses
{
    public class ContentPageResponse : BaseResponse
    {
        public ContentPageModel ContentPage { get; set; }
        public ContentPageModel ContentPageCopy { get; set; }
        public bool IsContentPageAdded { get; set; }
    }
}
