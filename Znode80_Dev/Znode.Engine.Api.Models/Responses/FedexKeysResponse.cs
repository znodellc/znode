﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Responses
{
   public class FedexKeysResponse:BaseResponse
    {
       public DataSet fedexkeys { get; set; }
    }
}
