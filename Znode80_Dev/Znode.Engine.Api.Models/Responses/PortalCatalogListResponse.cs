﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class PortalCatalogListResponse : BaseListResponse
	{
		public Collection<PortalCatalogModel> PortalCatalogs { get; set; }
	}
}
