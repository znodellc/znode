﻿namespace Znode.Engine.Api.Models.Responses
{
	public class PaymentOptionResponse : BaseResponse
	{
		public PaymentOptionModel PaymentOption { get; set; }
	}
}
