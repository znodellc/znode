﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class CountryListResponse : BaseListResponse
	{
		public Collection<CountryModel> Countries { get; set; }
	}
}
