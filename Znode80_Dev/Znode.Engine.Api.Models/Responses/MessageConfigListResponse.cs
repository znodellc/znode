﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class MessageConfigListResponse : BaseListResponse
    {
        public Collection<MessageConfigModel> MessageConfigs { get; set; }
    }
}
