﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class GiftCardListResponse : BaseListResponse
	{
		public Collection<GiftCardModel> GiftCards { get; set; }
	}
}
