﻿namespace Znode.Engine.Api.Models.Responses
{
	public class PaymentTypeResponse : BaseResponse
	{
		public PaymentTypeModel PaymentType { get; set; }
	}
}
