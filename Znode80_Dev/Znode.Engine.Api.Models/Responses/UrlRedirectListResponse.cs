﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class UrlRedirectListResponse : BaseListResponse
    {
        public Collection<UrlRedirectModel> UrlRedirects { get; set; }
    }
}
