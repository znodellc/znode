﻿namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// Response model for profile
    /// </summary>
    public class ProfileResponse : BaseResponse
    {
        public ProfileModel Profile { get; set; }
    }
}
