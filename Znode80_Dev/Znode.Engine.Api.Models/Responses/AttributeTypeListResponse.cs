﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class AttributeTypeListResponse : BaseListResponse
	{
		public Collection<AttributeTypeModel> AttributeTypes { get; set; }
	}
}
