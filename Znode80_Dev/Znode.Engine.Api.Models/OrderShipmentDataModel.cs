﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class OrderShipmentDataModel : BaseModel
    {
        public string addressid { get; set; }
        public string productid { get; set; }
        public int quantity { get; set; }
        public string slno { get; set; }
        public string itemguid { get; set; }
        public string ShippingDescription { get; set; }
        public int ShippingId { get; set; }
        public string ShippingCode { get; set; }
    }
}
