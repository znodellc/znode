﻿namespace Znode.Engine.Api.Models
{
	public enum CreditCardTypeModel
	{      
		AmericanExpress,
		Diners,
		Discover,
		Jcb,
		MasterCard,
		Solo,
		Maestro,
		Visa
	}
}
