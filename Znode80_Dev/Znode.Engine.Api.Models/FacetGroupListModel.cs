﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class FacetGroupListModel : BaseListModel
    {
        public Collection<FacetGroupModel> FacetGroups { get; set; }

        public FacetGroupListModel()
        {
            FacetGroups = new Collection<FacetGroupModel>();
        }
    }
}
