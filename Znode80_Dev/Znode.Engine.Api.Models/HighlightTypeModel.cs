﻿namespace Znode.Engine.Api.Models
{
	public class HighlightTypeModel : BaseModel
	{
		public string Description { get; set; }
		public int HighlightTypeId { get; set; }
		public string Name { get; set; }	

		public HighlightTypeModel Sample()
		{
			return new HighlightTypeModel
			{
				Description = "Default Highlight",
				HighlightTypeId = 1,
				Name = "Default Highlight"
			};
		}
	}
}
