﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class SuggestedSearchListModel : BaseListModel
	{
		public Collection<SuggestedSearchModel> SuggestedSearchResults { get; set; }

		public SuggestedSearchListModel()
		{
			SuggestedSearchResults = new Collection<SuggestedSearchModel>();
		}
	}
}
