﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class ManufacturerListModel : BaseListModel
	{
		public Collection<ManufacturerModel> Manufacturers { get; set; }

		public ManufacturerListModel()
		{
			Manufacturers = new Collection<ManufacturerModel>();
		}
	}
}