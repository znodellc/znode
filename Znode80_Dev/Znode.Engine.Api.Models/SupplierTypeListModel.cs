﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class SupplierTypeListModel : BaseListModel
	{
		public Collection<SupplierTypeModel> SupplierTypes { get; set; }

		public SupplierTypeListModel()
		{
			SupplierTypes = new Collection<SupplierTypeModel>();
		}
	}
}