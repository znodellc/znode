﻿namespace Znode.Engine.Api.Models
{
	public class SuggestedSearchModel : BaseModel
	{
		public string Category { set; get; }
		public string CategoryName;
		public string ProductName;
		public string Term { set; get; }
        public bool ExternalIdNullCheck { get; set; }
	}
}
