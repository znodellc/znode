﻿
namespace Znode.Engine.Api.Models
{
    public class ProductTagsModel : BaseModel
    {
        public string ProductTags { get; set; }
        public int TagId { get; set; }
        public int ProductId { get; set; }
    }
}
