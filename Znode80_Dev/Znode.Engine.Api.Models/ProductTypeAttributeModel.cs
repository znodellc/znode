﻿
namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// Model for ProductTypeAttributeModel.
    /// </summary>
    public class ProductTypeAttributeModel : BaseModel
    {
        public int ProductAttributeTypeID { get; set; }
        public int ProductTypeId { get; set; }
        public int AttributeTypeId { get; set; }
    }
}
