﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class TaxClassListModel : BaseListModel
	{
		public Collection<TaxClassModel> TaxClasses { get; set; }

		public TaxClassListModel()
		{
			TaxClasses = new Collection<TaxClassModel>();
		}
	}
}