﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
   public class BundleViewModel
    {
        //Znode Version 7.2.2 - Start

        public string ExternalId { get; set; }
        public int[] AddOnValueIds { get; set; }
        public int ProductId { set; get; }
        public string SelectedAttributes { get; set; }
        public string Sku { set; get; }
        //Znode Version 7.2.2 - End
    }
}
