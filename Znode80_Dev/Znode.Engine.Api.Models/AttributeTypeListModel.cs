﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class AttributeTypeListModel : BaseListModel
	{
		public Collection<AttributeTypeModel> AttributeTypes { get; set; }

		public AttributeTypeListModel()
		{
			AttributeTypes = new Collection<AttributeTypeModel>();
		}
	}
}
