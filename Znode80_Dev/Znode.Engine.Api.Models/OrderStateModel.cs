﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class OrderStateModel
    {
        public int OrderStateID { get; set; }
        public string OrderStateName { get; set; }
        public string Description { get; set; }
    }
}
