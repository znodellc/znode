﻿namespace Znode.Engine.Api.Models
{
	public class CasePriorityModel : BaseModel
	{
		public int CasePriorityId { get; set; }
		public int DisplayOrder { get; set; }
		public string Name { get; set; }

		public CasePriorityModel Sample()
		{
			return new CasePriorityModel
			{
				CasePriorityId = 2,
				DisplayOrder = 2,
				Name = "Medium"
			};
		}
	}
}
