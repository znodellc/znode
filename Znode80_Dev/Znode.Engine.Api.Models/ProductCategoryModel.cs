﻿using System;

namespace Znode.Engine.Api.Models
{
	public class ProductCategoryModel : BaseModel
	{
		public DateTime? BeginDate { get; set; }
		public CategoryModel Category { get; set; }
		public int CategoryId { get; set; }
		public int? CssId { get; set; }
		public int? DisplayOrder { get; set; }
		public DateTime? EndDate { get; set; }
		public bool IsActive { get; set; }
		public int? MasterPageId { get; set; }
		public ProductModel Product { get; set; }
		public int ProductCategoryId { get; set; }
		public int ProductId { get; set; }
		public int? ThemeId { get; set; }
		
	    public ProductCategoryModel Sample()
		{
			return new ProductCategoryModel
			{
				BeginDate = null,
				CategoryId = 81,
				CssId = null,
				DisplayOrder = 5,
				EndDate = null,
				IsActive = true,
				MasterPageId = null,
				ProductCategoryId = 1,
				ProductId = 302,
				ThemeId = null
			};
		}
	}
}