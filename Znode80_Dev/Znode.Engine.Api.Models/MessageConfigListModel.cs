﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class MessageConfigListModel : BaseListModel
    {
        public Collection<MessageConfigModel> MessageConfigs { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public MessageConfigListModel()
        {
            MessageConfigs = new Collection<MessageConfigModel>();
        }
    }
}