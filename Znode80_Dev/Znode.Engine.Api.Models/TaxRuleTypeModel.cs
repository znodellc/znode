﻿namespace Znode.Engine.Api.Models
{
	public class TaxRuleTypeModel : BaseModel
	{
		public string ClassName { get; set; }
		public string Description { get; set; }
		public bool IsActive { get; set; }
		public string Name { get; set; }
		public int? PortalId { get; set; }
		public int TaxRuleTypeId { get; set; }

		public TaxRuleTypeModel Sample()
		{
			return new TaxRuleTypeModel
			{
				ClassName = "ZnodeTaxSalesTax",
				Description = "Applies sales tax to the shopping cart.",
				IsActive = true,
				Name = "Sales Tax",
				PortalId = null,
				TaxRuleTypeId = 1
			};
		}
	}
}
