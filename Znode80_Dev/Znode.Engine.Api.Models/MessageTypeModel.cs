﻿
namespace Znode.Engine.Api.Models
{
	public class MessageTypeModel : BaseModel
	{
		public int DisplayOrder { get; set; }
		public bool IsActive { get; set; }
		public int MessageTypeId { get; set; }
		public string Name { get; set; }

		public MessageTypeModel Sample()
		{
			return new MessageTypeModel
			{
				DisplayOrder = 1,
				MessageTypeId = 1,
				Name = "Message Block",
				IsActive = true
			};
		}
	}
}
