﻿namespace Znode.Engine.Api.Models
{
	public class AccountTypeModel : BaseModel
	{
		public int AccountTypeId { get; set; }
		public string AccountTypeName { get; set; }

		public AccountTypeModel Sample()
		{
			return new AccountTypeModel
			{
				AccountTypeId = 0,
				AccountTypeName = "CUSTOMER"
			};
		}
	}
}
