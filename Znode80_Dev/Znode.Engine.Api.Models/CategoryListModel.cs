﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class CategoryListModel : BaseListModel
	{
		public Collection<CategoryModel> Categories { get; set; }

		public CategoryListModel()
		{
			Categories = new Collection<CategoryModel>();
		}
	}
}
