﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class PortalCatalogListModel : BaseListModel
	{
		public Collection<PortalCatalogModel> PortalCatalogs { get; set; }

		public PortalCatalogListModel()
		{
			PortalCatalogs = new Collection<PortalCatalogModel>();
		}
	}
}
