﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class HighlightListModel : BaseListModel
	{
		public Collection<HighlightModel> Highlights { get; set; }

		public HighlightListModel()
		{
			Highlights = new Collection<HighlightModel>();
		}
	}
}
