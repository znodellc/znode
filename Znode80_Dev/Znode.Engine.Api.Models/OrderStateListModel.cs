﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class OrderStateListModel : BaseListModel
    {
        public Collection<OrderStateModel> OrderStates { get; set; }
        public OrderStateListModel()
        {
            OrderStates = new Collection<OrderStateModel>();
        }
    }
}
