﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class AccountListModel : BaseListModel
	{
		public Collection<AccountModel> Accounts { get; set; }

		public AccountListModel()
		{
			Accounts = new Collection<AccountModel>();
		}
	}
}
