﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class GiftCardListModel : BaseListModel
	{
		public Collection<GiftCardModel> GiftCards { get; set; }

		public GiftCardListModel()
		{
			GiftCards = new Collection<GiftCardModel>();
		}
	}
}
