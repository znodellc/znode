﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class PromotionListModel : BaseListModel
	{
		public Collection<PromotionModel> Promotions { get; set; }

		public PromotionListModel()
		{
			Promotions = new Collection<PromotionModel>();
		}
	}
}