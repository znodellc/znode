﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ReasonReturnModel : BaseModel
    {    
        public ReasonReturnModel()
        {
            ReasonId = 1;
            Name = "";
            IsEnabled = true;
        }

        public string Name { get; set; }

        public int ReasonId { get; set; }

        public bool IsEnabled { get; set; }
    }
}
