﻿using System;

namespace Znode.Engine.Api.Models
{
    public class GiftCardHistoryModel : BaseModel
    {
        public int GiftCardHistoryId { get; set; }
        public string GiftCardNumber { get; set; }
        public int GiftCardId { get; set; }
        public int OrderId { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal TransactionAmount { get; set; }

        public GiftCardHistoryModel Sample()
        {
            return new GiftCardHistoryModel
            {
                GiftCardHistoryId = 1,
                GiftCardId = 5,
                OrderId = 123,
                TransactionAmount = 56.12m,
                TransactionDate = Convert.ToDateTime("2013-09-01 13:17:43.446")
            };
        }
    }
}
