﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// Profile list model 
    /// </summary>
    public class ProfileListModel : BaseListModel
    {
        /// <summary>
        /// constructor for profile model
        /// </summary>
        public Collection<ProfileModel> Profiles { get; set; }

        public ProfileListModel()
        {
            Profiles = new Collection<ProfileModel>();
        }
    }
}
