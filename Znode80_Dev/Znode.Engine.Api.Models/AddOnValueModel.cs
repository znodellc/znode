﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Znode.Engine.Api.Models
{
	public class AddOnValueModel :BaseModel	
	{
		//private decimal _price;
		
		public AddOnValueModel()
		{
		}
		

		/// <summary>
		/// Gets or sets the AddOnValue id
		/// </summary>
		public int AddOnValueId { get; set; }

		/// <summary>
		/// Gets or sets the AddOnid for this AddOn Value
		/// </summary>
		public int AddOnId { get; set; }

		/// <summary>
		/// Gets or sets the Add-on value Name
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the Add-on value description
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the SKU Name
		/// </summary>
		public string SKU { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the add-on value is default.
		/// </summary>
		public bool IsDefault { get; set; }

		/// <summary>
		/// Gets or sets the qantity on stock for this addonValue
		/// </summary>
		public int QuantityOnHand { get; set; }

		/// <summary>
		/// Gets or sets the order of the display
		/// </summary>
		public int DisplayOrder { get; set; }

		/// <summary>
		/// Gets or sets the retail price value for this addon value
		/// </summary>
		public decimal RetailPrice { get; set; }

		/// <summary>
		/// Gets or sets the sale price value for this addon value
		/// </summary>
		public decimal? SalePrice { get; set; }

		/// <summary>
		/// Gets or sets the wholesale price value for this addon value
		/// </summary>
		public decimal? WholesalePrice { get; set; }

		///// <summary>
		///// Gets a value indicating whether to use wholesale price.
		///// </summary>
		//[XmlIgnore()]
		//public bool UseWholeSalePrice
		//{
		//	get
		//	{
		//		if (System.Web.HttpContext.Current.Session["ProfileCache"] != null)
		//		{
		//			ZNode.Libraries.DataAccess.Entities.Profile profile = (ZNode.Libraries.DataAccess.Entities.Profile)System.Web.HttpContext.Current.Session["ProfileCache"];

		//			if (profile.UseWholesalePricing.HasValue)
		//			{
		//				return profile.UseWholesalePricing.Value;
		//			}
		//		}

		//		return false;
		//	}
		//}

		///// <summary>
		///// Gets the final price value for this addon value
		///// </summary>
		//[XmlIgnore()]
		//public decimal FinalPrice
		//{
		//	get
		//	{
		//		bool useWholeSalePrice = this.UseWholeSalePrice;
		//		decimal finalPrice = this.RetailPrice;
		//		decimal? finalAddOnPrice = this.RetailPrice;
		//		decimal? decSalePrice = this.SalePrice;
		//		decimal? decWholeSalePrice = this.WholesalePrice;

		//		if (decSalePrice.HasValue && !useWholeSalePrice)
		//		{
		//			finalPrice = decSalePrice.Value;
		//		}

		//		else if (useWholeSalePrice)
		//		{
		//			// If Wholesaleprice has null ,then ignore it
		//			if (decWholeSalePrice.HasValue)
		//			{
		//				finalPrice = decWholeSalePrice.Value;
		//			}
		//			else if (decSalePrice.HasValue)
		//			{
		//				finalPrice = decSalePrice.Value;
		//			}
		//		}

		//		return finalPrice;
		//	}
		//}

		///// <summary>
		///// Gets the final price value for this addon value
		///// </summary>
		//[XmlIgnore()]
		//public decimal Price
		//{
		//	get
		//	{
		//		bool useWholeSalePrice = this.UseWholeSalePrice;
		//		this._price = this.RetailPrice;
		//		decimal? salePrice = this.SalePrice;
		//		decimal? wholeSalePrice = this.WholesalePrice;

		//		// If sale price has value
		//		if (salePrice.HasValue && !useWholeSalePrice)
		//		{
		//			// Then set sale price value
		//			this._price = salePrice.Value;
		//		}
		//		else if (useWholeSalePrice)
		//		{
		//			// If Wholesaleprice has null ,then ignore it
		//			if (wholeSalePrice.HasValue)
		//			{
		//				this._price = wholeSalePrice.Value;
		//			}
		//			else if (salePrice.HasValue)
		//			{
		//				this._price = salePrice.Value;
		//			}
		//		}

		//		return this._price;
		//	}
		//}

		/// <summary>
		/// Gets or sets the weight of this addon value 
		/// </summary>
		public decimal Weight { get; set; }

		/// <summary>
		/// Gets or sets the add-on value package height  
		/// </summary>
		public decimal Height { get; set; }

		/// <summary>
		/// Gets or sets the add-on value package width 
		/// </summary>
		public decimal Width { get; set; }

		/// <summary>
		/// Gets or sets the add-on value package length 
		/// </summary>
		public decimal Length { get; set; }

		/// <summary>
		///  Gets or sets the add-on value shippingRuleTypeId
		/// </summary>
		public int? ShippingRuleTypeId { get; set; }

		///// <summary>
		///// Gets or sets the calculated shipping cost for this addonvalue
		///// </summary>
		//
		//public decimal ShippingCost { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the add-on is free shipping
		/// </summary>
		public bool FreeShippingInd { get; set; }

		/// <summary>
		/// Gets or sets the supplierId for this AddOn Value
		/// </summary>
		public int SupplierId { get; set; }

		/// <summary>
		/// Gets or sets the TaxClassId for this AddOn Value
		/// </summary>
		public int TaxClassId { get; set; }

		/// <summary>
		/// Gets or sets the Add-on value description
		/// </summary>
		[XmlIgnore()]
		public string CustomText { get; set; }
		

		///// <summary>
		///// Gets or sets the discount amount.
		///// </summary>
		//[XmlIgnore()]
		//public decimal DiscountAmount { get; set; }

		///// <summary>
		///// Gets or sets of HST
		///// </summary>
		//[XmlIgnore()]
		//public decimal HST { get; set; }

		///// <summary>
		///// Gets or sets of GST
		///// </summary>
		//[XmlIgnore()]
		//public decimal GST { get; set; }

		///// <summary>
		///// Gets or sets of PST
		///// </summary>
		//[XmlIgnore()]
		//public decimal PST { get; set; }

		///// <summary>
		///// Gets or sets the VAT
		///// </summary>
		//[XmlIgnore()]
		//public decimal VAT { get; set; }

		///// <summary>
		///// Gets or sets sales tax
		///// </summary>
		//[XmlIgnore()]
		//public decimal SalesTax { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the recurring billing is enabled
		/// </summary>
		public bool RecurringBillingInd { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether installmentInd  is enabled.
		/// </summary>

		public bool RecurringBillingInstallmentInd { get; set; }

		/// <summary>
		/// Gets or sets the billing period (days or months), in association with the frequency.
		/// </summary>

		public string RecurringBillingPeriod { get; set; }

		/// <summary>
		/// Gets or sets the billing frequency, in association with the Period.
		/// </summary>

		public string RecurringBillingFrequency { get; set; }

		/// <summary>
		/// Gets or sets the number of billing occurrences or payments for the subscription
		/// </summary>

		public int RecurringBillingTotalCycles { get; set; }

		/// <summary>
		/// Gets or sets the initial amount to be charged during subscription creation,
		/// </summary>

		public decimal RecurringBillingInitialAmount { get; set; }

	
		///// <summary>
		///// Gets or sets the initial amount to be charged during subscription creation,
		///// </summary>
		//
		//public int? ExternalProductId { get; set; }

		///// <summary>
		///// Gets or sets a value indicating whether is tax already calculated for the current shopping cart.
		///// </summary>
		//public bool IsTaxCalculated { get; set; }


		public AddOnValueModel Sample()
		{
			return new AddOnValueModel
			{
				
				AddOnValueId =  1,
				AddOnId = 1,
				CustomText = string.Empty,
				Description = "",
				DisplayOrder = 1,
				FreeShippingInd = false,
				Height =10,
				Length = 20,
				IsDefault = false,
				Name = "Vase",
				QuantityOnHand = 1000,
				RecurringBillingInd = false,
				RecurringBillingInstallmentInd = false,
				RecurringBillingPeriod = null,
				RecurringBillingFrequency = string.Empty,
				RecurringBillingTotalCycles = 1,
				RecurringBillingInitialAmount = 0,
				RetailPrice = (decimal) 10.00,
				SalePrice = (decimal?) 7.25,
				ShippingRuleTypeId = 0,
				SKU = "gb441",
				SupplierId =0,
				TaxClassId = 0,
				Weight = 12,
				WholesalePrice = 0,
				Width = 5
			};
		}




	}
}
