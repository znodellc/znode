﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class EnvironmentConfigModel : BaseModel
    {
        public string DataPath { get; set; }
        public string OriginalImagePath { get; set; }
        public string LargeImagePath { get; set; }
        public string MediumImagePath { get; set; }
        public string SmallImagePath { get; set; }
        public string ThumbnailImagePath { get; set; }
        public string SwatchImagePath { get; set; }
        public string CrossSellImagePath { get; set; }
        public string ConfigPath { get; set; }
        public string ContentPath { get; set; }
        public string ImagePath { get; set; }
        public string ApplicationPath { get; set; }
        public string HostPrefix { get; set; }
        public bool MultiStoreAdminEnabled { get; set; }
    }
}
