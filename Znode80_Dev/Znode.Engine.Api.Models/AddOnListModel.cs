﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
	public class AddOnListModel :BaseListModel
	{
		public Collection<AddOnModel> AddOnModels { get; set; }

		public AddOnListModel()
		{
			AddOnModels = new Collection<AddOnModel>();
		}
	}
}
