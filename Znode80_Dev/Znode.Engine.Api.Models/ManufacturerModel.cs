﻿namespace Znode.Engine.Api.Models
{
	public class ManufacturerModel : BaseModel
	{
		public string Custom1 { get; set; }
		public string Custom2 { get; set; }
		public string Custom3 { get; set; }
		public string Description { get; set; }
		public int DisplayOrder { get; set; }
		public string Email { get; set; }
		public string EmailNotificationTemplate { get; set; }
		public bool IsActive { get; set; }
		public bool? IsDropShipper { get; set; }
		public int ManufacturerId { get; set; }
		public string Name { get; set; }
		public int? PortalId { get; set; }
		public string Website { get; set; }

		public ManufacturerModel Sample()
		{
			return new ManufacturerModel
			{
				Custom1 = "",
				Custom2 = "",
				Custom3 = "",
				Description = "Whole Foods Corporation",
				DisplayOrder = 0,
				Email = "",
				EmailNotificationTemplate = "",
				IsActive = true,
				IsDropShipper = true,
				ManufacturerId = 1,
				Name = "Whole Foods",
				PortalId = null,
				Website = ""				
			};
		}
	}
}