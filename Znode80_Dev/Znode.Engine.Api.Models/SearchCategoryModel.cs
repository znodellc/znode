﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class SearchCategoryModel : BaseModel
	{
		public int Count { get; set; }
		public Collection<SearchCategoryModel> Hierarchy { get; set; }
		public string Id { get; set; }
		public string Name { get; set; }
		public string Title { get; set; }
	}
}