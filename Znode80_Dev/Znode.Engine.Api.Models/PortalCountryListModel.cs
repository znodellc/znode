﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class PortalCountryListModel : BaseListModel
	{
		public Collection<PortalCountryModel> PortalCountries { get; set; }

		public PortalCountryListModel()
		{
			PortalCountries = new Collection<PortalCountryModel>();
		}
	}
}
