﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class CSSModel : BaseModel
    {
        public int CSSID { get; set; }
        public int ThemeID { get; set; }
        public string Name { get; set; }
    }
}
