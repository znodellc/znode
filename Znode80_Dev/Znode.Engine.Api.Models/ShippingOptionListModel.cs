﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class ShippingOptionListModel : BaseListModel
	{
		public Collection<ShippingOptionModel> ShippingOptions { get; set; }

		public ShippingOptionListModel()
		{
			ShippingOptions = new Collection<ShippingOptionModel>();
		}
	}
}