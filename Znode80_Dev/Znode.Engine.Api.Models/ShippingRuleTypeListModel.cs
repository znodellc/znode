﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class ShippingRuleTypeListModel : BaseListModel
	{
		public Collection<ShippingRuleTypeModel> ShippingRuleTypes { get; set; }

		public ShippingRuleTypeListModel()
		{
			ShippingRuleTypes = new Collection<ShippingRuleTypeModel>();
		}
	}
}