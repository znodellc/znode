﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class TaxRuleListModel : BaseListModel
	{
		public Collection<TaxRuleModel> TaxRules { get; set; }

		public TaxRuleListModel()
		{
			TaxRules = new Collection<TaxRuleModel>();
		}
	}
}