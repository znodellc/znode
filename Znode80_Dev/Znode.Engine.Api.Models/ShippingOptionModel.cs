﻿namespace Znode.Engine.Api.Models
{
	public class ShippingOptionModel : BaseModel
	{
		public string CountryCode { get; set; }
		public string Description { get; set; }
		public int DisplayOrder { get; set; }
		public string ExternalId { get; set; }
		public decimal HandlingCharge { get; set; }
		public bool IsActive { get; set; }
		public int? ProfileId { get; set; }
		public string ShippingCode { get; set; }
		public int ShippingOptionId { get; set; }
		public ShippingTypeModel ShippingType { get; set; }
		public int ShippingTypeId { get; set; }
        public ShippingRuleModel ShippingRule { get; set; }
		public ShippingOptionModel Sample()
		{
			return new ShippingOptionModel
			{
				CountryCode = "US",
				Description = "Custom Flat Rate",
				DisplayOrder = 1,
				ExternalId = null,
				HandlingCharge = 2.50m,
				IsActive = true,
				ProfileId = 7,
				ShippingCode = "FLAT",
				ShippingOptionId = 9,
				ShippingTypeId = 1
			};
		}
	}
}
