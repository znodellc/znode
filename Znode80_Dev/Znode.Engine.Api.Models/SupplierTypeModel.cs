﻿namespace Znode.Engine.Api.Models
{
	public class SupplierTypeModel : BaseModel
	{
		public string ClassName { get; set; }
		public string Description { get; set; }
		public bool IsActive { get; set; }
		public string Name { get; set; }
		public int SupplierTypeId { get; set; }

		public SupplierTypeModel Sample()
		{
			return new SupplierTypeModel
			{
				ClassName = "ZnodeSupplierEmail",
				Description = "Sends an email receipt of the order to the supplier.",
				IsActive = true,
				Name = "Email",
				SupplierTypeId = 1
			};
		}
	}
}
