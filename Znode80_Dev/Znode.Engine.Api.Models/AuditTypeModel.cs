﻿using System;

namespace Znode.Engine.Api.Models
{
	public class AuditTypeModel : BaseModel
	{
		public int AuditTypeId { get; set; }
		public DateTime CreateDate { get; set; }
		public string CreatedBy { get; set; }
		public string ModificationType { get; set; }
		public DateTime? UpdateDate { get; set; }
		public string UpdatedBy { get; set; }
		
		public AuditTypeModel Sample()
		{
			return new AuditTypeModel
			{
				AuditTypeId = 1,
				CreateDate = Convert.ToDateTime("2013-04-29 10:49:32.657"),
				CreatedBy = "Username",
				ModificationType = "New",
				UpdateDate = null,
				UpdatedBy = null
			};
		}
	}
}
