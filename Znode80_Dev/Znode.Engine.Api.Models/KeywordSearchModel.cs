﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class KeywordSearchModel : BaseModel
	{
		public string Category { get; set; }
		public bool ExternalIdEnabled { get; set; }
        public bool ExternalIdNotNullCheck { get; set; }
		public Collection<string> InnerSearchKeywords { get; set; }
		public string Keyword { get; set; }
		public Collection<KeyValuePair<string, IEnumerable<string>>> RefineBy { get; set; }

		// For the response
		public Collection<SearchCategoryModel> Categories { get; set; }
		public Collection<SearchFacetModel> Facets { get; set; }
		public Collection<SearchProductModel> Products { get; set; }

		public KeywordSearchModel Sample()
		{
			return new KeywordSearchModel
			{

			};
		}
	}
}
