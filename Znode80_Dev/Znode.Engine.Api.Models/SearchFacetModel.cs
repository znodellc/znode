﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class SearchFacetModel : BaseModel
	{
		public string AttributeName { set; get; }
		public Collection<SearchFacetValueModel> AttributeValues { set; get; }
		public int ControlTypeId { get; set; }
	}
}