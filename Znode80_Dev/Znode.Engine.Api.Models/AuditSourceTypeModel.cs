﻿using System;

namespace Znode.Engine.Api.Models
{
	public class AuditSourceTypeModel : BaseModel
	{
		public int AuditSourceTypeId { get; set; }
		public DateTime CreateDate { get; set; }
		public string CreatedBy { get; set; }
		public string SourceType { get; set; }
		public DateTime? UpdateDate { get; set; }
		public string UpdatedBy { get; set; }

		public AuditSourceTypeModel Sample()
		{
			return new AuditSourceTypeModel
			{
				AuditSourceTypeId = 1,
				CreateDate = Convert.ToDateTime("2013-04-29 10:49:32.657"),
				CreatedBy = "Username",
				SourceType = "Product",
				UpdateDate = null,
				UpdatedBy = null
			};
		}
	}
}
