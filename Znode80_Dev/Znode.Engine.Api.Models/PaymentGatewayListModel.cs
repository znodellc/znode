﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class PaymentGatewayListModel : BaseListModel
	{
		public Collection<PaymentGatewayModel> PaymentGateways { get; set; }

		public PaymentGatewayListModel()
		{
			PaymentGateways = new Collection<PaymentGatewayModel>();
		}
	}
}
