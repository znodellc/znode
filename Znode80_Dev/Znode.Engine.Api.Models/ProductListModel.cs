﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class ProductListModel : BaseListModel
	{
		public Collection<ProductModel> Products { get; set; }

		public ProductListModel()
		{
			Products = new Collection<ProductModel>();
		}
	}
}