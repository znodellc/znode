﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
   public class WishListListModel : BaseListModel
    {

       public Collection<WishListModel> WishLists { get; set; }

       public WishListListModel()
       {
            WishLists = new Collection<WishListModel>();
       }


    }
}
