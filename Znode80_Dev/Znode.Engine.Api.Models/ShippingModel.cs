﻿namespace Znode.Engine.Api.Models
{
	public class ShippingModel : BaseModel
	{
		public string ResponseCode { get; set; }
		public string ResponseMessage { get; set; }
		public decimal ShippingDiscount { get; set; }
		public decimal ShippingHandlingCharge { get; set; }
		public string ShippingName { get; set; }
		public int ShippingOptionId { get; set; }
	}
}
