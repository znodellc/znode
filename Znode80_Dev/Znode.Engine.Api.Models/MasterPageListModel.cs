﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class MasterPageListModel : BaseListModel
    {
        public Collection<MasterPageModel> MasterPages { get; set; }

        public MasterPageListModel()
        {
            MasterPages = new Collection<MasterPageModel>();
        }
    }
}
