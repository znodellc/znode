﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class SkuListModel : BaseListModel
	{
		public Collection<SkuModel> Skus { get; set; }

		public SkuListModel()
		{
			Skus = new Collection<SkuModel>();
		}
	}
}